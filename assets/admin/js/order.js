function undo(id) {
    show_dialog('Bạn có chắc muốn phục hồi đơn hàng #' + id + " không", function () {
        $.post('/admincp/order/undo', {id: id}, function (results) {
            console.log(results);
            if (results == 1) {
                location.reload();
            } else {
                alert('Phục hồi thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
            }
        });
    });
}
function trash(id) {
    show_dialog('Bạn có chắc muốn xóa đơn hàng #' + id + " không", function () {
        $.post('/admincp/order/trash', {id: id}, function (results) {
            if (results == 1) {
                location.reload();
            } else {
                alert('Xóa thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
            }
        });
    });
}
function trash_empty(id) {
    show_dialog('Bạn có chắc muốn xóa hoàn toàn đơn hàng #' + id + " không", function () {
        $.post('/admincp/order/trash_empty', {id: id}, function (results) {
            if (results == 1) {
                location.reload();
            } else {
                alert('Xóa hoàn toàn thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
            }
        });
    });
}