$("#form").validate({
    rules: {
        'title': {required: true},
        'cate_id': {required: true},
        'alias': {required: true}
    },
    messages: {
        'title': 'Vui lòng nhập tiêu đề tin tức',
        'cate_id': 'Vui lòng chọn thể loại',
        'alias': 'alias không được bỏ trống'
    }
});
$(document).ready(function () {
})
