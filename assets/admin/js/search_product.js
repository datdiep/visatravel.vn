var provider_id = provider_id | -1;
function replaceAll(find, replace, str) {
    return str.replace(new RegExp(find, 'g'), replace);
}
var product;
get_for_search();
function get_for_search() {
    $.post('/' + admin_url + 'ajax/get_for_search', {provider_id: provider_id}, function (result) {
        product = JSON.parse(result);
        $.each(product, function (k, v) {
            v['title_temp'] = v['title'].toLowerCase();
            v['alias'] = replaceAll('-', ' ', v['alias']);
            v['product_code'] = v['product_code'].toLowerCase();
        });
    });
}
var current_title = '';
$('#title_search').keyup(function () {
    var title = $('#title_search').val().trim().toLowerCase();
    if (title) {
        if (current_title != title) {
            $('#list_result').html('');
            $.each(product, function (k, v) {
                if (v['product_code'].indexOf(title) !== -1 || v['alias'].indexOf(title) !== -1 || v['title_temp'].indexOf(title) !== -1 || v['id'].indexOf(title) !== -1) {
                    $('#list_result').append('<div id="pro_' + v['id'] + '" onclick="select_product(' + v['id'] + ')" class="row_search">\n\
                     <span>' + v['title'] + ' <br/>Mã :' + v['product_code'] + '#' + v['id'] + '</span>\n\
                    </div>');
                }
            });
            $('#list_result').slideDown();
        }

    } else {
        $('#list_result').slideUp();
        $('#list_result').html('');
    }
    current_title = title;

});

$('#title_search').keydown(function (e) {
    if (e.keyCode == 40 || e.keyCode == 38) {
        var more = e.keyCode == 40 ? 2 : 0;
        var count = $('#list_result div.row_search').length;
        if (count > 0) {
            var current = $('#list_result div.active').index();
            if (current == -1) {
                current = more > 0 ? 1 : count;
            } else {
                current += more;
            }
            if (current > count) {
                current = 1;
            }
            if (current == 0) {
                current = count;
            }
            $('#list_result div.row_search.active').removeClass('active');
            $('#list_result div.row_search:nth-child(' + current + ')').addClass('active');

        }
        return false;
    }
    if (e.keyCode == 13) {
        $('#list_result div.row_search.active').click();
        return false;
    }
});
var check_click_title = 0;
$('body').click(function () {
    if (check_click_title == 0) {
        $('#list_result').slideUp();
        $('#list_result').html('');
    }
    check_click_title = 0;

})
$('#title_search').click(function () {
    current_title = '';
    check_click_title = 1;
    $('#title_search').keyup();
});