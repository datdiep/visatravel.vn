$('#save_cate').click(function () {
    build_cate()
})
function build_cate() {
    var text = '';
    var value = ',';
    var i = 0;
    $('.ul_parent li').each(function (index) {
        if ($(this).hasClass('chose')) {
            value += $(this).attr('attr_id') + ',';
            text += '<span>' + $(this).text().replace(" Bỏ chọn", "") + '</span>';
        }
    });
    if (value == ',')
        value = '';
    $('#data_cate').html(text);
    $('.cat_ids').val(value);
    $('.cat_ids').valid();
    $('#cate_popup').hide();
}
$('#pop_cate').click(function () {
    $('#cate_popup').show();
})
$('.close_pop_cate').click(function () {
    $('#cate_popup').hide();
})
$('.parent').click(function () {
    //$('.ul_parent li').removeClass('chose');
    $(this).addClass('chose');
    $('.ul_parent_child').hide();
    var index = $(this).index() + 2;
    $('.ul_parent_child:nth-child(' + index + ')').show();
    $('.ul_parent_child_child').hide();
    if ($(".un_chose", this).length <= 0) {
        $(this).append(' <span class="un_chose">Bỏ chọn</span>');
    }
})
$('.parent_child').click(function () {
    //$('.ul_parent_child li').removeClass('chose');
    //$('.ul_parent_child li span').remove();
    $(this).addClass('chose');
    $('.ul_parent_child_child').hide();
    var index = $(this).index() + 2;
    $('.ul_parent_child_child:nth-child(' + index + ')').show();
    if ($(".un_chose", this).length <= 0) {
        $(this).append(' <span class="un_chose">Bỏ chọn</span>');
    }

})
$('.parent_child_child').click(function () {
    $(this).addClass('chose');
    if ($(".un_chose", this).length <= 0) {
        $(this).append(' <span class="un_chose">Bỏ chọn</span>');
    }
})
$('.cate').on('click', '.un_chose', function () {
    var parent = $(this).parent();
    if (parent.hasClass('chose')) {
        parent.removeClass('chose');
        var name = parent.attr('class')
        var index = parent.index() + 2;
        var child = $('.ul_' + name + '_child:nth-child(' + index + ')')
        $('li', child).each(function (result) {
            $(this).removeClass('chose');
            $('span', this).remove();
        })
        child.hide();
        $('span', parent).remove();
    }
})
$(document).ready(function () {
})
