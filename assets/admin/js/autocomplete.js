$(document).ready(function () {
    $(".search_input").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/admincp/news/search",
                data: {q: request.term, m: 'complete'},
                success: function (result) {
                    result = JSON.parse(result);
                    console.log(result);
                    if (typeof result !== 'undefined' && result.length > 0)
                    {
                        end_key = (result.length > 6) ? 6 : result.length;
                        var inner_html = "";
                        for (key = 0; key < end_key; key++) {
//                            var price_compare = (result[key].price_compare == 0) ? "" : result[key].price_compare;
                            inner_html += '<li rel=' + result[key].id + '>\n\
                        <img class="thumbnail" src="/assets/upload/page/' + result[key].image + '">\n\
                        <p style="float:left"><span class="title">' + result[key].title + '</span><br>\n\
                        <p class="del_product_id">Xóa</p></li>';
                        }
                        $('.search-results').html(inner_html).fadeIn();
                    } else {
                        $('.search-results').fadeOut();
                    }

                },
                error: function () {
                    response([]);
                }
            });
        }
    })
    $(".search_input").focusout(function () {
        $('.search-results').fadeOut();
    });
    $("div.add_product").on('click', '.search-results li', function () {
        var product_ids = $('.news_ids').val();
        var id = $(this).attr('rel');
        var html = $(this).html();
        product_ids += id + ',';
        $('.news_ids').val(product_ids);
        $('.results').append($(this));
        $(".search_input").val('');
    });
    $("div.add_product").on('click', '.del_product_id', function () {
        var product_ids = $('.news_ids').val();
        var id = $(this).parent().attr('rel');
        str_find = ',' + id + ',';
        product_ids = product_ids.replace(str_find, ',');
        $('.news_ids').val(product_ids);
        $(this).parent().remove();
    });
});

