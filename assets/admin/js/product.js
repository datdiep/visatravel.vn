String.prototype.replaceAll = function (target, replacement) {
    return this.split(target).join(replacement);
};
var editor2 = CKEDITOR.replace('content');
CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
var reader = new FileReader(),
        i = 0,
        numFiles = 0,
        imageFiles;
function readFile() {
    reader.readAsDataURL(imageFiles[i])
}
reader.onloadend = function (e) {
    var image = "<div class='row'><img src ='" + e.target.result + "'/></div>";
    $(image).appendTo('#image');
    if (i < numFiles) {
        i++;
        readFile();
    }
};
$('#images').change(function () {
    imageFiles = document.getElementById('images').files
    $('#image').html('');
    i = 0;
    numFiles = imageFiles.length;
    readFile();
});
$("#add_attr").click(function () {
    var attr_color = [];
    $('.attr_color').each(function () {
        if ($(this).is(':checked'))
            attr_color.push($(this).val());
    });
    if ($.isEmptyObject(attr_color))
        attr_color.push('');
    var attr_size = [];
    $('.attr_size').each(function () {
        if ($(this).is(':checked'))
            attr_size.push($(this).val());
    });
    if ($.isEmptyObject(attr_size))
        attr_size.push('');
    $.each(attr_color, function (k, v) {
        var c = v.split("|");
        $.each(attr_size, function (k1, v1) {
            var s = v1.split("|");
            var attr = c[0] + '|' + s[0];
            if (!c[1])
                c[1] = '';
            if ($('.table_attr tbody tr[rel="' + attr + '"]').length == 0) {
                var html = '<tr color="' + c[0] + '" rel="' + attr + '">' + '<td>' + s[0] +
                        '<input type="hidden" name="attr_color[]" value="' + c[0] + '"/><input type="hidden" name="attr_size[]" value="' + s[0] + '"/>' +
                        '<input type="hidden" name="attr_type[]" value="' + c[1] + '"/><input type="hidden" name="attr_order[]" value="' + s[1] + '"/></td>' +
                        '<td><input name="price_more[]" class="format_number" value="0"/></td>\
                        <td><a class="del_attr" href="javascript:void(0)">Xoá</a></td></tr>';
                if ($('.table_attr tbody tr[color="' + c[0] + '"]').length == 0) {
                    html = '<tr color="' + c[0] + '"><td rowspan="2" color="' + c[0] + '">' + c[0] + '</td><td colspan="2"><input type="file" name="' + (c[0].replaceAll(' ', '_')) + '"/></td></tr>' + html;
                    $('.table_attr tbody').append(html);
                } else {
                    $('.table_attr tbody tr td[color="' + c[0] + '"]').attr('rowspan', function (i, old) {
                        return +old + 1
                    });
                    $('.table_attr tbody tr:nth-child(' + get_last_color(c[0]) + ')').after(html);
                    $('.format_number').autoNumeric('init', {aPad: false});
                }

            }
        });
    });
    $('.add_attr_success').show();
    setTimeout(function () {
        $('.add_attr_success').fadeOut();
    }, 2000);
    return false;
});
$(".table_attr").on('click', '.del_attr', function () {

    var parent = $(this).parents('tr');
    var id = $(parent).attr('id') | 0;
    if (id) {
        $(".loading_content").show();
        $.ajax({
            url: admin_url + "del_attr",
            data: "id=" + id,
            type: "POST",
            async: false
        });
        $(".loading_content").hide();
    }
    var color = $(parent).attr('color');
    if ($('.table_attr tbody tr[color="' + color + '"]').length < 3) {
        $('.table_attr tbody tr[color="' + color + '"]').remove();
    } else {
        $('.table_attr tbody tr td[color="' + color + '"]').attr('rowspan', function (i, old) {
            return +old - 1
        });
        $(parent).remove();
    }
});
function get_last_color(color) {
    var index = 0;
    $('.table_attr tbody tr').each(function (key) {
        if ($(this).attr('color') == color)
            index = key;
    });
    return index + 1;
}
$("#form").validate({
    ignore: "",
    rules: {
        'title': {required: true},
        'price': {required: true},
        'cat_ids': {required: true}
    },
    messages: {
        'title': 'Vui lòng nhập tên sản phẩm',
        'price': 'Vui lòng nhập giá bán',
        'cat_ids': 'Chọn it nhất 1 danh mục'
    }
});
//$.validator.addMethod("one_required", function() {
//    return $("#form").find(".one_required:checked").length > 0;
//}, 'Please select at least one vehicle.');
//$('select[name="cat_id"]').change(function () {
//    var val = $(this).val();
//    if (val) {
//        var rel = $('option:selected', this).attr('rel').split(',');
//        $('.attr_filter').hide();
//        $.each(rel, function (k, v) {
//            $('div[rel="' + v + '"]').show();
//        });
//    } else {
//        $('.attr_filter').show();
//    }
//});
$('.format_number').autoNumeric('init', {aPad: false});
$('.primary').change(function () {
    var boolAllChecked = 0;
    var ele = $(this);
    $('.primary').each(function (chkboxIndex, chkbox) {
        if (true == chkbox.checked) {
            boolAllChecked++;
        }
        if (boolAllChecked > 1) {
            alert('Loại quà tặng chỉ được chọn 1');
            ele.prop('checked', false);
            return false;
        }
    });
    if (boolAllChecked == 1) {
        $('.primary_cat_id').val(ele.val());
        $('.cat_short_code').val(ele.attr('rel'));
    }
    if (boolAllChecked == 0) {
        $('.primary_cat_id').val('');
        $('.cat_short_code').val('');
    }
//    var val = $(this).val();
//    console.log(val);
//    if ($(this).is(':checked')) {
//        $('.primary').each( function( chkboxIndex, chkbox ) {
//            if( false == chkbox.checked ) {
//                $('.primary').prop( 'checked',false );
//            }
//        });
//    } else {
//        alert('You Un-Checked it');
//    }
//    if (val) {
//        var rel = $('option:selected', this).attr('rel').split(',');
//        $('.attr_filter').hide();
//        $.each(rel, function (k, v) {
//            $('div[rel="' + v + '"]').show();
//        });
//    } else {
//        $('.attr_filter').show();
//    }
});
$(document).ready(function () {
    $('.primary').each(function (chkboxIndex, chkbox) {
        if (true == chkbox.checked) {
            $('.cat_short_code').val($(this).attr('rel'));
        }
    });
})
