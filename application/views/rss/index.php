<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">RSS - Sitemap</h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>           
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="active"><span itemprop="title">Sitemap</span></li>
        </ul>
    </div>
</div>


<section id="content">
    <div class="container">
        <div id="main" class="sitemap">
            <div class="row add-clearfix">
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <div class="column">
                        <h6 class="title">Thông tin & Dịch vụ</h6>
                        <ul class="circle">
                            <li><a href="/rss/tin-tuc-moi-nhat.rss">Tin tức mới nhất</a></li>
                            <?php foreach ($cate_news as $item): ?>
                                <li><a href="/rss/tin-tuc/<?php echo $item['alias']; ?>.rss"><?php echo $item['title']; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-2">
                    <div class="column">
                        <h6 class="title">Tour du lịch</h6>
                        <ul class="circle">
                            <li><a href="/rss/san-pham-moi-nhat.rss">Tour mới nhất</a></li>

                            <?php foreach ($all_cat as $item): ?>
                                <li><a href="/rss/<?php echo $item['alias']; ?>.rss"><?php echo $item['title']; ?></a></li>

                            <?php endforeach; ?>          
                        </ul>
                    </div>
                </div>
            </div>
            <div class="block">
                <h2>RSS / Sitemap</h2>
                <div class="tab-container full-width-style white-bg">
                    
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="first-tab">
                            <h2 class="tab-content-title">Khái niệm RSS</h2>
                            <p>RSS ( viết tắt từ Really Simple Syndication ) là một tiêu chuẩn định dạng tài liệu dựa trên XML nhằm giúp người sử dụng dễ dàng cập nhật và tra cứu thông tin một cách nhanh chóng và thuận tiện nhất bằng cách tóm lược thông tin vào trong một đoạn dữ liệu ngắn gọn, hợp chuẩn.
                            </p>
                            <p>
                                Dữ liệu này được các chương trình đọc tin chuyên biệt ( gọi là News reader) phân tích và hiển thị trên máy tính của người sử dụng. Trên trình đọc tin này, người sử dụng có thể thấy những tin chính mới nhất, tiêu đề, tóm tắt và cả đường link để xem toàn bộ tin.
                            </p>
                            <p><span><span>&nbsp;Dienhoa24gio.net là một trong những shop&nbsp;</span><strong>hoa tươi</strong><span>&nbsp;hàng đầu về cung cách phục vụ & độ tươi đẹp của hoa, tinh thần trách nhiệm cao, yêu nghề và sáng tạo. Dienhoa24gio.net hân hạnh được phục vụ quý khách</span></span></p>
                        </div>
                        
                    </div>
                </div>
            </div>

        </div>
    </div>


</div>

</section>