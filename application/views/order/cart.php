<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Đặt tour - Booking tour</h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/tour" title="Tour du lịch"><span itemprop="title">Tour</span></a></li>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="active"><span itemprop="title">Đặt tour online</span></li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area cart_js">
    <div class="container">
        <div class="row">
            <div class="sidebar col-sms-6 col-sm-4 col-md-3">
                <div class="booking-details travelo-box" id="order_product">
                    <h4>Tour đã chọn</h4>

                    <?php foreach ($products as $item): ?>
                        <article class="tour-detail" id="<?php echo $item['id']; ?>">
                            <figure class="clearfix">
                                <a title="" href="<?php echo '/go/' . $item['alias'] . '-' . $item['id']; ?>" class="middle-block"><img class="middle-item" alt="" src="/assets/upload/product/<?php echo $item['folder'] . '/' . $item['alias'] . '.jpg' ?>"></a>
                                <div class="travel-title">
                                    <h5 class="box-title">
                                        <?php echo $item['title']; ?>
                                        <small class="price" rel="<?php echo $item['price'] * $increment_percent; ?>"><?php echo number_format($item['price'] * $increment_percent); ?> đ 
                                            <input class="ddl_quan" style="width:20px" value="<?php echo $maps_count_product[$item['id']]; ?>"/>    
                                        </small> 
                                    </h5>
                                    <a href="javascript:void(0)" onclick="del_product(<?php echo $item['id']; ?>, this)" title="Xóa tour này?" class="button gray">Delete</a>
                                </div>
                                <td class="price_final"></td>
                            </figure>
                        </article>
                    <?php endforeach; ?>
                    <div style="display: none;" class="form_loading"></div>
                    <dl class="other-details">
                        <dt class="total-price">Tổng tiền:</dt><dd class="total-price-value"><b id="total_money"></b></dd>
                    </dl>
                </div>

                <?php echo $block_right; ?>
            </div>
            <div id="main" class="col-sms-6 col-sm-8 col-md-9">
                <div class="booking-section travelo-box">

                    <form class="booking-form" method="post" action="/final_order">
                        <div class="person-information">
                            <h2>Thông tin liên hệ của bạn</h2>
                            <h5>Chúng tôi sẽ liên hệ vơi bạn qua điện thoại để xác nhận đặt tour trong vòng 24h. Xin vui lòng để lại thông tin chính xác.</h5>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Họ & Tên</label>
                                    <input type="text" name="full_name" class="input-text full-width" value="<?php echo $this->input->cookie('customer_info_fullname_giver'); ?>" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Địa chỉ đầy đủ</label>
                                    <input type="text" name="address" class="input-text full-width" value="<?php echo $this->input->cookie('customer_info_address_giver'); ?>" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6 col-md-5">
                                    <label>Điện thoại của bạn</label>
                                    <input type="text" name="phone_number" class="input-text full-width" value="<?php echo $this->input->cookie('customer_info_phone_giver'); ?>" placeholder="" />
                                </div>
                                <div class="col-sm-6 col-md-5">
                                    <label>Email của bạn</label>
                                    <input type="text" name="email_address" class="input-text full-width" value="<?php echo $this->input->cookie('customer_info_email_giver'); ?>" placeholder="" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="form-group">
                                    <label>Nội dung</label>
                                    <textarea name="note" rows="8" class="input-text full-width" placeholder="Vui lòng nhập nội dung bạn muốn liên hệ tại đây"></textarea>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Bằng cách click vào đây - Bạn đồng ý với tất cả <a href="#"><span class="skin-color"> Điều khoản của Achau.net</span></a>.
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6 col-md-5">
                                <button type="submit" class="full-width btn-large">XÁC NHẬN ĐẶT TOUR</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>