<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Đặt tour thành công</h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="/tour" title="Tour du lịch"><span itemprop="title">Tour</span></a></li>
            <li itemscope itemtype="http://data-vocabulary.org/Breadcrumb" class="active"><span itemprop="title">Đặt tour online</span></li>
        </ul>
    </div>
</div>
<section id="content" class="gray-area">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9">
                <div class="booking-information travelo-box">
                    <h2>Xác định đặt tour thành công</h2>
                    <hr />
                    <div class="booking-confirmation clearfix">
                        <i class="soap-icon-recommend icon circle"></i>
                        <div class="message">
                            <h4 class="main-message">Xin cám ơn, bạn đã đặt tour thành công.</h4>
                            <p>Một Email xác nhận đã gửi vào hộp thư của bạn, vui lòng xác nhận thông tin đặt tour du lịch.</p>
                        </div>
                        <a href="#" class="button print-button btn-small uppercase">In chi tiết</a>
                    </div>
                    <hr />
                    <h2>Thông tin người đặt tour du lịch</h2>
                    <dl class="term-description">
                        <dt>Mã đặt tour:</dt><dd>AC-Tour-<?php echo $order['id']; ?></dd>
                        <dt>Họ tên:</dt><dd><?php echo $order['fullname_giver']; ?></dd>
                        <dt>Điện thoại:</dt><dd><?php echo $order['phone_giver']; ?></dd>
                        <dt>Địa chỉ:</dt><dd><?php echo $order['address_giver']; ?></dd>
                        <dt>E-mail:</dt><dd><?php echo $order['email_giver']; ?></dd>
                    </dl>
                    <hr />
                    <h2>Thanh toán</h2>
                    <p>Thông tin đặt tour của anh/chị đã được chuyển đến bộ phận xử lý đơn hàng. Cty Á Châu rất hân hạnh được phục vụ anh/chị.</p>
                    
                    <hr />
                    <h2>Xem lại thông tin đặt tour</h2>
                    <p>Bằng cách click vào link bên dưới - Anh/chị sẽ xem lại thông tin tour của mình. Nếu cần bất cứ sự trợ giúp nào, đừng ngân ngại gọi vào số 08.22600007 hoặc 08.22600009 để chúng tôi hỗ trợ.</p>
                    <br />
                    <a href="#" class="red-color underline view-link">http://achau.net/booking-details/?=f4acb19f-9542-4a5c-b8ee</a>
                </div>
            </div>

            <div class="sidebar col-sm-4 col-md-3">
                <div class="travelo-box">
                    <h5 class="box-title">Tìm kiếm thông tin</h5>
                    <div class="with-icon full-width">
                        <input type="text" class="input-text full-width" placeholder="Nhập quốc gia bạn cần visa">
                        <button class="icon green-bg white-color"><i class="soap-icon-search"></i></button>
                    </div>
                </div>
                <div class="travelo-box book-with-us-box">
                    <h4>Tại sao dùng dịch vụ VISA của chúng tôi?</h4>
                    <ul>
                        <li>
                            <i class="soap-icon-hotel-1 circle"></i>
                            <h5 class="title"><a href="#" class="skin-color">Hơn 90% Visa</a></h5>
                            <p>Làm dịch vụ tại AChau.net đã thành công.</p>
                        </li>
                        <li>
                            <i class="soap-icon-savings circle"></i>
                            <h5 class="title"><a href="#" class="skin-color">Dịch vụ trọn gói</a></h5>
                            <p>Tất cả thủ tục đã có AChau lo đầy đủ bạn chỉ cần ký.</p>
                        </li>
                        <li>
                            <i class="soap-icon-support circle"></i>
                            <h5 class="title"><a href="#" class="skin-color">Cựu lãnh sự Mỹ</a></h5>
                            <p>Hơn 15 năm kinh nghiệm đứng ra làm dịch vụ</p>
                        </li>
                    </ul>
                </div>


                <div class="travelo-box contact-box">
                    <h4 class="box-title">Bạn cần thêm thông tin?</h4>
                    <p>Chúng tôi rất vinh dự được hỗ trợ bạn tìm hiểu thêm thông tin về tour này. Đừng ngần ngại liên hệ chúng tôi</p>
                    <address class="contact-details">
                        <span class="contact-phone"><i class="soap-icon-phone"></i> 08.22600009 Á CHÂU </span>
                        <br />
                        <a href="#" class="contact-email">hotro@achau.net</a>
                    </address>
                </div>
            </div>
        </div>
    </div>
</section>