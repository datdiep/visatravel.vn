<br/><br/>
<b>Thông tin người gửi</b><br/>
Họ tên  : <?php echo $order['fullname_giver']; ?><br/>
Địa chỉ   : <?php echo $order['address_giver']; ?><br/>
Email  : <?php echo $order['email_giver']; ?><br/>
Số điện thoại : <?php echo $order['phone_giver']; ?><br/><br/>

<b>Thông tin người nhận</b><br/>
Họ tên : <?php echo $order['fullname_recipient']; ?><br/>
Địa chỉ : <?php echo $order['address_recipient']; ?> - <?php echo $order['district']; ?> - <?php echo $order['city']; ?><br/>
Số điện thoại : <?php echo $order['phone_recipient']; ?><br/><br/>
Yêu cầu : <?php echo $order['note']; ?><br/>
Lời nhắn, lời chúc : <?php echo $order['message']; ?><br/>
Thời gian giao hàng : <?php echo $order['hours_ship'] . ' ' . date('d-m-Y', strtotime($order['date_ship'])); ?><br/>

<br/>
<br/>
<br/>
<table style="border-collapse:collapse;width: 50%" id="order_product" >
    <thead>
        <tr style="background: #ccc;color: #fff;border: 1px solid #ccc;"><td style="text-align: center;" colspan="4">Chi tiết đơn hàng</td></tr>
        <tr style="padding: 6px;border: 1px solid #ccc;text-align: left;">                       
            <th style="padding: 6px;border: 1px solid #ccc;text-align: left;">Sản phẩm</th>                    
            <th style="padding: 6px;border: 1px solid #ccc;text-align: left;">Đơn giá</th>                    
            <th style="padding: 6px;border: 1px solid #ccc;text-align: left;">Số lượng</th>
            <th style="padding: 6px;border: 1px solid #ccc;text-align: left;">Thành tiền</th>
        </tr>
    </thead>
    <tbody>  
        <?php
        $url = base_url();
        $total = 0;
        ?>
        <?php foreach ($products as $item): ?>                 
            <tr style="padding: 6px;border: 1px solid #ccc;text-align: left;">
                <td style="padding: 6px;border: 1px solid #ccc;text-align: left;">                                       
                    <?php echo $item['product_name']; ?>
                </td>                        
                <td style="padding: 6px;border: 1px solid #ccc;text-align: left;"><?php echo number_format($item['price']); ?></td>                        
                <td style="padding: 6px;border: 1px solid #ccc;text-align: left;">
                    <?php echo $item['count']; ?>                       
                </td>
                <td style="padding: 6px;border: 1px solid #ccc;text-align: left;">
                    <?php
                    $price = $item['count'] * $item['price'];
                    $total+=$price;
                    echo number_format($price);
                    ?>
                </td>

            </tr>
        <?php endforeach; ?>
        <tr style="padding: 6px;border: 1px solid #ccc;text-align: left;">
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;" colspan="3">Tổng mua hàng</td>
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;"><b id="total_money"><?php echo number_format($total); ?></b></td>
        </tr>
        <tr style="padding: 6px;border: 1px solid #ccc;text-align: left;">
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;" colspan="3">Tiền ship</td>
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;"><b id="total_money"><?php echo number_format($order['ship_price']); ?></b></td>
        </tr>
        <tr style="padding: 6px;border: 1px solid #ccc;text-align: left;background: #ccc;color: #088;font-weight: bold;">
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;" colspan="3">Tổng tiền</td>
            <td style="padding: 6px;border: 1px solid #ccc;text-align: left;"><b id="total_money"><?php echo number_format($total + $order['ship_price']); ?></b></td>
        </tr>
    </tbody>
</table>    

<br/>   

