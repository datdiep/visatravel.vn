<div class="hero-area height-80 bg-img background-overlay" style="background-image: url(/assets/user/img/blog-img/bg4.jpg);"></div>
<!-- ********** Hero Area End ********** -->

<div class="regular-page-wrap section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-8">
                <div class="page-content">
                    <h1 class="error_404">ERROR 404</h1>
                    <a href="<?php echo base_url() ?>"> <i class="fa fa-arrow-left"></i> Quay Lại Trang Chủ </a>
                </div>
            </div>
        </div>
    </div>
</div>