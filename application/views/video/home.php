<!-- tab chon loc -->
<div id="product_hot" class="list_product_parent">
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-11 col-xs-15">
                <div class="panel-body-hot">
                    <a href="/" title="Shop hoa tuoi dep - Dien Hoa 24 gio"><?php echo $page_title; ?></a>
                </div>
            </div>

        </div>
    </div>
    <div id="list_product_hot" class="list_product">
        <div class='row main-product'>      
            <div class="col-md-3">
                <ul class="video_menu nopadding">
                    <li class="title">Danh mục Video clip</li>
                    <?php foreach ($video_categories as $item): ?>
                        <li class="default <?php echo $item['id'] == $cat_id_video ? 'active' : ''; ?>"><a href="/danh-muc-video/<?php echo $item['alias']; ?>"><?php echo $item['title']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div class="col-md-12">
                <?php foreach ($videos as $item): ?>
                    <a href="/video/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?> ">
                        <div class="col-md-5">
                            <div class="text-center list-product"> 
                                <span class="productImg">
                                    <img src="/assets/upload/video/<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>" class="img-responsive "/>
                                </span>
                                <span class="productName"><?php echo $item['title']; ?> </span>                          
                                <span style="background:#5f9c09;padding:5px 8px;color:#fff">Xem chi tiết</span>                           

                            </div>
                        </div>
                    </a>    
                <?php endforeach; ?>
            </div>
        </div>
        <div class="text-center">
            <ul class="pagination">
                <?php echo @$links; ?>
            </ul>
        </div>
    </div>
</div>
</div>

