<!-- tab chon loc -->
<div id="product_hot" class="list_product_parent">
    <div class="panel panel-default">
        <div class="row">
            <div class="col-md-11 col-xs-15">
                <div class="panel-body-hot">
                    <a href="/" title="Shop hoa tuoi dep - Dien Hoa 24 gio"><!-- <span class="cat_icon cat_hot"></span> --> <?php echo $page_title; ?></a>
                </div>
            </div>

        </div>
    </div>
    <div id="list_product_hot" class="list_product">
        <div class='row main-product'>      
            <div class="col-md-11" style="padding-left: 0;position: relative;">
                <div class="col-md-4">
                    <ul class="video_menu nopadding">
                        <li class="title">Danh mục Video clip</li>
                        <?php foreach ($video_categories as $item): ?>
                            <li  class="<?php echo $item['id'] == $cat_id_video ? 'active' : ''; ?>"><a href="/danh-muc-video/<?php echo $item['alias']; ?>"><?php echo $item['title']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="col-md-11" style="padding-left: 0;position: relative;">
                    <div  id="show_content"  >
                        <div id="play">

                        </div>
                        <br/>
                        <div>
                            <div class="fb-like hidden-xs" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                            <div style="float: right;">
                                <a href="#" class="btn btn-danger lightout">
                                    <i class="icon icon-map-marker"></i>
                                    <span>Tắt đèn</span>
                                </a>
                                <a href="#" class="btn btn-warning ZoomPlayer">
                                    <i class="icon icon-zoom-in"></i>
                                    <span>Phóng to</span>
                                </a>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="carousel-title">
                    <h4><span class="decor left"></span>Có thể bạn quan tâm<span class="decor right"></span></h4>
                </div>
                <div id="list_product_rand" class="list_product">
                    <div class='row main-product' style="left: 0;" >       
                        <?php foreach ($videos as $item): ?>
                            <a href="/video/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?> ">
                                <div class="col-md-3">
                                    <div class="text-center list-product"> 
                                        <span class="productImg">
                                            <img src="/assets/upload/video/<?php echo $item['image']; ?>" alt="<?php echo $item['title']; ?>" class="img-responsive "/>
                                        </span>
                                        <span class="productName"><?php echo $item['title']; ?> </span>                          
                                        <span style="background:#5f9c09;padding:5px 8px;color:#fff">Xem chi tiết</span>                           

                                    </div>
                                </div>
                            </a>    
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div id="product_in_video" class="col-md-3">
                <div class="row main-product">
                    <?php foreach ($results as $item): ?>
                        <a href="/san-pham/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>">
                            <div style="100%">
                                <div class="text-center list-product"> 
                                    <span class="productImg">
                                        <img src="/assets/upload/product/<?php echo $item['folder'] . '/274_default.jpg'; ?>" alt="<?php echo $item['title']; ?>" class="img-responsive"/>
                                    </span>
                                    <span class="productName"><?php echo $item['title']; ?></span>
                                    <span class="price">
                                        <?php echo number_format($item['price']) . ' ' . $item['unit'] . ''; ?>
                                    </span>
                                    <span style="background:#5f9c09;padding:5px 8px;color:#fff">Xem chi tiết</span>
                                </div>
                            </div>

                        </a>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>        
    </div>
</div>

<script src="/assets/user/js/md5H+A5oEeOHuCIACusDuQ.js"></script>

<script>
    jwplayer.key = "TMWMZGpO6KbF1A7W8beGFcKOfVOdP0zjGYQgnW3Lhso=";
    jwplayer('play').setup({
        title: "<?php echo $video['title']; ?>",
        width: '600',
        height: '390',
        playlist: [{
                sources: [
                    {
                        file: "<?php echo $video['url']; ?>",
                        label: "360p SD"
                    }
                ]
            }],
        startparam: "start",
    });
    $('.ZoomPlayer').click(function () {
        if ($('span', this).text() == 'Phóng to') {
            $('span', this).text('Thu nhỏ');
            $('#play').animate({width: 818, height: 510}, 0, function () {
                $('#product_in_video').hide();
            });

        } else {
            $('span', this).text('Phóng to');
            $('#play').animate({width: 600, height: 390}, 0, function () {
                setTimeout(function () {
                    $('#product_in_video').fadeIn(200);
                }, 500);

            });
        }
        return false;
    });
    $('#light-overlay').click(function () {
        $('.lightout').click();
    })
    $('.lightout').click(function () {
        if ($('#light-overlay').css('display') == 'block') {
            $('body').css({'overflow': 'visible'});
            $('#light-overlay').hide();
            $('#show_content').css("position", "");
            $('span', this).text('Tắt đèn');
        } else {
            var w = $(document).width() - $('#play_wrapper').attr('width');
            $('#light-overlay').show();
            $('#show_content').css({"position": "absolute", 'z-index': 1000000});
            $('span', this).text('Mở đèn');
        }
        return false;

    });

</script>
<style>
    #play{
        transition: all 1s;
        -moz-transition: all 1s;
        -ms-transition: all 1s;
        -webkit-transition: all 1s;
        -o-transition: all 1s;
    }
    .btn{display: inline-block;padding: 4px 12px;margin-bottom: 0;font-size: 14px;line-height: 20px;color: #333333;text-align: center;text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);vertical-align: middle;cursor: pointer;background-color: #f5f5f5;background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);background-repeat: repeat-x;border: 1px solid #cccccc;border-color: #e6e6e6 #e6e6e6 #bfbfbf;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);border-bottom-color: #b3b3b3;-webkit-border-radius: 4px;-moz-border-radius: 4px;border-radius: 4px;filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);-webkit-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);-moz-box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.2), 0 1px 2px rgba(0, 0, 0, 0.05);}
    .control{margin: 20px 0px 15px 55px;margin-right: 15px;}
    .control a{display: block;float: right;color: #ffffff;margin-right: 5px;}
    .icon{display: inline-block;margin-top: 1px;line-height: 14px;vertical-align: text-top;background-image: url("../images/glyphicons-halflings.png");background-position: 14px 14px;background-repeat: no-repeat}

    .btn-inverse {background-color: #363636;background-image: -moz-linear-gradient(top, #444444, #222222);background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#444444), to(#222222));background-image: -webkit-linear-gradient(top, #444444, #222222);background-image: -o-linear-gradient(top, #444444, #222222);background-image: linear-gradient(to bottom, #444444, #222222);background-repeat: repeat-x;border-color: #222222 #222222 #000000;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff444444', endColorstr='#ff222222', GradientType=0);filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);}
    .btn-info {background-color: #49afcd;background-image: -moz-linear-gradient(top, #5bc0de, #2f96b4);background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#5bc0de), to(#2f96b4));background-image: -webkit-linear-gradient(top, #5bc0de, #2f96b4);background-image: -o-linear-gradient(top, #5bc0de, #2f96b4);background-image: linear-gradient(to bottom, #5bc0de, #2f96b4);background-repeat: repeat-x;border-color: #2f96b4 #2f96b4 #1f6377;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff5bc0de', endColorstr='#ff2f96b4', GradientType=0);filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);}
    .btn-danger {color: #ffffff;text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);background-color: #da4f49;background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);background-repeat: repeat-x;border-color: #bd362f #bd362f #802420;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);}
    .btn-warning {color: #ffffff;text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);background-color: #faa732;background-image: -moz-linear-gradient(top, #fbb450, #f89406);background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));background-image: -webkit-linear-gradient(top, #fbb450, #f89406);background-image: -o-linear-gradient(top, #fbb450, #f89406);background-image: linear-gradient(to bottom, #fbb450, #f89406);background-repeat: repeat-x;border-color: #f89406 #f89406 #ad6704;border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0)filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);}
    .icon-thumbs-down {background-position: -120px -144px;}
    .icon-thumbs-up {background-position: -96px -144px;}
    .icon-map-marker {background-position: -24px -72px;}
    .icon-zoom-in {background-position: -336px 0;}
</style>