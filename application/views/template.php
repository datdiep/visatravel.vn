<!doctype html>
<html lang="en">
    <head>
        <title> <?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?> </title>
        <meta name="description" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>" />
        <meta name="keywords" content="<?php echo isset($web['meta_keyword']) ? $web['meta_keyword'] : $seo['meta_keyword']; ?>" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta http-equiv='cache-control' content='no-cache'>
        <meta http-equiv='expires' content='0'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="robots" content="index,follow" />
        <meta name="author" content="<?php echo $seo['website']; ?>" />
        <link rel="canonical" href="<?php echo $current_url; ?>" />
        <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
        <?php
        if (@$amp_header) {
            echo $amp_header;
        };
        ?>
        <meta name="DC.title" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>" />
        <meta name="DC.description" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>" />
        <meta name="DC.subject" content="<?php echo isset($web['meta_keyword']) ? $web['meta_keyword'] : $seo['meta_keyword']; ?>" />
        <meta name="DC.language" scheme="UTF-8" content="en" />
        <link rel="image_src" href="<?php echo @$seo['img']; ?>" />
        <meta name="generator" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>" />
        <meta itemprop="name" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>">
        <meta itemprop="description" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>">
        <meta itemprop="image" content="<?php echo @$seo['img']; ?>">
        <meta property="og:title" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>" />
        <meta property="og:locale" content="en" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="<?php echo $current_url; ?>" />
        <meta property="og:image" content="<?php echo @$seo['img']; ?>" />
        <meta property="og:description" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>" />
        <meta property="og:site_name" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>" />
        <meta property="fb:app_id" content="1075181152563097"/>
        <meta property="fb:admins" content="1702519701"/>
        <meta name="distribution" content="Global" />
        <meta name="author" itemprop="author" content="<?php echo $seo['website']; ?>" />
        <meta name="abstract" content="<?php echo isset($web['meta_keyword']) ? $web['meta_keyword'] : $seo['meta_keyword']; ?>" />
        <meta name="classification" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>" />
        <meta name="twitter:title" content="<?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?>">
        <meta name="twitter:description" content="<?php echo isset($web['meta_description']) ? $web['meta_description'] : $seo['meta_description']; ?>">
        <meta name="twitter:image" content="<?php echo @$seo['img']; ?>">
        <link rel="SHORTCUT ICON" href="/favicon.png"/>


        <link rel="stylesheet" type="text/css" href="/assets/user/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/user/css/animate.css" />
        <link rel="stylesheet" type="text/css" href="/assets/user/css/magnific-popup.css" />
        <link rel="stylesheet" type="text/css" href="/assets/user/css/font-awesome.min.css" />
        <link rel="stylesheet" type="text/css" href="/assets/user/css/themify-icons.css" />
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Oswald">
        <link rel="stylesheet" type="text/css" href="/assets/user/css/owl.carousel.css" />      
        <link rel="stylesheet" type="text/css" href="/assets/user/css/style.css" />
        <script src="/assets/user/js/jquery/jquery-2.2.4.min.js"></script>
    </head>
    <body>
        <!--        Preloader Start -->
        <div id="preloader">
            <div class="preload-content">
                <div id="world-load"></div>
            </div>
        </div>
        <!--        Preloader End -->

        <!-- ***** Header Area Start ***** -->
        <header class="header-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <nav class="navbar navbar-expand-lg">
                            <!-- Logo -->
                            <a class="navbar-brand" href="./"><img class="logo_home" src="/assets/user/img/core-img/logo.png" alt="Logo"></a>
                            <!-- Navbar Toggler -->
                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#worldNav" aria-controls="worldNav" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                            <!-- Navbar -->
                            <div class="collapse navbar-collapse" id="worldNav">
                                <ul class="navbar-nav ml-auto">
                                    <li class="nav-item <?php echo $pre == 'home' ? "active" : "" ?>">
                                        <a class="nav-link" href="<?php echo base_url() ?>">Home <span class="sr-only">(current)</span></a>
                                    </li>
                                    <!--                                    <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                                                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                                                                <a class="dropdown-item" href="index.html">Home</a>
                                                                                <a class="dropdown-item" href="single-blog.html">Single Blog</a>
                                                                                <a class="dropdown-item" href="regular-page.html">Regular Page</a>
                                                                                <a class="dropdown-item" href="contact.html">Contact</a>
                                                                            </div>
                                                                        </li>-->
                                    <?php foreach ($list_cat_news as $value) : ?>
                                        <li class="nav-item <?php echo $pre == $value['alias'] ? "active" : "" ?>">
                                            <a class="nav-link " href="/<?php echo $value['alias'] ?>"><?php echo $value['title'] ?></a>
                                        </li>
                                    <?php endforeach; ?>
                                    <li class="nav-item <?php echo $pre == 'contact' ? "active" : "" ?>">
                                        <a class="nav-link" href="/contact">Liên Hệ</a>
                                    </li>

                                </ul>
                                <!-- Search Form  -->
                                <div id="search-wrapper">
                                    <form action="/tim-kiem">
                                        <input required type="text" name="q"  id="search" placeholder="Bạn cần tìm">
                                        <div id="close-icon"></div>
                                        <input class="d-none" type="submit" value="">
                                    </form>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </header>
        <!-- ***** Header Area End ***** -->

        <?php echo $content_block; ?> 

        <!-- footer -->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="footer-single-widget">
                            <a href="#"><img src="/assets/user/img/core-img/logo.png" alt=""></a>
                            <div class="copywrite-text mt-30">
                                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<!--                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> -->
                                    Địa chỉ: 236 Nguyễn Thái Bình, Phường 12, Quận Tân Bình | Hồ Chí Minh <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Visatravel.vn</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-single-widget">
                            <ul class="footer-menu d-flex justify-content-between">

                                <?php foreach ($list_cat_news as $value) : ?>
                                    <li>
                                        <a href="/<?php echo $value['alias'] ?>"><?php echo $value['title'] ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-single-widget">
                            <h5>Hỗ trợ tư vấn</h5>
                            <form action="#" method="post">
                                <input required type="text" name="phone" id="phone" placeholder="Số điện thoại của bạn">
                                <br> <br>
                                <button onclick="get_call()" type="button"><i class="fa fa-arrow-right"></i></button>
                                <p class="alert_phone_footer"></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
    <style>

        .footer-area .footer-single-widget form input[type="text"] {
            width: 100%;
            height: 35px;
            background-color: transparent;
            border: none;
            border-bottom: 1px solid;
            border-color: #959595;
            color: #959595;
            font-size: 14px;
            padding: 0 5px;
            font-family: "Ubuntu", sans-serif;
        </style>
        <!-- Google Code dành cho Thẻ tiếp thị lại -->

<!--    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1008001964;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
    </script>
    <noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1008001964/?guid=ON&amp;script=0"/>
    </div>
    </noscript>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-1297306-21', 'auto');
        ga('send', 'pageview');
        var lastScrollTop = 0;
        $(window).scroll(function () {
            if ($(this).scrollTop() > 300) {
                $('.m_button').css({'bottom': '0px'});
            } else {
                $('.m_button').css({'bottom': '-65px'});
            }
            //lastScrollTop = st;

        });
    </script>-->

        <script type="text/javascript">
            //        /* <![CDATA[ */
            //        goog_snippet_vars = function () {
            //            var w = window;
            //            w.google_conversion_id = 1008001964;
            //            w.google_conversion_label = "R4l2COq_nHEQrMfT4AM";
            //            w.google_remarketing_only = false;
            //        }
            //        // DO NOT CHANGE THE CODE BELOW.
            //        goog_report_conversion = function (url) {
            //            goog_snippet_vars();
            //            window.google_conversion_format = "3";
            //            var opt = new Object();
            //            opt.onload_callback = function () {
            //                if (typeof (url) != 'undefined') {
            //                    window.location = url;
            //                }
            //            }
            //            var conv_handler = window['google_trackConversion'];
            //            if (typeof (conv_handler) == 'function') {
            //                conv_handler(opt);
            //            }
            //        }
            //        /* ]]> */


<?php
for ($i = 1; $i <= $all_question; $i++) {
    ?>
                load_phone(<?php echo $i ?>);
<?php } ?>
            var flag_get_call = 0;
            function get_call()
            {
                phone = $("#phone").val();
                if (phone != "" && flag_get_call != 1) {
                    flag_get_call = 1;
                    $.post("/contact", {phone: phone, request: 1}, function (results) {
                        $(".alert_phone_footer").html("<i class='fa fa-check'></i> " + results + " ");
                    });
                } else if (phone == "")
                    $(".alert_phone_footer").html("<i class='fa fa-check'></i> Xin nhập số điện thoại");
                else
                    $(".alert_phone_footer").html("<i class='fa fa-warning'></i> Visatravel đã nhận được thông tin");
            }





        </script>
    <!--    <script type="text/javascript"
                src="//www.googleadservices.com/pagead/conversion_async.js">
        </script>-->
        <!-- jQuery (Necessary for All JavaScript Plugins) -->

        <!-- Popper js -->
        <script src="/assets/user/js/popper.min.js"></script>
        <!-- Bootstrap js -->
        <script src="/assets/user/js/bootstrap.min.js"></script>
        <!-- Plugins js -->
        <script src="/assets/user/js/plugins.js"></script>
        <!-- Active js -->
        <script src="/assets/user/js/active.js"></script>

        <link rel="stylesheet" type="text/css" href="/assets/user/css/style_tri.css" />
    </html>