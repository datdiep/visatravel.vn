<div class="travelo-box">
    <h4 class="box-title">Xem thông tin theo quốc gia</h4>
    <ul class="check-circle box">
        <?php foreach ($country_list as $item): ?>
            <li><h5 class="entry-title"><a href="/country/<?php echo $item['alias']; ?>" title="<?php echo $item['seo_title']; ?>"><?php echo $item['name']; ?></a></h5></li>
                <?php endforeach; ?>
    </ul>
</div>