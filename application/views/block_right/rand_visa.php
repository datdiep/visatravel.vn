<div class="travelo-box">
    <h4 class="box-title">Các bài viết xin visa hữu ích</h4>
    <div class="image-box style14">
        <?php $i = 0; ?>
        <?php foreach ($other_news as $item): ?>
            <article class="box">
                <h5 class="box-title"><span class="soap-icon-arrow-right"></span>  <a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?> - <?php echo $item['id']; ?>"><?php echo $item['title']; ?></a></h5>
            </article>
        <?php endforeach; ?>
    </div>
</div>