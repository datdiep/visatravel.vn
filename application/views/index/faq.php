
<div class="container">     
    <div class="breadcrumbs">
        <ul class="breadcrumb clearfix" itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="/" class="active"><span itemprop="name">Trang chủ</span></a></li>
            <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" title="<?php echo $detail['title'] ?>" href="/info/hoi-dap-visa" class="active"><span itemprop="name">Câu hỏi thường gặp</span></a></li>

        </ul>
    </div>
    <div class="row clearfix">
        <div class="col-lg-9 col-sm-8 article-content">
            <div class="article">
                <h1>Câu hỏi thường gặp về dịch vụ làm VISA :</h1>

                <blockquote class="style1 border-color-blue quote-color-yellow">
                    <span class="triangle"></span>- Gọi ngay để được tư vấn 24/7 : <a href="tel:0979.555.090" rel="nofollow" title="Hotline hỗ trợ làm visa"> 0979.555.090</a>
                </blockquote>
                <div class="requirement-detail">
                    <div class="center apply_button">
                        <a class="btn btn-lg btn-danger callme" title="Gọi hỗ trợ" href="tel:0969686101"> Call: <i class="fa fa-phone-square" aria-hidden="true"></i> 0969.686.101  </a>
                    </div>
                </div>


                <blockquote class="style1 border-color-blue quote-color-yellow">

                    <?php
                    foreach ($question as $item) {
                        echo "<h2>- ". $item['title'] . "</h2>";
                        echo "<p>" . $item['content'] . "</p>";
                    }
                    ?>
                </blockquote>


            </div>

            <!--            <div class="comments">
                             FB comments
                            <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter/r/FdM1l_dpErI.js?version=42#channel=f15df2d800febd&amp;origin=https%3A%2F%2Fvisatop.vn" style="border: none;"></iframe></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div><iframe name="f7d68a8e18c12c" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" src="https://www.facebook.com/connect/ping?client_id=1075181152563097&amp;domain=visatop.vn&amp;origin=1&amp;redirect_uri=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FFdM1l_dpErI.js%3Fversion%3D42%23cb%3Df72ed50382435%26domain%3Dvisatop.vn%26origin%3Dhttps%253A%252F%252Fvisatop.vn%252Ff15df2d800febd%26relation%3Dparent&amp;response_type=token%2Csigned_request%2Ccode&amp;sdk=joey" style="display: none;"></iframe></div></div></div>
                            <script>(function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id))
                                        return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1075181152563097";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));</script>
                            <div class="fb-comments fb_iframe_widget fb_iframe_widget_fluid" data-href="https://visatop.vn/from-vietnam" data-width="100%" data-num-posts="10" fb-xfbml-state="rendered"><span style="height: 187px;"><iframe id="f39aa0b8ddee62" name="ff977f60c09fc" scrolling="no" title="Facebook Social Plugin" class="fb_ltr" src="https://www.facebook.com/plugins/comments.php?api_key=1075181152563097&amp;channel_url=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FFdM1l_dpErI.js%3Fversion%3D42%23cb%3Df30decbac2ec3bc%26domain%3Dvisatop.vn%26origin%3Dhttps%253A%252F%252Fvisatop.vn%252Ff15df2d800febd%26relation%3Dparent.parent&amp;href=https%3A%2F%2Fvisatop.vn%2Ffrom-vietnam&amp;locale=vi_VN&amp;numposts=10&amp;sdk=joey&amp;version=v2.6&amp;width=100%25" style="border: none; overflow: hidden; height: 187px; width: 100%;"></iframe></span></div>
            
                        </div>-->
            <script>
                $('#datetimepicker').datetimepicker({format: 'MMMM D, YYYY HH:mm:ss'});
                function like_fb(id, _this) {
                    var text = $(_this).text();
                    var method = 'unlike';
                    $(_this).html('Thích');
                    if (text == 'Thích') {
                        method = 'like';
                        $(_this).html('Bỏ thích');
                    }
                    $.post('/ajax/like_fb', {id: id, method: method}, function (results) {
                        console.log(results);
                        if (results > 0) {
                            $('.like_fb_' + id).css('display', 'inline-block');
                            $('.like_fb_' + id + ' .num_like').html(results);
                        } else {
                            $('.like_fb_' + id).css('display', 'none');
                            $('.like_fb_' + id + ' .num_like').html(results);
                        }

                    });
                }
            </script>
        </div>
        <div class="col-lg-3 col-sm-4 hidden-xs">
            <div class="support-online-widget right-corner">
                <div class="title">HỖ TRỢ ONLINE</div>
                <div class="content">
                    <p><i>Đừng ngần ngại liên hệ chúng tôi 24/7</i></p>
                    <table cellpadding="2" width="100%">
                        <tbody><tr>
                                <td>Email</td><td>: </td>
                                <td class="email"><a href="mailto:chudutravel@gmail.vn">chudutravel@gmail.vn</a></td>
                            </tr>
                            <tr>
                                <td>Hotline VN1</td><td>: </td>
                                <td class="phone"><a href="tel:0969686101">0979.555.090</a></td>
                            </tr>
                            <tr>
                                <td>Hotline VN2</td><td>: </td>
                                <td class="phone"><a href="tel:+19018480937">093.3000.300</a></td>
                            </tr>
                        </tbody></table>
                </div>
            </div>

            <div class="payment-online">
                <a>
                    <h1>THANH TOÁN ONLINE</h1>
                    <p>100% Bảo mật &amp; an toàn tuyệt đối</p>
                </a>
            </div>				
            <div class="confidence-widget">
                <div class="banner"></div>
                <div class="content">
                    <div class="title">
                        TẠI SAO CHỌN CHÚNG TÔI?
                    </div>
                    <ul>
                        <li>Cam kết và trách nhiệm</li>
                        <li>Thực hiện hồ sơ nhanh</li>
                        <li><b>Hơn 15 năm kinh nghiệm</b></li>
                        <li>Chính sách hoàn phí rõ ràng</li>

                    </ul>
                </div>
            </div>				
            <div class="service-widget">
                <div class="banner"></div>
                <div class="content">
                    <div class="title">
                        DỊCH VỤ KÈM THÊM
                    </div>
                    <ul>
                        <li><a title="" class="clearfix" href="#"><i class="passport"></i> Gia hạn visa Việt Nam</a></li>
                        <li><a title="" class="clearfix" href="#"><i class="airport-concierge"></i> Đưa đón tại sân bay</a></li>
                        <li><a title="" class="clearfix" href="#"><i class="car"></i> Cho thuê xe du lịch</a></li>
                        <li><a title="" class="clearfix" href="#"><i class="hotel"></i> Đặt phòng khách sạn</a></li>
                        <li><a title="" class="clearfix" href="#"><i class="travel"></i> Cho thuê hướng dẫn viên</a></li>
                        <li><a title="" class="clearfix" href="#"><i class="flight"></i> Đặt vé máy bay giá rẻ</a></li>
                    </ul>
                </div>
            </div>				
        </div>
    </div>
</div>