
<!-- ********** Hero Area Start ********** -->
<div class="hero-area height-400 bg-img background-overlay" style="background-image: url(/assets/upload/category_news/<?php echo $category_news['image'] ?>);">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div itemscope itemtype="http://schema.org/BreadcrumbList" class="single-blog-title text-center">
                    <!-- Catagory -->
                    <div itemprop="itemListElement" class="post-cta"><a  itemscope itemtype="http://schema.org/ListItem" href="./"><span itemprop="name">Trang Chủ</span></a></div>
                    <h3 itemtype="http://schema.org/Thing" itemprop="name"  title="<?php echo $category_news['title'] ?>"><?php echo $category_news['title'] ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->
<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area Start ============= -->
            <div class="col-12 col-lg-8">
                <div class="post-content-area mb-100">
                    <!-- Catagory Area -->
                    <div class="world-catagory-area">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="title"><a itemscope="" itemtype="http://schema.org/Thing" itemprop="item" href="/" class="active"><span itemprop="url">Trang chủ / </span></a><span itemprop="title" ><?php echo $category_news['title'] ?></span></li>
                            <li class="nav-item">
                                <a class="nav-link active" id="tab1" data-toggle="tab" href="#world-tab-1" role="tab" aria-controls="world-tab-1" aria-selected="true">All</a>
                            </li>

                        </ul>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                <?php foreach ($list_news as $item_news) : ?>
                                    <!-- Single Blog Post -->
                                    <div class="single-blog-post post-style-4 d-flex align-items-center">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail">
                                            <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content">
                                            <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                <h5><?php echo $item_news['title'] ?></h5>
                                            </a>
                                            <p><?php echo $item_news['description'] ?></p>
                                            <!-- Post Meta -->
                                            <div class="post-meta">
                                                <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>


                                <div class="pagination">
                                    <?php echo $links ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========== Sidebar Area ========== -->
            <?php include 'right_content.php' ?>
        </div>

        <!-- Load More btn -->

    </div>
</div>
