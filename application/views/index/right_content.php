<!-- ========== Sidebar Area ========== -->
<div class="col-12 col-md-8 col-lg-4">
    <div class="post-sidebar-area wow fadeInUpBig" data-wow-delay="0.2s">
        <!-- Widget Area -->
        <div class="sidebar-widget-area">
            <h5 class="title">CÔNG TY VISA TRAVEL</h5>
            <div class="widget-content">
                <ul class="fnav-links" id="ul_right_content">
                    <li><i class="fa fa-address-book"></i> 236 Nguyễn Thái Bình, phường 12, quận Tân Bình, HCM</li>
                    <li><i class="fa fa-phone"></i> Hotline 1 :<b class="c_red"> 0979.555.090</b> </li>
                    <li><i class="fa fa-phone"></i> Hotline 2 : <b class="c_red">0911.90.11.00</b> </li>
                    <li><i class="fa fa-bank"></i> Số tài khoản ngân hàng: <b class="c_red">11 3333 8888</b>
                        Ngân hàng ACB chi nhánh Phú Lâm, TP.HCM</li>
                    <li>MST: 0301011001</li>
                </ul>
            </div>
        </div>
        <!-- Widget Area -->
        <div class="sidebar-widget-area hidden-xs">
            <h5 class="title">  DỊCH VỤ KÈM THÊM</h5>
            <div class="widget-content">
                <ul id="ul_right_content">
                    <li><img src="/assets/user/img/bg-2.jpg"></li>
                    <li><i class="fa fa-book"></i> Gia hạn visa Việt Nam</li>
                    <li><i class="fa fa-umbrella"></i> Đưa đón tại sân bay</li>
                    <li><i class="fa fa-car"></i> Cho thuê xe du lịch</li>
                    <li><i class="fa fa-book"></i> Đặt phòng khách sạn</li>
                    <li><i class="fa fa-vcard"></i> Cho thuê hướng dẫn viên</li>
                    <li><i class="fa fa-trello"></i> Đặt vé máy bay giá rẻ</li>
                </ul>
            </div>
        </div>
        <div class="sidebar-widget-area">
            <h5 class="title">Liên hệ</h5>
            <div class="widget-content">
                <div class="social-area d-flex justify-content-between">
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-twitter"></i></a>
                    <a href="#"><i class="fa fa-pinterest"></i></a>
                    <a href="#"><i class="fa fa-vimeo"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                    <a href="#"><i class="fa fa-google"></i></a>
                </div>
            </div>
        </div>
        <!--         Widget Area 
                <div class="sidebar-widget-area">
                    <h5 class="title">Today’s Pick</h5>
                    <div class="widget-content">
                         Single Blog Post 
                        <div class="single-blog-post todays-pick">
                             Post Thumbnail 
                            <div class="post-thumbnail">
                                <img src="/assets/user/img/blog-img/b22.jpg" alt="">
                            </div>
                             Post Content 
                            <div class="post-content px-0 pb-0">
                                <a href="#" class="headline">
                                    <h5>How Did van Gogh’s Turbulent Mind Depict One of the Most Complex Concepts in Physics?</h5>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>-->
    </div>
</div>
<style>

    #ul_right_content li {
        padding-bottom:  20px
    }
</style>