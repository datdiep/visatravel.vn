<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo $page['title']; ?></h2>

        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li class="active"><span itemprop="title">Bài viết</span></li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <div class="row">
            <div id="main" class="col-sm-8 col-md-9 detail-article">
                <div class="image-style style1 box">
                    <h1><?php echo $page['title']; ?></h1>
                    <?php if ($this->data['admin']): ?>
                        <p> <a href="/admincp/page/edit/<?php echo $page['id']; ?>" target="blank"><b>[ Edit ]</b></a></p>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                </div>

                <blockquote class="style1 border-color-blue quote-color-yellow">
                    <span class="triangle"></span>Dịch vụ làm <?php echo $page['title']; ?> chuyên nghiệp - <a href="tel:<?php echo $getphone['click']; ?>" rel="nofollow" title="Hotline hỗ trợ làm visa"> <?php echo $getphone['show']; ?></a>
                </blockquote>

                <div class="article-content">
                    <img class="img-responsive padding1020" src="/assets/upload/page/<?php echo $page['image'] ? $page['image'] : 'no_img.jpg'; ?>" alt="<?php echo $page['title']; ?>" />

                    <p> <?php echo $page['content']; ?> </p>
                    <p>Lượt tham khảo: <b><?php echo $page['view']; ?></b></p>
                    <p>Date: <?php echo $page['publish_date']; ?></p>
                    <p>
                        <!-- facebook like -->
                    <div id="fb-root"></div>
                    <script>(function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id))
                                return;
                            js = d.createElement(s);
                            js.id = id;
                            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1075181152563097";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                    <div class="fb-like" style="padding-top: 8px" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    <!-- end -->  
                    <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                    <g:plusone></g:plusone>
                    </p>
                </div>


                <div class="row facebook-comments">
                    <!-- FB comments-->
                    <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-num-posts="10"></div>
                </div>

                <div class="col-md-12" style="border-bottom: 1px #ddd solid; padding: 10px " itemscope itemtype="http://schema.org/Recipe">

                    <span itemprop="name" class="col-md-9 pull-right"><?php echo @$page_title; ?></span>
                    <img itemprop="image" class="col-md-3 pull-left" src="https://achau.net/assets/user/images/logo.png" alt="<?php echo @$page_title; ?>" title="<?php echo @$page_title; ?>">

                    <div class="col-md-8 pull-left">

                        <?php
                        if ($page['tags']) {
                            $tags = explode(',', $page['tags']);
                            foreach ($tags as $k => $v) {
                                echo '<a class="button btn-small gray tags" href="/tim-kiem/' . urlencode($v) . '">' . $v . '</a> ';
                            }
                        }
                        ?>
                    </div>

                </div>
            </div>

            <div class="sidebar col-sm-4 col-md-3">
                <?php echo $block_right; ?>
            </div>
        </div>

    </div>
</section>
<div id="getquote" class="travelo-login-box travelo-box">
    <div class="login-social" id="list_ajax">
        <img src="/assets/user/gift/taiwan.png" align="left" class="img-responsive " />
        Xin chào ! Hôm nay là 1 ngày đẹp trời, và tôi muốn tặng bạn 1 tài liệu <b>10 BÍ KÍP XIN VISA 100% ĐÂU</b>
        <br><br>
        Tất tần tật những bí kíp trước và sau khi xin visa Đài Loan, Bí kíp này sẽ hướng dẫn bạn những gì cần chuẩn bị trước và sau khi đi
        <br><br>
        Đây là 1 món quà tâm huyết của tôi dành tặng riêng cho bạn. Nếu Muốn - Bạn có thể để lại thông tin để tôi có thể gửi tặng món quà này cho bạn 
        <br><br>
    </div>
    <div class="seperator"><label><b class="c-red">NHẬN QUÀ TẶNG</b></label></div>
    <br>
    <div id="received_gift">
        <form>
            <div class="form-group">
                <input type="text" name="full_name" class="input-text full-width input-border" placeholder="Nhập Tên của bạn">
            </div>
            <div class="form-group">
                <input type="text" name="email" class="input-text full-width input-border" placeholder="Nhập email của bạn">
            </div>
        </form>
        <div class="seperator"></div>
        <button type="submit" class="full-width icon-check animated bounce save_gift" data-animation-type="bounce" data-animation-duration="1" style="animation-duration: 1s; visibility: visible;">NHẬN QUÀ TẶNG</button>
    </div>
</div> 
<script>
//                        $(document).ready(function () {
//                            setTimeout(function () {
//                                $("#getquote").fadeIn();
//                            }, 1000);
//                        });

                        $('body').on('click', '.save_gift', function () {

                            var data = {
                                full_name: $('input[name="full_name"]').val(),
                                email: $('input[name="email"]').val(),
                            }
                            var full_name = $('input[name="full_name"]').val();
                            var email = $('input[name="email"]').val();
                            if (full_name == '') {
                                alert('Tên không được bỏ trống');
                                $('input[name="full_name"]').focus();
                                return false;
                            }
                            if (email == '') {
                                alert('email không được bỏ trống');
                                $('input[name="email"]').focus();
                                return false;
                            }
                            $.post('/save_gift', {data: data}, function (results) {
                                console.log(results);
                                $('#received_gift').html(results);
                            })
                        })

</script>