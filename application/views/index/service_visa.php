
<!-- start Main Wrapper -->
<div class="main-wrapper scrollspy-container">

    <div class="page-title detail-page-title" style="background-image:url('/assets/upload/category_service/<?php echo $service_cate['icon'] ?>');">
        <div class="container">

            <div class="flex-row">

                <div class="flex-column flex-md-8 flex-sm-12">

                    <h1 class="hero-title"><?php echo $service_cate['title'] . ' : ' . $service_visa['title'] ?></h1>
                    <p class="line18"><?php echo $service_cate['description'] ?></p>
                </div>

                <div class="flex-column flex-md-4 flex-align-bottom flex-sm-12 mt-20-sm">
                    <div class="text-right text-left-sm">
                        <a href="#section-5" class="anchor btn btn-primary">Lượt xem : <?php echo $service_cate['view'] ?></a>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <div class="breadcrumb-wrapper bg-light-2">

        <div class="container">

            <ol class="breadcrumb-list">
                <li><a href="<?php echo base_url() ?>">Home</a></li>
                <li><a href="<?php echo base_url() . 'dich-vu-visa/' . $service_cate['alias'] . '.html' ?>">dich-vu-visa</a></li>

                <li><span><?php echo $service_cate['title'] . " / " . $service_visa['title'] ?></span></li>
            </ol>

        </div>

    </div>

    <div class="content-wrapper">

        <div class="container">

            <div class="row">

                <div class="col-md-9" role="main">

                    <div class="detail-content-wrapper">

                        <div  class="detail-content">
                            <div class="section-title text-left">
                                <h4><?php echo $service_visa['title'] ?></h4>
                            </div>
                            <img src="/assets/upload/service_detail/<?php echo $service_visa['image'] ?>" alt="Map" class="map-route" />
                            <p><?php echo $service_visa['description'] ?></p>
                            <p><?php echo $service_visa['content'] ?></p>
                        </div>


                        <div id="section-<?php echo ++$flag ?>" class="detail-content">

                            <div class="section-title text-left">
                                <h4> &amp; Câu hỏi thường gặp về <?php echo $service_visa['title'] ?></h4>
                            </div>

                            <p>Những điều hữu ích bạn cần biêt :</p>
                            <!--
                                                        <div class="row">
                                                            
                                                                <div class="col-sm-6 col-md-6 mb-15">
                            
                                                                    <div class="read-more-div" data-collapsed-height="107">
                            
                                                                        <div class="read-more-div-inner">
                            
                                                                            <h5 class="heading mt-0"><?php echo $value['title'] ?></h5>
                            
                                                                            <p><?php echo $value['description'] ?></p>
                            
                                                                            <p><?php echo $value['content'] ?></p>
                            
                                                                        </div>
                            
                                                                    </div>
                            
                                                                </div>
                                                          
                            
                                                        </div>-->

                            <h5 class="heading mt-0">FAQ</h5>

                            <div class="faq-alt-2-wrapper">

                                <div class="panel-group bootstarp-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php foreach ($service_question as $value) {
                                        ?>
                                        <div class="panel">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h6 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $value['id'] ?>" aria-expanded="true" aria-controls="collapse<?php echo $value['id'] ?>"><?php echo $value['title'] ?></a>
                                                </h6>
                                            </div>
                                            <div id="collapse<?php echo $value['id'] ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $value['id'] ?>">
                                                <div class="panel-body">
                                                    <div class="faq-thread">
                                                        <p><?php echo $value['content'] ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
                                    <?php } ?>

                                </div>


                                <!--                                <div class="text-center pt-10">
                                                                    <a href="#" class="btn btn-primary">Show ALl FAQs</a>
                                                                </div>-->

                            </div>

                        </div>

                        <div id="section-<?php echo ++$flag ?>" class="detail-content">


                            <div class="text-left">
                                <h4>Bạn cần tư vấn <?php echo $service_visa['title'] ?> ?</h4>
                            </div>



                            <form method="post" class="" enctype="multipart/form-data">

                                <div class="row">

                                    <div class="col-sm-6 col-md-4">

                                        <div class="form-group">
                                            <label>Họ Tên: </label>
                                            <input type="text" id="comment_name" class="form-control" />
                                        </div>

                                    </div>

                                    <div class="clear"></div>

                                    <div class="col-sm-6 col-md-4">

                                        <div class="form-group">
                                            <label>Địa chỉ email: </label>
                                            <input type="email" id="comment_email" class="form-control" />
                                        </div>

                                    </div>
                                    <div class="col-sm-6 col-md-4">

                                        <div class="form-group">
                                            <label>Số điện thoại: </label>
                                            <input type="phone" required id="comment_phone" class="form-control" />
                                            <p id="check_phone"></p>
                                        </div>

                                    </div>

                                    <div class="clear"></div>

                                    <div class="col-sm-12 col-md-8">

                                        <div class="form-group">
                                            <label>Nội Dung: </label>
                                            <textarea id="comment_content" class="form-control form-control-sm" rows="5"></textarea>
                                        </div>
                                    </div>

                                    <div class="clear"></div>

                                    <div class="col-sm-12 col-md-8 mt-10">
                                        <a type="submit" name="submit" onclick="add_coment('submit')" class="btn btn-primary">Submit</a>
                                    </div>
                                </div>
                            </form>


                        </div>



                    </div>

                </div>

                <div  class="col-sm-3 scrollspy-sidebar sidebar-detail" role="complementary">

                    <nav class="scrollspy-sidebar sidebar-detail" data-spy="affix" data-offset-top="197">
                        <ul  class="scrollspy-sidenav">
                            <li>
                                <ul class="nav bs-docs-sidenav">
                                    <li > <h5 style="color: black"> &nbsp;&nbsp;&nbsp;&nbsp;- Dịch vụ <?php echo $service_cate['title'] ?> gồm : </h5></li>
                                    <?php
                                    foreach ($service_detail as $key => $value) {
                                        ?>
                                        <li  <?php echo $value['alias'] == $service_visa['alias'] ? "class='active'" : "" ?> ><a href="<?php echo $value['alias'] != $service_visa['alias'] ? base_url() . 'dich-vu-visa/' . $service_cate['alias'] . '/' . $value['alias'] . '.html' : "#" ?>" class="anchor" style="font-size: 13px"><i class="fa fa-check"></i> <?php echo $value['title'] ?></a></li>
                                    <?php } ?>
                                    <li><a href="#section-1" class="anchor" style="font-size: 13px"><i class="fa fa-check"></i>  Câu hỏi thường gặp về <?php echo $service_visa['title'] ?></a></li>
                                    <li><a href="#section-2" class="anchor" style="font-size: 13px"><i class="fa fa-check"></i> Bạn cần tư vấn <?php echo $service_visa['title'] ?></a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>

        </div>

    </div>

</div>
<!-- end Main Wrapper -->

<script>

    /**
     *  Sidebar Sticky
     */

    !function ($) {

        $(function () {

            var $window = $(window)
            var $body = $(document.body)

            var navHeight = $('.navbar').outerHeight(true) + 50

            $body.scrollspy({
                target: '.scrollspy-sidebar',
                offset: navHeight
            })

            $window.on('load', function () {
                $body.scrollspy('refresh')
            })

            $('.scrollspy-container [href=#]').click(function (e) {
                e.preventDefault()
            })

            // back to top
            setTimeout(function () {
                var $sideBar = $('.scrollspy-sidebar')

                $sideBar.affix({
                    offset: {
                        top: function () {
                            var offsetTop = $sideBar.offset().top
                            var sideBarMargin = parseInt($sideBar.children(0).css('margin-top'), 10)
                            var navOuterHeight = $('.scrollspy-nav').height()

                            return (this.top = offsetTop - navOuterHeight - sideBarMargin)
                        }
                        , bottom: function () {
                            return (this.bottom = $('.scrollspy-footer').outerHeight(true))
                        }
                    }
                })
            }, 100)

        })

    }(window.jQuery)
    function add_coment(submit) {
        var id = <?php echo $service_cate['id'] ?>;
        var name = $('#comment_name').val();
        var email = $('#comment_email').val();
        var content = $('#comment_content').val();
        var phone = $('#comment_phone').val();
        var check = submit;
        if (phone != "") {
            $.post('<?php echo base_url() ?>comment', {id: id, check: check, name: name, phone: phone, email: email, content: content}, function (results) {
                alert(results);
                location.reload();
            });
        } else
            $("#check_phone").html('<p style="color:red">(Xin nhập số điện thoại để chúng tôi dễ liên hệ)</p>');
    }

</script>