<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title">Liên hệ công ty Visa Á Châu</h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>           
            <li class="active"><span itemprop="title">Liên hệ</span></li>
        </ul>
    </div>
</div>

<section id="content">
    <div class="container">
        <h1 class="margin-t-30" style="margin-top:30px; margin-bottom: 30px"><center>Dịch vụ làm hộ chiếu lấy nhanh toàn quốc</center></h1>

        <div class="row image-box style1 add-clearfix">

            <div class="col-sm-4 one-third ">
                <div class="pricing-table box white">
                    <div class="header clearfix">
                        <i class="soap-icon-user circle green-color"></i>
                        <h4 class="box-title passport"><span>LẤY NHANH HỘ CHIẾU MỚI CẤP</span></h4>

                    </div>
                    <p class="description">Bạn không cần phải chờ đợi thời gian dài mới có hộ chiếu. Chúng tôi sẽ hỗ trợ lấy nhanh trong ngày cho bạn.</p>

                    <ul class="check-square features">
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>

                    </ul>
                    <p><a href="#" class="button btn-small full-width green" target="">GỌI NGAY: 0988.011.249</a></div>
            </div> 

            <div class="col-sm-4 one-third ">
                <div class="pricing-table box white">
                    <div class="header clearfix">
                        <i class="soap-icon-friends circle yellow-color"></i>
                        <h4 class="box-title passport"><span>ĐỔI HỘ CHIẾU LẤY NHANH</span></h4>
                    </div>
                    <p class="description">Bạn không cần phải chờ đợi thời gian dài mới có hộ chiếu. Chúng tôi sẽ hỗ trợ lấy nhanh trong ngày cho bạn.</p>

                    <ul class="check-square features">
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                    </ul>
                    <p><a href="#" class="button btn-small full-width yellow" target="">GỌI NGAY: 0988.011.249</a></div>
            </div> 

            <div class="col-sm-4 one-third ">
                <div class="pricing-table box white">
                    <div class="header clearfix">
                        <i class="soap-icon-suitcase circle blue-color"></i>
                        <h4 class="box-title passport"><span>LẤY NHANH HỘ CHIẾU BỊ MẤT</span></h4>
                    </div>
                    <p class="description">Bạn không cần phải chờ đợi thời gian dài mới có hộ chiếu. Chúng tôi sẽ hỗ trợ lấy nhanh trong ngày cho bạn.</p>
                    <ul class="check-square features">
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>
                        <li>1 ngày: 3.600.000 đ</li>

                    </ul>
                    <p><a href="#" class="button btn-small full-width sky-blue1" target="">GỌI NGAY: 0988.011.249</a></div>
            </div> 
        </div>

    </div>
</section>



