<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo $cat['title']; ?></h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li><a itemprop="url" href="/tour" title="Tour du lịch"><span itemprop="title">Tour</span></a></li>
            <?php if (@$cat_parent['title']): ?>
                <li class="active"><span itemprop="title"><?php echo @$cat_parent['title']; ?></span></li>
            <?php else: ?>
                <li class="active"><span itemprop="title"><?php echo @$cat['title']; ?></span></li>
            <?php endif; ?>
        </ul>
    </div>
</div>

<section id="content">

    <div class="container">
        <div id="main">
            <div class="sort-by-section clearfix box">
                <h4 class="sort-by-title block-sm">Sắp xếp theo:</h4>
                <ul class="sort-bar clearfix block-sm">
                    <li class="sort-by-rating <?php echo (!@$_GET['sort'] ? 'active' : ''); ?>"><a class="sort-by-container" href="?"><span>Mới nhất</span></a></li>
                    <li class="sort-by-price-down <?php echo (@$_GET['sort'] == 'price-down' ? 'active' : ''); ?>"><a class="sort-by-container" href="?sort=price-down"><span>Giá giảm dần</span></a></li>
                    <li class="sort-by-price-up <?php echo (@$_GET['sort'] == 'price-up' ? 'active' : ''); ?>"><a class="sort-by-container" href="?sort=price-up"><span>Giá tăng dần</span></a></li>

                </ul>

                <ul class="swap-tiles clearfix block-sm">

                    <li class="swap-grid active">
                        <a href="#"><i class="soap-icon-grid"></i></a>
                    </li>

                </ul>
            </div>

            <div class="row add-clearfix image-box style1 tour-locations">

                <?php foreach ($results as $item): ?>
                    <div class="col-sm-6 col-md-4 col-lg-3 nopadding">
                        <article class="box">
                            <figure>
                                <a href="/go/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>" class="hover-effect">
                                    <img src="/assets/upload/product/<?php echo $item['folder'] . '/' . $item['alias'] . '.jpg'; ?>" alt="<?php echo $item['title']; ?>" class="img-responsive"/>
                                </a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><a href="/go/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a><small>Khởi hành: HCM</small></h4>
                                <span class="price"><small>Chỉ từ</small><?php echo number_format($item['price']) . ' ' . $item['unit'] . ''; ?> </span>
                                
                            </div>

                        </article>
                    </div>
                <?php endforeach; ?>


            </div>
            <div class="text-center">
                <ul class="pagination">
                    <?php echo @$links; ?>
                </ul>
            </div>
        </div>
    </div>
</section>