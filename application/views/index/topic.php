<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo $cat['title']; ?></h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li><a itemprop="url" href="#" title="Chủ đề du lịch"><span itemprop="title">Chủ đề</span></a></li>
            <li class="active"><span itemprop="title"><?php echo $topic['title']; ?></span></li>

        </ul>
    </div>
</div>

<section id="content">

    <div class="container">
        <div id="main">

            <div class="travel-story-container box">
                <div class="travel-story-content">
                    <div class="description">
                        <h1 class="skin-color"><a href="/topic/<?php echo $topic['alias']; ?>" title="<?php echo $topic['title']; ?>"><?php echo $topic['title']; ?></a></h1>
                        <p><?php echo @$topic['meta_description']; ?></p>
                    </div>
                </div>
            </div>

            <div class="row add-clearfix image-box style1 tour-locations">

                <?php foreach ($products as $item): ?>
                    <div class="col-sm-6 col-md-4 col-lg-3">
                        <article class="box">
                            <figure>
                                <a href="/go/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>" class="hover-effect">
                                    <img src="/assets/upload/product/<?php echo $item['folder'] . '/' . $item['alias'] . '.jpg'; ?>" alt="<?php echo $item['title']; ?>" class="img-responsive"/>
                                </a>
                            </figure>
                            <div class="details">
                                <h4 class="box-title"><?php echo $item['title']; ?><small>Khởi hành: HCM</small></h4>
                                <span class="price"><small>Chỉ từ</small><?php echo number_format($item['price']) . ' ' . $item['unit'] . ''; ?> </span>
                                <div class="text-center">
                                    <div class="time">
                                        
                                    </div>
                                </div>
                                <a href="/go/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>" class="button btn-small full-width">ĐẶT NGAY</a>
                            </div>
                        </article>
                    </div>
                <?php endforeach; ?>


            </div>
            <div class="text-center">
                <ul class="pagination">
                    <?php echo @$links; ?>
                </ul>
            </div>
        </div>
    </div>
</section>