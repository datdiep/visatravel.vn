<!DOCTYPE html>
<html amp>
    <head>
        <?php $canonical = str_replace('/amp', '', base_url(uri_string())); ?>
        <link rel="canonical" href="<?php echo $canonical; ?>" />
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <meta name="apple-mobile-web-app-title" content="Achau.net" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <title><?php echo isset($web['seo_title']) ? $web['seo_title'] : $seo['seo_title']; ?></title>
        <meta name="author" content="Visa Du lịch Á Châu - Achau.net"/>
        <meta name="description" content="<?php echo isset($web['meta_description']) ? str_replace('"', '', $web['meta_description']) : str_replace('"', '', $seo['meta_description']); ?>"/>
        <meta name="thumbnail" content="<?php echo base_url(); ?>/assets/upload/page/<?php echo $news['image'] ? $news['image'] : 'no_img.jpg'; ?>"/>
        <style amp-custom>
            *{margin:0;padding:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}:before,:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}body{color:#000;font:400 16px arial;text-align:left;position:relative}a:focus{outline:1px}a{color:#000;text-decoration:none;outline:1px}a:hover{text-decoration:none;color:#000}img{border:0;font-size:0;line-height:0;max-width:100%}ul,li{list-style-type:none}input,textarea{font-family:arial;font-size:11px;border:none;background:none}.clear{clear:both;font-size:0;line-height:0}#page{position:relative;z-index:1;width:100%;float:left}#header,#container{padding:0 10px}#wrapper_container{position:relative;z-index:0;width:100%;float:left}.left{float:left}.right{float:right}html{-webkit-text-size-adjust:none}.navButton,.navClose{position:absolute;top:0;left:0;z-index:3;background-color:transparent;border:none;outline:none;color:#333;font-size:20px;line-height:1.5em;text-align:left;display:block;padding:5px 0 0 10px;cursor:pointer}.navClose{left:240px;z-index:-1;visibility:hidden}.navButton:focus{opacity:0}.navFrame{background-color:#333;border:0;padding:0;margin:0;position:absolute;top:0;left:0;width:240px;height:100%;box-sizing:border-box;z-index:2;text-align:left;display:block;transform:translateX(-245px);-webkit-transform:translateX(-245px);transition:transform 500ms cubic-bezier(0,1,0,1)}.navButton:focus ~ .navFrame{transform:translateX(0px);-webkit-transform:translateX(0px)}.navButton:focus ~ .navFrame .sitenav{transform:translateX(0);-webkit-transform:translateX(0)}.navFrame:active,.navButton:focus ~ .navClose{z-index:3;visibility:visible}.navFrame:active ~ #navscrim,.navButton:focus ~ #navscrim,.navFrame:active ~ #navscrim,.navButton:focus ~ #navscrim{opacity:.75;pointer-events:auto}.block_scoll_menu{width:100%;float:left}#wrapper_header{height:auto;position:relative;width:100%;background:url(<?php echo base_url(); ?>assets/user/images/bg-top.png) #fff repeat-x;border-bottom:2px solid #fdb716}.breakumb_timer{padding:5px 0;position:relative;z-index:9}.breakumb_timer .timer_header ul li{height:34px;float:left}.breakumb_timer .timer_header ul li a{height:34px;float:left;display:block;font:700 13px/34px arial;padding:0 7px 0 5px;background:#eceeed;white-space:nowrap}.breakumb_timer .timer_header ul li.start a{border-left:2px solid #098eb8;color:#098eb8}.breakumb_timer .timer_header ul li .arrow_breakumb{display:block;float:left;height:34px;width:20px;background:url(<?php echo base_url(); ?>assets/user/images/icon_total_01.png) no-repeat 0 -325px}.breakumb_timer .timer_header ul li.have_cap2 a{background:#eceeed}.breakumb_timer .timer_header ul li.have_cap2 .arrow_breakumb{background-position:0 -257px}.breakumb_timer .timer_header ul li.cap2 a{background:#c2c4c5}.breakumb_timer .timer_header ul li.cap2 .arrow_breakumb{background-position:0 -291px}.timer_header{width:85%;float:left}.block_timer{padding:10px 10px 0 0}.list_item_panel{width:100%;float:left}.list_item_panel li{float:left;width:100%}.list_item_panel li img{float:left}.list_item_panel li a{display:block;width:100%;float:left;color:#fff;font:700 16px/40px arial;background:#555;border-bottom:1px solid #484848}.list_item_panel li.active a{background:#333 url(<?php echo base_url(); ?>assets/user/images/icon_total_01.png) no-repeat 213px -549px;border-left:5px solid #9f224e}.list_item_panel li.end a{border:none}.coppy_right_left{background:#333 url(<?php echo base_url(); ?>assets/user/images/icon_arrow.gif) no-repeat 15px 0;color:#666;float:left;font:400 14px arial;padding:20px 5px 50px 15px;width:100%}.coppy_right_left p{padding-bottom:.5em}.coppy_right_left .txt_lienhe,.coppy_right_left .txt_lienhe a{color:#fff}.width_common{width:100%;float:left}.icon_responsive{width:20px;height:20px;display:inline-block;line-height:0;font-size:0;background:url(<?php echo base_url(); ?>assets/user/images/icon_resonesive.gif) no-repeat}.ico_respone_02{background-position:-20px 0}.c_red{color:red}.logo_mobile{float:left;margin:8px 0 0 30px}.img_logo{float:left;margin:0 0 7px;width:180px}.short_intro,.relative_news{font-weight:700}#box_details_news .fck_detail a.txt_thamgia{color:#666;text-decoration:underline;font-size:12px}#box_details_news .fck_detail{width:100%;float:left;overflow:hidden;font:400 14px arial;padding:0}#box_details_news .fck_detail table caption{caption-side:bottom;font:400 14px arial;color:#333;background:#f5f5f5;padding:10px;text-align:left}#box_details_news .fck_detail table img{font-size:0}#box_details_news .fck_detail table{max-width:100%;margin:0 auto 10px}#box_details_news .fck_detail ul,#box_details_news .fck_detail ul li{list-style-type:disc;margin-left:7px}#box_details_news .fck_detail ol{list-style:decimal;padding:0 0 0 30px;margin:0}#box_details_news .fck_detail ol li{list-style-type:decimal;margin-left:7px}#box_details_news .fck_detail .Normal,#box_details_news .fck_detail p{margin:0 0 1em}#box_details_news .fck_detail p{font-size:18px;line-height:24px}#box_details_news .short_intro{font:700 18px/24px arial;width:100%;float:left;padding-bottom:10px;color:#323232}#box_details_news .short_intro a,#box_details_news .fck_detail a{color:#004f8b}.sitenav{width:240px;position:absolute;top:45px;right:0;transform:translateX(-150px);-webkit-transform:translateX(-150px);transition:transform 500ms cubic-bezier(0,1,0,1)}#navscrim{position:fixed;top:0;left:0;width:100vw;height:100vh;z-index:1;background-color:#999;opacity:0;transition:opacity 250ms cubic-bezier(0,.67,0,.67);pointer-events:none}.social{float:right;padding-top:13px}.m-call{font-size:23px;width:155px;font-weight:700;padding:6px 0 0;background-color:#f60;text-align:center;text-shadow:-1px 1px 1px rgba(0,0,0,.2);position:fixed;right:5px;bottom:5px;box-shadow:0 2px 4px 2px rgba(95,82,125,.45);-webkit-tap-highlight-color:rgba(0,0,0,0);-webkit-tap-highlight-color:transparent;border-radius:6px;opacity:.9;line-height:18px}.m-call span{font-size:12px;display:block;color:#fff;font-weight:400}.m-call a{color:#fff;-webkit-tap-highlight-color:rgba(0,0,0,0);-webkit-tap-highlight-color:transparent}.space_bottom_20{margin-bottom:20px}.icon_menu_phone{background:url(<?php echo base_url(); ?>assets/user/images/icon_menu.png) no-repeat;display:inline-block;float:left;height:24px;margin:9px 10px 0;width:24px}#wrapper_footer{float:left;margin:20px 0 0;width:100%}#footer{float:left;width:100%}.list_menu_small{background:none repeat scroll 0 0 #f4f4f4;float:left;width:100%}.list_menu_small li{border-bottom:1px solid #d7d7d7;float:left;margin:0 3%;width:44%}.list_menu_small li a{display:block;float:left;font:400 16px/44px arial;height:44px;width:100%}.ft-bot{float:left;padding:10px 0 0;width:100%}.coppy_right_small{color:#333;display:block;font:400 12px arial;padding:10px 0 0;float:left;width:100%}.coppy_right{border-top:1px solid #d7d7d7;float:left;margin:10px 0 0;padding:20px 0;width:100%}.width_50{float:left;margin:0 0 10px 10px;width:45%}.width_50 img{margin:0 10px 0 0}.coppy_right p{padding:0 0 0 10px}.coppy_right p span{font-weight:700}.title_news{padding:0 0 10px}.title_news a{font:700 18px arial}.box_sub_hot_news .title_news a,.list_news .title_news a{font-weight:400}.txt_num_comment{color:#9f224e;font:400 14px arial;white-space:nowrap}.news_lead{font:400 16px arial}.box_hot_news .list_news_dot_3x3_300 li a{color:#666;font:700 14px/21px arial}.box_hot_news .list_news_dot_3x3_300{border:medium none;margin:0;padding:0}.title_box_category{background:#eee url(<?php echo base_url(); ?>assets/user/images/bg_1x28.gif) repeat-x scroll left top;height:44px;margin-bottom:10px}.txt_main_category{color:#9f224e;display:inline;float:left;font:700 20px/44px arial;margin:0 0 0 10px}.txt_main_category span,.txt_main_category a{background:url(<?php echo base_url(); ?>assets/user/images/bg_active_categoryname.gif) repeat-x scroll left top;color:#ff751a;display:block;float:left;font:700 20px/44px arial;height:44px;white-space:nowrap}.only_title .title_news .box_category{margin-bottom:20px}.block_tag{margin:0 0 20px}.block_tag .txt_tag{background:#f1f1f1 url(<?php echo base_url(); ?>assets/user/images/bg_icon_tag.gif) no-repeat left top;padding:0 10px 0 20px;height:20px;color:#939393;float:left;font:400 11px/20px arial;margin:0 5px 0 0}.block_tag .tag_item{display:inline-block;height:20px;font:400 11px/20px arial;color:#656565;padding:0 10px;white-space:nowrap;background:#f1f1f1;margin:0 5px 5px 0;float:left}.box_tinkhac_detail .title_box_category{background:url(<?php echo base_url(); ?>assets/user/images/bg_2x2_xam.gif) repeat-x left 0}.box_tinkhac_detail .list_news .thumb_160{margin:0 10px 0 0;width:20%;position:relative;float:left}.box_tinkhac_detail .list_news li{border-bottom:1px dotted #e2e2e3;display:inline-block;margin-bottom:10px;padding-bottom:10px;width:100%;vertical-align:top}.box_tinkhac_detail .list_news li:last-child{border:none;padding:0;margin:0}.thumb{float:left;margin:0 10px 0 0;position:relative}@media screen and (min-width: 481px) and (max-width: 1600px){.box_tinkhac_detail .list_news li{width:47%;box-sizing:border-box;margin-right:2%}}
            .c_yellow{color: yellow;}
            <?php echo $news['data']['style']; ?>
        </style>
        <script type="application/ld+json">
            {
            "@context": "http://schema.org",
            "@type": "Article",
            "mainEntityOfPage": "<?php echo $canonical; ?>",
            "description": "<?php echo isset($page_descript) ? '' . str_replace('"', '', $page_descript) . $page_title : ''; ?>",
            "headline": "<?php echo $news['title'] ?>",
            "image": {
            "@type": "ImageObject",
            "url": "<?php echo base_url(); ?>/assets/upload/page/<?php echo $news['image'] ? $news['image'] : 'no_img.jpg'; ?>",
            "width": 696,
            "height": 454      },
            "datePublished": "<?php echo $news['timeadd'] ?>",
            "dateModified": "<?php echo $news['timeadd'] ?>0",
            "publisher": {
            "@type": "Organization",
            "name": "Achau.net",
            "logo": {
            "@type": "ImageObject",
            "url": "<?php echo base_url(); ?>assets/user/images/logo.png",
            "width": 196,
            "height": 56        }
            },
            "author": {
            "@type": "Person",
            "name": "Visa Achau Team"
            }
            }
        </script>

        <script async custom-element="amp-dynamic-css-classes" src="https://cdn.ampproject.org/v0/amp-dynamic-css-classes-0.1.js"></script>
        <script async custom-element="amp-audio" src="https://cdn.ampproject.org/v0/amp-audio-0.1.js"></script>
        <script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
        <script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>
        <script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
        <script async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style>
        <noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
    </head>

    <body class="comic-amp-font-loading">
    <amp-analytics type="googleanalytics" id="analytics1">
        <script type="application/json">
            {
            "vars": {
            "account": "UA-1297306-19"
            },
            "triggers": {
            "trackPageview": {
            "on": "visible",
            "request": "pageview"
            }
            }
            }
        </script>
    </amp-analytics>


    <div id="page" >
        <a class="navButton" tabindex="0">&#9776;</a>
        <span class="navClose" tabindex="0">&#9776;</span>
        <div class="navFrame">
            <div class="block_scoll_menu">
                <ul class="list_item_panel">
                    <?php foreach ($all_cat_visa as $item): ?>
                        <li><a href="/visa/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo str_replace('"', '', $item['title']); ?>"><span class="icon_menu_phone ico_menu_0<?php echo $item['id']; ?>">&nbsp;</span> <?php echo $item['title']; ?></a></li>
                    <?php endforeach; ?>

                    <?php foreach ($all_cat_news as $item): ?>
                        <li><a href="/guide/<?php echo $item['alias'] . '-' . $item['id']; ?>" title="<?php echo $item['title']; ?>"><span class="icon_menu_phone ico_menu_08">&nbsp;</span> <?php echo $item['title']; ?></a></li>
                    <?php endforeach; ?>

                    <li><a href="/contact"><span class="icon_menu_phone ico_menu_21">&nbsp;</span>Liên hệ Á Châu</a></li>
                    <li class="end"><a href="/contact"><span class="icon_menu_phone ico_menu_22">&nbsp;</span>Liên hệ chúng tôi</a></li>
                </ul>
                <div class="coppy_right_left width_common">
                    <div>
                        <p>© Copyright 2014 - Achau.net</p>
                        <p>® Dịch vụ Visa quốc tế uy tín.</p>
                    </div>
                    <div class="txt_lienhe"> <br><b>Hotline:</b><br><br>
                        <a href="tel:<?php echo $seo['telco']; ?>" class="c_yellow"><?php echo $seo['telco']; ?> TƯ VẤN VISA </a> <br><br>
                        <a href="tel:0984415828">098.441.5828 (Mrs Văn) </a> <br><br>
                        <a href="tel:0987227744">0987.22.77.44 (Mr Sơn) </a> <br><br>
                        <a href="tel:0971300230">0971.300.230 (Mr Tâm) </a> <br><br>
                    </div>
                </div>
            </div>
        </div>

        <div id="navscrim"></div>

        <div id="wrapper_header">
            <div id="header">
                <div class="width_common">
                    <div>
                        <a href="/" class="logo_mobile"><amp-img class="img_logo" alt="Dịch vụ xin visa quốc tế uy tín" src="/assets/user/images/logo.png" width="115" height="35"></amp-img></a>
                    </div>
                    <div class="social">
                        <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $canonical; ?>" title="Chia sẽ dịch vụ xin visa lên Facebook"><amp-img src="<?php echo base_url(); ?>assets/user/images/social_fb.gif" width=25 height=25></amp-img> </a>
                        <a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo str_replace('"', '', $news['title']); ?>&url=<?php echo $canonical; ?>" title="Chia sẽ dịch vụ xin visa lên Twiiter"><amp-img src="<?php echo base_url(); ?>assets/user/images/social_tweet.gif" width=25 height=25></amp-img> </a>
                        <a target="_blank" href="https://plus.google.com/share?url=<?php echo $canonical; ?>" title="Chia sẽ dịch vụ xin visa lên Goolge Plus"><amp-img src="<?php echo base_url(); ?>assets/user/images/social_google.gif" width=25 height=25></amp-img> </a>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
                <div class="clear">&nbsp;</div>
            </div>
        </div>

        <div id="wrapper_container">
            <div id="container">
                <div class="width_common">
                    <div class="breakumb_timer width_common">
                        <div class="timer_header">
                            <?php
                            if ($cat['type'] == 'visa') {
                                $r = 'visa';
                            } elseif ($cat['type'] == 'news') {
                                $r = 'guide';
                            }
                            ?>
                            <ul>

                                <li class="start"><a href="/<?php echo $r . '/' . $cat['alias']; ?>" title="<?php echo $cat['title']; ?>"><?php echo $cat['title']; ?> </a> <span class="arrow_breakumb">&nbsp;</span></li>

                            </ul>
                        </div>
                    </div>
                    <div id="detail_page" class="width_common">
                        <div id="col_660" class="left">
                            <div class="box_width_common" id="box_details_news">
                                <div class="left">
                                    <div class="main_content_detail width_common">

                                        <div class="title_news">
                                            <h1><?php echo $news['title'] ?></h1>
                                            <br>
                                            <amp-img src="<?php echo base_url(); ?>assets/user/images/hotline.gif" width="14" height="14"></amp-img> Gọi ngay để được tư vấn miễn phí: <b class="c_red"><?php echo $getphone['show']; ?></b>
                                            <br>
                                        </div>
                                        <div class="short_intro txt_666"> <?php echo $news['descript']; ?> </div>

                                        <div class="width_common space_bottom_20">
                                            <div>
                                                <div class="fck_detail width_common">

                                                    <amp-img alt="<?php echo str_replace('"', '', $news['title']); ?>" src="<?php echo base_url(); ?>/assets/upload/page/<?php echo $news['image'] ? $news['image'] : 'no_img.jpg'; ?>" width="230" height="150"></amp-img>
                                                    <?php echo $news['data']['html']; ?>
                                                    <br>
                                                    <amp-img src="<?php echo base_url(); ?>assets/user/images/hotline.gif" width=14 height=14></amp-img> Gọi ngay để được tư vấn miễn phí: <b class="c_red"><?php echo $getphone['show']; ?></b>
                                                    <br>
                                                </div>
                                                <div class="clear">&nbsp;</div>
                                            </div>

                                            <div class="block_tag width_common space_bottom_20">
                                                <div class="txt_tag">Tags:</div>
                                                <?php
                                                if ($news['tags']) {
                                                    $tags = explode(',', $news['tags']);
                                                    foreach ($tags as $k => $v) {

                                                        echo '<a href="/tim-kiem/' . urlencode($v) . '" class="tag_item" title="' . $v . '">' . $v . '</a>';
                                                    }
                                                }
                                                ?>

                                            </div>
                                            <?php if (@$related_page): ?>
                                                <div class="box_tinkhac_detail" class="box_category width_common style_01">
                                                    <div class="title_box_category width_common">
                                                        <div class="txt_main_category"><a href="#">Bài viết visa liên quan</a></div>
                                                    </div>
                                                    <div class="content_box_category width_common">
                                                        <ul class="list_news">
                                                            <?php foreach ($related_page as $item): ?>
                                                                <li>
                                                                    <div class="thumb_160 width_common">
                                                                        <a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"><amp-img alt="<?php echo $item['title']; ?>" src="/assets/upload/page/<?php echo $item['image']; ?>" width=120 height=120 layout=responsive></amp-img></a>
                                                                    </div>
                                                                    <strong><a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a></strong>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php if (@$other_product): ?>
                                                <div class="box_tinkhac_detail" class="box_category width_common style_01">
                                                    <div class="title_box_category width_common">
                                                        <div class="txt_main_category"><a href="#">Bài viết visa hữu ích</a></div>
                                                    </div>
                                                    <div class="content_box_category width_common">
                                                        <ul class="list_news">
                                                            <?php foreach ($other_product as $item): ?>
                                                                <li>
                                                                    <div class="thumb_160 width_common">
                                                                        <a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"><amp-img alt="<?php echo $item['title']; ?>" src="/assets/upload/page/<?php echo $item['image']; ?>" width=60 height=60 layout=responsive></amp-img></a>
                                                                    </div>
                                                                    <strong><a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a></strong>
                                                                </li>
                                                            <?php endforeach; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>




                        </div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>

            <div id="wrapper_footer">
                <div class="go_head">
                    <div class="right">
                        <div id="footer"><a href="#top"><span class="icon_responsive ico_respone_02">&nbsp;</span></a></div>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
                <div id="footer">
                    <ul class="list_menu_small">
                        <li><a href="/">Trang chủ</a></li>
                        <?php foreach ($all_cat_visa as $item): ?>
                            <li><a href="/visa/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"> <?php echo $item['title']; ?></a></li>
                        <?php endforeach; ?>
                        <?php foreach ($all_cat_news as $item): ?>
                            l<i><a href="/guide/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>"><?php echo $item['title']; ?></a></li>
                            <?php endforeach; ?>
                    </ul>

                    <div class="ft-bot">
                        <div class="coppy_right_small">
                            <div class="tttoasoan">
                                <div class="txt_up width_common">
                                    <div class="width_50 "><a href="tel:<?php echo $getphone['click']; ?>" rel="nofollow"><amp-img src="<?php echo base_url(); ?>assets/user/images/hotline.gif" width=14 height=14></amp-img> Liên hệ hotline: <b class="c_red"><?php echo $getphone['show']; ?></b></a></div>
                                    <div class="width_50"><a href="tel:<?php echo $seo['phone2_click']; ?>" rel="nofollow"><amp-img src="<?php echo base_url(); ?>assets/user/images/icon_thongtintoasoan.gif" width=14 height=9></amp-img> <b class="c_red"><?php echo $seo['phone2_show']; ?></b></a></div>
                                </div>
                                <div class="txt_down width_common">
                                    <div class="width_50 "><b><a href="tel:0987227744">0987227744</a></b> (Mr.Sơn) </div>
                                    <div class="width_50"><b><a href="tel:0984415828">098.441.5828</a></b> (Mrs Văn)</div>
                                    <div class="width_50"><b><a href="tel:0971300230">0971.300.230</a></b> (Miss Tâm)</div>
                                </div>
                            </div>
                            <div class="coppy_right">
                                <p>&copy; <span>Copyright 2010 Achau.net,</span>  All rights reserved</p>
                                <p>&reg; Dịch vụ xin visa du lịch quốc tế uy tín.</p>
                            </div>
                        </div>
                    </div>
                    <div class="clear">&nbsp;</div>
                </div>
            </div>
        </div>
        <div class="m-call">
            <a href="tel:<?php echo $getphone['click']; ?>" rel="nofollow" title="Hotline hỗ trợ làm visa"> <?php echo $getphone['show']; ?><br><span>(chạm để gọi tư vấn VISA)</span></a>
        </div>
    </body>
</html>
