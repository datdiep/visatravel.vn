<!-- ********** Hero Area Start ********** -->
<div class="hero-area height-400 bg-img background-overlay" style="background-image: url(/assets/upload/category_news/<?php echo $cate_news['image'] ?>);">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-12 col-md-8 col-lg-6">
                <div itemscope itemtype="http://schema.org/BreadcrumbList" class="single-blog-title text-center">
                    <!-- Catagory -->
                    <div itemprop="itemListElement" class="post-cta"><a  itemscope itemtype="http://schema.org/ListItem" href="/<?php echo $cate_news['alias'] ?>"><span itemprop="name"><?php echo $cate_news['title'] ?></span></a></div>
                    <h3 itemtype="http://schema.org/Thing" itemprop="name"  title="<?php echo $detail['title'] ?>"><?php echo $detail['title'] ?></h3>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->

<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area ============= -->
            <div class="col-12 col-lg-8">
                <div class="single-blog-content mb-100">
                    <!-- Post Meta -->
                    <div class="post-meta">
                        <p><a href="#" class="post-author"></a> on <a href="#" class="post-date"><?php echo date("d-m-Y", strtotime($detail['date_create'])) ?></a></p>
                    </div>
                    <!-- Post Content -->
                    <div class="post-content">
                        <div class="article">
                            <?php echo $detail['content'] ?>
                        </div>
                        <?php if (!empty($tags)): ?>
                            <ul class="post-tags">
                                <?php foreach ($tags as $item_tag): ?>
                                    <li><a href="/<?php echo 'tag/' . urlencode($item_tag) ?>"><?php echo $item_tag ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <?php echo $comments ?>
                    </div>
                    <!-- Post Tags -->

                    <!-- Post Meta -->
                    <div class="post-meta second-part">
                        <p><a href="#" class="post-author"></a> Ngày đăng : <a href="#" class="post-date"><?php echo date("d-m-Y", strtotime($detail['date_create'])) ?></a></p>
                    </div>
                </div>
            </div>


            <!-- ========== Sidebar Area ========== -->
            <?php include 'right_content.php' ?>
        </div>

        <!-- ============== Related Post ============== -->
        <div class="title">
            <h5>Tin Liên Quan</h5>
        </div>
        <?php if (!empty($news_connect)): ?>
            <div class="row">
                <?php foreach ($news_connect as $item_news) : ?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <!-- Single Blog Post -->
                        <div class="single-blog-post">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                <!-- Catagory -->
                                <div class="post-cta"><a href="/<?php echo $cate_news['alias'] ?>"> <?php echo $cate_news['title'] ?></a></div>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                    <h5><?php echo $item_news['title'] ?></h5>
                                </a>
                                <p><?php echo $item_news['description'] ?></p>

                                <!-- Post Meta -->
                                <div class="post-meta">
                                    <p><a href="#" class="post-author"></a> Ngày đăng : <a href="#" class="post-date"><?php echo date("d-m-Y", strtotime($item_news['date_create'])) ?></a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>
    </div>
</div>

<!-- ***** Footer Area Start ***** -->


<script>
    $(document).ready(function ($) {
        $('.btn-check').click(function () {
            window.location.href = "<?php echo base_url() ?>" + "from-" + $("#nation").val();
        });
    });
</script>