<div class="page-title-container">
    <div class="container">
        <div class="page-title pull-left">
            <h2 class="entry-title"><?php echo $product['title']; ?></h2>
        </div>
        <ul class="breadcrumbs pull-right" itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
            <li><a itemprop="url" href="/" title="<?php echo WEB_TITLE; ?>"><span itemprop="title">Trang chủ</span></a></li>
            <li><a itemprop="url" href="/tour" title="Tour du lịch"><span itemprop="title">Tour</span></a></li>
            <li class="active"><span itemprop="title"><?php echo @$cat['title']; ?></span></li>
        </ul>
    </div>
</div>
<section id="content">
    <div class="container tour-detail-page">
        <div class="row tour-detail">
            <div class="col-md-6 col-sm-12">
                <?php if (count($list_image) > 1): ?>
                    <div class="featured-gallery image-box">
                        <span class="discount"><span class="discount-text">Achau.net - Tour du lịch chất lượng</span></span>
                        <div class="flexslider photo-gallery style1" id="post-slideshow1" data-sync="#post-carousel1" data-func-on-start="showTourDetailedDiscount">
                            <ul class="slides">
                                <?php foreach ($list_image as $item): ?>
                                    <li><img src="/assets/upload/product/<?php echo $product['folder'] . '/870/' . $item['name']; ?>" alt="<?php echo $product['title']; ?>" /></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="flexslider image-carousel style1" id="post-carousel1"  data-animation="slide" data-item-width="70" data-item-margin="10" data-sync="#post-slideshow1">
                            <ul class="slides">
                                <?php foreach ($list_image as $item): ?>
                                    <li><img height="70" width="70" src="/assets/upload/product/<?php echo $product['folder'] . '/870/' . $item['name']; ?>" alt="<?php echo $product['title']; ?>" /></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="featured-image">
                        <?php foreach ($list_image as $item): ?>
                            <img src="/assets/upload/product/<?php echo $product['folder'] . '/870/' . $item['name']; ?>" alt="<?php echo $product['title']; ?>" />
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </div>


            <div class="col-md-6 col-sm-12 nopadding">
                <h1><?php echo $product['title']; ?></h1>
                <div class="facelike">
                    <!-- facebook like -->
                    <div class="fb-like" style="padding-top: 8px" data-href="<?php echo current_url(); ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    <!-- end -->  
                </div>

                <div class="descript"><?php echo $product['descript']; ?></div>
                <div class="row">
                    <div class="col-md-6 term-description">
                        <p>Khởi hành: <?php echo @$product['start_from']; ?></p>
                        <p>Thời gian: <?php echo @$product['departure_time']; ?></p>
                        <p>Phương tiện: <?php echo @$product['vehicle']; ?></p>
                        <p>Khách sạn: <?php echo @$product['hotel']; ?></p>
                    </div>
                    <div class="col-md-6 term-price">
                        <div class="price"><span>Giá từ: </span><?php
                            if ($product['promotion'] > 0)
                                echo '<s style="color:#b3b3b3;font-size: 15px;">' . number_format($product['price']) . '' . $product['unit'] . '</s> <big>' . number_format($product['promotion']) . '' . $product['unit'] . '</big>';
                            else
                                echo '<big>' . number_format($product['price']) . '' . $product['unit'] . '</big>';
                            ?></div>
                        <div class="quantity">Số lượng: <input type="number" id="rq_quantity" name="rq_quantity" class="rq_ddl valid" value="1"></div>
                        <div class="tour-booking">
                            <a href="javascript:void(0)" rel="<?php echo $product['id']; ?>" class="order_product button red btn-medium uppercase">Đặt Tour</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div id="main" class="col-md-9" style="padding-left: 0;padding-right: 5px;">
                <blockquote class="style1 border-color-blue quote-color-yellow" style="text-align:center">
                    <span class="triangle"></span>Liên hệ đặt tour và nhận thông tin khuyến mãi: <span class="c_red">098.441.5828</span> or <span class="c_red">0822.600.009</span>
                </blockquote>
                <div id="tour-details" class="travelo-box">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#info-tour" class="tab-info"><b>Lịch trình</b></a></li>
                        <li><a data-toggle="tab" href="#tour-term" class="tab-term"><b>Chính sách</b></a></li>
                        <li><a data-toggle="tab" href="#more-info" class="tab-term"><b>Thông tin thêm</b></a></li>
                    </ul>

                    <div class="tab-content">
                        <div id="info-tour" class="info-tour tab-pane fade in active">
                            <h2>THÔNG TIN <?php echo $product['title']; ?></h2>
                            <p>
                                <?php echo $product['content']; ?>
                            </p>
                        </div>
                        <div id="tour-term" class="info-tour tab-pane fade">
                            <h2>Chính sách cho: <?php echo $product['title']; ?></h2>
                            <p>
                                <?php echo $product['policy']; ?>
                            </p>
                        </div>
                        <div id="more-info" class="info-tour tab-pane fade">
                            <h2>Thông tin thêm</h2>
                            <p>
                                <?php echo $product['more_info']; ?>
                            </p>
                        </div>
                    </div>
                    <div class="row facebook-comments">
                        <!-- FB comments-->
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id))
                                    return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1075181152563097";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-num-posts="10"></div>
                    </div>

                    <?php if (!empty($related_page)): ?>
                        <div id="dashboard">
                            <div class="row block" >
                                <div class="col-md-12 notifications">
                                    <h2>Bài viết liên quan</h2>
                                    <?php
                                    foreach ($related_page as $item):
                                        ?>
                                        <a href="/<?php echo $item['alias']; ?>" title="<?php echo $item['title']; ?>">
                                            <div class="icon-box style1 fourty-space">
                                                <img align="left" src="/assets/upload/page/<?php echo $item['img']; ?>" alt="<?php echo $item['title']; ?>" width="50" height="50">
                                                <p class="box-title"><?php echo $item['title']; ?></p>
                                            </div>
                                        </a>

                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="sidebar col-md-3" style="padding-left: 5px;padding-right: 0;">
                <?php echo $block_right; ?>                
            </div>
        </div>

    </div>
</div>

</div>
</section>
