<div class="hero-area height-80 bg-img background-overlay" style="background-image: url(/assets/user/img/blog-img/bg4.jpg);"></div>

<!-- ********** Hero Area End ********** -->
<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area Start ============= -->
            <div class="col-12 col-lg-8">
                <div class="post-content-area mb-100">
                    <!-- Catagory Area -->
                    <div class="world-catagory-area">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="title" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="/" class="active">Trang chủ / </a><span itemprop="name">Tìm Kiếm </span></li>
                            <li class="nav-item" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a class="nav-link active" itemscope itemtype="http://schema.org/Thing" itemprop="item"  href="/tag/<?php echo urlencode($keyword) ?>"> <span itemprop="name" ><?php echo $keyword ?></span></a>
                            </li>

                        </ul>
                        <div class="tab-container style1" style="margin-bottom:20px">

                            <div class="alert alert-info">
                                Kết quả của từ khóa  :  <?php echo $keyword; ?>
                                <span class="close"></span>
                            </div>


                            <?php
                            $color = array('silver', 'sky-blue1', 'yellow', 'dark-blue1', 'green', 'red', 'light-brown', 'orange', 'dull-blue', 'light-orange', 'light-purple', 'sea-blue', 'sky-blue2', 'dark-blue2', 'dark-orange', 'purple', 'light-yellow');
                            ?>

                            <?php foreach ($list_keyword as $item): $rand_color = $color[rand(0, count($color) - 1)]; ?>

                                <a class="button btn-small <?php echo $rand_color; ?>" href="/tim-kiem?q=<?php echo urlencode($item['keyword']); ?>"><?php echo $item['keyword']; ?></a>
                            <?php endforeach; ?>

                        </div>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                <?php foreach ($list_news as $item_news) : ?>
                                    <!-- Single Blog Post -->
                                    <div class="single-blog-post post-style-4 d-flex align-items-center">
                                        <!-- Post Thumbnail -->
                                        <div class="post-thumbnail">
                                            <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                        </div>
                                        <!-- Post Content -->
                                        <div class="post-content">
                                            <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                <h5><?php echo $item_news['title'] ?></h5>
                                            </a>
                                            <p><?php echo $item_news['description'] ?></p>
                                            <!-- Post Meta -->
                                            <div class="post-meta">
                                                <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>


                                <div class="pagination">
                                    <?php echo $links ?>
                                </div>
                                <div class="row">
                                    <script>
                                        (function () {
                                            var cx = '000916275728382050282:vaosgxbxmfg';
                                            var gcse = document.createElement('script');
                                            gcse.type = 'text/javascript';
                                            gcse.async = true;
                                            gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
                                            var s = document.getElementsByTagName('script')[0];
                                            s.parentNode.insertBefore(gcse, s);
                                        })();
                                    </script>
                                    <gcse:search enableAutoComplete="true"></gcse:search>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========== Sidebar Area ========== -->
            <?php include 'right_content.php' ?>
        </div>

        <!-- Load More btn -->

    </div>
</div>



<style>
    .gsc-control-cse .gsc-table-result {
        font-family : inherit;
    }

    .gsc-control-cse .gsc-input-box {
        height : inherit;
    }

    input.gsc-input,
    .gsc-input-box,
    .gsc-input-box-hover,
    .gsc-input-box-focus,
    .gsc-search-button,
    input.gsc-search-button-v2 {
        box-sizing  : content-box;
        line-height : normal;
        margin-top  : 0px;
    }
    .gsc-table-result td{border: none}
</style>


