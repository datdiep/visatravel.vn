<div class="hero-area height-80 bg-img background-overlay" style="background-image: url(/assets/user/img/blog-img/bg4.jpg);"></div>

<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyA5wrBO7A_kN_wXU2Q-bXOmXNPLCIdTY38"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var options = {
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapcanvas"), options);
        var location = new google.maps.LatLng(10.780136, 106.662646);
        map.setCenter(location);
        var marker = new google.maps.Marker({
            map: map,
            position: location,
            title: 'VIETNAM VISA OFFICE'
        });
    });
</script>
<div class="main-content-wrapper section-padding-50">
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="title" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="/" class="active">Trang chủ / </a><span itemprop="name">Liên hệ </span></li>

        </ul>
    </div>
    <div class="container" style="margin-top: 20px">
        <div class="">
            <div class="clearfix">
                <h3 style="padding-top: 15px; padding-bottom: 15px;">Chúng tôi có thể giúp gì cho bạn?</h3>
                <div class="row">
                    <div class="col-md-3 col-xs-padding-bottom-40">
                        <img alt="Apply Vietnam visa online" class="img-responsive" src="/assets/user/images/vietnamvisa-on-arrival.jpg">
                        <h5>Dịch vụ visa đi nước ngoài</h5>
                        <p>Bạn cần xin visa đi nước ngoài? Hãy để chuyên viên visa của chúng tôi giúp bạn hoàn thiện hồ sơ nhanh và chính xác.</p>
                    </div>
                    <div class="col-md-3 col-xs-padding-bottom-40">
                        <img alt="Extra service options" class="img-responsive" src="/assets/user/images/extra-service-option.jpg">
                        <h5>Các dịch vụ VIP</h5>
                        <p>Các dịch vụ visa đi nước ngoài hưởng chế độ VIP, không cần trình diện lãnh sự. Cam kết giao visa tận nhà</p>
                    </div>
                    <div class="col-md-3 col-xs-padding-bottom-40">
                        <img alt="Account and payment" class="img-responsive" src="/assets/user/images/account-support.jpg">
                        <h5>Chăm sóc khách hàng</h5>
                        <p>Các chuyên viên của chúng tôi luôn online để hỗ trợ giải đáp các thắc mắc của bạn.</p>
                    </div>
                    <div class="col-md-3">
                        <img alt="Technical support" class="img-responsive" src="/assets/user/images/technical-support.jpg">
                        <h5>Hỗ trợ kỹ thuật</h5>
                        <p>Đội CNTT của chúng tôi luôn hoàn thiện hệ thống để hoạt động ổn định phục vụ bạn.</p>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <h5 style="padding-top: 15px; padding-bottom: 15px;">Lien hệ trực tiếp</h5>
                <div class="row">
                    <div class="col-md-6 clearfix">
                        <div class="left" style="width: 120px">
                            <img alt="Live chat" src="/assets/user/images/icon-chat.jpg">
                        </div>
                        <h5 style="margin-top: 0px;">Chat trực tuyến</h5>
                        <p>Chat với chuyên viên của chúng tôi.</p>
                    </div>
                    <div class="col-md-6 clearfix">
                        <div class="left" style="width: 120px">
                            <img alt="Call us" src="/assets/user/images/icon-phone.jpg">
                        </div>
                        <h5 style="margin-top: 0px;">Gọi cho chúng tôi</h5>
                        <p>Hotline: <a href="tel:<?php echo $seo['phone1_click']; ?>"><?php echo $seo['phone1_show']; ?></a></p>
                    </div>
                </div>
            </div>
            <div class="clearfix" style="padding: 20px 0px 40px;">
                <div class="row">
                    <div class="col-md-6">
                        <h3 style="padding-top: 15px; padding-bottom: 15px;">Liên hệ Vietnam Visa Services Team</h3>
                        <div class="clearfix">
                            <div class="left" style="width: 100px">
                                <p><i class="fa fa-map-marker"></i> <strong>Địa chỉ:</strong></p>
                            </div>
                            <div class="left">
                                <p class="">
                                    <b>CÔNG TY TM-DV-DL TÂN VĂN LANG (Thành lập 1997)</b><br>
                                    <?php echo $seo['address']; ?>
                                </p>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="left" style="width: 100px">
                                <p><i class="fa fa-phone"></i> <strong>Hotline 1:</strong></p>
                            </div>
                            <div class="left">
                                <span><?php echo $seo['phone2_show']; ?></span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="left" style="width: 100px">
                                <p><i class="fa fa-phone"></i> <strong>Hotline 2:</strong></p>
                            </div>
                            <div class="left">
                                <span><?php echo $seo['phone1_show']; ?></span>
                            </div>
                        </div>
                        <div class="clearfix">
                            <div class="left" style="width: 100px">
                                <p><i class="fa fa-envelope-o"></i> <strong>Email:</strong></p>
                            </div>
                            <div class="left">
                                <a href="mailto:<?php echo $seo['email']; ?>"><?php echo $seo['email']; ?></a>
                            </div>
                        </div>
                        <div class="googlemap">
                            <iframe src="https://www.google.com/maps/d/u/0/embed?mid=13xkXbxp2FNComRTAkZ_qWBtbVVg" width="555" height="280" style="border:0px"></iframe>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <h3 style="padding-top: 15px; padding-bottom: 15px;">Bạn cần hỗ trợ : </h3>
                        <p class="alert_phone"><?php echo $msg ?></p>
                        <form id="contact-form" action="/contact" method="POST">
                            <div class="form-group">
                                <label class="form-label">TÊN CỦA BẠN <span class="required">*</span></label> 
                                <input type="text" value="" id="fullname" name="fullname" required="" class="form-control">

                            </div>
                            <div class="form-group">
                                <label class="form-label">EMAIL</label> 
                                <input type="email" value="" id="email" name="email"  class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-label">SỐ ĐIỆN THOẠI</label> <span class="required">*</span>
                                <input type="text" value="" id="phone" name="phone" required="true" class="form-control"><br>
                            </div>
                            <div class="form-group">
                                <label class="form-label">NỘI DUNG <span class="required">*</span></label> 
                                <textarea required="" style="height: 108px;" id="message" name="message" type="text" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-danger btn-contact" name="submit" value="GỬI TIN NHẮN">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $(".btn-contact").click(function () {
                var err = 0;
                var msg = new Array();
                if ($("#fullname").val() == "") {
                    $("#fullname").addClass("error");
                    msg.push("Your name is required.");
                    err++;
                } else {
                    $("#fullname").removeClass("error");
                }

                if ($("#phone").val() == "") {
                    $("#phone").addClass("error");
                    msg.push("Your email is required.");
                    err++;
                } else {
                    $("#phone").removeClass("error");
                }

                if ($("#message").val() == "") {
                    $("#message").addClass("error");
                    msg.push("Please give us your message.");
                    err++;
                } else {
                    $("#message").removeClass("error");
                }



                if (err == 0) {
                    return true;
                } else {
                    showErrorMessage(msg);
                    return false;
                }
            });
        });
        $("#contact-form").submit(function () {
            alert('Cảm ơn bạn gửi thông tin, Chúng tôi sẽ gọi lại ngay khi nhận');
        });
    </script>			
</div>