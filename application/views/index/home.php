<!-- ********** Hero Area Start ********** -->
<div class="hero-area">

    <!-- Hero Slides Area -->
    <div class="hero-slides owl-carousel">

        <?php foreach ($sliders as $item_slider) : ?>
            <!-- Single Slide -->
            <div class="single-hero-slide bg-img background-overlay" style="background-image: url(assets/upload/gallery/<?php echo $item_slider['image'] ?>);"></div>
        <?php endforeach; ?>
        <!--                 Single Slide 
                        <div class="single-hero-slide bg-img background-overlay" style="background-image: url(assets/user/img/blog-img/bg1.jpg);"></div>-->

    </div>


    <div id="searchBox">
        <div class="inner">
            <h3 class="text-center">Dịch vụ visa uy tín 15 năm</h3>
            <p class="alert_phone"></p>
            <input required type="text" class="phone_custumer" placeholder="Số điện thoại của bạn?">
            <select class="content_service" name="type">
                <option value="Visa đi Đài Loan">Visa đi Đài Loan</option>
                <option value="Vi sa nhật bản">Vi sa Nhật Bản</option>
                <option value="Visa đi Hàn Quốc">Visa đi Hàn Quốc</option>
                <option value="Visa đi Hàn Quốc">Visa đi Trung Quốc</option>
                <option value="Visa đi Hàn Quốc">Visa đi HongKong</option>
            </select>
            <a class="btn_get_call"><i class="fa fa-phone"></i> YÊU CẦU GỌI LẠI</a>
        </div>
    </div>


    <!-- Hero Post Slide -->
    <div class="hero-post-area">
        <div class="container">

            <div class="row">

                <div class="col-12">
                    <div class="hero-post-slide">
                        <!-- Single Slide -->
                        <div class="single-slide d-flex align-items-center">
                            <div class="post-number">
                                <p>1</p>
                            </div>
                            <div class="post-title">
                                <a href="single-blog.html">15+ Năm kinh nghiệm xin Visa</a>
                            </div>
                        </div>
                        <!-- Single Slide -->
                        <div class="single-slide d-flex align-items-center">
                            <div class="post-number">
                                <p>2</p>
                            </div>
                            <div class="post-title">
                                <a href="single-blog.html">100% Khách hàng hài lòng</a>
                            </div>
                        </div>
                        <!-- Single Slide -->
                        <div class="single-slide d-flex align-items-center">
                            <div class="post-number"> 
                                <p>3</p>
                            </div>
                            <div class="post-title">
                                <a href="single-blog.html">24/7 Tư vấn miễn phí</a>
                            </div>
                        </div>
                        <!-- Single Slide -->
                        <div class="single-slide d-flex align-items-center">
                            <div class="post-number">
                                <p>4</p>
                            </div>
                            <div class="post-title">
                                <a href="single-blog.html">99.9% Tỷ lệ đậu visa cực cao</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ********** Hero Area End ********** -->
<div class="main-content-wrapper section-padding-100">
    <div class="container">
        <div class="row justify-content-center">
            <!-- ============= Post Content Area Start ============= -->
            <div class="col-12 col-lg-8">
                <div class="post-content-area mb-50">
                    <!-- Catagory Area -->
                    <div class="world-catagory-area">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="title"><?php echo $cate_news_1['title'] ?></li>

                            <li class="nav-item">
                                <a class="nav-link active" id="tab1" data-toggle="tab" href="#world-tab-1" role="tab" aria-controls="world-tab-1" aria-selected="true">All</a>
                            </li>                            
                        </ul>

                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="world-tab-1" role="tabpanel" aria-labelledby="tab1">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="world-catagory-slider owl-carousel wow fadeInUpBig" data-wow-delay="0.1s">
                                            <?php foreach ($news_1_hot as $item_news): ?>
                                                <!-- Single Blog Post -->
                                                <div class="single-blog-post">
                                                    <!-- Post Thumbnail -->
                                                    <div class="post-thumbnail">
                                                        <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                                        <!-- Catagory -->
                                                        <div class="post-cta"><a href="/<?php echo $list_cat_news[$item_news['cate_id']]['alias'] ?>"><?php echo $list_cat_news[$item_news['cate_id']]['title'] ?></a></div>
                                                    </div>
                                                    <!-- Post Content -->
                                                    <div class="post-content">
                                                        <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                            <h5><?php echo $item_news['title'] ?></h5>
                                                        </a>
                                                        <p><?php echo $item_news['description'] ?></p>
                                                        <!-- Post Meta -->
                                                        <div class="post-meta">
                                                            <p><a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="post-author"></a> Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <?php foreach ($news_1 as $item_news) : ?>
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post post-style-2 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                        <h5><?php echo $item_news['title'] ?></h5>
                                                    </a>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <p><a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Catagory Area -->
                    <div class="world-catagory-area mt-50">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="title"><?php echo $cate_news_2['title'] ?></li>

                            <li class="nav-item">
                                <a class="nav-link active" id="tab10" data-toggle="tab" href="#world-tab-10" role="tab" aria-controls="world-tab-10" aria-selected="true">All</a>
                            </li>


                        </ul>

                        <div class="tab-content" id="myTabContent2">

                            <div class="tab-pane fade show active" id="world-tab-10" role="tabpanel" aria-labelledby="tab10">
                                <div class="row">
                                    <?php foreach ($news_2_hot as $item_news): ?>
                                        <div class="col-12 col-md-6">
                                            <!-- Single Blog Post -->
                                            <div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
                                                <!-- Post Thumbnail -->
                                                <div class="post-thumbnail">
                                                    <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                                    <!-- Catagory -->
                                                    <div class="post-cta"><a href="/<?php echo $list_cat_news[$item_news['cate_id']]['alias'] ?>"><?php echo $list_cat_news[$item_news['cate_id']]['title'] ?></a></div>
                                                </div>
                                                <!-- Post Content -->
                                                <div class="post-content">
                                                    <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                        <h5><?php echo $item_news['title'] ?></h5>
                                                    </a>
                                                    <p><?php echo $item_news['description'] ?></p>
                                                    <!-- Post Meta -->
                                                    <div class="post-meta">
                                                        <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <div class="col-12">
                                        <div class="world-catagory-slider2 owl-carousel wow fadeInUpBig" data-wow-delay="0.4s">
                                            <!-- ========= Single Catagory Slide ========= -->


                                            <?php foreach ($news_2 as $key => $item_news) : ?>
                                                <!-- tai vi tri chia het cho 4 de lay 4 phan tu thi phai co div cata slide -->
                                                <?php if ($key == 0 || $key % 4 == 0) : ?>
                                                    <div class="single-cata-slide">
                                                        <div class="row">
                                                        <?php endif; ?>
                                                        <div class="col-12 col-md-6">
                                                            <!-- Single Blog Post -->
                                                            <div class="single-blog-post post-style-2 d-flex align-items-center mb-1">
                                                                <!-- Post Thumbnail -->
                                                                <div class="post-thumbnail">
                                                                    <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                                                </div>
                                                                <!-- Post Content -->
                                                                <div class="post-content">
                                                                    <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                                                        <h5><?php echo $item_news['title'] ?></h5>
                                                                    </a>
                                                                    <!-- Post Meta -->
                                                                    <div class="post-meta">
                                                                        <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- dong div tai vi tri ke ben vi tri mo => -->
                                                        <?php if (($key + 1) % 4 == 0) : ?>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endforeach; ?>

                                            <!-- ========= Single Catagory Slide ========= -->


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <?php include 'right_content.php'; ?>
        </div>
        <div class="world-latest-articles">
            <div class="row">
                <div class="col-12 col-lg-8">

                    <div class="title">
                        <h5>Tin Mới nhất</h5>
                    </div>
                    <?php foreach ($news_views_new as $item_news) : ?>
                        <!-- Single Blog Post -->
                        <div class="single-blog-post post-style-4 d-flex align-items-center wow fadeInUpBig" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                    <h5><?php echo $item_news['title'] ?></h5>
                                </a>
                                <p><?php echo $item_news['description'] ?></p>
                                <!-- Post Meta -->
                                <div class="post-meta">
                                    <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <div class="col-12 col-lg-4">
                    <div class="title">
                        <h5>Tin Hot</h5>
                    </div>
                    <?php foreach ($news_views_hot as $item_news) : ?>
                        <!-- Single Blog Post -->
                        <div class="single-blog-post wow fadeInUpBig" data-wow-delay="0.2s">
                            <!-- Post Thumbnail -->
                            <div class="post-thumbnail">
                                <a href="<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>"><img src="/assets/upload/news/<?php echo $item_news['image'] ?>" alt=""></a>
                                <!-- Catagory -->
                                <div class="post-cta"><a href="/<?php echo $list_cat_news[$item_news['cate_id']]['alias'] ?>"><?php echo $list_cat_news[$item_news['cate_id']]['title'] ?></a></div>
                                <!-- Video Button -->
                            </div>
                            <!-- Post Content -->
                            <div class="post-content">
                                <a href="/<?php echo $item_news['alias'] . '-' . $item_news['id'] . '.html' ?>" class="headline">
                                    <h5><?php echo $item_news['title'] ?></h5>
                                </a>
                                <p><?php echo $item_news['description'] ?></p>
                                <!-- Post Meta -->
                                <div class="post-meta">
                                    <p><a href="#" class="post-author"></a>  Ngày đăng :  <a href="#" class="post-date"><?php echo date("d/m/Y", strtotime($item_news['date_create'])) ?></a></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    var flag_btn = 0;
    $(".btn_get_call").click(function () {
        phone_custumer = $(".phone_custumer").val();
        content_service = $(".content_service").val();
        if (phone_custumer != "" && flag_btn != 1) {
            flag_btn = 1;
            $.post("/contact", {phone: phone_custumer, content: content_service, request: 1}, function (results) {
                $(".alert_phone").html("<i class='fa fa-check'></i> " + results + " ");
            });
        } else if (phone_custumer == "")
            $(".alert_phone").html("<i class='fa fa-check'></i> Xin nhập số điện thoại ");
        else
            $(".alert_phone").html("<i class='fa fa-warning'></i> Visatravel đã nhận được thông tin");
    });
</script>

