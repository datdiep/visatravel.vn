

<input type="file" class="form-control" accept="image/*,application/pdf" id="images" name="images[]" multiple />
<div id="image" class="image_product_details">

</div>
<div style="clear: both"></div>
<?php if (!empty($images)): ?>
    <hr/>
    <div class="image_product_details" id="lightgallery">

        <?php foreach ($images as $k => $item): ?>                       
            <a class="img_<?php echo $k; ?>" data-sub-html='<span class="del_img" type="it_img" name="<?php echo $item; ?>" rel="img_<?php echo $k; ?>"><i class="fa fa-trash-0"></i> Del Image</span>' href="<?php echo $item; ?>">
                <img src="<?php echo $item; ?>"/>
            </a>                                                       
        <?php endforeach; ?>                                                                          

    </div>
    <div style="clear: both"></div>
<?php endif; ?>   


<?php if (!empty($pdf)): ?>

    <ul class="todo-list ui-sortable" style="margin-top:5px">
        <?php foreach ($pdf as $k => $item): ?>
            <li class="pdf_<?php echo $k; ?>">
                <a target="_bank" href="<?php echo $item; ?>">
                    <span class="handle ui-sortable-handle">
                        <i class="fa fa-file-pdf-o"></i>
                    </span>
                    <span class="text"><?php echo basename($item); ?></span>
                </a>
                <div class="tools">
                    <i type="it_pdf" name="<?php echo $item; ?>" rel="pdf_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                </div>

            </li>
        <?php endforeach; ?>     
    </ul>
<?php endif; ?>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/css/lightgallery.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="https://cdn.jsdelivr.net/g/lightgallery@1.3.5,lg-fullscreen@1.0.1,lg-zoom@1.0.3"></script>
<script>
    var reader = new FileReader(),
            i = 0,
            numFiles = 0,
            imageFiles;
    function readFile() {
        reader.readAsDataURL(imageFiles[i])
    }
    reader.onloadend = function (e) {
        i++;
        var type = base64MimeType(e.target.result);
        var data = e.target.result;
        if (type == 'application/pdf') {
            data = '/assets/user/images/pdf-icon.png';
        }
        if (typeof type == 'undefined') {
            data = '/assets/user/images/zip-icon.png';
        }
        //var pdf = "<div class='row'><object id='pdf' data='" + e.target.result + "' type='application/pdf'></object></div>";
        var image = "<div class='row'><img src ='" + data + "'/></div>";
        $(image).appendTo('#image');
        if (i < numFiles) {
            readFile();
        } else {
            var btn = '<button type="submit" value="-1" name="submit" class="btn btn-app btn-up-pic"><i class="fa fa-upload"></i> Upload</a>';
            $(btn).appendTo('#image');
        }
    };

    $('#images').change(function () {
        imageFiles = document.getElementById('images').files
        $('#image').html('');

        i = 0;
        numFiles = imageFiles.length;
        readFile();
    });
    var gallery = $('#lightgallery').lightGallery({
        appendSubHtmlTo: '.lg-item'
    });
    $("body").on('click', '.del_img', function () {
        var ele = $(this).attr('rel');
        var name = $(this).attr('name');
        var type = $(this).attr('type');
        if (type == 'it_img') {
            $("#lightgallery ." + ele).remove();
            gallery.data('lightGallery').destroy(true);
            gallery = $('#lightgallery').lightGallery({
                appendSubHtmlTo: '.lg-item'
            });
        } else {
            $("." + ele).remove();
        }
        $.post('/order/del_file', {name: name}, function (results) {
            console.log(results);
        });
    })
    function base64MimeType(encoded) {
        if (!encoded)
            return;
        var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

        if (mime && mime.length)
            return mime[1];
    }
</script>
<style>
    .lg-item span{
        position: absolute;
        background: red;
        padding: 5px 10px;
        bottom: 10px;
        cursor: pointer;
        color: #fff
    }
    #images{
        padding: 4px 5px;
    }
    #image{
        margin-top: 5px;
        margin-left: -5px;
    }
    /*    .box {
            position: relative;
            border-radius: 3px;
            background: #ffffff;
            border-top: 3px solid #d2d6de;
            margin-bottom: 20px;
            width: 100%;
            padding: 10px;
            box-shadow: 0 1px 1px rgba(0,0,0,0.1);
        }*/
    .btn-app {
        border-radius: 3px;
        position: relative;
        padding: 15px 5px;
        margin: 0 0 10px 10px;
        min-width: 80px;
        height: 60px;
        text-align: center;
        color: #666;
        border: 1px solid #ddd;
        background-color: #f4f4f4;
        font-size: 12px;
        text-align: center;
    }
    .btn-app>.fa, .btn-app>.glyphicon, .btn-app>.ion {
        font-size: 20px;
        display: block;
        width: auto;
    }
    .btn-up-pic{
        float: left;
        margin: 5px 5px 0px 5px;
        width: 72px;
        min-width: inherit;
        height: 72px;
    }
    .btn-app:hover {
        background: #f4f4f4;
        color: #444;
        border-color: #aaa;
    }
    .box-tools span{
        display: inline-block;
        padding: 2px 10px;
        font-size: 20px;
    }
    .box-tools span b{
        color: red
    }
    .image_product_details{
        margin: -10px 0px 0 -5px;
    }
    .image_product_details div.row {
        float: left;
        margin: 5px;
        padding: 5px;
        border: 1px solid #09f;
    }
    .image_product_details img {
        float: left;
        height: 60px;
        width: 60px;
    }
    #lightgallery a {
        float: left;
        margin: 5px;
        padding: 5px;
        border: 1px solid #09f;
        position: relative;
        width: auto;
    }
    #lightgallery a span{
        position: absolute;
        bottom: 0px;
        right: 0px;
        background: #f4f4f4;
        padding: 1px 5px;
    }
    .todo-list {
        margin: 0;
        padding: 0;
        list-style: none;
        overflow: auto;
    }
    .todo-list>li {
        border-radius: 2px;
        padding: 10px;
        background: #f4f4f4;
        margin-bottom: 2px;
        border-left: 2px solid #e6e7e8;
        color: #444;
    }
    .todo-list>li:last-of-type {
        margin-bottom: 0;
    }
    .todo-list>li .tools {
        display: none;
        float: right;
        color: #dd4b39;
    }
    .todo-list>li .tools>.fa, .todo-list>li .tools>.glyphicon, .todo-list>li .tools>.ion {
        margin-right: 5px;
        cursor: pointer;
    }
    .todo-list>li:hover .tools {
        display: inline-block;
    }
</style>