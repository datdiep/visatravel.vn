<div class="panel-fees">
    <div class="payment-methods">
        <img alt="" src="/assets/user/images/payment-methods.jpg">
    </div>
    <ul>
        <li class="total clearfix">
            <div class="clearfix">
                <label>TỔNG PHÍ:</label>
                <span class="total_fee">Waiting...</span>
            </div>
            <div class="text-right">
                <i class="stamping_fee_included display-none">(<a target="_blank" rel="noopener" title="stamping fee" href="#">Phí dán tem</a> included, no need to pay any extra fee)</i>
                <i class="stamping_fee_excluded">(<a target="_blank" rel="noopener" title="stamping fee" href="#">Phí dán tem</a> thanh toán bằng tiền mặt tại sân bay)</i>
            </div>
        </li>
        <li class="clearfix">
            <label>Số lượng khách:</label>
            <span class="group_size_t">1 khách</span>
        </li>
        <li class="clearfix">
            <label>Mục đích nhập cảnh :</label>
            <span class="visit_purpose_t">Du lịch</span>
        </li>
        <li class="clearfix">
            <label>Thời gian lưu trú:</label>
            <span class="visa_type_t">1 tháng 1 lần<br>(Lưu trú 30 ngày / Ra vào 1 lần)</span>
        </li>
        <li class="clearfix">
            <label>Đến tại sân bay:</label>
            <span class="arrival_port_t">Sân bay Tân Sơn Nhất (HCM)</span>
        </li>
        <li class="clearfix">
            <label>Ngày nhập cảnh:</label>
            <span class="exit_port_t">Please select...</span>
        </li>
        <li class="clearfix display-none" id="stamping_fee_li">
            <label>Phí dán tem:</label>
            <span class="total_stamping_fee price"></span>
        </li>
        <li class="clearfix">
            <label>Phí dịch vụ:</label>
            <span class="total_service_fee price">Waiting...</span>
            <!--            <div class="text-justify">
                            <i class="help-block f12">the service fee is flexible for each nationality and may change in the next steps based on The Vietnam Immigration Rule</i>
                        </div>-->
        </li>
        <li class="clearfix display-none" id="nationality_li">
            <label>Phí xử lý hồ sơ quốc gia khó:</label>
            <div class="nationality-fee-detail text-right">

            </div>
        </li>
        <li class="clearfix" id="processing_time_li">
            <div class="clearfix">
                <label>Thời gian xử lý:</label>
                <span class="processing_note_t">Bình thường (2 ngày làm việc)</span>
            </div>
            <div class="clearfix">
                <span class="processing_t price">0 USD x 1 khách = 0 USD</span>
            </div>
        </li>

        <li class="clearfix display-none" id="private_visa_li">
            <label>Private letter:</label>
            <span class="private_visa_t price"></span>
        </li>
        <li class="clearfix display-none" id="full_package_li">
            <label>Full package service:</label>
            <div class="full_package_services"></div>
        </li>
        <li class="clearfix display-none" id="extra_service_li">
            <label>Extra services:</label>
            <div class="extra_services"></div>
        </li>
        <li class="clearfix display-none" id="vipsave_li">
            <label>VIP discount:</label>
            <span class="vipsave_t price"></span>
        </li>
        <li class="clearfix" id="promotion_li" style="background-color: #F8F8F8">
            <div class="" id="promotion-box-input">
                <div class="row clearfix">
                    <label class="col-md-5">Mã giảm giá</label>
                    <div class="col-md-7">
                        <div class="input-group">
                            <input type="text" class="promotion-input form-control" id="promotion-input" name="promotion-input" value="">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-apply-code">SỬ DỤNG</button>
                            </div>
                        </div>
                        <div class="promotion-error red display-none">Code không tồn tại. Thử nhập mã khác!</div>
                    </div>
                </div>
            </div>
            <div class="clearfix display-none" id="promotion-box-succed">
                <label class="left">Promotion discount:</label>
                <span class="promotion_t price"></span>
            </div>
        </li>

    </ul>

</div>