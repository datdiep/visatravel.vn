<div class="support-online-widget right-corner">
    <div class="title">HỖ TRỢ ONLINE</div>
    <div class="content">
        <p><i>Đừng ngần ngại liên hệ chúng tôi 24/7</i></p>
        <table cellpadding="2" width="100%">
            <tbody><tr>
                    <td>Email</td><td>: </td>
                    <td class="email"><a href="mailto:<?php echo $seo['email']; ?>"><?php echo $seo['email']; ?></a></td>
                </tr>
                <tr>
                    <td>Hotline VN</td><td>: </td>
                    <td class="phone"><a href="tel:<?php echo $seo['phone1_click']; ?>"><?php echo $seo['phone1_show']; ?></a></td>
                </tr>
                <tr>
                    <td>Hotline US</td><td>: </td>
                    <td class="phone"><a href="tel:<?php echo $seo['phone2_click']; ?>"><?php echo $seo['phone2_show']; ?></a></td>
                </tr>
            </tbody></table>
    </div>
</div>

