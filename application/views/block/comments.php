
<div class="comments">
    <!-- FB comments-->
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1075181152563097";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <div class="fb-comments" data-href="<?php echo current_url(); ?>" data-width="100%" data-num-posts="10"></div>
    <?php if (!empty($list_comments)): ?>
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="pad">
                <?php foreach ($list_comments as $item): ?>
                    <div class="row">
                        <div class="avatar col-md-1 col-xs-2 col-sm-2 text-center">
                            <a href="<?php echo empty($item['link_facebook']) ? '#': $item['link_facebook']; ?>" target="blank" title="<?php echo $item['name']; ?>">
                            <img src="<?php echo empty($item['link_avatar']) ? '/assets/user/avatar/'.$item['avatar']: $item['link_avatar']; ?>">
                            </a>
                            <?php if ($user['username']): ?>
                                <button type="submit" style="margin-top: 5px;margin-left: 10px;" name="delete" title="Xóa comments của <?php echo $item['name']; ?>" value="<?php echo $item['id']; ?>" class="fa fa-trash-o"></button>
                            <?php endif; ?>
                        </div>
                        <div class="content col-md-11 col-xs-10 col-sm-10">
                            <div class="row question">
                                <a href="<?php echo empty($item['link_facebook']) ? '#': $item['link_facebook']; ?>" target="blank" title="<?php echo $item['name']; ?>"><?php echo $item['name']; ?></a>
                                <span><?php echo $item['sub_title']; ?></span>
                                <div class="text"> 
                                    <?php echo $item['content']; ?>
                                    <div class="rank col-md-12">
                                        <a href="javascript:void(0)" onclick="like_fb(<?php echo $item['id'];?>, this)">Thích</a>
                                        <span role="presentation" aria-hidden="true"> · </span>
                                        <a href="#">Phản hồi</a>
                                        <span class="like_fb_<?php echo $item['id'];?> <?php echo ($item['like_fb'] > 0) ? '':'display-none';?>">
                                            <span role="presentation" aria-hidden="true"> · </span>
                                            <span><i class="sp_wwcvnv9pM9Q" alt=""></i> <span class="num_like"><?php echo $item['like_fb'];?></span></span>
                                        </span>
                                        <span role="presentation" aria-hidden="true"> · </span>
                                        <span><?php echo date('M d Y H:i', strtotime($item['date_create'])); ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="_5lm5 _2pi3"><div direction="left" class="clearfix _3-8y"><div class="_ohe lfloat"><i alt="" class="img _8o _8r img sp_EKGFHzGY4C3 sx_9d390b"></i></div><div class=""><div class="_42ef _8u"><a target="_blank" href="https://developers.facebook.com/docs/plugins/comments/?utm_campaign=social_plugins&amp;utm_medium=offsite_pages&amp;utm_source=comments_plugin">Facebook Comments Plugin</a></div></div></div></div>
            </div>
        </form>
    <?php endif; ?>

    <?php if ($user['username']): ?>
        <div class="post-comments col-md-12 col-xs-12">
            <form id="form" method="post" enctype="multipart/form-data">

                <?php if (@$post_sucess): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Alert!</h4>
                        Thêm thành công
                    </div>
                <?php endif; ?>

                <div class="box box-info">
                    <div class="box-body">
                        <div class="form-group left_comment col-md-5 col-xs-12 nopadding">
                            <i class="fa fa-user-md" aria-hidden="true"></i>
                            <label>Tên:</label>
                            <input type="text" name="name" placeholder="Tên người comments" class="form-control dashed">
                        </div>

                        <div class="form-group left_comment col-md-4 col-xs-12">
                            <i class="fa fa-check-square" aria-hidden="true"></i>
                            <label>Tiêu đề phụ:</label>
                            <input type="text" name="sub_title" placeholder="Giám đốc Marketing tại HCM" class="form-control dashed">
                        </div>
                        <div class="form-group left_comment col-md-3 col-xs-12 nopadding">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                            <label>Ngày post:</label>
                            <input type='text' class="form-control dashed" id='datetimepicker' name="date_create" />
                        </div>
                        <div class="form-group left_comment col-md-5 col-xs-12 nopadding">
                            <i class="fa fa-facebook-square" aria-hidden="true"></i>
                            <label>Link profile:</label>
                            <input type="text" name="link_facebook" placeholder="Profile comments" class="form-control dashed">
                        </div>
                        <div class="form-group left_comment   col-md-7 col-xs-12" style="padding-right: 0px;">
                            <i class="fa fa-link" aria-hidden="true"></i>
                            <label>Link avatar:</label>
                            <input type="text" name="link_avatar" placeholder="Avatar comments" class="form-control dashed">
                        </div>
                        <div class="form-group  col-xs-12 nopadding">
                            <i class="fa fa-exchange" aria-hidden="true"></i>
                            <label>Nội dung comments:</label>
                            <textarea class="textarea form-control" name="content" placeholder="Nhập nội dung cần comments"></textarea>
                        </div>
                        <div class="center apply_button">
                            <button type="submit" name="submit" value="1" class="btn btn-lg btn-danger"> Gửi comments </button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    <?php endif; ?>
</div>
<script>
    $('#datetimepicker').datetimepicker({format: 'MMMM D, YYYY HH:mm:ss'});
    function like_fb(id,_this) {
        var text = $(_this).text();
        var method = 'unlike';
        $(_this).html('Thích');
        if(text == 'Thích'){
           method = 'like';
           $(_this).html('Bỏ thích');
        }            
        $.post('/ajax/like_fb', {id: id,method:method}, function (results) {
            console.log(results);
            if(results > 0){
                $('.like_fb_'+id).css('display','inline-block');
                $('.like_fb_'+id+' .num_like').html(results);
            }else{
                $('.like_fb_'+id).css('display','none');
                $('.like_fb_'+id+' .num_like').html(results);
            }
            
        });
    }
</script>