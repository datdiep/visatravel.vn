<div class="payment-online">
    <a href="/payment-online">
        <h1>THANH TOÁN ONLINE</h1>
        <p>100% Bảo mật & an toàn tuyệt đối</p>
    </a>
</div>				
<div class="confidence-widget">
    <div class="banner"></div>
    <div class="content">
        <div class="title">
            TẠI SAO CHỌN CHÚNG TÔI?
        </div>
        <ul>
            <li>Cam kết và trách nhiệm</li>
            <li>Thực hiện hồ sơ nhanh</li>
            <li><b>Hơn 15 năm kinh nghiệm</b></li>
            <li>Chính sách hoàn phí rõ ràng</li>
            <li>Thu hồ sơ tận nhà</li>
        </ul>
    </div>
</div>				
<div class="service-widget">
    <div class="banner"></div>
    <div class="content">
        <div class="title">
            DỊCH VỤ KÈM THÊM
        </div>
        <ul>
            <li><a title="" class="clearfix" href="#"><i class="passport"></i> Gia hạn visa Việt Nam</a></li>
            <li><a title="" class="clearfix" href="#"><i class="airport-concierge"></i> Đưa đón tại sân bay</a></li>
            <li><a title="" class="clearfix" href="#"><i class="car"></i> Cho thuê xe du lịch</a></li>
            <li><a title="" class="clearfix" href="#"><i class="hotel"></i> Đặt phòng khách sạn</a></li>
            <li><a title="" class="clearfix" href="#"><i class="travel"></i> Cho thuê hướng dẫn viên</a></li>
            <li><a title="" class="clearfix" href="#"><i class="flight"></i> Đặt vé máy bay giá rẻ</a></li>
        </ul>
    </div>
</div>				
