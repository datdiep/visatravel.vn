<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */
$route['default_controller'] = 'index';
$route['admincp'] = 'admincp/dashboard';
$route['admincp/(:any)'] = 'admincp/$1';
$route['admincp/order'] = 'admincp/order/index';
$route['admincp/homeinfo'] = 'admincp/homeinfo/index';
$route['admincp/order/(:num)'] = 'admincp/order/index/$1';
$route['admincp/customer'] = 'admincp/customer/index';
$route['admincp/customer/(:num)'] = 'admincp/customer/index/$1';
$route['admincp/admin'] = 'admincp/admin/index';
$route['admincp/admin/(:num)'] = 'admincp/admin/index/$1';
//
//$route['tim-kiem/(:any)'] = "index/search/$1";
//$route['schedule'] = 'index/schedule_post';
//$route['san-pham/(:any)-(:num).html'] = 'index/product/$2';
//$route['gio-hang.html'] = 'order/cart';
//$route['thanh-toan.html'] = 'order/finish_order';
//$route['thanh-toan/(:num)'] = "order/payment/$1";
//$route['page/(:any)'] = 'index/page/$1';
//
//$route['update_news'] = "index/update_news";
//$route['rss'] = "index/rss";
//$route['rss/(:any)/(:any).rss'] = "index/rss/$1/$2";
//$route['sitemap.xml'] = "index/sitemap";
//
//$route['ho-chieu'] = "index/passport";
//$route['visa'] = 'index/category_news/visa';
//$route['visa/(:any)'] = 'index/category_news/visa/$1';
//$route['visa/(:any)/(:num)'] = 'index/category_news/visa/$1/$2';
//
//$route['guide'] = 'index/category_news/guide/';
//$route['guide/(:any)'] = 'index/category_news/guide/$1';
//$route['guide/(:any)/(:num)'] = 'index/category_news/guide/$1/$2';
//
//$route['country/(:any)'] = 'index/category_news/country/$1';
//$route['country/(:any)/(:num)'] = 'index/category_news/country/$1/$2';
//
//$route['(:any)'] = "index/news/$1";
//$route['(:any)/(:any)'] = "index/news/$1/$2";
// Tri add code

$route['tim-kiem'] = 'index/search';
$route['kiem-tra-don-hang.html'] = 'auth/tracking';
$route['san-pham/(:any)-(:num).html'] = 'index/product/$2';
$route['gio-hang.html'] = 'order/cart';
$route['thanh-toan.html'] = 'order/finish_order';
$route['thanh-toan/(:num)'] = "order/payment/$1";
$route['page/(:any)'] = 'index/page/$1';

$route['rss'] = "index/rss";
$route['rss/(:any)/(:any).rss'] = "index/rss/$1/$2";
$route['sitemap.xml'] = "index/sitemap";

$route['category'] = 'index/category_news';

//$route['(:any)'] = 'index/category_news/$1'; // danh muc
//$route['(:any)/page/(:num)'] = 'index/category_news/$1/$2';


$route['(:any)-(:num).html'] = "index/news/$2";
$route['info/status'] = "index/check_visa_status";
//$route['dich-vu-visa'] = "index/dichvuvisa";
$route['hoi-dap-visa'] = "index/faq";
$route['(:any)'] = "index/news_cat/$1";
$route['(:any)/(:num)'] = "index/news_cat/$1/$2";
$route['(:any)/(:any).html'] = "index/direct_link/$1/$2";
$route['contact'] = "index/contact";
$route['search'] = "index/tim-kiem";
$route['tag/(:any)'] = "index/tag/$1";
$route['tag/(:any)/(:num)'] = "index/tag/$1/$2";

$route['su-kien/(:any).html'] = 'index/collection/$1';

$route['404_override'] = 'index/error_404/';
$route['translate_uri_dashes'] = FALSE;

