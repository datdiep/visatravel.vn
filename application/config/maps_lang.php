<?php

// Map dữ liệu dạng enum // trách sử dụng enum
$this->data['menu_status'] = array(
    'visa' => 'Dịch vụ visa',
    'guide' => 'Cẩm nang du lịch',
);

$this->data['country_status'] = array(
    'tourist' => 'Du lịch',
    'business' => 'Công tác - Thương mại',
    'relatives' => 'Thăm thân nhân',
    'study' => 'Du học',
);

$this->data['order_status'] = array(
    'create' => 'Mới tạo',
    'process' => 'Đang xử lý',
    'wait' => 'Đã xong chờ nộp',
    'waiting' => 'Chờ kết quả',
    'result' => 'Có kết quả',
    'done' => 'Hoàn tất',
    'trash' => 'thùng rác',
);

