<?php

$this->data['status_order'] = array(
    'order' => 'Đơn hàng mới',
    'approved' => 'Chờ in',
    'had_invoiced' => 'Đã in',
    'shipping' => 'Đang vận chuyển',
    'success' => 'Thành công',
    'unknown' => 'Chưa liên lạc',
    'waitting_product' => 'Chờ đổi sản phẩm',
    'waitting_pay' => 'Chờ chuyển khoàn',
    'cancel' => 'Bị huỷ',
    'fail' => 'Thất bại'
);
$this->load->model('shipper_model');
$maps_shipping = $this->shipper_model->get_all();
$this->data['maps_shipping'][0] = 'Chưa chọn';
foreach ($maps_shipping as $temp) {
    $this->data['maps_shipping'][$temp['id']] = $temp['name'];
}