<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
define('ADMIN_URL', 'admincp/');
define('PATH_UPLOAD', APPPATH . '../' . 'assets/upload/');
define('PATH_LOG', APPPATH . '../' . 'assets/logs/');
define('URL_UPLOAD_FONTED', '/assets/upload/');
define('LIMIT_PRODUCT', 5);
define('PRE_CATEGORY_CHILD', '-- &nbsp;&nbsp;');
define('PAYPAL_USERNAME', 'usddat_api1.gmail.com');
define('PAYPAL_PASSOWRD', '9TR4BRK6KQLMNRTS');
define('PAYPAL_SIGNATURE', 'A6t9nkA2Lw4VX60TgqEgsM0eY2-PA5Q3JX.I9kGloNwkk8D1pe1WN2CY');
define('USER_NGANLUONG', 'hongocbang2008@yahoo.com');
define('DOLLAR_RATE', 22500); // Tỉ giá Dola
//define('SECRET_KEY_VTC', '$@aEWE#@E#@2312sdas22d2'); // key VTC
define('SECRET_KEY_VTC', '12313$@#*&&sadSD'); // key VTC
define('SITE_NETWORK_ID', ',5,'); // ID mac dinh cua he thong
define('SITE_API', 'http://vietnamvisa.lc/api/'); // trang gọi api
define('TEST_LOCAL', '1'); // Sử dụng cho trường hợp chỉ chạy dưới local
define('WEB_TITLE', 'Dịch Vụ Làm Visa uy tín + Du lịch quốc tế Achau.net');

define('SITE_ADMIN', 'http://vietnamvisa.lc/');
define('DOMAIN_SITE', 'http://visatravel.lc');
define('TOKEN_SITE', 'sdfsadsf-dsf-sdafa-dsf');
