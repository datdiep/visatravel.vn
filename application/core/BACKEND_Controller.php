<?php

class BACKEND_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        //$this->data['username'] = $this->session->userdata('username');
        //unset($_COOKIE['username']);

        $this->data['username'] = @$_COOKIE['username'];
        if (!$this->data['username']) {
            redirect(base_url() . ADMIN_URL . 'auth/login');
        } else {

            $this->load->model(array('admin_model', 'role_controller_model'));
            $this->data['user'] = $this->admin_model->get_row_by(array('username' => $this->data['username']));
            $all_role = $this->role_controller_model->get_all();
            foreach ($all_role as $value) {
                $this->data['all_role'] .= $value['controller'] . ",";
            }

            if ($this->data['user']['status'] == 'career_break') {
                $this->session->set_userdata('username', '');
                redirect(base_url() . ADMIN_URL . 'auth/login');
            }

            $this->build_user();

            $role = $this->router->fetch_class();
          
            if (!$this->check_role($role)) {
                redirect(base_url() . ADMIN_URL );
            }
        }
        include APPPATH . 'config/maps_lang.php';
    }

    protected function build_user() {
        $roles = $this->role_controller_model->get_all();
        foreach ($roles as $r) {
            if ($r['type'] == "check-square")
                $check .= ',' . $r['controller'];
            if ($this->check_role($r['controller']) && $r['type'] == "codepen")
                $this->data['user'][$r['controller']] = 1;
        }
        if ($this->check_role("root"))
            $this->data['user']["root"] = 1;
        $this->data['user']['role_controller'] .= $check;
    }

    protected function check_role($role) {
        if ((strpos(',' . $this->data['user']['role_controller'] . ',', ',' . $role . ',') === false) && strpos(',' . $this->data['user']['role_controller'] . ',', ',root,') === false)
            return false;
        return true;
    }

    protected function get_page($cur_page = 0, $total_rows = 0, $limit = 10) {
        $this->load->library('my_pagination');
        $config['per_page'] = $limit;
        $config['cur_page'] = $cur_page;
        $config['total_rows'] = $total_rows;
        $config['use_page_numbers'] = false;
        $this->my_pagination->initialize($config);
        return $this->my_pagination->create_links();
    }

    function upload_image($dir, $name = 'image') {
        $this->load->library('upload');
        $image = '';
        $return = array('error' => 1, 'msg' => 'No image uploaded');
        if ($_FILES[$name]['name']) {
            $config = array(
                'upload_path' => $dir,
                'allowed_types' => 'gif|jpg|png',
                'file_name' => random_string()
            );
            $this->upload->initialize($config);
            if (!$this->upload->do_upload($name)) {
                $return['msg'] = $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $return = array('error' => 0, 'file_name' => $data['file_name']);
            }
        }
        return $return;
    }

    function deleteDir($dirPath) {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }

    protected function revert_time($time = "") {
        $exp = explode('/', $time);
        @$var = $exp[2] . '-' . $exp[1] . '-' . $exp[0];
        return $var;
    }

    function addnotify($content, $url, $receiver, $sender = null) {
        $notify = $this->getnotify($receiver);
        if (empty($sender)) {
            $name = 'system';
            $avatar = 'system.jpg';
        } else {
            $this->db->select('avatar');
            $user = $this->db->get_where('admin', array('username' => $sender))->row_array();
            $name = $sender;
            $avatar = $user['avatar'];
            if (empty($user['avatar']))
                $avatar = 'default.jpg';
        }
        $data = array(
            'date_create' => date('Y-m-d H:i:s'),
            'name' => $name,
            'avatar' => $avatar,
            'url' => $url,
            'content' => $content,
            'has_view' => 0
        );
        $notify[] = $data;
        $str = serialize($notify);
        $this->db->update('admin', array('notify' => $str), array('username' => $receiver));
        return $data;
    }

    function updatenotify($receiver, $id) {
        $notify = $this->getnotify($receiver);
        if (!empty($notify[$id])) {
            $notify[$id]['has_view'] = 1;
        }
        $str = serialize($notify);
        $this->db->update('admin', array('notify' => $str), array('username' => $receiver));
        return $notify[$id];
    }

    function getnotify($user) {
        $this->db->select('notify');
        $log = $this->db->get_where('admin', array('username' => $user))->row_array();
        return unserialize($log['notify']);
    }

    protected function load_block($func) {

        $return = '';
        foreach ($func as $f) {
            $f = 'load_block_' . $f;
            $return .= $this->$f();
        }
        return $return;
    }

    protected function load_block_chat($model = null, $id = null) {

        $this->load->model($model);
        if (!empty($model) && !empty($model)) {
            $this->data['log'] = $this->$model->getlog($id);
        } else {
            $this->data['msg_error'] = 1;
        }
        $this->data['block_file']['id'] = $id;
        $this->data['block_file']['model'] = $model;
        return $this->load->view('block/log_chat', $this->data, true);
    }

    protected function load_block_file($model, $id = 0) {
        $data = $this->$model->get_by_key($id);
        if (!empty($data)) {
            if (empty($data['folder'])) {
                $folder = date('Y') . '/' . date('m') . '/' . str_replace('_model', '', $model) . $id;
                $path = PATH_UPLOAD . 'data/' . $folder;
                @mkdir(PATH_UPLOAD . 'data/' . date('Y'), 0777);
                @mkdir(PATH_UPLOAD . 'data/' . date('Y') . '/' . date('m'), 0777);
                @mkdir($path, 0777);
                $this->$model->update(array('folder' => $folder), $id);
                $this->data['order'] = $order = $this->$model->get_by_key($id);
            } else {
                $path = PATH_UPLOAD . 'data/' . $data['folder'];
            }

            if (isset($_POST['submit']) && $_POST['submit'] == -1 && $_FILES['images']['name'][0] != '') {

                $this->upload_mutiple($_FILES, $path);
            }

            $this->data['folder'] = $data['folder'];
            $this->loadfile($path . '/');
        }
        $this->data['block_file']['id'] = $id;
        $this->data['block_file']['model'] = $model;
        return $this->load->view('block/file', $this->data, true);
    }

    protected function view_up_file($model, $id, $method = 0) {
        $data = $this->$model->get_by_key($id);
        if (!empty($data)) {
            if (empty($data['folder']) || (!file_exists(PATH_UPLOAD . 'data/' . $data['folder']) && !is_dir(PATH_UPLOAD . 'data/' . $data['folder']))) {
                $folder = date('Y') . '/' . date('m') . '/' . str_replace('_model', '', $model) . $id;
                $path = PATH_UPLOAD . 'data/' . $folder;
                @mkdir(PATH_UPLOAD . 'data/' . date('Y'), 0777);
                @mkdir(PATH_UPLOAD . 'data/' . date('Y') . '/' . date('m'), 0777);
                @mkdir($path, 0777);
                $this->$model->update(array('folder' => $folder), $id);
                $this->data['order'] = $order = $this->$model->get_by_key($id);
            } else {
                $path = PATH_UPLOAD . 'data/' . $data['folder'];
            }
            $files = $this->loadfile($path . '/');
            $files['folder'] = $data['folder'];
            $files['id'] = $id;
            $files['model'] = $model;
        } else {
            $files = array('model' => $model, 'id' => 0);
        }
        $files['only_view'] = $method;
        return $this->load->view('block/up_file', $files, true);
    }

    function up_file($encoded_string, $target_dir) {
        //$this->load->library('transload');
        //$repla = array("data:image/jpeg;base64,","data:application/pdf;base64,");
        //$mime = $this->transload->split_content("data:","base64,",$encoded_string);
        $ifp = fopen($target_dir, "wb");
        fwrite($ifp, base64_decode($encoded_string));
        fclose($ifp);
    }

    private function upload_mutiple($files, $path) {
        $cpt = count($_FILES['images']['name']);
        $images = '';
        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'gif|jpg|jpeg|png|pdf|zip|rar|xls|xlsx|doc|docx',
            'overwrite' => FALSE,
        );

        for ($i = 0; $i < $cpt; $i++) {
            //$config['file_name'] = 'img-'.$product_id;
            $config['file_name'] = $_FILES['images']['name'] = $files['images']['name'][$i];
            $config['file_name'] = sanitizeTitle($config['file_name']);

            $this->upload->initialize($config);
            $_FILES['images']['type'] = $files['images']['type'][$i];
            $_FILES['images']['tmp_name'] = $files['images']['tmp_name'][$i];
            $_FILES['images']['error'] = $files['images']['error'][$i];
            $_FILES['images']['size'] = $files['images']['size'][$i];
            //pre($config,false);
            $this->upload->do_upload('images');
        }
    }

    public function loadfile($path) {
        $data = array('images' => [], 'pdf' => [], 'zip' => [], 'excel' => [], 'word' => [], 'images_customer' => [], 'pdf_customer' => []);
        foreach (glob($path . '*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE) as $filename) {
            $data['images'][] = basename($filename);
        }
        foreach (glob($path . '*.{pdf,PDF}', GLOB_BRACE) as $filename) {
            $data['pdf'][] = basename($filename);
        }

        foreach (glob($path . '*.{zip,ZIP}', GLOB_BRACE) as $filename) {
            $data['zip'][] = basename($filename);
        }
        foreach (glob($path . '*.{xls,xlsx,XLS,XLSX}', GLOB_BRACE) as $filename) {
            $data['excel'][] = basename($filename);
        }
        foreach (glob($path . '*.{doc,docx,DOC,DOCX}', GLOB_BRACE) as $filename) {
            $data['word'][] = basename($filename);
        }
        foreach (glob($path . '/customer/*.{jpg,jpeg,png,gif,JPG,JPEG,PNG,GIF}', GLOB_BRACE) as $filename) {
            $data['images_customer'][] = basename($filename);
        }
        foreach (glob($path . '/customer/*.{pdf,PDF}', GLOB_BRACE) as $filename) {
            $data['pdf_customer'][] = basename($filename);
        }
        return $data;
    }

    protected function parse_options() {
        foreach ($_POST as $key => $val) {
            $options[$key] = $this->input->post($key);
        }
        return $options;
    }

    public function getdays($sStartDate, $sEndDate) {
//        $sEndDate = date("Y-m-d", strtotime($sEndDate));
//        $aDays = 0;
//        $sCurrentDate = $sStartDate;
//        while ($sCurrentDate < $sEndDate) {
//            $sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
//            $aDays++;
//        }
//        return $aDays;
        $sEndDate = date("Y-m-d", strtotime($sEndDate));
        $aDays_interval = strtotime($sEndDate) - strtotime($sStartDate);
        $aDays = floor($aDays_interval / (60 * 60 * 24));
        return $aDays;
    }

}

// Hàm rùi wiget
//addnotify($content, $url, $receiver, $sender = null);
//$this->data['load_file'] = $this->load_block_file();
//$this->data['load_chat'] = $this->load_block_chat('order_services_model',$id);