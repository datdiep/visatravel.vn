<?php

class FONTEND_Controller extends CI_Controller {

    function __construct() {
        parent::__construct($is_ajax = false);
        $this->load->library('mobiledetect');
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
        $this->load->model(array('gallery_model','category_news_model', 'category_service_model', 'service_detail_model', 'keyword_model'));

        $this->data['is_mobile'] = 0;
        $this->data['home'] = '';
        $this->cache_time = 600;
        if (@$this->mobiledetect->detect_mobile())
            $this->data['is_mobile'] = 1;

        if ($is_ajax == false)
            $this->ini_info();
        if (isset($_GET['utm_source'])) {
            setcookie("source", @$_GET['utm_source'], (time() + (86400 * 2)), '/', $_SERVER['SERVER_NAME']);
            setcookie("campaign", @$_GET['utm_campaign'], (time() + (86400 * 2)), '/', $_SERVER['SERVER_NAME']);
        }

        $this->data['list_cat_news'] = $this->category_news_model->get_by("type like 'news'", null, 'id');
        $this->data['sliders'] = $this->gallery_model->get_by('type = "slider"');
        // Kiem tra Admin login chua
        if ($_COOKIE['username']) {
            $this->data['username'] = $_COOKIE['username'];
            $this->load->model('admin_model');
            $this->data['user'] = $this->admin_model->get_row_by(array('username' => $this->data['username']));
            // pre($this->data['user']);
        }
    }

    protected function product_view($id = 0) {
        $product_ids_viewed = @$_COOKIE['product_ids_viewed'];
        if (!empty($product_ids_viewed)) {
            $product_ids_viewed = unserialize($product_ids_viewed);
            if (count($product_ids_viewed) == 7) {
                array_shift($product_ids_viewed);
            }
        }
        if ($id != 0) {
            $product_ids_viewed[] = $id;
            setcookie("product_ids_viewed", serialize($product_ids_viewed), (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        } else {
            return $product_ids_viewed;
        }
    }

    function send_mail($receiver, $subject, $content, $attach = '') {
        // Cấu hình
        $this->load->library('email');
        $config['protocol'] = 'sendmail';
        $config['charset'] = 'utf-8';
        $config['mailtype'] = 'html';
        $config['wordwrap'] = TRUE;
        $config['smtp_timeout'] = '7';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'tls://email-smtp.us-east-1.amazonaws.com';
        $config['smtp_user'] = 'AKIAIXSW3MYBFRBMP3LA';
        $config['smtp_pass'] = 'Ap6+5VS3LLcstKaWX1ORU+CYrr3frt1qXIhpbRgexLel';
        $config['smtp_port'] = '465';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n";

        $this->email->initialize($config);
        //cau hinh email va ten nguoi gui
        $this->email->from('noreply@congvannhapcanh.net', 'Vietnam Visa Group');
        //cau hinh nguoi nhan
        $this->email->to($receiver);
        $this->email->subject($subject);
        $this->email->message($content);

        if ($attach) {
            $this->email->attach($attach);
        }
        //dinh kem file
        //$this->email->attach('/path/to/photo1.jpg');
        //thuc hien gui
        if (!$this->email->send()) {
            // echo $this->email->print_debugger();
            return 1;
        } else {
            //  echo $this->email->print_debugger();
            return 0;
        }
    }

    private function ini_info() {
        $this->load->model(array('user_model', 'richsnipnet_model', 'page_model'));
        $this->data['visa_fees'] = $this->cache->get('visa_fees');
        if (!$this->data['visa_fees']) {
            $this->load->library('my_category');
            $this->data['visa_fees'] = $this->my_category->build_visa_fees();
            $this->cache->save('visa_fees', $this->data['visa_fees'], $this->cache_time);
        }

        $this->limit = LIMIT_PRODUCT;
        $this->data['current_url'] = str_replace('/index.php', '', current_url());
        $this->data['user'] = $this->session->userdata('user_fontend_id');
        if ($this->data['user']) {
            $this->data['user'] = $this->user_model->get_by_key($this->data['user']);
            if (empty($this->data['user']))
                $this->session->unset_userdata('user_fontend_id');
        }
        $this->data['total_money'] = intval($this->session->userdata('total_money'));
        $this->data['total_count'] = intval($this->session->userdata('total_count'));
        $this->data['seo'] = unserialize(file_get_contents(APPPATH . 'cache/seo_home'));
        $this->data['seo']['img'] = base_url() . 'assets/user/images/cover.jpg';
        $this->data['richsnipnet'] = $this->richsnipnet_model->get_by(array('url_request' => $_SERVER['REQUEST_URI']));
        $this->data['footer_about'] = $this->page_model->get_for_page(6, 0, array('type' => 'about'), 'id DESC');
        $this->data['footer_infomation'] = $this->page_model->get_for_page(6, 0, array('type' => 'infomation'), 'id DESC');
        $this->data['footer_tips'] = $this->page_model->get_for_page(6, 0, array('type' => 'tips'), 'id DESC');
        $this->data['footer_resource'] = $this->page_model->get_for_page(6, 0, array('type' => 'resource'), 'id DESC');
    }

    protected function get_page($cur_page = 0, $total_rows = 0, $limit = 10) {
        $this->load->library('my_pagination');
        $config['per_page'] = $limit;
        $config['cur_page'] = $cur_page;
        $config['total_rows'] = $total_rows;
        $this->my_pagination->initialize($config);
        return $this->my_pagination->create_links();
    }

    protected function load_block($func) {

        $return = '';
        foreach ($func as $f) {
            $f = 'load_block_' . $f;
            $return .= $this->$f();
        }
        return $return;
    }

    private function load_block_support() {
        return $this->load->view('block/support', $this->data, true);
    }

    private function load_block_payment() {
        return $this->load->view('block/payment', $this->data, true);
    }

    private function load_block_ads() {
        return $this->load->view('block/ads', $this->data, true);
    }

    private function load_block_fee() {
        return $this->load->view('block/fee', $this->data, true);
    }

}
