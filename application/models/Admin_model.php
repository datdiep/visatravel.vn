<?php

class Admin_Model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'admin';
    }
    function get_join($cond = null, $rex_index = '', $order = null) {
        if(!empty($cond)){
           $this->db->where($cond);
        }
        return $this->db->select('ad.*,n.name as network_name,lv.name as level_department_name')
                        ->from('admin as ad')
                        ->join('network as n', 'n.id = ad.network_id','left')
                        ->join('department as d', 'd.id = ad.department_id','left')
                        ->join('level_department as lv', 'lv.id = ad.level_department','left')
			->order_by($order)
                        ->get()->result_array($rex_index);
    }
}
