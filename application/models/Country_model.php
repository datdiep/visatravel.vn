<?php

class Country_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'country';
    }

    public function get_all_alphabet($order = null, $rex_index = '') {

        $this->db->select("distinct SUBSTRING(name,1,1) AS letter", FALSE);
        if ($order !== null) {
            $this->db->order_by($order);
        }
        return $this->db->get($this->table_name)->result_array($rex_index);
    }
    

}
