<?php

/**
 * @property CI_DB_active_record $db
 */
class Keyword_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function update_or_add($keyword) {
        $this->db->set('count', 'count + 1', false)
                ->where('keyword', $keyword)
                ->update('tbl_keyword');
        $affect = $this->db->affected_rows();
        if ($affect == 0) {
            $this->db->insert('tbl_keyword', array('keyword' => $keyword));
        }
    }

    function get_rand() {
        return $this->db->select('keyword')->order_by('RAND()')->limit(10)
                        ->get('tbl_keyword')->result_array();
    }

    function get_keyword($limit = 10) {
        return $this->db->select('keyword')->order_by('count DESC')->limit($limit)
                        ->get('tbl_keyword')->result_array();
    }
    function get_keyword_home($limit = 3) {
        return $this->db->select('keyword')->order_by('count DESC')->limit($limit)
                        ->get('tbl_keyword')->result_array();
    }

}
