<?php

/**
 * @property CI_DB_active_record $db
 */
class Banner_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function get_all_banner() {
      return  $this->db->select()->from('tbl_banner')
                ->get()->result_array('id');
    }

}
