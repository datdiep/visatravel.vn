<?php

class Page_Model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'page';
    }

    function count_news($cond = null, $type = 'news') {
        if ($cond != null) {
            $this->db->where($cond);
        }
        $data = $this->db->select('count(id) as count')
                        ->where('type ="' . $type . '"')->get($this->table_name)->row_array();
        return $data['count'];
    }

    function get_by_pos($pos = 0, $limit = 10, $cond = null, $order = 'id DESC', $type = 'news') {
        if ($cond != null) {
            $this->db->where($cond);
        }
        return $this->db->select('id,title,alias,publish_date,image,descript')->where('type ="' . $type . '"')->from($this->table_name)
                        ->order_by($order)->limit($limit, $pos)->get()->result_array();
    }

}
