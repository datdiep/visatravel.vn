<?php

class Department_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'department';
    }
}
