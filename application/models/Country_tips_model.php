<?php

class Country_tips_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'country_tips';
    }

    function get_site_maps($cond = null) {
        if ($cond != null) {
            $this->db->where($cond);
        }
        return $this->db->select('id,alias,country_id')->order_by('id DESC')
                        ->get($this->table_name)->result_array();
    }

}
