<?php

class Order_services_Model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'order_services';
    }

    function get_count() {
        $data = $this->db->select('sum(count) as count,product_id as id')
                        ->from('sku')->group_by('product_id')
                        ->get()->result_array();
        @$this->db->update_batch('product', $data, 'id');
    }

    function set_total_order($order_id) {
        $data = $this->db->select('sum(price*count) as total')
                        ->where('order_id = ' . $order_id)
                        ->get('order_product')->row_array();
        $total = intval(@$data['total']);
        $this->db->update('order', array('total' => $total), array('id' => $order_id));
    }

    function get_count_by_status($cond) {
        return $this->db->select('count(id) as count')
                        ->where($cond)->get($this->table_name)->row_array();
    }

    function get_report_customer($phone, $order_id) {
        return $this->db->select('count(id) as total,status')
                        ->where('phone = "' . $phone . '" and id != ' . $order_id)
                        ->group_by('status')
                        ->get($this->table_name)
                        ->result_array();
    }

}
