<?php

/**
 * @property CI_DB_active_record $db
 */
class Tags_model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'tags';
    }

}
