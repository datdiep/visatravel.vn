<?php

class Category_Product_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'tbl_category_product';
    }

    function get_cat_new($type) {
        return $this->db->get_where('category_news', array('type' => $type))->result_array();
    }
    
    function get_cat_news_by_id($id) {
        return $this->db->get_where('category_news', array('id' => $id))->row_array();
    }
    
    function get_cat_visa_by_id($id,$type='visa') {
        return $this->db->get_where('category_news', array('id' => $id,'type' => $type))->row_array();
    }

}
