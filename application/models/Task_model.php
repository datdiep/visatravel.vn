<?php

class Task_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'task_list';
    }

    function get_for_search($cond) {
        $this->db->select('id,title,alias,img');
        if ($cond != null) {
            $this->db->where($cond);
        }
        return $this->db->get($this->table_name)->result_array();
    }

}
