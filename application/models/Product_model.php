<?php

class Product_Model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'tbl_product';
    }

    function get_by_pos($pos = 0, $limit = 10, $order = "id DESC", $cond = null) {
        if ($cond != null) {
            $this->db->where($cond);
        }
        return $this->db->from('tbl_product')
                        ->order_by($order)->limit($limit, $pos)->get()->result_array();
    }

    function get_page_cat($cat_id, $pos = 0, $limit = 10, $order = "id DESC") {
        return $this->db->select()->from('tbl_product')
                        ->where('is_hide = 0 and cat_id LIKE "' . $cat_id . '%"')
                        ->order_by($order)->limit($limit, $pos)->get()->result_array();
    }

    function get_discount($limit = 3) {
        return $this->db->select($this->seleted)->from('tbl_product as p')
                        ->join('tbl_category_product c', 'p.cat_id = c.parent')
                        ->order_by('p.discount ASC')->limit($limit)->get()->result_array();
    }

    function get_image($id) {
        return $this->db->select()->from('tbl_img')
                        ->where('product_id = ' . $id)
                        ->order_by('id')->get()->result_array();
    }

//    function get_site_maps() {
//        return $this->db->select('p.id,p.alias,c.alias as cat_alias')->from('tbl_product as p')
//                        ->join('tbl_category_product as c', 'p.cat_id = c.parent')
//                        ->order_by('id DESC')->get()->result_array();
//    }
//    
    
}
