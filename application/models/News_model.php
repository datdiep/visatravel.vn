<?php

/**
 * @property News_Model $news_model
 */
class News_Model extends My_Model {

    public function __construct() {
        parent::__construct();
        $this->table_name = 'news';
    }

}
