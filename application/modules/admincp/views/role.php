<div class="modal fade bs-example-modal-lg" id="role" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">
                    <i class="fa fa-cog"></i>TẠO QUYỀN CHO HỆ THỐNG
                </h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
		    <input type='hidden' class="form-control" name="id" value="<?php echo @$role['id']; ?>"/>
                    <div class="form-group col-md-4 ">
                        <label for="name">Tên quyền</label>
                        <input type="text" class="form-control valid" name="name" placeholder="Nhập tên" value="<?php echo @$role['name']; ?>">
                    </div>

                    <div class="form-group col-md-4 ">
                        <label for="controller">Controller</label>
                        <input type="text" class="form-control valid" name="controller" placeholder="Nhập controller" value="<?php echo @$role['controller']; ?>">
                    </div>

                    <div class="form-group col-md-4 ">
                        <label for="type">Phân loại</label>
                        <select name="type" class="form-control">
                            <?php foreach ($map_type as $k => $item): ?>
				<option <?php echo $k == @$role['type'] ? "selected":"";?> value="<?php echo $k; ?>"> -- <?php echo $item; ?> -- </option>
			    <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="description">Mô tả</label>
                        <input type='text' class="form-control" name="description" value="<?php echo @$role['description']; ?>"/>
                    </div>



                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-primary save_role"><i class="fa fa-save"></i> Lưu lại</p>
                <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
            </div>
        </div>
    </div> 
</div>