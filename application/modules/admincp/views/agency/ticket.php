<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="col-md-8">
            <div class="row">
                <div class="box box-primary" id="list_ajax">
                </div>
                <?php if ($check_error == 0): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                        Câu hỏi của bạn đã được gửi. Chúng tôi sẽ trả lời bạn trong thời gian sớm nhất!
                    </div>
                <?php endif; ?>
                <?php if ($check_error == 1): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo validation_errors(); ?>                          
                    </div>
                <?php endif; ?>

                <!-- quick email widget -->
                <div class="box box-info">
                    <div class="box-header">
                        <i class="fa fa-envelope"></i>
                        <h3 class="box-title">Gửi câu hỏi</h3>
                        <!-- /. tools -->
                    </div>
                    <form id="form" method="post" enctype="multipart/form-data">
                        <div class="box-body">

                            <div class="form-group">
                                <select name="room" class="form-control valid">
                                    <option value="">Vui lòng chọn phòng ban muốn gửi hỗ trợ</option>
                                    <?php foreach ($ticket_room as $key => $v): ?>
                                        <option value="<?php echo $key; ?>"><?php echo $v; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control" name="subject" placeholder="Tiêu đề" value="<?php if ($check_error == 1) echo @$_POST['subject']; ?>">
                            </div>

                            <div class="form-group">
                                <p><i class="fa fa-file-pdf-o"></i> <b> File đính kèm</b></p>
                                <input type="file" accept="image/*,application/pdf,application/zip,application/x-zip,application/x-zip-compressed,application/octet-stream,application/x-rar-compressed,compressed/rar,application/x-rar,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template" id="images" name="images[]" multiple />
                            </div>

                            <div>
                                <textarea name="content" id="content" placeholder="Nội dung muốn gửi" style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php if ($check_error == 1) echo @$_POST['content']; ?></textarea>
                            </div>

                        </div>
                        <div class="box-footer clearfix">
                            <button type="submit" name="submit" class="pull-right btn btn-default" id="sendEmail">Send
                                <i class="fa fa-arrow-circle-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            asdas
        </div>
    </section>
</div>

<script>

    $(".select2").select2();
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    var current_pos = <?php echo $pos; ?>;


    function del(id) {
        show_dialog('Bạn có muốn xóa hỏi đáp id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>agency/ticket_del', {id: id}, function (results) {
                //location.reload();
                loadlist(current_pos);
            });
        });
    }


    function loadlist(pos) {
        current_pos = pos;
        $.post('/<?php echo ADMIN_URL; ?>agency/ticket_list', {pos: pos}, function (results) {
            console.log('aaaaaaaaaaaaa');
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });


</script>