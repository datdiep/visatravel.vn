<?php $this->load->library('transload'); ?>


<div class="box-header">
    <i class="ion ion-clipboard"></i>

    <h3 class="box-title">Danh sách câu hỏi</h3>

    <div class="box-tools pull-right">
        <ul class="pagination pagination-sm inline">
            <?php echo $links; ?>
        </ul>
    </div>
</div>

<div class="box-body">
    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->

    <ul class="todo-list">
        <?php foreach ($results as $item): ?>
            <li>
                <!-- drag handle -->
                <span class="handle">
                    <i class="fa fa-gg"></i>
                </span>

                <!-- todo text -->
                <span class="text" style="text-transform: capitalize"><?php echo $item['subject']; ?></span>
                <!-- Emphasis label -->
                <small class="label label-default text-11"><i class="fa fa-clock-o"></i> <?php echo $this->transload->ago(strtotime($item['date_create']), 0) . ' trước'; ?></small>

                <!-- General tools such as edit or delete-->
                <div class="tools">
                    <?php if (($item['author_username'] == $this->data['user']['username'] && $item['network_id'] == $this->data['user']['network_id']) || !empty($this->data['user']['role_department'])): ?>
                        <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i></a>
                        <?php endif; ?>
                </div>


            </li>
        <?php endforeach; ?>

    </ul>
</div>


