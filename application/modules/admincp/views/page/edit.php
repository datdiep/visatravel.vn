<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT PAGE</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>page">List page</a></li>
            <li class="active">Edit Page</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                            Sửa thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="col-md-6 box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Basic info</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="<?php echo $page['title']; ?>" class="form-control"  name="title" placeholder="Enter Title">
                            </div>

                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" value="<?php echo $page['alias']; ?>" class="form-control" name="alias" placeholder="Enter Alias">
                            </div>



                            <div class="form-group">
                                <label for="alias">Loại page</label>
                                <select name="type" class="form-control valid">
                                    <?php foreach ($type as $value): ?>
                                        <option <?php echo $page['type'] == $value ? 'selected' : ''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title" value="<?php echo $page['seo_title'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description" value="<?php echo $page['meta_description'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword" value="<?php echo $page['meta_keyword'] ?>">
                            </div>
                        </div>    
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin thêm (ko bắt buộc)</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="headline1">Tiêu đề lớn (Slide)</label>
                                <input type="text" class="form-control"  name="headline1" value="<?php echo $page['headline1']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="headline2">Tiêu đề nhỏ (Slide)</label>
                                <input type="text" class="form-control"  name="headline2" value="<?php echo $page['headline2']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="image">Hình ảnh (<label class="label_slider label_gallery">1583 x 300</label>
                                    <label class="label_banner label_gallery" style="display: none;">1100 x 150</label>)</label>
                                <input type="file" class="form-control"  name="image" >
                                <?php if ($page['image']): ?>
                                    <br/>
                                    <img src="/assets/upload/page/<?php echo $page['image']; ?>" width="200"/>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" value="<?php echo $page['tags'] ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Content</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"><?php echo $page['content']; ?></textarea>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>