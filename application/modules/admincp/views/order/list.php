<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
		    <th>Ngày tạo</th>
		    <th>Khách hàng</th>
		    <th>Tư vấn</th>
		    <?php if (!empty($user['role_network'])): ?>
                        <th>Tổng tiền</th>
                    <?php endif; ?>
                    <th>Dịch vụ</th>
		    <th>Ghi chú</th>
                    <th>Tiến trình</th>
		    <th>Trạng thái</th>
                    <th style="text-align: center">Thực thi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo date('d/m/Y', strtotime($item['date_create'])); ?></td>
                        <td><a href="/admincp/order/process/<?php echo $item['id']; ?>"><?php echo $item['full_name']; ?></a></td>                     
                        <td><?php echo $item['who_consultant']; ?></td>                       
                        <?php if (!empty($user['role_network'])): ?>
                            <td><?php echo number_format($item['total']); ?></td>
                        <?php endif; ?>
                        <td>
                            <?php echo $item['service_name']; ?>
                        </td>
                        <td><?php echo nl2br($item['note']); ?></td>
			<td>
			    <?php if(!empty($item['progress'])):?>
			    <?php $progress = @unserialize($item['progress']); $last = end($progress);?>
			    <?php echo date('d/m/Y', strtotime($last['date_create']));?> - <?php echo $last['content']; ?>
			    <?php endif;?>
                        </td>
			 <td>
                            <?php echo $order_status[$item['status']]; ?>
                        </td>
                        <td style="text-align: center">
                            <?php if ($item['status'] == 'trash'): ?>
                                <a  class="btn btn-default" onclick="undo(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-undo"></i> Undo</a>
				<?php if (!empty($user['role_network'])): ?>
                                <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
				<?php endif; ?>
                            <?php else:?>
                                <a  class="btn btn-default" href="/admincp/order/process/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i></a>
                                <a  class="btn btn-default" onclick="trash(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>      


        </table>

    </div>
    <div class="col-md-4 col-md-offset-5">
        <ul class="pagination">
            <?php echo $links; ?>
        </ul>
    </div>

</div>

<style>
    #example2 th{white-space: nowrap}
</style>