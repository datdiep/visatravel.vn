<div class="modal fade bs-example-modal-lg" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
            <div class="modal-header">
		<div class="pull-right">
		    <p class="btn btn-info save_profile" next="new" rel_alert="alert_profile"><i class="fa fa-save"></i> Lưu & thêm mới</p>
		    <p class="btn btn-primary save_profile" next="close" rel_alert="alert_profile_main"><i class="fa fa-save"></i> Lưu & đóng</p>
		    <?php if($profile['id'] > 0):?>
		    <p class="btn btn-danger save_profile" next="update" rel_alert="alert_profile"><i class="fa fa-upload"></i> Cập nhật</p>
		    <?php endif;?>
		    <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
		</div>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user"></i> Thêm hồ sơ mới</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
		    <div class="alert alert-success alert-dismissable alert_profile" style="display:none"></div>
		    <input type="hidden" name="id" value="<?php echo $profile['id'];?>">
		    <div class="form-group col-md-4">
			<label for="full_name">Họ và tên</label>
			<input type="text" uppercase class="order-input form-control" name="ho_ten" placeholder="Tên khách hàng" value="<?php echo @$profile['ho_ten'];?>">
		    </div>
		    <div class="form-group col-md-4">
			<label for="phone">Số điện thoại</label>
			<input type="text" value="<?php echo @$profile['so_dien_thoai'];?>" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" class="order-input form-control" name="so_dien_thoai" placeholder="Số điện thoại khách hàng">
		    </div>
		      <div class="form-group col-md-4">
			<label for="email">Quốc tịch</label>
			<select name="quoc_tich" class="order-input form-control">
			    <option <?php echo @$profile['quoc_tich'] == 'Việt Nam' ? 'selected':''?> value="Việt Nam">Việt Nam</option>
			    <option <?php echo @$profile['quoc_tich'] == 'Hàn Quóc' ? 'selected':''?> value="Hàn Quóc">Hàn Quóc</option>

			</select>
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Giới tính</label>
			<select name="gioi_tinh" class="order-input form-control">
			    <option <?php echo @$profile['gioi_tinh'] == 'female' ? 'selected':''?> value="female">Nữ</option>
			    <option <?php echo @$profile['gioi_tinh'] == 'male' ? 'selected':''?> value="male">Nam</option>

			</select>
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Ngày sinh</label>
			<div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
			    <input type="text" class="form-control" name="ngay_sinh" value="<?php echo !empty($profile['ngay_sinh']) ? format_date($profile['ngay_sinh'],'d/m/Y'):''; ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                </div>
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Nơi sinh</label>
			<input type="text" uppercase value="<?php echo @$profile['noi_sinh'];?>" class="order-input form-control" name="noi_sinh" placeholder="Nhập email liên hệ của khách">
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Số CMND</label>
			<input type="text" value="<?php echo @$profile['cmnd'];?>" class="order-input form-control" name="cmnd" placeholder="Nhập email liên hệ của khách">
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Ngày cấp</label>
			<div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="ngay_cap_cmnd" value="<?php echo !empty($profile['ngay_cap_cmnd']) ? format_date($profile['ngay_cap_cmnd'],'d/m/Y'):''; ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                </div>
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Nơi cấp</label>
			<input type="text" uppercase value="<?php echo @$profile['noi_cap_cmnd'];?>" class="order-input form-control" name="noi_cap_cmnd" placeholder="Nhập email liên hệ của khách">
		    </div>

		  
		    <hr class="style3">
		    <div class="form-group col-md-6">
			<label for="address">Địa chỉ thường trú</label>
			<input type="text" value="<?php echo @$profile['dia_chi_thuong_tru'];?>" class="order-input form-control" name="dia_chi_thuong_tru" placeholder="Địa chỉ khách hàng">
		    </div>
		    <div class="form-group col-md-6">
			<label for="address">Địa chỉ tạm trú</label>
			<input type="text" value="<?php echo @$profile['dia_chi_tam_tru'];?>" class="order-input form-control" name="dia_chi_tam_tru" placeholder="Địa chỉ khách hàng">
		    </div>
		     <div class="form-group col-md-6">
			<label for="company">Nghề nghiệp/ Chức vụ</label>
			<input type="text" value="<?php echo @$profile['nghe_nghiep'];?>" class="order-input form-control" name="nghe_nghiep">

		    </div>
		    <div class="form-group col-md-6">
			<label for="address">Mã số thuế</label>
			<input type="text" value="<?php echo @$profile['ma_so_thue'];?>" class="order-input form-control" name="ma_so_thue" placeholder="Địa chỉ khách hàng">
		    </div>
		    <hr class="style3">
		    <div class="form-group col-md-4">
			<label for="email">Số hộ chiếu</label>
			<input type="text" uppercase value="<?php echo @$profile['so_ho_chieu'];?>" class="order-input form-control" name="so_ho_chieu" placeholder="Nhập email liên hệ của khách">
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Ngày cấp</label>
			<div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="ngay_cap_ho_chieu" value="<?php echo !empty($profile['ngay_cap_ho_chieu']) ? format_date($profile['ngay_cap_ho_chieu'],'d/m/Y'):''; ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                </div>
		    </div>
		    <div class="form-group col-md-4">
			<label for="email">Ngày hết hạn</label>
			<div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="ngay_het_han_ho_chieu" value="<?php echo !empty($profile['ngay_het_han_ho_chieu']) ? format_date($profile['ngay_het_han_ho_chieu'],'d/m/Y'):''; ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
                                </div>
		    </div>
		    <hr class="style3">
		    <?php if($profile['id'] > 0):?>
			<?php
			    $visa_hien_co = unserialize($profile['visa_hien_co']);
			    $thong_tin_gia_dinh = unserialize($profile['thong_tin_gia_dinh']);
			    $danh_sach_dich_vu = unserialize($profile['danh_sach_dich_vu']);
			?>
			<div class="form-group col-md-12">
			<label><i class="fa fa-cc-visa"></i> VISA HIỆN CÓ</label>
			<table class="table table-bordered profile_table">
			    <tbody>
				<tr class="bg-gray-light">
				    <th>Visa</th>
				    <th>Mã visa</th>
				    <th>Ngày cấp</th>
				    <th>Ngày hết hạn</th>
				    <th></th>
				</tr>
				<?php foreach ($visa_hien_co as $vs): ?>
				<tr>

				    <td>
					<input name="visa" value="<?php echo $vs['visa']; ?>" field_name="visa_hien_co" type="text">
				    </td>
				    <td><input name="ma_visa" value="<?php echo $vs['ma_visa']; ?>" uppercase field_name="visa_hien_co" type="text"></td>
				    <td><input name="ngay_cap" value="<?php echo $vs['ngay_cap']; ?>" field_name="visa_hien_co" type="text" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="ngay_het_han" value="<?php echo $vs['ngay_het_han']; ?>" field_name="visa_hien_co" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" class="add_tr"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>
				<?php endforeach; ?>
			    </tbody></table>

		    </div>
		
		    <div class="form-group col-md-12">
			<label><i class="fa fa-users"></i> THÔNG TIN GIA ĐÌNH</label>
			<table class="table table-bordered profile_table">
			    <tbody><tr class="bg-gray-light">
<!--                                        <th style="width: 10px">#</th>-->
				    <th>Họ tên</th>
				    <th>Ngày sinh</th>
				    <th>Mối Quan hệ</th>
				    <th></th>
				</tr>
				<?php foreach ($thong_tin_gia_dinh as $gd): ?>
				<tr>
		
				    <td><input name="ho_ten_gia_dinh" uppercase field_name="thong_tin_gia_dinh" type="text" value="<?php echo $gd['ho_ten_gia_dinh']; ?>"></td>
				    <td><input name="ngay_sinh"  value="<?php echo $gd['ngay_sinh']; ?>" field_name="thong_tin_gia_dinh" type="text" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="moi_quan_he"  value="<?php echo $gd['moi_quan_he']; ?>" field_name="thong_tin_gia_dinh"  class="add_tr" type="text"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>
				<?php endforeach; ?>
			    </tbody></table>

		    </div>
		 
		    <div class="form-group col-md-12">
			<label class="text-red"><i class="fa fa-plus"></i> DANH SÁCH DỊCH VỤ</label>
			<table class="table table-bordered profile_table">
			    <tbody><tr class="bg-gray-light">
<!--                                        <th style="width: 10px">#</th>-->
				    <th width="150">Dịch vụ</th>
				    <th>Ngày làm</th>
				    <th>Ngày hoàn thành</th>
				    <th>Trạng thái</th>
				    <th>Ghi chú</th>
				    <th></th>
				</tr>
				<?php foreach ($danh_sach_dich_vu as $dv): ?>
				<tr>
				     <td>
					<select name="dich_vu" class="order-input" field_name="danh_sach_dich_vu">
				    <?php foreach ($services as $v) : ?>
				    <option <?php echo @$dv['dich_vu'] == $v['name'] ? 'selected':''?> value="<?php echo $v['name'];?>"><?php echo $v['name'];?></option>
				    <?php endforeach;?>
				</select>
				    </td>
				    <td>
					<input name="ngay_lam" value="<?php echo $dv['ngay_lam'];?>" type="text" field_name="danh_sach_dich_vu" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
				    </td>
				    <td><input name="ngay_hoan_thanh" type="text" value="<?php echo $dv['ngay_hoan_thanh'];?>" field_name="danh_sach_dich_vu" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="trang_thai" value="<?php echo $dv['trang_thai'];?>" field_name="danh_sach_dich_vu" type="text" class=""></td>
				    <td><input name="ghi_chu" class="add_tr" value="<?php echo $dv['ghi_chu'];?>" field_name="danh_sach_dich_vu" type="text"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>
				<?php endforeach; ?>
			    </tbody></table>

		    </div>
		    <?php else:?>
		    <div class="form-group col-md-12">
			<label><i class="fa fa-cc-visa"></i> VISA HIỆN CÓ</label>
			<table class="table table-bordered profile_table">
			    <tbody>
				<tr class="bg-gray-light">
<!--                                        <th style="width: 10px">#</th>-->
				    <th>Visa</th>
				    <th>Mã visa</th>
				    <th>Ngày cấp</th>
				    <th>Ngày hết hạn</th>
				    <th></th>
				</tr>
				<tr>

				    <td>
					<input name="visa" field_name="visa_hien_co" type="text" class="">
				    </td>
				    <td><input name="ma_visa" uppercase field_name="visa_hien_co" type="text" class=""></td>
				    <td><input name="ngay_cap" field_name="visa_hien_co" type="text" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="ngay_het_han" field_name="visa_hien_co" type="text" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" class="add_tr"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>

			    </tbody></table>

		    </div>
		
		    <div class="form-group col-md-12">
			<label><i class="fa fa-users"></i> THÔNG TIN GIA ĐÌNH</label>
			<table class="table table-bordered profile_table">
			    <tbody><tr class="bg-gray-light">
<!--                                        <th style="width: 10px">#</th>-->
				    <th>Họ tên</th>
				    <th>Ngày sinh</th>
				    <th>Mối Quan hệ</th>
				    <th></th>
				</tr>
				<tr>
		
				    <td><input name="ho_ten_gia_dinh" uppercase field_name="thong_tin_gia_dinh" type="text" class=""></td>
				    <td><input name="ngay_sinh" field_name="thong_tin_gia_dinh" type="text" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="moi_quan_he" field_name="thong_tin_gia_dinh"  class="add_tr" type="text"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>
			    </tbody></table>

		    </div>
		 
		    <div class="form-group col-md-12">
			<label class="text-red"><i class="fa fa-plus"></i> DANH SÁCH DỊCH VỤ</label>
			<table class="table table-bordered profile_table">
			    <tbody><tr class="bg-gray-light">
<!--                                        <th style="width: 10px">#</th>-->
				    <th width="150">Dịch vụ</th>
				    <th>Ngày làm</th>
				    <th>Ngày hoàn thành</th>
				    <th>Trạng thái</th>
				    <th>Ghi chú</th>
				    <th></th>
				</tr>
				<tr class="data_row">
				     <td>
					<select name="dich_vu" class="order-input" field_name="danh_sach_dich_vu" >
				    <?php foreach ($services as $v) : ?>
				    <option value="<?php echo $v['name'];?>"><?php echo $v['name'];?></option>
				    <?php endforeach;?>
				</select>
				    </td>
				    <td>
					<input name="ngay_lam" value="<?php echo date('d/m/Y');?>" type="text" field_name="danh_sach_dich_vu" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="">
				    </td>
				    <td><input name="ngay_hoan_thanh" type="text"  field_name="danh_sach_dich_vu" class="" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask=""></td>
				    <td><input name="trang_thai"  field_name="danh_sach_dich_vu" type="text" class=""></td>
				    <td><input name="chi_chu" class="add_tr" field_name="danh_sach_dich_vu" type="text"></td>
				    <td>
					<i class="text-muted fa fa-trash-o"></i>
				    </td>
				</tr>
	
			    </tbody></table>

		    </div>
		    <?php endif;?>
		    

		    <div class="form-group col-md-12">
			<label for="leave_message">Ghi chú hồ sơ</label>
			<textarea name="ghi_chu" style="height: 110px" class="order-input form-control"><?php echo @$profile['ghi_chu'];?></textarea>
		    </div>
		    
		</div>
            </div>

            <div class="modal-footer">
		<p class="btn btn-info save_profile" next="new" rel_alert="alert_profile"><i class="fa fa-save"></i> Lưu & thêm mới</p>
		<p class="btn btn-primary save_profile" next="close" rel_alert="alert_profile_main"><i class="fa fa-save"></i> Lưu & đóng</p>
		<?php if($profile['id'] > 0):?>
		<p class="btn btn-danger save_profile" next="update" rel_alert="alert_profile"><i class="fa fa-upload"></i> Cập nhật</p>
		<?php endif;?>
                <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
            </div>
        </div>
    </div> 
</div>