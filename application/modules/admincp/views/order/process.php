<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM HỒ SƠ XỬ LÝ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>customer">Quản lý khách hàng</a></li>
            <li class="active">thêm khách hàng</li>
        </ol>

    </section>    
    <section class="content">

            <div class="row">
                <div class="col-md-12 margin-bottom">
                    <div class="box-footer">
                        <button class="save_order btn btn-info margin-r-5" rel_alert="alert_order">Lưu khách hàng</button>
                         <button type="button" class="btn btn-primary margin-r-5">
                                <i class="fa fa-question-circle"></i> Xem hướng dẫn
                            </button>
                        <!--<button type="submit" value="1" name="submit" class="btn btn-success">Lưu & In hóa đơn</button>-->
                        <small class="pull-right">
                           
                            <?php if($order['id'] != 0):?>
                            <a href="invoice-print.html" target="_blank" class="btn btn-default margin-r-5"><i class="fa fa-print"></i> In hồ sơ</a>
                            <button type="button" class="btn btn-warning btn_profile">
                                <i class="fa fa-plus"  title="Thêm hồ sơ"></i> Thêm hồ sơ
                            </button>
                            <?php endif;?>
                        </small>

                    </div>
                </div>
                <div class="col-md-12">
		    <div class="alert alert-success alert-dismissable alert_order" <?php echo empty($msg_new_order) ? 'style="display:none"' : ''; ?>>
			<?php echo $msg_new_order; ?>
		    </div>
                    <div class="box box-primary box-primary <?php echo ($order['id'] != 0 && empty($msg_new_order)) ? 'collapsed-box':'';?>">
                        <div class="box-header with-border">
                            <h3 class="box-title"><b> Thông tin khách hàng</b></h3>
                            <div class="box-tools">
                                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-calendar-check-o"></i> Lịch hẹn</button>
                                <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-<?php echo!empty($task) ? 'plus' : 'minus'; ?>"></i></button>
                            </div>
                        </div>

                        <div class="box-body">
                            <div class="form-group col-md-4">
                                <label for="full_name">Tên khách hàng</label>
                                <input type="text" class="order-input form-control" name="full_name" placeholder="Tên khách hàng" value="<?php echo @$order['full_name'];?>">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" value="<?php echo @$order['phone'];?>" onkeyup="if (/\D/g.test(this.value))this.value = this.value.replace(/\D/g, '')" class="order-input form-control" name="phone" placeholder="Số điện thoại khách hàng">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Email</label>
                                <input type="text" value="<?php echo @$order['email'];?>" class="order-input form-control"  name="email" placeholder="Nhập email liên hệ của khách">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="address">Địa chỉ</label>
                                <input type="text" value="<?php echo @$order['address'];?>" class="order-input form-control" name="address" placeholder="Địa chỉ khách hàng">
                            </div>

                            <div class="form-group col-md-4">
                                <label for="company">Tên Cty/ Tổ chức</label>
                                <div class="input-group">
                                    <input type='text' value="<?php echo @$order['company'];?>" class="order-input form-control" name="company"/>
                                    <label class="input-group-addon btn" for="company">
                                        <span class="fa fa-home"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="vat">Mã số thuế</label>
                                <div class="input-group">
                                    <input type='text' value="<?php echo @$order['tax_code'];?>" class="order-input form-control" name="tax_code"/>
                                    <label class="input-group-addon btn" for="tax_code">
                                        <span class="fa fa-line-chart"></span>
                                    </label>
                                </div>
                            </div>



                        </div> 

                    </div>
                </div>
                <div class="col-md-6">   

                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-cc-visa"></i>
                            <h3 class="box-title">Tiến trình đơn hàng</h3>
                        </div>
                        <div class="box-body">
			    <div class="form-group col-md-4 nopadding">
                                <label for="who_consultant">NV tư vấn</label>
                                <?php if($order['id'] != 0):?>
				<input type='text' value="<?php echo $order['who_consultant'];?>" disabled class="form-control"/>
                                <?php else:?>
                                    <select name="who_consultant" class="order-input form-control">
                                        <?php foreach ($staff as $value): ?>
                                            <option <?php echo $user['username'] == $value['username'] ? 'selected':''?> value="<?php echo $value['username']; ?>"><?php echo $value['fullname']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif;?>
                            </div>
			    <div class="form-group col-md-4 nopadding">
                                <label for="who_consultant">Dịch vụ chính</label>
                                <select name="service_id" class="order-input form-control">
				    <?php foreach ($services as $v) : ?>
				    <option <?php echo @$order['service_id'] == $v['id'] ? 'selected':''?> value="<?php echo $v['id'];?>"><?php echo $v['name'];?></option>
				    <?php endforeach;?>
				</select>
				<input class="order-input" type="hidden" name="service_name">
                            </div>
                            
                            <div class="form-group col-md-4 nopadding">
                                <label for="status">Trạng thái</label>
                                <select name="status" class="order-input form-control">
                                    <?php foreach ($order_status as $k => $v) : ?>
                                        <option <?php echo @$order['status'] == $k ? 'selected' : ''; ?> value="<?php echo $k; ?>">-- <?php echo $v; ?> --</option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-12 nopadding">
                                <label for="leave_message">Ghi chú đơn hàng</label>
                                <textarea name="note" style="width: 100%; height: 110px" class="order-input form-control"><?php echo @$order['note'];?></textarea>
                            </div>
                        </div>
                        <?php if($order['id'] != 0):?>
                        <div class="box-footer box-comments">
			    <?php if(!empty($order['progress'])):?>
			    <?php $progress = @unserialize($order['progress']); $len = count($progress) - 1;?>
			    <?php foreach ($progress as $k => $v):?>
			    <div class="box-comment">
                                <div class="col-md-12">
				    <span class="username <?php echo $len == $k ? 'text-red':'';?>">
                                        <?php echo date('d/m/Y', strtotime($v['date_create']));?> - <?php echo $v['username'];?>:
                                    </span> 
                                    <?php echo $v['content'];?>
                                </div>

                            </div>
			    <?php endforeach;?>
			    <?php else:?>
			    <div class="box-comment">
                                <div class="col-md-12">
				    <span class="username text-red">
                                        <?php echo date('d/m/Y');?> - Sailormoon:
                                    </span> 
                                    Vui lòng thường xuyền cập nhật tiến trình đầy đủ giúp em nhé. Nếu không biết có thể anh Duy
                                </div>

                            </div>
			    <?php endif;?>
                        </div>
                        <div class="box-footer">
                            <form action="#" method="post">
                                <img class="img-responsive img-circle img-sm" src="/assets/upload/avatar/datdiep.jpg" alt="Alt Text">
                                <!-- .img-push is used to add margin to elements next to floating images -->
                                <div class="img-push">
                <!--                  <input type="text" class="form-control input-sm" placeholder="Press enter to post comment">-->
                                    <div class="input-group">
                                        <input class="form-control" id="progress_update" placeholder="Type message...">

                                        <div class="input-group-btn">
                                            <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <?php endif;?>
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="box" id="list_cost">
                        <div class="box-header with-border">
                            <i class="fa fa-usd"></i>
                            <h3 class="box-title">Chi tiết thanh toán</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="vat">Tổng tiền (VNĐ)</label>
                                <div class="input-group">
                                    <input type='text' value="<?php echo @$order['total'];?>" class="order-input form-control format_number" name="total"/>
                                    <label class="input-group-addon btn" for="total">
                                        <span class="fa fa-dollar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="vat">Còn lại</label>
                                <div class="input-group">
                                    <input disabled type='text' class="order-input con_lai form-control format_number" name="arrears"/>
                                    <label class="input-group-addon btn">
                                        <span class="fa fa-dollar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="vat">Đã thanh toán</label>
                                <div class="input-group">
                                    <input disabled type='text' class="da_thanh_toan form-control format_number"/>
                                    <label class="input-group-addon btn">
                                        <span class="fa fa-money"></span>
                                    </label>
                                </div>
                            </div>
                            <table class="transaction table table-bordered">
                                <tbody><tr>
<!--                                        <th style="width: 10px">#</th>-->
                                        <th>Loại</th>
                                        <th>Mô tả dịch vụ</th>
                                        <th>S/L</th>
                                        <th>Ghi chú</th>
                                        <th>Số tiền</th>
                                        <th></th>
                                    </tr>
                                    <?php if(!empty($transaction)):?>
                                        <?php foreach ($transaction as $item):?>
                                        <tr class="transaction_row" data_id="<?php echo $item['id'];?>">
                                            <td class="hd_loai">
                                                <select class="transaction_data" name="type">
                                                    <option <?php echo $item['type'] == 'Thu' ? 'selected':''?> value="Thu">Thu</option>
                                                    <option <?php echo $item['type'] == 'Chi' ? 'selected':''?> value="Chi">Chi</option>
                                                </select>
                                            </td>
                                            <td class="hd_md">
                                                <select class="transaction_data" name="description">
                                                    <?php foreach ($services as $v) : ?>
                                                    <option <?php echo $item['description'] == $v['alias'] ? 'selected':''?> value="<?php echo $v['alias'];?>"><?php echo $v['name'];?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </td>
                                            <td class="hd_sl" style="width:50px"><input value="<?php echo $item['quantity'];?>" name="quantity" type="number" class="transaction_data"></td>
                                            <td class="hd_gc"><input value="<?php echo $item['note'];?>" name="note" type="text" class="transaction_data"></td>
                                            <td class="hd_st" style="width:110px"><input value="<?php echo $item['money'];?>" name="money" class="transaction_data format_number" type="text" rel="Thu"></td>
                                            <td>
						<?php if($item['un_lock'] == 0):?>
						    <i class="text-muted fa fa-save"></i>
						    <i class="text-muted fa fa-trash-o"></i>
						    <?php if(!empty($user['role_network'])):?>
							<i class="text-muted fa fa-unlock toggle-lock"></i>
						    <?php endif;?>
						<?php else:?>
						    <i class="text-muted fa fa-lock <?php echo !empty($user['role_network']) ? "toggle-lock":"";?>"></i>
						<?php endif;?>
						
					    </td>
                                        </tr>
                                        <?php endforeach;?>
                                    <?php else:?>
                                    <tr class="transaction_row">
                                        <td class="hd_loai">
                                            <select class="transaction_data" name="type">
                                                <option value="Thu">Thu</option>
                                                <option value="Chi">Chi</option>
                                            </select>
                                        </td>
                                        <td class="hd_md">
                                            <select class="transaction_data" name="description">
                                                <?php foreach ($services as $v) : ?>
                                                <option value="<?php echo $v['alias'];?>"><?php echo $v['name'];?></option>
                                                <?php endforeach;?>
                                            </select>
                                        </td>
                                        <td class="hd_sl" style="width:50px"><input name="quantity" type="number" class="transaction_data"></td>
                                        <td class="hd_gc"><input name="note" type="text" class="transaction_data"></td>
                                        <td class="hd_st" style="width:110px"><input name="money" class="transaction_data format_number" type="text" rel="Thu"></td>
                                        <td>
					    <?php if($order['id'] > 0):?>
						<i class="text-muted fa fa-save text-red"></i>
					    <?php endif;?>
					    <i class="text-muted fa fa-trash-o"></i>
					</td>
                                    </tr>
                                    <?php endif;?>
                                </tbody></table>
                            
                        </div>
                    </div>
		
                    <?php echo @$load_chat; ?>

              

                </div>
		<div class="col-md-12"><div class="alert alert-success alert-dismissable alert_profile_main" style="display:none"></div></div>
		
                <div class="col-md-12" id="show_profile"> 
                    <?php if(!empty($profiles)):?>
			<?php foreach ($profiles as $profile):?>
			    <div class="box box-primary box-warning" id="profile_<?php echo $profile['id']; ?>">
				<div class="box-header with-border">
				    <h3 class="box-title">#<?php echo $profile['id']; ?>. <?php echo $profile['gioi_tinh'] == 'male' ? 'Anh' : 'Chị'; ?> <b><?php echo $profile['ho_ten']; ?></b> <span>&nbsp;&nbsp;&nbsp;  <a href="tel:0943000300"><i class="fa fa-phone-square"></i> <?php echo $profile['so_dien_thoai']; ?></a></span></h3>
				    <div class="box-tools">
					
					<button type="button" class="btn btn-warning btn-sm btn_profile" rel_id="<?php echo $profile['id']; ?>"><i class="fa fa-edit"></i> Cập nhật thông tin</button>
					<button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
					<button type="button" class="btn btn-default btn-sm del_profile" data_id="<?php echo $profile['id']; ?>"><i class="fa fa-trash"></i></button>
				    </div>
				</div>

				<div class="box-body">
				    <div class="invoice-info">
					<div class="col-md-6 nopadding">
					    <div class="col-md-4"><b>Ngày sinh:</b> <?php echo format_date($profile['ngay_sinh'],'d/m/Y'); ?></div>
					    <div class="col-md-4"><b>Nơi sinh:</b> <?php echo $profile['noi_sinh']; ?></div>
					    <div class="col-md-4"><b>Quốc tịch:</b> <?php echo $profile['quoc_tich']; ?></div>
					    <div class="col-md-4"><b>Số CMND:</b> <?php echo $profile['cmnd']; ?></div>
					    <div class="col-md-4"><b>Ngày cấp:</b> <?php echo format_date($profile['ngay_cap_cmnd'],'d/m/Y'); ?></div>
					    <div class="col-md-4"><b>Nơi cấp:</b> <?php echo $profile['noi_cap_cmnd']; ?></div>

					    <div class="col-md-4"><b>Số hộ chiếu:</b> <?php echo $profile['so_ho_chieu']; ?></div>
					    <div class="col-md-4"><b>Ngày Cấp:</b> <?php echo format_date($profile['ngay_cap_ho_chieu'],'d/m/Y'); ?></div>
					    <div class="col-md-4"><b>Ngày hết hạn:</b> <?php echo format_date($profile['ngay_het_han_ho_chieu'],'d/m/Y'); ?></div>

					    <div class="col-md-6"><b>Nghề nghiệp:</b> <?php echo $profile['nghe_nghiep']; ?></div>
					    <div class="col-md-6"><b>Mã số thuế:</b> <?php echo $profile['ma_so_thue']; ?></div>
					    <div class="col-md-6"><b>Địa chỉ thường trú:</b> <?php echo $profile['dia_chi_thuong_tru']; ?></div>
					    <div class="col-md-6"><b>Địa chỉ tạm trú:</b> <?php echo $profile['dia_chi_tam_tru']; ?></div>

					    <div class="col-md-12">
						<?php
						$visa_hien_co = unserialize($profile['visa_hien_co']);
						$thong_tin_gia_dinh = unserialize($profile['thong_tin_gia_dinh']);
						$danh_sach_dich_vu = unserialize($profile['danh_sach_dich_vu']);
						?>
						<table class="table table-bordered">
						    <thead>
							<tr>
							    <th>Họ và tên gia đình</th>
							    <th>Ngày sinh</th>
							    <th>Mối quan hệ</th>
							</tr>
						    </thead>
						    <tbody>
							<?php foreach ($thong_tin_gia_dinh as $gd): ?>
							<tr>
							    <td><?php echo $gd['ho_ten_gia_dinh']; ?></td>
							    <td><?php echo $gd['ngay_sinh']; ?></td>
							    <td><?php echo $gd['moi_quan_he']; ?></td>
							</tr>
							<?php endforeach; ?>
						    </tbody>
						</table>

					    </div>
					</div>

					<div class="col-md-6 table-responsive">
					    <table class="table table-striped">
						<thead>
						    <tr>
							<th>Visa</th>
							<th>Mã visa</th>
							<th>Ngày cấp</th>
							<th>Ngày hết hạn</th>
						    </tr>
						</thead>
						<tbody>
						    <?php foreach ($visa_hien_co as $vs): ?>
						    <tr>
							<td><?php echo $vs['visa']; ?></td>
							<td><b><?php echo $vs['ma_visa']; ?></b></td>
							<td><?php echo $vs['ngay_cap']; ?></td>
							<td><?php echo $vs['ngay_het_han']; ?></td>
						    </tr>
						    <?php endforeach; ?>
						</tbody>
					    </table>
					    <hr>
					    <table class="table table-striped">
						<thead>
						    <tr>
							<th>Dịch vu</th>
							<th>Ngày làm</th>
							<th>Ngày hoàn thành</th>
							<th>Trạng thái</th>
							<th>Ghi chú</th>
						    </tr>
						</thead>
						<tbody>
						    <?php foreach ($danh_sach_dich_vu as $dv): ?>
						    <tr class="text-muted">
							<td><?php echo $dv['dich_vu']; ?></td>
							<td><?php echo $dv['ngay_lam']; ?></td>
							<td><?php echo $dv['ngay_hoan_thanh']; ?></td>
							<td><?php echo $dv['trang_thai']; ?></td>
							<td><?php echo $dv['ghi_chu']; ?></td>
						    </tr>
						    <?php endforeach; ?>

						</tbody>
					    </table>
					    <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
						<?php echo!empty($profile['ghi_chu']) ? $profile['ghi_chu'] : 'Chưa có ghi chú cho hồ sơ này'; ?>
					    </p>
					</div>
				    </div>
				</div> 

			    </div>
			<?php endforeach;?>
		    <?php endif;?>
                    
                </div>
		<div class="col-md-12"> 
		<div class="box-footer">
                        <a class="save_order btn btn-info margin-r-5" rel_alert="alert_order">Lưu khách hàng</a>
                        <button type="button" class="btn btn-primary margin-r-5">
                                <i class="fa fa-question-circle"></i> Xem hướng dẫn
                            </button>
        
                        <small class="pull-right">
                            
                            <?php if($order['id'] != 0):?>
                            <a href="invoice-print.html" target="_blank" class="btn btn-default margin-r-5"><i class="fa fa-print"></i> In hồ sơ</a>
                            <button type="button" class="btn btn-warning btn_profile">
                                <i class="fa fa-plus"  title="Thêm hồ sơ"></i> Thêm hồ sơ
                            </button>
                            <?php endif;?>
                        </small>

                    </div>
		     </div>
            </div>

    </section>

</div>
<table style="display:none">
    <tr class="transaction_row_data">
        <td class="hd_loai">
            <select class="transaction_data" name="type">
                <option value="Thu">Thu</option>
                <option value="Chi">Chi</option>
            </select>
        </td>
        <td class="hd_md">
            <select class="transaction_data" name="description">
                <?php foreach ($services as $v) : ?>
                <option value="<?php echo $v['alias'];?>"><?php echo $v['name'];?></option>
                <?php endforeach;?>
            </select>
        </td>
        <td class="hd_sl" style="width:50px"><input name="quantity" type="number" class="transaction_data"></td>
        <td class="hd_gc"><input name="note" type="text" class="transaction_data"></td>
        <td class="hd_st" style="width:110px"><input name="money" class="transaction_data format_number" type="text" rel="Thu"></td>
        <td>
	    <?php if($order['id'] > 0):?>
		<i class="text-muted fa fa-save text-red"></i>
	    <?php endif;?>
	    <i class="text-muted fa fa-trash-o"></i>
	</td>
    </tr>
</table>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="popup_control">
        </div>
    </div>
</div>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="/assets/admin/js/webcam.js?t=1513415516"></script>
<script>
    var network_id = <?php echo $user['network_id']; ?>;
    $(".search_input").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/admincp/order/search_profile",
                data: {q: request.term},
                success: function (result) {
                    console.log(result);
                    result = JSON.parse(result);
                    console.log(result);
                    if (typeof result !== 'undefined' && result.length > 0)
                    {
                        end_key = (result.length > 6) ? 6 : result.length;
                        var inner_html = "";
                        for (key = 0; key < end_key; key++) {
//                            var price_compare = (result[key].price_compare == 0) ? "" : result[key].price_compare;
                            inner_html += '<li rel=' + result[key].username + '>\n\
                        <img class="thumbnail" src="/assets/upload/avatar/' + result[key].avatar + '">\n\
                        <p style="float:left"><span class="title">' + result[key].username + '</span><br>\n\
\n\                     <span style="color:red">Nhóm: ' + result[key].department_name + '</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span style="color:#777">Danh hiệu: ' + result[key].level_department_name + '</span></p>\n\
                        <p class="del_product_id">Xóa</p></li>';
                        }
                        $('.search-results').html(inner_html).fadeIn();
                    } else {
                        $('.search-results').fadeOut();
                    }

                },
                error: function () {
                    response([]);
                }
            });
        },
        minLength: 0
    })
    $(".search_input").click(function () {
        $(".search_input").autocomplete("search", "");
    });
    $(".search_input").focusout(function () {
        $('.search-results').fadeOut();
    });
    $("div.add_product").on('click', '.search-results li', function () {
        var handle = $(this).attr('rel');
        var who_handle = $('#who_handle').val();
        var n = who_handle.indexOf(handle);
        if (n == -1) {
            if (who_handle == "") {
                $('#who_handle').val(handle);
            } else {
                $('#who_handle').val(who_handle + ',' + handle);
            }
            $('.results').append($(this));
        }
        $(".search_input").val('');
    });
    $("div.add_product").on('click', '.del_product_id', function () {
        $(this).parent().remove();
        var who_handle = "";
        $('.results li').each(function () {
            if (who_handle == "")
                who_handle = $(this).attr('rel');
            else
                who_handle = who_handle + ',' + $(this).attr('rel');
        })
        $('#who_handle').val(who_handle);
    });
    var data = {};
    var error = 0;
    var ele;
    $('.btn_tips').click(function () {
        error = 0;
        $('.loading').fadeIn();
        var id = $(this).attr('rel');
        ele = $('#' + id);
        $.when(get_data(ele)).done(function () {
            if (error == 1) {
                $('.loading').fadeOut();
                return false;
            }
            console.log(data);
            $.post('/<?php echo ADMIN_URL; ?>country/add_edit_tips', {data: data}, function (results) {
                console.log(results);
                $('input[name="id"]', ele).val(results);
                $('.loading').fadeOut();
                $('.alert_popup', ele).fadeIn();
                var tb = '<?php echo date('d-m-Y \L\ú\c H:i:s'); ?>: Cập nhật Tips <u>"' + data.tips + '"</u> thành công';
                $('.alert_popup p', ele).html(tb);
                $(window).scrollTop(0);
                //$('#list_ajax').html(results);
            });
        });
    })
    $('.close').click(function () {
        var parent = $(this).parent().parent();
        parent.fadeOut();
    })
    var order_id = <?php echo $order['id']; ?>;
    function get_data_order() {
        data = {};
        $('.order-input').each(function () {
            var key = $(this).attr('name');
            if (key == 'full_name' && $(this).val() == '') {
                //alert('Tên khách hàng không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if (key == 'phone' && $(this).val() == '') {
                //alert('Số điện thoại khách hàng không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if ($(this).hasClass('return_img')) {
                data.image = $(this).val();
            } else if (key == 'total' || key == 'arrears') {
                data[key] = $(this).autoNumeric('get');
            } else {
                data[key] = $(this).val();
            }
        })
	
	data_transaction = [];
	if(order_id == 0){
	    var i = 0;
	    $('.transaction_row').each(function (){
		data_transaction[i] = {};
		$('.transaction_data', this).each(function (){
		    var key = $(this).attr('name');
		    if (key == 'money') {
		       if($(this).val() == "" || $(this).val() == 0){
			   data_transaction.splice(i,1);
			   return false;
		       }
		       data_transaction[i][key] = $(this).autoNumeric('get');
		    }else{
		       data_transaction[i][key] = $(this).val();
		    }
		})
		i++;
	    })
	}
    }
    var is_click = 1;
    var html_old = '';
    function loading(_this,msg = '',method = 'on'){
        if(method == 'off'){
            _this.html(html_old);
            is_click = 1;
	    var alert_box = _this.attr('rel_alert');
	    $('.'+alert_box).html(msg);
	    $('.'+alert_box).fadeIn();
	    setTimeout(function(){$('.'+alert_box).fadeOut();},6000);
            return false;
        }
        is_click = 0;
        html_old = _this.html();
        var h = _this.outerHeight();
        var html = '<i class="fa fa-spinner fa-pulse fa-fw"></i>'+msg;
        _this.css('height',h);
        _this.html(html);
    }
    $('.save_order').click(function(){
        if(is_click == 1){
            loading($(this),' Đang lưu');
	    console.log(is_click);
	    var _this = $(this);
            $.when(get_data_order()).done(function () {
		if(error == 1){
		    error = 0;
		    loading(_this,'Vui lòng không được bỏ trống tên khách hàng và số điện thoại','off');
		    return false;
		}
                if(order_id > 0){
                    data.id = order_id;
                }
                $.post('/<?php echo ADMIN_URL; ?>order/save_order', {data: data,data_transaction:data_transaction}, function (results) {
		    if(order_id <= 0){
			window.location.href = '/<?php echo ADMIN_URL; ?>order/process/'+results;
		    }else{
			if(results == 0){
			    loading(_this,'THÔNG BÁO: Không tìm thấy đơn hàng','off');
			}else{
			    loading(_this,'THÔNG BÁO: Cập nhật đơn hàng #'+order_id+' thành công','off');
			}
		    }
                    
                });
            });
        }
    });
    function get_profile() {
        data = {
	    'visa_hien_co' : {},
	    'thong_tin_gia_dinh' : {},
	    'danh_sach_dich_vu' : {}
	};
        $('#profile [name]').each(function () {
            var key = $(this).attr('name');
	    var field = $(this).attr('field_name');
	    console.log(field);
            if (key == 'ho_ten' && $(this).val() == '') {
                //alert('Tên khách hàng không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if (key == 'so_dien_thoai' && $(this).val() == '') {
                //alert('Số điện thoại khách hàng không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if (typeof field != "undefined") {
		var index = $(this).parent().parent().index();
		if(!(index in data[field])){
		    data[field][index] = {};
		}
                data[field][index][key] = $(this).val();
            } else {
                data[key] = $(this).val();
            }
        })
	
	
    }
    $('body').on('click','.save_profile', function(){
        if(is_click == 1){
            loading($(this),' Đang lưu');
	    var _this = $(this);
	    var next = $(this).attr('next');
            $.when(get_profile()).done(function () {
	
		if(error == 1){
		    error = 0;
		    _this.attr('rel_alert','alert_profile');
		    loading(_this,'Vui lòng không được bỏ trống tên khách hàng và số điện thoại','off');
		    _this.attr('rel_alert','alert_profile_main');
		    return false;
		}
		if(order_id <= 0){
		    return false;
                }
		data.order_ids = order_id;
                $.post('/<?php echo ADMIN_URL; ?>order/save_profile', {data: data}, function (results) {
		    if(data.id > 0){			
			if(results == 0){
			    loading(_this,'THÔNG BÁO: Không tìm thấy hồ sơ','off');
			    return false;
			}else{
			    $('#profile_'+data.id).remove();
			}
		    }
		    if(next == 'new'){
			loading(_this,'THÔNG BÁO: bạn vừa thêm hô sơ thành công, có thể tiếp tục thêm hô sơ khác','off');
			//$('#profile').html($('#profile').html());
			$('#profile [name]').each(function () {
			    $(this).val('');
			})
			$('.profile_table').each(function () {
			    var _this = $(this);
			    $('tr', _this).each(function () {
				console.log($('tr', _this).length);
				console.log($(this).index());
				if($('tr', _this).length > 2 && $(this).index() > 0){
				    $(this).remove();
				}
			    })
			})
			$('#profile input[name="ho_ten"]').focus();
			$('#profile option:first-child').prop('selected', true);
			$('#profile [next="update"]').remove();
		    }
		    if(next == 'close'){
			loading(_this,'THÔNG BÁO: bạn vừa thêm hoặc cập nhật hô sơ thành công, hồ sơ đang nằm ở trên cùng','off');
			$('#profile').modal('hide');
		    }
		    if(next == 'update'){
			loading(_this,'THÔNG BÁO: bạn vừa cập nhật hồ sơ thành công, hồ sơ đã được chuyển lên trên cùng','off');
		    }
		    $('#show_profile').prepend(results);
                    
                });
            });
        }
    });
    $('body').on('click','.del_profile',function(){
	var id = $(this).attr('data_id');
	show_dialog('Bạn có chắc chắn muốn xóa hồ sơ này ra khổi đơn hàng không ?', function () {
	    $.post('/<?php echo ADMIN_URL; ?>order/del_profile', {id: id,order_id:order_id}, function (results) {
		if(results > 0){
		    $('#profile_'+results).remove();
		}else{
		   alert('Hồ sơ xóa không thành công, vùi lòng báo cho bộ phận kỹ thuật xử lý');
		}
	    });
	});
    })
    $('body').on('click','.transaction td .fa-save', function(){
	var tr = $(this).parent().parent();
	var money = $('input[name="money"]', tr).autoNumeric('get');
	if(money == '' || money == 0){
	    alert('Vui lòng nhập số tiền');
	    return false;
	}
	var _this = $(this);	
	var type = $('select[name="type"]', tr).val();
	var description = $('select[name="description"]', tr).val();
	var quantity = $('input[name="quantity"]', tr).val();
	var note = $('input[name="note"]', tr).val();
	var id = tr.attr('data_id');
	var transaction = {
	    id : id,
	    order_id : order_id,
	    money : money,
	    note : note,
	    description : description,
	    quantity : quantity,
	    type : type	    
	};
	var total = $('input[name="total"]').autoNumeric('get');
	var arrears = $('input[name="arrears"]').autoNumeric('get');
	var order = {
	    id : order_id,
	    total : total,
	    arrears : arrears
	    
	};
	$(this).removeClass('text-muted fa fa-save');
	$(this).addClass('fa fa-spinner fa-pulse fa-fw');
        $.post('/<?php echo ADMIN_URL; ?>order/save_transaction', {transaction:transaction,order:order}, function (results) {
	    _this.removeClass('fa fa-spinner fa-pulse fa-fw text-red');
	    _this.addClass('text-muted fa fa-save');
	    tr.attr('data_id',results);
	});
    });
    var html_tr = $('.transaction_row_data').html();
    $('body').on('click','.transaction td .fa-trash-o', function(){
        if($('.transaction tr').length > 2){
	    var tr = $(this).parent().parent();
	    if(order_id > 0){
		var id = tr.attr('data_id');
		if(typeof id != 'undefined'){
		    show_dialog('Bạn có chắc chắn muốn xóa không?', function () {
			$.post('/<?php echo ADMIN_URL; ?>order/del_transaction', {id: id}, function (results) {
			    console.log(results);
			    if(results == 0){
				console.log('Không tồn tại giao dịch này');
			    }
			    if(results == -1){
				alert('Bạn không thể xóa giao dịch do người khác tạo');
			    }
			    if(results == 1){
				tr.remove();
				update_money();
			    }
			});
		    });
		}else{
		    tr.remove();
		    update_money();
		}
	    }else{
		tr.remove();
		update_money();
	    }
        }
    });
    
    <?php if(!empty($user['role_network'])):?>
    $('body').on('click','.transaction td .toggle-lock', function(){
	var tr = $(this).parent().parent();
	var td = $(this).parent();
	var id = tr.attr('data_id');
	var _this = $(this);
	$(this).removeClass('text-muted fa fa-unlock fa-lock');
	$(this).addClass('fa fa-spinner fa-pulse fa-fw');
        $.post('/<?php echo ADMIN_URL; ?>order/lock_transaction', {id:id}, function (results) {
	    console.log(results);
	    _this.removeClass('fa fa-spinner fa-pulse fa-fw');
	    if(results == 1){
		td.html(' <i class="text-muted fa fa-lock toggle-lock"></i>');		
	    }else if(results == 0){
		td.html('<i class="text-muted fa fa-save"></i> <i class="text-muted fa fa-trash-o"></i> <i class="text-muted fa fa-unlock toggle-lock"></i>');
	    }else{
		alert("Đã xãy ra lỗi");
	    }
	    
	});
    });
    <?php endif;?>
    $('body').on('change','.transaction select',function(){
        $('.format_number').autoNumeric('init', {aPad:false});
        var rel = $(this).val();
        var tr = $(this).parent().parent();
        $('.hd_st input', tr).attr('rel',rel);
        var val = $('.hd_st input', tr).autoNumeric('get');
        if(rel == "Chi" && val > 0 || rel == "Thu" && val < 0){
            $('.hd_st input', tr).autoNumeric('set', -val);
        }
        update_money();
        return false;
    })
    $('body').on('keyup','.hd_st input', function(e){
        if(e.keyCode == 13){
	    var tr = $(this).parent().parent();
	    var s = $('.hd_md select', tr).val();
            tr.after('<tr class="transaction_row">'+html_tr+'</tr>');
	    $('.hd_st input',tr.next()).focus();
	    $('.hd_md option[value="' + s + '"]',tr.next()).prop('selected', true);
            $('.format_number').autoNumeric('init', {aPad:false});
            return false;
        }
        var rel = $(this).attr('rel');
        var val = $(this).autoNumeric('get');
        if(rel == "Chi" && val > 0 || rel == "Thu" && val < 0){
            $(this).autoNumeric('set', -val);
        }
        update_money();
    })
    var tc_bk = <?php echo empty($order['total']) ? 0 : $order['total']?>;
    $('input[name="total"]').keyup(function(){
        update_money();
	tc_bk = $(this).autoNumeric('get');
    })
    var tc_flat = 0;
    function update_money(){
         var st = 0;
        $('.transaction .hd_st input').each(function () {
	    if($(this).val() != ""){
		st += parseInt($(this).autoNumeric('get'));
	    }
        });
        var tc = $('input[name="total"]').autoNumeric('get');
	if(tc_flat == 1 && tc > st){
	       $('input[name="total"]').autoNumeric('set', tc_bk);
	       tc_flat = 0
	       tc = tc_bk;
	   }
	if(tc < st){
	    tc = st;
	    $('input[name="total"]').autoNumeric('set', st);
	    tc_flat = 1;
	}
        $('.con_lai').autoNumeric('set', tc - st);
        $('.da_thanh_toan').autoNumeric('set', st);
    }
    if(order_id > 0){
        $('.format_number').autoNumeric('init', {aPad:false});
        update_money();
    }
    $('.box-comments').slimScroll({
        height: '250px',
        start: 'bottom',
    });
    $('#progress_update').on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            var target = $(event.target);
            var val = target.val();
            target.val('');
            if (val) {
                $.post('/<?php echo ADMIN_URL; ?>order/progress_update', {msg: val, id: order_id}, function (results) {
		    results = JSON.parse(results);
		    $('.box-comments .username').removeClass('text-red');
		    var html = '<div class="box-comment"> <div class="col-md-12"> <span class="username text-red"> '+results.date_create+' - '+results.username+': </span> '+results.content+' </div> </div>';
                    $('.box-comments').append(html);
                    var box_comments_h = $('.box-comments')[0].scrollHeight;
                    $('.box-comments').slimScroll({scrollTo: box_comments_h});
                })
            }
            e.preventDefault();
            return false;
        }
    });
    $('select[name="service_id"]').change(function(){
	$('input[name="service_name"]').val($(this).find('option:selected').text());
    })   
    $('input[name="service_name"]').val($('select[name="service_id"]').find('option:selected').text());
    $('body').on('click', '.btn_profile', function (event){ 
	event.preventDefault();
	var id = $(this).attr('rel_id');
	$.post('/<?php echo ADMIN_URL; ?>order/profile', {id:id}, function (data) {
	    console.log(data);
	    $(data).modal().on(function () {});
	}).success(function () {
		
	});
	
    });
    $('body').on('hidden.bs.modal', '#profile', function (){ 
	$('#profile').remove();
    });
    $('body').on('mouseover', '[data-mask]', function (){ 
	$(this).inputmask();
    });
    $('body').on('keyup', '[uppercase]', function (){ 
	this.value = this.value.toUpperCase();
    });

    $('body').on('keyup','.profile_table .add_tr', function(e){
        if(e.keyCode == 13){
	    var tr = $(this).parent().parent();
	    var html_tr = tr.html();
            tr.after('<tr>'+html_tr+'</tr>');
	    $('input',tr.next()).focus();
	    $('input[name="ngay_lam"]',tr.next()).val(moment().format('DD/MM/YYYY'));
            return false;
        }
    })
    
    $('body').on('click','.profile_table td .fa-trash-o', function(){
	var table = $(this).parent().parent().parent();
        if($('tr',table).length > 2){
	    var tr = $(this).parent().parent();
	    show_dialog('Bạn có chắc chắn muốn xóa không ?', function () {
		tr.remove();
	    }); 
        }
    });
    
</script>
<style>
    .ui-dialog { z-index: 4000 !important ;}
    .invoice{
        padding-top: 0px;
    }
    .kc{
        display: inline-block;
        width: 40px;
        height: 16px;
    }
    .invoice-info div{

        padding-bottom: 5px;
        margin-bottom: 5px;
    }
    #total_fees{
        color: red;
        font-size: 22px;
    }
    td{
        outline: none;
    }
    .transaction tr td{
        position: relative;
    }
    .transaction ul{
        position: absolute;
        top: 100%;
        left: 0px;
        z-index: 9999;
        padding: 0px;
        box-shadow: 1px 1px 5px #ccc;
        background: #f4f4f4;
    }
    .transaction li{
        display: inline-block;
        list-style: none;
        white-space: nowrap;
        width: 100%;
        padding: 6px 10px;
        cursor: pointer;
        border-bottom: 1px solid #ccc;
    }
    .transaction input, .transaction select{
        width: 100%;
        border: 0px;
    }
    .transaction input:focus, .transaction select:focus{
        outline: none;
    }
    .transaction li:first-child{
        position: absolute;
        left: -20px;
        top: 22px;
        width: 20px !important;
        padding: 0px 5px!important;
        background: #d0d0d0;
        color: #929292;
    }
    .transaction li:hover{
        background: #fefefe;
    }
    .transaction li:first-child:hover{
        background: #777;
        color:#000;
    }
    .transaction td i{
        color:#ccc;
        cursor: pointer;
    }
    .transaction td .fa-save, .fa-lock {
	margin-left: 5px;
    }
    .transaction td i:hover{
        color:#222;
    }
    .box-comments{
	background: #fdfdfd;
    }
    .box-comments .username{
	color: #a9a9a9;
    }
    hr.style3 {
	border-top: 1px dashed #ccc;
	float:left;
	width: 100%;
    }
    hr.style4 {
	border-top: 1px solid #ccc;
	float:left;
	width: 100%;
    }
     .profile_table input, .profile_table select{
        width: 100%;
        border: none;
	padding: 0px;
    }
    .profile_table input:focus, .transaction select:focus{
        outline: none;
    }
    .profile_table td:last-child{
	width: 30px;
    }
    .profile_table td:last-child i{
	color: #ccc;
	cursor: pointer;
    }
    .profile_table td:last-child i:hover{
	color: #000;
    }
</style>