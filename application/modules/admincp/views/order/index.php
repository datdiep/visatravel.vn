<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Danh sách đơn hàng</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Danh sách đơn hàng</li>
        </ol>
        <br>
        <a href="/admincp/order/process" class="btn btn-success"><i class="fa fa-user-plus"></i> Thêm mới</a>
        <?php if (!empty($networks) && !empty($user['role_network'])): ?>
            <select class="form-control" onchange="location = this.value;" style="width: 200px;float: left;margin:0 10px 5px 0px">
                <?php foreach ($networks as $n): ?>
                    <option <?php echo $n['id'] == $network_id ? 'selected' : ''; ?> value="/admincp/order/<?php echo $n['id'] ?>"><?php echo $n['name']; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
        <input type="text" id="keyword" class="form-control" placeholder="Số điện thoại hoặc email khách hàng" style="width: 250px;float: left;margin-right: 10px;"/>
        <select class="form-control" id="status" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Lọc tiến trình -- </option>
            <?php foreach ($order_status as $k => $v) : ?>
                <option <?php echo $status == $k ? 'selected' : ''; ?> value="<?php echo $k; ?>">-- <?php echo $v; ?> --</option>
            <?php endforeach; ?>
        </select>
    </section>


    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if (!empty($msg_error)): ?>
                    <div class="alert alert-success alert-dismissable">
                        <?php echo $msg_error; ?>
                    </div>
                <?php endif; ?>
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="row" style="margin-top:20px">
            <div class="col-md-12">
                <div class="box-footer">
                    <span class="bg-gray disabled color-palette" style="padding: 1px 5px"><i class="fa fa-clock-o"></i></span>: Normal(2 working days) &nbsp;&nbsp;&nbsp;
                    <span class="bg-yellow color-palette" style="padding: 1px 5px"><i class="fa fa-clock-o"></i></span>: Urgent(4-8 working hours) &nbsp;&nbsp;&nbsp;
                    <span class="bg-red color-palette" style="padding: 1px 5px"><i class="fa fa-clock-o"></i></span>: Emergency(Within 30 minutes) &nbsp;&nbsp;&nbsp;<br>
                    <b>1 S</b>: 1 tháng &nbsp;&nbsp;&nbsp; <b>1 M</b>: 1 tháng nhiều lần &nbsp;&nbsp;&nbsp;
                    <b>3 S</b>: 3 tháng &nbsp;&nbsp;&nbsp; <b>3 M</b>: 3 tháng nhiều lần &nbsp;&nbsp;&nbsp;
                    <b>6 M</b>: 6 tháng nhiều lần &nbsp;&nbsp;&nbsp;
                    <b>1 Y</b>: 12 tháng nhiều lần<br>
                    <span class="badge bg-light-blue">Mới</span> Đơn hàng mới &nbsp;&nbsp;&nbsp;
                    <span class="badge bg-yellow">25%</span> Kiểm tra hồ sơ &nbsp;&nbsp;&nbsp;
                    <span class="badge bg-red">50%</span> Chuyển đơn hàng &nbsp;&nbsp;&nbsp;
                    <span class="badge bg-green">75%</span> Đang kiểm duyệt &nbsp;&nbsp;&nbsp;
                    <span class="badge bg-gray">100%</span> Hoàn tất đơn hàng &nbsp;&nbsp;&nbsp;

                </div>
            </div>
        </div>
    </section>
</div><!-- /.row -->
<script>
    var current_pos = <?php echo $pos; ?>;
    var network_id = <?php echo $network_id; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var keyword = $('#keyword').val();
        var status = $('#status').val();
        $.post('/<?php echo ADMIN_URL; ?>order/page', {network_id:network_id,pos: pos, status: status, keyword: keyword}, function (results) {
            //console.log(results);    
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    $('#keyword').keypress(function (e) {
        if (e.keyCode == 13)
            loadlist(0);
    });
    
    $('#status').change(function () {
        loadlist(0);
    });
</script>
<script src="/assets/admin/js/order.js"></script>

