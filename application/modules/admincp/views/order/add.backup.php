<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM HỒ SƠ XỬ LÝ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>customer">Quản lý khách hàng</a></li>
            <li class="active">thêm khách hàng</li>
        </ol>

    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <?php if ($check_error == 0): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                        Thêm thành công
                    </div>
                <?php endif; ?>
                <?php if ($check_error == 1): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo validation_errors(); ?>                          
                    </div>
                <?php endif; ?>
                <div class="col-md-6">   

                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-cc-visa"></i>
                            <h3 class="box-title">Thông tin khách hàng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-6 nopadding">
                                <label for="full_name">Tên khách hàng</label>
                                <input type="text" class="form-control" name="full_name" placeholder="Tên khách hàng">
                            </div>

                            <div class="form-group col-md-6 nopadding">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại khách hàng">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="email">Email</label>
                                <input type="text" class="form-control"  name="email" placeholder="Nhập email liên hệ của khách">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="address">Địa chỉ</label>
                                <input type="text" class="form-control" name="address" placeholder="Địa chỉ khách hàng">
                            </div>

                            <div class="form-group">
                                <label for="leave_message">Ghi chú khách hàng</label>
                                <textarea name="leave_message" style="width: 100%; height: 70px" class="form-control"></textarea>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="who_consultant">NV tư vấn</label>
                                <select name="who_consultant" class="form-control valid">
                                    <option value="">Chọn nhân viên tư vấn</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>"><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="who_censor">NV xử lý</label>
                                <select name="who_censor" class="form-control valid">
                                    <option value="">Chọn nhân viên xử lý</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>"><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="progress">Tình trạng hồ sơ</label>
                                <select name="progress" class="form-control valid">
                                    <?php foreach ($progress as $value): ?>
                                        <option value="<?php echo $value; ?>"><?php echo $map_process[$value]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                        </div>
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="pax">Số lượng khách</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="pax"/>
                                    <label class="input-group-addon btn" for="pax">
                                        <span class="fa fa-user"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="vat">Mã số thuế</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="vat"/>
                                    <label class="input-group-addon btn" for="vat">
                                        <span class="fa fa-line-chart"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="appointment">Hẹn khách ngày trả hồ sơ</label>
                                <div class="input-group field-date">
                                    <input type='text' class="form-control" id='datetimepicker' name="appointment"/>
                                    <label class="input-group-addon btn" for="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div  class="col-md-12"> 
                        <div class="row">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="fa fa-usd"></i>
                                    <h3 class="box-title">Tổng tiền chi ra</h3>
                                </div>

                                <div class="box-body">

                                    <ul class="todo-list ui-sortable">
                                        <li>
                                            <span class="handle ui-sortable-handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <span class="text text-danger">Chưa có chi phí chi ra cho hồ sơ này</span>
                                        </li>
                                    </ul>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer clearfix no-border">
                                    <p class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Thêm chi phí hồ sơ</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div  class="col-md-12"> 

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="ion ion-clipboard"></i>
                            <h3 class="box-title">Chọn loại dịch vụ khách sử dụng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-2 nopadding">
                                <label for="services">Chọn dịch vụ</label>
                                <select name="services" class="form-control valid">
                                    <?php foreach ($map_services as $k => $value): ?>
                                        <option value="<?php echo $k; ?>"><?php echo mb_strtoupper($value, "UTF-8"); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <?php if (@$country): ?>
                                <div class="form-group col-md-2 nopadding">
                                    <label for="country">Quốc gia</label>
                                    <select name="country" class="form-control valid">
                                        <?php foreach ($country as $k => $v): ?>
                                            <option value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                            <div class="form-group col-md-2 nopadding">
                                <label for="purpose_of_visit">Mục đích</label>
                                <select name="purpose_of_visit" class="form-control valid">
                                    <?php foreach ($map_type_of_service['Visa export'] as $k => $value): ?>
                                        <option value="<?php echo $k; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-2 nopadding">
                                <label for="total_fees">Tổng tiền</label>
                                <div class="input-group">
                                    <input type='text' class="form-control format_number" name="total_fees"/>
                                    <label class="input-group-addon btn" for="total_fees">
                                        <span class="fa fa-usd"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group col-md-2 nopadding">
                                <label for="money_paid">Đã thanh toán</label>
                                <div class="input-group">
                                    <input type='text' class="form-control format_number" name="money_paid"/>
                                    <label class="input-group-addon btn" for="money_paid">
                                        <span class="fa fa-usd"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12"> 

                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thiết lập hồ sơ</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">

                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Họ và Tên</th>
                                        <th style="width: 100px">Giới tính</th>
                                        <th style="width: 140px">Ngày sinh</th>
                                        <th>Quốc tịch</th>
                                        <th style="width: 150px">Số hộ chiếu</th>
                                        <th style="width: 150px">Loại visa</th>
                                        <th style="width: 120px">Ngày hết hạn</th>
                                        <th>Địa chỉ ở VN</th>
                                        <th style="width: 100px">Trình độ</th>
                                        <th style="width: 90px">Action</th>
                                    </tr>
                                    <tr>
                                        <td>1.</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td class="actions">
                                            <i class="fa fa-trash-o fa-lg" title="Xóa"></i> 
<!--                                                <i class="fa fa-ban fa-lg" title="Từ chối"></i> -->
                                            <i class="fa fa-print fa-lg"  title="In"></i>
                                        </td>
                                    </tr>



                                </tbody></table>
                            <span id="total_fees">Tổng tiền: <b>100 $</b></span>    
                            <a href="" class="btn btn-warning pull-right" data-toggle="modal" data-target="#myModal" style="margin-top: 15px;"><i class="fa fa-plus"  title="Thêm hồ sơ"></i> Thêm hồ sơ </a>

                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <span style="color:#ccc"><b>Double Click:</b> Để thêm & sữa </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span style="color:#ccc"><b>Nhấp Enter:</b> Để Cập nhật </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span style="color:#ccc"><b>Nhấp Esc:</b> Để bỏ qua </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>

                </div>
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="0" name="submit" class="btn btn-info">Lưu khách hàng</button>
                        <button type="submit" value="1" name="submit" class="btn btn-success">Lưu & In hóa đơn</button>
                    </div>
                </div>

            </div>
        </form>
    </section>

</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm mới hồ sơ</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="full_name">Họ và tên</label>
                        <input type="text" class="form-control" id="full_name" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="gender">Giới tính</label>
                        <select class="form-control" id="gender">
                            <option value="">-- Chọn giới tính --</option>
                            <option value="male">Nam</option>
                            <option value="female">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="date_of_birth">Ngày sinh</label>
                        <div class="input-group field-date">
                            <input type="text" class="form-control" id="date_of_birth" value="">
                            <label class="input-group-addon btn" for="date_of_birth">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address_vn">Địa chỉ ở VN</label>
                        <input type="text" class="form-control" id="address_vn" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="nationality">Quốc tịch</label>
                        <select class="form-control" id="nationality">
                            <option value="">-- Chọn quốc tịch --</option>
                            <option value="male">Nam</option>
                            <option value="female">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="passport_number">Số hộ chiếu</label>
                        <input type="text" class="form-control" id="passport_number" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="purpose_of_visit">Loại visa</label>
                        <select class="form-control" id="purpose_of_visit">
                            <option value="">-- Visa hiện tại của bạn --</option>
                            <option value="male">Du lịch (DL)</option>
                            <option value="female">Thăm thân (TT, VR)</option>
                            <option value="female">Đầu tư (DT)</option>
                            <option value="female">Lao động (LĐ)</option>
                            <option value="female">Công Tác (DN)</option>
                            <option value="female">Du học sinh (DH)</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exp_date_visa">Ngày hết hạn</label>
                        <div class="input-group field-date">
                            <input type="text" class="form-control" id="exp_date_visa" value="">
                            <label class="input-group-addon btn" for="appointment">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="literacy">Trình độ học vấn</label>
                        <select class="form-control" id="literacy">
                            <option value="">-- Vui lòng chọn --</option>
                            <option value="male">Không có</option>
                            <option value="female">Đại học</option>
                            <option value="male">Cao đẳng</option>
                            <option value="female">Chứng chỉ nghề</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="literacy">Giấy chứng nhận kinh nghiệp làm việc</label>
                        <p>Bạn cần xin từ công ty mình đã làm việc sau cho đủ 5 năm.</p>
                        <select class="form-control" id="literacy">
                            <option value="">-- Vui lòng chọn --</option>
                            <option value="male">Không xin được</option>
                            <option value="female">Xin được</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="purpose_of_visit">Công ty bảo lãnh</label>
                        <select class="form-control" id="purpose_of_visit">
                            <option value="">-- Vui lòn chọn --</option>
                            <option value="male">Có</option>
                            <option value="female">Không</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exp_date_visa">Mã số thuê</label>
                        <input type="text" class="form-control" id="full_name" placeholder="">
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        //$('#myInput').focus()
    })
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    $('.format_number').autoNumeric('init', {aPad: false});
    $('#datetimepicker').datepicker({autoclose: true});
</script>