<div class="content-wrapper">    
    <section class="content-header">
        <h1>KIỂM TRA ĐƠN #<?php echo $order['id']; ?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">Quản lý đơn hàng</a></li>
            <li class="active">Kiểm tra đơn</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <?php if (!empty($msg_error)): ?>
                        <div class="alert alert-success alert-dismissable">
                            <?php echo $msg_error; ?>
                        </div>
                    <?php endif; ?>
                    <div class="box-footer">
                        <a  class="btn btn-default" onclick="cancel(<?php echo $order['id']; ?>)" href="javascript:void(0)">Hủy</a>
                        <button type="submit" value="1" name="submit" class="btn btn-info">Lưu lại</button>                    
                        <button type="submit" value="2" name="submit" class="btn btn-success <?php echo $order['progress'] == 'transfer' ? '':'transfer';?>">
                            <?php if ($order['progress'] == 'transfer'): ?>
                                <i class="fa fa-retweet"></i> Thu hồi đơn
                            <?php else: ?>
                                <i class="fa fa-retweet"></i> Lưu lại & Chuyển
                            <?php endif; ?>
                        </button>
                        <button type="submit" value="3" name="submit" class="btn btn-danger">Hoàn thành</button>
                        <?php if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',check,') !== false): ?>
                            <div class="box-tools pull-right">
                                <span id="total_fees">TOTAL FEES: <b><?php echo $order['total_fees'] . '$'; ?></b></span>
                                <span id="visa_fees">Services fees: <b><?php echo $order['services_fees'] . '$'; ?></b></span>
                                <span id="processing_time_fees	">Processing fees: <b><?php echo $order['processing_time_fees'] . '$'; ?></b></span>
                                <span id="nationality_fees">Nationality fees: <b><?php echo $order['nationality_fees'] . '$'; ?></b></span>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin visa</h3>
                        </div>


                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alias">Trạng thái thanh toán</label>
                                        <select id="has_get_money" name="has_get_money" class="form-control">
                                            <option <?php echo $order['has_get_money'] == 1 ? 'selected' : '' ?> value="1">Đã thanh toán</option>
                                            <option <?php echo $order['has_get_money'] == 0 ? 'selected' : '' ?> value="0">Chưa thanh toán</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alias">Số tiền thanh toán($)</label>
                                        <input type="text" id="money_paid" name="money_paid" disabled="" class="form-control dashed" value="<?php echo $order['money_paid']; ?>">
                                    </div>
                                </div>
                                <?php if ($order['has_get_money'] == 0): ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-warning alert-dismissible" style="word-break: break-all">
                                            <b>Link thanh toán: </b><?php echo 'https://congvannhapcanh.net/apply-vietnamvisa/step-3?orderid=' . $order['id'] . '&token=' . $order['token']; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="pax">Number of persons</label>
                                <input class="form-control" disabled value="<?php echo $order['pax']; ?> persons">
                                <input type="hidden" name="group_size" value="<?php echo $order['pax']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="visit_purpose">Purpose of visit</label>
                                <select id="visit_purpose" name="visit_purpose" class="form-control">
                                    <?php foreach ($service_fees['visa_fees'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>" <?php echo $key == $order['purpose_of_visit'] ? 'selected' : ''; ?>><?php echo $key; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="visa_type">Type of visa</label>
                                <select id="visa_type" name="visa_type" class="form-control">
                                    <!--<option>Please select...</option>-->
                                    <?php foreach ($service_fees['visa_fees']['for tourism'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>" <?php echo $key == $order['type_of_visa'] ? 'selected' : ''; ?>><?php echo $key == '1 year multiple' ? $key . ' (US citizens only)' : $key; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="arrival_port">Arrival airport</label>
                                <select id="arrival_port" name="arrival_port" class="form-control">
                                    <option value="Tan Son Nhat International Airport (Ho Chi Minh City)" <?php echo "Tan Son Nhat International Airport (Ho Chi Minh City)" == $order['arrival_airport'] ? 'selected' : ''; ?>>Tan Son Nhat International Airport (Ho Chi Minh City)</option>
                                    <option value="Noi Bai International Airport (Ha Noi)" <?php echo "Noi Bai International Airport (Ha Noi)" == $order['arrival_airport'] ? 'selected' : ''; ?>>Noi Bai International Airport (Ha Noi)</option>
                                    <option value="Da Nang International Airport" <?php echo "Da Nang International Airport" == $order['arrival_airport'] ? 'selected' : ''; ?>>Da Nang International Airport</option>
                                    <option value="Cam Ranh International Airport (Khanh Hoa)" <?php echo "Cam Ranh International Airport (Khanh Hoa)" == $order['arrival_airport'] ? 'selected' : ''; ?>>Cam Ranh International Airport (Khanh Hoa)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="arrival_date">Arrival date</label>
                                <div class="input-group field-date">
                                    <input type='text' class="form-control" id='datetimepicker' name="arrival_date" />
                                    <label class="input-group-addon btn" for="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_news">Processing time</label>
                                <select id="processing_time" name="processing_time" class="form-control">
                                    <!--<option>Please select...</option>-->
                                    <?php foreach ($service_fees['Processing time'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>" <?php echo $key == $order['processing_time'] ? 'selected' : ''; ?>><?php echo $key == '1 year multiple' ? $key . ' (US citizens only)' : $key; ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin liên hệ</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="contact_fullname">Full name</label>
                                <input type="text" value="<?php echo $order['full_name']; ?>" class="form-control" name="contact_fullname">
                            </div>
                            <div class="form-group">
                                <label for="contact_email">Email</label>
                                <input type="text" value="<?php echo $order['email']; ?>" class="form-control" name="contact_email">
                            </div>
                            <div class="form-group">
                                <label for="contact_phone">Phone number</label>
                                <input type="text" value="<?php echo $order['phone']; ?>" class="form-control" name="contact_phone">
                            </div>
                            <div class="form-group">
                                <label for="contact_address">Address</label>
                                <input type="text" value="<?php echo $order['address']; ?>" class="form-control" name="contact_address">
                            </div>
                            <div class="form-group">
                                <label for="leave_message">Leave a message</label>
                                <textarea id="comment" name="leave_message" class="form-control" rows="3"><?php echo $order['leave_message']; ?></textarea>
                            </div>
                        </div> 
                    </div>
                    <?php echo $load_file; ?>
                </div>

                <div  class="col-md-8"> 

                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách hộ chiếu</h3>
                            <div class="box-tools pull-right">

                                <!--<a href="" class="btn btn-warning"><i class="fa fa-plus"></i> Thêm</a>-->
                            </div> 
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th class="fname">Full name</th>
                                        <th style="width: 100px">Gender</th>
                                        <th style="width: 140px">Birth date</th>
                                        <th class="nation">Nationality</th>
                                        <th style="width: 150px">Passport number</th>
                                        <th style="width: 90px">Action</th>
                                    </tr>
                                    <?php foreach ($order_sku as $i => $p): ?>
                                        <tr rel="<?php echo $p['id']; ?>">
                                            <td><?php echo $i ?>.</td>
                                            <td rel="full_name"><?php echo $p['full_name']; ?></td>
                                            <td rel="gender"><?php echo $p['gender']; ?></td>
                                            <td rel="date_of_birth"><?php echo date('d/m/Y', strtotime($p['date_of_birth'])); ?></td>
                                            <td rel="nationality"><?php echo $p['nationality']; ?></td>
                                            <td rel="passport_number"><?php echo $p['passport_number']; ?></td>
                                            <td class="actions">
    <!--                                                <i class="fa fa-edit fa-lg" title="Sữa"></i>
                                                <i class="fa fa-save fa-lg" title="Lưu"></i>-->
                                                <i class="fa fa-trash-o fa-lg" title="Xóa"></i> 
                                                <i class="fa fa-ban fa-lg" title="Từ chối"></i> 
                                                <i class="fa fa-print fa-lg"  title="In"></i>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>


                                </tbody></table>
                        </div>
                        <div class="box-footer">
                            <span style="color:#ccc"><b>Double Click:</b> Để sữa </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span style="color:#ccc"><b>Nhấp Enter:</b> Để Cập nhật </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <span style="color:#ccc"><b>Nhấp Esc:</b> Để bỏ qua </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- Chat box -->
                    <?php echo $load_chat; ?>

                </div>
            </div>
            <?php if (!empty($user['role_order_progress'])): ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">
                            <a  class="btn btn-default" onclick="cancel(<?php echo $order['id']; ?>)" href="javascript:void(0)">Hủy</a>
                            <button type="submit" value="1" name="submit" class="btn btn-info">Lưu lại</button>                    
                            <button type="submit" value="2" name="submit" class="btn btn-success <?php echo $order['progress'] == 'transfer' ? '':'transfer';?>">
                                <?php if ($order['progress'] == 'transfer'): ?>
                                    <i class="fa fa-retweet"></i> Thu hồi đơn
                                <?php else: ?>
                                    <i class="fa fa-retweet"></i> Lưu lại & Chuyển
                                <?php endif; ?>
                            </button>
                            <button type="submit" value="3" name="submit" class="btn btn-danger">Hoàn thành</button>
                            <?php if (!empty($user['role_network'])): ?>
                                <div class="box-tools pull-right">
                                    <span>agency: <b><?php echo $order['agency']; ?></b></span>
                                    <span>source: <b><?php echo $order['source']; ?></b></span>
                                    <span>campaign: <b><?php echo $order['campaign']; ?></b></span>
                                </div>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            <?php endif; ?>
        </form>
    </section>

    <div class="hidden" id="full_name_data">
        <input type="text" class="full_name in_put">
    </div>
    <div class="hidden" id="date_of_birth_data">
        <div class="input-group field-date-data">
            <input type='text' class='date_of_birth in_put'/>
            <span class="fa fa-calendar"></span>
        </div>
    </div>
    <div class="hidden" id="nationality_data">
        <select class="nationality">
            <option value="" selected="selected">Select...</option>
            <?php foreach ($country as $c): ?>
                <option value="<?php echo $c['name']; ?>" rel="<?php echo $c['special_fee']; ?>" note='<?php echo $c['note']; ?>'><?php echo $c['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="hidden" id="passport_number_data">
        <input type="text" class="passport_number in_put">
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="popup_control">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> 
                     Chọn đối tác cần chuyển.
                </h4>
            </div>
            <div class="modal-body">
                <div class="box-body">

                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
                <p class="btn btn-primary add_group" submit="1"><i class="fa fa-save"></i> Lưu lại</p>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="/assets/admin/plugins/datepicker/datepicker3.css">
<script src="/assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
    $('.transfer').click(function () {
        $("#myModal").modal("show");
        return false;
    })
    var html_map = {
        'full_name': '<input id="in_put" type="text">',
        'date_of_birth': '<input id="in_put" type="text">',
        'gender': '<select id="in_put" name="gender" class="gender"><option value="Male">Male</option><option value="Female">Female</option></select>',
        'nationality': '<select id="in_put" class="nationality"><option value="" selected="selected">Select...</option><?php foreach ($country as $c): ?><option value="<?php echo $c['name']; ?>"><?php echo $c['name']; ?></option><?php endforeach; ?></select>',
        'passport_number': '<input id="in_put" type="text">',
    };
    console.log(html_map);
    var arrival_date = '<?php echo date('d/m/Y', strtotime($order['arrival_date'])); ?>';
    $('#datetimepicker').datepicker({autoclose: true});
    $('#datetimepicker').datepicker('update', arrival_date);

    $('.fname').css('width', $('.fname').width());
    $('.nation').css('width', $('.nation').width());
    var id, column;
    $('#passport').on('dblclick', 'td', function () {
        var text_old = $('#in_put').attr('placeholder');
        $('#in_put').parent().html(text_old);
        column = $(this).attr('rel');
        id = $(this).parent().attr('rel');
        var text = $(this).text();
        if (!column) {
            return false;
        }
        $(this).html(html_map[column]);
        $(this).children().attr('placeholder', text);
        $('#in_put').focus();
    })
    var order_id = <?php echo $order['id']; ?>;
    var model = 'order_services_model';
    $('#passport').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            var target = $(event.target);
            var val = target.val();
            console.log('aaabbb');
            if (!target.val()) {
                val = target.attr('placeholder');
                $('#in_put').parent().html(val);
            } else {
                var data = {
                    'order_id': order_id
                };
                data.id = id;
                data[column] = val;
                $.post('/<?php echo ADMIN_URL; ?>order/update_passport', {data: data}, function (results) {
                    results = JSON.parse(results);
                    if (!$.isEmptyObject(results)) {
                        $('#nationality_fees b').html(results.nationality_fees + '$');
                        $('#total_fees b').html(results.total_fees + '$');
                    }

                });
            }
            //console.log(val);
            $('#in_put').parent().html(val);
            e.preventDefault();
            return false;
        }
    });
    $(document).on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 27) {
            var text_old = $('#in_put').attr('placeholder');
            $('#in_put').parent().html(text_old);
        }
    });
    if ($('#has_get_money').val() == 1) {
        $("#money_paid").prop('disabled', false);
    }
    $('#has_get_money').change(function () {
        var v = $(this).val();
        console.log(v);
        if (v == 1) {
            $("#money_paid").prop('disabled', false);
        } else {
            $("#money_paid").prop('disabled', true);
        }
    })
</script>
<script src="/assets/admin/js/order.js"></script>
