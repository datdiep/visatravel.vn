<div class="content-wrapper">    
    <section class="content-header">
        <h1>XỬ LÝ ĐƠN HÀNG #<?php echo $order['id']; ?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">Quản lý đơn hàng</a></li>
            <li class="active">Kiểm tra đơn</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">

            <div class="row" style="margin-bottom:20px">

                <div class="col-md-12">
                    <?php if (!empty($msg_error)): ?>
                        <div class="alert alert-success alert-dismissable">
                            <?php echo $msg_error; ?>
                        </div>
                    <?php endif; ?>
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-default">Từ chối</button>
                        <button type="submit" value="3" name="submit" class="btn btn-info">Hoàn thành</button>
                        <button type="submit" value="2" name="submit" class="btn btn-success"><i class="fa fa-retweet"></i> Chuyển bổ sung</button>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin visa</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="pax">Number of persons</label>
                                <input class="form-control" value="<?php echo $order['pax']; ?> persons">
                            </div>
                            <div class="form-group">
                                <label for="visit_purpose">Purpose of visit</label>
                                <input class="form-control" value="<?php echo $order['purpose_of_visit']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="visa_type">Type of visa</label>
                                <input class="form-control" value="<?php echo $order['type_of_visa']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="arrival_port">Arrival airport</label>
                                <input class="form-control" value="<?php echo $order['arrival_airport']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="arrival_date">Arrival date</label>
                                <input class="form-control" value="<?php echo $order['arrival_date']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="category_news">Processing time</label>
                                <input class="form-control" value="<?php echo $order['processing_time']; ?>">

                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <?php echo $load_file; ?>
                </div>

                <div  class="col-md-8"> 

                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách hộ chiếu</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Full name</th>
                                        <th style="width: 100px">Gender</th>
                                        <th style="width: 140px">Birth date</th>
                                        <th>Nationality</th>
                                        <th style="width: 150px">Passport number</th>
                                        <th style="width: 90px">Action</th>
                                    </tr>
                                    <?php foreach ($order_sku as $i => $p): ?>
                                        <tr>
                                            <td><?php echo $i ?>.</td>
                                            <td><?php echo $p['full_name']; ?></td>
                                            <td><?php echo $p['gender']; ?></td>
                                            <td><?php echo date('d/m/Y', strtotime($p['date_of_birth'])); ?></td>
                                            <td><?php echo $p['nationality']; ?></td>
                                            <td><?php echo $p['passport_number']; ?></td>
                                            <td class="actions">
    <!--                                                <i class="fa fa-edit fa-lg" title="Sữa"></i>
                                                <i class="fa fa-save fa-lg" title="Lưu"></i>-->
                                                <i class="fa fa-trash-o fa-lg" title="Xóa"></i> 
                                                <i class="fa fa-ban fa-lg" title="Từ chối"></i> 
                                                <i class="fa fa-print fa-lg"  title="In"></i>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>


                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- Chat box -->
                    <?php echo $load_chat; ?>
                    <!-- /.box (chat box) -->

                </div>
            </div>
            <?php if (!empty($user['role_order_progress'])): ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">
                            <button type="submit" value="1" name="submit" class="btn btn-default">Từ chối</button>
                            <button type="submit" value="3" name="submit" class="btn btn-info">Hoàn thành</button>
                            <button type="submit" value="2" name="submit" class="btn btn-success"><i class="fa fa-retweet"></i> Chuyển bổ sung</button>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </form>
    </section>
    <div class="hidden" id="gender_data">
        <select class="gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>
    <div class="hidden" id="full_name_data">
        <input type="text" class="full_name in_put">
    </div>
    <div class="hidden" id="date_of_birth_data">
        <div class="input-group field-date-data">
            <input type='text' class='date_of_birth in_put'/>
            <span class="fa fa-calendar"></span>
        </div>
    </div>
    <div class="hidden" id="nationality_data">
        <select class="nationality">
            <option value="" selected="selected">Select...</option>
            <?php foreach ($country as $c): ?>
                <option value="<?php echo $c['name']; ?>" rel="<?php echo $c['special_fee']; ?>" note='<?php echo $c['note']; ?>'><?php echo $c['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="hidden" id="passport_number_data">
        <input type="text" class="passport_number in_put">
    </div>
</div>
<script>
    var order_id = <?php echo $order['id']; ?>;
    var model = 'order_services_model';
</script>
<script src="/assets/admin/js/order.js"></script>
<style>

    #passport td{
        position: relative;
        padding-right: 30px;

    }
    #passport input{
        padding: 0 5px;
    }
    #passport i{
        margin: 0 3px;
        cursor: pointer;
        color: #ccc;
    }
    #passport i:hover{
        color: red
    }
    .fa-save{
        display: none;
    }
    #passport .fa-edit{
        position: absolute;
        top: 12px;
        right: 5px;
        display: none;
    }
    #passport .fa-times-circle{
        position: absolute;
        top: 12px;
        right: 5px;
    }
    #passport td:hover .fa-edit{
        display: block
    }
    .field-date-data .fa-calendar{
        position: absolute;
        right: 5px;
        top: 5px;
    }
    .date_of_birth{
        padding-right: 20px;
    }
    .actions{
        padding-right: 0px !important;
    }
</style>