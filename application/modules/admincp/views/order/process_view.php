<div class="content-wrapper">    
    <section class="content-header">
        <h1>Hồ sơ khách hàng: <span class="c-red"><?php echo $order['full_name']; ?></span></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">Quản lý đơn hàng</a></li>
            <li class="active">Kiểm tra đơn</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">

            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <?php if (!empty($msg_error)): ?>
                        <div class="alert alert-success alert-dismissable">
                            <?php echo $msg_error; ?>
                        </div>
                    <?php endif; ?>
                    <div class="box-footer">

                        <span class="btn" style="background:#004b91;padding: 5px 10px;text-transform: uppercase;color:#fff">NV tư vấn: <b><?php echo $order['who_consultant']; ?></b> </span>                                
                        <span class="btn" style="background:#009142;padding: 5px 10px;text-transform: uppercase;color:#fff">NV xử lý: <b><?php echo $order['who_censor']; ?></b> </span>      
                        <span class="btn"><?php echo $map_order[$order['progress']]; ?></span>

                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false && $order['status'] == 'cancel'): ?>
                            <a  class="btn btn-default" onclick="trash(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Xóa</a>
                        <?php endif; ?>

                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',empty,') !== false && $order['status'] == 'trash'): ?>
                            <a  class="btn btn-default" onclick="trash_empty(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-cc-visa"></i>
                            <h3 class="box-title">Thông tin visa (<?php echo $order['status']; ?>)</h3>
                        </div>

                        <div class="box-body">

                            <div class="form-group col-md-3 nopadding">
                                <label for="progress">Tình trạng hồ sơ</label>
                                <select name="progress" class="form-control valid">
                                    <?php foreach ($progress as $value): ?>
                                        <option value="<?php echo $value; ?>" <?php if ($order['progress'] == $value) echo 'selected style="color:#ff0000"'; ?>><?php echo $map_process[$value]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-2 nopadding">
                                <label for="pax">SL khách</label>
                                <input class="form-control" name="pax" value="<?php echo $order['pax']; ?>">
                            </div>
                            <?php if ($order['country']): ?>
                                <div class="form-group col-md-3 nopadding">
                                    <label for="visa_type">Quốc gia</label>
                                    <input class="form-control" value="<?php echo $order['country']; ?>">
                                </div>
                            <?php endif; ?>

                            <div class="form-group col-md-4 nopadding">
                                <label for="services">Chọn dịch vụ</label>
                                <select name="services" class="form-control valid">
                                    <?php foreach ($map_services as $k => $value): ?>
                                        <option value="<?php echo $k; ?>" <?php if ($order['services'] == $k) echo 'selected style="color:#ff0000"'; ?>><?php echo mb_strtoupper($value, "UTF-8"); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-3 nopadding">
                                <label for="purpose_of_visit">Mục đích</label>
                                <select name="purpose_of_visit" class="form-control valid">
                                    <?php foreach ($map_type_of_service['Visa export'] as $k => $value): ?>
                                        <option value="<?php echo $k; ?>" <?php if ($order['purpose_of_visit'] == $k) echo 'selected style="color:#ff0000"'; ?>><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-12 nopadding">
                                <label for="leave_message">Leave a message</label>
                                <textarea id="comment" name="leave_message" class="form-control" rows="3"><?php echo $order['leave_message']; ?></textarea>
                            </div>

                            <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false || !empty($user['role_network'])): ?>
                                <div class="form-group col-md-4 nopadding">
                                    <label for="total_fees">Tổng tiền dịch vụ</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control format_number" name="total_fees" value="<?php echo $order['total_fees']; ?>"/>
                                        <label class="input-group-addon btn" for="total_fees">
                                            <span class="fa fa-usd"></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group col-md-4 nopadding">
                                    <label for="money_paid">Đã thanh toán</label>
                                    <div class="input-group">
                                        <input type='text' class="form-control format_number" name="money_paid" value="<?php echo $order['money_paid']; ?>"/>
                                        <label class="input-group-addon btn" for="money_paid">
                                            <span class="fa fa-usd"></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-4 nopadding">
                                    <label for="money_paid">Khách còn nợ</label>
                                    <input type="text" class="form-control format_number" disabled="" value="<?php echo ($order['total_fees'] - $order['money_paid']); ?>">
                                </div>
                            <?php endif; ?>

                        </div>                       
                    </div>         
                    <div class="box box-primary">
                        <div class="box-header">
                            <i class="fa fa-usd"></i>
                            <h3 class="box-title">Tổng tiền chi ra</h3>
                        </div>

                        <div class="box-body">

                            <ul class="todo-list ui-sortable">
                                <li>
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <span class="text text-danger">Chưa có chi phí chi ra cho hồ sơ này</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix no-border">
                            <p class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Thêm chi phí hồ sơ</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <i class="fa fa-user"></i>
                            <h3 class="box-title">Thông tin liên hệ</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="full_name">Full name</label>
                                <input type="text" value="<?php echo $order['full_name']; ?>" class="form-control" name="full_name">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="email">Email</label>
                                <input type="text" value="<?php echo $order['email']; ?>" class="form-control" name="email">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="phone">Phone number</label>
                                <input type="text" value="<?php echo $order['phone']; ?>" class="form-control" name="phone">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="appointment">Hẹn khách ngày trả hồ sơ</label>
                                <div class="input-group field-date">
                                    <input type='text' class="form-control" id='datetimepicker' name="appointment"/>
                                    <label class="input-group-addon btn" for="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="address">Address</label>
                                <input type="text" value="<?php echo $order['address']; ?>" class="form-control" name="address">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="vat">Mã số thuế</label>
                                <input class="form-control" name="vat" value="<?php echo $order['vat']; ?>">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="who_consultant">NV tư vấn</label>
                                <select name="who_consultant" class="form-control valid">
                                    <option value="">Chọn nhân viên tư vấn</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>" <?php if ($value['username'] == $order['who_consultant']) echo 'selected style="color:#ff0000"'; ?>><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="who_censor">NV xử lý</label>
                                <select name="who_censor" class="form-control valid">
                                    <option value="">Chọn nhân viên xử lý</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>" <?php if ($value['username'] == $order['who_censor']) echo 'selected style="color:#ff0000"'; ?>><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="who_censor">Ngày nhận hồ sơ</label>
                                <input value="<?php echo $order['date_create']; ?>" disabled="" class="form-control valid">
                            </div>
                        </div> 
                    </div>

                    <?php echo $load_file; ?>
                </div>

                <div  class="col-md-8"> 

                    <div class="box box-warning">
                        <div class="box-header">
                            <i class="ion ion-clipboard"></i>
                            <h3 class="box-title">Danh sách hộ chiếu</h3>
                            <div class="box-tools pull-right">
                                <a href="" class="btn btn-warning" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Thêm hồ sơ</a>
                            </div> 
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Full name</th>
                                        <th style="width: 100px">Gender</th>
                                        <th style="width: 140px">Birth date</th>

                                        <th style="width: 150px">Passport number</th>
                                        <th style="width: 90px">Action</th>
                                        <th>Sailor Moon</th>
                                    </tr>
                                    <?php foreach ($order_sku as $i => $p): ?>
                                        <tr>
                                            <td><?php echo $i ?>.</td>
                                            <td><span><?php echo $p['full_name']; ?></span><i class="fa fa-edit fa-lg" rel="full_name" title="Sữa"></i></td>
                                            <td><span><?php echo $p['gender']; ?></span><i class="fa fa-edit fa-lg" rel="gender" title="Sữa"></i></td>
                                            <td><span><?php echo date('d/m/Y', strtotime($p['date_of_birth'])); ?></span><i class="fa fa-edit fa-lg" rel="date_of_birth" title="Sữa"></i></td>
                                            <td><span><?php echo $p['passport_number']; ?></span><i rel="passport_number" class="fa fa-edit fa-lg" title="Sữa"></i></td>
                                            <td class="actions">
                                                <!--  <i class="fa fa-edit fa-lg" title="Sữa"></i>
                                                 <i class="fa fa-save fa-lg" title="Lưu"></i>-->
                                                <i class="fa fa-trash-o fa-lg" title="Xóa"></i>
                                            </td>
                                            <td>
                                                <i class="fa fa-book fa-lg" style="color: #424242;" title="In hợp đồng lao động"></i>
                                                <i class="fa fa-cc-visa fa-lg" style="color: #424242;" title="Điền form visa"></i>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>


                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- Chat box -->
                    <?php echo $load_chat; ?>
                    <!-- /.box (chat box) -->

                </div>
            </div>

            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="0" name="submit" class="btn btn-info">Lưu thông tin</button>

                        <?php if ($order['services'] == 'VOA'): ?>
                            <button type="submit" value="1" name="submit" class="btn btn-success"><i class="fa fa-repeat"></i> Chuyển hồ sơ</button>
                        <?php endif; ?>
                        <?php if (!empty($user['role_network'])): ?>
                            <div class="box-tools pull-right">
                                <span>agency: <b><?php echo $order['agency']; ?></b></span>
                                <span>source: <b><?php echo $order['source']; ?></b></span>
                                <span>campaign: <b><?php echo $order['campaign']; ?></b></span>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

        </form>
    </section>

</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Thêm chi phí cần chi ra</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="full_name">Họ và tên</label>
                        <input type="text" class="form-control" id="full_name" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="gender">Giới tính</label>
                        <select class="form-control" id="gender">
                            <option value="">-- Chọn giới tính --</option>
                            <option value="male">Nam</option>
                            <option value="female">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="date_of_birth">Ngày sinh</label>
                        <div class="input-group field-date">
                            <input type="text" class="form-control" id="date_of_birth" value="">
                            <label class="input-group-addon btn" for="date_of_birth">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="address_vn">Địa chỉ ở VN</label>
                        <input type="text" class="form-control" id="address_vn" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="nationality">Quốc tịch</label>
                        <select class="form-control" id="nationality">
                            <option value="">-- Chọn quốc tịch --</option>
                            <option value="male">Nam</option>
                            <option value="female">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="passport_number">Số hộ chiếu</label>
                        <input type="text" class="form-control" id="passport_number" placeholder="">
                    </div>

                    <div class="form-group col-md-6">
                        <label for="purpose_of_visit">Loại visa</label>
                        <select class="form-control" id="purpose_of_visit">
                            <option value="">-- Visa hiện tại của bạn --</option>
                            <option value="male">Du lịch (DL)</option>
                            <option value="female">Thăm thân (TT, VR)</option>
                            <option value="female">Đầu tư (DT)</option>
                            <option value="female">Lao động (LĐ)</option>
                            <option value="female">Công Tác (DN)</option>
                            <option value="female">Du học sinh (DH)</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exp_date_visa">Ngày hết hạn</label>
                        <div class="input-group field-date">
                            <input type="text" class="form-control" id="exp_date_visa" value="">
                            <label class="input-group-addon btn" for="appointment">
                                <span class="fa fa-calendar"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="literacy">Trình độ học vấn</label>
                        <select class="form-control" id="literacy">
                            <option value="">-- Vui lòng chọn --</option>
                            <option value="male">Không có</option>
                            <option value="female">Đại học</option>
                            <option value="male">Cao đẳng</option>
                            <option value="female">Chứng chỉ nghề</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="literacy">Giấy chứng nhận kinh nghiệp làm việc</label>
                        <p>Bạn cần xin từ công ty mình đã làm việc sau cho đủ 5 năm.</p>
                        <select class="form-control" id="literacy">
                            <option value="">-- Vui lòng chọn --</option>
                            <option value="male">Không xin được</option>
                            <option value="female">Xin được</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="purpose_of_visit">Công ty bảo lãnh</label>
                        <select class="form-control" id="purpose_of_visit">
                            <option value="">-- Vui lòn chọn --</option>
                            <option value="male">Có</option>
                            <option value="female">Không</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exp_date_visa">Mã số thuê</label>
                        <input type="text" class="form-control" id="full_name" placeholder="">
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="/assets/admin/plugins/datepicker/datepicker3.css">
<script src="/assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
                                $('#myModal').on('shown.bs.modal', function () {
                                    //$('#myInput').focus()
                                })
<?php if ($order['appointment']): ?>
                                    var appointment = '<?php echo date('d/m/Y', strtotime($order['appointment'])); ?>';
<?php endif; ?>
                                $('#datetimepicker').datepicker({autoclose: true});
                                $('#datetimepicker').datepicker('update', appointment);
</script>

<style>
    .box-tools span{
        display: inline-block;
        padding: 2px 10px;
        font-size: 20px;
    }
    .box-tools span b{
        color: red
    }
    #passport td{
        position: relative;
        padding-right: 30px;

    }
    #passport input{
        padding: 0 5px;
    }
    #passport i{
        margin: 0 3px;
        cursor: pointer;
        color: #ccc;
    }
    #passport i:hover{
        color: red
    }
    .fa-save{
        display: none;
    }
    #passport .fa-edit{
        position: absolute;
        top: 12px;
        right: 5px;
        display: none;
    }
    #passport .fa-times-circle{
        position: absolute;
        top: 12px;
        right: 5px;
    }
    .field-date-data .fa-calendar{
        position: absolute;
        right: 5px;
        top: 5px;
    }
    .date_of_birth{
        padding-right: 20px;
    }
    .actions{
        padding-right: 0px !important;
    }
    .filter_chat span{
        padding: 2px 5px;
        cursor: pointer;
        background: #f4f4f4;
    }
    .filter_chat span:hover{
        background: #ccc;
    }
    .filter_chat .active{
        background: #00a65a;
        color: #fff;
    }
</style>