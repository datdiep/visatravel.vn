<div class="box box-primary box-warning" id="profile_<?php echo $profile['id']; ?>">
    <div class="box-header with-border">
	<h3 class="box-title"><i class="fa fa-user"></i> <?php echo $profile['gioi_tinh'] == 'male' ? 'Anh' : 'Chị'; ?> <b><?php echo $profile['ho_ten']; ?></b> <span>&nbsp;&nbsp;&nbsp;  <a href="tel:0943000300"><i class="fa fa-phone-square"></i> <?php echo $profile['so_dien_thoai']; ?></a></span></h3>
	<div class="box-tools">
	    <button type="button" class="btn btn-warning btn-sm btn_profile" rel_id="<?php echo $profile['id']; ?>"><i class="fa fa-edit"></i> Cập nhật thông tin</button>
	    <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-minus"></i></button>
	    <button type="button" class="btn btn-default btn-sm del_profile" data_id="<?php echo $profile['id']; ?>"><i class="fa fa-trash"></i></button>
	</div>
    </div>

    <div class="box-body">
	<div class="invoice-info">
	    <div class="col-md-6 nopadding">
		<div class="col-md-4"><b>Ngày sinh:</b> <?php echo format_date($profile['ngay_sinh'],'d/m/Y'); ?></div>
		<div class="col-md-4"><b>Nơi sinh:</b> <?php echo $profile['noi_sinh']; ?></div>
		<div class="col-md-4"><b>Quốc tịch:</b> <?php echo $profile['quoc_tich']; ?></div>
		<div class="col-md-4"><b>Số CMND:</b> <?php echo $profile['cmnd']; ?></div>
		<div class="col-md-4"><b>Ngày cấp:</b> <?php echo format_date($profile['ngay_cap_cmnd'],'d/m/Y'); ?></div>
		<div class="col-md-4"><b>Nơi cấp:</b> <?php echo $profile['noi_cap_cmnd']; ?></div>

		<div class="col-md-4"><b>Số hộ chiếu:</b> <?php echo $profile['so_ho_chieu']; ?></div>
		<div class="col-md-4"><b>Ngày Cấp:</b> <?php echo format_date($profile['ngay_cap_ho_chieu'],'d/m/Y'); ?></div>
		<div class="col-md-4"><b>Ngày hết hạn:</b> <?php echo format_date($profile['ngay_het_han_ho_chieu'],'d/m/Y'); ?></div>

		<div class="col-md-6"><b>Nghề nghiệp:</b> <?php echo $profile['nghe_nghiep']; ?></div>
		<div class="col-md-6"><b>Mã số thuế:</b> <?php echo $profile['ma_so_thue']; ?></div>
		<div class="col-md-6"><b>Địa chỉ thường trú:</b> <?php echo $profile['dia_chi_thuong_tru']; ?></div>
		<div class="col-md-6"><b>Địa chỉ tạm trú:</b> <?php echo $profile['dia_chi_tam_tru']; ?></div>

		<div class="col-md-12">
		    <?php
		    $visa_hien_co = unserialize($profile['visa_hien_co']);
		    $thong_tin_gia_dinh = unserialize($profile['thong_tin_gia_dinh']);
		    $danh_sach_dich_vu = unserialize($profile['danh_sach_dich_vu']);
		    ?>
		    <table class="table table-bordered">
			<thead>
			    <tr>
				<th>Họ và tên gia đình</th>
				<th>Ngày sinh</th>
				<th>Mối quan hệ</th>
			    </tr>
			</thead>
			<tbody>
			    <?php foreach ($thong_tin_gia_dinh as $gd): ?>
    			    <tr>
    				<td><?php echo $gd['ho_ten_gia_dinh']; ?></td>
    				<td><?php echo $gd['ngay_sinh']; ?></td>
    				<td><?php echo $gd['moi_quan_he']; ?></td>
    			    </tr>
			    <?php endforeach; ?>
			</tbody>
		    </table>

		</div>
	    </div>

	    <div class="col-md-6 table-responsive">
		<table class="table table-striped">
		    <thead>
			<tr>
			    <th>Visa</th>
			    <th>Mã visa</th>
			    <th>Ngày cấp</th>
			    <th>Ngày hết hạn</th>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($visa_hien_co as $vs): ?>
    			<tr>
    			    <td><?php echo $vs['visa']; ?></td>
    			    <td><b><?php echo $vs['ma_visa']; ?></b></td>
    			    <td><?php echo $vs['ngay_cap']; ?></td>
    			    <td><?php echo $vs['ngay_het_han']; ?></td>
    			</tr>
			<?php endforeach; ?>
		    </tbody>
		</table>
		<hr>
		<table class="table table-striped">
		    <thead>
			<tr>
			    <th>Dịch vu</th>
			    <th>Ngày làm</th>
			    <th>Ngày hoàn thành</th>
			    <th>Trạng thái</th>
			    <th>Ghi chú</th>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($danh_sach_dich_vu as $dv): ?>
    			<tr class="text-muted">
    			    <td><?php echo $dv['dich_vu']; ?></td>
    			    <td><?php echo $dv['ngay_lam']; ?></td>
    			    <td><?php echo $dv['ngay_hoan_thanh']; ?></td>
    			    <td><?php echo $dv['trang_thai']; ?></td>
    			    <td><?php echo $dv['ghi_chu']; ?></td>
    			</tr>
			<?php endforeach; ?>

		    </tbody>
		</table>
		<p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
		    <?php echo!empty($profile['ghi_chu']) ? $profile['ghi_chu'] : 'Chưa có ghi chú cho hồ sơ này'; ?>
		</p>
	    </div>
	</div>
    </div> 

</div>