<?php $this->load->library('transload'); ?>
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <?php if (!empty($user['role_network'])): ?>
                        <th>ID</th>
                    <?php endif; ?>    

                    <th>Tên</th>
                    <th>Loại dịch vụ</th>
                    <th>SL</th>
                    <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false): ?>
                        <th>Email</th>
                        <th>Điện thoại</th>
                    <?php endif; ?>   
                    <th>Tổng tiền</th>
                    <th>Tiến trình</th>
                    <th>NV tư vấn</th>
                    <th>NV xử lý</th>
                    <th>Lời nhắn</th>
                    <th style="text-align: center">Thực thi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $item): ?>
                    <tr>
                        <?php if (!empty($user['role_network'])): ?>
                            <td><span class="color-palette" style="padding: 1px 5px" title="<?php echo $item['date_create']; ?>"><?php echo $item['id']; ?></span></td>
                        <?php endif; ?>


                        <td><a href="/admincp/order/process_view/<?php echo $item['id']; ?>"><?php echo $item['full_name']; ?></a></td>      
                        <td><?php echo @$map_services[$item['services']]; ?></td>
                        <td><?php echo $item['pax']; ?></td>                       
                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false): ?>
                            <td><?php echo $item['email']; ?></td>
                            <td><?php echo $item['phone']; ?></td>
                            <td><?php echo number_format($item['total_fees']); ?></td>
                        <?php endif; ?>

                        <td>
                            <?php echo $map_order[$item['progress']]; ?> 

                        </td>
                        <td><?php echo $item['who_consultant']; ?> </td>
                        <td><?php echo $item['who_censor']; ?> </td>

                        <td><?php echo nl2br($item['leave_message']); ?></td>
                        <td style="text-align: center">
                            <?php if (empty($item['who_check']) || $item['who_check'] == $user['username']): ?>

                                <?php if (empty($item['who_handle']) || $item['who_handle'] == $user['username']): ?>

                                    <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false && $item['status'] == 'handle'): ?>
                                        <a class="btn btn-default" href="order/check/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i>  Kiểm tra</a>
                                    <?php else: ?>
                                        <a class="btn btn-default" href="order/view/<?php echo $item['id']; ?>"><i class="fa fa-eye"></i> Xem</a>
                                    <?php endif; ?>

                                    <?php if ((strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false || !empty($user['role_network'])) && $item['status'] != 'cancel'): ?>                           
                                        <a  class="btn btn-default" onclick="cancel(<?php echo $item['id']; ?>)" href="javascript:void(0)" title="Hủy đơn hàng"><i class="fa fa-ban"></i></a>
                                    <?php endif; ?>

                                    <?php if (((strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false || !empty($user['role_network'])) && ($item['status'] == 'cancel' || $item['status'] == 'trash')) || ($item['status'] == 'deny') && strpos(',' . $user['role_order_progress'] . ',', ',censor,') !== false): ?>
                                        <a  class="btn btn-default" onclick="uncancel(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-reply-all"></i> Phục hồi</a>
                                    <?php endif; ?>

                                    <?php if (strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false && $item['status'] == 'cancel'): ?>
                                        <a  class="btn btn-default" onclick="trash(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Xóa</a>
                                    <?php endif; ?>

                                    <?php if (strpos(',' . $user['role_order_progress'] . ',', ',empty,') !== false && $item['status'] == 'trash'): ?>
                                        <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a class="btn btn-default" href="order/view/<?php echo $item['id']; ?>"><i class="fa fa-eye"></i> Xem</a>

                                <?php endif; ?>

                            <?php elseif (empty($item['who_censor']) || $item['who_censor'] == $user['username']): ?>

                                <a class="btn btn-default" href="order/view/<?php echo $item['id']; ?>"><i class="fa fa-eye"></i> Xem</a>
                                <?php if ((strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false || !empty($user['role_network'])) && ($item['status'] == 'cancel' || $item['status'] == 'trash')): ?>
                                    <a  class="btn btn-default" onclick="uncancel(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-reply-all"></i> Phục hồi</a>
                                <?php endif; ?>
                                <?php if (strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false && $item['status'] == 'cancel'): ?>
                                    <a  class="btn btn-default" onclick="trash(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Xóa</a>
                                <?php endif; ?>
                                <?php if (strpos(',' . $user['role_order_progress'] . ',', ',empty,') !== false && $item['status'] == 'trash'): ?>
                                    <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                                <?php endif; ?>
                            <?php endif; ?>

                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>      


        </table>

    </div>
    <div class="col-md-4 col-md-offset-5">
        <ul class="pagination">
            <?php echo $links; ?>
        </ul>
    </div>

</div>

