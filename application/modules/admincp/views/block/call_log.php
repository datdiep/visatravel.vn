<div class="col-md-12">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header">
                <i class="fa fa-phone-square" aria-hidden="true"></i>

                <h3 class="box-title" style="padding-bottom: 10px">Quản lý cuộc gọi gần đây <span style="color:#ff0000">(Đang làm)</span></h3>

                <div class="input-group col-md-5 pull-right">
                    <input type="text" placeholder="Tìm số đt khách" class="input form-control">
                    <span class="input-group-btn">
                        <button type="button" class="btn btn btn-primary"> <i class="fa fa-search"></i> Tìm</button>
                    </span>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="ibox">
                    <div class="ibox-content">

                        <p style="margin-top:-10px; padding: 10px">
                            <i class="fa fa-clock-o"></i> Khách hàng là nguồn sống của cty. Và cũng tạo ra thu nhập của <?php echo $user['fullname']; ?>, vì vậy hãy cố gắng nhé!
                        </p>

                        <div class="clients-list">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-mobile" aria-hidden="true"></i> Gọi nhỡ</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-phone" aria-hidden="true"></i> Gọi đến</a></li>
                                <li class=""><a data-toggle="tab" href="#tab-2"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i> Tiềm năng</a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <div class="slimScrollDiv"><div class="full-height-scroll">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a href="#contact-1" class="client-link">0909701186</a></td>
                                                            <td> 10:45am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"><i class="fa fa-phone"> </i></td>
                                                            <td> Hồng Sơn</td>
                                                            <td class="client-status"><span class="label label-primary">Tiềm năng</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-4" class="client-link">0978885599</a></td>
                                                            <td>09:45am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"><i class="fa fa-phone"> </i></td>
                                                            <td> Anh Minh</td>
                                                            <td class="client-status"><span class="label label-warning">Tiềm năng</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-3" class="client-link">0962754232</a></td>
                                                            <td>12:10am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"></td>
                                                            <td> </td>
                                                            <td class="client-status"><span class="label label-info">Úc</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-1" class="client-link">0978885599</a></td>
                                                            <td> 12:22am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"></td>
                                                            <td> </td>
                                                            <td class="client-status"><span class="label label-danger">Mỹ</span></td>
                                                        </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 365.112px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 100%;"><div class="full-height-scroll" style="overflow: hidden; width: auto; height: 100%;">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover">
                                                    <tbody>
                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a href="#contact-1" class="client-link">0909701186</a></td>
                                                            <td> 10:45am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"><i class="fa fa-phone"> </i></td>
                                                            <td> Hồng Sơn</td>
                                                            <td class="client-status"><span class="label label-primary">Tiềm năng</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-4" class="client-link">0978885599</a></td>
                                                            <td>09:45am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"><i class="fa fa-phone"> </i></td>
                                                            <td> Anh Minh</td>
                                                            <td class="client-status"><span class="label label-warning">Tiềm năng</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-3" class="client-link">0962754232</a></td>
                                                            <td>12:10am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"></td>
                                                            <td> </td>
                                                            <td class="client-status"><span class="label label-info">Úc</span></td>
                                                        </tr>

                                                        <tr>
                                                            <td><input type="checkbox" value=""></td>
                                                            <td><a data-toggle="tab" href="#contact-1" class="client-link">0978885599</a></td>
                                                            <td> 12:22am <span class="c-gray">(26/12/2017)</span></td>
                                                            <td class="contact-type"></td>
                                                            <td> </td>
                                                            <td class="client-status"><span class="label label-danger">Mỹ</span></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>

</div>