<?php foreach ($list_note as $key => $item): ?>
<div class="panel box"  id="note_<?php echo $item['id'];?>" rel_id="<?php echo $item['id'];?>">
    <div class="box-header with-border <?php echo $key == 0 ? '':'collapsed'?>" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $item['id'];?>">
        <h4 class="box-title">
            <i class="fa fa-chevron-circle-down"></i><i class="fa fa-chevron-circle-right"></i>
            <input type="text" name="title" value="<?php echo $item['title'];?>" />
        </h4>
    </div>
    <div id="collapse_<?php echo $item['id'];?>" class="panel-collapse collapse <?php echo $key == 0 ? 'in':''?>">
        <div class="box-body">
            <?php if($item['type'] == 'text'):?>
            <span class="text alone" placeholder="Nội dung..." contenteditable="true"><?php echo $item['content'];?></span>
            <?php else:?>
            <?php $list_item = explode("<br>",$item['content']); array_pop($list_item);?>
            <ul class="todo-list ui-sortable">
                <?php foreach ($list_item as $v): ?>
                <li <?php echo strpos($v,"<strike>") !== false ? 'style="opacity:0.5"':'';?>>
                    <input type="checkbox" <?php echo strpos($v,"<strike>") !== false ? 'checked':'';?>>
                    <span class="text" placeholder="Nội dung..." contenteditable="true"><?php echo $v;?></span>
                    <i class="fa fa-trash-o remove_checknote"></i>
                </li>
                <?php endforeach;?>
            </ul>
            <?php endif;?>
        </div>
        <div class="box-footer">
            <div class="btn-group pull-right">
                <button type=button" onclick="change_note(<?php echo $item['id'];?>, 'check')" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-check-square-o"></i> Hộp kiểm</button>
                <button type=button" onclick="change_note(<?php echo $item['id'];?>, 'text')" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-edit"></i> Bảng ghi</button>
                <button type=button" onclick="del(<?php echo $item['id'];?>)" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-trash-o"> Xóa</i></button>
                <button type=button" onclick="save(<?php echo $item['id'];?>)" class="btn btn-success btn-xs"><i class="fa fa-save"></i> Lưu</button>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>