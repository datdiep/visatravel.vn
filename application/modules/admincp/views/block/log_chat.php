<div class="box box-success">
    <div class="box-header with-border">
        <i class="fa fa-comments-o"></i>

        <h3 class="box-title">Ghi chú</h3>

        <div class="pull-right filter_chat">
            <span class="active" rel="all"><i class="fa fa-filter"></i> All</span>
            <span rel="user"><i class="fa fa-filter" rel="user"></i> User</span>
            <span rel="system"><i class="fa fa-filter"></i> System</span>
        </div>
    </div>
    <div class="box-body chat" id="chat-box">
        <?php if (!empty($log)): ?>
            <?php foreach ($log as $k => $p): ?>
                <div class="item" user="<?php echo $p['type']; ?>">
                    <img src="/assets/upload/avatar/<?php echo $p['avatar']; ?>" alt="user image" class="online">
                    <p class="message">
                        <a href="#" class="name">
                            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo date('d-m H:i:s', strtotime($p['date_create'])); ?></small>
                            <?php echo $p['name'] == 'system' ? '<span style="color:red">' . $p['name'] . '</span>' : $p['name']; ?>
                        </a>
                        <?php echo $p['content']; ?>
                    </p>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <?php
            $msg = 'Chào <b>' . $user['username'] . '</b> .Thời gian qua rất nhanh, vì vậy hãy làm việc vui vẻ và chuyên cần nhé.';
            if (!empty($msg_error)) {
                $msg = 'Phát hiện lỗi, vui lòng báo cho quản trị.';
            }
            ?>
            <div class="item" user="system">
                <img src="/assets/upload/avatar/system.jpg" alt="user image" class="offline">
                <p class="message">
                    <a class="name">
                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo date('d-m H:i:s'); ?></small>
                        <span style="color:red">System</span>
                    </a>
                    <?php echo $msg; ?>
                </p>
            </div>
        <?php endif; ?>

    </div>
    <!-- /.chat -->
    <div class="box-footer">
        <div class="input-group">
            <input class="form-control" id="submit_chat" placeholder="Type message...">
            <div class="input-group-btn">
                <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- /.box (chat box) -->
<script>
    var order_id = <?php echo $block_file['id']; ?>;
    var model = "<?php echo $block_file['model']; ?>";
    var url = "<?php echo current_url(); ?>";
    var html_chat = $('#chat-box').html();
    var bottomCoord = $('#chat-box').height();
    $('#chat-box').slimScroll({
        height: '220px',
        start: 'bottom',
    });
    $('#submit_chat').on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            var target = $(event.target);
            var val = target.val();
            target.val('');
            if (val) {
                $.post('/admincp/ajax/updata_chat', {msg: val, id: order_id, model: model, url: url}, function (results) {
                    html_chat += results;
                    $('#chat-box').append(results);
                    var bottomCoord = $('#chat-box')[0].scrollHeight;
                    console.log(bottomCoord);
                    $('#chat-box').slimScroll({scrollTo: bottomCoord});
                })
            }
            e.preventDefault();
            return false;
        }
    });
    $('.filter_chat span').click(function () {
        $('.filter_chat span').removeClass('active');
        $(this).addClass('active');
        var rel = $(this).attr('rel');
        if (rel == 'all') {
            $('#chat-box').html(html_chat);
            var bottomCoord = $('#chat-box')[0].scrollHeight;
            $('#chat-box').slimScroll({scrollTo: bottomCoord});
        } else {
            $.when(get_data(rel)).done(function () {
                $('#chat-box').html(html_chat_new);
                var bottomCoord = $('#chat-box')[0].scrollHeight;
                $('#chat-box').slimScroll({scrollTo: bottomCoord});
            });
        }
    })
    var html_chat_new = '';
    function get_data(rel) {
        html_chat_new = '';
        $(html_chat).filter('.item').each(function (e) {
            var user = $(this).attr('user');
            if (rel == 'system') {
                if (user == 'system') {
                    html_chat_new += $(this).prop('outerHTML');
                }
            } else {
                if (user != 'system') {
                    html_chat_new += $(this).prop('outerHTML');
                }
            }
        });
    }
</script>
<style>
    .filter_chat span{
        padding: 2px 5px;
        cursor: pointer;
        background: #f4f4f4;
    }
    .filter_chat span:hover{
        background: #ccc;
    }
    .filter_chat .active{
        background: #00a65a;
        color: #fff;
    }

</style>