<div class="box" id="list_note">
    <div class="box-header with-border">
        <i class="ion ion-clipboard"></i>
        <h3 class="box-title">Ghi chú công việc</h3>
        <button class="btn btn-default pull-right add_note btn-xs"><i class="fa fa-plus"></i> Thêm ghi chú</button>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="box-group" id="accordion">
            <?php if(!empty($list_note)):?>
                <?php foreach ($list_note as $key => $item): ?>
            <div class="panel box"  id="note_<?php echo $item['id'];?>" rel_id="<?php echo $item['id'];?>">
                <div class="box-header with-border <?php echo $key == 0 ? '':'collapsed';?>" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $item['id'];?>" aria-expanded="<?php echo $key == 0 ? 'true':'false';?>">
                    <h4 class="box-title">
                        <i class="fa fa-chevron-circle-down"></i><i class="fa fa-chevron-circle-right"></i>
                        <input type="text" disabled name="title" spellcheck="false" value="<?php echo $item['title'];?>" />
                        <span class="pull-right"><i class="fa fa-edit"></i></span>
                    </h4>
                </div>
                <div id="collapse_<?php echo $item['id'];?>" class="panel-collapse collapse <?php echo $key == 0 ? 'in':''?>" aria-expanded="<?php echo $key == 0 ? 'true':'false';?>">
                    <div class="box-body">
                        <?php if($item['type'] == 'text'):?>
                        <span class="text alone" placeholder="Nội dung..." contenteditable="true" spellcheck="false"><?php echo $item['content'];?></span>
                        <?php else:?>
                        <?php $list_item = explode("<br>",$item['content']); array_pop($list_item);?>
                        <ul class="todo-list ui-sortable">
                            <?php foreach ($list_item as $v): ?>
                            <li <?php echo strpos($v,"<strike>") !== false ? 'style="opacity:0.5"':'';?>>
                                <input type="checkbox" <?php echo strpos($v,"<strike>") !== false ? 'checked':'';?>>
                                <span class="text" placeholder="Nội dung..." contenteditable="true" spellcheck="false"><?php echo $v;?></span>
                                <i class="fa fa-trash-o remove_checknote"></i>
                            </li>
                            <?php endforeach;?>
                        </ul>
                        <?php endif;?>
                    </div>
                    <div class="box-footer">
                        <div class="btn-group pull-right">
                            <button type=button" onclick="change_note(<?php echo $item['id'];?>, 'check')" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-check-square-o"></i> Hộp kiểm</button>
                            <button type=button" onclick="change_note(<?php echo $item['id'];?>, 'text')" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-edit"></i> Bảng ghi</button>
                            <button type=button" onclick="del(<?php echo $item['id'];?>)" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-trash-o"></i> Xóa</button>
                            <button type=button" onclick="save(<?php echo $item['id'];?>)" class="save_note btn btn-success btn-xs"><i class="fa fa-save"></i> Lưu</button>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
            <?php endif;?>
        </div>
    </div>
<!--    <div class="box-footer">
        <ul class="pagination pagination-sm inline">
            <li><a href="#">«</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">»</a></li>
        </ul>
        <div class="box-tools pull-right" data-toggle="tooltip" title="" data-original-title="Status">
            <div class="btn-group" data-toggle="btn-toggle">
                <button type="button" class="btn btn-default btn-sm active"><i class="fa fa-square text-green"></i>
                </button>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-square text-red"></i></button>
            </div>
        </div>
        
    </div>-->
    <!-- /.box-body -->
</div>
<div id="temp_html_note"><?php echo $this->load->view('block/html_note', '', true);?></div>
<script type="text/javascript">
    var html_note = $('#temp_html_note').html();
    $('#temp_html_note').remove();
    $('.add_note').click(function(){
        if($('#note_0').length == 0){
            $('.panel-collapse[aria-expanded="true"]').removeClass('in');
            $('.box-header[aria-expanded="true"]').addClass('collapsed');
            $('.panel-collapse[aria-expanded="true"]').attr('aria-expanded',false);
            $('.box-header[aria-expanded="true"]').attr('aria-expanded',false);
            $('#accordion').prepend(html_note);
            $('#accordion input').first().focus();
            //$('#accordion .in').collapse('hide');
        }
    })
    $('#list_note').on('click','.color-chooser li',function(){
        var id = $(this).attr('rel_id');
        var c = $(this).attr('rel_c');
        $('#note_'+id).removeClass();
        $('#note_'+id).addClass('panel box '+c);
    })
    $('#list_note').on('click','.remove_checknote',function(){
        var ele = $(this).parent().parent().parent();
        var p = $(this).parent().parent().parent().parent().parent();
        if($('li',ele).length > 1){
            $(this).parent().remove();
            var id = p.attr('rel_id');
            if(id > 0){
                save(id);
            }
        }
    })
    $('#list_note').on('click','.change_note',function(){
        var method = $(this).attr('method');
        var id = $(this).parent().attr('rel_id');
        var text = $('#change_txt_'+id).text();
        if(method == 'check' && text == 'Bảng ghi'){
            $('#change_txt_'+id).text('Hộp kiểm');
            change_note(id,method);
        }
        if(method == 'text' && text == 'Hộp kiểm'){
             $('#change_txt_'+id).text('Bảng ghi');
             change_note(id,method);
        }
    })
    $('#list_note').on('click','input[type="checkbox"]',function(){
        var txt = $(this).next().text();
        var p = $(this).parent().parent().parent().parent().parent();
        if ($(this).is(":checked")){
            $(this).next().html('<strike>'+txt+'</strike>');
            $(this).parent().css('opacity','0.5');
        }else{
            $(this).parent().css('opacity','1');
            $(this).next().html(txt);
        }
        var id = p.attr('rel_id');
        if(id > 0){
            save(id);
        }
    })
//    $('.panel').on('click','.text',function(){
//        $(this).attr('contenteditable','true');
//    });
    $('body').on('keydown', function(e){
        if(e.ctrlKey && e.which === 83){ // Check for the Ctrl key being pressed, and if the key = [S] (83)
            //console.log('Ctrl+S!');
//            var id = $(this).attr('rel_id');
//            save(id);
//            e.preventDefault();
//            return false;
        }
    });
    $('#list_note').on('keydown','#accordion .todo-list .text',function(e){
        if(e.keyCode == 13){
            add_checknote($(this));
            e.preventDefault();
        }
    });
    $('#list_note').on('keydown','input',function(e){
        if(e.keyCode == 13){
            var ele = $(this).parent().parent().parent();
            $('.text',ele).focus();
            e.preventDefault();
        }
    });
    $('#list_note').on('dblclick','.alone',function(e){
        document.execCommand('strikeThrough');
    });
    $('#list_note').on('click','.box-header[aria-expanded="true"] input[name="title"]', function (e) {
        e.stopPropagation();
    });
    $('#list_note').on('click','#accordion .box-title .pull-right', function (e) {   
        $('.box-header[aria-expanded="true"] input[name="title"]').prop('disabled',false);
        $('.box-header[aria-expanded="true"] input[name="title"]').putCursorAtEnd();
        e.stopPropagation();
    });
    $("#list_note").on('hide.bs.collapse', function(){
        $('.box-header[aria-expanded="false"] input[name="title"]',this).prop('disabled',true);
    });
//    $("#list_note").on('shown.bs.collapse', function(){
//        $('.box-header[aria-expanded="true"] input[name="title"]',this).prop('disabled',false);
//        //$('input[name="title"]',parent).putCursorAtEnd();
//    });
    function save(id){
        var data = {};
        var ele = $('#note_'+id);
        data["content"] = '';
        data["title"] = $('input[name="title"]',ele).val();
        if(data["title"] == ''){
            alert('Tiều đề không được bỏ trống');
            $('input[name="title"]',ele).focus();
            return false;
        }
        if($('.text',ele).html() == '' || $('.text',ele).html() == '<br>'){
            alert('Không tồn tại nội dung để lưu');
            $('.text',ele).focus();
            return false;
        }
        $('.loading',ele).fadeIn();
        if($('#note_'+id+' .text').length == 1){
            data["content"] = $('#note_'+id+' .text').html();
            data["type"] = 'text';
        }else{
            $('.text', ele).each(function () {
                var val = $(this).html();
                if(val != "")
                    data["content"] += val+'<br>';
            })
            data["type"] = 'check';
        }
        $.post('/<?php echo ADMIN_URL; ?>dashboard/save_note', {data: data,id:id}, function (results) {
            var time = moment().format('h:mm a');
            if(id == 0){
                _alert('Lúc '+time+': Thêm ghi chú thành công.',$('#note_0 .save_note'));
                $('#note_0').attr({'id':'note_'+results,'rel_id':results});
                $('#note_'+results).removeClass('box-danger');
                $('#note_'+results+' .box-title .pull-right').css('display','block');
                $('#note_'+results+' .box-title input').prop('disabled',true);
                $('#note_'+results+' .box-header').attr({'data-toggle':'collapse','data-parent':'#accordion','href':'#collapse_'+results,'aria-expanded':true});
                $('#note_'+results+' .panel-collapse').attr({'id':'collapse_'+results,'class':'panel-collapse collapse in','aria-expanded':true});
                $('#note_'+results+' .change_check').attr({'onclick':'change_note('+results+', "check")'});
                $('#note_'+results+' .change_text').attr({'onclick':'change_note('+results+', "text")'});
                $('#note_'+results+' .save_note').attr({'onclick':'save('+results+')'});
                $('#note_'+results+' .close_note').remove();
                $('#note_'+results+' .del_note').attr({'onclick':'del('+results+')'});
                $('#note_'+results+' .del_note').removeClass('del_note');
            }else{
                _alert('Lúc '+time+': Cập nhật ghi chú #'+results+' thành công.',$('#note_'+results+' .save_note'));
            }
        });
    }
    function _alert(msg, _this){
        var html = '<i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Đã lưu';
        //var html = '<div class="alert_note alert alert-success alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-check"></i> '+msg+'</div>';
        _this.html(html);
        //$('.alert_note').fadeIn(2000);
        setTimeout(function(){
            _this.html('<i class="fa fa-save"></i> Lưu');
            //$('.alert_note').remove();
            //$('.alert_note').fadeOut(2000,function(){$(this).remove();});
        },1000)
    }
    function _empty(id){
        $.post('/<?php echo ADMIN_URL; ?>dashboard/empty_note', {id:id}, function (results) {
            if(results == 1)
                $('#note_'+id).remove();
        });
    }
    function del(id){
        show_dialog('Bạn có muốn xóa ghi chú #' + id + " không", function () {
            $.post('/<?php echo ADMIN_URL; ?>dashboard/del_note', {id:id}, function (results) {
                if(results == 1)
                    $('#note_'+id).remove();
            });
        });
    }
    function close_add(){
        $('#note_0').remove();
    }
    function add_checknote(_this){
        var html = '<li> <input type="checkbox"> <span class="text" placeholder="Nội dung..." contenteditable="true" spellcheck="false"></span> <i class="fa fa-trash-o remove_checknote"></i> </li>';
        //$('#note_'+id+' .add_checknote').before(html);
        _this.parent().after(html);
        $('.text',_this.parent().next()).focus();
    }
    
    function change_note(id,method){
        var text = "";
        $('#note_'+id+' .text').each(function () {
            var val = $(this).html();
            if(val != "")
                text += val+'<br>';
        })
        var html = '';
        if(method == 'check'){
            var res = text.split('<br>');
            html = '<ul class="todo-list ui-sortable">';
            var i = 0;
            $.each( res, function( key, value ) {
                if(value != ""){
                    i++;
                    var newHtml = $('<div></div>').html(value);
                    var check = '';
                    var opacity = 1;
                    if($(newHtml).find('strike').length > 0){
                        value = '<strike>'+newHtml.text()+'</strike>';
                        check = 'checked';
                        opacity = 0.3;
                    }
                    html += '<li style="opacity:'+opacity+'"> <input type="checkbox" '+check+'> <span class="text" placeholder="Nội dung..." contenteditable="true" spellcheck="false">'+value+'</span> <i class="fa fa-trash-o remove_checknote"></i> </li>';
                }   
            });
            if(i == 0)
                html += '<li> <input type="checkbox"> <span class="text" placeholder="Nội dung..." contenteditable="true" spellcheck="false"></span> <i class="fa fa-trash-o remove_checknote"></i> </li>';      
            html += '</ul>';
        }else{
            html = '<span class="text alone" placeholder="Nội dung..." contenteditable="true" spellcheck="false">'+text+'</span>';
        }
        $('#note_'+id+' .box-body').html(html);
        $('#note_'+id+' .box-body .text').focus();
    }
    <?php if(empty($list_note)):?>
        $('.add_note').click();
    <?php endif;?>
    
    function autosize(_this){
      setTimeout(function(){
        _this.style.cssText = 'height:auto; padding:0';
        // for box-sizing other than "content-box" use:
        _this.style.cssText = '-moz-box-sizing:content-box';
        _this.style.cssText = 'height:' + _this.scrollHeight + 'px';
      },0);
    }
jQuery.fn.putCursorAtEnd = function() {

  return this.each(function() {
    
    // Cache references
    var $el = $(this),
        el = this;

    // Only focus if input isn't already
    if (!$el.is(":focus")) {
     $el.focus();
    }

    // If this function exists... (IE 9+)
    if (el.setSelectionRange) {

      // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
      var len = $el.val().length * 2;
      
      // Timeout seems to be required for Blink
      setTimeout(function() {
        el.setSelectionRange(len, len);
      }, 1);
    
    } else {
      
      // As a fallback, replace the contents with itself
      // Doesn't work in Chrome, but Chrome supports setSelectionRange
      $el.val($el.val());
      
    }

    // Scroll to the bottom, in case we're in a tall textarea
    // (Necessary for Firefox and Chrome)
    this.scrollTop = 999999;

  });

};
</script>
<style type="text/css">
    #accordion .collapsed .pull-right{
        display: none !important;
    }
    .text{
        width: 100%;
        display: inline-block;
    }
    .text:focus{
        outline: none;
    }
    
    #accordion .box-title{
        width: 100%;
        padding-top: 2px;
        position: relative;
        padding-left: 28px;
    }
    #accordion .box-title .pull-right{
        position: absolute;
        right: -5px;
        top: -5px;
        cursor: pointer;
        background: #5191b5;
        width: 32px;
        height: 32px;
    }
    #accordion .box-title .pull-right:hover{
        background: #1f658c;
    }
    #accordion .box-title .pull-right i{
        left: 9px;
        top: 8px;
    }
    #accordion textarea:focus, #accordion input:focus{
        outline: none;
    }
    #accordion .todo-list li{
        border-left: 0px;
        padding-left: 25px;
        padding-right: 20px;
        position: relative;
        background: none;
        border-bottom: 1px dashed #e4e4e4;
    }
    #accordion .todo-list li:last-child{
        border-bottom: none;
    }
    #accordion .todo-list input[type="checkbox"]{
        position: absolute;
        top: 14px;
        left: 10px;
        margin:0px !important;
 
    }
    .add_checknote{
        padding-left: 10px !important;
        color: #ccc !important;
        cursor: pointer;
    }
    .add_checknote:hover{
        color: #777;
    }
    #accordion .todo-list>li .text {
         margin-left: 2px;
         font-weight: normal;
    }
    #accordion .todo-list .fa-trash-o {
        position: absolute;
        right: 12px;
        top: 12px;
        margin: 0px;
        cursor: pointer;
        display: none
    }
    #accordion .todo-list .fa-trash-o:hover {
        color: red; 
    }
    #accordion .todo-list li:hover .fa-trash-o{
        display: block;
    }
    #accordion .box-header i{
        position: absolute;
        top: 3px;
        left: 5px;
        color: #fff;
    }
    #accordion .box-footer{
        padding: 5px;
    }
    #accordion .box-header .fa-chevron-circle-right{
        display: none;
    }
    .loading{
        display: none;
    }
    [contenteditable=true]:empty:before{
        content: attr(placeholder);
        color:#ccc;
      }
      #accordion .box{
          border-top: 0px;
      }
      #accordion .box-header{
          padding: 5px;
          background: #65bbec;
      }
/*      #accordion .panel:nth-of-type(odd) .box-header{
          background: #f9f9f9;
      }*/
      #accordion .box-header.collapsed{
          background: #f9f9f9;
      }
      #accordion .box-header.collapsed .fa-chevron-circle-down{
          display: none;
      }
      #accordion .box-header.collapsed .fa-chevron-circle-right{
          display: block;
          color: #d8d8d8;
      }
      #accordion input[type="text"]{
            width: 100%;
            border: 0px;
            background: transparent;
            -webkit-text-fill-color:#fff;
            font-size: 16px;
        }
      #accordion .box-header.collapsed input[type="text"]{
          -webkit-text-fill-color:#222;
      }
      .del_note{
          display: none;
      }
      #accordion #note_0 .box-header{
          background: #f39c12 !important;
      }
      #accordion .alert_note {
        padding: 6px 30px 6px 10px;
        margin-bottom: 8px;
      }
      #accordion input, #accordion button, #accordion .text{
          font-family: sans-serif !important;
      }
      
/*      #accordion input {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }*/
      .save_note{
          height: 22px;
      }
      
</style>