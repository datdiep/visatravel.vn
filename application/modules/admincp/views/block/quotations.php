<div class="col-md-12 col-xs-12">
    <div class="row">
        <div class="box box-primary box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">Danh ngôn hay</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?php echo nl2br($quotations['content']); ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>