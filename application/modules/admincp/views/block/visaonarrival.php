
<?php if ($list_visa_onarrival): ?>
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Đơn hàng công văn nhập cảnh mới có</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table no-margin">
                            <thead>
                                <tr>
                                    <th>Mã đơn hàng</th>
                                    <th>Tên</th>
                                    <th>Số người</th>
                                    <th>Thanh toán</th>
                                    <th>Tổng tiền</th>
                                    <th>Tiến trình</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($list_visa_onarrival as $item): ?>
                                    <tr>
                                        <td><a href="/admincp/order/view/<?php echo $item['id']; ?>" title=""><?php echo $item['id']; ?></a>
                                            <?php
                                            $bg = 'bg-gray disabled';
                                            if ($item['processing_time'] == 'Urgent') {
                                                $bg = 'bg-yellow';
                                            }
                                            if ($item['processing_time'] == 'Emergency') {
                                                $bg = 'bg-red';
                                            }
                                            ?>
                                            <span class="<?php echo $bg; ?> color-palette" style="padding: 1px 5px"><i class="fa fa-clock-o"></i>
                                                <?php echo date('d/m/Y', strtotime($item['date_create'])); ?>
                                        </td>
                                        <td><a href="/admincp/order/view/<?php echo $item['id']; ?>" title="Xem đơn hàng <?php echo $item['id']; ?>"><?php echo $item['full_name']; ?></a></td>
                                        <td><?php echo $item['pax']; ?></td>
                                        <td><?php echo $item['paid_method'] == '' ? 'Chưa' : $item['paid_method']; ?></td>
                                        <td><?php echo $item['total_fees']; ?> $</td>
                                        <td>
                                            <?php echo $map_order[$item['progress']]; ?> 

                                            <?php if (!empty($item['who_check']) && ($user['network_type'] == 'agency' || !empty($user['role_network']))): ?>
                                                <span class="badge bg-gray">
                                                    <?php echo $item['who_check'] == $user['username'] ? '<span style="color:red">' . $item['who_check'] . '</span>' : $item['who_check']; ?>
                                                    <?php if (!empty($item['who_censor'])): ?>
                                                        <i style="color:#777" class="fa fa-retweet"></i> <?php echo $item['who_censor']; ?>
                                                    <?php endif; ?>
                                                </span>
                                                <?php if ($item['progress'] == 'receive' && $item['who_handle'] == $user['username']): ?>
                                                    <br><span class="badge bg-gray"><a href="order/check/<?php echo $item['id']; ?>">Yêu cầu bổ sung</a></span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                            <?php if ($user['network_type'] == 'issue visa'): ?>
                                                <span class="badge bg-gray">
                                                    <?php if (!empty($item['who_censor'])): ?>
                                                        <?php echo $item['who_censor'] == $user['username'] ? '<span style="color:red">' . $item['who_censor'] . '</span>' : $item['who_censor']; ?>
                                                        <i style="color:#777" class="fa fa-retweet"></i> 
                                                    <?php endif; ?>
                                                    <?php echo $item['who_check']; ?>
                                                </span>
                                                <?php if ($item['progress'] == 'receive' && $item['who_handle'] == $item['who_check'] && $item['who_censor'] == $user['username']): ?>
                                                    <br><span class="badge bg-gray"><a>Đợi bổ sung</a></span>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="/admincp/order" class="btn btn-sm btn-default btn-flat pull-right">Xem toàn bộ</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
    </div>

<?php endif; ?>

