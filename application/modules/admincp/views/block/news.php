<div class="col-md-12 col-xs-12">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Thông báo + Tin tức</h3>

                <div class="box-tools pull-right">
                    <div class="has-feedback">
                        <input type="text" class="form-control input-sm" placeholder="Search Mail">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                    </div>
                </div>
                <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

                <div class="table-responsive mailbox-messages">
                    <table class="table table-hover table-striped">
                        <tbody>
                            <?php foreach ($list_news as $v): ?>
                                <tr>
                                    <td><div class="icheckbox_flat-blue" aria-checked="false" aria-disabled="false" style="position: relative;"><input type="checkbox" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div></td>
                                    <td class="mailbox-star">
                                        <?php if ($v['is_hot']): ?>
                                            <i class="fa fa-star text-yellow"></i>
                                        <?php else: ?>
                                            <i class="fa fa-star-o text-yellow"></i>
                                        <?php endif; ?>

                                    </td>
                                    <td class="mailbox-name"><?php echo $v['author_username']; ?> </td>
                                    <td class="mailbox-subject"><a data-href="/<?php echo ADMIN_URL; ?>dashboard/news_detail/<?php echo $v['id']; ?>" title="Xem chi tiết" data-target="#" data-toggle="modal"><b><?php echo $v['title']; ?></a></b> </td>


                                    <td class="mailbox-attachment"><i class="fa fa-paperclip"></i></td>

                                    <td class="mailbox-date"><?php echo $v['date_create']; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    <!-- /.table -->
                </div>
                <!-- /.mail-box-messages -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer no-padding">
                <div class="mailbox-controls">
                    <!-- Check all button -->

                    <!-- /.btn-group -->
                    <button type="button" class="btn btn-default btn-sm"><i class="fa fa-refresh"></i></button>
                    <div class="pull-right">
                        1-50/200
                        <div class="btn-group">
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></button>
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <!-- /.btn-group -->
                    </div>
                    <!-- /.pull-right -->
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
// Support for AJAX loaded modal window.
// Focuses on first input textbox after it loads the window.
//        $('[data-toggle="modal"]').click(function (e) {
//            e.preventDefault();
//            var url = $(this).attr('href');
//            if (url.indexOf('#') == 0) {
//                $(url).modal('open');
//            } else {
//                $.get(url, function (data) {
//                    $('<div class="modal hide fade">' + data + '</div>').modal();
//                }).success(function () {
//                    $('input:text:visible:first').focus();
//                });
//            }
//        });

        $("[data-toggle=modal]").click(function (event) {
            event.preventDefault();
            var url = $(this).attr("data-href");
            if (url.indexOf("#") == 0) {
                $(url).modal('open');
            } else {
                $.get(url, function (data) {
		    console.log(data);
                    $(data).modal().on('hidden', function () {
                        $(this).remove();
                    });
                }).success(function () {
                    //$('input:text:visible:first').focus();
                });
            }
        });

    });



</script>