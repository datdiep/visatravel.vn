<div class="box box-footer">

    <fieldset>
        <legend><i class="fa fa-file-pdf-o"></i> Tải file đi kèm</legend>
        <input type="file" accept="image/*,application/pdf,application/zip,application/x-zip,application/x-zip-compressed,application/octet-stream,application/x-rar-compressed,compressed/rar,application/x-rar,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/vnd.openxmlformats-officedocument.wordprocessingml.template" id="images" name="images[]" multiple />
        <div id="image" class="image_product_details">

        </div>
        <div style="clear: both"></div>
        <hr/>
        <div class="image_product_details" id="lightgallery">
            <?php if (!empty($images)): ?>
                <?php foreach ($images as $k => $item): ?>                       
                    <a class="img_<?php echo $k; ?>" data-sub-html='<span class="del_img" type="it_img" name="<?php echo $item; ?>" rel="img_<?php echo $k; ?>" parent="lightgallery"><i class="fa fa-trash-0"></i> Del Image</span>' href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                        <img src="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>"/>                                            
                        <!--<span onclick="del('<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>', this)" >Del</span>-->
                    </a>                                                       
                <?php endforeach; ?>                                                                          
            <?php endif; ?>          
        </div>

    </fieldset>

    <?php if (!empty($word)): ?>
        <ul class="todo-list ui-sortable" style="margin-top:5px">
            <?php foreach ($word as $k => $item): ?>
                <li class="word_<?php echo $k; ?>">
                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                        <span class="handle ui-sortable-handle">
                            <i class="fa fa-file-word-o"></i>
                        </span>
                        <span class="text"><?php echo $item; ?></span>
                    </a>
                    <div class="tools">
                        <i type="it_word" name="<?php echo $item; ?>" rel="word_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                    </div>
                </li>
            <?php endforeach; ?>     
        </ul>
    <?php endif; ?>
    <?php if (!empty($excel)): ?>
        <ul class="todo-list ui-sortable" style="margin-top:5px">
            <?php foreach ($excel as $k => $item): ?>
                <li class="excel_<?php echo $k; ?>">
                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                        <span class="handle ui-sortable-handle">
                            <i class="fa fa-file-excel-o"></i>
                        </span>
                        <span class="text"><?php echo $item; ?></span>
                    </a>
                    <div class="tools">
                        <i type="it_excel" name="<?php echo $item; ?>" rel="excel_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                    </div>
                </li>
            <?php endforeach; ?>     
        </ul>
    <?php endif; ?>
    <?php if (!empty($pdf)): ?>
        <ul class="todo-list ui-sortable" style="margin-top:5px">
            <?php foreach ($pdf as $k => $item): ?>
                <li class="pdf_<?php echo $k; ?>">
                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                        <span class="handle ui-sortable-handle">
                            <i class="fa fa-file-pdf-o"></i>
                        </span>
                        <span class="text"><?php echo $item; ?></span>
                    </a>
                    <div class="tools">
                        <i type="it_pdf" name="<?php echo $item; ?>" rel="pdf_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                    </div>

                </li>
            <?php endforeach; ?>     
        </ul>
    <?php endif; ?>
    <?php if (!empty($zip)): ?>
        <ul class="todo-list ui-sortable" style="margin-top:5px">
            <?php foreach ($zip as $k => $item): ?>
                <li class="zip_<?php echo $k; ?>">
                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                        <span class="label-danger" style="padding: 1px 5px"><i class="fa fa-file-text-o"></i> Hồ sơ của khách</span>
                        <span class="text"><?php echo $item; ?></span>
                    </a>
                    <div class="tools">
                        <i type="it_zip" name="<?php echo $item; ?>" rel="zip_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                    </div>

                </li>
            <?php endforeach; ?>     
        </ul>
    <?php endif; ?>

</div>
<?php if (!empty($images_customer) && !empty($pdf_customer)): ?>
    <div class="box box-footer">
        <fieldset>
            <legend>Khách hàng upload</legend>
            <?php if (!empty($images_customer)): ?>
                <div class="image_product_details" id="lightgallery_customer">

                    <?php foreach ($images_customer as $k => $item): ?>                       
                        <a class="img_customer_<?php echo $k; ?>" data-sub-html='<span class="del_img" type="it_img" name="<?php echo 'customer/' . $item; ?>" rel="img_customer_<?php echo $k; ?>" parent="lightgallery_customer"><i class="fa fa-trash-0"></i> Del Image</span>' href="<?php echo "/assets/upload/data/{$folder}/customer/{$item}"; ?>">
                            <img src="<?php echo "/assets/upload/data/{$folder}/customer/{$item}"; ?>"/>                                            
                            <!--<span onclick="del('<?php echo "/assets/upload/data/{$folder}/customer/{$item}"; ?>', this)" >Del</span>-->
                        </a>                                                       
                    <?php endforeach; ?>                                                                          

                </div>
            <?php endif; ?>
        </fieldset>

        <?php if (!empty($pdf_customer)): ?>
            <ul class="todo-list ui-sortable" style="margin-top:5px">
                <?php foreach ($pdf_customer as $k => $item): ?>
                    <li class="pdf_customer_<?php echo $k; ?>">
                        <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/customer/{$item}"; ?>">
                            <span class="handle ui-sortable-handle">
                                <i class="fa fa-file-pdf-o"></i>
                            </span>
                            <span class="text"><?php echo $item; ?></span>
                        </a>
                        <div class="tools">
                            <i type="it_pdf" name="<?php echo 'customer/' . $item; ?>" rel="pdf_customer_<?php echo $k; ?>" class="del_img fa fa-trash-o"></i>
                        </div>

                    </li>
                <?php endforeach; ?>     
            </ul>
        <?php endif; ?>
    </div>
<?php endif; ?>
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.9/css/lightgallery.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
<script src="https://cdn.jsdelivr.net/g/lightgallery@1.3.5,lg-fullscreen@1.0.1,lg-zoom@1.0.3"></script>
<script>
    var order_id = <?php echo $block_file['id']; ?>;
    var model = "<?php echo $block_file['model']; ?>";
    var reader = new FileReader(),
            i = 0,
            numFiles = 0,
            imageFiles;
    function readFile() {
        reader.readAsDataURL(imageFiles[i])
    }
    reader.onloadend = function (e) {
        i++;
        var type = base64MimeType(e.target.result);
        var data = e.target.result;
        if (type == 'application/pdf') {
            data = '/assets/user/images/pdf-icon.png';
        }
        if (type == 'application/x-rar-compressed') {
            data = '/assets/user/images/rar-icon.png';
        }
        if (type == 'application/vnd.ms-excel' || type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
            data = '/assets/user/images/excel-icon.png';
        }
        if (type == 'application/msword' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' || type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.template') {
            data = '/assets/user/images/word-icon.png';
        }
        if (typeof type == 'undefined') {
            data = '/assets/user/images/zip-icon.png';
        }
        //var pdf = "<div class='row'><object id='pdf' data='" + e.target.result + "' type='application/pdf'></object></div>";
        var image = "<div class='row'><img src ='" + data + "'/></div>";
        $(image).appendTo('#image');
        if (i < numFiles) {
            readFile();
        } else {
            if(order_id > 0){
                var btn = '<button type="submit" value="-1" name="submit" class="btn btn-app btn-up-pic"><i class="fa fa-upload"></i> Upload</a>';
                $(btn).appendTo('#image');
            }
        }
    };

    $('#images').change(function () {
        imageFiles = document.getElementById('images').files
        $('#image').html('');

        i = 0;
        numFiles = imageFiles.length;
        readFile();
    });
    var gallery = $('#lightgallery').lightGallery({
        appendSubHtmlTo: '.lg-item'
    });

    var gallery_customer = $('#lightgallery_customer').lightGallery({appendSubHtmlTo: '.lg-item'});
    $("body").on('click', '.del_img', function () {
        var ele = $(this).attr('rel');
        var name = $(this).attr('name');
        var type = $(this).attr('type');

        if (type == 'it_img') {
            var parent = $(this).attr('parent');
            if (parent == 'lightgallery') {
                gallery.data('lightGallery').destroy(true);
                gallery = $('#' + parent).lightGallery({
                    appendSubHtmlTo: '.lg-item'
                });
            }
            if (parent == 'lightgallery_customer') {
                gallery_customer.data('lightGallery').destroy(true);
                gallery_customer = $('#' + parent).lightGallery({
                    appendSubHtmlTo: '.lg-item'
                });
            }

        }
        $("." + ele).remove();
        $.post('/<?php echo ADMIN_URL; ?>ajax/del_file', {name: name, id: order_id, model: model}, function (results) {
//            results = JSON.parse(results);
            console.log(results);
        });
    })
    function base64MimeType(encoded) {
        if (!encoded)
            return;
        var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

        if (mime && mime.length)
            return mime[1];
    }
</script>
<style>
    .lg-item span{
        position: absolute;
        background: red;
        padding: 5px 10px;
        bottom: 10px;
        cursor: pointer;
        color: #fff
    }
    #image{
        margin-top: 5px;
        margin-left: -5px;
    }
    .btn-up-pic{
        float: left;
        margin: 5px 5px 0px 5px;
        width: 72px;
        min-width: inherit;
        height: 72px;
    }
    .box-tools span{
        display: inline-block;
        padding: 2px 10px;
        font-size: 20px;
    }
    .box-tools span b{
        color: red
    }
    .image_product_details{
        margin: -10px 0px 0 -5px;
    }
    .image_product_details a {
        float: left;
        margin: 5px;
        padding: 5px;
        border: 1px solid #09f;
        position: relative;
        width: auto;
    }
    .image_product_details a span{
        position: absolute;
        bottom: 0px;
        right: 0px;
        background: #f4f4f4;
        padding: 1px 5px;
    }
</style>