<div class="panel box" id="note_0" rel_id="0">
    <div class="box-header with-border active">
        <h4 class="box-title">
            <i class="fa fa-chevron-circle-down"></i><i class="fa fa-chevron-circle-right"></i> 
            <input type="text" name="title" spellcheck="false" autocomplete="off" rel_id="0"/>
            <span class="pull-right" style="display: none"><i class="fa fa-edit"></i></span>
            <!--<span class="title" placeholder="Tiêu đề" contenteditable="true"></span>-->
        </h4>
    </div>
    <div id="collapse_0" class="panel-collapse">
        <div class="box-body">
            <ul class="todo-list ui-sortable">
                <li>
                    <input type="checkbox" value="">
                    <span class="text" placeholder="Nội dung..." contenteditable="true" spellcheck="false"></span>
                    <i class="fa fa-trash-o remove_checknote"></i>
                </li>
            </ul>
        </div>
        <div class="box-footer">
        
            <div class="pull-right">
                <button type=button" onclick="change_note(0, 'check')" class="change_check btn btn-default btn-xs margin-r-5"><i class="fa fa-check-square-o"></i> Hộp kiểm</button>
                <button type=button" onclick="change_note(0, 'text')" class="change_text btn btn-default btn-xs margin-r-5"><i class="fa fa-edit"></i> Bảng ghi</button>
                <button type=button" onclick="close_add()" class="close_note btn btn-default btn-xs margin-r-5"><i class="fa fa-close"></i> Đóng</button>
                <button type=button" onclick="del(0)" class="del_note btn btn-default btn-xs margin-r-5"><i class="fa fa-trash-o"></i> Xóa</button>
                <button type=button" onclick="save(0)" class="save_note btn btn-success btn-xs"><i class="fa fa-save"></i> Lưu</button>
            </div>
        </div>
    </div>
</div>