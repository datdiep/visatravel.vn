
<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT SERVICE</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>service_detail">List news</a></li>
            <li class="active">Edit news</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php endif; ?>
            <?php if ($show_button != -1) : ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">
                            <?php if ($show_button == 1) : ?>
                                <button type="submit" value="1" name="submit" class="btn btn-info"><?php echo $user['manager_editor'] ? "Xuất bản" : "Tái xuất bản và đợi Leader thẩm định" ?></button>
                            <?php else : ?>
                                <button type="submit" value="3" name="submit" class="btn btn-info">Lưu nháp</button>
                                <?php if ($user['publish'] || $user['manager_editor']) : ?>
                                    <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                                <?php else: ?>
                                    <button type="submit" value="2" name="submit" class="btn btn-success">Đợi duyệt</button>
                                <?php
                                endif;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Basic info</h3>
                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="<?php echo $detail['title']; ?>"  class="form-control"  name="title" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" value="<?php echo $detail['alias']; ?>" class="form-control"  name="alias" placeholder="Enter Alias">
                            </div>

                            <div class="form-group">
                                <label>Chọn Loaị Dịch Vụ</label>
                                <select name="service_id" class="form-control select2" style="width: 100%;">
                                    <?php foreach ($list_country as $value): ?>
                                        <option <?php if ($detail['service_id'] == $value['id']) echo 'selected=""' ?> value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">

                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title" value="<?php echo $detail['seo_title'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description" value="<?php echo $detail['meta_description'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword" value="<?php echo $detail['meta_keyword'] ?>">
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" value="<?php echo $detail['tags'] ?>">
                            </div>

                            <div class="form-group">
                                <label for="schedule">Lịch đăng bài</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="schedule" value="<?php if ($detail['schedule'] !== '0000-00-00') echo date('d/m/Y', strtotime($detail['schedule'])); ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>


                        </div>                            

                    </div>  
                </div>
                <div  class="col-md-12">   
                    <div class="box box-primary box-success">   
                        <div class="box-header">
                            <h3 class="box-title">Dịch vụ bao gồm các câu hỏi :</h3>
                        </div> 

                        <div class="slimScrollDiv" style="padding: 10px 0;position: relative; overflow: auto; width: auto; height: 200px;margin-left: 5px">
                            <?php
                            if (!empty($service_question)):
                                foreach ($service_question as $key => $item_all) {
                                    ?>
                                    <div class="item">
                                        <div style="width:100%;margin:5px 2% 0 0" class="input-group">
                                            <span class="input-group-addon">
                                                <input type ="checkbox" name ="checkbox[]" <?php echo isset($list_question[$item_all['id']]) ? "checked" : "" ?> value="<?php echo $item_all['id'] ?>">
                                            </span>
                                            <input type="text" class="form-control" value="<?php echo $item_all['title'] ?>" disabled>
                                        </div>
                                    </div>
                                    <?php
                                }
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="description_content" name="description"><?php echo $detail['description']; ?></textarea>


                        <div class="box-header">
                            <h3 class="box-title">Content</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="textarea_content" name="content"><?php echo $detail['content']; ?></textarea>
                        <?php if ($show_button != -1) : ?>
                            <div class="box-footer">
                                <?php if ($show_button == 1) : ?>
                                    <button type="submit" value="1" name="submit" class="btn btn-info"><?php echo $user['manager_editor'] ? "Xuất bản" : "Tái xuất bản và đợi Leader thẩm định" ?></button>
                                <?php else : ?>
                                    <button type="submit" value="3" name="submit" class="btn btn-info">Lưu nháp</button>
                                    <?php if ($user['publish'] || $user['manager_editor']) : ?>
                                        <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                                    <?php else: ?>
                                        <button type="submit" value="2" name="submit" class="btn btn-success">Đợi duyệt</button>
                                    <?php
                                    endif;
                                endif;
                                ?>
                            </div>
                        <?php endif; ?>
                    </div>

                </div>
            </div>

        </form>
    </section>

</div>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>
<script>
    crop('photo', 230, 150<?php echo $detail['image'] ? ',"/assets/upload/service_detail/' . $detail['image'] . '"' : ''; ?>);
    CKEDITOR.config.entities_latin = false;
    var editor1 = CKEDITOR.replace('textarea_content');
    var editor2 = CKEDITOR.replace('description_content');
    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
<script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })
</script>