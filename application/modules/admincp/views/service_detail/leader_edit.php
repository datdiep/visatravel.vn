
<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT SERVICE</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>service_detail">List news</a></li>
            <li class="active">Edit news</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php endif; ?>
            <?php if ($show_button == 1) { ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">
                            <button type="submit" value="-1" name="submit" class="btn btn-info">Tái xuất bản bài viết</button>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Basic info</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="<?php echo $tmp_detail['title']; ?>"  class="form-control" id="name"  onblur="taolink('name', 'alias')" onkeyup="taolink('name', 'alias')" name="title" placeholder="Enter title">
                                </br>                               
                                <?php if (strcmp($tmp_detail['title'], $detail['title']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['title'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" value="<?php echo $tmp_detail['alias']; ?>" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                                </br>                               
                                <?php if (strcmp($tmp_detail['alias'], $detail['alias']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['alias'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>

                            <div class="form-group">
                                <label>Chọn Loaị Dịch Vụ</label>
                                <select name="service_id" class="form-control select2" style="width: 100%;">
                                    <?php foreach ($list_country as $value): ?>
                                        <option <?php if ($tmp_detail['service_id'] == $value['id']) echo 'selected=""' ?> value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <label for="category_detail"> Danh mục cũ</label>
                                <?php
                                foreach ($list_country as $cate) :
                                    if ($detail['service_id'] == $cate['id']) {
                                        ?>
                                        <p> <?php echo $cate['title'] ?></p>
                                    <?php } endforeach; ?>
                            </div>
                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">
                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title" value="<?php echo $tmp_detail['seo_title'] ?>">
                                </br>                               
                                <?php if (strcmp($tmp_detail['seo_title'], $detail['seo_title']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['seo_title'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description" value="<?php echo $tmp_detail['meta_description'] ?>">
                                </br>
                                <?php if (strcmp($tmp_detail['meta_description'], $detail['meta_description']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['meta_description'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword" value="<?php echo $tmp_detail['meta_keyword'] ?>">
                                </br>
                                <?php if (strcmp($tmp_detail['meta_keyword'], $detail['meta_keyword']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['meta_keyword'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" value="<?php echo $tmp_detail['tags'] ?>">
                                </br>
                                <?php if (strcmp($tmp_detail['tags'], $detail['tags']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $detail['tags'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="schedule">Lịch đăng bài</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="schedule" value="<?php echo $tmp_detail['schedule'] != '' ? date('d/m/Y', strtotime($tmp_detail['schedule'])) : ""; ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>


                        </div>                            

                    </div>  
                </div>
                <div  class="col-md-12">   
                    <div class="box box-primary box-success">   
                        <div class="box-header">
                            <h3 class="box-title">Dịch vụ bao gồm các câu hỏi :</h3>
                        </div> 

                        <div class="slimScrollDiv" style="padding: 10px 0;position: relative; overflow: auto; width: auto; height: 200px;margin-left: 5px">
                            <?php
                            if (!empty($service_question)):
                                foreach ($service_question as $key => $item_all) {
                                    ?>
                                    <div class="item">
                                        <div style="width:100%;margin:5px 2% 0 0" class="input-group">
                                            <span class="input-group-addon">
                                                <input type ="checkbox" name ="checkbox[]" <?php echo isset($list_question[$item_all['id']]) ? "checked" : "" ?> value="<?php echo $item_all['id'] ?>">
                                            </span>
                                            <input type="text" class="form-control" value="<?php echo $item_all['title'] ?>" disabled>
                                        </div>
                                    </div>
                                    <?php
                                }
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="description_content" name="description"><?php echo $tmp_detail['description']; ?></textarea>
                        </br>
                        <?php if (strcmp($tmp_detail['description'], $detail['description']) != 0) : ?>
                            <div style="background-color:#fcfcfc" class="form-group" ><?php echo "OLD : " . $detail['description'] ?></div>
                        <?php endif; ?>

                        <div class="box-header">
                            <h3 class="box-title">Content</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="textarea_content" name="content"><?php echo $tmp_detail['content']; ?></textarea>
                        </br>
                        <?php if (strcmp($tmp_detail['content'], $detail['content']) != 0) : ?>
                            <div style="background-color:#fcfcfc" class="form-group"><?php echo "OLD : " . $detail['content'] ?></div>
                        <?php endif; ?>
                        <?php if ($show_button == 1) { ?>
                            <div class="box-footer">
                                <button type="submit" value="-1" name="submit" class="btn btn-info">Tái xuất bản bài viết</button>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>

        </form>
    </section>

</div>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>
<script>
    crop('photo', 230, 150<?php echo $tmp_detail['image'] ? ',"/assets/upload/service_detail/' . $tmp_detail['image'] . '"' : ''; ?>);
    CKEDITOR.config.entities_latin = false;
    var editor1 = CKEDITOR.replace('textarea_content');
    var editor2 = CKEDITOR.replace('description_content');
    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
<script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })
</script>