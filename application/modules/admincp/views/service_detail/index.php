
<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH SÁCH DỊCH VỤ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List service</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>service_detail/add"><button style="float:left;margin-right: 10px;width: 200px;" class="btn btn-primary">Thêm dịch vụ</button></a>
        <input id="keyword" class="form-control" placeholder="Nhập tên dịch vụ" value="" style="width: 200px;float: left;margin-right: 10px;"/>

        <select class="form-control" id="status" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Trạng thái -- </option>
            <option <?php echo $status == 2 ? 'selected' : ''; ?> value="3">Bản nháp</option>
            <option <?php echo $status == 1 ? 'selected' : ''; ?> value="1">Hoàn chỉnh</option>
            <option <?php echo $status == 3 ? 'selected' : ''; ?> value="2">Chờ duyệt</option>
            <option <?php echo $status == -1 ? 'selected' : ''; ?> value="-1">Tái xuất bản</option>
        </select>

        <select class="form-control" id="service_id" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Danh mục Dịch Vụ -- </option>
            <?php foreach ($list_country as $child) : ?>
                <option <?php echo $cate_id == $child['id'] ? 'selected' : ''; ?> value="<?php echo $child['id'] ?>"><?php echo $child['title']; ?></option>
            <?php endforeach; ?>
        </select>           

    </section>
    <br/><br/>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var service_id = $('#service_id').val();
        var keyword = $('#keyword').val();
        var order = $('#order').val();
        var status = $('#status').val();
        $.post('/<?php echo ADMIN_URL; ?>service_detail/page', {pos: pos, status: status, order: order, service_id: service_id, keyword: keyword}, function (results) {
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    function del(id) {
        show_dialog('Bạn có muốn xóa tin tức id = ' + id + " không", function () {
            $.post('/<?php echo ADMIN_URL; ?>service_detail/del', {id: id}, function (results) {
                if (results != "")
                    alert(results);
                loadlist(current_pos);
            });
        });
    }
    $('#service_id').change(function () {
        loadlist(0);
    });
    $('#keyword').keypress(function (e) {
        loadlist(0);
    });

    $('#order').change(function () {
        loadlist(0);
    });
    $('#status').change(function () {
        loadlist(0);
    });
    function approved(key) {
        $("#chage_icon_" + key).removeClass("fa-hourglass-2");
        $("#chage_icon_" + key).addClass("fa-refresh");
        $.post('/<?php echo ADMIN_URL; ?>service_detail/approved', {id: key, status: "approved"}, function (results) {
            if (results != "") {
                $("#chage_icon_" + key).removeClass("fa-refresh");
                $("#chage_icon_" + key).addClass("fa-lock");
                loadlist(current_pos);
            } else {
                alert("ERROR");
            }
        });

    }

</script>

