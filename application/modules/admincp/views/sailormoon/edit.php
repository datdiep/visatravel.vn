<div class="content-wrapper">    
    <section class="content-header">
        <h1>SỮA TRƯỜNG HỢP</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>service_fees">Giá và các trường hợp</a></li>
            <li class="active">Sữa trường hợp</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $msg; ?>                          
                </div>
            <?php endif; ?>
            <div class="row">            
                <div class="col-md-12">   
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin </h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-6 nopadding"> 
                                <div class="form-group col-md-12">
                                    <label for="services">Loại dịch vụ</label>
                                    <select name="services" class="form-control valid"  id="service">
                                        <?php foreach ($services as $k => $value): ?>
                                            <option <?php echo $k == $service['services'] ? 'selected' : ''; ?> value="<?php echo $k; ?>"><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exp_service">Thời hạn dịch vụ</label>
                                    <select name="exp_service" class="form-control valid">
                                        <?php foreach ($exp_date as $k => $value): ?>
                                            <option <?php echo $k == $service['exp_service'] ? 'selected' : ''; ?> value="<?php echo $k; ?>"><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="type_visa">Loại Visa của khách</label>
                                    <select name="type_visa" class="form-control valid">
                                        <?php foreach ($purpose_of_visit as $k => $value): ?>
                                            <option <?php echo $k == $service['type_visa'] ? 'selected' : ''; ?> value="<?php echo $k; ?>"><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="fee">Giá</label>
                                    <input type="text" class="form-control" name="fee" value="<?php echo $service['fee']; ?>">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="fee_net">Giá net</label>
                                    <input type="text" class="form-control" name="fee_net" value="<?php echo $service['fee_net']; ?>">
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="add_service">Dịch vụ thêm</label>
                                     <div class="row">
                                        <?php foreach ($services as $k => $value): ?>
                                        <div class="col-md-4 group_service" rel="<?php echo $k; ?>">
                                            <div class="checkbox" style="margin:0px;">
                                                <label><input <?php echo strpos($service['add_service'], $k) ? 'checked':''; ?> name="add_service[]" type="checkbox" value="<?php echo $k; ?>"><?php echo $value; ?></label>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="checklist_id">Danh sách hồ sơ</label>
                                    <select multiple="" name="checklist_id[]" class="form-control valid" id="checklist_id" style="height: 150px">
                                        
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div> 
                </div>

                <div  class="col-md-12">   
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Ghi chú cho trường hợp này (Nếu có)</h3>
                        </div>
                        <div class="box-body">
                            <textarea id="textarea_content" name="note"><?php echo $service['note']; ?></textarea>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    </div>

                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    var checklist_ids = "<?php echo $service['checklist_id']; ?>";
    function load_checklist() {
        var service = $('#service').val();
        var _this = $('div[rel="'+service+'"]');
        $('.group_service').css('display','block');
         $(".group_service input").prop('disabled', false);
        _this.css('display','none');
        $("input", _this).prop('disabled', true);
        $.post('/<?php echo ADMIN_URL; ?>service_fees/load_checklist', {service: service}, function (results){
            $('#checklist_id').html(results);
            var checklist_ids_temp = checklist_ids;
            $('#checklist_id option').prop('selected', false);
            $('#checklist_id option').each(function () {
                var text = ',' + $(this).val() + ',';
                if (checklist_ids_temp.indexOf(text) != -1) {
                    $(this).prop('selected', true);
                }
            })
        });
    }
    load_checklist();
    $('#service').change(function () {
        load_checklist();
    });
</script>