<?php foreach ($results as $v): ?>
<li class="markerDiv">
    <span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span>
    <input type="checkbox" data_title="<?php echo $v['title'];?>" data_id="<?php echo $v['id'];?>">
    <span class="text"><?php echo $v['title'];?></span>
    <div class="tools">
        <i class="fa fa-edit"></i>
    </div>
</li>
<?php endforeach; ?>
<div class="col-md-12 nopadding">
    <ul class="pagination">
        <?php echo $links; ?>
    </ul
</div>