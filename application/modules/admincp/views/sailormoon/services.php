<div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">XÂY DỰNG DỊCH VỤ
        <div class="box-tools pull-right">
        <p class="btn btn-default" data-dismiss="modal">Bỏ qua</p>
        <p class="btn btn-primary save_services" rel_id="<?php echo !empty($service) ? $service['id']:0;?>"><i class="fa fa-save"></i> Lưu lại</p>
        </div>
    </h4>
</div>
<div class="modal-body">
    <div class="box-body">
        <?php if (!empty($msg)): ?>
            <div class="alert alert-warning alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <?php echo $msg; ?>                          
            </div>
        <?php endif; ?>
        <div class="form-group col-md-4">
            <label for="name">Tên dịch vụ</label>
            <input type="text" name="name" class="form-control"  value="<?php echo @$service['name'];?>" id="title" <?php echo !empty($service) ? '':'onblur="taolink(\'title\', \'alias\')" onkeyup="taolink(\'title\', \'alias\')"';?>>
        </div>
        <div class="form-group col-md-4">
            <label for="alias">Alias</label>
            <input type="text" class="form-control"  value="<?php echo @$service['alias'];?>" id="alias" name="alias" placeholder="Enter Alias">
        </div>
        <div class="form-group col-md-4">
            <label for="group_service">Thuộc nhóm dịch vụ</label>
            <select class="form-control" name="group">
                <?php foreach ($group_services as $value): ?>
                    <option <?php echo @$service['group'] == $value ? 'selected':'';?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group col-md-12">
            <label for="note">Ghi chú (Nếu có)</label>
            <textarea name="note" class="form-control"><?php echo @$service['note'];?></textarea>
        </div>

        <div class="form-group col-md-12">
            <label>Thiết lập trường tùy chọn &nbsp;&nbsp;<button type="button" class="btn btn-info btn-xs pull-right add_select"><i class="fa fa-plus"></i> Thêm</button></label>
            <?php if(!empty($service)):?>
            <p style="color:#ccc">Cẩn thật khi sữa <b><u>Name key select</u></b> và  <b><u>Value option</u></b>. Nếu bạn muốn sữa thì bạn cần phải 
                cập nhật lại chi tiết cho mỗi trường hợp.</p>
            <?php endif;?>
            <div class="panel-group">
                <div class="panel panel-default">
                    
                    <?php if(!empty($service['select'])):?>
                        <?php foreach ($service['select'] as $key => $value): ?>
                        <div class="panel-body">
                            <hr>
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-default btn-xs" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Namekeyselect">Name key select</label>
                                <input type="text" class="inputTextBox form-control Namekeyselect" placeholder="Viết liền không dấu" value="<?php echo $key;?>">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="alias">Text lable select</label>
                                <input type="text" class="form-control Textlableselect" placeholder="Càng xác nghĩ càng tốt" value="<?php echo $value['lable'];?>">
                            </div>
                            <?php if(!empty($value['option'])):?>
                                <?php 
                                    $valueoption = ""; $titleoption = "";
                                    foreach ($value['option'] as $k => $v){
                                        $valueoption .= $k.PHP_EOL ;
                                        $titleoption .= $v.PHP_EOL ;
                                    }
                                ?>
                            <?php endif;?>
                            <div class="form-group col-md-4">
                                <label for="alias">Value option</label>
                                <textarea class="inputTextBox form-control Valueoption" rows="6" placeholder="Mỗi giá trị một dòng"><?php echo @$valueoption;?></textarea>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="alias">Title option</label>
                                <textarea class="form-control Titleoption" rows="6" placeholder="Mỗi tên một dòng"><?php echo @$titleoption;?></textarea>
                            </div>
                            
                        </div>
                        <?php endforeach; ?>
                    <?php else:?>
                        <div class="panel-body">
                            <hr>
                            <div class="pull-right box-tools">
                                <button type="button" class="btn btn-default btn-xs" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                                    <i class="fa fa-times"></i></button>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Namekeyselect">Name key select</label>
                                <input type="text" class="inputTextBox form-control Namekeyselect" placeholder="Viết liền không dấu">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="alias">Text lable select</label>
                                <input type="text" class="form-control Textlableselect" placeholder="Càng xác nghĩ càng tốt">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="alias">Value option</label>
                                <textarea class="inputTextBox form-control Valueoption" rows="6" placeholder="Mỗi giá trị một dòng"></textarea>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="alias">Title option</label>
                                <textarea class="form-control Titleoption" rows="6" placeholder="Mỗi tên một dòng"></textarea>
                            </div>

                        </div>
                    <?php endif;?>
                </div>

            </div>

        </div>

    </div>
</div>
<div class="modal-footer">
    <p class="btn btn-default" data-dismiss="modal">Bỏ qua</p>
    <?php if(!empty($service)):?>
        <p class="btn btn-default clone">Sao chép</p>
    <?php endif;?>
    <p class="btn btn-primary save_services" rel_id="<?php echo !empty($service) ? $service['id']:0;?>"><i class="fa fa-save"></i> Lưu lại</p>
</div>
<div id="temp_select" style="display: none">
    <div class="panel-body">
        <hr>
        <div class="pull-right box-tools">
            <button type="button" class="btn btn-default btn-xs" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                <i class="fa fa-times"></i></button>
        </div>
        <div class="form-group col-md-4">
            <label for="Namekeyselect">Name key select</label>
            <input type="text" class="form-control inputTextBox Namekeyselect" placeholder="Viết liền không dấu">
        </div>
        <div class="form-group col-md-8">
            <label for="alias">Text lable select</label>
            <input type="text" class="form-control Textlableselect" placeholder="Càng xác nghĩ càng tốt">
        </div>
        <div class="form-group col-md-4">
            <label for="alias">Value option</label>
            <textarea class="form-control inputTextBox Valueoption" rows="6" placeholder="Mỗi giá trị một dòng"></textarea>
        </div>
        <div class="form-group col-md-8">
            <label for="alias">Title option</label>
            <textarea class="form-control Titleoption" rows="6" placeholder="Mỗi tên một dòng"></textarea>
        </div>

    </div>
</div>
<script>
    $('.clone').click(function(){
        $('.save_services').attr('rel_id',0);
        $('#build_services input[name="name"]').val("");
        $('#build_services input[name="name"]').focus();
        $('#build_services input[name="name"]').attr('onblur',"taolink('title', 'alias')");
        $('#build_services input[name="name"]').attr('onkeyup',"taolink('title', 'alias')");
        $(this).remove();
    })
    $('.save_services').click(function(){
        var id = $(this).attr('rel_id');
        var data = {
            name : $('#build_services input[name="name"]').val(),
            alias : $('#build_services input[name="alias"]').val(),
            group : $('#build_services select[name="group"]').val(),
            note : $('#build_services textarea[name="note"]').val(),
            select : {}
        }
        if(data["name"] == ''){
            alert('Tên dịch vụ không được bỏ trống');
            $('#build_services input[name="name"]').focus();
            return false;
        }
        $('#build_services .panel-body').each(function () {
            var Namekeyselect = $('.Namekeyselect', this).val();
            var Textlableselect = $('.Textlableselect', this).val();     
            if (Namekeyselect != "" && Textlableselect!="") {
                var Valueoption = $('.Valueoption', this).val();
                var Titleoption = $('.Titleoption', this).val();
                Valueoption = Valueoption.split('\n');
                Titleoption = Titleoption.split('\n');
                var option = {};
                $.each(Valueoption, function( key, value ) {
                    if(value != "" && typeof Titleoption[key] != 'undefined'){
                       option[value] = Titleoption[key];
                    }   
                });
                data['select'][Namekeyselect] = {
                    lable: Textlableselect,
                    option: option
                }
                
            }
        })
        $.post('/<?php echo ADMIN_URL; ?>sailormoon/build_services', {id: id,data: data}, function (results) {
            $('#build_services').html(results);
        });
    })
    setTimeout(function(){
         $('#build_services .panel-default textarea').each(function () {
         if($(this).val() != ""){
            autosize(this); 
         }
     })
    },100)
    <?php if(!empty($add_service)):?>
        var html = '<li><span class="handle ui-sortable-handle"><i class="fa fa-ellipsis-v"></i><i class="fa fa-ellipsis-v"></i></span>'+
                   '<span class="text"><?php echo $service['name']; ?></span>'+               
                   '<div class="tools">'+
                   '<i class="fa fa-trash-o del_services" rel_id="<?php echo $service['id'];?>"></i><i class="fa fa-edit build_services" rel_id="<?php echo $service['id'];?>"></i>'+
                   '<a href="/<?php echo ADMIN_URL; ?>sailormoon/training?service=<?php echo $service['alias']; ?>"><i class="fa fa-mortar-board"></i></a>'+
                   '</div></li>';
       $('#list_services').prepend(html);
    <?php endif;?>
    <?php if(!empty($edit_service)):?>
        var li_service = $('.build_services[rel_id="<?php echo $service['id'];?>"]').parent().parent();
        $('.text', li_service).html("<?php echo $service['name']; ?>");
    <?php endif;?>
</script>   