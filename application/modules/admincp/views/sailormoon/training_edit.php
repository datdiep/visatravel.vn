<div class="content-wrapper">    
    <section class="content-header">
        <h1>Cập nhật dịch vụ: <b><?php echo $service['name']; ?></b></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>service_fees">Giá và các trường hợp</a></li>
            <li class="active">Thêm trường hợp</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo $msg; ?>                          
                </div>
            <?php endif; ?>
            <div class="row">            
                <div class="col-md-6">   
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Trường hợp xủ lý cụ thể</h3>
                        </div>
                        <?php 
                            pre($services,false);
                            pre($service);
                            $select = unserialize($services[$service['service']]['select']);
                            $mind_case = unserialize($service['mind_case']);//pre($select);?>
                        <div class="box-body">
                            <?php foreach ($mind_case as $k => $v){
                                echo '<b>'.$select[$k]['lable'].':</b> '.$select[$k]['option'][$v].'<br>';
                            };?>
                            <hr class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="fee">Giá đề xuất</label>
                                <input type="text" class="form-control" name="fee" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="fee_consul">Phí lãnh sự</label>
                                <input type="text" class="form-control" name="fee_consul" placeholder="">
                            </div>
                             <div class="form-group col-md-12">
                                <label for="note">Ghi chú trường hợp</label>
                                <textarea class="form-control" name="note" placeholder=""></textarea>
                            </div>
                        </div>

                    </div>
                    <div class="box box-primary">
                        <div class="box-header">
                            <i class="ion ion-clipboard"></i>

                            <h3 class="box-title">Moon sẽ hỏi khách điều gì</h3>

                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <input type="hidden" name="mind_question" value=""/>
                            <ul class="todo-list ui-sortable" id="mind_question">
                                <li>
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <span class="text text-danger">Chưa có câu hỏi nào cho trường hợp này</span>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix no-border">
                            <p class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> Thêm & sữa các câu hỏi</p>
                            
                        </div>
                    </div>
                    <div class="box box-danger">
                      <div class="box-header with-border">
                          <i class="ion ion-clipboard"></i>

                          <h3 class="box-title">Nhóm câu hỏi -> Hướng xử lý</h3>

                          <p class="btn btn-warning pull-right" data-toggle="modal" data-target="#myModal1"><i class="fa fa-plus"></i> Thêm hướng xủ lý</p>

                          <div style="color:#777">
                              Trong trường hợp không có hướng xử lý đặc biệt Moon sẽ tự xử lý theo hướng mặc định. Có nghĩ là thiếu cái gì thì làm cái đó
                          </div>

                      </div>
                      <!-- /.box-header -->
                      <div class="box-body ">
                          <div class="row">
                              <div class="col-md-12" id="mind_handled">

                              </div>
                          </div>

                      </div>

                  </div>
                </div>
                <div class="col-md-6">   
                    <div class="box box-default box-success">
   
                        <div class="box-body">
                            <div class="form-group">
                                <label for="checklist">Danh sách hồ sơ (Checklist)</label>
                                <textarea name="checklist" id="checklist"></textarea>
                            </div>
                        </div>

                    </div>
                    <?php echo $load_file; ?>
                </div>

                

</div>
<div class="box-footer">
    <button type="submit" name="submit" value="-1" class="btn btn-success">Dậy Moon học <?php echo $service['name'];?></button>
</div>
</form>
</section>

</div>
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content"  id="handled">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Thiết lập hướng xử lý</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label>Chọn câu hỏi trong(trường hợp khách hàng trả lời không có)</label>
                        <ul class="todo-list ui-sortable question_ajax">

                        </ul>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Chọn cách xử lý</label>
                        <ul class="todo-list ui-sortable margin-bottom handle_ajax">

                        </ul>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default" data-dismiss="modal">Đóng</p>
                <p class="btn btn-default" id="ok_handled"><i class="fa fa-arrow-down"></i> Thiết lập</p>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content display-right"  id="list_question">
            <div class="modal-header">
                <div class="box-tools pull-right">
                    <select class="form-control">
                        <option value="">-- Lọc theo dịch vụ --</option>
                        <?php foreach ($group_services as $value): ?>
                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <h4 class="modal-title" id="myModalLabel">Các câu hỏi Moon đã học được</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label>Chọn câu hỏi cho Moon trong trường hợp này</label>
                        <ul class="todo-list ui-sortable margin-bottom" id="question_ajax">
                            <li class="markerDiv">
                                <input type="checkbox" value="">
                                <span class="text">Xử lý thiếu công ty bảo lãnh</span>
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default" data-dismiss="modal">Đóng</p>
                <p class="btn btn-default" id="ok_chose"><i class="fa fa-arrow-down"></i> Thêm câu hỏi</p>
                <p class="btn btn-primary change_box" show="add_question" hide="list_question"><i class="fa fa-plus"></i> Tạo câu hỏi mới</p>
            </div>
        </div>
        <div class="modal-content display-right" id="add_question">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Dạy Moon hỏi khách</h4>
                <span style="color:#777">Moon còn ngu lắm nên chỉ có thể hiểu được các câu hỏi dạng <u>có</u> hoặc <u>không</u> thôi à. Anh mà dạy phức tạp quá em éo hiểu đâu đó</span>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label for="title">Đặt câu hỏi với khách</label>
                        <input type="text" name="title" class="form-control" id="title" placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="group_service">Thuộc nhóm dịch vụ</label>
                        <div class="row">
                            <?php foreach ($group_services as $value): ?>
                                <div class="col-md-4">
                                    <div class="checkbox" style="margin:0px;">
                                        <label><input name="group_service" <?php echo $value == $service['group'] ? 'checked disabled' : ''; ?> type="checkbox" value="<?php echo $value; ?>"><?php echo $value; ?></label>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="note">Ghi chú (Nếu có)</label>
                        <textarea name="note" class="form-control"></textarea>
                    </div>

                    <div class="form-group col-md-12">
                        <label for="address_vn">Hướng xử lý</label>
                        <ul class="todo-list ui-sortable margin-bottom">
                            <li>
                                <span class="handle ui-sortable-handle">
                                    <i class="fa fa-ellipsis-v"></i>
                                    <i class="fa fa-ellipsis-v"></i>
                                </span>
                                <span class="text text-danger">Chưa có hướng xử lý nào cho trường hợp này</span>
                            </li>
                        </ul>

                    </div>

                </div>
            </div>


            <div class="modal-footer">
                <p class="btn btn-default change_box" show="list_question" hide="add_question">Bỏ qua</p>
                <p class="btn btn-primary change_box" show="list_question" hide="add_question" rel="add_question"><i class="fa fa-save"></i> Lưu lại</p>
                <p class="btn btn-default change_box" show="list_handle" hide="add_question" rel="list_handle"><i class="fa fa-plus"></i> Thêm xử lý</p>
            </div>
        </div>
        <div class="modal-content display-right" id="list_handle">
            <div class="modal-header">
                <div class="box-tools pull-right">
                    <select class="form-control">
                        <option value="">-- Lọc theo dịch vụ --</option>
                        <?php foreach ($group_services as $value): ?>
                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <h4 class="modal-title" id="myModalLabel">Các xử lý Moon đã học được</h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label for="address_vn">Chọn hướng xử lý phù hợp với câu hỏi</label>
                        <ul class="todo-list ui-sortable margin-bottom" id="handle_ajax">
                            <li for="address_vn">
                                <!-- checkbox -->
                                <input type="checkbox" value="" id="address_vn">
                                <!-- todo text -->
                                <span class="text">Xử lý thiếu công ty bảo lãnh</span>
                                <!-- General tools such as edit or delete-->
                                <div class="tools">
                                    <i class="fa fa-edit"></i>
                                </div>
                            </li>

                        </ul>
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default change_box" show="add_question" hide="list_handle" rel="push_handle"><i class="fa fa-arrow-left"></i> Chọn</p>
                <p class="btn btn-primary change_box" show="add_handle" hide="list_handle"><i class="fa fa-plus"></i> Tạo xử lý mới</p>
            </div>
        </div>
        <div class="modal-content display-right" id="add_handle">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Dạy Moon xử lý</h4>
                <span style="color:#777">Nếu khách hàng trả lời cho câu hỏi là <u>không</u> thì em sẽ chủ động giới thiệu <u>dịch vụ thêm của chúng ta</u> để kiếm thêm chúng đỉnh nhé <i class="fa fa-smile-o"></i>.</span>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="form-group col-md-12">
                        <label for="title">Tên dịch vụ thêm</label>
                        <input type="hidden" class="form-control" name="id" value="">
                        <input type="text" class="form-control" name="title" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fee">Giá</label>
                        <input type="text" class="form-control format_number" name="fee" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="fee_net">Giá net</label>
                        <input type="text" class="form-control format_number" name="fee_net" placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="note">Ghi chú (Nếu có)</label>
                        <textarea name="note" class="form-control"></textarea>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default change_box" show="list_handle" hide="add_handle">Bỏ qua</p>
                <p class="btn btn-primary change_box" show="list_handle" hide="add_handle" rel="add_handle"><i class="fa fa-save"></i> Lưu lại</p>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="/assets/user/css/animate.min.css">
<script>
    CKEDITOR.config.entities_latin = false;
    CKEDITOR.config.toolbarGroups = [
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'colors'},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
        '/',
        {name: 'styles'},
        {name: 'tools'},
        {name: 'mode'},
    ];
    CKEDITOR.config.height = '350px'; 
    var editor1 = CKEDITOR.replace('checklist');
    $('#myModal').on('shown.bs.modal', function (e) {
        $('#list_question').addClass('animated fadeInRightBig');
        $('#list_question').removeClass('display-right');
        load_question(0);
        var _this = $('#handle_ajax');
        load_handle(0,_this);
    })
    $('#myModal').on('hide.bs.modal', function (e) {
        $('#myModal .modal-content').removeClass('animated fadeOutRightBig fadeOutLeftBig fadeInRightBig fadeInLeftBig');
        $('#myModal .modal-content').addClass('display-right');
    })
    $('#myModal1').on('shown.bs.modal', function (e) {
        var html = $('#mind_question').html();
        $('#handled .question_ajax').html(html);
        var _this = $('#handled .handle_ajax');
        load_handle(0,_this);
    })
    $('.change_box').click(function () {
        var show = $(this).attr('show');
        var hide = $(this).attr('hide');
        if ($(this).attr('rel') == "add_question") {
            add_question();
        }
        if ($(this).attr('rel') == "add_handle") {
            if ($('#add_handle input[name="title"]').val() == '') {
                alert('Tên dịch vụ thêm không được bỏ trống');
                $(this).focus();
                return false;
            }
            add_handle();
        }
        if ($(this).attr('rel') == "push_handle") {
            var html = "";
            $('#handle_ajax input[type="checkbox"]:checked').each(function (index) {
                var id = $(this).attr('data_id');
                var title = $(this).attr('data_title');
                html += '<li><input class="display-none" name="handle_id" checked type="checkbox" value="' + id + '"><span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span> <span class="text">' + title + '</span> <div class="tools"> <i class="fa fa-trash-o del_this"></i> </div> </li>';
            });
            $('#add_question .todo-list').html(html);
        }
        if ($('#' + show).hasClass('fadeOutLeftBig')) {
            $('#' + hide).addClass('animated fadeOutRightBig');
            $('#' + show).addClass('animated fadeInLeftBig');
            $('#' + show).removeClass('fadeInRightBig fadeOutLeftBig');
        } else {
            $('#' + hide).addClass('animated fadeOutLeftBig');
            $('#' + show).addClass('animated fadeInRightBig');
            $('#' + show).removeClass('fadeOutRightBig');
        }
        $('#' + show).removeClass('display-right');
    });

    $("#ok_chose").click(function () {
        var html = ""; var value = ',';
        $('#question_ajax input[type="checkbox"]:checked').each(function (index) {
            var id = $(this).attr('data_id');
            var title = $(this).attr('data_title');
            value += id+',';
            html += '<li class="markerDiv"><span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span><input type="checkbox" data_id="' + id + '" data_title="' + title + '"> <span class="text">' + title + '</span> <div class="tools"> <i class="fa fa-trash-o del_this" name="mind_question" _id=",'+id+',"></i> </div> </li>';
        });
        if(value == ','){
            alert('Chưa chọn câu hỏi sau mà thêm, troll Moon à.');
        }else{
            $('input[name="mind_question"]').val(value);
            $('#mind_question').html(html);
            $("#myModal").modal("hide");
        }
        
    });
    $("#ok_handled").click(function () {
        //var html = '<div class="nav-tabs-custom"> <ul class="nav nav-tabs"> <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Câu hỏi thiếu</a></li> <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Hướng giải quyết</a></li> <p class="btn btn-default pull-right margin-r-5"><i class="fa fa-edit"></i> Sữa</p></li> </ul> <div class="tab-content"> <div class="tab-pane active" id="tab_1"> <ul class="todo-list ui-sortable"> </ul> </div> <div class="tab-pane" id="tab_2"> <ul class="todo-list ui-sortable"> </ul> </div> </div> </div>';
        var question = ''; var handle = ''; var key = value = ',';
        var count = $('#mind_handled .nav-tabs-custom').length;
        $('#handled .question_ajax input[type="checkbox"]:checked').each(function (index) {
            var id = $(this).attr('data_id');
            var title = $(this).attr('data_title');
            key += id+',';
            question += '<li><span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span><span class="text">' + title + '</span> <div class="tools"></div> </li>';
        });
        $('#handled .handle_ajax input[type="checkbox"]:checked').each(function (index) {
            var id = $(this).attr('data_id');
            var title = $(this).attr('data_title');
            value += id+',';
            handle += '<li><span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span><span class="text">' + title + '</span> <div class="tools"></div> </li>';
        });
        var html = '<div class="nav-tabs-custom"> <ul class="nav nav-tabs"> <li class="active"><a href="#tab_'+count+'_1" data-toggle="tab" aria-expanded="true">Câu hỏi thiếu</a></li> <li class=""><a href="#tab_'+count+'_2" data-toggle="tab" aria-expanded="false">Hướng giải quyết</a></li> <p class="btn btn-default pull-right margin-r-5 del_this"><i class="fa fa-trash"></i> Xóa</p></li> </ul> <div class="tab-content"> <div class="tab-pane active" id="tab_'+count+'_1"> <ul class="todo-list ui-sortable">'+question+'</ul> </div> <div class="tab-pane" id="tab_'+count+'_2"> <ul class="todo-list ui-sortable">'+handle+'</ul> </div> </div> </div>'; 
        html += '<input type="hidden" name="mind_handled_key[]" value="'+key+'"><input type="hidden" name="mind_handled_value[]" value="'+value+'">';
        $('#mind_handled').append(html);
        $("#myModal1").modal("hide");
    });
    var service = "<?php echo $service['alias']; ?>";
    var data = {};
    function add_question() {
        ele = $('#add_question');
        $.when(get_data(ele)).done(function () {
            console.log(data);
            $.post('/<?php echo ADMIN_URL; ?>sailormoon/add_edit_question', {data: data}, function (results) {
                results = $.parseJSON(results);
                var html = '<li><input checked type="checkbox" data_id="' + results.id + '" data_title="' + results.title + '"><span class="text">' + results.title + '</span> <div class="tools"> <i class="fa fa-edit"></i> </div> </li>';
                $('#question_ajax').prepend(html);
            });
        });

    }
    function add_handle() {
        ele = $('#add_handle');
        $.when(get_data(ele)).done(function () {
            // console.log(data);
            $.post('/<?php echo ADMIN_URL; ?>sailormoon/add_edit_handle', {data: data}, function (results) {
                results = $.parseJSON(results);
                var html = '<li><input checked type="checkbox" data_id="' + results.id + '" data_title="' + results.title + '"><span class="text">' + results.title + '</span> <div class="tools"> <i class="fa fa-edit"></i> </div> </li>';
                $('#handle_ajax').prepend(html);
            });
        });

    }
    function get_data(ele) {
        data = {};
        $('.form-control', ele).each(function () {
            var key = $(this).attr('name');
            if ($(this).hasClass('format_number')) {
                $(this).val($(this).autoNumeric('get'));
            }
            data[key] = $(this).val();
        })
        var key_temp = "";
        var total = 0;
        $('input[type="checkbox"]:checked', ele).each(function (index) {
            var key = $(this).attr('name');
            if (key_temp != key) {
                key_temp = key;
                total += $('input[name="' + key + '"]:checked').length;
            }
            if (typeof data[key] == 'undefined')
                data[key] = "";
            data[key] = data[key] + ',' + this.value;
            if (index === total - 1) {
                data[key] = data[key] + ',';
            }
        });
    }
    function load_question(pos) {
        $.post('/<?php echo ADMIN_URL; ?>sailormoon/load_question', {pos: pos, service: service}, function (results) {
            $('#question_ajax').html(results);
        });
    }
    function load_handle(pos,_this) {
        $.post('/<?php echo ADMIN_URL; ?>sailormoon/load_handle', {pos: pos, service: ''}, function (results) {
           // console.log(results);
            _this.html(results);
        });
    }
    $('#is_special').change(function () {
        var v = $(this).val();
        if (v == 'VOA') {
            $('.VOA').fadeIn();
            $('.other_service').fadeOut();
            $('.Visa_extension').fadeOut();
        } else if (v == "") {
            $('.VOA').fadeOut();
            $('.other_service').fadeOut();
            $('.Visa_extension').fadeOut();
        } else if (v == "Visa extension") {
            $('.VOA').fadeOut();
            $('.other_service').fadeOut();
            $('.Visa_extension').fadeIn();
        } else {
            $('.VOA').fadeOut();
            $('.Visa_extension').fadeOut();
            $('.other_service').fadeIn();
        }

    })

    $('#question_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        load_question(pos);
        return false;
    });
    $('body').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        var _this = $(this).parent().parent().parent().parent();
        console.log(_this);
        load_handle(pos,_this);
        return false;
    });
    $('body').on('click', '.del_this', function () {
        var name = $(this).attr('name');
        var id = $(this).attr('_id');
        var value = $('input[name="'+name+'"]').val().replace(id, ",");
        var html = '<li> <span class="handle ui-sortable-handle"> <i class="fa fa-ellipsis-v"></i> <i class="fa fa-ellipsis-v"></i> </span> <span class="text text-danger">Chưa có câu hỏi nào cho trường hợp này</span> </li>';
        if(value == ','){
            console.log(value);
            $(this).parent().parent().parent().html(html);
        }
        $('input[name="'+name+'"]').val(value);
        $(this).parent().parent().remove();
    });
    $('body').on('click', '.markerDiv', function () {
        if ($(this).find('input').is(":checked")) {
            $(this).find('input').attr("checked", false);
        } else {
            $(this).find('input').prop("checked", true);
        }
    });
    $('input[type=checkbox]').click(function (e) {
        e.stopPropagation();
    });
</script>
<style>
    .modal-content{
        position: absolute;
        top: 0px;
        width: 100%;
    }
    .todo-list .handle {
        float: left 
    }
    .todo-list>li>input[type='checkbox'] {
        margin: 3px 10px 0 5px;
        float: left;
    }
    #handled .tools, #mind_question input, #mind_handled input{
        display: none;
    }
    #handled input{
        display: block;
    }
</style>