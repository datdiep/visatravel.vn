<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->

    <section class="content">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/assets/upload/avatar/system.jpg" alt="User profile picture">

                        <h3 class="profile-username text-center">Sailor Moon</h3>

                        <p class="text-muted text-center">Thủy thủ mặt trăng</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Sinh ngày</b> <a class="pull-right">14/05/2017</a>
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-mortar-board margin-r-5"></i><b>Kiến thức</b> <a class="pull-right">543</a>

                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-book margin-r-5"></i><b>Kỹ năng</b> <a class="pull-right">13,287</a>
                            </li>
                            <li class="list-group-item">
                                <b>Danh hiệu trí tuệ</b> <a class="pull-right">Ngu như con bò</a>
                            </li>
                        </ul>
                        <p>
                            <span class="label label-danger">UI Design</span>
                            <span class="label label-success">Coding</span>
                            <span class="label label-info">Javascript</span>
                            <span class="label label-warning">PHP</span>
                            <span class="label label-primary">Node.js</span>
                        </p>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Đạo tạo dịch vụ</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <button class="btn btn-warning btn-block build_services" rel_id="0">XÂY DỰNG DỊCH VỤ</button>
                        <hr>
                        <ul class="todo-list ui-sortable" id="list_services">
                            <?php foreach ($services as $value): ?>
                                <li>
                                    <span class="handle ui-sortable-handle">
                                        <i class="fa fa-ellipsis-v"></i>
                                        <i class="fa fa-ellipsis-v"></i>
                                    </span>
                                    <span class="text"><?php echo $value['name']; ?></span>
                                    <div class="tools">
                                        <i class="fa fa-trash-o del_services" rel_id="<?php echo $value['id'];?>"></i>
                                        <i class="fa fa-edit build_services" rel_id="<?php echo $value['id'];?>"></i>
                                        <a href="/<?php echo ADMIN_URL; ?>sailormoon/training?service=<?php echo $value['alias']; ?>"><i class="fa fa-mortar-board"></i></a>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box" id="list_mind_service">

                </div>
            </div>
            <!-- /.col -->
        </div>
    </section>
</div><!-- /.row -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="build_services">



        </div>
    </div>
</div>
<script>
    $('body').on('click', '.build_services', function () {
        var id = $(this).attr('rel_id');
        $.post('/<?php echo ADMIN_URL; ?>sailormoon/build_services', {id: id}, function (results) {
            $('#build_services').html(results);
        });
        $("#myModal").modal("show");
    })
    $('body').on('click', '.del_services', function () {
        var id = $(this).attr('rel_id');
        var parent = $(this).parent().parent();
        show_dialog('Hãy suy nghĩ kỉ trước khi quyết định', function () {
            $.post('/<?php echo ADMIN_URL; ?>sailormoon/del_services', {id: id}, function (results) {
                parent.remove();
            });
        });
    })
    $('body').on('click', '.add_select', function () {
        var html = $('#temp_select').html();
        $('#build_services .panel').prepend(html);
    })
    $('body').on('click', '#build_services .panel-body button', function () {
        var parent = $(this).parent().parent();
        parent.remove();
    })
    $('body').on('keypress', '.inputTextBox', function (event) {
        var regex = new RegExp("^[a-z0-9_\r\n]*$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    function del(id) {
        show_dialog('Bạn có muốn xóa trường hợp id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>checklist/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }

    var mind_service_pos = <?php echo $mind_service_pos; ?>;
    function list_mind_service(pos) {
        mind_service_pos = pos;
        $.post('/<?php echo ADMIN_URL; ?>sailormoon/load_mind_service', {pos: pos}, function (results) {
            $('#list_mind_service').html(results);
        });
    }

    list_mind_service(mind_service_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    $('#type').change(function () {
        loadlist(0);
    });
    $('body').on('keydown', 'textarea', function (e) {
        if (e.keyCode == 13) {
            autosize(this);
        }
    });
    function autosize(_this) {
        setTimeout(function () {
            _this.style.cssText = 'height:auto; padding:0';
            // for box-sizing other than "content-box" use:
            _this.style.cssText = '-moz-box-sizing:content-box';
            _this.style.cssText = 'height:' + _this.scrollHeight + 'px';
        }, 0);
    }
</script>
<style>
    .panel-group .panel-body{
        position: relative;
    }
    .panel-group .panel-body:nth-child(odd){
        background: #f4f4f4;
    }
    .panel-group .pull-right button{
        position: absolute;
        right: 5px;
        top: 5px;
        z-index: 1000;
    }
    .panel-group .panel{
        margin-top: 5px;
    }
    textarea{  
        overflow:hidden;
    }
</style>