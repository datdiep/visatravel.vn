<form method="POST">
    <?php
    error_reporting(E_ALL ^ E_NOTICE);
    $data = file_get_contents(APPPATH . '../assets/pdf/fields_956a.txt');
    $data = explode(PHP_EOL . "---" . PHP_EOL, $data);
    $result = [];
    for ($i = 1; $i < count($data); $i++) {
        $lines = explode(PHP_EOL, $data[$i]);
        $obj = array();
        foreach ($lines as $key => $value) {
            $properties = explode(': ', $value);
            if ($properties[0] == 'FieldType' && $properties[1] == 'Button') {
                $obj['type'] = 'button';
            }
            if ($properties[0] == 'FieldType' && $properties[1] == 'Text') {
                $obj['type'] = 'text';
            }
            if ($properties[0] == 'FieldName') {
                $obj['name'] = $properties[1];
            }
            if ($properties[0] == 'FieldStateOption') {
                $obj['children'][] = $properties[1];
            }
        }
        $result[] = $obj;
    }

    foreach ($result as $key => $value) {
        $value['title'] = $value['name'];
        $value['name'] = str_replace(array('.', ' '), array('__', '_'), $value['name']);
        if ($value['type'] == 'text') {
            echo '
				<div style="clear:both">
					<div style="width: 200px;float:left">
						' . $value['title'] . '
					</div>
					<div style="width: 500px;float:left">
						<input type="text" style="width: 500px" name="' . $value['name'] . '" />
					</div>
				</div>

			';
        }
        if ($value['type'] == 'button') {
            echo '
				<div style="clear:both">
					<div style="width: 200px;float:left">
						' . $value['title'] . '
					</div>
					<div style="width: 500px;float:left">
						<select style="width: 300px" name="' . $value['name'] . '">
						';

            foreach ($value['children'] as $k => $v) {
                echo '<option value="' . $v . '">' . $v . '</option>';
            }
            echo '
						</select>
					</div>
				</div>

			';
        }
    }
    ?>
    <div style="clear:both">
        <input type="submit">
    </div>
</form>