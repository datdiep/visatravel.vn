<?php $this->load->library('transload'); ?>
<div class="box-body">
    <h4><i class="fa fa-mortar-board"></i> Kiến thức học được</h4>
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>#</th>
                    <th>Dịch vụ</th>   
                    <th>Trường hợp</th>
                    <th>Giá($)</th>
                    <th>Phí lãnh sự($)</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($mind_services as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?>.</td> 
                        <td><a href="/<?php echo ADMIN_URL . 'sailormoon/training_edit/' . $item['id']; ?>"><?php echo $services[$item['service']]['name']; ?></a></td>                                                 
                        <?php 
                            $select = unserialize($services[$item['service']]['select']);
                            $mind_case = unserialize($item['mind_case']);//pre($select);?>
                        <td>
                            <?php foreach ($mind_case as $k => $v){
                                echo '<b>'.$select[$k]['lable'].':</b> '.$select[$k]['option'][$v].'<br>';
                            };?>
                        </td>
                        <td><?php echo $item['fee'] ?></td>
                        <td><?php echo $item['fee_consul'] ?></td>
                        <td>
                            <a href="/<?php echo ADMIN_URL . 'sailormoon/training_edit/' . $item['id']; ?>">Edit</a> | 
                            <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      
        </table>
    </div>
</div>

<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
