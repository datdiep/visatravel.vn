<div class="box-body">
   
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Title</th>
                    <th>Danh Mục Dịch vụ</th> 
                    <th>icon</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($category_service as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td> 
                        <td><a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item['id']; ?>"><?php echo $item['title']; ?></a></td>                                                 
                        <td><?php $item['title'] ?></td>
                        <td > <image width="100" src="/assets/upload/category_service/<?php echo $item['icon'] ?>"> </td>
                        <td>
                            <a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item['id']; ?>">Edit</a> | 
                        </td>
                    </tr>
                    <?php foreach ($item['childs'] as $item_child):
                        ?>
                        <tr>
                            <td><?php echo $item_child['id']; ?></td> 
                            <td><a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item_child['id']; ?>"><?php echo $item_child['title']; ?></a></td>                                                 
                            <td><?php $item['title'] ?></td>
                            <td > <image width="100" src="/assets/upload/category_service/<?php echo $item_child['icon'] ?>"> </td>
                            <td>
                                <a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item_child['id']; ?>">Edit</a> | 
                                <a onclick="del(<?php echo $item_child['id']; ?>)" href="javascript:void(0)">Delete</a> 
                            </td>
                        </tr>
                        <?php
                    endforeach;
                endforeach;
                ?>
            </tbody>      
        </table>
    </div>
</div>
<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
