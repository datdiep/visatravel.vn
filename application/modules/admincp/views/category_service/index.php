
<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>QUẢN LÝ DANH MỤC DỊCH VỤ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List category service</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>category_service/add"><button style="width: 200px;" class="btn btn-block btn-primary">Thêm Danh Mục Dịch Vụ</button></a>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th>STT</th>
                                        <th>Title</th>                                                                            
                                        <th>icon</th>
                                        <th>Actions</th>                                                
                                    </tr>
                                </thead>
                                <tbody>    
                                    <?php foreach ($category_service as $item): ?>
                                        <tr>
                                            <td><?php echo $item['id']; ?></td> 
                                            <td><a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item['id']; ?>"><?php echo $item['title']; ?></a></td>                                                 
                                            <td > <image width="100" src="/assets/upload/category_service/<?php echo $item['icon'] ?>"> </td>
                                            <td>
                                                <a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item['id']; ?>">Edit</a> | 
                                            </td>
                                        </tr>
                                        <?php if (!empty($item['childs'])): ?>
                                            <?php foreach ($item['childs'] as $item_child):?>
                                                <tr>
                                                    <td></td> 
                                                    <td>|---- <a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item_child['id']; ?>"><?php echo $item_child['title']; ?></a></td>                                                 
                                                    <td > <image width="100" src="/assets/upload/category_service/<?php echo $item_child['icon'] ?>"> </td>
                                                    <td>
                                                        <a href="/<?php echo ADMIN_URL . 'category_service/edit/' . $item_child['id']; ?>">Edit</a> | 
                                                        <a onclick="del(<?php echo $item_child['id']; ?>)" href="javascript:void(0)">Delete</a> 
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php endif;?>
                                    <?php endforeach; ?>
                                </tbody>      
                            </table>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Do you want to delete category news id =' + id + ' ?', function () {
            $.post('/<?php echo ADMIN_URL; ?>category_service/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }
</script>

