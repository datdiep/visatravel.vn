<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT CATEGORY</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>category">List category_service</a></li>
            <li class="active">Edit Country</li>
        </ol>
    </section>    
    <section class="content">
        <!-- Custom Tabs (Pulled to the right) -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="pull-left header"><i class="fa fa-th"></i> <?php echo $category_detail['name']; ?></li>
                <li class="active"><a href="#tab_1-1" data-toggle="tab">Thông tin cơ bản</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1-1">

                    <div class="row">

                        <form id="form" method="post" enctype="multipart/form-data">
                            <div class="col-md-12">
                                <?php if ($check_error == 0): ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                                        Sửa thành công
                                    </div>
                                <?php endif; ?>
                                <?php if ($check_error == 1): ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                                        <?php echo validation_errors(); ?>                          
                                    </div>
                                <?php endif; ?>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="title">Tên Danh Mục Dịch Vụ</label>
                                    <input type="text" class="form-control" id="title"  onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="title" value="<?php echo $category_detail['title']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="alias">Alias</label>
                                    <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $category_detail['alias']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Chọn loại dịch vụ</label>
                                    <select name="id_parent" class="form-control select2" style="width: 100%;">
                                        <?php foreach ($category_service as $value): ?>
                                            <option <?php echo $category_detail['id_parent'] == $value['id'] ? "selected" : "" ?> value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                                        <?php endforeach; ?>
                                        <option value="0">Chọn là danh mục dịch vụ</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="image">Icon</label>
                                    <input type="file" class="form-control"  name="image" >
                                    <?php if ($category_detail['icon']): ?>
                                        <br/>
                                        <img src="/assets/upload/category_service/<?php echo $category_detail['icon']; ?>" width="200"/>
                                    <?php endif; ?>
                                </div>

                            </div>
                            <div class="col-md-5">

                                <div class="form-group">
                                    <label for="seo_title">Seo title</label>
                                    <input type="text" class="form-control" name="seo_title" value="<?php echo @$category_detail['seo_title']; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="meta_keyword">Meta keyword</label>
                                    <input type="text" class="form-control" name="meta_keyword" value="<?php echo @$category_detail['meta_keyword']; ?>">
                                </div>
                            </div>
                            <div  class="col-md-12">   
                                <div style="padding: 15px;" class="box box-primary box-success" ">
                                    <div class="box-header">
                                        <h3 class="box-title">Giới thiệu</h3>
                                    </div>  
                                    <textarea id="textarea_description" style="width: 100%;height: 150px;" name="description" style="width: 100%;  height: 200px"><?php echo @$category_detail['description']; ?></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="box-footer">
                                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
                <!-- /.tab-pane -->


            </div>
            <!-- /.tab-content -->
            <div class="overlay loading">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>

        <!-- nav-tabs-custom -->
    </section>

</div>
<script>

    CKEDITOR.config.entities_latin = false;
    CKEDITOR.config.removePlugins = 'autosave';
    var editor1 = CKEDITOR.replace('textarea_content');
    var editor2 = CKEDITOR.replace('textarea_note');

    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    var data = {};
    var error = 0;
    var ele;
    $('.btn_tips').click(function () {
        error = 0;
        $('.loading').fadeIn();
        var id = $(this).attr('rel');
        ele = $('#' + id);
        $.when(get_data(ele)).done(function () {
            if (error == 1) {
                $('.loading').fadeOut();
                return false;
            }
            console.log(data);
            $.post('/<?php echo ADMIN_URL; ?>category_service/add_edit_tips', {data: data}, function (results) {
                console.log(results);
                $('input[name="id"]', ele).val(results);
                $('.loading').fadeOut();
                $('.alert_popup', ele).fadeIn();
                var tb = '<?php echo date('d-m-Y \L\ú\c H:i:s'); ?>: Cập nhật Tips <u>"' + data.tips + '"</u> thành công';
                $('.alert_popup p', ele).html(tb);
                $(window).scrollTop(0);
                //$('#list_ajax').html(results);
            });
        });
    })
    $('.close').click(function () {
        var parent = $(this).parent().parent();
        parent.fadeOut();
    })
    function get_data(ele) {
        data = {
            category_service_id: <?php echo $category_detail['id']; ?>
        };
        $('.form-control', ele).each(function () {
            var key = $(this).attr('name');
            if (key == 'title' && $(this).val() == '') {
                alert('Title không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if ($(this).hasClass('return_img')) {
                data.image = $(this).val();
            } else if (key == 'content') {
                var source = $(this).attr('id');
                data.content = CKEDITOR.instances[source].getData();
            } else {
                data[key] = $(this).val();
            }
        })
    }
</script>
<style>
    .nav-tabs-custom{
        position: relative;
    }
    .loading{
        position: absolute;
        top:0px;
        left: 0px;
        right: 0px;
        bottom: 0px;
        background: rgba(255,255,255,.8);
        display: none;
    }
    .loading .fa{
        position: absolute;
        top: 50%;
        left: 50%;
        margin-left: -15px;
        margin-top: -15px;
        color: #000;
        font-size: 30px;
    }
    .alert_popup{
        display: none;
    }
</style>