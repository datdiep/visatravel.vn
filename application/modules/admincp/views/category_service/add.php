<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM DANH MỤC VISA</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>category_service">List Category Service</a></li>
            <li class="active">Add CATEGORY VISA</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <style>
                        .form-group label span{font-weight: normal!important}
                    </style>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thêm Danh Mục Visa</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tên danh mục</label>
                                <input type="text" class="form-control" id="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="title" placeholder="Tên quốc gia">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                            </div>
                            <div class="form-group">
                                <label>Chọn loại dịch vụ</label>
                                <select name="id_parent" class="form-control select2" style="width: 100%;">
                                    <option value="0">Chọn dịch vụ của bài viết (Không chọn là danh mục dịch vụ)</option>
                                    <?php foreach ($category_service as $value): ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['title']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="image">Icon danh mục</label>
                                <input type="file" class="form-control"  name="image" >
                            </div>

                        </div>

                    </div> 
                </div>

                <div class="col-md-6">  
                    <div class="box box-primary box-default">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin thêm (Không bắt buộc)</h3>
                        </div>
                        <div class="box-body">

                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword">
                            </div>
                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu</h3>
                        </div>  
                        <textarea id="textarea_description" name="description" style="width: 100%; height: 200px"></textarea>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
        </form>
    </section>
</div>
<script>
    CKEDITOR.config.entities_latin = false;

    var editor1 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
</script>