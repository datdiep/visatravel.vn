<div class="sTableWrapper" style="padding: 5px;">     
    <div class="col-xs-6">
        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">

            <tr class="sTableHead" >              
                <th>Thiết bị</th>                            
                <th>Trạng thái</th>                            
                <th>Số đơn hàng</th>                                   
                <th>Tổng tiền</th>                                   

            </tr>
            <?php if (!empty($orders)): ?>
                <?php
                $total_money = $total_order = 0;
                ?>
                <?php foreach ($orders as $item): ?>               
                    <tr class="gradeC ">                       
                        <td><?php echo $item['device_type']; ?></td>                       
                        <td><?php echo $status_order[$item['status']]; ?></td>                       
                        <?php
                        $total_order+= $item['count'];
                        $total_money+=$item['total'];
                        ?>
                        <td class="format_number"><?php echo $item['count']; ?></td>                            
                        <td class="format_number"><?php echo $item['total']; ?></td>                            
                    </tr>
                <?php endforeach; ?>
                <tr style="background: #D2A6AE;">
                    <td colspan="2">Tổng</td>
                    <td class="format_number"><?php echo $total_order; ?></td>                            
                    <td class="format_number"><?php echo $total_money; ?></td>                            
                </tr>
            <?php else: ?>
                <tr><td colspan="10">Khong co thong ke</td></tr>
            <?php endif; ?>

        </table>
    </div>

    <div style="clear: both;"></div>
</div>

<style>
    .empty{color: #999;}
    .source td{font-weight: bold;;background: #f4f4f4;}
</style>

<script>
    $('.format_number').autoNumeric('init', {aPad: false});
</script>