<div class="sTableWrapper" style="padding: 5px;"> 
    <div class="col-xs-6">
        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">                  
                    <th>Source</th>
                    <th>MÃ SP</th>                                                            
                    <th>Tên SP</th>                                                            
                    <th>Mua</th>                                                            
                    <th>View</th>                                                            
                </tr>
            </thead>       
            <?php if (!empty($results)): ?>
                <?php
                $total_view = $total_buy = $i = 0;
                ?>
                <?php foreach ($results as $item): ?>
                    <?php
                    $total_view+= $item['view'];
                    $total_buy+= $item['buyed'];
                    $i++;
                    ?>
                    <?php if ($i < 30): ?>
                        <tr class="gradeC ">   
                            <td><?php echo $item['source']; ?></td>
                            <td><?php echo $item['product_id']; ?></td>
                            <td><?php echo $item['product_name']; ?></td>

                            <td class="format_number"><?php echo $item['buyed']; ?></td>
                            <td class="format_number"><?php echo $item['view']; ?></td>                    
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
                <tr style="background: #D2A6AE;">
                    <td colspan="3">Tổng</td>
                    <td class="format_number"><?php echo $total_buy; ?></td>
                    <td class="format_number"><?php echo $total_view; ?></td>                
                </tr>
            <?php else: ?>
                <tr><td colspan="10">Khong co thong ke</td></tr>
            <?php endif; ?>

        </table>
    </div>
    <div class="col-xs-6">
        <div id="temp"></div>
        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">

            <tr class="sTableHead" >  
                <th>Nguồn</th>
                <th>Chiến dịch</th>
                <th>Thiết bị</th>                            
                <th>Trạng thái</th>                            
                <th>Số đơn hàng</th>                                   
                <th>Tổng tiền</th>                                   

            </tr>
            <?php if (!empty($orders)): ?>
                <?php
                $total_money = $total_order = 0;
                $total_source = '';
                ?>
                <?php foreach ($orders as $item): ?>                       
                    <tr class="gradeC ">    
                        <td><?php echo $item['source']; ?></td>
                        <td><?php echo $item['campaign']; ?></td>
                        <td><?php echo $item['device_type']; ?></td>                       
                        <td><?php echo $item['status']; ?></td>                       
                        <?php
                        $total_order+= $item['count'];
                        $total_money+=$item['total'];
                        @$total_source[$item['source']]['count']+=$item['count'];
                        @$total_source[$item['source']]['total']+=$item['total'];
                        ?>
                        <td class="format_number"><?php echo $item['count']; ?></td>                            
                        <td class="format_number"><?php echo $item['total']; ?></td>                            
                    </tr>
                <?php endforeach; ?>
                <tr style="background: #D2A6AE;">
                    <td colspan="4">Tổng</td>
                    <td class="format_number"><?php echo $total_order; ?></td>                            
                    <td class="format_number"><?php echo $total_money; ?></td>                            
                </tr>
            <?php else: ?>
                <tr><td colspan="10">Khong co thong ke</td></tr>
            <?php endif; ?>

        </table>

        <table class="table table-bordered table-hover dataTable table_temp" role="grid" aria-describedby="example2_info">

            <tr class="sTableHead" >  
                <th>Nguồn</th>        
                <th>Số đơn hàng</th>                                   
                <th>Tổng tiền</th>                                   
            </tr>
            <?php if (!empty($total_source)): ?>  
                <?php
                $total_money = $total_order = 0;                
                ?>
                <?php foreach ($total_source as $k => $item): ?>                       
                    <tr class="gradeC ">    
                        <td><?php echo $k; ?></td>
                        <td><?php echo $item['count']; ?></td>
                        <td><?php echo number_format($item['total']); ?></td>                                                                                                          
                    </tr>
                    <?php
                    $total_order+= $item['count'];
                    $total_money+=$item['total'];                 
                    ?>
                <?php endforeach; ?>     
                <tr style="background: #D2A6AE;">
                    <td colspan="">Tổng</td>
                    <td class="format_number"><?php echo $total_order; ?></td>                            
                    <td class="format_number"><?php echo $total_money; ?></td>                            
                </tr>
            <?php else: ?>
                <tr><td colspan="10">Khong co thong ke</td></tr>
            <?php endif; ?>

        </table>
    </div>

    <div style="clear: both;"></div>
</div>

<style>
    .empty{color: #999;}
    .source td{font-weight: bold;;background: #f4f4f4;}
</style>

<script>
    $(".table_temp").appendTo('#temp');
    $('.format_number').autoNumeric('init', {aPad: false});
</script>