<div class="sTableWrapper" style="padding: 5px;">     
    <div class="col-xs-6">
        <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">

            <tr class="sTableHead" >              
                <th>Tên sản phẩm</th>                            
                <th>View</th>                            
                <th>Lượt mua</th>                                                   

            </tr>
            <?php if (!empty($orders)): ?>              
                <tr class="gradeC ">                       
                    <td><?php echo $orders['product_name']; ?></td>                                          
                    <td class="format_number"><?php echo $orders['total_view']; ?></td>                            
                    <td class="format_number"><?php echo $orders['total_buyed']; ?></td>                            
                </tr>
            <?php else: ?>
                <tr><td colspan="10">Khong co thong ke</td></tr>
            <?php endif; ?>

        </table>
    </div>

    <div style="clear: both;"></div>
</div>
