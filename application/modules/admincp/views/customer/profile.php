<div class="modal fade bs-example-modal-lg" id="profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user"></i> 
                    <?php if (@$customer['id']): ?>
                        Cập nhật hồ sơ
                    <?php else: ?>
                        Thêm hồ sơ mới
                    <?php endif; ?>
                </h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <div class="alert alert-success alert-dismissable display-none"></div>
                    <div class="form-group col-md-6">
                        <label for="phone">Số điện thoại</label>
                        <input type="text" value="<?php echo @$customer['phone']; ?>" onkeyup="if (/\D/g.test(this.value))
                                    this.value = this.value.replace(/\D/g, '')" class="order-input form-control" name="phone" placeholder="Số điện thoại khách hàng">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="full_name">Tên khách hàng</label>
                        <input type="text" class="order-input form-control" name="full_name" placeholder="Tên khách hàng" value="<?php echo @$customer['full_name']; ?>">
                    </div>



                    <div class="form-group col-md-6">
                        <label for="service_id">Dịch vụ chính</label>
                        <select name="service_name" class="order-input form-control">
                            <option value="">Chọn dịch vụ</option>
                            <?php foreach ($services as $v) : ?>
                                <option <?php echo @$customer['service_name'] == $v['name'] ? 'selected' : '' ?> value="<?php echo $v['name']; ?>"><?php echo $v['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="email">Email</label>
                        <input type="text" value="" class="order-input form-control" name="email" placeholder="Nhập email liên hệ của khách">
                    </div>

                    <?php if (@$customer['id']): ?>
                        <div class="form-group col-md-12">
                            <label for="status">Tiến trình</label>
                            <select name="status" class="order-input form-control" style="border: 1px #ff0000 dashed">
                                <?php foreach ($status as $k => $v) : ?>
                                    <option <?php echo @$customer['status'] == $v ? 'selected' : ''; ?> value="<?php echo $v; ?>"><?php echo $customer_status[$v]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif; ?>

                    <div class="form-group col-md-6">
                        <label for="source">Nguồn gốc khách hàng</label>
                        <select name="source" class="order-input form-control">
                            <option value="">Chọn nguồn gốc khách hàng</option>
                            <?php foreach ($customer_source as $k => $v) : ?>
                                <option <?php echo @$customer['source'] == $k ? 'selected' : ''; ?> value="<?php echo $k; ?>"><?php echo $v; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="form-group col-md-6">
                        <label for="facebook">Facebook</label>
                        <input type="text" class="order-input form-control" name="facebook" placeholder="Nhập link facebook của khách" value="<?php echo @$customer['facebook']; ?>">
                    </div>

                    <div class="form-group col-md-12">
                        <label for="leave_message">Ghi chú khách hàng</label>
                        <textarea name="note" style="width: 100%; height: 110px" class="order-input form-control"><?php echo @$customer['note']; ?></textarea>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="leave_message">Kết quả</label>
                        <textarea name="content" style="width: 100%; height: 110px" class="order-input form-control"><?php echo @$customer['content']; ?></textarea>
                    </div>

                </div>
            </div>

            <div class="modal-footer button_action">
                <button class="save_customer btn btn-info margin-r-5" rel_id="<?php echo!empty($customer) ? $customer['id'] : 0; ?>">Lưu khách hàng</button>
                <p class="btn btn-default change_box" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
            </div>
        </div>
    </div> 
</div>
