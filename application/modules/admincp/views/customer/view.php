<div class="content-wrapper">    
    <section class="content-header">
        <h1>XEM ĐƠN #<?php echo $order['id']; ?> </h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">Quản lý đơn hàng</a></li>
            <li class="active">Kiểm tra đơn</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">

            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <?php if (!empty($msg_error)): ?>
                        <div class="alert alert-success alert-dismissable">
                            <?php echo $msg_error; ?>
                        </div>
                    <?php endif; ?>
                    <div class="box-footer">
                        <?php if (empty($order['who_handle'])): ?>
                            <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false && $order['status'] == 'handle'): ?>
                                <a class="btn btn-primary" href="/admincp/customer/check/<?php echo $order['id']; ?>"><i class="fa fa-edit"></i>  NHẬN KIỂM TRA</a>                                   
                            <?php endif; ?>
                            <?php if ((strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false || !empty($user['role_network'])) && $order['status'] != 'cancel' && $order['status'] != 'trash'): ?>                           
                                <a  class="btn btn-default" onclick="cancel(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-ban"></i> Hủy</a>
                            <?php endif; ?>
                        <?php else: ?>
                            <span class="btn" style="background:#004b91;padding: 5px 10px;text-transform: uppercase;color:#fff"><b><?php echo $order['who_handle']; ?></b>  <?php echo $map_order[$order['progress']]; ?></span>                                
                        <?php endif; ?>
                        <?php if ((strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false || !empty($user['role_network'])) && ($order['status'] == 'cancel' || $order['status'] == 'trash')): ?>
                            <a  class="btn btn-default" onclick="uncancel(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-reply-all"></i> Phục hồi</a>
                        <?php endif; ?>
                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',trash,') !== false && $order['status'] == 'cancel'): ?>
                            <a  class="btn btn-default" onclick="trash(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Xóa</a>
                        <?php endif; ?>
                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',empty,') !== false && $order['status'] == 'trash'): ?>
                            <a  class="btn btn-default" onclick="trash_empty(<?php echo $order['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                        <?php endif; ?>
                        <?php if (strpos(',' . $user['role_order_progress'] . ',', ',check,') !== false || !empty($user['role_network'])): ?>
                            <div class="box-tools pull-right">
                                <span>TOTAL FEES: <b><?php echo $order['total_fees'] . '$'; ?></b></span>
                                <span>Services fees: <b><?php echo $order['services_fees'] . '$'; ?></b></span>
                                <span>Processing fees: <b><?php echo $order['processing_time_fees'] . '$'; ?></b></span>
                                <span>Nationality fees: <b><?php echo $order['nationality_fees'] . '$'; ?></b></span>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin visa (<?php echo $order['status']; ?>)</h3>

                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="alias">Trạng thái thanh toán</label>
                                <input class="form-control" value="<?php echo $order['has_get_money'] == 1 ? 'Đã thanh toán' : 'Chưa thanh toán'; ?>">
                            </div>
                            <div class="form-group">
                                <label for="pax">Number of persons</label>
                                <input class="form-control" value="<?php echo $order['pax']; ?> persons">
                            </div>
                            <div class="form-group">
                                <label for="visit_purpose">Purpose of visit</label>
                                <input class="form-control" value="<?php echo $order['purpose_of_visit']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="visa_type">Type of visa</label>
                                <input class="form-control" value="<?php echo $order['type_of_visa']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="arrival_port">Arrival airport</label>
                                <input class="form-control" value="<?php echo $order['arrival_airport']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="arrival_date">Arrival date</label>
                                <input class="form-control" value="<?php echo $order['arrival_date']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="category_news">Processing time</label>
                                <input class="form-control" value="<?php echo $order['processing_time']; ?>">

                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin liên hệ</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="contact_fullname">Full name</label>
                                <input type="text" value="<?php echo $order['full_name']; ?>" class="form-control" name="contact_fullname">
                            </div>
                            <div class="form-group">
                                <label for="contact_email">Email</label>
                                <input type="text" value="<?php echo $order['email']; ?>" class="form-control" name="contact_email">
                            </div>
                            <div class="form-group">
                                <label for="contact_phone">Phone number</label>
                                <input type="text" value="<?php echo $order['phone']; ?>" class="form-control" name="contact_phone">
                            </div>
                            <div class="form-group">
                                <label for="contact_address">Address</label>
                                <input type="text" value="<?php echo $order['address']; ?>" class="form-control" name="contact_address">
                            </div>
                            <div class="form-group">
                                <label for="leave_message">Leave a message</label>
                                <textarea id="comment" name="leave_message" class="form-control" rows="3"><?php echo $order['leave_message']; ?></textarea>
                            </div>
                        </div> 
                    </div>


                    <div class="box box-footer">
                        <div class="form-group">
                            <label for="exampleInputFile">Tải file lên (.zip)</label>
                            <input type="file" id="exampleInputFile">
                        </div>
                                    <!--       <a href="" class="btn btn-primary"><i class="fa fa-upload"></i> Up file</a>
                                  <a href="" class="btn btn-primary"><i class="fa fa-download"></i> Tải file</a>              -->
                    </div>

                </div>

                <div  class="col-md-8"> 

                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách hộ chiếu</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Full name</th>
                                        <th style="width: 100px">Gender</th>
                                        <th style="width: 140px">Birth date</th>
                                        <th>Nationality</th>
                                        <th style="width: 150px">Passport number</th>
                                        <th style="width: 90px">Action</th>
                                    </tr>
                                    <?php foreach ($order_sku as $i => $p): ?>
                                        <tr>
                                            <td><?php echo $i ?>.</td>
                                            <td><span><?php echo $p['full_name']; ?></span><i class="fa fa-edit fa-lg" rel="full_name" title="Sữa"></i></td>
                                            <td><span><?php echo $p['gender']; ?></span><i class="fa fa-edit fa-lg" rel="gender" title="Sữa"></i></td>
                                            <td><span><?php echo date('d/m/Y', strtotime($p['date_of_birth'])); ?></span><i class="fa fa-edit fa-lg" rel="date_of_birth" title="Sữa"></i></td>
                                            <td><span><?php echo $p['nationality']; ?></span> <?php echo $p['special_fee'] == 0 ? '' : '(' . $p['special_fee'] . '$)'; ?><i  rel="nationality" class="fa fa-edit fa-lg" title="Sữa"></i></td>
                                            <td><span><?php echo $p['passport_number']; ?></span><i rel="passport_number" class="fa fa-edit fa-lg" title="Sữa"></i></td>
                                            <td class="actions">
    <!--                                                <i class="fa fa-edit fa-lg" title="Sữa"></i>
                                                <i class="fa fa-save fa-lg" title="Lưu"></i>-->
                                                <i class="fa fa-trash-o fa-lg" title="Xóa"></i> 
                                                <i class="fa fa-ban fa-lg" title="Từ chối"></i> 
                                                <i class="fa fa-print fa-lg"  title="In"></i>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>


                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- Chat box -->
                    <?php echo $load_chat; ?>
                    <!-- /.box (chat box) -->

                </div>
            </div>
            <?php if (!empty($user['role_network'])): ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">
                            <?php if (!empty($user['role_network'])): ?>
                                <div class="box-tools pull-right">
                                    <span>agency: <b><?php echo $order['agency']; ?></b></span>
                                    <span>source: <b><?php echo $order['source']; ?></b></span>
                                    <span>campaign: <b><?php echo $order['campaign']; ?></b></span>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </form>
    </section>

</div>
<script>
    var order_id = <?php echo $order['id']; ?>;
    var model = 'order_services_model';

</script>
<script src="/assets/admin/js/order.js"></script>
<style>
    .box-tools span{
        display: inline-block;
        padding: 2px 10px;
        font-size: 20px;
    }
    .box-tools span b{
        color: red
    }
    #passport td{
        position: relative;
        padding-right: 30px;

    }
    #passport input{
        padding: 0 5px;
    }
    #passport i{
        margin: 0 3px;
        cursor: pointer;
        color: #ccc;
    }
    #passport i:hover{
        color: red
    }
    .fa-save{
        display: none;
    }
    #passport .fa-edit{
        position: absolute;
        top: 12px;
        right: 5px;
        display: none;
    }
    #passport .fa-times-circle{
        position: absolute;
        top: 12px;
        right: 5px;
    }
    .field-date-data .fa-calendar{
        position: absolute;
        right: 5px;
        top: 5px;
    }
    .date_of_birth{
        padding-right: 20px;
    }
    .actions{
        padding-right: 0px !important;
    }
    .filter_chat span{
        padding: 2px 5px;
        cursor: pointer;
        background: #f4f4f4;
    }
    .filter_chat span:hover{
        background: #ccc;
    }
    .filter_chat .active{
        background: #00a65a;
        color: #fff;
    }
</style>