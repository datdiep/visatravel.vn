<div class="box-body table-responsive">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered " role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>Ngày tạo</th>
                    <th>Tên khách</th>
                    <th>Số điện thoại</th>
                    <th>Dịch vụ</th>
                    <th>Ghi chú</th>
                    <th>Kết quả</th>
                    <th>Tiến trình</th>
                    <th style="text-align: center">Thực thi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo date('d/m/Y', strtotime($item['date_create'])); ?></td>
                        <td><?php echo $item['full_name']; ?> 
                        <td>
                            <?php
                            echo substr($item['phone'], 0, -2) . 'xx ';

                            if ($item['status'] == 'potential') {
                                echo '<i class="fa fa-star c-orange"></i>';
                            }
                            ?>
                        </td>


                        </td>

                        <td>
                            <?php echo $item['service_name'];
                            ?>
                        </td>
                        <td><?php echo $item['note']; ?></td>
                        <td><?php echo $item['content']; ?></td>
                        <td><?php echo $item['status']; ?></td>
                        <td style="text-align: left">
                            <?php if ($item['status'] == 'trash'): ?>
                                <a  class="btn btn-default" onclick="undo(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-undo"></i> Undo</a>
                                <?php if (!empty($user['role_network'])): ?>
                                    <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if ($item['consultant_user']): ?>
                                    <!-- Nếu đã có nội dung với khách hàng -->
                                    <?php if ($item['content']): ?>
                                        <?php if ($item['consultant_user'] == $user['username']): ?>
                                            <button class="btn btn-default add_profile" rel_id='<?php echo $item['id']; ?>' title="Khách hàng đã được tư vấn "><i class="fa fa-phone-square"></i></button> 
                                        <?php endif; ?>


                                        <span class="c-gray"><?php echo $item['consultant']; ?> Đã tư vấn</span>

                                        <!-- Mới click vào -->    
                                    <?php else: ?>
                                        <button  class="btn btn-default <?php
                                        if ($item['consultant_user'] == $user['username']) {
                                            echo 'add_profile c-orange';
                                        } else {
                                            echo 'c-green';
                                        }
                                        ?>" rel_id='<?php echo $item['id']; ?>' title="Khách hàng chưa tư vấn xong"><i class="fa fa-phone-square"></i></button>
                                        <?php echo $item['consultant']; ?> đang tư vấn ....
                                    <?php endif; ?>

                                <?php else: ?>
                                    <button  class="btn btn-default add_profile c-purple" rel_id='<?php echo $item['id']; ?>' title="Nhận khách hàng này"><i class="fa fa-user"></i></button> Khách mới cần được tư vấn
                                    <?php if ($item['username'] == $user['username'] && $item['username'] != 'new'): ?>
                                        <a class="btn btn-default" onclick="trash(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash"></i></a>
                                    <?php endif; ?>
                                <?php endif; ?>


                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>

    </div>
    <div class="col-md-4 col-md-offset-5">
        <ul class="pagination">
            <?php echo $links; ?>
        </ul>
    </div>

</div>

<style>
    #example2 th{white-space: nowrap}
</style>