<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM HỒ SƠ XỬ LÝ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>customer">Quản lý khách hàng</a></li>
            <li class="active">thêm khách hàng</li>
        </ol>

    </section>    
    <section class="content">

        <div class="row">
            <div class="col-md-12 margin-bottom">
                <div class="box-footer">
                    <button class="save_order btn btn-info margin-r-5">Lưu khách hàng</button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissable" <?php echo empty($msg_new_order) ? 'style="display:none"' : ''; ?>>
                    <?php echo $msg_new_order; ?>
                </div>
                <div class="box box-primary box-primary <?php echo ($order['id'] != 0 && empty($msg_new_order)) ? 'collapsed-box' : ''; ?>">
                    <div class="box-header with-border">
                        <h3 class="box-title"><b> Thông tin khách hàng</b></h3>
                        <div class="box-tools">
                            <button type="button" class="btn btn-default btn-sm"><i class="fa fa-calendar-check-o"></i> Lịch hẹn</button>
                            <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-<?php echo!empty($task) ? 'plus' : 'minus'; ?>"></i></button>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group col-md-6">
                            <label for="phone">Số điện thoại</label>
                            <input type="text" value="<?php echo @$order['phone']; ?>" onkeyup="if (/\D/g.test(this.value))
                                        this.value = this.value.replace(/\D/g, '')" class="order-input form-control" name="phone" placeholder="Số điện thoại khách hàng">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="full_name">Tên khách hàng</label>
                            <input type="text" class="order-input form-control" name="full_name" placeholder="Tên khách hàng" value="<?php echo @$order['full_name']; ?>">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="address">Địa chỉ</label>
                            <input type="text" value="<?php echo @$order['address']; ?>" class="order-input form-control" name="address" placeholder="Địa chỉ khách hàng">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="email">Email</label>
                            <input type="text" value="<?php echo @$order['email']; ?>" class="order-input form-control"  name="email" placeholder="Nhập email liên hệ của khách">
                        </div>
                    </div> 

                </div>
            </div>
            <div class="col-md-6">   

                <div class="box box-primary box-success">
                    <div class="box-header with-border">
                        <i class="fa fa-cc-visa"></i>
                        <h3 class="box-title">Tiến trình khách hàng</h3>
                    </div>
                    <div class="box-body">
                        <div class="form-group col-md-4 nopadding">
                            <label for="who_consultant">NV tư vấn</label>

                            <?php if ($order['id'] != 0): ?>
                                <input type='text' value="<?php echo $order['who_consultant']; ?>" disabled class="form-control"/>
                            <?php else: ?>
                                <select name="who_consultant" class="order-input form-control">
                                    <option>Chọn nhân viên chỉ định</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>"><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            <?php endif; ?>
                        </div>
                        <div class="form-group col-md-4 nopadding">
                            <label for="who_consultant">Dịch vụ chính</label>
                            <select name="service_id" class="order-input form-control">
                                <?php foreach ($services as $v) : ?>
                                    <option <?php echo @$order['service_id'] == $v['id'] ? 'selected' : '' ?> value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
                                <?php endforeach; ?>
                            </select>
                            <input class="order-input" type="hidden" name="service_name">
                        </div>

                        <div class="form-group col-md-4 nopadding">
                            <label for="status">Trạng thái</label>
                            <select name="status" class="order-input form-control">
                                <?php foreach ($map_customer as $k => $v) : ?>
                                    <option <?php echo @$order['status'] == $k ? 'selected' : ''; ?> value="<?php echo $k; ?>">-- <?php echo $v; ?> --</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12 nopadding">
                            <label for="leave_message">Ghi chú khách hàng</label>
                            <textarea name="note" style="width: 100%; height: 110px" class="order-input form-control"><?php echo @$order['note']; ?></textarea>
                        </div>
                    </div>

                </div> 

            </div>
            <div class="col-md-6">
                <?php echo @$load_chat; ?>
            </div>
            <div class="col-md-12"> 
                <div class="box-footer">
                    <a class="save_order btn btn-info margin-r-5">Lưu khách hàng</a>

                </div>
            </div>

        </div>

    </section>

</div>
<table style="display:none">
    <tr class="transaction_row_data">
        <td class="hd_loai">
            <select class="transaction_data" name="type">
                <option value="Thu">Thu</option>
                <option value="Chi">Chi</option>
            </select>
        </td>
        <td class="hd_md">
            <select class="transaction_data" name="description">
                <?php foreach ($services as $v) : ?>
                    <option value="<?php echo $v['alias']; ?>"><?php echo $v['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
        <td class="hd_sl" style="width:50px"><input name="quantity" type="number" class="transaction_data"></td>
        <td class="hd_gc"><input name="note" type="text" class="transaction_data"></td>
        <td class="hd_st" style="width:110px"><input name="money" class="transaction_data format_number" type="text" rel="Thu"></td>
        <td>
            <?php if ($order['id'] > 0): ?>
                <i class="text-muted fa fa-save text-red"></i>
            <?php endif; ?>
            <i class="text-muted fa fa-trash-o"></i>
        </td>
    </tr>
</table>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="popup_control">
        </div>
    </div>
</div>
<script>
    var network_id = <?php echo $user['network_id']; ?>;

    var data = {};
    var error = 0;
    var ele;
    $('.btn_tips').click(function () {
        error = 0;
        $('.loading').fadeIn();
        var id = $(this).attr('rel');
        ele = $('#' + id);
        $.when(get_data(ele)).done(function () {
            if (error == 1) {
                $('.loading').fadeOut();
                return false;
            }
            console.log(data);
            $.post('/<?php echo ADMIN_URL; ?>country/add_edit_tips', {data: data}, function (results) {
                console.log(results);
                $('input[name="id"]', ele).val(results);
                $('.loading').fadeOut();
                $('.alert_popup', ele).fadeIn();
                var tb = '<?php echo date('d-m-Y \L\ú\c H:i:s'); ?>: Cập nhật Tips <u>"' + data.tips + '"</u> thành công';
                $('.alert_popup p', ele).html(tb);
                $(window).scrollTop(0);
                //$('#list_ajax').html(results);
            });
        });
    })
    $('.close').click(function () {
        var parent = $(this).parent().parent();
        parent.fadeOut();
    })
    var order_id = <?php echo $order['id']; ?>;
    function get_data_order() {
        data = {};
        $('.order-input').each(function () {
            var key = $(this).attr('name');
//            if (key == 'full_name' && $(this).val() == '') {
//                //alert('Tên khách hàng không được bỏ trống');
//                $(this).focus();
//                error = 1;
//                return false;
//            }
            if (key == 'phone' && $(this).val() == '') {
                //alert('Số điện thoại khách hàng không được bỏ trống');
                $(this).focus();
                error = 1;
                return false;
            }
            if ($(this).hasClass('return_img')) {
                data.image = $(this).val();
            } else if (key == 'total' || key == 'arrears') {
                data[key] = $(this).autoNumeric('get');
            } else {
                data[key] = $(this).val();
            }
        })

        data_transaction = [];
        if (order_id == 0) {
            var i = 0;
            $('.transaction_row').each(function () {
                data_transaction[i] = {};
                $('.transaction_data', this).each(function () {
                    var key = $(this).attr('name');
                    if (key == 'money') {
                        if ($(this).val() == "" || $(this).val() == 0) {
                            data_transaction.splice(i, 1);
                            return false;
                        }
                        data_transaction[i][key] = $(this).autoNumeric('get');
                    } else {
                        data_transaction[i][key] = $(this).val();
                    }
                })
                i++;
            })
        }
    }
    var is_click = 1;
    var html_old = '';
    function loading(_this, msg = '', method = 'on', timeout = 0) {
        if (method == 'off') {
            _this.html(html_old);
            is_click = 1;
            $('.alert').html(msg);
            $('.alert').fadeIn();
            setTimeout(function () {
                $('.alert').fadeOut();
            }, 2000);
            return false;
        }
        is_click = 0;
        html_old = _this.html();
        var h = _this.outerHeight();
        var html = '<i class="fa fa-spinner fa-pulse fa-fw"></i>' + msg;
        _this.css('height', h);
        _this.html(html);
        if (timeout > 0) {
            setTimeout(function () {
                _this.html(html_old);
                is_click = 1;
            }, timeout);
    }
    }
    $('.save_order').click(function () {
        if (is_click == 1) {
            loading($(this), ' Đang lưu');
            var _this = $(this);
            $.when(get_data_order()).done(function () {
                if (error == 1) {
                    error = 0;
                    loading(_this, 'Vui lòng không được bỏ trống số điện thoại', 'off');
                    return false;
                }
                if (order_id > 0) {
                    data.id = order_id;
                }
                $.post('/<?php echo ADMIN_URL; ?>customer/save_order', {data: data, data_transaction: data_transaction}, function (results) {
                    if (order_id <= 0) {
                        window.location.href = '/<?php echo ADMIN_URL; ?>customer/process/' + results;
                    } else {
                        if (results == 0) {
                            loading(_this, 'THÔNG BÁO: Không tìm thấy đơn hàng', 'off');
                        } else {
                            loading(_this, 'THÔNG BÁO: Cập nhật đơn hàng #' + order_id + ' thành công', 'off');
                        }
                    }

                });
            });
        }
    });

    $('select[name="service_id"]').change(function () {
        $('input[name="service_name"]').val($(this).find('option:selected').text());
    })
    $('input[name="service_name"]').val($('select[name="service_id"]').find('option:selected').text());
    $("#add_profile").click(function (event) {
        //event.preventDefault();
        $.get('/<?php echo ADMIN_URL; ?>customer/profile', function (data) {
            console.log(data);
            $(data).modal().on('hidden', function () {
                $(this).remove();
            });
        }).success(function () {
            //$('input:text:visible:first').focus();
        });

    });
</script>