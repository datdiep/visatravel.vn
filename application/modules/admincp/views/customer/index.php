<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Danh sách khách hàng</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Danh sách khách hàng</li>
        </ol>
        <br>
        <a href="/admincp/customer/process" class="btn btn-success add_profile" rel_id="0"><i class="fa fa-user-plus"></i> Thêm mới</a>
        <?php if (!empty($networks)): ?>
            <select class="form-control" onchange="location = this.value;" style="width: 200px;float: left;margin:0 10px 5px 0px">
                <?php foreach ($networks as $n): ?>
                    <option <?php echo $n['id'] == $network_id ? 'selected' : ''; ?> value="/admincp/customer/<?php echo $n['id'] ?>"><?php echo $n['name']; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
        <input type="text" id="keyword" class="form-control" placeholder="Số điện thoại hoặc email khách hàng" style="width: 250px;float: left;margin-right: 10px;"/>
        <select class="form-control" id="status" style="width: 200px;float: left;margin-right: 10px;">
            <option value="">Lọc tiến trình</option>
            <?php foreach ($customer_status as $k => $v) : ?>
                <option <?php echo $status == $k ? 'selected' : ''; ?> value="<?php echo $k; ?>"><?php echo $v; ?></option>
            <?php endforeach; ?>
        </select>
        <select class="form-control" id="employees" style="width: 200px;float: left;margin-right: 10px;">
            <option value="">Lọc nhân sự</option>
            <?php foreach ($staff as $k => $v) : ?>
                <option <?php echo $staff['username'] == $v['username'] ? 'selected' : ''; ?> value="<?php echo $v['username']; ?>"><?php echo $v['fullname']; ?></option>
            <?php endforeach; ?>
        </select>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if (!empty($msg_error)): ?>
                    <div class="alert alert-success alert-dismissable">
                        <?php echo $msg_error; ?>
                    </div>
                <?php endif; ?>
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->

    </section>
</div><!-- /.row -->
<script>
    var current_pos = <?php echo $pos; ?>;
    var network_id = <?php echo $network_id; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var keyword = $('#keyword').val();
        var status = $('#status').val();
        var employees = $('#employees').val();
        $.post('/<?php echo ADMIN_URL; ?>customer/page', {network_id: network_id, pos: pos, status: status, keyword: keyword, employees: employees}, function (results) {
            //console.log(results);    
            $('#list_ajax').html(results);
        });
    }



    loadlist(current_pos);

    // tu refresh sau 5 phut
    setInterval(function () {
        loadlist(current_pos);
    }, 300000);

    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    $('#keyword').keypress(function (e) {
        if (e.keyCode == 13)
            loadlist(0);
    });

    $('#employees').change(function () {
        loadlist(0);
    });
    $('#status').change(function () {
        loadlist(0);
    });

    $('select[name="service_id"]').change(function () {
        $('input[name="service_name"]').val($(this).find('option:selected').text());
    })

    $('input[name="service_name"]').val($('select[name="service_id"]').find('option:selected').text());
    $('body').on('click', '.add_profile', function (event) {
        event.preventDefault();
        var id = $(this).attr('rel_id');
        $.post('/<?php echo ADMIN_URL; ?>customer/profile', {id: id}, function (data) {
            $(data).modal().on(function () {});
        }).success(function () {
            //$('input:text:visible:first').focus();
        });

    });
    $('body').on('hidden.bs.modal', '#profile', function () {
        $('#profile').remove();
    });

    $('body').on('click', '.change_box', function () {
        loadlist(current_pos);
    });

    $('body').on('click', '.save_customer', function () {
        var data = {
            id: $(this).attr('rel_id'),
            phone: $('input[name="phone"]').val(),
            full_name: $('input[name="full_name"]').val(),
            email: $('input[name="email"]').val(),
            note: $('textarea[name="note"]').val(),
            content: $('textarea[name="content"]').val(),
            service_name: $('select[name="service_name"]').val(),
            source: $('select[name="source"]').val(),
            facebook: $('input[name="facebook"]').val(),
            status: $('select[name="status"]').val()
        }

        var phone = $('input[name="phone"]').val();

        if (phone == '') {
            alert('Số điện thoại không được bỏ trống');
            $('input[name="phone"]').focus();
            return false;
        }
        $.post('/<?php echo ADMIN_URL; ?>customer/save_order', {data: data}, function (results) {
            //console.log(data['id']);
            if (data['id'] == '') {
                $('input[name="phone"]').val('');
                $('input[name="full_name"]').val('');
            }

            $('input[name="phone"]').focus();
            $('.alert-success').html(results);
            $('.alert-success').fadeIn();
            loadlist(current_pos);
        })
    })
    function undo(id) {
        show_dialog('Bạn có chắc muốn phục hồi đơn hàng #' + id + " không", function () {
            $.post('/admincp/customer/undo', {id: id}, function (results) {
                console.log(results);
                if (results == 1) {
                    loadlist(current_pos);
                } else {
                    alert('Phục hồi thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
                }
            });
        });
    }
    function trash(id) {
        show_dialog('Bạn có chắc muốn xóa đơn hàng #' + id + " không", function () {
            $.post('/admincp/customer/trash', {id: id}, function (results) {
                if (results == 1) {
                    loadlist(current_pos);
                } else {
                    alert('Xóa thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
                }
            });
        });
    }
    function trash_empty(id) {
        show_dialog('Bạn có chắc muốn xóa hoàn toàn đơn hàng #' + id + " không", function () {
            $.post('/admincp/customer/trash_empty', {id: id}, function (results) {
                if (results == 1) {
                    loadlist(current_pos);
                } else {
                    alert('Xóa hoàn toàn thất bại, vùi lòng liên hệ bộ phận kỹ thuật');
                }
            });
        });
    }
</script>

