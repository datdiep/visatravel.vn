<div class="content-wrapper">    
    <section class="content-header">
        <h1>SEO HOME</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>SEO HOME</li>            
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-12">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Cập nhật thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo @$msg; ?>
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">   
                        <div class="box-header with-border">
                            <h3 class="box-title">Update hot-line :  Trong mục content sau khi điền nội dung -> chọn mục Mã HTML (góc trên cùng bên trái) -> chèn mã này vào cuối nội dung html  </h3>
                            </br>
                            </br>
                            <textarea style="width: 90%;height: 35px;" disabled> <p class="show_fone_({key})"></p>  * NOTE : {key} là số thứ tự EX: show_fone_1 </textarea>
                        </div>
                        <div class="box-body">
                            <?php
                            $i = 0;
                            if (!empty($result)) {
                                foreach ($result as $key => $value) {
                                    $i++;
                                    ?>
                                    <div class="form-group cellphone" id="row<?php echo $i ?>">
                                        <div class="col-md-3">
                                            <label for="date_shipping">Di động (Click)</label>
                                            <input type="text" required class="form-control" value="<?php echo @$key; ?>" name="phone_click[]">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="date_shipping">Di động (Hiển thị)</label>
                                            <input type="text" required class="form-control" value="<?php echo @$value; ?>" name="phone_show[]">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="date_shipping">Chèn mã HTML</label>
                                            <input type="text" disabled class="form-control" value="<?php echo "<p class='show_fone_" . $i . "'></p> " ?>">
                                        </div>
                                        <div class="col-md-3">
                                            <label for="date_shipping">Hiển thị trên web</label><br>
                                            <a style="display: block;background: #fff;padding: 7px 10px;border-radius: 5px;" href="tel:<?php echo @$key; ?>"><?php echo @$value; ?></a>
                                        </div>
                                        <?php if ($user['manager_editor']): ?>
                                            <a name="remove" id="<?php echo $i ?>" class="a-remove">&nbsp;<i class="fa fa-remove"></i>&nbsp;</a>
                                        <?php endif; ?>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class="form-group cellphone">
                                    <div class="col-md-6">
                                        <label for="date_shipping">Di động (Click)</label>
                                        <input type="text" required class="form-control"  name="phone_click[]">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="date_shipping">Di động (Hiển thị)</label>
                                        <input type="text" required class="form-control" name="phone_show[]">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($user['manager_editor']): ?>
                                <div id="phone_plus"></div>
                                <div class="box-footer">
                                    <a id="addphone" name="addphone" class="btn btn-primary"> Thêm + </a>
                                    <button type="submit" name="submit" class="btn btn-danger">Cập Nhật</button>
                                </div>
                            <?php endif; ?>
                        </div>    

                    </div>
                </div>
            </div>
        </form>
    </section>

</div>
<script>
    var i = 100;
    $('#addphone').click(function () {
        i++;
        $("#phone_plus").append("<div id='row" + i + "'class='form-group cellphone'><div class='col-md-6'><label for='date_shipping'>Di động (Click)</label> <input type='text' class='form-control' required name='phone_click[]'></div><div class='col-md-6'><label for='date_shipping'>Di động (Hiển thị) </label>  <input type='text' class='form-control' required name='phone_show[]'></div><a style='float:right;padding: 5px 0;' name='remove' id ='" + i + "'class='a-remove'>&nbsp<i class='fa fa-remove'></i>&nbsp</a></div>");
    });
    $(document).on('click', '.a-remove', function () {
        var button_id = $(this).attr("id");
        $("#row" + button_id).remove();
    });
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    var editor1 = CKEDITOR.replace('textarea_qna');
    var editor3 = CKEDITOR.replace('textarea_documenthome');
    var editor4 = CKEDITOR.replace('textarea_servicehome');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>
<style>
    a[name="remove"]{
        padding: 0px 0;
        position: absolute;
        top: 0px;
        right: 0px;
        background: #ccc;
    }
    .cellphone{
        position: relative
    }
</style>