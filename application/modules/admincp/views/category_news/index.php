<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH MỤC TIN TỨC</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List category</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>category_news/add_edit/"><button style="width: 200px;" class="btn btn-block btn-primary">Thêm thể loại</button></a>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th>STT</th>
                                        <th>Title</th>                                       
                                        <th>Image</th>                                       
                                        <th>Actions</th>                                                
                                    </tr>
                                </thead>
                                <tbody>    
                                    <?php foreach ($category_news as $item): ?>
                                        <tr>
                                            <td><?php echo $item['id']; ?></td> 
                                            <td><a href="/<?php echo ADMIN_URL . 'category_news/add_edit/' . $item['id']; ?>"><?php echo $item['title']; ?></a></td>                   
                                            <td> <image width="100" src="/assets/upload/cat/<?php echo $item['image'] ?>"> </td>              
                                            <td>
                                                <a href="/<?php echo ADMIN_URL . 'category_news/add_edit/' . $item['id']; ?>">Edit</a> | 
                                                <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>      
                            </table>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Do you want to delete category news id =' + id + ' ?', function () {
            $.post('/<?php echo ADMIN_URL; ?>category_news/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }
</script>

