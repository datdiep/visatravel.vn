<div class="pull-right">
    <a href="/<?php echo ADMIN_URL; ?>task/process" class="btn btn-info" style="margin:0 15px 5px 0px"><i class="fa fa-plus"></i> Thêm công việc</a>
    <?php if (!empty($networks)): ?>
        <select class="form-control" onchange="location = this.value;" style="width: 120px;float: left;margin:0 10px 5px 0px">
            <?php foreach ($networks as $n): ?>
                <option <?php echo $n['id'] == $network['id'] ? 'selected' : ''; ?> value="/admincp/task/department/<?php echo $n['id'] ?>"><?php echo $n['name']; ?></option>
            <?php endforeach; ?>
        </select>
    <?php endif; ?>
    <?php if (!empty($departments)): ?>
        <select class="form-control" onchange="location = this.value;" style="width: 120px;float: left;margin:0 10px 5px 0px">
            <option value="0">Phòng ban</option> 
            <?php foreach ($departments as $d): ?>
                <option <?php echo $d['id'] == @$department['id'] ? 'selected' : ''; ?> value="/admincp/task/department/<?php echo $network['id'] . '/' . $d['id']; ?>"><?php echo $d['name']; ?></option>
            <?php endforeach; ?>
        </select>
    <?php endif; ?>
    <select class="form-control" onchange="location = this.value;" style="width: 120px;float: left;margin:0 10px 5px 0px">
        <option value="0">Thành viên</option> 
        <?php foreach ($list_member as $u): ?>
            <option <?php echo $u['id'] == @$member['id'] ? 'selected' : ''; ?> value="/admincp/task/department/<?php echo $network['id'] . '/' . $u['department_id'].'?u='.$u['username']; ?>"><?php echo $u['username']; ?></option>
        <?php endforeach; ?>
    </select>


    <div class="form-group" style="width: 250px;float: left;margin-right: 10px;">          
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input  type="text" value="<?php echo @$date;?>" class="form-control pull-left" id="date_install"/>
        </div>               
    </div>
</div>
<script type="text/javascript">
    $('#date_install').daterangepicker({
//        startDate: start,
//        endDate: end,
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        locale: {
            format: 'MMMM DD, YYYY'
        },
    });
    $('#date_install').change(function () {
        $.post('/<?php echo ADMIN_URL; ?>task/load_date', {d: this.value}, function (results) {
            if(results == 1)
               location.reload();
        });
    });
</script>