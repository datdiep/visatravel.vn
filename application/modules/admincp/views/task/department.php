<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> 
        <?php echo!empty($department) ? 'Cập nhật' : 'Thêm'; ?> phòng ban.
    </h4>
</div>
<div class="modal-body">
    <div class="box-body">
        <?php if (!empty($msg)): ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="icon fa fa-check"></i> Lúc: <?php echo date('H:i:s') . ' bạn đã ' . $msg; ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="form-group col-md-4">
                <label for="name">Tên phòng ban</label>
                <input type="hidden" id="department_id" value="<?php echo @$department['id']; ?>">
                <input type="text" class="form-control" name="name" value="<?php echo @$department['name']; ?>">
            </div>
            <div class="form-group col-md-8">
                <label for="slogan">Slogan</label>
                <input type="text" class="form-control format_number" name="slogan" value="<?php echo @$department['slogan']; ?>">
            </div>
            <div class="form-group col-md-12">
                <label for="skill"><i class="fa fa-graduation-cap margin-r-5"></i>Chuyên môn</label>
                <span>(Cách nhau bởi dấy phẩy)</span>
                <input type="text" class="form-control format_number" name="skill" value="<?php echo @$department['skill']; ?>">
            </div>
            <div class="form-group col-md-12">
                <label for="rule"><i class="fa fa-gavel margin-r-5"></i>Nội quy</label>
                <textarea name="rule" class="form-control"><?php echo @$department['rule']; ?></textarea>
            </div>

            <div class="form-group col-md-12">
                <label for="note"><i class="fa fa-book margin-r-5"></i>Quy định</label>
                <textarea name="note" class="form-control"><?php echo @$department['note']; ?></textarea>
            </div>
            <div class="form-group col-md-12">
                <label for="image">Hình nền</label>
                <input type="file" id="inp">
                <input type="hidden" id="b64" class="form-control" name="img">
                <?php if (!empty($department['folder'])): ?>
                    <br/>
                    <img src="/assets/upload/data/<?php echo $department['folder'] . '/banner.jpg?' . date('s'); ?>" width="100%"/>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
    <p class="btn btn-primary add_group" submit="<?php echo!empty($user['edit_department']) ? 1 : 0; ?>"><i class="fa fa-save"></i> Lưu lại</p>
    <?php if (!empty($department) && !empty($user['role_department'])): ?>
        <p class="btn btn-primary handle_department" rel_id=""><i class="fa fa-plus"></i> Thêm mới</p>
    <?php endif; ?>
</div>
<script type="text/javascript">
    var sumbit = 0;
    document.getElementById("inp").addEventListener("change", readFile);
    $('.add_group').click(function () {
        var ele = $('#popup_control');
        submit = $(this).attr('submit');
        upload(ele);
    })

</script>
<?php if (!empty($change_department)): ?>
    <ul class="todo-list ui-sortable" id="temp_department" style="display: none">
        <li class="department_<?php echo $department['id']; ?>">
            <!-- drag handle -->
            <span class="handle ui-sortable-handle">
                <i class="fa fa-ellipsis-v"></i>
                <i class="fa fa-ellipsis-v"></i>
            </span>

            <span class="text"><a onclick="location = '/admincp/task/department/<?php echo $department['network_id'] . '/' . $department['id']; ?>';" href="javascript:void(0)"><?php echo $department['name']; ?></a></span>
            <!--<small class="label label-danger"><i class="fa fa-user"></i> 2</small>-->
            <div class="tools">
                <i class="fa fa-edit handle_department" rel_id="<?php echo $department['id']; ?>"></i>
                <i class="fa fa-trash-o"  onclick="del_department(<?php echo $department['id']; ?>)"></i>
            </div>
        </li>
    </ul>
    <script type="text/javascript">
        $('#ajax_department .department_<?php echo $department['id']; ?>').remove();
        $('#ajax_department').prepend($('#temp_department').html());
    </script>
<?php endif; ?>

