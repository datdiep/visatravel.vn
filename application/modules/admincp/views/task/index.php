<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Quản lý công việc hằng ngày</h1><br/>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>

    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-9" style="padding-right: 0px;">
                <ul class="timeline">
                    <?php if (!empty($task)): ?>
                        <?php
                        $i = 0;
                        foreach ($task as $k => $v):
                            ?>
                            <?php $i++; ?>
                            <li class="time-label">
                                <?php if ($i == 1): ?>
                                    <?php echo $load_header; ?>
                                <?php endif; ?>
                                <span class="bg-<?php echo ($i % 2 == 0) ? 'red' : 'green'; ?>">
                                    <?php if (!empty($member)): ?>
                                        <img style="width:30px" src="/assets/upload/avatar/<?php echo empty($member['avatar']) ? 'default.jpg' : $member['avatar']; ?>">
                                    <?php endif; ?>
                                    <?php echo $k; ?>
                                </span>

                            </li>
                            <?php foreach ($v as $item): ?>
                                <li>

                                    <?php $avatar = $list_member[$item['user_handle_id']]['avatar']; ?>
                                    <i class="fa">
                                        <?php if (!empty($member)): ?>
                                            <?php echo date('d', strtotime($item['date_create'])); ?>
                                        <?php else: ?>
                                            <img style="width:30px" src="/assets/upload/avatar/<?php echo empty($avatar) ? 'default.jpg' : $avatar; ?>"> 
                                        <?php endif; ?>
                                    </i>
                                    <div class="timeline-item">
                                        <div class="pull-right" style="padding: 7px">
                                            <span style="background: #f4f4f4;color:#999;padding: 0 3px; margin-right: 5px"><i class="fa fa-clock-o"></i> <?php echo date('H:i', strtotime($item['date_create'])); ?></span> 
                                            <?php if ($item['mark']): ?>
                                                <a href="/<?php echo ADMIN_URL . 'task/process/' . $item['id']; ?>" class="mark btn btn-warning btn-xs">
                                                    <?php
                                                    $mark = array(
                                                        '-2' => '<i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> Quá tệ',
                                                        '-1' => '<i class="fa fa-star-o"></i> Không đạt',
                                                        '1' => '<i class="fa fa-star"></i> Đạt',
                                                        '2' => '<i class="fa fa-star"></i><i class="fa fa-star"></i> Tốt',
                                                        '3' => '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Xuất sắc',
                                                    );
                                                    echo $mark[$item['mark']];
                                                    ?>
                                                </a>
                                            <?php else: ?>
                                                <?php
                                                if ($item['status'] == 'open') {
                                                    if ($user['id'] == $item['user_handle_id'])
                                                        echo '<a href="/' . ADMIN_URL . 'task/process/' . $item['id'] . '" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> Delete</a>';
                                                    else
                                                        echo '<a href="/' . ADMIN_URL . 'task/process/' . $item['id'] . '" class="btn btn-default btn-xs">Đang chờ</a>';
                                                }
                                                if ($item['status'] == 'doing')
                                                    echo '<a href="/' . ADMIN_URL . 'task/process/' . $item['id'] . '" class="btn btn-primary btn-xs">Đang làm</a>';
                                                if ($item['status'] == 'finish' && $user['id'] == $item['user_leader'])
                                                    echo '<a href="/' . ADMIN_URL . 'task/process/' . $item['id'] . '" class="btn btn-success btn-xs"><i class="fa fa-trophy"></i> Đánh giá</a>';
                                                ?>
                                            <?php endif; ?>
                                        </div>
                                        <h3 class="timeline-header">
                                            <a href="/<?php echo ADMIN_URL . 'task/process/' . $item['id']; ?>" class="<?php echo $item['user_handle'] != $item['user_create'] ? 'c-red' : ''; ?>"><?php echo $item['user_handle']; ?></a>
                                        </h3>

                                        <div class="timeline-body">
                                            <div class="row">
                                                <div class="col-md-<?php echo!empty($item['log']) ? '6' : '10'; ?>">
                                                    <?php echo $item['content']; ?>
                                                </div>
                                                <?php if (!empty($item['log'])): ?>
                                                    <div class="col-md-6" style="border-left: 1px dashed #ccc;">
                                                        <label style="background: #f4f4f4;padding: 2px 5px"><i class="fa fa-comments-o"></i> Lời nhắn gần nhất:</label><br>
                                                        <?php
                                                        $item['log'] = unserialize($item['log']);
                                                        $item['log'] = $item['log'][count($item['log']) - 1];
                                                        //pre($item['log'],false);
                                                        echo $item['log']['name'] . ': ' . $item['log']['content'];
                                                        ?>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>

                        <?php endforeach; ?>
                    <?php else: ?>
                        <li class="time-label">                              
                            <?php echo $load_header; ?>
                            <span class="bg-green">
                                <?php echo date('d M,Y'); ?>
                            </span>

                        </li>
                        <li>
                            <i class="fa"><img style="width:30px" src="/assets/upload/avatar/system.jpg"> </i>
                            <div class="timeline-item">

                                <h3 class="timeline-header">
                                    <a href="/" class="c-red">Sailormoon</a>
                                </h3>

                                <div class="timeline-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            Không tìm thấy công việc hằng ngày vui lòng yêu cầu nhân viên thêm công việc hàng ngày để giúp doanh nghiệp quản lý và phát triển chuyên nghiệp hơn.
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
                    <li>
                        <i class="fa fa-clock-o bg-gray"></i>

                    </li>

                </ul>


            </div><!-- /.box -->
            <div class="col-md-3">
                <?php if (!empty($department)): ?>
                    <?php echo $load_chat; ?>

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title"><i class="fa fa-trophy"></i> Mục tiêu trong tháng</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-danger btn-xs handle_planing" rel_id=""><i class="fa fa-plus"></i> Thêm</button>
                            </div>
                        </div>
                        <div class="box-body" id="ajax_planing">
                            <?php if (!empty($planings)): ?>
                                <?php foreach ($planings as $p): ?>
                                    <div class="item_planing planing_<?php echo $p['id']; ?>">
                                        <strong class="handle_planing" rel_id="<?php echo $p['id']; ?>">
                                            <span class="bg-note pull-right"><?php echo $p['user_create']; ?></span>
                                            <i class="fa fa-<?php echo $p['status'] == 'finish' ? 'thumbs-o-up' : 'file-text-o'; ?> margin-r-5"></i> 
                                            <?php echo $p['status'] == 'close' ? '<strike>' . $p['name'] . '</strike>' : $p['name']; ?>
                                        </strong>
                                        <p><?php echo $p['description']; ?></p>

                                    </div>
                                <?php endforeach; ?>
                                <script type="text/javascript">
                                    $('#ajax_planing').slimScroll({
                                        height: '220px'
                                    });
                                </script>
                            <?php else: ?>
                                <strong><i class="fa fa-file-text-o margin-r-5"></i> Không có kế hoạch</strong>
                                <p>Vui lòng lập kế hoạch, để giúp bạn thực hiện công việc hiệu quả hơn.</p>
                            <?php endif; ?>
                        </div>
                        <?php if (!empty($planings)): ?>
                            <div class="text-center box-footer">
                                <a href="javascript:void(0)" class="uppercase">Xem Tất Cả</a>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="box box-widget widget-user">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header bg-black" style="background: url('/assets/upload/data/<?php echo $department['folder'] . '/banner.jpg'; ?>') center center;">
                            <h3 class="widget-user-username <?php echo!empty($user['edit_department']) ? 'handle_department' : ''; ?>" rel_id="<?php echo $department['id']; ?>"><?php echo $department['name']; ?></h3>
                            <h5 class="widget-user-desc"><?php echo $department['slogan']; ?></h5>
                        </div>
                        <div class="widget-user-image">
                            <img class="img-circle" src="/assets/upload/avatar/<?php echo empty($user['avatar']) ? 'default.jpg' : $user['avatar']; ?>" alt="User Avatar">
                        </div>
                        <div class="box-footer">
                            <div class="row">
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">3,200</h5>
                                        <span class="description-text">Năng nổ</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4 border-right">
                                    <div class="description-block">
                                        <h5 class="description-header">13,000</h5>
                                        <span class="description-text">Công việc</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                                <div class="col-sm-4">
                                    <div class="description-block">
                                        <h5 class="description-header">35</h5>
                                        <span class="description-text">Tín nhiệm</span>
                                    </div>
                                    <!-- /.description-block -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <hr>
                            <strong><i class="fa fa-gavel margin-r-5"></i> Nội quy</strong>

                            <p class="text-muted">
                                <?php echo nl2br($department['rule']); ?>
                            </p>

                            <hr>

                            <strong><i class="fa fa-book margin-r-5"></i> Quy định</strong>

                            <p class="text-muted"><?php echo nl2br($department['note']); ?></p>

                            <hr>

                            <strong><i class="fa fa-graduation-cap margin-r-5"></i> Chuyên môn</strong>
                            <?php
                            $skill = explode(',', $department['skill']);
                            $color = array('danger', 'success', 'info', 'warning', 'primary');
                            //pre($skill);
                            echo '<p>';
                            $i = 0;
                            foreach ($skill as $k => $v) {
                                echo '<span class="label label-' . $color[$i] . '">' . $v . '</span> ';
                                $i = ($i == 4) ? 0 : $i + 1;
                            }
                            echo '</p>';
                            ?>
                        </div>
                    </div>
                <?php elseif (!empty($this->data['user']['role_department'])): ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <i class="fa fa-tags"></i>

                            <h3 class="box-title">Phòng ban</h3>

                            <!--              <div class="box-tools pull-right">
                                            <ul class="pagination pagination-sm inline">
                                              <li><a href="#">«</a></li>
                                              <li><a href="#">1</a></li>
                                              <li><a href="#">2</a></li>
                                              <li><a href="#">3</a></li>
                                              <li><a href="#">»</a></li>
                                            </ul>
                                          </div>-->
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <ul class="todo-list ui-sortable" id="ajax_department">
                                <?php if (!empty($departments)): ?>
                                    <?php foreach ($departments as $v): ?>
                                        <li class="department_<?php echo $v['id']; ?>">
                                            <!-- drag handle -->
                                            <span class="handle ui-sortable-handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>

                                            <span class="text"><a onclick="location = '/admincp/task/department/<?php echo $network['id'] . '/' . $v['id']; ?>';" href="javascript:void(0)"><?php echo $v['name']; ?></a></span>
                                            <!--<small class="label label-danger"><i class="fa fa-user"></i> 2</small>-->
                                            <div class="tools">
                                                <i class="fa fa-edit handle_department" rel_id="<?php echo $v['id']; ?>"></i>
                                                <i class="fa fa-trash-o" onclick="del_department(<?php echo $v['id']; ?>)"></i>
                                            </div>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <!-- /.box-body -->
                        <div class="box-footer clearfix no-border">
                            <button type="button" rel_id="" class="handle_department btn btn-default pull-right"><i class="fa fa-plus"></i> Thêm</button>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (!empty($list_member)): ?>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thành viên</h3>


                        </div>
                        <!-- /.box-header -->

                        <div class="box-body no-padding">
                            <ul class="users-list clearfix">
                                <?php foreach ($list_member as $v): ?>

                                    <li>
                                        <img src="/assets/upload/avatar/<?php echo empty($v['avatar']) ? 'default.jpg' : $v['avatar']; ?>" alt="User Image">
                                        <a class="users-list-name" href="#"><?php echo $v['username']; ?></a>
                                        <span class="users-list-date"><?php echo $v['level_department_name']; ?></span>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <!-- /.users-list -->
                        </div>

                    </div>

                <?php endif; ?>
            </div>
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="popup_control">

        </div>
    </div>
</div>
<script type="text/javascript">
    var network_id = <?php echo $network['id']; ?>;
    var department_id = <?php echo!empty($department['id']) ? $department['id'] : 0; ?>;
    $('body').on('click', '.handle_department', function () {
        var id = $(this).attr('rel_id');
        $.post('/<?php echo ADMIN_URL; ?>task/handle_department', {id: id, network_id: network_id}, function (results) {
            $('#popup_control').html(results);
        });
        $("#myModal").modal("show");
    })
    $('body').on('click', '.handle_planing', function () {
        if (department_id > 0) {
            var id = $(this).attr('rel_id');
            $.post('/<?php echo ADMIN_URL; ?>task/handle_planing', {id: id, network_id: network_id, department_id: department_id}, function (results) {
                console.log(results);
                $('#popup_control').html(results);
            });
            $("#myModal").modal("show");
        }
    })
    $('body').on('focus', ".datetimepicker", function () {
        $(this).datetimepicker({format: 'hh:mm A D MMMM, YYYY', minDate: moment()});
    })

    var data = {};
    function upload(ele) {
        $.when(get_data(ele)).done(function () {
            if (data.name == '') {
                alert('Tên không được bỏ trống');
                return false;
            }
            var id = $('#department_id').val();
            $.post('/<?php echo ADMIN_URL; ?>task/handle_department', {id: id, network_id: network_id, data: data}, function (results) {
                if (submit == 1)
                    location.reload();
                else
                    $('#popup_control').html(results);
            });
        });
    }

    function get_data(ele) {
        data = {};
        $('.form-control', ele).each(function () {
            var key = $(this).attr('name');
            if (key != '' && typeof key != 'undefined') {
                if (key == 'content') {
                    var source = $(this).attr('id');
                    data.content = CKEDITOR.instances[source].getData();
                } else {
                    data[key] = $(this).val();
                }
            }
        })
    }
    function del_department(id) {
        show_dialog('Bạn có chắc muốn xóa phòng ban với mã số: #' + id + " không?", function () {
            $.post('/<?php echo ADMIN_URL; ?>task/del_department', {id: id}, function (results) {
                if ($.isNumeric(results)) {
                    $('#ajax_department .department_' + results).remove();
                } else {
                    alert(results);
                }
            });
        });
    }
</script>
<style type="text/css">
    .widget-user-username.handle_department{
        cursor: pointer;
    }
    .widget-user-username.handle_department:hover{
        text-decoration: underline;
    }
    .item_planing{
        border-bottom: 1px dashed #e4e4e4;
        margin-bottom: 10px;
    }
    .item_planing:last-child{
        border-bottom: none;
        margin-bottom: 0px;
    }
    .handle_planing{
        cursor: pointer;
    }
    .item_planing .handle_planing:hover{
        text-decoration: underline
    }
    .widget-user .widget-user-header {
        padding: 10px;
    }

</style>