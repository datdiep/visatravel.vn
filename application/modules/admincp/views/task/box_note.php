<div class="panel box" id="note_0" rel_id="0">
    <div class="box-header with-border">
        <h4 class="box-title">
            <a><input type="text" name="title" rel_id="0" placeholder="Tiêu đề"/></a>
        </h4>
    </div>
    <div id="collapse_0">
        <div class="box-body">
            <ul class="todo-list ui-sortable">
                <li>
                    <input type="checkbox" value="">
                    <span class="text" placeholder="Nội dung..." contenteditable="true"></span>
                    <i class="fa fa-trash-o remove_checknote"></i>
                </li>
            </ul>
        </div>
        <div class="box-footer">
            <span class="loading"><i class="fa fa-spinner fa-pulse fa-fw margin-bottom"></i> Đã lưu...</span>
            <div class="btn-group pull-right">
                <!--                            <button type=button" class="btn btn-primary margin-r-5">Xong</button>-->
                <button type=button" onclick="change_note(0, 'check')"class="btn btn-primary margin-r-5"><i class="fa fa-check-square-o"></i></button>
                <button type=button" onclick="change_note(0, 'text')"class="btn btn-primary margin-r-5"><i class="fa fa-edit"></i></button>
                <button type=button" onclick="save(0)" class="btn btn-success"><i class="fa fa-save"></i> Lưu</button>
            </div>
        </div>
    </div>
</div>