<div class="content-wrapper">    
    <section class="content-header">
        <h1><?php echo!empty($task) ? $map_status[$task['status']] . 'Công việc #' . $task['id'] : 'Thêm công việc'; ?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>task">List công việc</a></li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12"> 
                    <?php if ($check_error == 1): ?>

                        <div class="alert alert-error alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            <?php echo validation_errors() . @$msg; ?>
                        </div>
                    <?php elseif ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>

                    <?php endif; ?>
                </div>
                <div class="col-md-8">
                    <?php if (!empty($task)): ?>
                        <div class="box box-widget">
                            <div class="box-header with-border">
                                <div class="user-block">
                                    <img class="img-circle" src="/assets/upload/avatar/<?php echo!empty($user_handle[0]['avatar']) ? $user_handle[0]['avatar'] : 'default.jpg'; ?>" alt="User Image">
                                    <span class="username"><a href="#"><?php echo $user_handle[0]['username']; ?></a></span>
                                    <span class="description">
                                        Nhóm: <?php echo!empty($user_handle[0]['department_name']) ? $user_handle[0]['department_name'] : 'Không có'; ?>&nbsp;&nbsp;|&nbsp;&nbsp;Danh hiệu: <?php echo!empty($user_handle[0]['level_department_name']) ? $user_handle[0]['level_department_name'] : 'Không có'; ?>
                                    </span>                                                    
                                </div>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <label class="bg-green disabled" style="padding: 2px 5px"><i class="fa fa-edit"></i> Nội dung công việc:</label><br>
                                <blockquote><?php echo $task['content']; ?></blockquote>

                            </div>
                            <div class="box-footer box-comments">
                                <div class="box-comment">
                                    <label class="bg-red" style="padding: 2px 5px"><i class="fa fa-code"></i> Chi tiết thực hiện:</label><br>
                                    <blockquote>
                                        <?php
                                        if (!empty($task['action'])) {
                                            echo detect_url($task['action']);
                                        } else {
                                            echo '<p style="color:#777">Vui lòng thực hiện công việc đầy đủ công việc rùi chọn "Hoàn thành", để leader có thể đánh giá. Sẽ giúp bạn có nhũng bước định hướng phát triển tốt nhất trong sự nghiệp</p>';
                                        }
                                        ?>
                                    </blockquote>
                                </div>
                            </div>
                            <div class="box-footer">
                                <span class="pull-right" <?php echo $task['status'] == 'finish' ? '' : 'style="display:none"'; ?>">
                                    <button type="button" value="-2" class="mark btn <?php echo $task['mark'] == -2 ? 'btn-warning' : 'btn-default'; ?> btn-xs"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> Quá tệ</button>
                                    <button type="button" value="-1" class="mark btn <?php echo $task['mark'] == -1 ? 'btn-warning' : 'btn-default'; ?> btn-xs"><i class="fa fa-star-o"></i> Không đạt</button>
                                    <button type="button" value="1"  class="mark btn <?php echo $task['mark'] == 1 ? 'btn-warning' : 'btn-default'; ?> btn-xs"><i class="fa fa-star"></i> Đạt</button>
                                    <button type="button" value="2"  class="mark btn <?php echo $task['mark'] == 2 ? 'btn-warning' : 'btn-default'; ?> btn-xs"><i class="fa fa-star"></i><i class="fa fa-star"></i> Tốt</button>
                                    <button type="button" value="3"  class="mark btn <?php echo $task['mark'] == 3 ? 'btn-warning' : 'btn-default'; ?> btn-xs"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> Xuất sắc</button>
                                </span>
                            </div>
                            <!-- /.box-footer -->
                        </div>
                    <?php endif; ?>
                    <?php if (@$task['status'] != 'finish'): ?>

                        <div class="box box-primary box-primary <?php echo!empty($task) ? 'collapsed-box' : ''; ?>">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-edit"></i> Mô tả ngắn gọn công việc</h3>
                                <div class="box-tools">
                                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-<?php echo!empty($task) ? 'plus' : 'minus'; ?>"></i></button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="form-group">
                                    <textarea id="content" name="content"><?php echo @$task['content']; ?></textarea>
                                </div> 
                            </div> 

                        </div>    
                        <div class="box box-primary box-primary <?php echo (empty($task) || !empty($task['action'])) ? 'collapsed-box' : ''; ?>">
                            <div class="box-header with-border">
                                <h3 class="box-title"><i class="fa fa-code"></i> Cập nhật chi tiết thực hiện</h3>
                                <div class="box-tools">
                                    <button type="button" class="btn btn-info btn-sm" data-widget="collapse"><i class="fa fa-<?php echo empty($task) ? 'plus' : 'minus'; ?>"></i></button>
                                </div>
                            </div>
                            <div class="box-body"> 
                                <div class="form-group">
                                    <textarea id="action" name="action"><?php echo @$task['action']; ?></textarea>
                                </div> 
                            </div> 

                        </div>
                    <?php endif; ?>
                </div>
                <div class="col-md-4">                
                    <div class="box box-primary box-success">


                        <div class="box-body">

                            <div  class="form-group"> 
                                <label for="date_create"><?php echo!empty($task['date_create']) ? 'Tạo lúc' : 'Hôm nay là'; ?></label>
                                <input type="text" name="date_create" disabled="" class="form-control" value="<?php echo!empty($task['date_create']) ? date('h:i A d/m/Y', strtotime($task['date_create'])) : date('h:i A d/m/Y'); ?>"/>
                            </div>
                            <div  class="form-group"> 
                                <label>Loại công việc</label><br>
                                <?php if (empty($task)): ?>
                                    <select class="form-control" name="type" id="type">
                                        <option <?php echo @$task['date_create'] == 'daily' ? 'selected' : ''; ?> value="daily">Hằng ngày</option>
                                        <?php if (!empty($leader_user)): ?>
                                            <option <?php echo @$task['date_create'] == 'assign' ? 'selected' : ''; ?> value="assign">Giao cho</option>
                                        <?php endif; ?>
                                    </select>
                                    <ul class="list_item">
                                        <?php if (!empty($leader_user)): ?>
                                            <?php foreach ($leader_user as $k => $user): ?>
                                                <li rel="<?php echo $user['id']; ?>">
                                                    <img class="thumbnail" src="/assets/upload/avatar/<?php echo $user['avatar']; ?>">
                                                    <p style="float:left">
                                                        <span class="title"><?php echo $user['username']; ?></span><br>
                                                        <span style="color:red">Nhóm: <?php echo!empty($user['department_name']) ? $user['department_name'] : 'Không có'; ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span style="color:#777">Danh hiệu: <?php echo!empty($user['level_department_name']) ? $user['level_department_name'] : 'Không có'; ?></span>
                                                    </p>
                                                    <p class="del_product_id" <?php echo $k == 0 ? 'style="background:red"' : ''; ?> data_id="<?php echo $user['id']; ?>" data_name="<?php echo $user['username']; ?>">Chọn</p>
                                                </li>
                                                <?php if ($k == 0): ?>

                                                    <input type="hidden" name="user_handle_id" disabled value="<?php echo $user['id']; ?>"/>
                                                    <input type="hidden" name="user_handle" disabled value="<?php echo $user['username']; ?>"/>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                    <div class="form-group" id="deadline">
                                        <label for="deadline">Deadline</label>
                                        <div class="input-group field-date">
                                            <input type='text' class="form-control" id='datetimepicker' name="deadline"/>
                                            <label class="input-group-addon btn" for="datetimepicker">
                                                <span class="fa fa-calendar"></span>
                                            </label>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <ul class="todo-list ui-sortable">
                                        <li>
                                            <span class="handle ui-sortable-handle">
                                                <i class="fa fa-ellipsis-v"></i>
                                                <i class="fa fa-ellipsis-v"></i>
                                            </span>
                                            <span class="text"><?php echo $task['type'] == 'daily' ? 'Hằng ngày' : 'Được giao bởi: <b>' . $task['user_create'] . '</b>'; ?></span>
                                        </li>
                                    </ul>
                                    <?php if ($task['type'] == 'assign'): ?>
                                        <br><label>Cần hoàn thành trước</label><br>
                                        <ul class="todo-list ui-sortable" id="mind_question">
                                            <li>
                                                <span class="handle ui-sortable-handle">
                                                    <i class="fa fa-ellipsis-v"></i>
                                                    <i class="fa fa-ellipsis-v"></i>
                                                </span>
                                                <span class="text text-danger"><?php echo date('H:i A d/m/Y', strtotime($task['deadline'])); ?></span>
                                            </li>
                                        </ul>
                                    <?php endif; ?>
                                <?php endif; ?>

                            </div>


                        </div> 
                    </div>    
                    <?php if (!empty($task)): ?>

                        <?php echo $load_chat; ?>

                    <?php endif; ?>
                    <?php if (!empty($department)): ?>
                        <div class="box box-primary box-danger">
                            <div class="box-footer">
                                <strong><i class="fa fa-gavel margin-r-5"></i> Nội quy</strong>
                                <p class="text-muted"><?php echo nl2br($department['rule']); ?></p>
                                <hr>
                                <strong><i class="fa fa-book margin-r-5"></i> Quy định</strong>
                                <p class="text-muted"><?php echo nl2br($department['note']); ?> </p>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
            <?php if (@$task['status'] != 'finish'): ?>
                <div class="box-footer">
                    <button type="submit" name="submit" value="2" class="btn btn-default margin-r-5"><i class="fa fa-outdent"></i> Lưu & thoát</button>
                    <button type="submit" name="submit" value="1" class="btn btn-success margin-r-5"><i class="fa fa-save"></i> Lưu lại</button>
                    <?php if (!empty($task)): ?>
                        <button type="submit" name="submit" value="3" class="btn btn-danger"><i class="fa fa-check-circle"></i> Hoàn thành</button>
                    <?php endif; ?>
                </div>
                <script type="text/javascript">
                    CKEDITOR.config.entities_latin = false;
                    CKEDITOR.config.toolbarGroups = [
                        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
                        {name: 'colors'},
                        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
                        '/',
                        {name: 'styles'},
                        {name: 'tools'},
                        {name: 'mode'},
                    ];

                    var editor1 = CKEDITOR.replace('content');
                    var editor2 = CKEDITOR.replace('action');
                    CKFinder.setupCKEditor(editor1, '<?php echo base_url(); ?>../assets/admin/ckfinder/');
                </script>
            <?php endif; ?>
        </form>
    </section>

</div>
<script>


    $('.list_item').css('display', 'none');
    $('#deadline').css('display', 'none');
    $('#datetimepicker').datetimepicker({format: 'hh:mm A D MMMM, YYYY', minDate: moment()});
    $('#type').change(function () {
        var type = $(this).val();
        if (type == 'assign') {
            $('.list_item').css('display', 'block');
            $('#deadline').css('display', 'block');
            $('input[name="user_handle_id"]').prop('disabled', false);
            $('input[name="user_handle"]').prop('disabled', false);
        } else {
            $('.list_item').css('display', 'none');
            $('#deadline').css('display', 'none');
            $('input[name="user_handle_id"]').prop('disabled', true);
            $('input[name="user_handle"]').prop('disabled', true);
        }
    });

    $(".list_item").on('click', '.del_product_id', function () {
        $('.del_product_id').css('background', '#f4f4f4');
        this.style.backgroundColor = 'red';
        var id = $(this).attr('data_id');
        var name = $(this).attr('data_name');
        $('input[name="user_handle_id"]').val(id);
        $('input[name="user_handle"]').val(name);
    });
<?php if (!empty($task) && $task['status'] == 'finish'): ?>
        $('.mark').click(function () {
            var mark = $(this).val();
            var id = <?php echo $task['id']; ?>
            //console.log(mark);
            show_dialog('Bạn có hãy chắc trước khi chọn đồng ý, vì sẽ không thể chỉnh sữa được', function () {
                $.post('/<?php echo ADMIN_URL; ?>task/mark', {mark: mark, id: id}, function (results) {
                    console.log(results);
                    if (results == 1) {
                        $('.mark[value="' + mark + '"]').removeClass('btn-default');
                        $('.mark[value="' + mark + '"]').addClass('btn-warning');
                    } else {
                        alert('Không thành công, vi bạn không có quyền hoặc công việc này đã được đánh giá.');
                    }
                });
            });
        })
<?php endif; ?>
</script>
<style>
    .list_item li{
        background: #dfeaf2;
    }
</style>