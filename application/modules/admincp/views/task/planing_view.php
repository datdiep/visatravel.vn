<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="user-block">
        <img class="img-circle img-bordered-sm" src="/assets/upload/avatar/<?php echo empty($user_create['avatar']) ? 'default.jpg' : $user_create['avatar']; ?>" alt="User Image">
            <span class="username">
              <a href="#"><?php echo $user_create['username'];?></a>
              
            </span>
        <span class="description">Leader: <?php echo $planing['user_leader'];?></span>
      </div>
</div>
<div class="modal-body">
    <div class="box-body">
        <?php if(!empty($msg)):?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-check"></i> Lúc: <?php echo date('H:i:s').' bạn đã '.$msg;?>
          </div>
        <?php endif;?>
        <strong><i class="fa fa-file-text-o margin-r-5"></i> <?php echo $planing['name'];?></strong><br>
        <?php echo $planing['description'];?>
        <br><br>
        <?php echo $planing['content'];?>
        <br><br>
        <span style="background: #f4f4f4;color:#999;padding: 0 3px; margin-right: 5px">
            <i class="fa fa-clock-o"></i> 
            Bắt đầu: <span style="color:#222"><?php echo date('d/m/Y', strtotime($planing['date_start']));?></span>
            Kết thúc: <span style="color:#222"><?php echo date('d/m/Y', strtotime($planing['date_end']));?></span>
        </span>
        <span style="background: #f4f4f4;color:#999;padding: 0 3px; margin-right: 5px">
            <i class="fa fa-user"></i> 
            Leader: <span style="color:#222"><?php echo $planing['user_leader'];?></span>
        </span>
        <?php if(!empty($planing['appraise'])):?>
            <br><br>
            <strong class="appraise"><i class="fa fa-trophy margin-r-5"></i> Leader đanh giá</strong><br>
            <?php echo $planing['appraise'];?>
        <?php endif;?>
        <?php if($planing['status'] != 'open' && $planing['user_leader'] == $user['username']):?>
            <br><br>
            <div class="form-group" id="appraise" style="<?php echo !empty($planing['appraise']) ? 'display:none':''?>">
                <label for="description"><i class="fa fa-edit margin-r-5"></i>Nhập đánh giá</label>
                <textarea id="v_appraise" class="form-control"><?php echo @$planing['appraise'];?></textarea>
            </div>
        <?php endif;?>
    </div>
</div>

<div class="modal-footer">
    <input type="hidden" id="planing_id" value="<?php echo $planing['id'];?>">
    <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
    <?php if($user['id'] == $user['leader'] && $planing['status'] == 'open' && $planing['user_create'] == $user['username']):?>
        <p class="btn btn-default edit_planing" submit="10">Chỉnh sữa</p>
    <?php endif;?>
    <?php if($planing['approve'] == 2 && $planing['status'] == 'open' && $planing['user_create'] == $user['username']):?>
        <p class="btn btn-default update_planing"  submit="1"><i class="fa fa-times"></i> Hủy kế hoạch</p>
        <p class="btn btn-warning update_planing"  submit="2"><i class="fa fa-trello"></i> Hoàn thành</p>
    <?php endif;?>
    <?php if($planing['user_leader'] == $user['username']):?>
        <?php if($planing['approve'] == 1):?>
            <p class="btn btn-warning update_planing" submit="3"><i class="fa fa-trello"></i> Duyệt</p>
        <?php endif;?>
        <?php if($planing['status'] != 'open'):?>
            <p class="btn btn-primary update_planing" submit="4"><i class="fa fa-save"></i> Lưu lại</p>
        <?php endif;?>
    <?php endif;?>
</div>
<script type="text/javascript">
    $(".appraise").click(function(){
        $("#appraise").toggle();
    });
    $('.update_planing').click(function(){
        var submit = $(this).attr('submit');
        var id = $('#planing_id').val();
        var appraise = $('#v_appraise').val();
        $.post('/<?php echo ADMIN_URL; ?>task/update_planing', {id:id,submit:submit,appraise: appraise}, function (results) {
            $('#popup_control').html(results);
        });
    }) 
    $('.edit_planing').click(function(){
        var submit = $(this).attr('submit');
        var id = $('#planing_id').val();
        $.post('/<?php echo ADMIN_URL; ?>task/handle_planing', {id:id,submit:submit}, function (results) {
            $('#popup_control').html(results);
        });
    })   
</script>
<style type="text/css">
    #appraise{
        cursor: pointer
    }
    .appraise{
        cursor: pointer
    }
    .appraise:hover{
        text-decoration: underline;
    }
</style>
<?php if(!empty($change_planing)):?>
    <div id="temp_planing" style="display: none">
        <div class="item_planing planing_<?php echo $planing['id'];?>">
            <strong class="handle_planing" rel_id="<?php echo $planing['id'];?>"><i class="fa fa-file-text-o margin-r-5"></i><?php echo $planing['name'];?></strong>
            <p><?php echo $planing['description'];?></p>
        </div>
    </div>
    <script type="text/javascript">
        $('#ajax_planing .planing_<?php echo $planing['id'];?>').remove();
        $('#ajax_planing').prepend($('#temp_planing').html());
    </script>
<?php endif;?>