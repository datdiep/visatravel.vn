<div class="content-wrapper">    
    <section class="content-header">
        <h1>Cập nhật & chỉnh sữa công việc</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>task">List công việc</a></li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12"> 
                    <?php if ($check_error == 1): ?>

                        <div class="alert alert-error alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            <?php echo validation_errors() . @$msg; ?>
                        </div>
                    <?php elseif ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>

                    <?php endif; ?>
                </div>
                <div class="col-md-6">                
                    <div class="box box-primary box-success">


                        <div class="box-body">
                            <div class="row">
                                <div  class="col-md-4"> 
                                    <label for="timeadd">Ngày làm</label>
                                    <input type="text" name="timeadd" disabled="" value="<?php echo date('d/m/Y \L\ú\c H:i:s',strtotime($task['timeadd'])); ?>" class="form-control" />
                                </div>
                                <div  class="col-md-8"> 
                                    <label>Trạng thái</label><br>
                                    <select class="form-control" name="type">
                                        <option value="Open" <?php echo $task['type'] == 'Open' ? 'selected' : ''; ?>>Open</option>
                                        <option value="Progress" <?php echo $task['type'] == 'Progress' ? 'selected' : ''; ?>>Progress</option>
                                        <option value="Completed" <?php echo $task['type'] == 'Completed' ? 'selected' : ''; ?>>Completed</option>
                                        <option value="Closed" <?php echo $task['type'] == 'Closed' ? 'selected' : ''; ?>>Closed</option>
                                        <option value="Cancel" <?php echo $task['type'] == 'Cancel' ? 'selected' : ''; ?>>Cancel</option>
                                    </select>
                                </div>
                            </div>
                        </div> 
                        <div class="box-body">
                            <label>Todo List</label>
                            <blockquote>
                                <?php echo $task['propose']; ?>
                            </blockquote>
                            <?php if ($user['role'] != 'admin'): ?>
                                <!--<textarea style="width: 400px;height: 100px;" id="propose" name="propose"><?php echo $task['propose']; ?></textarea>-->
                            <?php endif; ?>                          
                        </div>
                    </div>                          
                </div>
                <div  class="col-md-6">
                    <div style="padding: 15px;" class="box box-primary box-success" ">

                        <h4 class="box-title">Đánh giá của leader</h4>
                        <textarea style="min-height: 150px;" name="appraise" class="form-control" <?php echo $user['lv'] < $task['lv'] ? '' : 'disabled'; ?>><?php echo $task['appraise']; ?></textarea>
                        <br>
                        <?php if ($user['lv'] < $task['lv']): ?>
                            <button type="submit" name="submit" class="btn btn-primary">Cập nhật</button>
                        <?php endif; ?> 
                    </div>

                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">

                        <h4 class="box-title">THỰC HIỆN CÔNG VIỆC </h4>

                        <?php if ($user['username'] != $task['username']): ?>
                            <blockquote>
                                <?php

                                function processString($s) {
                                    return preg_replace('/https?:\/\/[\w\-\%.!~#?&=;+\*\'"(),\/]+/', '<a href="$0" target="blank" rel="nofollow">$0</a>', $s);
                                }
                                ?>
                                <?php echo processString($task['action']); ?>
                            </blockquote>
                        <?php else : ?>


                            <textarea name="action"><?php echo $task['action']; ?></textarea>

                        <?php endif; ?>
                            <?php if ($workload_link != ''): ?>
                            <div class="box-footer">
                            <a href="<?php echo $workload_link;?>" target="_bank" style="float:left;margin-left: -10px;" class="btn btn-default">Workload Link</a>
                            </div>
                            <?php endif; ?>
                    </div>
                    <?php if ($user['lv'] < $task['lv'] || $user['username'] == $task['username']): ?>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Cập nhật</button>
                        </div>
                    <?php endif; ?> 

                </div>
            </div>

        </form>
    </section>

</div>
<script>
   
    CKEDITOR.config.entities_latin = false;
    $('form').submit(function (e) {
        $('textarea[name="action"]').val(CKEDITOR.instances.action.document.getBody().getHtml().replace(/&nbsp;/g, ' '));
    });
    //var editor1 = CKEDITOR.replace('propose');
    var editor2 = CKEDITOR.replace('action');
    //CKFinder.setupCKEditor(editor1, '<?php echo base_url(); ?>../assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '<?php echo base_url(); ?>../assets/admin/ckfinder/');
</script>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>