<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">                  
                    <th>STT</th>
                    <th width="10%">Thời gian</th>
                    <th>User</th>
                    <th width="25%">Todo List</th>
                    <th>Trạng thái</th>
                    <th>Leader đánh giá</th>             
                    <th align="center">Thực thi</th>                                                
                </tr>
            </thead>
            <tbody>
                <?php if (!empty($results)): ?>
            <?php $i = 0; ?>
            <?php foreach ($results as $item): ?>
                <tr class="gradeC">                                        
                    <td><?php echo $item['id']; ?></td>                        
                    <td><a href="/<?php echo ADMIN_URL . "task/edit/{$item['id']}"; ?>"><?php echo ago(strtotime($item['timeadd']), 0) . ' trước'; ?></a></td>
                    <td><?php echo $item['username']; ?></td>
                    <td><?php echo $item['propose']; ?></td>
                    <td><div class="<?php echo $item['type']; ?>"><?php echo $item['type']; ?></div></td> 
                   
                    <td><?php echo $item['appraise']; ?></td>
                    <td>
                        <a href="/<?php echo ADMIN_URL . 'task/edit/' . $item['id']; ?>">Edit</a> 
                        <?php if ($user == $item['username'] && $item['type'] != 'Close' && $item['type'] != 'Completed' && $item['type'] != 'Cancel'): ?>
                            <br> <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        <?php endif; ?>
               
            </tbody>      
        </table>
    </div>
</div>
<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
<style>
    .Progress, .Completed, .Open, .Cancel, .Contineus, .Closed, .adddays {
    display: inline-block;
    padding: 3px 5px;
    -webkit-border-radius: 2px;
    -moz-border-radius: 2px;
    border-radius: 2px;
    font-weight: bold;
}
    .Completed {
        background: #06485d !important;
        color: #fff;
    }
    .Closed {
    background: #7f7f7f !important;
    color: #fff;
}
    .Progress {
    background: #75923c !important;
    color: #fff;
}
.Open {
    background: #ddd9c3 !important;
    color: #000;
}
.Cancel {
    background: #f12292 !important;
    color: #fff;
}
</style>