<div class="content-wrapper">
    <section class="content">

        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Bảng giá visa</li>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header ">
                        <h3 class="box-title"><i class="fa fa-cc-visa"></i> Bảng giá visa cho người Việt Nam đi nước ngoài </h3>
                    </div>
                    <div class="box-body no-padding">
                        <table class="table table-striped table-hover visa_price">
                            <tbody><tr style="background-color: #e8e8e8;">
                                    <th style="width: 10px">Icon</th>
                                    <th>QUỐC GIA</th>
                                    <th>GIÁ DỊCH VỤ</th>
                                    <th>PHÍ LÃNH SỰ</th>
                                    <th>HIỆU LỰC VISA</th>
                                    <th>THỜI GIAN LƯU TRÚ</th>
                                    <th>NỘP LẠI</th>
                                    <th>THỜI GIAN CÓ VISA</th>
                                    <th>SỔ TIẾT KIỆM</th>
                                </tr>
                                <tr class="gradeC" >            
                                    <td><img src="/assets/admin/images/flags/us.png"></td>
                                    <td><span data-toggle="tooltip" data-original-title="3 New Messages">Visa Mỹ</span></td>
                                    <td>180 - 200$</td>
                                    <td>160$</td>
                                    <td>1 Năm</td>
                                    <td>6 tháng (Multiple)</td>
                                    <td>Ko giới hạn</a>
                                    <td>Có ngay</td>
                                    <td>Tối thiểu 200 tr</td>

                                </tr>

                                <tr class="gradeC" >         
                                    <td><img src="/assets/admin/images/flags/us.png"></td>
                                    <td>Gia hạn Visa Mỹ</td>
                                    <td>100$</td>
                                    <td>160$</td>
                                    <td>1 Năm</td>
                                    <td>6 tháng (Multiple)</td>
                                    <td>Ko giới hạn</a>
                                    <td>2 tuần</td>
                                    <td></td>

                                </tr>

                                <tr class="gradeC">         
                                    <td><img src="/assets/admin/images/flags/au.png" align="top">
                                    <td> Visa Úc</td>
                                    <td>180 - 200$</td>
                                    <td>2 tr 872k</td>
                                    <td>1 Năm</td>
                                    <td>3 tháng (Single/Multiple)</td>
                                    <td>Ko giới hạn</a>
                                    <td>2 tuần</td>
                                    <td>Tối thiểu 220 tr</td>

                                </tr>

                                <tr class="gradeC" >  
                                    <td><img src="/assets/admin/images/flags/ca.png" align="top"></td>
                                    <td> Visa Canada</td>
                                    <td>230 - 250$</td>
                                    <td>145$</td>
                                    <td>10 Năm</td>
                                    <td>6 tháng (Multiple)</td>
                                    <td>Ko giới hạn</a>
                                    <td>2 tuần</td>
                                    <td>Tối thiểu 220 tr</td>

                                </tr>

                                <tr class="gradeC">     
                                    <td></td>
                                    <td>Visa Schengen (Pháp)</td>
                                    <td>240 - 250$ (Đã gồm BH Du lịch)</td>
                                    <td>2tr2</td>
                                    <td>6 tháng</td>
                                    <td>3 tháng (Multiple)</td>
                                    <td>Ko giới hạn</a>
                                    <td>2 tuần</td>
                                    <td>Tối thiểu 220 tr</td>

                                </tr>

                                <tr class="gradeC">   
                                    <td><img src="/assets/admin/images/flags/jp.png" align="top"> </td>
                                    <td>Visa Nhật Bản</td>
                                    <td>150$ (Nộp thường) | 250 - 400$ (bao đậu)</td>
                                    <td>750k</td>
                                    <td>3 tháng</td>
                                    <td>15 ngày (Single)</td>
                                    <td>6 tháng</a>
                                    <td>1 tuần</td>
                                    <td>Tối thiểu 100 tr</td>

                                </tr>

                                <tr class="gradeC">    
                                    <td><img src="/assets/admin/images/flags/kr.png" align="top"></td>
                                    <td> Visa Hàn Quốc</td>
                                    <td>160 - 180$</td>
                                    <td>20$</td>
                                    <td>3 tháng</td>
                                    <td>1 tháng (Single)</td>
                                    <td>3 tháng</a>
                                    <td>1 tuần</td>
                                    <td>Tối thiểu 100 tr</td>

                                </tr>

                                <tr class="gradeC">    
                                    <td></td>
                                    <td>Visa Đài Loan</td>
                                    <td>150$ - 30tr (Visa 10 năm / Hộ khẩu HCM giảm 5tr)</td>
                                    <td>50$</td>
                                    <td>3 tháng</td>
                                    <td>1 tháng (Single)</td>
                                    <td>3 tháng</a>
                                    <td>1 tuần</td>
                                    <td>Tối thiểu 60 tr</td>

                                </tr>

                                <tr class="gradeC" > 
                                    <td></td>
                                    <td>Visa HongKong</td>
                                    <td>130$</td>
                                    <td>55$</td>
                                    <td>1 tháng</td>
                                    <td>7 - 14 ngày (Single)</td>
                                    <td>3 tháng</a>
                                    <td>2-3 tuần trước ngày bay</td>
                                    <td>Tối thiểu 60 tr</td>

                                </tr>

                                <tr class="gradeC" >    
                                    <td></td>
                                    <td>Visa Dubai</td>
                                    <td>250$</td>
                                    <td>90$</td>
                                    <td>3 tháng</td>
                                    <td>14 ngày (Single)</td>
                                    <td>3 tháng</a>
                                    <td>3 - 5 Ngày</td>
                                    <td>Tối thiểu 100 tr</td>

                                </tr>

                            </tbody></table>
                    </div>
                </div>


                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">
                            <i class="fa fa-money"></i> Bảng giá chứng minh tài chính (Mượn sổ gốc + 200k - Đối với sổ lùi ngày + 1tr)
                        </h3>
                    </div>

                    <div class="box-body no-padding">
                        <table class="table table-striped table-hover visa_price">
                            <tbody>
                                <tr style="background-color: #e8e8e8;">
                                    <th>SỔ TIẾT KIỆM</th>
                                    <th>KHÔNG LÙI</th>
                                    <th>LÙI NGÀY</th>
                                    <th>HỖ TRỢ QUỸ PHÚC LỢI CTY</th>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 100 tr</td>
                                    <td>800.000</td>
                                    <td>3.500.000</td>
                                    <td>100.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 200 tr</td>
                                    <td>1.200.000</td>
                                    <td>3.500.000</td>
                                    <td>100.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 300 tr</td>
                                    <td>1.500.000</td>
                                    <td>3.500.000</td>
                                    <td>150.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 400 tr</td>
                                    <td>2.000.000</td>
                                    <td>7.000.000</td>
                                    <td>150.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 500 tr</td>
                                    <td>2.400.000</td>
                                    <td>7.000.000</td>
                                    <td>200.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 600 tr</td>
                                    <td>2.800.000</td>
                                    <td>7.000.000</td>
                                    <td>200.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sổ tiết kiệm 700 tr</td>
                                    <td>3.200.000</td>
                                    <td>7.000.000</td>
                                    <td>250.000</td>
                                </tr>
                            </tbody></table>
                    </div>
                </div>


                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">
                            <i class="fa fa-black-tie"></i> Bảng giá chứng minh công việc
                        </h3>
                    </div>

                    <div class="box-body no-padding">
                        <table class="table table-striped table-hover visa_price">
                            <tbody>
                                <tr style="background-color: #e8e8e8;">
                                    <th>LOẠI DỊCH VỤ</th>
                                    <th>GIÁ THU KHÁCH</th>
                                    <th>BAO GỒM</th>
                                    <th>HỖ TRỢ QUỸ PHÚC LỢI CTY</th>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Công ty 1-2 Năm</td>
                                    <td>1tr5 - 3tr</td>
                                    <td>HĐLD + ĐXNP + Xác nhận lương tiền mặt</td>
                                    <td>200.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Công ty 3-5 Năm</td>
                                    <td>3tr5 - 5tr</td>
                                    <td>HĐLD + ĐXNP + Xác nhận lương tiền mặt</td>
                                    <td>300.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Công ty > 10 Năm</td>
                                    <td>8tr</td>
                                    <td>HĐLD + ĐXNP + Xác nhận lương tiền mặt</td>
                                    <td>500.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Hợp đồng góp vốn cty > 4 năm</td>
                                    <td>8tr - 12tr</td>
                                    <td>HĐ góp vốn + Bảng phân chia lợi nhuận</td>
                                    <td>500.000</td>
                                </tr>
                                <tr class="gradeC" >            
                                    <td>Sao kê lương ngân hàng 3 tháng</td>
                                    <td>6tr</td>
                                    <td>Sao kê lương ngân hàng</td>
                                    <td>200.000</td>
                                </tr>
                            </tbody></table>
                    </div>


                </div>
                <div class="box col-md-12">
                    <br>
                    <p>
                        <b>*Lưu ý:</b> Quỹ Phúc lợi cty khi thu tiền khách, sẽ được trích ngay vào quỹ này. Mục đích phúc lợi và cho nhân viên đi du lịch nước ngoài , thăm ốm đau, sinh nhật.
                    </p>
                    <p>
                        Trong trường hợp cuối năm sử dụng ko hết, có thể biểu quyết ý kiến chia tiền mặt cho nhân viên.
                    </p>
                </div>
            </div>

        </div>
    </section>
</div>
