<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel"><i class="fa fa-plus-square"></i> 
        <?php echo !empty($planing) ? 'Cập nhật' : 'Thêm' ;?> kế hoạch.
    </h4>
</div>
<div class="modal-body">
    <div class="box-body">
        <?php if(!empty($msg)):?>
          <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-check"></i> Lúc: <?php echo date('H:i:s').' bạn đã '.$msg;?>
          </div>
        <?php endif;?>
        <div class="row">
            <div class="form-group col-md-12">
                <label for="name">Tên kế hoạch</label>
                <input type="hidden" id="planing_id" value="<?php echo @$planing['id'];?>">
                <input type="text" class="form-control" name="name" value="<?php echo @$planing['name'];?>">
            </div>
<!--            <div class="form-group col-md-12">
                <label for="user_handle"><i class="fa fa-user margin-r-5"></i>Thành viên tham gia</label>
                <span>(Để trống khi chọn tất cả thành viên trong nhóm)</span>
                <input type="text" class="form-control" name="user_handle" value="<?php echo @$planing['user_handle'];?>">
            </div>-->

                <div class="form-group col-md-6">
                    <label for="date_start"><i class="fa fa-clock-o margin-r-5"></i>Ngày bắt đầu</label>
                    <div class="input-group field-date">
                        <input type='text' class="form-control datetimepicker" id='date_start' value="<?php echo !empty($planing['date_start']) ? date('H:i A d/m/Y', strtotime($planing['date_start'])) : ''; ?>" name="date_start"/>
                        <label class="input-group-addon btn" for="date_start">
                            <span class="fa fa-calendar"></span>
                        </label>
                    </div>

                </div>
                <div class="form-group col-md-6">
                    <label for="date_end"><i class="fa fa-clock-o margin-r-5"></i>Ngày kết thúc</label>
                    <div class="input-group field-date">
                        <input type='text' class="form-control datetimepicker" id='date_end' value="<?php echo !empty($planing['date_start']) ? date('H:i A d/m/Y', strtotime($planing['date_start'])):''; ?>" name="date_end"/>
                        <label class="input-group-addon btn" for="date_end">
                            <span class="fa fa-calendar"></span>
                        </label>
                    </div>
                </div>
     

            <div class="form-group col-md-12">
                <label for="description"><i class="fa fa-file-text-o margin-r-5"></i>Mô tả ngắn</label>
                <textarea name="description" class="form-control"><?php echo @$planing['description'];?></textarea>
            </div>
            
            <div class="form-group col-md-12">
                <label for="content"><i class="fa fa-edit margin-r-5"></i>Chi tiết</label>
                <textarea name="content" id="content" class="form-control"><?php echo @$planing['content'];?></textarea>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <p class="btn btn-default" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
    <p class="btn btn-primary add_planing" submit="0"><i class="fa fa-save"></i> Lưu lại</p>
    <?php if(@$planing['approve'] == 0 && $user['id'] != $user['leader']):?>
        <p class="btn btn-warning add_planing" submit="1"><i class="fa fa-arrow-circle-right"></i> Đề xuất</p>
    <?php endif;?>
    <?php if(!empty($planing['approve'])):?>
        <p class="btn btn-primary handle_planing" rel_id=""><i class="fa fa-plus"></i> Thêm mới</p>
    <?php endif;?>
</div>
<script type="text/javascript">
    CKEDITOR.config.entities_latin = false;
    CKEDITOR.config.toolbarGroups = [
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'colors'},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align']},
        '/',
        {name: 'styles'},
        {name: 'tools'},
        {name: 'mode'},
    ];
    var editor1 = CKEDITOR.replace('content');
    var submit = 0;
    $('.add_planing').click(function(){
        var ele = $('#popup_control');
        submit = $(this).attr('submit');
        upload_planing(ele);
    })
    function upload_planing(ele){
        $.when(get_data(ele)).done(function () {
            if (data.name == '') {
                alert('Tên không được bỏ trống');
                return false;
            }
            var id = $('#planing_id').val();
            $.post('/<?php echo ADMIN_URL; ?>task/handle_planing', {id:id,submit:submit,data: data,network_id:network_id,department_id:department_id}, function (results) {
                $('#popup_control').html(results);
            });
        });
    }
    
</script>
<?php if(!empty($change_planing)):?>
    <div id="temp_planing" style="display: none">
        <div class="item_planing planing_<?php echo $planing['id'];?>">
            <strong class="handle_planing" rel_id="<?php echo $planing['id'];?>"><i class="fa fa-file-text-o margin-r-5"></i><?php echo $planing['name'];?></strong>
            <p><?php echo $planing['description'];?></p>
        </div>
    </div>
    <script type="text/javascript">
        $('#ajax_planing .planing_<?php echo $planing['id'];?>').remove();
        $('#ajax_planing').prepend($('#temp_planing').html());
    </script>
<?php endif;?>

