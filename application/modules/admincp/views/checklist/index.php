<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>CHECK LIST</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Check list gửi khách</li>
        </ol>
        <br/>
        <a href="/<?php echo ADMIN_URL; ?>checklist/add"><button style="float:left;margin-right: 10px;width: 200px;" class="btn btn-block btn-primary">Thêm check list</button></a>
        <input id="keyword" class="form-control" placeholder="Nhập tiêu đề cần tìm" value="" style="width: 200px;float: left;margin-right: 10px;"/>
        <select class="form-control" id="type" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Thể loại -- </option>
            <?php foreach ($type as $value) : ?>
                <option <?php echo @$type_cookies == $value ? 'selected' : ''; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
            <?php endforeach; ?>
        </select>
    </section>

    <!-- Main content -->
    <br><br>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Bạn có muốn xóa trang id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>checklist/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }

    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var keyword = $('#keyword').val();
        var type = $('#type').val();
        $.post('/<?php echo ADMIN_URL; ?>checklist/load', {pos: pos, type: type, keyword: keyword}, function (results) {
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });


    $('#type').change(function () {
        loadlist(0);
    });
    $('#keyword').keypress(function (e) {
        if (e.keyCode == 13)
            loadlist(0);
    });

</script>