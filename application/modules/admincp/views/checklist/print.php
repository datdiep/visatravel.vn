<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Bản In <?php echo $page['title']; ?></title>
        <style>
            body {
                background: rgb(204,204,204); 
                font-size: 16px;
                font-family: Arial;
            }
            page {
                background: white;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
                box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                padding: 5mm;
            }
            page[size="A4"] {  
                width: 21cm;
            }
            page[size="A4"][layout="portrait"] {
                width: 29.7cm;
            }
            page[size="A3"] {
                width: 29.7cm;
            }
            page[size="A3"][layout="portrait"] {
                width: 42cm;
            }
            page[size="A5"] {
                width: 14.8cm;
            }
            page[size="A5"][layout="portrait"] {
                width: 21cm; 
            }
            @media print {
                body, page {
                    margin: 0;
                    box-shadow: 0;
                }
            }
            .letterhead{margin: 0 auto; text-align: center; padding-bottom: 5mm}
            .letterhead img{position: absolute; top: 0; left: 0; height: 56px }
            .letterhead .bg{
                background: url('/assets/user/images/letterhead.png') ;
                height: 88px;
                width: 665px;
                margin: 0 auto;
                position: relative
            }
            .title{
                color:#ff0000; 
                font-weight: bold; 
                font-size: 2em;
                text-align: center
            }
            .c-red{color:#ff0000}
            .c-green{color:green}

        </style>
    </head>
    <body>
        <page size="A4">
            <div class="letterhead">
                <div class="bg">
                    <?php
                    if ($network['logo']) {
                        echo ' <img src="/assets/upload/network/' . $network['logo'] . '" border="0">';
                    }
                    ?>

                </div>
                <img src="" borer="0">
            </div>
            <div class="title">
                <?php echo $page['title']; ?>
            </div>
            <div class="content">
                <?php echo $page['content']; ?>  
            </div>
            <div class="contact">
                <p><b><?php echo $network['name']; ?></b></p>
                <p><b class="c-green">Người liên hệ: <?php echo $user['fullname']; ?></b></p>
                <p><b class="c-red">Di Động: <?php echo $user['phone']; ?></b></p>
                <p>Điện thoại : <?php echo $network['phone']; ?></p>
                <p>Email: <?php echo $user['email']; ?></p>
                <p>Website: <?php echo $network['website']; ?></p>
                <p>Địa chỉ : <?php echo $network['address']; ?></p>

            </div>
        </page>
    </body>

</html>