<?php $this->load->library('transload'); ?>
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Tên hồ sơ</th>      
                    <th>Thuộc loại</th>
                    <th>Tạo lúc</th>
                    <th>Cập nhật lúc</th>
                    <th>Hồ sơ phụ</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td> 
                        <td><a href="/<?php echo ADMIN_URL . 'checklist/edit/' . $item['id']; ?>"><?php echo $item['title']; ?><?php echo empty($item['short_code']) ? '':'('.$item['short_code'].')'; ?></a></td>                                                 
                        <td><?php echo $item['type'] ?></td>
                        <td><?php echo $item['author'] ?>: <?php echo $this->transload->ago(strtotime($item['publish_date'])); ?></td>
                        <td>
                           <?php echo !empty($item['author_update']) ? $item['author_update'].': '.$this->transload->ago(strtotime($item['last_update'])) : '';?>
                        </td>
                        <td align="center"><?php echo !empty($item['sub_checklist']) ? '<span title="'. strip_tags($item['sub_checklist']).'"> ✔ </span>' : '';?></td>
                        <td>
                            <a href="/<?php echo ADMIN_URL . 'checklist/edit/' . $item['id']; ?>">Edit</a> | 
                            <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      
        </table>
    </div>
</div>

<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
