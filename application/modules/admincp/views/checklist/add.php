<div class="content-wrapper">    
    <section class="content-header">
        <h1>ADD CHECK LIST</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>checklist">Check list</a></li>
            <li class="active">Add page</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo validation_errors(); ?>                          
                </div>
            <?php endif; ?>
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu lại</button>
                            <button type="submit" value="2" name="submit" class="btn btn-success"><i class="fa fa-edit"></i> Lưu & tiếp tục</button>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-12">   
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin </h3>
                        </div>
                        <div class="box-body">
                            <div class="col-md-6 nopadding"> 
                                <div class="form-group col-md-6">
                                    <label for="title">Tên hồ sơ</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="alias">Loại hồ sơ</label>
                                    <select name="type" class="form-control valid">
                                        <?php foreach ($type as $value): ?>
                                            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-12">
                                    <label for="group_service">Thuộc nhóm dịch vụ</label>
                                     <div class="row">
                                        <?php foreach ($map_services as $k => $value): ?>
                                        <div class="col-md-4">
                                            <div class="checkbox" style="margin:0px;">
                                                <label><input name="group_service[]" type="checkbox" value="<?php echo $k; ?>"><?php echo $value; ?></label>
                                            </div>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>


                                

                               
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fee_net">Ghi chú hồ sơ</label>
                                    <textarea class="form-control" name="note" style="height: 150px"></textarea>
                                </div>
                            </div>
                        </div>

                    </div> 
                </div>

                <div  class="col-md-12">   
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Hồ sơ phụ (Nếu có)</h3>
                        </div>
                        <div class="box-body">
                            <textarea id="textarea_content" name="sub_checklist" style="width: 100%; height: 200px"></textarea>
                        </div>
                        <div class="box-footer">
                            <button type="submit" value="1" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu lại</button>
                            <button type="submit" value="2" name="submit" class="btn btn-success"><i class="fa fa-edit"></i> Lưu & tiếp tục</button>
                        </div>
                    </div>

                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>