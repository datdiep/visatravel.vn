<div class="content-wrapper">    
    <section class="content-header">
        <h1>Chào <?php echo $user['username'];?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Cài đặt tài khoản</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Cập nhật thông tin thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo @$msg; ?>
                    <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <div class="row">            
                <div class="col-md-6">   

                    <div class="box box-primary box-success">                       
                        <div class="box-body">
                            <div class="row"> 
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="username">Tên đăng nhập</label>
                                        <input type="text" disabled class="form-control" name="username" value="<?php echo $user['username']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="fullname">Họ và tên</label>
                                        <input type="text" class="form-control" name="fullname" value="<?php echo $user['fullname']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Số điện thoại</label>
                                        <input type="text" class="form-control" name="phone" value="<?php echo $user['phone']; ?>">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control" name="email" value="<?php echo $user['email']; ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>
                        </div>      


                    </div>    

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title">Thay đổi mật khẩu</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="password">Mật khẩu mới</label>
                                <input type="password"  class="form-control" name="password">
                            </div>    

                            <div class="form-group">
                                <label for="repassword">Nhập lại mật khẩu</label>
                                <input type="password"  class="form-control" name="repassword">
                            </div>


                        </div>
                        <div class="box-footer">
                            <button type="submit" value="1" name="submit" class="btn btn-default">Lưu mật khẩu</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-success">Cập nhật thông tin</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

</div>
<script>

    crop('photo', 250, 250<?php echo $user['avatar'] ? ',"/assets/upload/avatar/' . $user['avatar'] . '"' : ''; ?>);
    var id = <?php echo $user['network_id']; ?>;
    function load_access(id) {
        $.post('/<?php echo ADMIN_URL; ?>admin/load_access', {id: id}, function (results) {
            $('#list_ajax').html(results);
            auto_uncheck();
        });
    }
    load_access(id);
<?php if (!empty($user['role_network'])): ?>
        $('#type').change(function () {
            var id = $(this).val();
            var type = $("#type :selected").attr('rel');
            $('input[name="network_id"]').val(id);
            $('input[name="network_type"]').val(type);
            load_access(id);
        });
<?php endif; ?>
    map_roles = "<?php echo ',' . $user['role_controller'] . ',' . $user['role_order_progress'] . ','; ?>";
    console.log(map_roles);
    function auto_uncheck() {
        var roles = map_roles;
        $('#list_ajax input[type="checkbox"]').prop('checked', false);
        $('#list_ajax input[type="checkbox"]').parent().css('color', 'black');
        $('#list_ajax input[type="checkbox"]').each(function () {
            var text = ',' + $(this).val() + ',';
            if (roles.indexOf(text) != -1) {
                $(this).prop('checked', true);
                $(this).parent().css('color', 'red');
            }
        })
    }
</script>
<style>
    .roles{
        padding: 2px 5px;
    }
</style>