<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> Danh sách nhân sự</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Admin</li>
        </ol>
        <br/>
        <a href="/<?php echo ADMIN_URL; ?>admin/add"><button style="float:left; width: 200px;" class="btn btn-block btn-primary">Thêm quản trị</button></a>
	<?php if(!empty($user['role_network'])):?>
	<select class="form-control" onchange="location = this.value;" style="width: 200px;float: left;margin:0px 10px 5px 10px">
	    <?php foreach ($networks as $n): ?>
		<option <?php echo $n['id'] == $network_id ? 'selected' : ''; ?> value="/admincp/admin/<?php echo $n['id'] ?>"><?php echo $n['name']; ?></option>
	    <?php endforeach; ?>
	</select>
	<?php endif;?>

    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">

                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th>Mã NV</th>
                                        <th>Username</th>
                                        <th>Họ và tên</th>
                                        <th>Phòng ban</th>
                                        <th>Chức vụ</th>
                                        <th>Trạng thái</th>
                                        <th>Điện thoại cá nhân</th>
                                        <th>Email</th>
                                        <th>Network</th>
                                        <th>Thực thi</th>
                                    </tr>
                                </thead>
                                <tbody>    
                                    <?php foreach ($results as $item): ?>
                                        <tr>
                                            <td><?php echo $item['id']; ?></td> 
                                            <td><a href="/<?php echo ADMIN_URL . 'admin/edit/' . $item['id']; ?>"><?php echo $item['username']; ?></a></td>
                                            <td><?php echo $item['fullname']; ?></td>
                                            <td><?php echo $item['department_id']; ?></td>
                                            <td><?php echo $item['job']; ?></td>
                                            <td><?php echo $item['status']; ?></td>
                                            <td><?php echo $item['phone']; ?></td>
                                            <td><?php echo $item['email']; ?></td>
                                            <td><?php echo $item['network_id']; ?></td>
                                            <td>
                                                <a href="/<?php echo ADMIN_URL . 'admin/edit/' . $item['id']; ?>">Edit</a>  
                                                <?php if($user['id'] != $item['id']):?>
                                                | <a href="javascript:void(0)" onclick="del(<?php echo $item['id'] ?>)">Delete</a> 
                                                <?php endif;?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>      
                            </table>   
                            </table>
                        </div>
                    </div>                  
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Bạn có muốn xóa tài khoản id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>admin/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }
</script>

</script>
