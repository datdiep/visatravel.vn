<div class="content-wrapper">    
    <section class="content-header">
        <h1>Thêm tài khoản</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>admin">Danh sách tài khoản</a></li>            
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm tài khoản thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo @$msg; ?>
                    <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <div class="row">            
                <div class="col-md-6">   

                    <div class="box box-primary box-success">      
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin cơ bản </h3>

                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="fullname">Họ và tên</label>
                                <input type="text" class="form-control" name="fullname" value="<?php echo $admin['fullname']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="username">Tên đăng nhập</label>
                                <input type="text" disabled class="form-control" name="username" value="<?php echo $admin['username']; ?>">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="password">Password</label>
                                <input type="password"  class="form-control" name="password">
                            </div>    

                            <div class="form-group col-md-6">
                                <label for="repassword">Re-password</label>
                                <input type="password"  class="form-control" name="repassword">
                            </div>

                            <div class="form-group col-md-6 nopadding">
                                <label for="phone">Số điện thoại cá nhân</label>
                                <input type="text" class="form-control" name="phone" value="<?php echo $admin['phone']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="work_phone">Số điện thoại làm việc</label>
                                <input type="text" class="form-control" name="work_phone" value="<?php echo $admin['work_phone']; ?>">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" value="<?php echo $admin['email']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="job">Vị trí</label>
                                <input type="text" class="form-control" name="job" value="<?php echo $admin['job']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <div class="row">
                                    <label for="dob">Sinh nhật</label>
                                    <div class="input-group field-date">
                                        <input type='text' class="form-control" id='datetimepicker' name="dob" value="<?php echo $admin['dob']; ?>"/>
                                        <label class="input-group-addon btn" for="datetimepicker">
                                            <span class="fa fa-calendar"></span>
                                        </label>
                                    </div>

                                </div>
                            </div>
                            <div class="form-group col-md-6">

                                <label for="date_joincompany">Ngày vào công ty</label>
                                <div class="input-group field-date">
                                    <input type='text' class="form-control" id='datetimepicker2' name="date_joincompany" value="<?php echo $admin['date_joincompany']; ?>"/>
                                    <label class="input-group-addon btn" for="datetimepicker2">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group col-md-6 nopadding">
                                <label for="tax_code">Mã số thuế</label>
                                <input type="text" class="form-control" name="tax_code" value="<?php echo $admin['tax_code']; ?>">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="bank_account">Tài khoản ngân hàng (ACB)</label>
                                <input type="text" class="form-control" name="bank_account" value="<?php echo $admin['bank_account']; ?>">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="home">Nơi ở hiện tại</label>
                                <input type="text" class="form-control" name="home" value="<?php echo $admin['home']; ?>">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="status">Trạng thái</label>
                                <select name="status" class="form-control valid">
                                    <?php foreach ($status as $value): ?>
                                        <option <?php if (@$admin['status'] == $value) echo 'selected'; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Ảnh đại diện</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>
                        </div>      


                    </div>    

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title">Thiệt lập tài khoản</h3>
                        </div>

                        <div class="box-body">
                            <?php if (!empty($user['role_network'])): ?>
                                <div class="form-group">
                                    <label for="alias">Thuộc đối tác</label>
                                    <select name="type" class="form-control valid" id="type">
                                        <?php foreach ($networks as $v): ?>
                                            <option <?php echo $v['id'] == $admin['network_id'] ? 'selected style="color:red"' : "" ?> rel="<?php echo $v['type']; ?>" value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <input type="hidden" class="form-control" name="network_id" value="<?php echo $admin['network_id']; ?>">
                                    <input type="hidden" class="form-control" name="network_type" value="<?php echo $admin['network_type']; ?>">
                                </div>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <label for="department">Thuộc nhóm</label>
                                    <select name="department_id" class="form-control valid" id="department">
                                        <option>-- Chọn Nhóm -- </option>
                                        <?php foreach ($department as $v): ?>
                                            <option <?php echo $v['id'] == $admin['department_id'] ? 'selected style="color:red"' : "" ?> value="<?php echo $v['id']; ?>" rel="<?php echo $v['network_id']; ?>"><?php echo $v['name']; ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="level">Cấp bậc danh hiệu</label>
                                    <select name="level_department" class="form-control valid" id="level">
                                        <?php foreach ($level_department as $v): ?>
                                            <option <?php echo $v['id'] == $admin['level_department'] ? 'selected style="color:red"' : "" ?> value="<?php echo $v['id']; ?>"><?php echo $v['name'] . ' (' . $v['star'] . ' Sao)'; ?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>
                            <div id="list_ajax"></div>



                        </div>
                    </div>

                    <div class="box box-primary box-success">    
                        <div class="box-header with-border">
                            <h3 class="box-title">Thêm người giám sát </h3>
                            <span style="color:#777"> (Là người hỗ trợ và đánh giá công việc cho thành viên này)</span>
                            <span style="color:#777"> (Nếu không thêm người tạo sẽ tự động nhận là leader của thành viên này)</span>
                        </div>
                        <div class="box-body add_product" style="position: relative">
                            <input type="hidden" name="leader" id="leader" value="<?php echo $admin['leader'];?>">
                            <input class="form-control search_input" placeholder="Tìm nhân viên" name="q" type="text" />
                            <ul class="list_item search-results"></ul>
                            <ul class="list_item results">
                                <?php if (!empty($leader)): ?>
                                    <li rel="<?php echo $leader['id']; ?>">
                                        <img class="thumbnail" src="/assets/upload/avatar/<?php echo $leader['avatar']; ?>">
                                        <p style="float:left">
                                            <span class="title"><?php echo $leader['username']; ?></span><br>
                                            <span style="color:red">Nhóm: <?php echo!empty($leader['department_name']) ? $leader['department_name'] : 'Không có'; ?></span>&nbsp;&nbsp;|&nbsp;&nbsp;<span style="color:#777">Danh hiệu: <?php echo!empty($leader['level_department_name']) ? $leader['level_department_name'] : 'Không có'; ?></span>
                                        </p>
                                        <p class="del_product_id">Xóa</p>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>                            

                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-success">Cập nhật</button>
                    </div>
                </div>
            </div>
        </form>
    </section>

</div>

<script>
    crop('photo', 250, 250<?php echo $admin['avatar'] ? ',"/assets/upload/avatar/' . $admin['avatar'] . '?time=' . time() . '"' : ''; ?>);
    var id = <?php echo $admin['network_id']; ?>;
    function load_access() {
        $.post('/<?php echo ADMIN_URL; ?>admin/load_access', {id: id}, function (results) {
            $('#list_ajax').html(results);
            auto_uncheck();
        });
    }
    load_access();
    auto_ungroup();
<?php if (!empty($user['role_network'])): ?>
        $('#type').change(function () {
            id = $(this).val();
            var type = $("#type :selected").attr('rel');
            $('input[name="network_id"]').val(id);
            $('input[name="network_type"]').val(type);
            load_access();
            auto_ungroup();
        });
<?php endif; ?>
    function auto_ungroup() {
        $('#department option').css('display', 'block');
        $('#department option').each(function () {
            var n_id = $(this).attr('rel');
            if (typeof n_id != 'undefined' && n_id != id) {
                $(this).css('display', 'none');
                $(this).prop('selected', false);
            }
        })
    }
    map_roles = "<?php echo ',' . $admin['role_controller'] . ',' . $admin['role_order_progress'] . ','; ?>";
    function auto_uncheck() {
        var roles = map_roles;
        $('#list_ajax input[type="checkbox"]').prop('checked', false);
        $('#list_ajax input[type="checkbox"]').parent().css('color', 'black');
        $('#list_ajax input[type="checkbox"]').each(function () {
            var text = ',' + $(this).val() + ',';
            if (roles.indexOf(text) != -1) {
                $(this).prop('checked', true);
                $(this).parent().css('color', 'red');
            }
        })
    }

    $(".search_input").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/admincp/admin/search",
                data: {q: request.term, m: 'complete', n: id},
                success: function (result) {
                    console.log(result);
                    result = JSON.parse(result);
                    console.log(result);
                    if (typeof result !== 'undefined' && result.length > 0)
                    {
                        end_key = (result.length > 6) ? 6 : result.length;
                        var inner_html = '';
                        for (key = 0; key < end_key; key++) {
//                            var price_compare = (result[key].price_compare == 0) ? "" : result[key].price_compare;
                            inner_html += '<li rel=' + result[key].id + '>\n\
                        <img class="thumbnail" src="/assets/upload/avatar/' + result[key].avatar + '">\n\
                        <p style="float:left"><span class="title">' + result[key].username + '</span><br>\n\
\n\                     <span style="color:red">Nhóm: ' + result[key].department_name + '</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span style="color:#777">Danh hiệu: ' + result[key].level_department_name + '</span></p>\n\
                        <p class="del_product_id">Xóa</p></li>';
                        }
                        $('.search-results').html(inner_html).fadeIn();
                    } else {
                        $('.search-results').fadeOut();
                    }

                }
            });
        },
        minLength: 0
    })
    $(".search_input").click(function () {
        $(".search_input").autocomplete("search", "");
    });
    $(".search_input").focusout(function () {
        $('.search-results').fadeOut();
    });
    $("div.add_product").on('click', '.search-results li', function () {
        var id = $(this).attr('rel');
        $('#leader').val(id);
        $('.results').html($(this));
        $(".search_input").val('');
    });
    $("div.add_product").on('click', '.del_product_id', function () {
        $('#leader').val(0);
        $(this).parent().remove();
    });


    $('#datetimepicker').datepicker({autoclose: true});
    $('#datetimepicker2').datepicker({autoclose: true});

</script>
<style>
    .roles{
        padding: 2px 5px;
    }
</style>