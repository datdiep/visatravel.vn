<div class="form-group">
    <label for="roles" class="roles bg-yellow"><i class="fa fa-cog"></i> Quyền hệ thống</label>
    <div  class="col-md-12" style="background: #fff;margin-bottom: 15px">
        <div class="row">
            <?php foreach ($role_cog as $k => $v): ?>
                <div  class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <input checked type="checkbox" name="role_controller[]" value="<?php echo $v['controller']; ?>"/>
                            <?php echo $v['name']; ?>
                        </label>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </div>

</div>
<div class="form-group">
    <label class="roles bg-red"><i class="fa fa-codepen"></i> Quyền điều khiển</label>
    <div  class="col-md-12" style="background: #fff;">
        <div class="row">
            <?php foreach ($role_codepen as $v): ?>
                <div  class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="role_controller[]" value="<?php echo $v['controller']; ?>"/>
                            <?php echo $v['name']; ?>
                        </label>
                    </div>

                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>