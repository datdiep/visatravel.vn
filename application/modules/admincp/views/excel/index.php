
<div class="content-wrapper">    
    <section class="content-header">
        <h1>import</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">import</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
           <div class="row">            
                <div class="col-md-6">
                    <?php if($message):?>
      
                        <div class="callout callout-warning">
                            <p><?php echo $message;?></p>
                        </div>

                        <?php endif;?>
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Chọn file excel</h3>
                        </div>
                        
                        <div class="box-body">
                            <input type="file" id="file" name="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="float: left; margin: 5px 10px 0 0"> 
                            <button type="submit" name="submit" class="btn btn-danger">Import</button>
                        </div><!-- /.box-body -->
                        <!-- Loading (remove the following to stop the loading)-->
                        <div class="overlay" style="display: none">
                            <i class="fa fa-refresh fa-spin"></i>
                        </div>
                        <!-- end loading -->
                    </div>            
                </div>
           </div>
        </form>
    </section>

</div>