<div class="content-wrapper">    
    <section class="content-header">
        <h1>TRA CỨU THÔNG TIN DOANH NGHIỆP</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>gov">Tra cứu thông tin doanh nghiệp</a></li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <?php if ($error): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Alert!</h4>
                        Thêm thành công
                        <a href="<?php echo $customer['url']; ?>">DOWNLOAD VISA ĐÀI LOAN CỦA KHÁCH HÀNG </a>
                    </div>
                <?php endif; ?>
                <?php if ($this->data['check_error'] == 1): ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            Vui lòng kiểm tra lại mã số thuế và Captcha                        
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-md-4">
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-user-plus"></i>
                            <h3 class="box-title">Thông tin khách hàng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-12 nopadding">
                                <label for="full_name">Mã số thuế</label>
                                <input type="text" class="form-control" name="mst" placeholder="Nhập mã số thuế doanh nghiệp" value="<?php echo @$_POST['mst']; ?>">
                                <input type="hidden" name="session_company" value="<?php echo $session_company; ?>">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="phone">Captcha</label>
                                <input type="text" class="form-control" name="captcha">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <img src="/assets/logs/captcha/<?php echo $session_company . '.png'; ?>" style="padding-top:16px">
                            </div>

                            <div class="form-group col-md-12 nopadding">
                                <button type="submit" value="apply" name="submit" class="btn btn-info">TRA CỨU THÔNG TIN DOANH NGHIỆP</button>
                            </div>
                        </div>
                    </div> 

                </div>
                <div class="col-md-8">
                    <div class="box box-primary box-default">
                        <div class="box-header with-border">

                            <h3 class="box-title">Thông tin doanh nghiệp 
                                <?php if ($mst) echo 'có MST: ' . $mst; ?>
                            </h3>
                        </div>
                        <div class="box-body larger-txt">
                            <?php if ($dkkd['ten_doanh_nghiep']): ?>
                                <p><b>Tên doanh nghiệp:</b> <?php echo $dkkd['ten_doanh_nghiep']; ?> </p>

                                <p><b>Địa chỉ:</b> <?php echo $dkkd['dia_chi_tru_so']; ?>

                                <p class="c-red"><b>Ghi chú:</b> <?php echo $thue['ghi_chu']; ?></p>

                                <?php if ($dkkd['mau_dau']): ?> 
                                    <p class="c-red"><b>Mẫu dấu công ty:</b> <a href="/assets/logs/pdf/<?php echo $dkkd['mau_dau'] ?>" target="blank"> <img src="/assets/user/images/pdf-icon.png"> </a></p>
                                        <?php endif; ?>

                                <p><b>Ngày thành lập:</b><?php echo $dkkd['ngay_thanh_lap']; ?></p>

                                <p> <b>Ngày bắt đầu hoạt động:</b> <?php echo $thue['ngay_bat_dau_hd']; ?></p>

                                <p><b>Giám đốc:</b> <?php echo $thue['chu_so_huu']; ?></p>

                                <p><b>Địa chỉ nhà giám đốc: </b><?php echo $thue['dia_chi_chu_so_huu']; ?></p>

                                <p><b>Địa chỉ nhận báo cáo thuế: </b><?php echo $dkkd['dia_chi_tru_so']; ?></p>

                                <p> <b>Chi cục thuế: </b><?php echo $thue['co_quan_thue']; ?></p>

                                <p>
                                    <?php if ($thue['ngay_thay_doi_thong_tin']): ?>
                                        <b>Ngày thay đổi thông tin giấy phép: </b><?php echo $thue['ngay_thay_doi_thong_tin']; ?>
                                    <?php endif; ?>
                                </p>
                                <p>
                                    <?php if ($thue['dia_chi_nhan_thong_bao_thue']): ?>
                                        <b>Ngày đóng MST:</b> <?php echo $thue['dia_chi_nhan_thong_bao_thue']; ?>


                                    <?php endif; ?>
                                </p>
                                <p>
                                    <b>Cách tính thuế: </b><?php echo $thue['pp_tinh_thue_gtgt']; ?>
                                </p>

                                <p><b>Tên tiếng anh:</b> <?php echo $dkkd['ten_doanh_nghiep_tieng_anh']; ?></p>


                                <p><b>Tên viết tắt:</b> <?php echo $dkkd['ten_doanh_nghiep_viet_tat']; ?></p>

                                <p><b>Ngày thành lập:</b> <?php echo $dkkd['ngay_thanh_lap']; ?></p>

                                <p><b>Loại hình công ty:</b>  <?php echo $dkkd['loai_hinh_phap_ly']; ?></p>
                                <ul>
                                    <?php
                                    //echo '<pre>';
                                    foreach ($dkkd['nganh_nghe_kinh_doanh'] as $k => $v) {
                                        //print_r($v);
                                        echo '<li>' . $v[0] . ': ' . $v[1] . '</li>';
                                    }
                                    //echo '</pre>';
                                    ?>
                                </ul>
                            <?php else: ?>
                                Vui lòng nhập mã số doanh nghiệp vào ô bên trái để xem thông tin
                            <?php endif; ?>


                        </div>
                    </div>


                </div>

            </div>
        </form>
    </section>
</div>