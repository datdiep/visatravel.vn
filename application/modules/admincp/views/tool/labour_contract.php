
<div class="content-wrapper">    
    <section class="content-header">
        <h1>TẠO MẪU HỢP ĐỒNG LAO ĐỘNG ONLINE</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>gov">Đăng ký visa Đài Loan online</a></li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <?php if (@$result == 'ok'): ?>
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Apply thành công!</h4>

                            <i class="fa fa-download" aria-hidden="true"></i>  <a href="<?php echo $customer['url']; ?>" target="blank">  DOWNLOAD VISA ĐÀI LOAN CỦA KHÁCH HÀNG </a>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (@$result == 'false'): ?>
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Apply thất bại!</h4>

                            <i class="fa fa-download" aria-hidden="true"></i>  Vui lòng kiểm tra và báo lại Admin 
                        </div>
                    </div>
                <?php endif; ?>


                <?php if ($this->data['check_error'] == 1): ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-md-6">
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-user-plus"></i>
                            <h3 class="box-title">Thông tin nhân viên ký hợp đồng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="hoten">Họ & Tên</label>
                                <input type="text" class="form-control" name="hoten" placeholder="Họ & Tên" value="<?php echo @$info_labour['hoten'] ?>">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="DOB">Ngày tháng năm sinh</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="ngaysinh" value="<?php echo @$info_labour['ngaysinh'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="address">Địa chỉ thường trú</label>
                                <input type="text" class="form-control" name="diachi" value="<?php echo @$info_labour['diachi'] ?>" placeholder="Địa chỉ thường trú">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="cmnd">Số CMND / Thẻ căn cước</label>
                                <input type="text" class="form-control" name="cmnd" value="<?php echo @$info_labour['cmnd'] ?>" placeholder="Nhập số CMND">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="ngaycap">Ngày cấp CMND</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="ngaycap" value="<?php echo @$info_labour['ngaycap'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="noicap">Nơi cấp</label>
                                <input type="text" class="form-control" name="noicap" value="<?php echo @$info_labour['noicap'] ?>">
                            </div>

                            <!--                            <div class="form-group col-md-4 nopadding">
                                                            <label for="phone">Số điện thoại</label>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-phone"></i>
                                                                </div>
                                                                <input type="text" class="form-control"  name="dienthoai" placeholder="Nhập số điện thoại của khách" value="<?php echo @$info_labour['dienthoai'] ?>">
                                                            </div>
                            
                                                        </div>-->

                            <div class="form-group col-md-4 nopadding">
                                <label for="company_name">Chọn công ty ký HĐ</label>

                                <select name="company_name" class="form-control valid" style="background-color: #edf0f5;">
                                    <?php foreach ($list_company as $v): ?>
                                        <option <?php if ($info_labour['company_name'] == $v) echo 'selected'; ?> value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="position">Chức vụ</label>
                                <input type="text" class="form-control" name="chucvu" value="<?php echo @$info_labour['chucvu'] ?>" placeholder="Chức vụ">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="salary">Mức lương</label>
                                <input type="text" class="form-control format_number" style="background-color: #edf0f5;" name="luong" value="<?php echo @$info_labour['luong'] ?>" placeholder="Mức lương trong hợp đồng"">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="so_hd">Số hợp đồng</label>
                                <input type="text" class="form-control" name="so_hd" value="<?php echo @$info_labour['so_hd'] ?>" placeholder="076/2015/HDLĐ">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="loai_hd">Loại hợp đồng</label>
                                <select name="loai_hd" class="form-control valid">
                                    <?php foreach ($list_loaihd as $k => $v) : ?>
                                        <option <?php echo $info_labour['loai_hd'] == $v ? 'selected' : ''; ?> value="<?php echo $v; ?>"><?php echo $v; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="ngay_ky">Ngày ký hợp đồng</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="date" value="<?php echo @$info_labour['date'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                        </div>
                    </div> 

                    <div  class="col-md-12"> 
                        <div class="row">
                            <div class="box" id="list_cost">
                                <div class="box-header with-border">
                                    <i class="fa fa-key"></i>
                                    <h3 class="box-title">Thông tin nghỉ phép</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group col-md-6 nopadding">
                                        <label for="lydo">Lý do nghỉ phép</label>
                                        <input type="text" class="form-control" name="lydo" value="<?php echo @$info_labour['lydo'] ?>" placeholder="Đi du lịch nước ngoài" >

                                    </div>

                                    <!--                                    <div class="form-group col-md-4 nopadding">
                                                                            <label for="leave_start">Từ ngày</label>
                                                                            <div class="input-group field-date">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </div>
                                                                                <input type="text" class="form-control" name="tungay" value="<?php echo @$info_labour['tungay'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                                            </div>
                                                                        </div>
                                    
                                                                        <div class="form-group col-md-4 nopadding">
                                                                            <label for="leave_end">Đến ngày</label>
                                                                            <div class="input-group field-date">
                                                                                <div class="input-group-addon">
                                                                                    <i class="fa fa-calendar"></i>
                                                                                </div>
                                                                                <input type="text" class="form-control" name="denngay" value="<?php echo @$info_labour['denngay'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                                                            </div>
                                                                        </div>-->

                                    <div class="form-group col-md-6 nopadding">
                                        <label for="leave_submit">Ngày làm đơn nghỉ phép</label>
                                        <div class="input-group field-date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" class="form-control" name="date_nghiphep" value="<?php echo @$info_labour['date_nghiphep'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">
                        <div class="box-header with-border">

                            <h3 class="box-title">Thông tin bảng lương</h3>
                        </div>
                        <div class="box-body">
                            <?php
                            for ($i = 1; $i < 4; $i++):
                                ?>

                                <div class="col-md-4">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">Thu nhập Tháng <?php echo $salary_date[$i]; ?></h4>
                                    </div>

                                    <!-- input states -->

                                    <div class="form-group">
                                        <span class="help-block">Thưởng kinh doanh</span>
                                        <input type="text" name="kinhdoanh_<?php echo $i; ?>" value="<?php echo @$info_labour['kinhdoanh_' . $i] ?>" class="form-control format_number">
                                        <input type="hidden" name="thang_<?php echo $i; ?>" value="Tháng <?php echo $salary_date[$i]; ?>" class="form-control format_number">

                                    </div>
                                    <div class="form-group">
                                        <span class="help-block">Tiền phụ cấp</span>
                                        <input type="text" name="phucap_<?php echo $i; ?>" class="form-control format_number" value="<?php echo @$info_labour['phucap_' . $i] ?>" >
                                    </div>
                                    <div class="form-group">
                                        <span class="help-block">Giảm trừ</span>
                                        <input type="text" name="giamtru_<?php echo $i; ?>" class="form-control format_number" value="<?php echo @$info_labour['giamtru_' . $i] ?>">
                                    </div>
                                    <div class="form-group">
                                        <span class="help-block">Tiền thực lãnh</span>
                                        <input type="text" name="tong_<?php echo $i; ?>" class="form-control format_number" value="<?php echo @$info_labour['tong_' . $i] ?>">
                                    </div>

                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" name="submit" class="btn btn-info">TẠO HỢP ĐỒNG LAO ĐỘNG</button>
                    </div>
                </div>
            </div>
        </form>
    </section>



</div>
<!-- InputMask -->
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
    $(function () {

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })

    $('.format_number').autoNumeric('init', {aPad: false});
    $('form').submit(function () {
        $('input.format_number').each(function () {
            $(this).val($(this).autoNumeric('get'));
        });
    });


    $('#surname').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#given').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#passno').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#flightNo').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#address').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#credentials').keyup(function () {
        this.value = this.value.toUpperCase();
    });


</script>