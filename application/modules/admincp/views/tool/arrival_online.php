
<form id="form" method="post" enctype="multipart/form-data">
    <div class="content-wrapper arrival-card"> 
        <div class="col-sm-12 col-xs-12 header-background-page" >
            <div class="col-md-12 col-sm-12" style="padding-bottom:10px">
                <center> <img src="/assets/admin/images/barcode.gif"> </center>
            </div>  
            <div class="col-md-12 col-sm-12 col-xs-12 bg-white">
                <div class="col-md-12 col-sm-12">
                    <div class="border-gray" style="font-size: 150%;">
                        <center> <b>ARRIVAL CARD</b>
                        </center>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-sm-6 col-xs-6 border-gray">
                        <p>Family Name</p>
			<?php echo $objInfor['surname']; ?>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6 border-gray">
                        <p>Passport No.</p>
			<?php echo $objInfor['passno']; ?>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="border-gray">
                        <p> Given Name</p>
			<?php echo $objInfor['given']; ?>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-6 col-xs-6 border-gray" style="padding-bottom:15px">
                        <p>Date of Birth</p>

                        <span class="border-number"><?php echo $ngaysinh[0]; ?></span>
                        <span class="border-number"><?php echo $ngaysinh[1]; ?> </span>
                        <span class="border-number"><?php echo $ngaysinh[2]; ?></span>
                    </div>
                    <div class="col-md-6 col-xs-6 border-gray" style="padding-bottom:15px">
                        <p>Nationality</p>
                        VIETNAM
                    </div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="col-md-4 col-xs-4 border-gray">
                        <p>Sex</p>
                        <input type="checkbox" <?php if (@$objInfor['sex'] == 0) echo 'checked="checked"'; ?>> Male
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox" <?php if (@$objInfor['sex'] == 1) echo 'checked="checked"'; ?>> Femal
                    </div>
                    <div class="col-md-4 col-xs-4 border-gray">
                        <p>Flight / Vessel No.</p>
			<?php echo $objInfor['rq_flightNo']; ?>
                    </div>
                    <div class="col-md-4 col-xs-4 border-gray">
                        <p>Occupation</p>
                        Officer
                    </div>
                </div>

                <div class="col-md-12 col-xs-12">
                    <div class="border-gray">
                        <p>Visa Type</p>
                        <input type="checkbox"> Diplomatic
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox"> Courtesy
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox"> Resident
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox" checked=""> <b>Visitor</b>
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox"> Visa-Exempt
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox"> Landing
                        &nbsp;&nbsp;&nbsp;
                        <input type="checkbox"> Others
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="border-gray">
                        <p>Entry Permit / Visa No.</p>
                        <input type="text" class="form-control dash_border" name="evisa_no" placeholder="Nhập số Visa vào đây" value="<?php echo @$objInfor['evisa_no']; ?>">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="border-gray">
                        <p>Home Adress</p>
                        <input type="text" class="form-control dash_border" name="vn_address" placeholder="Nhập địa chỉ ở Việt Nam"  value="<?php echo @$objInfor['vn_address']; ?>">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="border-gray">
                        <p>Residential Address or Hotel Name in Taiwan</p>
                        <input type="text" class="form-control dash_border" name="taiwan_destination" placeholder="Nhập địa chỉ ở Đài Loan"  value="<?php echo @$objInfor['taiwan_destination']; ?>">
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="col-md-6 col-xs-6 border-gray">
                        <p>Purpose Of Visit</p>
                        <div class="col-md-6 col-xs-6">
                            <p><input type="checkbox"> 1.Business</p>
                            <p><input type="checkbox" checked=""> 3.Sightseeing</p>
                            <p><input type="checkbox"> 5.Visit Relative</p>
                            <p><input type="checkbox"> 7.Conference</p>
                            <p><input type="checkbox"> 9.Religion</p>
                            <p><input type="checkbox"> 10.Other: _________</p>
                        </div>

                        <div class="col-md-6 col-xs-6">
                            <p><input type="checkbox"> 2.Study</p>
                            <p><input type="checkbox"> 4.Exhibition</p>
                            <p><input type="checkbox"> 6.Medical Care</p>
                            <p><input type="checkbox"> 8.Employment</p>
                        </div>
                        <div class="col-md-12 col-xs-12" style="border-top:1px #d2d2d2 solid; border-bottom:1px #d2d2d2 solid">
                            <p>Singture: <span class="c-red">(Ký tên tại đây)</span></p>
                            <br>
                        </div>

                    </div>
                    <div class="col-md-6 col-xs-6 border-gray">
                        Offical Use Only
                        <div style="border:1px #000 solid; width:307px; height: 240px">

                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" value="apply" name="submit" class="btn btn-info" style="margin-top:20px">IN TỜ KHAI NHẬP CẢNH</button>
                </div>
            </div>  

            <div class="col-md-12">
                <center> WELCOME TO ROC (TAIWAN)</center>
            </div>
        </div>

    </div>
</form>

<style>
    @media print {
	.bg-white{
	    background-color: #fff !important;
	}
	.header-background-page{
	    background-color: #d2d2d2 !important;
	}
	.box-footer{display:none}
	input{
	    border: none!important;
	}
	p {
	    margin: 0 0 3px 0!important;
	}
    }
   
    .header-background-page{
	background-color: #d2d2d2; padding: 10px; margin: 10px; width: 18cm
    }

    .bg-white{background-color: #fff}
    .arrival-card {max-width: 700px}
    .arrival-card div{    padding: 5px 5px 5px 5px; }
    .arrival-card div p{font-weight: bold}
    .border-gray { padding: 10px; border: 1px #d2d2d2 solid}
    .border-number{padding: 3px 5px; border: 1px #d2d2d2 solid; margin-right: 5px}
    .dash_border {border: 1px #929292 dashed; color: #000}

    page {
        background: white;
        display: block;
        margin: 0 auto;
        margin-bottom: 0.5cm;
        box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
        padding: 5mm;
    }
    page[size="A4"] {  
        width: 21cm;
    }
    page[size="A4"][layout="portrait"] {
        width: 29.7cm;
    }
    page[size="A3"] {
        width: 29.7cm;
    }
    page[size="A3"][layout="portrait"] {
        width: 42cm;
    }
    page[size="A5"] {
        width: 14.8cm;
    }
    page[size="A5"][layout="portrait"] {
        width: 21cm; 
    }
    @media print {
        body, page {
            margin: 0;
            box-shadow: 0;
        }
    }

</style>
<link href="/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />