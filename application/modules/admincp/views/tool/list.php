<?php $this->load->library('transload'); ?>
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row" style="background-color: #e8e8e8">
                    <th>STT</th>
                    <th>Name</th>      
                    <th>Số hộ chiếu</th> 
                    <th>Hỗ trợ</th> 
                    <th>Ngày cập nhật</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><input type="checkbox" class="" name="id[]" value="<?php echo $item['id'] ?>"></td> 
                        <td>
                            <a href="/assets/logs/pdf/<?php echo $item['file'] ?>" title="Download visa của khách hàng <?php echo $item['englishName']; ?>">
                                <?php echo $item['englishName']; ?>
                            </a>
                        </td>                                                 
                        <td><?php echo $item['passno'] ?></td>
                        <td>
                            <?php if (($item['username'] == $this->data['user']['username'] && $item['network_id'] == $this->data['user']['network_id']) || !empty($this->data['user']['role_department'])): ?>
                                <a href="/<?php echo ADMIN_URL . 'tool/arrival/' . $item['id']; ?>" title="Apply lại visa cho <?php echo $item['englishName']; ?>" target="blank">Tờ khai nhập cảnh</a> 
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php
                            echo $this->transload->ago(strtotime($item['date_post']), 0) . ' trước';
                            ?>
                        </td>
                        <td>
                            <?php if (($item['username'] == $this->data['user']['username'] && $item['network_id'] == $this->data['user']['network_id']) || !empty($this->data['user']['role_department'])): ?>
                                <a href="/<?php echo ADMIN_URL . 'tool/apply/' . $item['id']; ?>" title="Apply lại visa cho <?php echo $item['englishName']; ?>">Apply Visa</a> | 
                                <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      
        </table>
    </div>
</div>

<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
