<div class="content-wrapper">    
    <section class="content-header">
        <h1>APPLY VISA ĐÀI LOAN <?php echo @$objInfor['surname'] ?> <?php echo @$objInfor['given'] ?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>gov">Đăng ký visa Đài Loan online</a></li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <?php if (@$result == 'ok'): ?>
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Apply thành công!</h4>

                            <i class="fa fa-download" aria-hidden="true"></i>  <a href="<?php echo $customer['url']; ?>" target="blank">  DOWNLOAD VISA ĐÀI LOAN CỦA KHÁCH HÀNG </a>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if (@$result == 'false'): ?>
                    <div class="col-md-12">
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Apply thất bạii!</h4>

                            <i class="fa fa-download" aria-hidden="true"></i>  Vui lòng kiểm tra và báo lại Admin 
                        </div>
                    </div>
                <?php endif; ?>


                <?php if ($this->data['check_error'] == 1): ?>
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    </div>
                <?php endif; ?>
                <div class="col-md-6">
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-user-plus"></i>
                            <h3 class="box-title">Thông tin khách hàng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="full_name">Họ/Surname</label>
                                <input type="text" class="form-control" name="surname" id="surname" placeholder="Họ" value="<?php echo @$objInfor['surname'] ?>">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="phone">Tên/Given Name</label>
                                <input type="text" class="form-control" name="given" id="given" placeholder="Tên" value="<?php echo @$objInfor['given'] ?>">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="nation">Quốc tịch</label>
                                <select name="nation" class="form-control valid">
                                    <option value="8">Ấn Độ </option>
                                    <option value="9">Indonesia</option>
                                    <option value="24">Philippines</option>
                                    <option value="34" selected="selected">Việt Nam</option>
                                    <option value="4">Myanmar</option>
                                    <option value="17">Lao</option>
                                    <option value="5">Cambodia</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="email">Số điện thoại</label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                    <input type="text" class="form-control"  name="phone_number" placeholder="Nhập số điện thoại của khách" value="<?php echo @$objInfor['phone_number'] ?>">
                                </div>

                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="address">Số hộ chiếu</label>
                                <input type="text" class="form-control" name="passno" id="passno" placeholder="Số hộ chiếu" value="<?php echo @$objInfor['passno'] ?>">
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="email">Giới tính</label>

                                <div class="form-control">
                                    <input type="radio" name="sex" value="0" class="minimal" <?php if (@$_POST['sex'] == 0) echo 'checked="checked"'; ?>> Nam
                                    <input type="radio" name="sex" value="1" class="minimal" <?php if (@$_POST['sex'] == 1) echo 'checked="checked"'; ?>> Nữ

                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="appointment">Ngày tháng năm sinh</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="birthDate" value="<?php echo @$objInfor['birthDate'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="passportExpiryDate">Ngày tháng hết hạn hộ chiếu</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="passportExpiryDate" value="<?php echo @$objInfor['passportExpiryDate'] ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="occupation">Công việc</label>
                                <select name="rq.occupation" class="form-control valid">
                                    <option value="">Chọn Công việc</option>
                                    <option value="29">ACCOUNTANT/CPA</option>
                                    <option value="12">ACTOR/ENTERTAINER</option>
                                    <option value="26">ARMY/NAVY</option>
                                    <option value="1">ARTIST</option>
                                    <option value="30">BANKER</option>
                                    <option value="10">BUSINESS/DIRCTOR/CEO/HOUSEWORKS</option>
                                    <option value="40">CARETAKER</option>
                                    <option value="2">CLERK/EMPLOYEE/STAFF</option>
                                    <option value="27">CONSULTANT</option>
                                    <option value="3">CREW/SEAMAN</option>
                                    <option value="33">DIPLOMAT</option>
                                    <option value="31">DIVER</option>
                                    <option value="4">DOCTOR/DENTIST</option>
                                    <option value="5">ENGINNER/ENG/ARCHITECH</option>
                                    <option value="6">FARMER/AGRICULTURE</option>
                                    <option value="34">FISHERMAN</option>
                                    <option value="7">GOVERNMENT OFFICER</option>
                                    <option value="39">HOUSEKEEPER/HOUSEMAKER</option>
                                    <option value="8">HOUSEWIFE</option>
                                    <option value="36">IT</option>
                                    <option value="16">JOURNALIST/REPORTER</option>
                                    <option value="41">LABOR</option>
                                    <option value="32">LAWYER/ATTORNEY/COUNSEL</option>
                                    <option value="9">MECHANIC/FACTORY WORKER</option>
                                    <option value="38">MANUFACTURE WORKER</option>
                                    <option value="35">MUSICIAN</option>
                                    <option value="13">NURSE</option>
                                    <option value="14">PILOT</option>
                                    <option value="11">PRIEST</option>
                                    <option value="28">PROFESSOR/LECTURER/INSTRUCTOR</option>
                                    <option value="37">RESEARCHER</option>
                                    <option value="17">SALESMAN</option>
                                    <option value="18">SCIENTIST</option>
                                    <option value="19">SECRETARY</option>
                                    <option value="15">SPECAILIST</option>
                                    <option value="20">STUDENT/SCHOLAR/PUPIL</option>
                                    <option value="21">TEACHER/EDUCATIOR</option>
                                    <option value="22">TECHNICIAN</option>
                                    <option value="23">WRITER</option>
                                    <option value="25">NONE/BABY/INFANT</option>
                                    <option value="42">OFW</option>
                                    <option value="24" selected="">KHÁC</option>
                                </select>
                            </div>

                            <div class="form-group col-md-12 nopadding">
                                <input type="checkbox" name="special_block" value="1" class="minimal" <?php if (@$_POST['special_block'] == 1) echo 'checked="checked"'; ?>> <b>Xử lý trường hợp khó</b> (Trường hợp không apply được hãy chọn nút này)
                            </div>
                        </div>
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">
                        <div class="box-header with-border">
                            <img src="/assets/admin/images/flags/taiwan.png" align="top">
                            <h3 class="box-title">Thông tin điểm đến tại Đài Loan</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-4 nopadding">
                                <label for="rq.flightNo">Số hiệu chuyến bay</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="rq.flightNo" id="flightNo" value="<?php echo @$objInfor['rq_flightNo'] ?>"/>
                                    <label class="input-group-addon btn" for="rq.flightNo">
                                        <span class="fa fa-plane"></span>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="expectEntryDate">Ngày đến Đài Loan</label>

                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="expectEntryDate" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="appointment">Mục đích nhập cảnh</label>
                                <div class="input-group field-date">
                                    <select name="rq.reason" class="form-control">
                                        <option value="15">Công tác</option>
                                        <option value="213">Du học</option>
                                        <option value="1" selected="">Du lịch</option>
                                        <option value="60">Hội chợ</option>
                                        <option value="3">Thăm thân</option>
                                        <option value="23">Khám bệnh</option>
                                        <option value="6">Hội thảo</option>
                                        <option value="214">Lao động</option>
                                        <option value="9">Tôn giáo</option>
                                        <option value="33">Khác</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="email">Nơi sẽ lưu trú</label>
                                <select name="rq.hotelFlag" class="form-control valid">
                                    <option value="">Chọn nơi lưu trú</option>
                                    <option value="0" <?php if (@$_POST['rq_hotelFlag'] == 0) echo 'selected=""'; ?>>Nhà người thân</option>
                                    <option value="1" <?php if (@$_POST['rq_hotelFlag'] == 1) echo 'selected=""'; ?>>Khách sạn</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="rq.zipCode">Thành phố sẽ đến</label>
                                <select name="rq.zipCode" class="form-control valid">
                                    <option value="">Chọn nơi lưu trú</option>
                                    <option value="1" <?php if (@$objInfor['rq_zipCode'] == 1) echo 'selected=""'; ?>>Taipei City</option>
                                    <option value="2" <?php if (@$objInfor['rq_zipCode'] == 2) echo 'selected=""'; ?>>Keelung City</option>
                                    <option value="3" <?php if (@$objInfor['rq_zipCode'] == 3) echo 'selected=""'; ?>>New Taipei City</option>
                                    <option value="4" <?php if (@$objInfor['rq_zipCode'] == 4) echo 'selected=""'; ?>>Yilan County</option>
                                    <option value="5" <?php if (@$objInfor['rq_zipCode'] == 5) echo 'selected=""'; ?>>Hsinchu County</option>
                                    <option value="6" <?php if (@$objInfor['rq_zipCode'] == 6) echo 'selected=""'; ?>>Taoyuan City</option>
                                    <option value="7" <?php if (@$objInfor['rq_zipCode'] == 7) echo 'selected=""'; ?>>Miaoli County</option>
                                    <option value="8" <?php if (@$objInfor['rq_zipCode'] == 8) echo 'selected=""'; ?>>Taichung City</option>
                                    <option value="10" <?php if (@$objInfor['rq_zipCode'] == 10) echo 'selected=""'; ?>>Changhua County</option>
                                    <option value="11" <?php if (@$objInfor['rq_zipCode'] == 11) echo 'selected=""'; ?>>Nantou County</option>
                                    <option value="12" <?php if (@$objInfor['rq_zipCode'] == 12) echo 'selected=""'; ?>>Chiayi County</option>
                                    <option value="13" <?php if (@$objInfor['rq_zipCode'] == 13) echo 'selected=""'; ?>>Yunlin County</option>
                                    <option value="14" <?php if (@$objInfor['rq_zipCode'] == 14) echo 'selected=""'; ?>>Tainan City</option>
                                    <option value="16" <?php if (@$objInfor['rq_zipCode'] == 16) echo 'selected=""'; ?>>Kaohsiung City</option>
                                    <option value="18" <?php if (@$objInfor['rq_zipCode'] == 18) echo 'selected=""'; ?>>Penghu County</option>
                                    <option value="19" <?php if (@$objInfor['rq_zipCode'] == 19) echo 'selected=""'; ?>>Pingtung County</option>
                                    <option value="20" <?php if (@$objInfor['rq_zipCode'] == 20) echo 'selected=""'; ?>>Taitung County</option>
                                    <option value="21" <?php if (@$objInfor['rq_zipCode'] == 21) echo 'selected=""'; ?>>Hualien County</option>
                                    <option value="22" <?php if (@$objInfor['rq_zipCode'] == 22) echo 'selected=""'; ?>>Kinmen County</option>
                                    <option value="23" <?php if (@$objInfor['rq_zipCode'] == 23) echo 'selected=""'; ?>>Lienchiang County</option>
                                    <option value="24" <?php if (@$objInfor['rq_zipCode'] == 24) echo 'selected=""'; ?>>Hsinchu City</option>
                                    <option value="25" <?php if (@$objInfor['rq_zipCode'] == 25) echo 'selected=""'; ?>>Chiayi City</option>
                                </select>
                            </div>

                            <div class="form-group col-md-4 nopadding">
                                <label for="rq.address">Địa chỉ hoặc tên khách sạn</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="rq.address" id="address" placeholder="Nhập tên khách sạn hoặc địa chỉ nhà người thân ở Đài Loan" value="<?php echo @$objInfor['rq_address'] ?>"/>
                                    <label class="input-group-addon btn" for="rq.address">
                                        <span class="fa fa-bed"></span>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div  class="col-md-12"> 
                        <div class="row">
                            <div class="box" id="list_cost">
                                <div class="box-header with-border">
                                    <i class="fa fa-key"></i>
                                    <h3 class="box-title">Điều kiện Nhập cảnh</h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="form-group col-md-4 nopadding">
                                        <label for="capacity">Bằng chứng</label>
                                        <select name="capacity" class="form-control valid">
                                            <option value="">Chọn loại bằng chứng</option>
                                            <option value="1">Người có thẻ cư trú</option>
                                            <option value="2" selected="">Có visa của nước phát triển</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4 nopadding">
                                        <label for="capacity_2_type">Quốc gia</label>
                                        <select name="capacity_2_type" class="form-control valid">
                                            <option value="">Chọn quốc gia có visa</option>
                                            <option value="013" <?php if (@$objInfor['capacity_2_type'] == '013') echo 'selected=""'; ?>>Nhật Bản</option>
                                            <option value="35" <?php if (@$objInfor['capacity_2_type'] == 35) echo 'selected=""'; ?>>Đài Loan</option>
                                            <option value="015" <?php if (@$objInfor['capacity_2_type'] == '015') echo 'selected=""'; ?>>Hàn Quốc</option>
                                            <option value="315" <?php if (@$objInfor['capacity_2_type'] == 315) echo 'selected=""'; ?>>Mỹ</option>
                                            <option value="303" <?php if (@$objInfor['capacity_2_type'] == 303) echo 'selected=""'; ?>>Canada</option>
                                            <option value="232" <?php if (@$objInfor['capacity_2_type'] == 232) echo 'selected=""'; ?>>Anh</option>
                                            <option value="101" <?php if (@$objInfor['capacity_2_type'] == 101) echo 'selected=""'; ?>>Úc</option>
                                            <option value="104" <?php if (@$objInfor['capacity_2_type'] == 104) echo 'selected=""'; ?>>New Zealand</option>
                                            <option value="299" <?php if (@$objInfor['capacity_2_type'] == 299) echo 'selected=""'; ?>>Schengen</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4 nopadding">
                                        <label for="rq.address">Số visa</label>
                                        <div class="input-group">
                                            <input type='text' class="form-control" name="credentials" id="credentials" value="<?php echo @$objInfor['credentials'] ?>"/>
                                            <input type="hidden" name="PRO" value="PRO_Task12ApplyStep1">
                                            <label class="input-group-addon btn" for="rq.address">
                                                <span class="fa fa-cc-visa"></span>
                                            </label>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="apply" name="submit" class="btn btn-info">APPLY VISA ONLINE</button>
                    </div>
                </div>


            </div>
        </form>
    </section>


    <div class="col-md-12 content">

        <div class="form-group">
            <ul>
                <li><p>Kiểm tra thật kỹ trước khi bấm APPLY. <b><?php echo $user['fullname'] ?></b> sẽ chịu trách nhiệm hoàn toàn về sự sai sót trong nhập liệu!</p></li>
                <li><p>Trường hợp apply bị thất bại. Báo ngay cho quản lý để được hỗ trợ hoặc apply qua link này: <a href="https://niaspeedy.immigration.gov.tw/nia_southeast/languageAction" target="blank">APPLY VISA ĐÀI LOAN ONLINE</a></p></li>
                <li>
                    <div class="timeline-footer">
                        <a href="/admincp/tool/" >DANH SÁCH KHÁCH HÀNG ĐÃ APPLY VISA ĐÀI LOAN</a>
                    </div>
                </li>
            </ul>

            <ul class="timeline">
                <li>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> Luật Đài Loan 1/7/2016</span>

                        <h3 class="timeline-header"><a href="#">Các quy định phải đọc</a> </h3>

                        <div class="timeline-body">
                            <ul>
                                <li>Hộ chiếu phải còn hạn từ 6 tháng trở lên (là chỉ thời điểm nhập cảnh Đài Loan, chứ không phải thời điểm xin giấy phép nhập cảnh).</li>
                                <li>Phải có vé máy bay, vé tàu khứ hồi, hoặc vé máy bay, vé tàu cho điểm đến tiếp theo</li>
                                <li>Là người chưa từng đến Đài Loan lao động.</li>
                                <li>Đối với đã có visa Đài Loan không áp dụng cho visa có ký hiệu FL và visa X . Các trường hợp phạm pháp ở Đài Loan đều không được áp dụng.</li>
                            </ul>
                        </div>

                    </div>
                </li>
            </ul>

            <ul class="timeline">
                <li>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> Luật Đài Loan 1/7/2016</span>
                        <h3 class="timeline-header"><a href="#">Lưu ý</a> </h3>
                        <div class="timeline-body">
                            <ul>
                                <li>Visa Đài Loan online có hiệu lực 90 ngày - Ra vô nhiều lần. Chỉ apply lại trước 7 ngày hết hạn mới được ra thời hạn mới.</li>
                                <li>Khi điền sai có thể xin lại ngay lập tức.</li>
                                <li>Người có visa vào các nước phát triển nhưng trên visa có đóng các loại dấu như: VOID, CANCELLED hoặc CANCELLED WITHOUT PREJUDICE thì không được áp dụng.</li>
                                <li>Quá hạn trong vòng 10 năm là chỉ ngày đến hạn hiệu lực của visa và ngày nhập cảnh Đài Loan, khoảng cách giữa hai ngày này là không vượt quá 10 năm.</li>
                                <li><a href="https://acard.immigration.gov.tw/nia_acard" target="blank">Có thể điền tờ khai nhập cảnh tại link này</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

</div>
<!-- InputMask -->
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script>
    $(function () {

        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()



    })

    $('#surname').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#given').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#passno').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#flightNo').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#address').keyup(function () {
        this.value = this.value.toUpperCase();
    });
    $('#credentials').keyup(function () {
        this.value = this.value.toUpperCase();
    });

</script>