<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH SÁCH HỢP ĐỒNG LAO ĐỘNG</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">danh sách khách hàng</li>
        </ol>
        <br/>
        <a href="/<?php echo ADMIN_URL; ?>tool/labour_contract/list"><button style="float:left;margin-right: 10px;width: 200px;" class="btn btn-block btn-info">TẠO HỢP ĐỒNG</button></a>

        <input id="keyword" class="form-control" placeholder="Nhập CMND hoặc Tên khách hàng" value="" style="width: 260px;float: left;margin-right: 10px;"/>

        <?php if (!empty($networks)): ?>
            <select class="form-control" id="type" style="width: 250px;float: left;margin:0 10px 5px 0px">
                <option value="">Chọn công ty cần xem</option>
                <?php foreach ($networks as $value): ?>
                    <option <?php echo @$taiwan_type == $value['id'] ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                <?php endforeach; ?>
            </select>
        <?php endif; ?>
    </section>

    <!-- Main content -->
    <br><br>
    <section class="content">

        <div class="box box-success">
            <div class="box-header ">
                <h3 class="box-title"><i class="fa fa-cc-visa"></i> Danh sách hợp đồng lao động đã tạo </h3>
            </div>
            <div class="box-body no-padding">
                <div id="list_ajax"></div>
            </div>
        </div>

    </section>
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Bạn có muốn xóa trang id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>tool/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }

    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;

        var keyword = $('#keyword').val();
        var type = $('#type').val();

        $.post('/<?php echo ADMIN_URL; ?>tool/labour_load', {pos: pos, type: type, keyword: keyword}, function (results) {
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });


    $('#type').change(function () {
        loadlist(0);
    });
    $('#keyword').keypress(function (e) {
        if (e.keyCode == 13)
            loadlist(0);
    });

</script>