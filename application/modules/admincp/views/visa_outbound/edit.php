<div class="content-wrapper">    
    <section class="content-header">
        <h1>KIỂM TRA ĐƠN #<?php echo $order['id']; ?></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">Quản lý đơn hàng</a></li>
            <li class="active">Kiểm tra đơn</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    Sửa tin thành công
                </div>
            <?php endif; ?>
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="0" name="submit" class="btn btn-info">Lưu lại</button>
                        <button type="submit" value="1" name="submit" class="btn btn-success"><i class="fa fa-repeat"></i> Chuyển hồ sơ</button>
                        <div class="box-tools pull-right">
                            <span>TOTAL FEES: <b><?php echo $order['total_fees'] . '$'; ?></b></span>
                            <span>Visa fees: <b><?php echo $order['visa_fees'] . '$'; ?></b></span>
                            <span>Processing fees: <b><?php echo $order['processing_time_fees'] . '$'; ?></b></span>
                            <span>Nationality fees: <b><?php echo $order['nationality_fees'] . '$'; ?></b></span>
                        </div>                      
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin visa</h3>
                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="alias">Trạng thái thanh toán</label>
                                <select id="visit_purpose" name="visit_purpose" disabled class="form-control">
                                    <option>Đã thanh toán</option>
                                    <option>Chưa thanh toán</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="pax">Number of persons</label>
                                <input class="form-control" name="pax" disabled value="<?php echo $order['pax']; ?> persons">
                            </div>
                            <div class="form-group">
                                <label for="alias">Purpose of visit</label>
                                <select id="visit_purpose" name="visit_purpose" disabled class="form-control">
                                    <?php foreach ($service_fees['visa_fees'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>" <?php echo $key == $order['purpose_of_visit'] ? 'selected' : ''; ?>><?php echo $key; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="category_news">Type of visa</label>
                                <select id="visa_type" name="visa_type" disabled class="form-control">
                                    <!--<option>Please select...</option>-->
                                    <?php foreach ($service_fees['visa_fees']['for tourism'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>" <?php echo $key == $order['type_of_visa'] ? 'selected' : ''; ?>><?php echo $key == '1 year multiple' ? $key . ' (US citizens only)' : $key; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="category_news">Arrival airport</label>
                                <select id="arrival_port" name="arrival_port" disabled class="form-control">
                                    <option value="Tan Son Nhat International Airport (Ho Chi Minh City)" <?php echo $key == $order['arrival_airport'] ? 'selected' : ''; ?>>Tan Son Nhat International Airport (Ho Chi Minh City)</option>
                                    <option value="Noi Bai International Airport (Ha Noi)" <?php echo $key == $order['arrival_airport'] ? 'selected' : ''; ?>>Noi Bai International Airport (Ha Noi)</option>
                                    <option value="Da Nang International Airport" <?php echo $key == $order['arrival_airport'] ? 'selected' : ''; ?>>Da Nang International Airport</option>
                                    <option value="Cam Ranh International Airport (Khanh Hoa)" <?php echo $key == $order['arrival_airport'] ? 'selected' : ''; ?>>Cam Ranh International Airport (Khanh Hoa)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="category_news">Arrival date</label>
                                <div class="input-group field-date">
                                    <input type='text' disabled class="form-control" id='datetimepicker' name="datetimepicker" />
                                    <label class="input-group-addon btn" for="datetimepicker">
                                        <span class="fa fa-calendar"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_news">Processing time</label>
                                <select id="visa_type" name="visa_type" disabled class="form-control">
                                    <!--<option>Please select...</option>-->
                                    <?php foreach ($service_fees['Processing time'] as $key => $item): ?>
                                        <option value="<?php echo $key; ?>"><?php echo $key == '1 year multiple' ? $key . ' (US citizens only)' : $key; ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>
                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin liên hệ</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="full_name">Full name</label>
                                <input type="text" value="<?php echo $order['full_name']; ?>" class="form-control" name="full_name">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" value="<?php echo $order['email']; ?>" class="form-control" name="email">
                            </div>
                            <div class="form-group">
                                <label for="phone">Phone number</label>
                                <input type="text" value="<?php echo $order['phone']; ?>" class="form-control" name="phone">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" value="<?php echo $order['address']; ?>" class="form-control" name="address">
                            </div>
                            <div class="form-group">
                                <label for="leave_message">Leave a message</label>
                                <textarea id="comment" name="leave_message" class="form-control" rows="3"><?php echo $order['leave_message']; ?></textarea>
                            </div>
                        </div> 
                    </div>


                    <div class="box box-footer">
                        <div class="form-group">
                            <label for="exampleInputFile">Tải file lên (.zip)</label>
                            <input type="file" id="exampleInputFile">
                        </div>
                                    <!--       <a href="" class="btn btn-primary"><i class="fa fa-upload"></i> Up file</a>
                                  <a href="" class="btn btn-primary"><i class="fa fa-download"></i> Tải file</a>              -->
                    </div>

                </div>

                <div  class="col-md-8"> 

                    <div class="box box-warning">
                        <div class="box-header">
                            <h3 class="box-title">Danh sách hộ chiếu</h3>
                            <div class="box-tools pull-right">

                                <a href="" class="btn btn-warning"><i class="fa fa-plus"></i> Thêm</a>
                            </div> 
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-bordered" id="passport">
                                <tbody>
                                    <tr>
                                        <th style="width: 10px">#</th>
                                        <th>Full name</th>
                                        <th style="width: 100px">Gender</th>
                                        <th style="width: 140px">Birth date</th>
                                        <th>Nationality</th>
                                        <th style="width: 150px">Passport number</th>
                                        <th style="width: 90px">Action</th>
                                    </tr>
                                    <?php foreach ($order_sku as $i => $p): ?>
                                        <tr>
                                            <td><?php echo $i ?>.</td>
                                            <td><span><?php echo $p['full_name']; ?></span><i class="fa fa-edit fa-lg" rel="full_name" title="Sữa"></i></td>
                                            <td><span><?php echo $p['gender']; ?></span><i class="fa fa-edit fa-lg" rel="gender" title="Sữa"></i></td>
                                            <td><span><?php echo date('d/m/Y', strtotime($p['date_of_birth'])); ?></span><i class="fa fa-edit fa-lg" rel="date_of_birth" title="Sữa"></i></td>
                                            <td><span><?php echo $p['nationality']; ?></span> <?php echo $p['special_fee'] == 0 ? '' : '(' . $p['special_fee'] . '$)'; ?><i  rel="nationality" class="fa fa-edit fa-lg" title="Sữa"></i></td>
                                            <td><span><?php echo $p['passport_number']; ?></span><i rel="passport_number" class="fa fa-edit fa-lg" title="Sữa"></i></td>
                                            <td class="actions">
    <!--                                                <i class="fa fa-edit fa-lg" title="Sữa"></i>
                                                <i class="fa fa-save fa-lg" title="Lưu"></i>-->
                                                <i class="fa fa-trash-o fa-lg" title="Xóa"></i> 
                                                <i class="fa fa-ban fa-lg" title="Từ chối"></i> 
                                                <i class="fa fa-print fa-lg"  title="In"></i>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>


                                </tbody></table>
                        </div>
                        <!-- /.box-body -->
                    </div>

                </div>
                <div class="col-md-4">
                    <!-- Chat box -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-comments-o"></i>

                            <h3 class="box-title">Ghi chú</h3>

                            <div class="box-tools pull-right">
                                <span data-toggle="tooltip" title="" class="badge bg-yellow" data-original-title="3 New Messages">3</span>
                            </div>
                        </div>
                        <div class="box-body chat" id="chat-box">
                            <!-- chat item -->
                            <div class="item">
                                <img src="/assets/admin/img/user4-128x128.jpg" alt="user image" class="online">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 2:15</small>
                                        Mike Doe
                                    </a>
                                    Hồ sơ #1 Không cấp visa cho quốc gia này được
                                </p>
                                <!-- /.attachment -->
                            </div>
                            <!-- /.item -->
                            <!-- chat item -->
                            <div class="item">
                                <img src="/assets/admin/img/user3-128x128.jpg" alt="user image" class="offline">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:15</small>
                                        Alexander Pierce
                                    </a>
                                    Hồ sơ #2 cần bỏ sung hình ảnh cmnd
                                </p>
                            </div>
                            <!-- /.item -->
                            <!-- chat item -->
                            <div class="item">
                                <img src="/assets/admin/img/user2-160x160.jpg" alt="user image" class="offline">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                                        Susan Doe
                                    </a>
                                    Từ nay cho đến 2019 Không cấp visa cho quốc gia "American Samoa"
                                </p>
                            </div>
                            <div class="item">
                                <img src="/assets/admin/img/user2-160x160.jpg" alt="user image" class="offline">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                                        Susan Doe
                                    </a>
                                    Từ nay cho đến 2019 Không cấp visa cho quốc gia "American Samoa"
                                </p>
                            </div>
                            <div class="item">
                                <img src="/assets/admin/img/user2-160x160.jpg" alt="user image" class="offline">

                                <p class="message">
                                    <a href="#" class="name">
                                        <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> 5:30</small>
                                        Susan Doe
                                    </a>
                                    Từ nay cho đến 2019 Không cấp visa cho quốc gia "American Samoa"
                                </p>
                            </div>
                            <!-- /.item -->
                        </div>
                        <!-- /.chat -->
                        <div class="box-footer">
                            <div class="input-group">
                                <input class="form-control" placeholder="Type message...">

                                <div class="input-group-btn">
                                    <button type="button" class="btn btn-success"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box (chat box) -->

                </div>
            </div>
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">
                        <button type="submit" value="0" name="submit" class="btn btn-info">Lưu lại</button>
                        <button type="submit" value="1" name="submit" class="btn btn-success"><i class="fa fa-repeat"></i> Chuyển hồ sơ</button>
                        <div class="box-tools pull-right">
                            <span>agency: <b><?php echo $order['agency']; ?></b></span>
                            <span>source: <b><?php echo $order['source']; ?></b></span>
                            <span>campaign: <b><?php echo $order['campaign']; ?></b></span>
                        </div> 
                    </div>
                </div>
            </div>
        </form>
    </section>
    <div class="hidden" id="gender_data">
        <select class="gender">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
    </div>
    <div class="hidden" id="full_name_data">
        <input type="text" class="full_name in_put">
    </div>
    <div class="hidden" id="date_of_birth_data">
        <div class="input-group field-date-data">
            <input type='text' class='date_of_birth in_put'/>
                <span class="fa fa-calendar"></span>
        </div>
    </div>
    <div class="hidden" id="nationality_data">
        <select class="nationality">
            <option value="" selected="selected">Select...</option>
            <?php foreach ($country as $c): ?>
                <option value="<?php echo $c['name']; ?>" rel="<?php echo $c['special_fee']; ?>" note='<?php echo $c['note']; ?>'><?php echo $c['name']; ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="hidden" id="passport_number_data">
        <input type="text" class="passport_number in_put">
    </div>
</div>
<link rel="stylesheet" href="/assets/admin/plugins/datepicker/datepicker3.css">
<script src="/assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
    var arrival_date = '<?php echo date('d/m/Y', strtotime($order['arrival_date'])); ?>';
    $('#datetimepicker').datepicker({autoclose: true});
    $('#datetimepicker').datepicker('update', arrival_date);
    $('#chat-box').slimScroll({
        height: '220px'
    });
    var html_old;
    var check = 0;
    $('#passport').on('click','.fa-edit',function () {
        if(check == 1)
            return false;
        check = 1;
        var parent = $(this).parent();
        html_old = parent.html();
        var id = $(this).attr('rel');
        var w = parent.width();
        var text = $('span', parent).html();
        var _this = $('.' + id);
        _this.css('width',w);
        console.log(_this);
        if(_this.hasClass('in_put')){
            _this.attr('placeholder',text);
        }else{
//            console.log(text);
//            console.log('.' + id+' option[value="'+text+'"]');
            $('.' + id+' option[value="'+text+'"]').prop('checked', true);
        }
        
        var html = $('#' + id + '_data').html();
        html += '<i class="fa fa-times-circle fa-lg"></i>';
        // var html = '<input class="edit_ele" style="width:' + w + 'px" placeholder="' + text + '"><i class="fa fa-times-circle fa-lg"></i>';
        parent.html(html);
        
        $('.fa-times-circle').click(function (parent) {
            check = 0;
            var parent = $(this).parent();
            parent.html(html_old);
        })
    })
</script>
<script src="/assets/admin/js/order.js"></script>
<style>
    .box-tools span{
        display: inline-block;
        padding: 2px 10px;
        font-size: 20px;
    }
    .box-tools span b{
        color: red
    }
    #passport td{
        position: relative;
        padding-right: 30px;

    }
    #passport input{
        padding: 0 5px;
    }
    #passport i{
        margin: 0 3px;
        cursor: pointer;
        color: #ccc;
    }
    #passport i:hover{
        color: red
    }
    .fa-save{
        display: none;
    }
    #passport .fa-edit{
        position: absolute;
        top: 12px;
        right: 5px;
        display: none;
    }
    #passport .fa-times-circle{
        position: absolute;
        top: 12px;
        right: 5px;
    }
    #passport td:hover .fa-edit{
        display: block
    }
    .field-date-data .fa-calendar{
        position: absolute;
        right: 5px;
        top: 5px;
    }
    .date_of_birth{
        padding-right: 20px;
    }
    .actions{
        padding-right: 0px !important;
    }
</style>