<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM HỒ SƠ XỬ LÝ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>customer">Quản lý khách hàng</a></li>
            <li class="active">thêm khách hàng</li>
        </ol>

    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-12 margin-bottom">
                    <div class="box-footer">
                        <button type="submit" value="0" name="submit" class="btn btn-info">Lưu khách hàng</button>
                        <button type="submit" value="1" name="submit" class="btn btn-success  margin-r-5">Lưu & In hóa đơn</button>
                        <a href="invoice-print.html" target="_blank" class="btn btn-default margin-r-5"><i class="fa fa-print"></i> Print tất cả</a>
                        <a href="" class="btn btn-warning pull-right margin-r-5" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"  title="Thêm hồ sơ"></i> Thêm hồ sơ </a>

                    </div>
                </div>
                <?php if ($check_error == 0): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                        Thêm thành công
                    </div>
                <?php endif; ?>
                <?php if ($check_error == 1): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                        <?php echo validation_errors(); ?>                          
                    </div>
                <?php endif; ?>
                 
                <div class="col-md-6">   

                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <i class="fa fa-cc-visa"></i>
                            <h3 class="box-title">Thông tin khách hàng</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group col-md-6 nopadding">
                                <label for="full_name">Tên khách hàng</label>
                                <input type="text" class="form-control" name="full_name" placeholder="Tên khách hàng">
                            </div>

                            <div class="form-group col-md-6 nopadding">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" class="form-control" name="phone" placeholder="Số điện thoại khách hàng">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="email">Email</label>
                                <input type="text" class="form-control"  name="email" placeholder="Nhập email liên hệ của khách">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="address">Địa chỉ</label>
                                <input type="text" class="form-control" name="address" placeholder="Địa chỉ khách hàng">
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="company">Tên Cty/ Tổ chức</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="company"/>
                                    <label class="input-group-addon btn" for="company">
                                        <span class="fa fa-home"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6 nopadding">
                                <label for="vat">Mã số thuế</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="vat"/>
                                    <label class="input-group-addon btn" for="vat">
                                        <span class="fa fa-line-chart"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12 nopadding">
                                <label for="leave_message">Ghi chú khách hàng</label>
                                <textarea name="leave_message" style="width: 100%; height: 70px" class="form-control"></textarea>
                            </div>



                        </div>
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">
                        <div class="box-body">
                            <div class="form-group col-md-8 nopadding">
                                <label for="who_consultant">NV tư vấn</label>
                                <select name="who_consultant" class="form-control valid">
                                    <option value="">Chọn nhân viên tư vấn</option>
                                    <?php foreach ($staff as $value): ?>
                                        <option value="<?php echo $value['username']; ?>"><?php echo $value['fullname']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="pax">Số lượng hồ sơ</label>
                                <div class="input-group">
                                    <input type='text' class="form-control" name="pax"/>
                                    <label class="input-group-addon btn" for="pax">
                                        <span class="fa fa-user"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-12 nopadding add_product">
                                <label for="pax">Nhân viên xử lý</label>
                                <input type="hidden" name="who_handle" id="who_handle">
                                <input class="form-control search_input" placeholder="Tìm nhân viên" name="q" type="text" />
                                <ul class="list_item search-results"></ul>
                                <ul class="list_item results">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="box" id="list_cost">
                                <div class="box-header with-border">
                                    <i class="fa fa-usd"></i>
                                    <h3 class="box-title">Chi tiết thanh toán</h3>
                                    <button class="btn btn-default pull-right add_note btn-xs"><i class="fa fa-plus"></i> Thêm chi phí</button>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div class="box-group" id="accordion">
                                        Updating ...
                                    </div>
                                </div>
                            </div>
                </div>
                <div class="col-md-12"> 

                    <div class="box box-warning">

                        <!-- /.box-header -->
                        <div class="row">
                            <section class="invoice">
                                <h2 class="page-header">
                                    <i class="fa fa-user"></i> Lê Hồng Thanh 
                                    <small class="pull-right">
                                        <button type="button" class="btn btn-primary btn-xs margin-r-5">
                                            <i class="fa fa-question-circle"></i> Xem hướng dẫn
                                        </button>
                                        <a href="invoice-print.html" target="_blank" class="btn btn-default btn-xs margin-r-5"><i class="fa fa-print"></i> In hồ sơ</a>
                                        <button type="button" class="btn btn-warning btn-xs ">
                                            <i class="fa fa-edit"></i> Cập nhật thông tin
                                        </button>
                                    </small>
                                    <span class="lead">Visa Nhật (Du Lịch - 3 tháng 1 lần):</span>
                                </h2>
                          
                                    

                          
                                <div class="invoice-info row">
                                    <div class="col-md-6 nopadding">
                                        <div class="col-md-4"><b>Giới tính:</b> Nữ</div>
                                        <div class="col-md-4"><b>Ngày sinh:</b> 10/06/2011</div>
                                        <div class="col-md-4"><b>Nơi sinh:</b> TP.HCM</div>
                                        <div class="col-md-4"><b>Số CMND:</b> 023318896</div>
                                        <div class="col-md-4"><b>Ngày cấp:</b> 10/06/2011</div>
                                        <div class="col-md-4"><b>Nơi cấp:</b> Lâm Đồng</div>
                                        <div class="col-md-4"><b>Dân tộc:</b> Kinh</div>
                                        <div class="col-md-4"><b>Tôn giáo:</b> Phật</div>
                                        <div class="col-md-4"><b>Quốc tịch:</b> VN</div>
                                        <div class="col-md-4"><b>Số hộ chiếu:</b> BCD098776</div>
                                        <div class="col-md-4"><b>Ngày Cấp:</b> 10/06/2011</div>
                                         <div class="col-md-4"><b>Ngày EXP:</b> 10/06/2011</div>
                                        <div class="col-md-12"><b>Địa chỉ thường trú:</b> 24/9A4 THỐNG NHẤT, Phường 16, Quận Gò Vấp,TP. Hồ Chi Minh</div>
                                        <div class="col-md-12"><b>Địa chỉ tạm trú:</b> 24/9A4 THỐNG NHẤT, Phường 16, Quận Gò Vấp,TP. Hồ Chi Minh</div>
                                        <div class="col-md-12"><b>Tên cơ quan:</b> Cty giấy phát sáng</div>
                                        <div class="col-md-12"><b>Địa chỉ cơ quan:</b> 24/9A4 THỐNG NHẤT, Phường 16, Quận Gò Vấp,TP. Hồ Chi Minh</div>
                                    </div>




                                    <div class="col-md-6 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Họ và tên gia đình</th>
                                                    <th>Ngày sinh</th>
                                                    <th>Quan hệ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Lê Hồng Hà</td>
                                                    <td>10/06/2011</td>
                                                    <td>Cha</td>
                                                </tr>
                                                <tr>
                                                    <td>Trần Thị Phấn</td>
                                                    <td>10/06/2011</td>
                                                    <td>Mẹ</td>
                                                </tr>
                                                <tr>
                                                    <td>Ngô Thừa Ân</td>
                                                    <td>10/06/2011</td>
                                                    <td>Chồng</td>
                                                </tr>
                                                <tr>
                                                    <td>Ngồ Gia Tịnh</td>
                                                    <td>10/06/2011</td>
                                                    <td>Con</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                            Không có ghi chú nào cho hồ sơ này.
                                        </p>
                                    </div>

                                    <!-- /.col -->
                                </div>
                               
                            </section>


                        </div>
                        <!-- /.box-body -->
                       
                    </div>
                     <div class="box-footer">
             
                            <button type="submit" value="0" name="submit" class="btn btn-info">Lưu khách hàng</button>
                            <button type="submit" value="1" name="submit" class="btn btn-success margin-r-5">Lưu & In hóa đơn</button>
                            <a href="invoice-print.html" target="_blank" class="btn btn-default margin-r-5"><i class="fa fa-print"></i> Print tất cả</a>
                            <a href="" class="btn btn-warning pull-right margin-r-5" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"  title="Thêm hồ sơ"></i> Thêm hồ sơ </a>
                           
                        </div>
                </div>

            </div>
        </form>
    </section>

</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="popup_control">

        </div>
    </div>
</div>
<script>
    var network_id = <?php echo $user['network_id']; ?>;
    $(".search_input").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/admincp/admin/search",
                data: {q: request.term, m: 'complete', n: network_id},
                success: function (result) {
                    console.log(result);
                    result = JSON.parse(result);
                    console.log(result);
                    if (typeof result !== 'undefined' && result.length > 0)
                    {
                        end_key = (result.length > 6) ? 6 : result.length;
                        var inner_html = "";
                        for (key = 0; key < end_key; key++) {
//                            var price_compare = (result[key].price_compare == 0) ? "" : result[key].price_compare;
                            inner_html += '<li rel=' + result[key].username + '>\n\
                        <img class="thumbnail" src="/assets/upload/avatar/' + result[key].avatar + '">\n\
                        <p style="float:left"><span class="title">' + result[key].username + '</span><br>\n\
\n\                     <span style="color:red">Nhóm: ' + result[key].department_name + '</span>&nbsp;&nbsp;|&nbsp;&nbsp;<span style="color:#777">Danh hiệu: ' + result[key].level_department_name + '</span></p>\n\
                        <p class="del_product_id">Xóa</p></li>';
                        }
                        $('.search-results').html(inner_html).fadeIn();
                    } else {
                        $('.search-results').fadeOut();
                    }

                },
                error: function () {
                    response([]);
                }
            });
        },
        minLength: 0
    })
    $(".search_input").click(function () {
        $(".search_input").autocomplete("search", "");
    });
    $(".search_input").focusout(function () {
        $('.search-results').fadeOut();
    });
    $("div.add_product").on('click', '.search-results li', function () {
        var handle = $(this).attr('rel');
        var who_handle = $('#who_handle').val();
        var n = who_handle.indexOf(handle);
        if (n == -1) {
            if (who_handle == "") {
                $('#who_handle').val(handle);
            } else {
                $('#who_handle').val(who_handle + ',' + handle);
            }
            $('.results').append($(this));
        }
        $(".search_input").val('');
    });
    $("div.add_product").on('click', '.del_product_id', function () {
        $(this).parent().remove();
        var who_handle = "";
        $('.results li').each(function () {
            if (who_handle == "")
                who_handle = $(this).attr('rel');
            else
                who_handle = who_handle + ',' + $(this).attr('rel');
        })
        $('#who_handle').val(who_handle);
    });
    $('body').on('click', '.handle_department', function () {
//        var id = $(this).attr('rel_id');
//        $.post('/<?php echo ADMIN_URL; ?>task/handle_department', {id: id, network_id: network_id}, function (results) {
//            $('#popup_control').html(results);
//        });
        $("#myModal").modal("show");
    })
</script>
<style>
    .invoice{
        padding-top: 0px;
    }
    .kc{
        display: inline-block;
        width: 40px;
        height: 16px;
    }
    .invoice-info div{

        padding-bottom: 5px;
        margin-bottom: 5px;
    }
    #total_fees{
        color: red;
        font-size: 22px;
    }
</style>