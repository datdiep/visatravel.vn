<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>Loại dịch vụ</th>
                    <th>Khách hàng</th>
                    <th>SL hồ sơ</th>
                    <?php if (!empty($user['role_network']) || !empty($user['role_department'])): ?>
                        <th>Tổng tiền</th>
                    <?php endif; ?>
                    <th>Tiến trình</th>
                    <th>NV tư vấn</th>
                    <th>NV xử lý</th>
                    <th>Lời nhắn</th>
                    <th style="text-align: center">Thực thi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo @$map_services[$item['services']]; ?></td>
                        <td><a href="/admincp/order/process_view/<?php echo $item['id']; ?>"><?php echo $item['full_name']; ?></a></td>                     
                        <td><?php echo $item['pax']; ?></td>                       
                        <?php if (!empty($user['role_network']) || !empty($user['role_department'])): ?>
                            <td><?php echo number_format($item['total_fees']); ?></td>
                        <?php endif; ?>
                        <td>
                            <?php echo $order_status[$item['status']]; ?>
                        </td>
                        <td><?php echo $item['who_consultant']; ?> </td>
                        <td><?php echo $item['who_handle']; ?> </td>

                        <td><?php echo nl2br($item['leave_message']); ?></td>
                        <td style="text-align: center">
                            <?php if ($item['status'] == 'trash'): ?>
                                <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Undo</a>
                                <a  class="btn btn-default" onclick="trash_empty(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Empty</a>
                            <?php else:?>
                                <a  class="btn btn-default" href="visa-outbound/handle/<?php echo $item['id']; ?>"><i class="fa fa-edit"></i>  Xử lý</a>
                                <a  class="btn btn-default" onclick="trash(<?php echo $item['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Xóa</a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>      


        </table>

    </div>
    <div class="col-md-4 col-md-offset-5">
        <ul class="pagination">
            <?php echo $links; ?>
        </ul>
    </div>

</div>

