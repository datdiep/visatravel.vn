<div class="content-wrapper"> 

    <section class="content">
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>training">Đào tạo</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>training/lesson/<?php echo $training_cat['id']; ?>"><?php echo $training_cat['name']; ?></a></li>
            <?php if (@$chapter['id']): ?>
                <li class="active"><a href="/<?php echo ADMIN_URL; ?>training/chapter/<?php echo $chapter['id']; ?>"><?php echo $chapter['name']; ?></a></li>
            <?php endif; ?>
        </ol>
        <div class="row">
            <div class="col-md-9">
                <form id="form" method="post" enctype="multipart/form-data">
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>

                    <div class="row">            
                        <div class="col-md-12">   
                            <div class="box box-primary box-success">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Thêm nội dung khóa học : <?php echo $training_cat['name']; ?> </h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-md-12 nopadding"> 
                                        <div class="form-group col-md-8 nopadding">
                                            <label for="name">Tên chương</label>
                                            <input type="text" class="form-control" id="name" name="name" value="<?php echo @$chapter['name'] ?>" placeholder="Nhập tiêu đề">
                                            <input type="hidden" class="form-control" name="cat_id" value="<?php echo $training_cat['id']; ?>">
                                            <?php if (@$chapter['id']): ?>
                                                <input type="hidden" class="form-control" name="post_type" value="edit">
                                            <?php else: ?>
                                                <input type="hidden" class="form-control" name="post_type" value="add">
                                            <?php endif; ?>

                                        </div>
                                        <div class="form-group col-md-4 nopadding">
                                            <label for="alias">Status</label>
                                            <select name="status" class="form-control valid">
                                                <?php foreach ($status_update as $value): ?>
                                                    <option <?php if (@$chapter['status'] == $value) echo 'selected'; ?> value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                    </div>

                                </div>

                            </div> 
                        </div>

                        <div  class="col-md-12">   
                            <div class="box box-primary box-success">
                                <div class="box-header">
                                    <h3 class="box-title">Nội dung chương</h3>
                                </div>
                                <div class="box-body">
                                    <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"><?php echo @$chapter['content'] ?></textarea>
                                </div>
                                <div class="box-footer">
                                    <button type="submit" value="1" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Lưu lại</button>
                                    <button type="submit" value="2" name="submit" class="btn btn-success"><i class="fa fa-edit"></i> Lưu & tiếp tục</button>
                                </div>
                            </div>

                        </div>               
                    </div>
                </form>


            </div>
            <div class="col-md-3">
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $training_cat['name']; ?></h3>
                        </div>
                        <div class="box-body box-profile">

                            <img src="/assets/upload/training/<?php echo $training_cat['image']; ?>" class="img-responsive" alt="<?php echo $training_cat['name']; ?>"/>

                            <ul class="list-group list-group-unbordered">

                                <li class="list-group-item">
                                    <b>Giáo viên</b> <a class="pull-right"><?php echo $training_cat['user_create']; ?></a>
                                </li>
                            </ul>

                            <?php foreach ($lesson as $k => $value): ?>
                                <a href="/admincp/training/chapter/<?php echo $value['id']; ?>" class="btn btn-primary btn-block txt-left" <?php if (@$chapter['id'] == $value['id']) echo 'style="background-color:#3cbc3e"'; ?>><?php echo $value['name']; ?></a>
                            <?php endforeach; ?>

                        </div>
                        <!-- /.box-body -->
                    </div>


                </div>
            </div>

            <!-- /.col -->
        </div>
    </section>
</div><!-- /.row -->

<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>