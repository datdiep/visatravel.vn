<div class="content-wrapper">    
    <section class="content">

        <ol class="breadcrumb">
            <li><i class="fa fa-mortar-board"></i> <a href="/<?php echo ADMIN_URL; ?>training">Đào tạo</a></li>
            <li class="active"><a href="/<?php echo ADMIN_URL; ?>training/lesson/<?php echo @$lesson['id']; ?>"><?php echo @$lesson['name']; ?></a></li>
        </ol>

        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>

            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">


                        <div class="box-body">

                            <div class="form-group">
                                <label for="title">Tên khóa học</label>
                                <input type="text" class="form-control" id="title" name="name" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" value="<?php echo @$lesson['name']; ?>">
                                <input type="hidden" class="form-control" id="alias" name="alias" value="<?php echo @$lesson['alias']; ?>">
                                <?php if (@$lesson['id']): ?>
                                    <input type="hidden" class="form-control" name="post_type" value="edit">
                                <?php else: ?>
                                    <input type="hidden" class="form-control" name="post_type" value="add">
                                <?php endif; ?>
                            </div>

                            <div class="form-group">
                                <label for="category_news"> Danh mục</label>
                                <select class="form-control" name="parent">
                                    <?php foreach ($list_cat as $cate) : ?>
                                        <option <?php if (@$lesson['parent'] == $cate['id']) echo 'selected="selected"' ?> value="<?php echo $cate['id'] ?>"><?php echo $cate['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>

                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">
                            <div class="box box-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-aqua-active" style="background: url('https://adminlte.io/themes/AdminLTE/dist/img/photo1.png') center;">
                                    <h3 class="widget-user-username"><?php echo $user['fullname']; ?></h3>
                                    <h5 class="widget-user-desc">Nhân viên gương mẫu</h5>
                                </div>

                                <div class="box-footer">
                                    <div class="row">
                                        <div class="col-sm-12 border-right">
                                            <div class="description-block">
                                                <h5 class="description-header">Thêm khóa học</h5>
                                                <span class="description-text">SALES</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>

                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" value="<?php echo @$training_cat['tags']; ?>" placeholder="tag1,tags2,tags3">
                            </div>
                        </div>                            

                    </div>  
                </div>

                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu khóa học</h3>
                        </div>  
                        <textarea id="textarea_description" name="description" style="width: 100%; height: 200px"><?php echo @$lesson['description']; ?></textarea>
                    </div>

                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                        <button type="submit" value="2" name="submit" class="btn btn-success">Lưu & Edit</button>
                    </div>
                </div>

            </div>

        </form>
    </section>

</div>
<script>
<?php if ($lesson['image']): ?>
        crop('photo', 292, 190<?php echo $lesson['image'] ? ',"/assets/upload/training/' . $lesson['image'] . '"' : ''; ?>);
<?php else: ?>
        crop('photo', 292, 190);
<?php endif; ?>

    CKEDITOR.config.entities_latin = false;
    var editor1 = CKEDITOR.replace('textarea_description');
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>