<?php $this->load->library('transload'); ?>
<div class="box-body">

    <div class="box-header with-border">
        <h3 class="box-title"><i class="fa fa-mortar-board"></i>Khóa học đang học</h3>

        <div class="box-tools pull-right">
            <div class="has-feedback">
                <?php if (!empty($id)): ?>
                    <a href="/admincp/training/lesson_process/<?php echo $id; ?>/add" class="btn btn-block btn-info btn-xs" title="Thêm bài học">Thêm bài học</a>
                <?php endif; ?>
            </div>
        </div>

        <!-- /.box-tools -->
    </div>
    <div class="box-body row">

        <?php foreach ($results as $item): ?>
            <div class="col-sm-12 col-md-6">
                <div class="box box-solid" style="background-color: #f7f7f7">
                    <div class="box-body">
                        <div class="media">
                            <div class="row">
                                <a href="/admincp/training/lesson/<?php echo $item['id']; ?>" title="<?php echo $item['name']; ?>">
                                    <img src="/assets/upload/training/<?php echo $item['image']; ?>" alt="<?php echo $item['name']; ?>" class="media-object" style="width: 462px; height: 280px">
                                </a>
                            </div>
                            <div class="media-body">
                                <div class="clearfix">
                                    <a href="/admincp/training/lesson/<?php echo $item['id']; ?>" title="<?php echo $item['name']; ?>"><h4><b><?php echo $item['name']; ?></b></h4></a>
                                    <p><?php echo $item['description']; ?></p>
                                    <i class="fa fa-user"></i> Người đăng: <?php echo $item['user_create']; ?>
                                    <span class="pull-right">
                                        <a class="btn btn-default" href="/admincp/training/lesson/<?php echo $item['id']; ?>" title="<?php echo $item['name']; ?>">Học ngay</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>