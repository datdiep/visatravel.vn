<div class="content-wrapper">
    <section class="content">
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>training">Đào tạo</a></li>
            <li class="active"><a href="/<?php echo ADMIN_URL; ?>training/lesson/<?php echo $training_cat['id']; ?>"><?php echo $training_cat['name']; ?></a></li>
        </ol>
        <div class="row">

            <div class="col-md-9">
                <div class="box box-primary box-primary ">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-apple"></i> <?php echo $chapter['name']; ?></h3>

                        <div class="box-tools">
                            <a href="/admincp/training/chapter_process/<?php echo $chapter['id']; ?>/edit" type="button" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                        </div>
                    </div>

                    <div class="box-body">
                        <div class="form-group">
                            <?php echo $chapter['content']; ?>
                        </div> 
                    </div> 
                </div>
            </div>

            <div class="col-md-3">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $training_cat['name']; ?></h3>
                    </div>
                    <div class="box-body box-profile">

                        <img src="/assets/upload/training/<?php echo $training_cat['image']; ?>" class="img-responsive" alt="<?php echo $training_cat['name']; ?>"/>

                        <ul class="list-group list-group-unbordered">

                            <li class="list-group-item">
                                <b>Giáo viên</b> <a class="pull-right"><?php echo $training_cat['user_create']; ?></a>
                            </li>
                        </ul>

                        <?php foreach ($chapter_list as $k => $value): ?>
                            <a href="/admincp/training/chapter/<?php echo $value['id']; ?>" class="btn btn-primary btn-block txt-left" <?php if (@$chapter['id'] == $value['id']) echo 'style="background-color:#3cbc3e"'; ?>><?php echo $value['name']; ?></a>
                        <?php endforeach; ?>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <a href="/admincp/training/chapter_process/<?php echo $training_cat['id']; ?>/add" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Add chapter</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->

                </div>
            </div>
        </div>
    </section>
</div><!-- /.row -->

