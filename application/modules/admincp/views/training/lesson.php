<div class="content-wrapper">
    <section class="content">

        <ol class="breadcrumb">
            <li class="active"><a href="/<?php echo ADMIN_URL; ?>training">Đào tạo</a></li>
        </ol>

        <div class="row">

            <!-- /.col -->
            <div class="col-md-9">
                <div class="box">
                    <div class="box-body">
                        <h4>
                            <i class="fa fa-mortar-board"></i> <?php echo $training_cat['name']; ?>
                            <span class="pull-right">
                                <a href="/admincp/training/lesson_process/<?php echo $training_cat['id']; ?>/edit" class="btn btn-default">
                                    <i class="fa fa-edit"></i> Edit
                                </a>
                                <a class="btn btn-default" onclick="del_lesson(<?php echo $training_cat['id']; ?>)" href="javascript:void(0)"><i class="fa fa-trash-o"></i> Delete</a> 
                            </span>
                        </h4>

                        <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <?php echo $training_cat['description']; ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3">


                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?php echo $training_cat['name']; ?></h3>

                    </div>
                    <div class="box-body box-profile">

                        <img src="/assets/upload/training/<?php echo $training_cat['image']; ?>" class="img-responsive" alt="<?php echo $training_cat['name']; ?>"/>

                        <ul class="list-group list-group-unbordered">

                            <li class="list-group-item">
                                <b>Giáo viên</b> <a class="pull-right"><?php echo $training_cat['user_create']; ?></a>
                            </li>
                        </ul>

                        <?php foreach ($topic as $k => $value): ?>
                            <a href="/admincp/training/chapter/<?php echo $value['id']; ?>" class="btn btn-primary btn-block txt-left"><?php echo $value['name']; ?></a>
                        <?php endforeach; ?>
                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <a href="/admincp/training/chapter_process/10/add" class="btn btn-default btn-xs"><i class="fa fa-plus"></i> Thêm bài học</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.box-body -->

                </div>
                <!-- /.col -->
            </div>
    </section>
</div><!-- /.row -->

<script>
    function del_lesson(id) {
        show_dialog('Bạn có chắc xóa bài học ID =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>training/lesson_del', {id: id}, function (results) {
                location.href = '/<?php echo ADMIN_URL; ?>training/';
            });
        });
    }

</script>