<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->

    <section class="content">

        <ol class="breadcrumb">
            <li class="active"><a href="/<?php echo ADMIN_URL; ?>training">Đào tạo</a></li>
        </ol>

        <div class="row">

            <!-- /.col -->
            <div class="col-md-9">
                <div class="box" id="list_ajax">

                </div>
            </div>
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="box box-primary">
                    <div class="box-body box-profile">
                        <?php if ($user['avatar']): ?>
                            <img src="/assets/upload/avatar/<?php echo $user['avatar']; ?>" class="profile-user-img img-responsive img-circle" alt="<?php echo $user['username']; ?>"/>
                        <?php endif; ?>

                        <h3 class="profile-username text-center">Lê Hồng Sơn</h3>

                        <p class="text-muted text-center">Lính mới</p>

                        <ul class="list-group list-group-unbordered">
                            <li class="list-group-item">
                                <b>Ngày gia nhập</b> <a class="pull-right"><?php echo $user['date_create']; ?></a>
                            </li>
                            <li class="list-group-item">
                                <i class="fa fa-book margin-r-5"></i><b>Kỹ năng</b> <a class="pull-right">13,287</a>
                            </li>
                            <li class="list-group-item">
                                <b>Bậc nghề</b> <a class="pull-right">Level <?php echo $user['level']; ?></a>
                            </li>
                        </ul>
                        <p>
                            <span class="label label-danger">Visa úc</span>
                            <span class="label label-success">Visa Mỹ</span>
                            <span class="label label-info">Visa Đài Loan</span>
                            <span class="label label-danger">Visa Nhật</span>
                        </p>

                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

                <!-- About Me Box -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Các khóa đạo tạo</h3>
                        <button class="btn btn-default pull-right add_note btn-xs"><i class="fa fa-plus"></i> Thêm danh mục</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php foreach ($training_cat as $k => $value): ?>
                            <a rel_id="<?php echo $value['id']; ?>" class="btn btn-primary btn-block training_cat"><?php echo $value['name']; ?></a>
                        <?php endforeach; ?>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
    </section>
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Bạn có muốn xóa trường hợp id =' + id + ' không', function () {
            $.post('/<?php echo ADMIN_URL; ?>checklist/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }

    var current_pos = <?php echo $pos; ?>;
    function loadlist(id) {
        $.post('/<?php echo ADMIN_URL; ?>training/load', {id: id}, function (results) {
            $('#list_ajax').html(results);
        });
    }

    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    $('.training_cat').click(function () {
        var id = $(this).attr('rel_id');
        loadlist(id);
    });
</script>