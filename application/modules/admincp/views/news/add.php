<div class="content-wrapper">    
    <section class="content-header">
        <h1>ADD NEWS</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">List news</a></li>
            <li class="active">Add News</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">
                        <?php if ($user['publish'] == 1 || $user['manager_editor']): ?>
                            <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                        <?php else : ?>
                            <button type="submit" value="2" name="submit" class="btn btn-success">Đợi duyệt</button>
                        <?php endif; ?>
                        <button type="submit" value="3" name="submit" class="btn btn-info">Lưu nháp</button>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin cơ bản</h3>
                        </div>

                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tiêu đề</label>
                                <input type="text" class="form-control" id="title" name="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                            </div>
                            <div class="form-group">
                                <label for="category_news"> Chọn thể loại</label>
                                <select class="form-control" name="cate_id" id="cate_id">
                                    <?php foreach ($list_cate as $cate) : ?>
                                        <option value="<?php echo $cate['id'] ?>"><?php echo $cate['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>

                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">

                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword">
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" placeholder="tag1,tags2,tags3">
                            </div>

                            <div class="form-group">
                                <label for="schedule">Lịch đăng bài</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="schedule" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>   


                        </div>                            

                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">    
                        <div class="box-header">
                            <h3 class="box-title">Thêm tin tức liên quan</h3>
                        </div>
                        <div class="box-body add_product" style="position: relative">
                            <input type="hidden" name="news_ids" class="news_ids" value=",">
                            <input class="form-control search_input" placeholder="Tìm sản phẩm liên quan" name="q" type="text" />
                            <ul class="list_item search-results"></ul>
                            <ul class="list_item results"></ul>
                        </div>                            

                    </div>  
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu</h3>
                        </div>  
                        <textarea id="textarea_description" name="description" style="width: 100%; height: 200px"></textarea>
                    </div>
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Nội dung chi tiết</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"></textarea>
                    </div>
                    <div class="box-footer">

                        <?php if ($user['publish'] == 1 || $user['manager_editor']): ?>
                            <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                        <?php else : ?>
                            <button type="submit" value="2" name="submit" class="btn btn-success">Đợi duyệt</button>
                        <?php endif; ?>
                        <button type="submit" value="3" name="submit" class="btn btn-info">Lưu nháp</button>
                    </div>
                </div>

            </div>

        </form>
    </section>

</div>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>
<script>
    crop('photo', 230, 150);
    CKEDITOR.config.entities_latin = false;
    var editor1 = CKEDITOR.replace('textarea_description');
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })
</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
<!-- Select2 -->
<script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
