
<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT NEWS</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">List news</a></li>
            <li class="active">Edit news</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php endif; ?>
            <?php if ($show_button == 1) { ?>
                <div class="row" style="margin-bottom:20px">
                    <div class="col-md-12">
                        <div class="box-footer">

                            <button type="submit" value="-1" name="submit" class="btn btn-info">Tái xuất bản bài viết</button>

                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Basic info</h3>
                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="<?php echo $tmp_news['title']; ?>" class="form-control" name="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" placeholder="Enter title">
                                </br>                               
                                <?php if (strcmp($tmp_news['title'], $news['title']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['title'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" value="<?php echo $tmp_news['alias']; ?>" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                                <?php if (strcmp($tmp_news['alias'], $news['alias']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['alias'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            </br>
                            <div class="form-group">
                                <label for="category_news"> Danh mục mới</label>
                                <select class="form-control" name="cate_id">
                                    <?php foreach ($list_cate as $cate) : ?>
                                        <option value="<?php echo $cate['id'] ?>" <?php echo $tmp_news['cate_id'] == $cate['id'] ? 'selected' : '' ?>><?php echo $cate['title'] ?></option>
                                    <?php endforeach; ?>
                                </select>

                                <label for="category_news"> Danh mục cũ</label>
                                <?php
                                foreach ($list_cate as $cate) :
                                    if ($news['cate_id'] == $cate['id']) {
                                        ?>
                                        <p> <?php echo $cate['title'] ?></p>
                                    <?php } endforeach; ?>
                            </div>

                            <div class="form-group">
                                <label for="image">Hình thumb</label>
                                <input id="photo" name="image" type="file" accept="image/*" /><br/>                                
                            </div>

                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">

                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title" value="<?php echo $tmp_news['seo_title'] ?>">
                                </br>
                                <?php if (strcmp($tmp_news['seo_title'], $news['seo_title']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['seo_title'] ?>" class="form-control" disabled>
                                <?php endif; ?>


                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description" value="<?php echo $tmp_news['meta_description'] ?>">
                                </br>
                                <?php if (strcmp($tmp_news['meta_description'], $news['meta_description']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['meta_description'] ?>" class="form-control" disabled>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword" value="<?php echo $tmp_news['meta_keyword'] ?>">
                                </br>
                                <?php if (strcmp($tmp_news['meta_keyword'], $news['meta_keyword']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['meta_keyword'] ?>" class="form-control" disabled>
                                <?php endif; ?>

                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" value="<?php echo $tmp_news['tags'] ?>">
                                </br>
                                <?php if (strcmp($tmp_news['tags'], $news['tags']) != 0) : ?>
                                    <input type="text" value="<?php echo "OLD : " . $news['tags'] ?>" class="form-control" disabled>
                                <?php endif; ?>

                            </div>

                            <div class="form-group">
                                <label for="schedule">Lịch đăng bài</label>
                                <div class="input-group field-date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" name="schedule" value="<?php echo strtotime($tmp_news['schedule']) != '' ? date('d/m/Y', strtotime($tmp_news['schedule'])) : "" ?>" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask>
                                </div>
                            </div>   
                        </div>                            

                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">    
                        <div class="box-header">
                            <h3 class="box-title">Thêm bài viết liên quan</h3>
                        </div>
                        <div class="box-body add_product" style="position: relative">
                            <input type="hidden" name="news_ids" class="news_ids" value="<?php echo $tmp_news['news_ids']; ?>">
                            <input class="form-control search_input" placeholder="Tìm bài viết liên quan" name="q" type="text" />
                            <ul class="list_item search-results"></ul>
                            <ul class="list_item results">
                                <?php if (!empty($link_news)): ?>
                                    <?php foreach ($link_news as $item): ?>
                                        <li rel="<?php echo $item['id']; ?>">
                                            <img class="thumbnail" src="/assets/upload/page/<?php echo $item['image']; ?>">
                                            <p style="float:left"><span class="title"><?php echo $item['title']; ?></span>
                                            </p><p class="del_product_id">Xóa</p>
                                        </li>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </ul>
                        </div>                            

                    </div>  
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Giới thiệu</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="description_content" name="description"><?php echo $tmp_news['description']; ?></textarea>
                        </br>
                        <?php if (strcmp($tmp_news['description'], $news['description']) != 0) : ?>
                            <div style="background-color:#fcfcfc" class="form-group" ><?php echo "OLD : " . $news['description'] ?></div>
                        <?php endif; ?>

                        <div class="box-header">
                            <h3 class="box-title">Content</h3>
                        </div>  
                        <textarea style="width: 100%;height: 150px;" id="textarea_content" name="content"><?php echo $tmp_news['content']; ?></textarea>
                        </br>
                        <?php if (strcmp($tmp_news['content'], $news['content']) != 0) : ?>
                            <div style="background-color:#fcfcfc" class="form-group"><?php echo "OLD : " . $news['content'] ?></div>
                        <?php endif; ?>
                        <?php if ($show_button == 1) { ?>
                            <div class="box-footer">
                                <button type="submit" value="-1" name="submit" class="btn btn-info">Tái xuất bản bài viết</button>
                            </div>
                        <?php } ?>
                    </div>

                </div>
            </div>

        </form>
    </section>

</div>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>
<script>
    crop('photo', 230, 150<?php echo $tmp_news['image'] ? ',"/assets/upload/page/' . $tmp_news['image'] . '"' : ''; ?>);
    CKEDITOR.config.entities_latin = false;
    var editor1 = CKEDITOR.replace('textarea_content');
    var editor2 = CKEDITOR.replace('description_content');

    CKFinder.setupCKEditor(editor1, '/assets/admin/ckfinder/');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');

</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
<script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
<script>
    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })
</script>