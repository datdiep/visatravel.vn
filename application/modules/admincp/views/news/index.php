<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH SÁCH BÀI VIẾT</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List news</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>news/add"><button style="float:left;margin-right: 10px;width: 150px;" class="btn btn-primary">Đăng tin tức</button></a>
        <input id="keyword" class="form-control" placeholder="Nhập tên bài viết" value="" style="width: 200px;float: left;margin-right: 10px;"/>
        <select class="form-control" id="cat_id" style="width: 150px;float: left;margin-right: 10px;">
            <option value=""> -- Danh mục -- </option>
            <?php foreach ($list_cate as $child) : ?>
                <option <?php echo $cat_id == $child['id'] ? 'selected' : ''; ?> value="<?php echo $child['id'] ?>"><?php echo $child['title']; ?></option>
            <?php endforeach; ?>
        </select>
        <select class="form-control" id="status" style="width: 150px;float: left;margin-right: 10px;">
            <option value=""> -- Trạng thái -- </option>
            <option <?php echo $status == 2 ? 'selected' : ''; ?> value="3">Bản nháp</option>
            <option <?php echo $status == 1 ? 'selected' : ''; ?> value="1">Hoàn chỉnh</option>
            <option <?php echo $status == 3 ? 'selected' : ''; ?> value="2">Chờ duyệt</option>
            <option <?php echo $status == -1 ? 'selected' : ''; ?> value="-1">Tái xuất bản</option>
        </select>
        <select class="form-control" id="order" style="width:  150px;float: left;margin-right: 10px;">
            <option value=""> -- Sắp xếp -- </option>            
            <option value="id DESC" <?php echo $sort == 'id DESC' ? 'selected' : '' ?>>  Mới nhất  </option>            
            <option value="is_hot DESC" <?php echo $sort == 'is_hot DESC' ? 'selected' : '' ?>>Sản phẩm hot</option>                                   
        </select>      
        <?php if ($user['manager_editor'] || $user['root']) : ?>
            <select class="form-control" id="author_username" style="width:  150px;float: left;margin-right: 10px;">
                <option value=""> -- Tác giả -- </option>
                <?php foreach ($list_editor as $editor) : ?>
                    <option value="<?php echo $editor['username'] ?>"> <?php echo $editor['username'] ?>  </option>            
                <?php endforeach; ?>
              
            </select>
        <?php endif; ?>
        <button id="update_hot" style="width: 130px;float: left;margin-right: 10px;" class="btn btn-block btn-primary">Cập nhật hot</button>
    </section>
    <br/><br/>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var cat_id = $('#cat_id').val();
        var keyword = $('#keyword').val();
        var order = $('#order').val();
        var status = $('#status').val();
        var author_username = $('#author_username').val();
        $.post('/<?php echo ADMIN_URL; ?>news/page', {pos: pos, status: status, order: order, cat_id: cat_id, keyword: keyword, author_username: author_username}, function (results) {

            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    function del(id) {
        show_dialog('Bạn có muốn xóa tin tức id = ' + id + " không", function () {
            $.post('/<?php echo ADMIN_URL; ?>news/del', {id: id}, function (results) {
                if (results != "")
                    alert(results);
                loadlist(current_pos);
            });
        });
    }
    $("#update_hot").click(function () {
        var update = [];
        $(".is_hot").each(function () {
            var val = $(this).is(':checked') ? 1 : 0;
            var tr = $(this).parents('tr');
            var is_hide = $('.is_hide', tr).is(':checked') ? 1 : 0;
            update.push({
                'is_hot': val,
                "id": $(this).attr('rel'),
            });
        });
        $.post('/<?php echo ADMIN_URL; ?>news/update_hot', {data: update}, function (results) {
            loadlist(current_pos);
        });
    });
    $('#cat_id').change(function () {
        loadlist(0);
    });
    $('#keyword').keypress(function (e) {
        if (e.keyCode == 13)
            loadlist(0);
    });
    $('#order').change(function () {
        loadlist(0);
    });
    $('#status').change(function () {
        loadlist(0);
    });
    $('#author_username').change(function () {
        loadlist(0);
    });

    function approved(key) {
        $("#chage_icon_" + key).removeClass("fa-hourglass-2");
        $("#chage_icon_" + key).addClass("fa-refresh");
        $.post('/<?php echo ADMIN_URL; ?>news/approved', {id: key, status: "approved"}, function (results) {
            if (results != "") {
                $("#chage_icon_" + key).removeClass("fa-refresh");
                $("#chage_icon_" + key).addClass("fa-lock");
                loadlist(current_pos);
            } else {
                alert("ERROR");
            }
        });

    }

</script>

