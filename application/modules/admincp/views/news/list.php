
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Title</th>
                    <th>Tác giả</th>
                    <th>Image</th>
                    <th>Hot</th>
                    <th>Trạng thái</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td> 
                        <td>
                            <?php if ($item['view_web']): ?>
                                <a target="_blank" href="/<?php echo $item['alias'] . '-' . $item['id'] . '.html'; ?>"><?php echo $item['title']; ?></a>
                            <?php else: ?>
                                <a href="/<?php echo ADMIN_URL . 'news/edit/' . $item['id']; ?>"><?php echo $item['title']; ?></a>
                            <?php endif; ?>
                        </td>      
                        <td><?php echo $item['author_username']; ?></td>      
                        <td><img height="50" src="/assets/upload/news/<?php echo $item['image']; ?>"/></td>
                        <td><input <?php echo $item['is_hot'] ? 'checked' : ''; ?> type="checkbox" class="is_hot" rel="<?php echo $item['id']; ?>"/></td>
                        <td> <?php echo $item['icon'] ?></td> 
                        <td>
                            <a href="/<?php echo $item['edit'] ? ADMIN_URL . 'news/edit/' . $item['id'] : ""; ?>"><?php echo $item['edit'] ? $item['status'] == -1 && $item['author_username'] != $this->data['user']['username'] ? "<i class='fa fa-copy'></i> Duyệt để xuất bản..." : "Edit | " : ""; ?></a>
                            <a onclick="<?php echo $item['del'] ? "del(" . $item['id'] . ")" : "" ?>" href="javascript:void(0)">
                                <?php echo $item['del'] ? "Delete" : "" ?>
                            </a> 
                            <a target="_blank" href="/<?php echo $item['alias'] . '-' . $item['id'] . '.html'; ?>"><?php echo $item['view_web'] ? "<i class='fa fa-eye-slash'></i> View" : ""; ?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      


        </table>

    </div>
</div>
<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
