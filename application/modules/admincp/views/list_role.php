
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>Tên</th>
                    <th>Controller</th>
                    <th>Loại</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr rel_id="<?php echo $item['id'];?>">
                        <td><a href="javascript:void(0)" class="btn_role" rel_id="<?php echo $item['id'];?>"><?php echo $item['name']; ?></a></td>    
                        <td><?php echo $item['controller']; ?></td>
                        <td><i class="fa fa-<?php echo $item['type']; ?>"> <?php echo $map_type[$item['type']]; ?></td>
                        <td>
                            <a href="javascript:void(0)" class="btn_role" rel_id="<?php echo $item['id'];?>">Edit</a> | 
                            <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      


        </table>

    </div>
</div>
<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
