<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $news['title']; ?></h4>
            </div>
            <div class="modal-body">
                <div class="box-body">
                    <?php if ($news['image']): ?>
                        <img src="/assets/upload/news/<?php echo $news['image']; ?>" align='left' style="padding-right: 10px; padding-bottom: 10px">
                    <?php endif; ?> 

                    <?php echo $news['description']; ?>
                    <p><?php echo $news['content']; ?></p>
                </div>
            </div>

            <div class="modal-footer">
                <p class="btn btn-default change_box" data-dismiss="modal" aria-label="Close">Bỏ qua</p>
            </div>
        </div>
    </div> 
</div>