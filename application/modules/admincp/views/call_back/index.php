<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH SÁCH CUỘC GỌI</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List question</li>
        </ol>
        <br>   
        <select class="form-control" id="status" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Trạng thái -- </option>
            <option value="0">Chưa gọi</option>
            <option  value="1">Đã Gọi</option>
        </select>
    </section>

    <br/><br/>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var status = $('#status').val();
        $.post('/<?php echo ADMIN_URL; ?>call_back/page', {pos: pos, status: status}, function (results) {
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    function del(id) {
        show_dialog('Bạn có muốn xóa tin tức id = ' + id + " không", function () {
            $.post('/<?php echo ADMIN_URL; ?>call_back/del', {id: id}, function (results) {
                loadlist(current_pos);
            });
        });
    }
    function update(id) {
        show_dialog('Bạn Đã gọi cho khách id = ' + id + " ?", function () {
            $.post('/<?php echo ADMIN_URL; ?>call_back/update', {id: id}, function (results) {
                loadlist(current_pos);
            });
        });
    }
    $('#status').change(function () {
        loadlist(0);
    });
</script>

