
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Số điện thoại</th>
                    <th>Nội dung</th>
                    <th>Ngày tạo</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td> 
                        <td><?php echo $item['name']; ?></td>      
                        <td><?php echo $item['email']; ?></td>      
                        <td><?php echo $item['phone']; ?></td>      
                        <td><?php echo $item['content']; ?></td>      
                        <td><?php echo date("d-m-Y", strtotime($item['date_create'])) ?></td>      
                        <td>
                            <?php if ($item['status'] == 0) { ?> <a  onclick="update(<?php echo $item['id']; ?>)" href="javascript:void(0)"">Chưa gọi (Cập nhập)</a>
                            <?php } else { ?> Đã gọi<?php } ?> | 
                            <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      


        </table>

    </div>
</div>
<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
