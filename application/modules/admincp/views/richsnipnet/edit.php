<div class="content-wrapper">    
    <section class="content-header">
        <h1>EDIT CATEGORY</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>category">List category</a></li>
            <li class="active">Edit Category</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Sửa thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Basic info</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" value="<?php echo $category_news['title']; ?>" class="form-control" id="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="title" placeholder="Enter Title">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" value="<?php echo $category_news['alias']; ?>" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                            </div>                           
                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" name="seo_title" value="<?php echo $category_news['seo_title']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" name="meta_description" value="<?php echo $category_news['meta_description']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" name="meta_keyword" value="<?php echo $category_news['meta_keyword']; ?>">
                            </div>

                        </div> 
                        <div class="box-body" style="background-color: #ecf0f5; border: #b2b2b2 1px solid">
                            <div class="form-group">
                                <label for="headline1">Tiêu đề lớn (Slide)</label>
                                <input type="text" class="form-control"  name="headline1" value="<?php echo $category_news['headline1']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="headline2">Tiêu đề nhỏ (Slide)</label>
                                <input type="text" class="form-control"  name="headline2" value="<?php echo $category_news['headline2']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="image">Hình ảnh (<label class="label_slider label_gallery">1583 x 300</label>
                                    <label class="label_banner label_gallery" style="display: none;">1100 x 150</label>)</label>
                                <input type="file" class="form-control"  name="image" >

                                <?php if ($category_news['image']): ?>
                                    <br/>
                                    <img src="/assets/upload/cat/<?php echo $category_news['image']; ?>" width="200px"/>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div> 
                </div>
            </div>
        </form>
    </section>

</div>
<script>
    crop('photo', 180, 180);
</script>