<div class="content-wrapper">    
    <section class="content-header">
        <h1>ADD RICHSNIPNET EVENT</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>category">List richsnipnet</a></li>
            <li class="active">Add richsnipnet</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <style>
                        .form-group label span{font-weight: normal!important}
                    </style>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thêm các event sự kiện</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="name">Event name <span>( ❶ ❷ ❸ ❹ ❺ ❻ ❼ ❽ ❾ ❿ ☂ ☔ ✈ ☀ ☼ ☁ ⚡ ⌁ ☇ ☈ ❄ ❅ ❆ ☃ ☉ ☄ ★ ☆ ☽ ☾ ⌛ ⌚ ⌂ ✆ ☎ ☏ ✉ ☑ ✓ ✔ ⎷ ⍻ ✖  ► )</span></label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="❶ Hỏi đáp về Visa Canada">
                            </div>
                            <div class="form-group">
                                <label for="url">URL</label>
                                <input type="text" class="form-control" id="url" name="url" placeholder="https://getvietnamvisa.net/visa-fees/">
                            </div>
                            <div class="form-group">
                                <label for="date">Date <span>(Example: 2017-12-28T09:00)</span></label>
                                <input type="text" class="form-control" id="date" name="date" placeholder="2017-12-28T09:00">
                            </div>
                            <div class="form-group">
                                <label for="Location">Location</label>
                                <input type="text" class="form-control" id="location" name="location" placeholder="► 1 Nguyen Hue Street, ward Ben Nghe, 1 District">
                            </div>
                            <div class="form-group">
                                <label for="url_request">URL request</label>
                                <input type="text" class="form-control" id="url_request" name="url_request" placeholder="/visa-fees">
                            </div>
                        </div>

                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div> 
                </div>

            </div>
        </form>
    </section>

</div>
<script>
    crop('photo', 180, 180);
</script>