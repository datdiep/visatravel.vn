<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>LIST RICHSNIPNET</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List category</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>richsnipnet/add"><button style="width: 200px;" class="btn btn-block btn-primary">Thêm Richsnipnet</button></a>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">
                    <div class="box-body">
                        <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                            <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                <thead>
                                    <tr role="row">
                                        <th>STT</th>
                                        <th>Title</th>                                       
                                        <th>Date</th> 
                                        <th>LOCATION</th> 
                                        <th>URL</th> 
                                        <th>URL REQUEST</th>   
                                        <th>Actions</th>                                                
                                    </tr>
                                </thead>
                                <tbody>    
                                    <?php foreach ($list_richsnipnet as $item): ?>
                                        <tr>
                                            <td><?php echo $item['id']; ?></td> 
                                            <td><a href="/<?php echo ADMIN_URL . 'richsnipnet/edit/' . $item['id']; ?>"><?php echo $item['name']; ?></a></td>                                                 
                                            <td><?php echo $item['date']; ?></td>
                                            <td><?php echo $item['location']; ?></td>
                                            <td><?php echo $item['url']; ?></td>
                                            <td><?php echo $item['url_request']; ?></td>
                                            <td>
                                                <a href="/<?php echo ADMIN_URL . 'richsnipnet/edit/' . $item['id']; ?>">Edit</a> | 
                                                <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>      
                            </table>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
</div><!-- /.row -->

<script>
    function del(id) {
        show_dialog('Do you want to delete category news id =' + id + ' ?', function () {
            $.post('/<?php echo ADMIN_URL; ?>richsnipnet/del', {id: id}, function (results) {
                location.reload();
            });
        });
    }
</script>

