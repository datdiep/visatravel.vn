<div class="content-wrapper">    
    <section class="content-header">
        <h1>Thêm Đối Tác</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>page">List page</a></li>
            <li class="active">Thêm đối tác</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <?php if ($check_error == 1): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                    <?php echo validation_errors(); ?>                          
                </div>
            <?php endif; ?>
            <div class="row"> 

                <div class="col-md-6">   

                    <div class="box box-primary box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin cơ bản </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tên đối tác</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter name">
                            </div>


                            <div class="form-group">
                                <label for="alias">Số điện thoại</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter phone">
                            </div>
                            <div class="form-group">
                                <label for="alias">Email</label>
                                <input type="text" class="form-control" id="email" name="email" placeholder="Enter email">
                            </div>
                            <div class="form-group">
                                <label for="alias">Địa chỉ</label>
                                <input type="text" class="form-control" id="address" name="address" placeholder="Enter address">
                            </div>
                            <div class="form-group">
                                <label for="alias">Website</label>
                                <input type="text" class="form-control" id="website" name="website" placeholder="Enter website">
                            </div>
                            <div class="form-group">
                                <label for="image">Logo</label>
                                <input type="file" class="form-control"  name="image" >

                            </div>
                        </div>

                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">

                        <div class="box-header with-border">
                            <h3 class="box-title">Thiệt lập quản trị</h3>
                        </div>

                        <div class="box-body" id="roles">

                            <div class="form-group">
                                <label for="alias">Hình thức hợp tác</label>
                                <select name="type" class="form-control valid" id="type">
                                    <?php foreach ($type as $value): ?>
                                        <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                    <?php endforeach; ?>
                                </select>

                            </div>

                            <div class="form-group">
                                <label for="roles" class="roles bg-yellow">Quyền hệ thống</label>

                                <div  class="col-md-12" style="background: #fff;margin-bottom: 15px">
                                    <div class="row">
                                        <?php foreach ($role_controller as $k => $v): ?>
                                            <div  class="col-md-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input  type="checkbox" name="role_controller[]" value="<?php echo $v['controller'] ?>"/>

                                                        <?php echo $v['name']; ?>
                                                    </label>
                                                </div>

                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="roles bg-red">Quyền xử lý đơn hàng</label>

                                <div  class="col-md-12" style="background: #fff;">
                                    <div class="row">
                                        <?php foreach ($role_order_progress as $v): ?>
                                            <div  class="col-md-4">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="role_order_progress[]" value="<?php echo $v ?>"/>
                                                        <?php echo $v; ?>
                                                    </label>
                                                </div>

                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin hợp tác</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"></textarea>

                    </div>
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-success">TẠO ĐƠN VỊ</button>

                    </div>
                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    $('#type').change(function () {
        var type = $(this).val();
        auto_check(type);
    })
    var map_roles = {
        'agency': ",admin,order,check,department,",
        'issue visa': ",admin,order,censor,department,",
        'root': "all"
    }
    auto_check('agency');
    function auto_check(type) {
        var roles = map_roles[type];
        $('#roles input[type="checkbox"]').prop('checked', false);
        $('#roles input[type="checkbox"]').parent().css('color', 'black');
        $('#roles input[type="checkbox"]').each(function () {
            var text = ',' + $(this).val() + ',';
            if (roles == 'all' || roles.indexOf(text) != -1) {
                $(this).prop('checked', true);
                $(this).parent().css('color', 'red');
            }
        })
    }
</script>
<style>
    .roles{
        padding: 2px 5px;
    }
</style>