<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Tên đối tác</th>
                    <th>Hình thức hợp tác</th>
                    <th>Số điện thoại</th>         
                    <th>Email</th>
                    <th>Website</th>
                    <th>Công cụ</th>
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><?php echo $item['id']; ?></td>
                        
                        <td><a href="/<?php echo ADMIN_URL . 'network/edit/' . $item['id']; ?>"><?php echo $item['name']; ?></a></td>                                                 
                        <td><?php echo $item['type']; ?></td>
                        <td><?php echo $item['phone']; ?></td>
                        <td><?php echo $item['email'] ?></td>
                        <td><?php echo $item['website'] ?></td>
                        <td>
                            <a href="/<?php echo ADMIN_URL . 'network/edit/' . $item['id']; ?>">Edit</a> | 
                            <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      
        </table>
    </div>
</div>

<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
