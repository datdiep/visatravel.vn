
<div class="content-wrapper">    
    <section class="content-header">
        <h1>ADD NEWS</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>news">List news</a></li>
            <li class="active">Add News</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <?php if ($check_error == 1): ?>
                <div class="alert alert-error alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                    <?php echo validation_errors() . @$msg; ?>
                </div>
            <?php elseif ($check_error == 0): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Alert!</h4>
                    Thêm thành công
                </div>
            <?php endif; ?>
            <div class="row" style="margin-bottom:20px">
                <div class="col-md-12">
                    <div class="box-footer">

                        <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                    </div>
                </div>
            </div>
            <div class="row">            
                <div class="col-md-6">                
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin cơ bản</h3>
                        </div>


                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tiêu đề</label>
                                <input type="text" class="form-control" id="title" name="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" placeholder="Enter title">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                            </div>



                        </div>                       
                    </div>                          
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-success">                       
                        <div class="box-body">
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <input type="text" class="form-control" name="tags" placeholder="tag1,tags2,tags3">
                            </div>
                        </div>                            

                    </div>  
                </div>

                <div  class="col-md-12">   
                    <div class="box box-primary box-success">   
                        <div class="box-header">
                            <h3 class="box-title">Câu hỏi thuộc dịch vụ :</h3>
                        </div> 

                        <div class="slimScrollDiv" style="padding: 10px 0;position: relative; overflow: auto; width: auto; height: 200px;margin-left: 5px">
                            <?php
                            foreach ($category_service as $key => $item_all) {
                                ?>
                                <div class="item">
                                    <div style="width:100%;margin:5px 2% 0 0" class="input-group"> 
                                        <input type="text" class="form-control" value="<?php echo $item_all['title'] ?>" disabled>
                                    </div>
                                </div>
                                <?php
                                if (!empty($item_all['detail'])):
                                    foreach ($item_all['detail'] as $item):
                                        ?>
                                        <div class="item">
                                            <div style="width:100%;margin:5px 2% 0 0" class="input-group">
                                                <span class="input-group-addon">
                                                    <input type ="checkbox" name ="checkbox[]" value="<?php echo $item['id'] ?>">
                                                </span>
                                       <input type="text" class="form-control" value="<?php echo '|-- '. $item['title'] ?>" disabled>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Nội dung chi tiết</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"></textarea>
                    </div>
                    <div class="box-footer">
                        <button type="submit" value="1" name="submit" class="btn btn-success">Xuất bản</button>
                    </div>
                </div>

            </div>

        </form>
    </section>

</div>
<style>
    div.checkbox{width: 25%;float: left;margin-top: 0px;}
    .checkbox+.checkbox, .radio+.radio{     margin-top: 0px;}
    .categories .has-error{
        white-space: nowrap;
        position: absolute;
        top: -25px;
        left: 78px;
    }
    .categories .has-error .error{
        padding-left: 5px;
    }
</style>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');

    $(function () {
        //Initialize Select2 Elements
        $('.select2').select2()
        //Datemask dd/mm/yyyy
        $('#datemask').inputmask('dd/mm/yyyy', {'placeholder': 'dd/mm/yyyy'})
        //Datemask2 mm/dd/yyyy
        $('#datemask2').inputmask('mm/dd/yyyy', {'placeholder': 'mm/dd/yyyy'})
        //Money Euro
        $('[data-mask]').inputmask()
    })

    $('.attr_service').click(function () {
        var id = $('.attr_service:checked').val();
        $.post('/<?php echo ADMIN_URL; ?>service_question/search', {id: id}, function (results) {
            $('#list_service').html(results);
        });
    });
</script>
<script src="/assets/admin/js/autocomplete.min.js"></script>
<script src="/assets/admin/js/news.min.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="/assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- Select2 -->
<link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
<!-- Select2 -->
<script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
