<div class="content-wrapper" style="min-height: 916px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>DANH SÁCH CÂU HỎI DỊCH VỤ</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List question</li>
        </ol>
        <br>
        <a href="/<?php echo ADMIN_URL; ?>service_question/add"><button style="float:left;margin-right: 10px;width: 200px;" class="btn btn-primary">Thêm Câu Hỏi</button></a>
        <input id="keyword" class="form-control" placeholder="Nhập tên câu hỏi" value="" style="width: 200px;float: left;margin-right: 10px;"/>
        <select class="form-control" id="cat_id" style="width: 200px;float: left;margin-right: 10px;">
            <option value=""> -- Chi Tiết Dịch Vụ -- </option>
            <?php
            foreach ($category_service as $key => $item_all) {
                ?>
                <option class="form-control" disabled> <?php echo $item_all['title'] ?></option>
                <?php
                if (!empty($item_all['detail'])):
                    foreach ($item_all['detail'] as $item):
                        ?>
                <option value="<?php echo $item['id'] ?>">&nbsp;&nbsp;|--<?php echo $item['title'] ?></option>
                        <?php
                    endforeach;
                endif;
            }
            ?>

        </select>           

    </section>
    <br/><br/>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box" id="list_ajax">

                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </section>
</div><!-- /.row -->

<script>
    var current_pos = <?php echo $pos; ?>;
    function loadlist(pos) {
        current_pos = pos;
        var cat_id = $('#cat_id').val();
        var keyword = $('#keyword').val();
        $.post('/<?php echo ADMIN_URL; ?>service_question/page', {pos: pos, cat_id: cat_id, keyword: keyword}, function (results) {
            $('#list_ajax').html(results);
        });
    }
    loadlist(current_pos);
    $('#list_ajax').on('click', '.pagination a', function () {
        var pos = $(this).attr('href').replace('/', '');
        loadlist(pos);
        return false;
    });
    function del(id) {
        show_dialog('Bạn có muốn xóa tin tức id = ' + id + " không", function () {
            $.post('/<?php echo ADMIN_URL; ?>service_question/del', {id: id}, function (results) {
                if (results != "")
                    alert(results);
                loadlist(current_pos);
            });
        });
    }
    $('#cat_id').change(function () {
        loadlist(0);
    });
    $('#keyword').keypress(function (e) {
        loadlist(0);
    });

</script>

