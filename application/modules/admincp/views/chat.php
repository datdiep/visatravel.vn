<div class="item" user="<?php echo $p['type'];?>">
    <img src="/assets/upload/avatar/<?php echo $p['avatar']; ?>" alt="user image" class="online">
    <p class="message">
        <a href="#" class="name">
            <small class="text-muted pull-right"><i class="fa fa-clock-o"></i> <?php echo date('d-m H:i:s', strtotime($p['date_create'])); ?></small>
            <?php echo $p['name'] == 'system' ? '<span style="color:red">' . $p['name'] . '</span>' : $p['name']; ?>
        </a>
        <?php echo $p['content']; ?>
    </p>
</div>