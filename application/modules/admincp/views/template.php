<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo isset($web_title) ? $web_title : $user['fullname'] . ' | Hệ thống quản lý '; ?>  </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>        

        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/assets/admin/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/assets/admin/dist/css/AdminLTE.min.css">
        <link rel="stylesheet" href="/assets/admin/dist/css/skins/_all-skins.min.css">
        <!-- bootrap css -->
        <link href="/assets/admin/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> 

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- daterange picker -->
        <link rel="stylesheet" href="/assets/admin/plugins/daterangepicker/daterangepicker.css">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="/assets/admin/plugins/datepicker/datepicker3.css">
        <script src="/assets/admin/js/jquery-1.11.3.min.js"></script> 
        <link href="/assets/admin/css/dev.css" rel="stylesheet" type="text/css" />       
        <link href="/assets/admin/css/jquery.Jcrop.css" rel="stylesheet" type="text/css" /> 
        <link href="/assets/admin/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" />

        <script src="/assets/admin/js/jquery-ui.min.js"></script>  
        <script src="/assets/admin/js/jquery.validate.min.js"></script> 

        <script type="text/javascript" src="/assets/admin/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="/assets/admin/ckfinder/ckfinder.js"></script>

        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="/assets/admin/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap datepicker -->
        <script src="/assets/admin/plugins/datepicker/bootstrap-datepicker.js"></script>
        <!-- bootstrap color picker -->
        <script src="/assets/admin/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="/assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
        <script>var pre2 = '<?php echo $pre2; ?>';</script>
        <script src="/assets/admin/js/main.js" type="text/javascript"></script>  
        <script src="/assets/admin/js/crop.js" type="text/javascript"></script>
        <script src="/assets/admin/js/auto.js" type="text/javascript"></script>
        <script src="/assets/admin/js/linktudong.js" type="text/javascript"></script>
        <script src="/assets/admin/js/jquery.Jcrop.min.js"></script>
        <script type="text/javascript" src="/assets/admin/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <link rel="stylesheet" href="/assets/admin/plugins/select2/select2.min.css">
        <script src="/assets/admin/plugins/select2/select2.full.min.js"></script>
    </head>
    <body class="skin-blue sidebar-mini">
        <div id="dialog"></div>
        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="/<?php echo ADMIN_URL; ?>" class="logo">
                    <span class="logo-mini"><b>Hệ Thống quản lý</b></span>
                    <img src="/assets/admin/images/logo-admin.png" width="150">
                </a>

                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu pull-left">
                        <ul class="nav navbar-nav">

                            <li class="dropdown messages-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <?php echo @$notify_number == 0 ? '' : '<span class="label label-danger">' . @$notify_number . '</span>'; ?>
                                </a>
                                <ul class="dropdown-menu">
                                    <div class="pull-right">
                                        <a href="/<?php echo ADMIN_URL; ?>auth/logout" class="btn btn-default btn-flat">Đăng xuất</a>
                                    </div>
                                    <?php if (!empty($notify)): ?>
                                        <li class="header"><?php echo $notify_number == 0 ? 'Không có tin nhắn nào chưa xem' : 'Bạn có ' . $notify_number . ' tin nhắn chưa xem'; ?> </li>
                                        <li>

                                            <ul class="menu">
                                                <?php
                                                $notify_not_see = '';
                                                $notify_see = '';
                                                ?>
                                                <?php
                                                foreach ($notify as $key => $p) {
                                                    if ($p['has_view'] == 0) {
                                                        $notify_not_see .= '<li class="notify_not_see"><a href="/admincp/notify/index/' . $key . '">
                                                            <div class="pull-left">
                                                                 <img src="/assets/upload/avatar/' . $p['avatar'] . '" class="img-circle" alt="User Image">
                                                            </div>
                                                            <h4>
                                                              ' . $p['name'] . '
                                                              <small><i class="fa fa-clock-o"></i> ' . date('d-m H:i', strtotime($p['date_create'])) . '</small>
                                                            </h4>
                                                            <p>' . $p['content'] . '</p>
                                                          </a></li>';
                                                    } else {
                                                        $notify_see .= '<li class="notify_see"><a href="/admincp/notify/index/' . $key . '">
                                                            <div class="pull-left">
                                                                 <img src="/assets/upload/avatar/' . $p['avatar'] . '" class="img-circle" alt="User Image">
                                                            </div>
                                                            <h4>
                                                              ' . $p['name'] . '
                                                              <small><i class="fa fa-clock-o"></i> ' . date('d-m H:i:s', strtotime($p['date_create'])) . '</small>
                                                            </h4>
                                                            <p>' . $p['content'] . '</p>
                                                          </a></li>';
                                                    }
                                                }
                                                ?>
                                                <?php echo $notify_not_see; ?>
                                                <?php echo $notify_see; ?>
                                            </ul>
                                        </li>
                                        <li class="footer">&nbsp;</li>
                                    <?php else: ?>
                                        <li class="header">Bạn chưa có tin nhắn nào.</li>
                                    <?php endif; ?>

                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                                    <?php if ($user['avatar']): ?>
                                        <img src="/assets/upload/avatar/<?php echo $user['avatar']; ?>" class="user-image" alt="User Image"/>
                                    <?php endif; ?>

                                    <span class="hidden-xs"><?php echo $username; ?></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <?php if ($user['avatar']): ?>
                                            <img src="/assets/upload/avatar/<?php echo $user['avatar']; ?>" class="img-circle" alt="User Image"/>
                                        <?php endif; ?>

                                        <p>
                                            <?php echo $username; ?>
                                            <small>Tham gia: <?php echo $user['date_create']; ?></small>
                                        </p>
                                    </li>                            
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="/<?php echo ADMIN_URL; ?>profile" class="btn btn-default btn-flat">Cài đặt</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="/<?php echo ADMIN_URL; ?>auth/logout" class="btn btn-default btn-flat">Đăng xuất</a>
                                        </div>
                                    </li>
                                </ul>

                            </li>
                            <!-- Control Sidebar Toggle Button -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <?php if ($user['avatar']): ?>
                            <div class="pull-left image">
                                <img src="/assets/upload/avatar/<?php echo $user['avatar']; ?>" class="img-circle" alt="User Image" />
                            </div>
                        <?php endif; ?>
                        <div class="pull-left info">
                            <p><?php echo $username; ?></p>
                            <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $user['job']; ?></a>
                        </div>
                    </div>
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Tìm khách hàng">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- search form -->                       
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>  
                        <li class="treeview active">
                            <a href="#" class="">
                                <span>Dashboard</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="dashboard <?php echo $pre == 'dashboard' ? 'active' : ''; ?>" action="dashboard"><a href="/admincp/"><i class="fa fa-dashboard <?php echo $pre == 'dashboard' ? 'text-yellow' : ''; ?>"></i> Dashboard</a></li>                                
                            </ul>
                        </li>

                        <li class="treeview active">
                            <a href="#" class="">
                                <i class="fa fa-file-text-o"></i>
                                <span>Quản Lý Dịch vụ Visa</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="category_service <?php echo $pre == 'category_service' ? 'active' : ''; ?>" action="category_service"><a href="/<?php echo ADMIN_URL; ?>category_service"><i class="fa fa-gg <?php echo $pre == 'category_service' ? 'text-yellow' : ''; ?>"></i> Danh Mục Dịch Vụ</a></li>                                                                                                                        
                                <li class="service_detail <?php echo $pre == 'service_detail' ? 'active' : ''; ?>" action="service_detail"><a href="/<?php echo ADMIN_URL; ?>service_detail"><i class="fa fa-twitch <?php echo $pre == 'service_detail' ? 'text-yellow' : ''; ?>"></i> Chi Tiết Dịch Vụ</a></li>
                                <li class="service_question <?php echo $pre == 'service_question' ? 'active' : ''; ?>" action="service_question"><a href="/<?php echo ADMIN_URL; ?>service_question"><i class="fa fa-twitch <?php echo $pre == 'service_question' ? 'text-yellow' : ''; ?>"></i> Câu hỏi về dịch vụ</a></li>
                                <li class="call_back <?php echo $pre == 'call_back' ? 'active' : ''; ?>" action="comment_service"><a href="/<?php echo ADMIN_URL; ?>call_back"><i class="fa fa-twitch <?php echo $pre == 'call_back' ? 'text-yellow' : ''; ?>"></i> Danh Sách Cuộc gọi</a></li>
                            </ul>
                        </li>

                        <li class="treeview active">
                            <a href="#" class="">
                                <i class="fa fa-file-text-o"></i>
                                <span>Quản lý tin tức</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="category_news <?php echo $pre == 'category_news' ? 'active' : ''; ?>" action="category_news"><a href="/<?php echo ADMIN_URL; ?>category_news"><i class="fa fa-gg <?php echo $pre == 'category_news' ? 'text-yellow' : ''; ?>"></i> Danh Mục Tin Tức</a></li>                                                                                                                        
                                <li class="news <?php echo $pre == 'news' ? 'active' : ''; ?>" action="news"><a href="/<?php echo ADMIN_URL; ?>news"><i class="fa fa-twitch <?php echo $pre == 'news' ? 'text-yellow' : ''; ?>"></i> Quản lý tin tức</a></li>
                            </ul>
                        </li>


                        <li class="treeview active">
                            <a href="#">
                                <i class="fa fa-cog"></i>
                                <span>Quản lý Khác</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li class="admin <?php echo $pre == 'admin' ? 'active' : ''; ?>" action="admin"><a href="/<?php echo ADMIN_URL; ?>admin"><i class="fa fa-circle-o <?php echo $pre == 'admin' ? 'text-yellow' : ''; ?>"></i> Quản lý tài khoản</a></li>                                                           
                                <li class="page <?php echo $pre == 'page' ? 'active' : ''; ?>" action="page"><a href="/<?php echo ADMIN_URL; ?>page"><i class="fa fa-forumbee <?php echo $pre == 'page' ? 'text-yellow' : ''; ?>"></i> Quản lý trang</a></li>
                                <li class="gallery <?php echo $pre == 'gallery_add' ? 'active' : ''; ?>" action="gallery_add"><a href="/<?php echo ADMIN_URL; ?>gallery/add"><i class="fa fa-circle-o <?php echo $pre == 'gallery_add' ? 'text-yellow' : ''; ?>"></i> Thêm thư viện</a></li>
                                <li class="gallery <?php echo $pre == 'gallery' ? 'active' : ''; ?>" action="gallery"><a href="/<?php echo ADMIN_URL; ?>gallery"><i class="fa fa-circle-o <?php echo $pre == 'gallery' ? 'text-yellow' : ''; ?>"></i> Danh sách thư viện</a></li>                                                             
                                <li class="homeinfo <?php echo $pre == 'seohome' ? 'active' : ''; ?>" action="homeinfo"><a href="/<?php echo ADMIN_URL; ?>homeinfo"><i class="fa fa-circle-o <?php echo $pre == 'seohome' ? 'text-yellow' : ''; ?>"></i> Thiết lập hệ thống</a></li>   
                                <li class="hotline <?php echo $pre == 'hotline' ? 'active' : ''; ?>" action="hotline"><a href="/<?php echo ADMIN_URL; ?>hotline"><i class="fa fa-circle-o <?php echo $pre == 'hotline' ? 'text-yellow' : ''; ?>"></i> HOT LINE</a></li>   
                                <li class="richsnipnet <?php echo $pre == 'richsnipnet' ? 'active' : ''; ?>" action="richsnipnet"><a href="/<?php echo ADMIN_URL; ?>richsnipnet"><i class="fa fa-circle-o <?php echo $pre == 'richsnipnet' ? 'text-yellow' : ''; ?>"></i> Richsnipnet</a></li>
                            </ul>
                        </li>

                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <?php if (strpos(',' . $user['role_controller'] . ',', ',root,') === false): ?>
                <?php
                $roles = explode(",", $user['role_controller']);
                $user['role_controller'] = "";
                foreach ($roles as $role) {
                    if (strpos(',' . $all_role . ',', ',' . $role . ',') !== false)
                        $user['role_controller'] .= $user['role_controller'] == "" ? $role : ',' . $role;
                }
                //pre($user['role_controller']);
                ?>
                <style>
                    .treeview .treeview-menu li{display: none;}
                    <?php echo '.' . str_replace(',', ',.', $user['role_controller']) ?>{display: block !important;}
                </style>
                <script>
                $("<?php echo '.' . str_replace(',', ',.', $user['role_controller']) ?>").addClass('has_permisstion');
                $(".treeview").each(function () {
                    if ($('li.has_permisstion', this).length == 0 && !$(this).hasClass('has_permisstion')) {
                        $(this).remove();
                    }
                });
                </script>
            <?php endif; ?>

            <div class="" style="position: relative;">
                <div style="display: none;" id="loading_content" class="overlay">
                    <i class="fa fa-refresh fa-spin"></i>
                </div> 
                <?php echo $content_block; ?>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2010 - 2017 <a href="https://achau.net" target="blank">Á Châu Visa</a>.</strong> All rights reserved
            </footer>
        </div>

        <!-- jQuery 2.1.4 -->              
        <script src="/assets/admin/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>        
        <script src='/assets/admin/plugins/fastclick/fastclick.min.js'></script>        
        <script src="/assets/admin/js/app.min.js" type="text/javascript"></script>                                      
        <script>
            $('.format_number').autoNumeric('init', {aPad: false});
            $('form').submit(function () {
                $('input.format_number').each(function () {
                    $(this).val($(this).autoNumeric('get'));
                });
            });
        </script>
    </body>
</html>