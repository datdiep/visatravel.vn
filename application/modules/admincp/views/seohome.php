<div class="content-wrapper">    
    <section class="content-header">
        <h1>Thiết Lập Hệ Thống</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>" ><i class="fa fa-dashboard"></i> Home</a></li>
            <li>SEO HOME</li>            
        </ol>
    </section>    
    <section class="content">
	<div class="row"> 
	    <div class="col-md-6"> 
		<form id="form" method="post" enctype="multipart/form-data">


		    <?php if ($check_error == 0): ?>
    		    <div class="alert alert-success alert-dismissable">
    			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    			<h4>	<i class="icon fa fa-check"></i> Alert!</h4>
    			Cập nhật thành công
    		    </div>
		    <?php endif; ?>
		    <?php if ($check_error == 1): ?>
    		    <div class="alert alert-danger alert-dismissable">
    			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    			<h4><i class="icon fa fa-ban"></i> Alert!</h4>
			    <?php echo @$msg; ?>
			    <?php echo validation_errors(); ?>
    		    </div>
		    <?php endif; ?>
                    <div class="box box-primary box-success">                       
                        <div class="box-body">
                            <div class="form-group">
                                <label for="date_shipping">Text Slider</label>
                                <textarea name="text_sliderhome" style="width: 100%; height: 200px"><?php echo @$result['text_sliderhome']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">Quote Trang chủ</label>
                                <textarea id="textarea_content" name="quote_home" style="width: 100%; height: 200px"><?php echo @$result['quote_home']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">Why choose  Trang chủ</label>
                                <textarea name="why_home" style="width: 100%; height: 200px"><?php echo @$result['why_home']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">Quick And Easy! Trang chủ</label>
                                <textarea id="textarea_easyhome" name="easy_home" style="width: 100%; height: 200px"><?php echo @$result['easy_home']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="telco">Link youtube</label>
                                <input type="text" class="form-control" value="<?php echo @$result['youtube']; ?>" name="youtube">
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">About us! Trang chủ</label>
                                <textarea id="textarea_abouthome" name="about_home" style="width: 100%; height: 200px"><?php echo @$result['about_home']; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="sale_off">Website</label>
                                <input type="text" class="form-control" value="<?php echo @$result['website']; ?>" name="website">
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">Email Liên hệ</label>
                                <input type="text" class="form-control" value="<?php echo @$result['email']; ?>" name="email">
                            </div>
                            <div class="form-group">
                                <label for="date_shipping">Button Apply Online</label>
                                <input type="text" class="form-control" value="<?php echo @$result['bt_apply']; ?>" name="bt_apply">
                            </div>
                            <div class="form-group cellphone">
                                <div class="col-md-6">
                                    <label for="date_shipping">Di động US (Click)</label>
                                    <input type="text" class="form-control" value="<?php echo @$result['phone2_click']; ?>" name="phone2_click">
                                </div>
                                <div class="col-md-6">
                                    <label for="date_shipping">Di động US (Hiển thị)</label>
                                    <input type="text" class="form-control" value="<?php echo @$result['phone2_show']; ?>" name="phone2_show">
                                </div>
                            </div>
                            <div class="form-group cellphone">
                                <div class="col-md-6">
                                    <label for="date_shipping">Di động VN (Click)</label>
                                    <input type="text" class="form-control" value="<?php echo @$result['phone1_click']; ?>" name="phone1_click">
                                </div>
                                <div class="col-md-6">
                                    <label for="date_shipping">Di động VN (Hiển thị)</label>
                                    <input type="text" class="form-control" value="<?php echo @$result['phone1_show']; ?>" name="phone1_show">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="telco">Tổng đài</label>
                                <input type="text" class="form-control" value="<?php echo @$result['telco']; ?>" name="telco">
                            </div>
                            <div class="form-group">
                                <label for="address">Địa chỉ</label>
                                <input type="text" class="form-control" value="<?php echo @$result['address']; ?>" name="address">
                            </div>
                            <div class="form-group">
                                <label for="date_work">Thời gian làm việc</label>
                                <input type="text" class="form-control" value="<?php echo @$result['date_work']; ?>" name="date_work">
                            </div>
                            <div class="form-group">
                                <label for="seo_title">Seo title</label>
                                <input type="text" class="form-control" value="<?php echo @$result['seo_title']; ?>" name="seo_title">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta description</label>
                                <input type="text" class="form-control" value="<?php echo @$result['meta_description']; ?>" name="meta_description">
                            </div>
                            <div class="form-group">
                                <label for="meta_keyword">Meta keyword</label>
                                <input type="text" class="form-control" value="<?php echo @$result['meta_keyword']; ?>" name="meta_keyword">
                            </div>

                        </div>  
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>    



		</form>
	    </div>
	    <div class="col-md-6"> 
		<div class="box box-danger">    
		    <div class="box-header with-border">
			<select class="form-control" id="role_type" style="width: 200px;float: left;margin-right: 10px;">
			    <option value=""> -- Tất cả quyền -- </option>
			    <?php foreach ($map_type as $k => $item): ?>
				<option value="<?php echo $k; ?>"> -- <?php echo $item; ?> -- </option>
			    <?php endforeach; ?>
			</select>
			<a href="javascript:void(0)" class="btn btn-success btn_role" rel_id="0"><i class="fa fa-user-plus"></i> Thêm quyền</a>
		    </div>
		    <div class="box-body list_role">
			
		    </div>                            

		</div>  
	    </div>
	</div>
    </section>

</div>
<script>

    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    var editor1 = CKEDITOR.replace('textarea_easyhome');
    var editor3 = CKEDITOR.replace('textarea_abouthome');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
    function loadlist() {
        var role_type = $('#role_type').val();
        $.post('/<?php echo ADMIN_URL; ?>homeinfo/page_role', {role_type: role_type}, function (results) {   
            $('.list_role').html(results);
        });
    }
     loadlist();
    $('#role_type').change(function () {
        loadlist();
    });
    $('body').on('click', '.btn_role', function (event) {
	event.preventDefault();
	var id = $(this).attr('rel_id');
	$.post('/<?php echo ADMIN_URL; ?>homeinfo/role', {id: id}, function (data) {
	    $(data).modal().on(function () {});
	})
    });
    $('body').on('hidden.bs.modal', '#role', function () {
	$('#role').remove();
    });
    $('body').on('click', '#role .save_role', function (event) {
	event.preventDefault();
	data = {};
	$('#role .form-control').each(function () {
	    var key = $(this).attr('name');
	    if ($(this).hasClass('valid') && $(this).val() == '') {
		var txt = $(this).prev().text();
		alert(txt+' không được bỏ trống');
		$(this).focus();
		return false;
	    }
	    data[key] = $(this).val();
	})
	$.post('/<?php echo ADMIN_URL; ?>homeinfo/save_role', {data: data}, function (result) {
	    console.log(result);
	    if(result == -1)
		alert('Không tìm thấy quyền cần cập nhật');
	    else{
		$('#role').modal('hide');
		$('#iframe').load(function(){ });
		$.when(loadlist()).done(function () {
		    setTimeout(function () {
			$('.list_role tr[rel_id="'+result+'"]').css("background-color", "#f4f4f4");
		    },200); 
		    
		})
	    }
	})
    });
    function del(id) {
	var check_text = Math.random().toString(36).substr(2, 5);
	show_dialog('Bạn có chắc chắn muốn xóa quyền này không ? Vui lòng nhập <span style="color:red">'+check_text+'</span> để xóa <br><br> <input style="width:100%">', function () {
	    $.post('/<?php echo ADMIN_URL; ?>homeinfo/del_role', {id: id}, function (result) {
		loadlist();
	    });
	},check_text);
    }
</script>