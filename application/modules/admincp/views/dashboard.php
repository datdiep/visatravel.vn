<div class="content-wrapper">    
    <section class="content-header">

        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>    
    <!-- Main content -->
    <br>

    <section class="content">
        <!-- Info boxes -->
        <div class="row ">


            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8 col-xs-12">

                        <?php echo @$block_customer; ?>
                        <?php echo @$block_news; ?>  
                    </div>

                    <div class="col-md-4 col-xs-12">
                        <?php echo $block_note; ?>
                        <?php echo $block_quotations; ?>
                    </div>
                </div>
            </div>
        </div>



    </section>

</div>
