<div class="content-wrapper">    
    <section class="content-header">
        <h1>GỬI EMAIL CHO KHÁCH HÀNG</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>checklist">Check list</a></li>
            <li class="active">Gửi email cho khách</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Cập nhật thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin khách hàng </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Email của khách</label>
                                <input type="text" class="form-control" id="title" name="title">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="alias">Xưng hô</label>
                                <select name="type" class="form-control valid">
                                    <option value="anh">anh</option>
                                    <option value="chị">chị</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="title">Tên của khách</label>
                                <input type="text" class="form-control" id="title" name="title">
                            </div>
                            <div class="form-group col-md-4 nopadding">
                                <label for="alias">File đính kèm</label>
                                <select name="type" class="form-control valid">
                                    <option value="chị">Không</option>
                                    <option value="anh">Có</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <p> Email sử dụng để gửi là <b><?php echo $network['email']; ?></b></p>
                            </div>
                        </div>

                    </div> 
                </div>
                <div class="col-md-6">
                    <?php echo $load_file; ?>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Nội dung</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 400px">
                            Kính gửi anh/ chị, 
                            <br>
                            Em gửi anh/chị  <?php echo $page['title']; ?>

                            <?php echo $page['content']; ?>

                            <p><b><?php echo $network['name']; ?></b></p>
                            <p><b class="c-green">Người liên hệ: <?php echo $user['fullname']; ?></b></p>
                            <p><b class="c-red">Di Động: <?php echo $user['phone']; ?></b></p>
                            <p>Điện thoại : <?php echo $network['phone']; ?></p>
                            <p>Email: <?php echo $user['email']; ?></p>
                            <p>Website: <?php echo $network['website']; ?></p>
                            <p>Địa chỉ : <?php echo $network['address']; ?></p> 
                        </textarea>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');

    var order_id = <?php echo $page['id']; ?>;
    var model = 'checklist_model';
</script>