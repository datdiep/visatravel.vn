<div class="content-wrapper">    
    <section class="content-header">
        <h1>Thêm Folder tài liệu</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>category">List category</a></li>
            <li class="active">Add Category</li>
        </ol>
    </section>    
    <section class="content">

        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin folder</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tên folder</label>
                                <input type="text" class="form-control" id="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="name" placeholder="Enter Title" value="<?php echo @$folder['name']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" placeholder="Enter Alias" value="<?php echo @$folder['alias']; ?>">
                            </div>
                            <div class="form-group">
                                <label for="alias">Parent</label>
                                <select class="form-control" name="parent">

                                    <option value="0" <?php echo @$folder['parent'] == 0 ? 'selected' : ''; ?>> Root </option>
                                    <?php foreach ($list_parent as $value) : ?>
                                        <option <?php echo @$folder['parent'] == $value['id'] ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alias">Priority</label>
                                <input type="text" class="form-control"  name="priority" placeholder="Enter priority" value="<?php echo @$folder['alias']; ?>">
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Lưu lại</button>
                        </div>
                    </div> 
                </div>
                <div class="col-md-6">  
                    <div class="box box-primary box-default">
                        <div class="box-header">
                            <h3 class="box-title pull-left">Danh sách Folder</h3>

                            <a href="/<?php echo ADMIN_URL; ?>document/manage_folder"><span style="margin-top: 10px;width: 150px;" class="pull-right btn btn-block btn-info">Thêm Folder</span></a>

                        </div>
                        <div class="box-body">
                            <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                                <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
                                    <thead>
                                        <tr role="row">
                                            <th>STT</th>
                                            <th>Title</th>                                       
                                            <th>Parent</th>
                                            <th>Actions</th>                                                
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if (!empty($parent)): ?>

                                            <?php foreach ($parent as $item): ?>
                                                <tr>
                                                    <td><?php echo $item['id']; ?></td>
                                                    <td><?php echo $item['title'] . "({$item['count']})"; ?></td>
                                                    <td><?php echo $item['alias']; ?></td>
                                                    <td><input rel="<?php echo $item['id']; ?>" value="<?php echo $item['order']; ?>"/></td>
                                                    <td><a href="<?php echo base_url(); ?>category_product/edit/<?php echo $item['id']; ?>">EDIT</a> | <a href="javascript:void(0)" onclick="del(<?php echo $item['id']; ?>)">DEL</a></td>
                                                </tr>
                                                <?php
                                                if (isset($item['child']))
                                                    foreach ($item['child'] as $child):
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $child['id']; ?></td>
                                                            <td>---- <?php echo $child['title'] . "({$child['count']})"; ?></td>
                                                            <td><?php echo $child['alias']; ?></td>
                                                            <td><input rel="<?php echo $child['id']; ?>" value="<?php echo $child['order']; ?>"/></td>
                                                            <td><a href="<?php echo base_url(); ?>category_product/edit/<?php echo $child['id']; ?>">EDIT</a> | <a href="javascript:void(0)" onclick="del(<?php echo $child['id']; ?>)">DEL</a></td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                        <?php foreach ($type as $item): ?>
                                            <tr>
                                                <td><?php echo $item['id']; ?></td> 
                                                <td><a href="/<?php echo ADMIN_URL . 'document/manage_folder/' . $item['id']; ?>"><?php echo $item['name']; ?></a></td>                                                 
                                                <td><?php echo $item['parent']; ?></td>  
                                                <td>
                                                    <a href="/<?php echo ADMIN_URL . 'document/manage_folder/' . $item['id']; ?>">Edit</a> | 
                                                    <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>



                                    </tbody>      
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>
    </section>

</div>

<script>
    function del(id) {
        show_dialog('Bạn có chắc xóa folder id =' + id + ' ?', function () {
            $.post('/<?php echo ADMIN_URL; ?>document/del_folder', {id: id}, function (results) {
                location.reload();
            });
        });
    }
</script>

