<div class="content-wrapper">    
    <section class="content-header">
        <h1>THÊM TÀI LIỆU</h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>checklist">Tài liệu</a></li>
            <li class="active">Add page</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Thêm thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Tên tài liệu</label>
                                <input type="text" class="form-control" id="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="title" placeholder="Enter Title">
                            </div>

                            <div class="form-group hidden-md hidden-xs hidden-lg">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" placeholder="Enter Alias">
                            </div>

                            <div class="form-group">
                                <label for="alias">Link youtube video (Nếu có)</label>
                                <input type="text" class="form-control" id="alias" name="youtube" placeholder="Nhập link video Youtube vào">
                            </div>

                            <div class="form-group col-md-7 nopadding">
                                <label for="alias">Danh mục</label>
                                <select name="folder_id" class="form-control valid">
                                    <?php foreach ($type as $value): ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>

                            <div class="form-group col-md-5 nopadding">
                                <div class="form-group">
                                    <label>Tag thành viên muốn chia sẻ</label>
                                    <select name="tags" class="form-control select2" multiple="multiple" data-placeholder="Chọn thành viên muốn chia sẽ">
                                        <?php foreach ($staff as $value): ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['username']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="box box-primary box-default">

                        <div class="box-header">
                            <h3 class="box-title">Ghi chú</h3>
                        </div>

                        <div class="box-body">

                            <div class="form-group">
                                <ul>
                                    <li><p>Nội dung tài liệu sẽ được chuẩn bị đầy đủ và trình bày trực quan.</p></li>
                                    <li><p>Không thêm số điện thoại và thông tin liên lạc vào nội dung. Hệ thống sẽ tự tạo thông tin liên hệ ở phía cuối file dựa vào tài khoản của user.</p></li>
                                    <li><p>Logo hệ thống sẽ tự thêm vào. Vui lòng chuẩn bị tài liệu kỹ càng về mặt nội dung</p></li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Nội dung</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"></textarea>
                        <div class="box-footer">
                            <button type="submit" name="submit" value="1" class="btn btn-primary">Thêm tài liệu</button>
                            <button type="submit" name="submit" value="2" class="btn btn-success">Thêm và Chỉnh sửa</button>
                        </div>
                    </div>
                </div>               
            </div>
        </form>
    </section>

</div>
<script>
    $(".select2").select2();
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>