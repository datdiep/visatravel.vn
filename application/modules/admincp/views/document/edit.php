<div class="content-wrapper">    
    <section class="content-header">
        <h1>SỬA TÀI LIỆU: <span><?php echo $page['title'] ?></span></h1>
        <ol class="breadcrumb">
            <li><a href="/<?php echo ADMIN_URL; ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/<?php echo ADMIN_URL; ?>document">Quản lý tài liệu</a></li>
            <li class="active">Edit Checklist</li>
        </ol>
    </section>    
    <section class="content">
        <form id="form" method="post" enctype="multipart/form-data">
            <div class="row">            
                <div class="col-md-6">   
                    <?php if ($check_error == 0): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4>	<i class="icon fa fa-check"></i> Alert!</h4>
                            Cập nhật thành công
                        </div>
                    <?php endif; ?>
                    <?php if ($check_error == 1): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo validation_errors(); ?>                          
                        </div>
                    <?php endif; ?>
                    <div class="box box-primary box-success">
                        <div class="box-header">
                            <h3 class="box-title">Thông tin </h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" onblur="taolink('title', 'alias')" onkeyup="taolink('title', 'alias')" name="title" value="<?php echo $page['title']; ?>">
                            </div>


                            <div class="form-group hidden-md hidden-xs hidden-lg">
                                <label for="alias">Alias</label>
                                <input type="text" class="form-control" id="alias" name="alias" value="<?php echo $page['alias']; ?>">
                            </div>

                            <div class="form-group col-md-12 nopadding">
                                <label for="tags">Youtube</label>
                                <input type="text" class="form-control" name="youtube" value="<?php echo $page['youtube']; ?>">
                            </div>

                            <div class="form-group col-md-6 nopadding">
                                <label for="alias">Danh mục</label>
                                <select name="folder_id" class="form-control valid">
                                    <?php foreach ($type as $value): ?>
                                        <option <?php echo $page['folder_id'] == $value['id'] ? 'selected' : ''; ?> value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>


                            <!--                            <div class="form-group col-md-6 nopadding">
                                                            <label for="tags">Tags</label>
                                                            <input type="text" class="form-control" name="tags" value="<?php echo $page['tags']; ?>">
                                                        </div>-->
                            <div class="form-group col-md-6 nopadding">
                                <div class="form-group">
                                    <label>Tag thành viên muốn chia sẻ</label>
                                    <select name="tags" class="form-control select2" multiple="multiple" data-placeholder="Chọn thành viên muốn chia sẽ">
                                        <?php foreach ($staff as $value): ?>
                                            <option value="<?php echo $value['id']; ?>"><?php echo $value['username']; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>


                        </div>

                    </div> 
                </div>
                <div class="col-md-6">
                    <?php echo $load_file; ?>
                </div>
                <div  class="col-md-12">   
                    <div style="padding: 15px;" class="box box-primary box-success" ">
                        <div class="box-header">
                            <h3 class="box-title">Nội dung</h3>
                        </div>  
                        <textarea id="textarea_content" name="content" style="width: 100%; height: 200px"><?php echo $page['content']; ?></textarea>
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </div>               
            </div>
        </form>
    </section>

</div>
<script>
     $(".select2").select2();
    CKEDITOR.config.entities_latin = false;
    var editor2 = CKEDITOR.replace('textarea_content');
    CKFinder.setupCKEditor(editor2, '/assets/admin/ckfinder/');
</script>