<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Bản In <?php echo $page['title']; ?></title>
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="/assets/admin/bootstrap/css/bootstrap.min.css" />
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
        <!-- Theme style -->
        <link rel="stylesheet" href="/assets/admin/dist/css/AdminLTE.min.css" />

        <style>
            body {
                background: rgb(204,204,204); 
                font-size: 16px;
                font-family: Arial;
            }
            page {
                background: white;
                display: block;
                margin: 0 auto;
                margin-bottom: 0.5cm;
                box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
                padding: 5mm;
            }
            page[size="A4"] {  
                width: 21cm;
            }
            page[size="A4"][layout="portrait"] {
                width: 29.7cm;
            }
            page[size="A3"] {
                width: 29.7cm;
            }
            page[size="A3"][layout="portrait"] {
                width: 42cm;
            }
            page[size="A5"] {
                width: 14.8cm;
            }
            page[size="A5"][layout="portrait"] {
                width: 21cm; 
            }
            @media print {
                body, page {
                    margin: 0;
                    box-shadow: 0;
                    -webkit-print-color-adjust: exact;
                }
                .letterhead .bg{
                    background: url('/assets/user/images/letterhead.png')!important ;
                }
                .title{
                    color:#ff0000!important; 
                }
            }
            .letterhead{margin: 0 auto; text-align: center; padding-bottom: 5mm}
            .letterhead img{position: absolute; top: 0; left: 0; height: 56px ; border:0}
            .letterhead .bg{
                background: url('/assets/user/images/letterhead.png') ;
                height: 88px;
                width: 665px;
                margin: 0 auto;
                position: relative
            }
            .title{
                color:#ff0000; 
                font-weight: bold; 
                font-size: 2em;
                text-align: center;
                margin-bottom: 20px
            }
            .c-red{color:#ff0000}
            .c-green{color:green}

        </style>
    </head>
    <body>
        <page size="A4">
            <div class="letterhead">
                <div class="bg">
                    <?php
                    if (@$network['logo']) {
                        echo ' <img src="/assets/upload/network/' . $network['logo'] . '" border="0">';
                    }
                    ?>
                </div>
            </div>
            <div class="title">
                <?php echo $page['title']; ?>
            </div>
            <div class="content">
                <?php if ($page['youtube']): ?>
                    <script src="https://content.jwplatform.com/libraries/FfMSyPCK.js"></script>	
                    <script src="/assets/jwplayer/jwplayer.js"></script>
                    <script>jwplayer.key = "ABCdeFG123456SeVenABCdeFG123456SeVen==";</script>
                    <div id="container"></div>
                    <script>
                        jwplayer("container").setup({
                            file: "<?php echo trim($page['youtube']); ?>"
                        });
                    </script>
                <?php endif; ?>
                <!-- attach file -->
                <div class="row">

                    <?php if (!empty($word)): ?>
                        <ul class="todo-list ui-sortable" style="margin-top:5px">
                            <?php foreach ($word as $k => $item): ?>
                                <li class="word_<?php echo $k; ?>">
                                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                                        <span class="handle ui-sortable-handle">
                                            <i class="fa fa-file-word-o"></i>
                                        </span>
                                        <span class="text"><?php echo $item; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>     
                        </ul>
                    <?php endif; ?>
                    <?php if (!empty($excel)): ?>
                        <ul class="todo-list ui-sortable" style="margin-top:5px">
                            <?php foreach ($excel as $k => $item): ?>
                                <li class="excel_<?php echo $k; ?>">
                                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                                        <span class="handle ui-sortable-handle">
                                            <i class="fa fa-file-excel-o"></i>
                                        </span>
                                        <span class="text"><?php echo $item; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>     
                        </ul>
                    <?php endif; ?>
                    <?php if (!empty($pdf)): ?>
                        <ul class="todo-list ui-sortable" style="margin-top:5px">
                            <?php foreach ($pdf as $k => $item): ?>
                                <li class="pdf_<?php echo $k; ?>">
                                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                                        <span class="handle ui-sortable-handle">
                                            <i class="fa fa-file-pdf-o"></i>
                                        </span>
                                        <span class="text"><?php echo $item; ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>     
                        </ul>
                    <?php endif; ?>
                    <?php if (!empty($zip)): ?>
                        <ul class="todo-list ui-sortable" style="margin-top:5px">
                            <?php foreach ($zip as $k => $item): ?>
                                <li class="zip_<?php echo $k; ?>">
                                    <a target="_bank" href="<?php echo "/assets/upload/data/{$folder}/{$item}"; ?>">
                                        <span class="label-danger" style="padding: 1px 5px"><i class="fa fa-file-text-o"></i> Hồ sơ của khách</span>
                                        <span class="text"><?php echo $item; ?></span>
                                    </a>

                                </li>
                            <?php endforeach; ?>     
                        </ul>
                    <?php endif; ?>

                </div>
                <p>
                    <?php echo $page['content']; ?> 
                </p>

            </div>
            <div class="contact">
                <p><b><?php echo $network['name']; ?></b></p>
                <p><b class="c-green">Người liên hệ: <?php echo $user['fullname']; ?></b></p>
                <p><b class="c-red">Di Động: <?php echo $user['work_phone']; ?></b></p>
                <p>Điện thoại công ty : <?php echo $network['phone']; ?></p>
                <p>Email: <?php echo $user['email']; ?></p>
                <p>Website: <?php echo $network['website']; ?></p>
                <p>Địa chỉ : <?php echo $network['address']; ?></p>

            </div>
        </page>
    </body>

</html>
<script>
    function nWin() {
        var w = window.open();
        var html = $("#toNewWindow").html();
        /*for(var i=0;i<5;i++){
         $("#toNewWindow").clone().prependTo("body")
         }*/
        $(w.document.body).html(html);
        setTimeout(function () {
            w.print()
        }, 1500);
    }

    $(function () {
        $("a#print").click(nWin);
    });
        </scrip>