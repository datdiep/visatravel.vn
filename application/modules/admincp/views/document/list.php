<?php $this->load->library('transload'); ?>
<div class="box-body">
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
        <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
            <thead>
                <tr role="row">
                    <th>STT</th>
                    <th>Title</th>      
                    <th>Folder</th>  
                    <th>Ngày cập nhật</th>
                    <th>Actions</th>                                                
                </tr>
            </thead>
            <tbody>    
                <?php foreach ($results as $item): ?>
                    <tr>
                        <td><input type="checkbox" class="" name="id[]" value="<?php echo $item['id'] ?>"></td> 
                        <td>
                            <a target="blank" title="Xem bản in" href="/<?php echo ADMIN_URL . 'document/document_print/' . $item['id']; ?>">
                               
                                
                                <?php if ($item['youtube']) echo '<i class="fa fa-video-camera" aria-hidden="true"></i>'; ?>  <?php echo $item['title']; ?>

                            </a>
                        </td>                                                 
                        <td><?php echo $item['folder_name'] ?></td>
                        
                        <td>
                            <?php
                            if ($item['last_update'] != $item['publish_date']) {
                                echo $this->transload->ago(strtotime($item['last_update']), 0) . ' trước';
                            }
                            ?>
                        <td>
                            <?php if (($item['author_username'] == $this->data['user']['username'] && $item['network_id'] == $this->data['user']['network_id']) || !empty($this->data['user']['role_department'])): ?>
                                <a href="/<?php echo ADMIN_URL . 'document/edit/' . $item['id']; ?>">Edit</a> | 
                                <a onclick="del(<?php echo $item['id']; ?>)" href="javascript:void(0)">Delete</a> 
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>      
        </table>
    </div>
</div>

<div class="col-md-4 col-md-offset-5">
    <div class="pagination">
        <?php echo $links; ?>
    </div>
</div>
