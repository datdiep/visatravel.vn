<?php

/**
 * @property Admin_Model $admin_model
 */
Class Profile extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('admin_model', 'network_model', 'role_controller_model'));
        $this->load->library(array('form_validation', 'resize_image', 'upload'));
        $this->data['pre'] = '';
        //pre($this->data['map_role_controller']);
    }

    public function index() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('password', 'Password', 'matches[repassword]');
            $this->form_validation->set_rules('repassword', 'Password Confirmation');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $update = array(
                    'phone' => $this->input->post('phone'),
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                );
                if ($_POST['password']) {
                    $update['password'] = md5($_POST['password']);
                }
                $path = PATH_UPLOAD . 'avatar/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    if ($this->data['user']['avatar']) {
                        $img_name = $this->data['user']['avatar'];
                    } else {
                        $img_name = $this->data['user']['username'] . '.jpg';
                    }

                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $update['avatar'] = $img_name;
                }
                $this->admin_model->update($update, $this->data['user']['id']);
                $this->data['check_error'] = 0;
            }
            $this->data['user'] = $this->admin_model->get_by_key($this->data['user']['id']);
            $this->data['user']['role_controller'] = $this->data['user']['role_controller'] . $this->data['default_controller'];
        }
        $this->template->write_view('content_block', 'profile/index', $this->data);
        $this->template->render();
    }

    function info() {
        $this->template->write_view('content_block', 'profile/info', $this->data);
        $this->template->render();
    }

}
