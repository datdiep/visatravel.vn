<?php

class Script_visatravel extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('country_model', 'visa_fees_model', 'news_model', 'category_news_model'));
        $this->load->library(array('resize_image', 'transload'));
        $this->load->library(array('transload', 'alias', 'resize_image', 'curl'));
    }

    function get_total($domain = 0, $cat = 0, $page = -1) {
        $this->total_config = array(
            array(
                'name' => 'visatravel',
                'curl' => true,
                'will_script' => 1,
                'domain' => 'http://visatravel.vn/',
                'prefix' => 'http://visatravel.vn',
                'row' => '.post-list',
                'last_url' => '',
                'element' => array(
                    'href' => array('tag' => 'a', 'attr' => 'href', 'index' => 0),
                    'image' => array('has_get' => TRUE, 'tag' => 'img', 'attr' => 'src', 'index' => 0),
                    'title' => array('tag' => '.entry-title', 'attr' => '.entry-title', 'index' => 0),
                    'description' => array('tag' => '.entry-content', 'attr' => '.entry-title', 'index' => 0),
                ),
                'category' => array(
                    array('url' => 'tin-du-lich/page/', 'page1' => "tin-du-lich", 'from' => 1, 'to' => 2, 'cat_id' => 6),
                )
            ),
        );
        $domain = intval($domain);
        $cat = intval($cat);

        if (isset($this->total_config[$domain]['category'][$cat])) {

            //$this->load->model(array('product_model'));
            $this->load->library(array('transload', 'alias', 'resize_image', 'curl'));
            include APPPATH . 'libraries/simple_html_dom.php';
            $config_domain = $this->total_config[$domain];
            $category = $config_domain['category'][$cat];
            if ($page == -1) {
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_visatravel/get_total/' . $domain . "-" . $config_domain['name'] . '/' . $cat . '/' . $category['to'] . '" />';
                return false;
            }

            $link = $config_domain['domain'] . $category['url'] . $page . $config_domain['last_url'];
            if ($page == 1)
                $link = $config_domain['domain'] . $category['page1'];
            echo $link . '<br/>';
            //$http_headers = get_headers($link);
            //pre($http_headers);
            //if (strpos($http_headers[0], '200 OK') >= 0) {
            if ($config_domain['curl']) {
                $header = 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>';
                $agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
                if (!empty($config_domain['param'])) {
                    pre($config_domain['param'], FALSE);
                    $reponse = $this->curl->post($link, $config_domain['param'], $header, 'http://visatravel.vn/');
                    $html = str_get_html($reponse);
                } else {
                    $html = str_get_html($this->curl->get($link, $agent, $config_domain['domain']));
                }
            } else {
                $html = file_get_html($link);
            }
            // echo $html; exit;
            $row = $html->find($config_domain['row']);

            //pre($row);
            $result = array();
            $fHref = @file_get_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_href.txt');
            $fTitle = @file_get_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_title.txt');
            $more_fhref = $more_ftitle = '';

            // @mkdir(PATH_PAGE . date('d-m-Y'), 0777);      
            $url_local = '/assets/upload/news/';
            $path = PATH_UPLOAD . 'news';
            $path_content = APPPATH . '../' . 'assets/images/';
            $url_content = "/assets/images/";
            @mkdir($path, 0777, true);

            foreach ($row as $key => $element) {
                $href = strtolower($element->find($config_domain['element']['href']['tag'], $config_domain['element']['href']['index'])->{$config_domain['element']['href']['attr']});
                // if is one attr ->innertext;
                $title = $this->removeLink($element->find($config_domain['element']['title']['tag'], $config_domain['element']['title']['index'])->innertext);
                $description = $this->remove_p($element->find($config_domain['element']['description']['tag'], $config_domain['element']['title']['index'])->innertext);
                $page_html = file_get_html($href);
                $author_username = $this->removeLink($page_html->find(".author", 0)->innertext);
                $date_create = date("Y-m-d h:i:s", strtotime($page_html->find(".entry-date", 0)->innertext));
                $tags = $page_html->find(".tags-links", 0) != "" ? substr($this->removeLink($page_html->find(".tags-links", 0)->innertext), 6) : "";
                $content_html = $page_html->find(".entry-content", 0);
                $post_connect = ",";
                foreach ($page_html->find('#main') as $e) {
                    $content_array = $e->innertext;
                }
                //get data_connect alias 
                foreach (str_get_html($content_array)->find('li') as $e) {
                    $post_connect .= $this->alias->create_alias(($this->removeLink($e->innertext))) . ",";
                }
                if ($href && $title && $description) {
                    if (strpos($fHref, $href . ',') === false && stripos($fTitle, $title . ',') === false) {
                        $fHref .= $href . ',';
                        $fTitle .= $title . ',';
                        $more_fhref .= $href . ',';
                        $more_ftitle .= $title . ',';
                        if (strpos($href, 'http://') === false && strpos($href, 'https://') === false) {
                            $href = $this->total_config[$domain]['prefix'] . $href;
                        }
                        $result[$key]['title'] = $this->removeLink($title);
                        $result[$key]['alias'] = $this->alias->create_alias($result[$key]['title']);
                        $result[$key]['cate_id'] = $category['cat_id'];
                        $result[$key]['url_source'] = $href;

                        //tri add
                        $result[$key]['description'] = $description;
                        $result[$key]['date_create'] = $date_create;
                        $result[$key]['author_username'] = $author_username;
                        $result[$key]['tags'] = $tags;
                        $result[$key]['type'] = 'news';
                        // post_connect
                        $result[$key]['post_connect'] = $post_connect;
                        // get all img in content
                        foreach (str_get_html($page_html)->find('img') as $e) {
                            $content_image[] = $e->src;
                        }
                        // change data image in content and get image to server
                        foreach ($content_image as $img_link) {
                            $image_name = $this->save_image($img_link, $path_content);
                            $image_local = $url_content . "/" . $image_name;
                            if ($image_local != "")
                                $content_html = str_replace($img_link, $image_local, $content_html);
                        }
                        $result[$key]['content'] = $content_html;
                        if ($config_domain['element']['image']['has_get']) {
                            if (strlen($result[$key]['alias']) > 50) {
                                $img_name = substr($result[$key]['alias'], 0, 50);
                            } else {
                                $img_name = $result[$key]['alias'];
                            }

                            $img = @$element->find($config_domain['element']['image']['tag'], $config_domain['element']['image']['index'])->$config_domain['element']['image']['attr'];
                            $img = str_replace(' ', '%20', $img);
                            //$img = str_replace('https://', 'http://', $img);
                            //echo $img;
                            if (!empty($img)) {
                                $contents_img = $this->GetImageFromUrl($img);
                                $savefile = fopen($path . '/' . $img_name . '.jpg', 'w');
                                fwrite($savefile, $contents_img);
                                fclose($savefile);
                                $this->resize_image->crop_resize($path . '/' . $img_name . '.jpg', $path . '/' . $img_name . '.jpg', 292, 190);
                                $result[$key]['image'] = $img_name . '.jpg';
                            }
                        }
                    }
                }
            }
            //}

            $count_result = count($result);
            if ($more_fhref) {
                file_put_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_href.txt', $more_fhref, FILE_APPEND);
                file_put_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_title.txt', $more_ftitle, FILE_APPEND);
            }
//            foreach ($result as $v) {
//                $this->page_model->insert($v);
//            }

            if ($count_result) {
                $this->news_model->insert_batch($result);
            }
        }

        echo '<pre>';
        print_r($result);
        if ($page == $this->total_config[$domain]['category'][$cat]['from']) {
            if (isset($this->total_config[$domain]['category'][++$cat])) {
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_visatravel/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . $this->total_config[$domain]['category'][$cat]['to'] . '" />';
            } elseif (isset($this->total_config[++$domain])) {
                $cat = 0;
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_visatravel/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . $this->total_config[$domain]['category'][$cat]['to'] . '" />';
            } else {
                //pre($result);
                echo 'Lấy thanh công == ( ^_^ ) == ';
                // $link_redirect = '/' . ADMIN_URL . 'script_visatravel/get_total';
                // echo '<meta http-equiv="refresh" content="0; url=' . $link_redirect . '" />';
            }
        } else {
            echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_visatravel/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . ( --$page) . '" />';
        }
    }

    private function removeLink($content) {
        $html = str_get_html($content);
        // link content
        foreach ($html->find('a') as $e) {
            $e->outertext = $e->innertext;
        }
        $ret = $html->save();
        return $ret;
    }

    private function remove_p($content) {
        $html = str_get_html($content);
        // link content
        foreach ($html->find('p') as $e) {
            $e->outertext = $e->innertext;
        }
        $ret = $html->save();
        return $ret;
    }

    function GetImageFromUrl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    // function xoa phan tu html cuoi cung
    private function removeLastElement($content, $element) {
        $html = str_get_html($content);
        // link content
        $html->find($element, -1)->outertext = '';
        $ret = $html->save();
        return $ret;
    }

    // function xoa phan tu html truyen vao dau tien
    private function removeFirstElement($content, $element) {
        $html = str_get_html($content);
        $html->find($element, 0)->outertext = '';
        $ret = $html->save();
        return $ret;
    }

    //remove the p
    //tri add code // get all category wordpress
    public function get_data_content() {
//
        include APPPATH . 'libraries/simple_html_dom.php';
//
        $html = file_get_html("http://visatravel.vn/visa-nhap-canh/dich-vu-gia-han-visa-viet-nam-cho-nguoi-campuchia.html");
////        //get number page in category
//        foreach ($html->find('#main') as $e) {
//            $content_array = $e->innertext;
//        }
        // get data_connect
//        foreach (str_get_html($content_array)->find('li') as $e) {
//            $content[] = $e->innertext;
//        }
//        foreach (str_get_html($content_array)->find('.nav-links') as $element)
//            $content_results = $element->plaintext;
//        $page_end = $this->removeLink($content_results);
//        $num_page = str_replace(" ", "", $page_end);
//        $num_page = explode("Page", $num_page);
//        $num_page = $num_page[count($num_page) - 1];
//        for ($i = 1; $i <= (int) $num_page; $i++) {
//            echo "========= Get 10 data in page: " . $i . " =============";
//            if ($i == 1)
//                $html = "http://visatravel.vn/visa-xuat-canh";
//            else
//                $html = "http://visatravel.vn/visa-xuat-canh/page/" . $i;
//            $data_page = $this->get_onepage($html);
//            foreach ($data_page as $item_post) {
//                $this->news_model->insert($item_post);
//            }
//            echo "-----------End get page" . $i . "---------";
//        }
//        exit;
//    }
//
//    function get_onepage($url) {
//        $html = file_get_html($url);
//        /// setting post before get_page
//        $path = APPPATH . '../assets/upload/news/visa-nhap-canh';
//        $url_local = base_url() . 'assets/upload/news/visa-nhap-canh/';
//        @mkdir($path, 0777, true);
//        $cate_id = 1;
//        $type = 'news';
//        $author_id = -1;
//        // get one page
//        foreach ($html->find('#main') as $e) {
//            $content_array = $e->innertext;
//        }
//        //get list post in page to get descript_visatravelion and link herf every post
//        foreach (str_get_html($content_array)->find('.post-list') as $element)
//            $content_results[] = $element->innertext;
//        // get every post
//        foreach ($content_results as $key => $value) {
//            foreach (str_get_html($value)->find('img') as $element) {
//                $image_name = $this->save_image($element->src, $path);
//                $content_all[$key]['image'] = $image_name;
//            }
//            foreach (str_get_html($value)->find('.entry-content') as $element)
//                $content_all[$key]['descript_visatravelion'] = str_replace("  ", "", $this->remove_p($element->innertext));
//            // get href and get data one post
//            foreach (str_get_html($value)->find('a') as $element) {
//                $page_html = file_get_html($element->href);
//                            // get author
//        $author_username = $page_html->find(".author", $config_domain['element']['title']['index'])->innertext;
//        //get date_create
//        $date_create = date("Y-m-d h:i:s", strtotime($page_html->find(".entry-date", 0)));
//        //get data from position 6 to remove "tags: " tag EX tags:  du lịch,tour du lịch Mỹ
//        $tags = substr($this->removeLink($page_html->find(".tags-links", 0)), 6);
//        //get content
//        $content_html = $page_html->find(".entry-content", 0);
//                foreach (str_get_html($content_html)->find('img') as $e) {
//                    $content_image[] = $e->src;
//                }
//                // change data image in content and get image to server
//                foreach ($content_image as $img_link) {
//                    $image_name = $this->save_image($img_link, $path);
//                    $image_local = $url_local . $image_name;
//                    if ($image_local != "")
//                        $content_html = str_replace($img_link, $image_local, $content_html);
//                }
//                $content_all[$key]['content'] = $content_html;
//                // set array insert
//                $content_all[$key]['cate_id'] = $cate_id;
//                $content_all[$key]['status'] = 1;
//                $content_all[$key]['type'] = $type;
//                $content_all[$key]['author_id'] = $author_id;
//            }
//        }
//        return $content_all;
    }

//    //get data tri add
    function get_entry($html, $attr) {
//        foreach ($html->find("$attr") as $e) {
//            $content = $e->innertext;
//        }
//        return $content;
    }

//
    function save_image($img, $path) {
        $img_url = $img;
        // get name image position last after "/"
        $image_ = explode("/", $img);
        $img_name = $this->alias->create_alias($image_[count($image_) - 1]);
        $img_name = str_replace("-jpg", ".jpg", $img_name);
        $img_name = str_replace("-png", ".png", $img_name);
        $contents_img = $this->GetImageFromUrl($img_url);
        $savefile = fopen($path . '/' . $img_name, 'w');
        fwrite($savefile, $contents_img);
        fclose($savefile);
        return $img_name;
    }

// input array alias (,title-a,title b,)
    function get_ids($alias) {
        if ($alias_cate && $alias_detail) {
            $url_old = "http://visatravel.vn/" . $alias_cate . "/" . $alias_detail . '.html';
            $item_detail = $this->news_model->get_row_by("url_source = '" . $url_old . "'");
            if ($item_detail)
                redirect(base_url() . $item_detail['alias'] . '-' . $item_detail['id'] . ".html");
            else
                redirect(base_url());
        } else
            redirect(base_url());
    }

    function convert_postconnet() {
        $data_news = $this->news_model->get_all();

        foreach ($data_news as $news) {
            $news_ids = ",";
            if (!empty($news['post_connect'])) {
                // get array post_connect == delete key "," first and last in string
                $array_alias = explode(",", substr($news['post_connect'], 1, -1));
                foreach ($array_alias as $alias) {
                    $item = $this->news_model->get_row_by("alias = '" . $alias . "'");
                    if ($item)
                        $news_ids .= $item['id'] . ",";
                }
            }
            $news['news_ids'] = $news_ids;
            // print_r($news['post_connect'] . " -- " . $news_ids . "</br>");
            $this->news_model->update($news, $news['id']);
            echo 'update_ids ' . $news['title'] . " : " . $news_ids . "</br>";
        }
    }

}
