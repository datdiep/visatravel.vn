<?php

class Call_back extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('call_back_model'));
    }

    public function index() {
        $this->data['pos'] = 0;
        $this->data['pre'] = 'call_back';
        $this->template->write_view('content_block', 'call_back/index', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->call_back_model->delete($id);
    }

    public function update() {
        $id = intval(@$_POST['id']);
        $this->call_back_model->update(array('status' => 1), $id);
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $cond = 'id > 0';
        if ($_POST['status'] != "")
            $cond .= ' and status like ' . $_POST['status'];
        $limit = 15;
        $order = 'id DESC';
        $data['results'] = $this->call_back_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->call_back_model->get_total_rows($cond);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('call_back/list', $data);
        return false;
    }

}
