<?php

class Ajax extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
    }

    function get_for_search() {
        $this->load->model('product_model');
        $data = $this->product_model->get_for_search(null);
        echo json_encode($data);
    }

    function updata_chat() {
        $msg = $_POST['msg'];
        $id = intval(@$_POST['id']);
        $model = $_POST['model'];
        $url = @$_POST['url'];
        $this->load->model($model);

        if ($url) {
            $data = $this->$model->get_by_key($id);

            $data['log'] = @unserialize($data['log']);

            $url = str_replace(base_url(), '', $url);
            $controller = explode('_', $model);
            $controller = $controller[0];
            $datalog = unique_multidim_array($data['log'], 'username');
            // Load các thành viên trong nhóm chat trừ bản thân mình ra (Gửi leader thì chưa làm)
            foreach ($datalog as $key => $value) {
                if ($value['username'] != $this->data['user']['username']) {
                    if ($controller == 'task')
                        $content = $this->data['user']['username'] . ' đã thảo luận công việc của ' . $data['user_create'];
                    if ($controller == 'customer')
                        $content = $this->data['user']['username'] . ' đã thảo luận về khách hàng ' . $data['name'];
                    if ($controller == 'order')
                        $content = $this->data['user']['username'] . ' đã thảo luận hồ sơ đang xử lý khách hàng ' . $data['name'];

                    $this->addnotify($content, $url, $value['username']);
                }
            }
        }

        $data['p'] = $this->$model->log($msg, $id, 1);
        $this->load->view('chat', $data);
        return false;
    }

    function del_file() {

        $id = intval(@$_POST['id']);
        $model = $_POST['model'];
        $name = $_POST['name'];
        $this->load->model($model);
        $data = $this->$model->get_by_key($id);
        if (!empty($data) && (strpos($data['network_id'], $this->data['user']['network_id']) !== false || !empty($this->data['user']['role_network']))) {
            $path = PATH_UPLOAD . 'data/' . $data['folder'] . '/';
            @unlink($path . $name);
            echo 1;
        } else {
            echo 0;
        }
    }


}
