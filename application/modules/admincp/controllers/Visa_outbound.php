<?php

/**
 * @property Video_model $order_services_model
 */
class Visa_outbound extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('order_services_model', 'order_sku_model','network_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'visa_outbound';
        $this->data['msg_error'] = $this->session->flashdata('msg_error');
    }

    function index($network_id = 0) {
        if(empty($this->data['user']['role_network']))
            redirect(base_url('admincp/visa-outbound/'.$this->data['user']['network_id']));
        else
            $this->data['networks'] = $this->network_model->get_all();
        $network = $this->network_model->get_by_key($network_id);
        if(empty($network))
            redirect(base_url('admincp/visa-outbound/'.$this->data['user']['network_id']));
        $this->data['web_title'] = 'Hồ sơ khách hàng đang xử lý';
        $this->data['pos'] = intval(@$_COOKIE['order_pos']);
        $this->data['status'] = (@$_COOKIE['order_status']);
        $this->data['network_id'] = $network_id;
        $this->template->write_view('content_block', 'visa_outbound/index', $this->data);
        $this->template->render();
    }
    function add() {
        $this->data['pre'] = 'order_add';
        $this->data['web_title'] = 'Thêm hồ sơ đang xử lý';
        $this->data['check_error'] = -1;

        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['staff'] = $this->admin_model->get_by($cond);

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('full_name', 'full_name', 'required');
            $this->form_validation->set_rules('phone', 'phone', 'required');
            $this->form_validation->set_rules('services', 'services', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $add = @$_POST;

                $add['date_create'] = date('Y-m-d H:i:s');
                $add['last_update'] = date('Y-m-d H:i:s');
                $add['appointment'] = date('Y-m-d', strtotime($_POST['appointment']));
                $add['network_id'] = ',' . $this->data['user']['network_id'] . ',';
                unset($add['submit']);

                $this->data['check_error'] = 0;
                $new_customer = $this->order_services_model->insert($add);

                if ($this->data['root_network']['username']) {
                    $url = 'admincp/customer/edit/' . $new_customer;
                    $msg_send = $this->data['user']['username'] . ' vừa thêm hóa đơn dịch vụ cho khách hàng ' . $this->input->post('name');
                    $this->addnotify($msg_send, $url, $this->data['root_network']['username']);
                }
            }
        }

       
        $this->template->write_view('content_block', 'visa_outbound/add', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $status = @$_POST['status'];
        $keyword = @$_POST['keyword'];
        $network_id = $_POST['network_id'];
        if (empty($this->data['user']['role_network'])) {
            $cond = 'network_id like "%,' . $this->data['user']['network_id'] . ',%"';
            if (empty($this->data['user']['role_department'])) {
                $cond .= ' and (who_consultant ='.$this->data['user']['username'];
                $cond .= ' or who_handle like "%,' . $this->data['user']['username'] . ',%")';
            }
        }else{
            $cond = 'network_id like "%,' . $network_id . ',%"';
        }
        if ($keyword)
            $cond .= " and (phone = '$keyword' or email like '%$keyword%')";
        else{
            if ($status)
                $cond .= ' and status = "' . $status . '"';
        }
        //echo $cond.'<br>'; exit;
        $limit = 20;
        $order = 'status DESC, id DESC';
        $this->data['results'] = $this->order_services_model->get_for_page($limit, $pos, $cond, $order);
        //pre($data['results']);
        $total = $this->order_services_model->get_total_rows($cond);
        setcookie("order_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_status", $status, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('visa_outbound/list', $this->data);
        return false;
    }


    function trash() {
        $id = intval(@$_POST['id']);
        $this->check_role_order($id, 'trash');
        if (!$this->check_role_order($id, 'trash'))
            return false;
        $this->order_services_model->update(array('status' => 'trash'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa');
        echo 1;
    }

    function trash_empty() {
        $id = intval(@$_POST['id']);
        if (!$this->check_role_order($id, 'empty'))
            return false;
        $this->order_services_model->delete($id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa hoàn toàn');
        echo 1;
    }

    private function check_role_order($id, $role_order_progress) {
        $order = $this->order_services_model->get_by_key($id);
        if (empty($order))
            return false;
        if (!empty($this->data['user']['role_network']) || $role_order_progress == 'view')
            return true;
        if (strpos(',' . $order['network_id'] . ',', ',' . $this->data['user']['network_id'] . ',') === false)
            return false;
        if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',' . $role_order_progress . ',') === false)
            return false;
        return true;
    }

    function update_passport() {
        $data = @$_POST['data'];
        $result = array();
        if (!$this->check_role_order($data['order_id'], 'check'))
            return false;
        if ($data['id'] == '' || empty($data['id'])) {
            unset($data['id']);
            $id = $this->order_sku_model->insert($data);
        } else {
            $order_sku = $this->order_sku_model->get_by_key($data['id']);
            if (empty($order_sku))
                return false;
            if (!empty($data['nationality'])) {
                $this->load->model('country_model');
                $country_old = $this->country_model->get_row_by(array('name' => $order_sku['nationality']));
                $country_new = $this->country_model->get_row_by(array('name' => $data['nationality']));
                $nationality_fees = $country_new['special_fee'] - $country_old['special_fee'];
                if ($nationality_fees != 0) {
                    $order = $this->order_services_model->get_by_key($data['order_id']);
                    $result['nationality_fees'] = $order['nationality_fees'] + $nationality_fees;
                    $result['total_fees'] = $order['total_fees'] + $nationality_fees;
                    $this->order_services_model->update($result, $data['order_id']);
                    $data['special_fee'] = $country_new['special_fee'];
                }
            }
            $this->order_sku_model->update($data, $data['id']);
        }
        echo json_encode($result);
    }

}
