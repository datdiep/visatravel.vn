<?php

class Dashboard extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('order_services_model', 'customer_model', 'note_model', 'news_model', 'quotations_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload', 'my_pagination', 'user_agent'));
        $this->data['pre'] = 'dashboard';
        $this->data['pre1'] = 'dashboard';
        $this->limit = 10;
    }

    private function check_role_order($id, $role_order_progress) {
        $order = $this->order_services_model->get_by_key($id);
        if (empty($order))
            return false;
        if (!empty($this->data['user']['role_network']) || $role_order_progress == 'view')
            return true;
        if (strpos(',' . $order['network_id'] . ',', ',' . $this->data['user']['network_id'] . ',') === false)
            return false;
        if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',' . $role_order_progress . ',') === false)
            return false;
        return true;
    }

    public function index() {

        if (strpos(',' . $this->data['user']['role_controller'] . ',', ',customer,') !== FALSE) {
            $this->data['block_call_log'] = $this->block_call_log();
        }

        $this->data['block_note'] = $this->block_note();
        $this->data['block_quotations'] = $this->block_quotations();
        $this->data['block_news'] = $this->block_news();

        $this->data['latest_news'] = $this->news_model->get_row_by(array('network_id' => $this->data['user']['network_id']), 'id DESC');
        $this->template->write_view('content_block', 'dashboard', $this->data);
        $this->template->render();
    }

    public function news_detail($id) {

        $this->load->model(array('news_model', 'category_news_model'));

        $id = intval($id);
        $this->data['check_error'] = -1;

        $this->data['news'] = $this->news_model->get_by_key($id);
        if ($this->data['news']['news_ids'] != ',') {
            $temp = '(' . trim($this->data['news']['news_ids'], ',') . ')';
            $cond = 'id   in' . $temp;
            $this->data['link_news'] = $this->news_model->get_by($cond, null, 'id');
        }

        $this->load->view('news/detail', $this->data);
    }

    public function block_quotations() {
        $this->data['quotations'] = $this->quotations_model->get_row_by(NULL, 'rand()');
        return $this->load->view('block/quotations', $this->data, true);
    }

    public function block_call_log() {
        return $this->load->view('block/call_log', $this->data, true);
    }

    public function block_news() {
        $page = intval(@$page);
        $page < 1 ? 1 : $page;
        $cond = '';
        $this->data['list_news'] = $this->news_model->get_for_page($this->limit, 0, $cond, 'id DESC');
        $total_row = $this->news_model->get_total_rows($cond);
        $url = '/admincp/dashboard/';

        $this->data['link'] = $this->get_page($page, $total_row, $this->limit, $url);
        return $this->load->view('block/news', $this->data, true);
    }

    public function block_note() {
        $page = intval(@$page);
        $page < 1 ? 1 : $page;
        $cond = 'status = 0 and user_create = "' . $this->data['user']['username'] . '"';
        $this->data['list_note'] = $this->note_model->get_for_page($this->limit, 0, $cond, 'id DESC');
        $total_row = $this->note_model->get_total_rows($cond);
        $url = '/admincp/dashboard/';
        $this->data['link'] = $this->get_page($page, $total_row, $this->limit, $url);
        return $this->load->view('block/note', $this->data, true);
    }

    function save_note() {
        $id = intval(@$_POST['id']);
        $data = @$_POST['data'];
        $color = array('box-info', 'box-primary', 'box-warning', 'box-success', 'box-danger');
        if (empty($id)) {
            $data['user_create'] = $this->data['user']['username'];
            $data['date_create'] = date('Y-m-d H:i:s');
            $data['network_id'] = $this->data['user']['network_id'];
            $data['status'] = 0;
            $data['class_color'] = $color[array_rand($color)];
            $id = $this->note_model->insert($data);
        } else {
            $note = $this->note_model->get_by_key($id);
            if (!empty($note) && $note['user_create'] == $this->data['user']['username']) {
                $data['user_create'] = $this->data['user']['username'];
                $data['network_id'] = $this->data['user']['network_id'];
                $this->note_model->update($data, $id);
            }
        }
        echo $id;
    }

    function del_note() {
        $id = intval(@$_POST['id']);
        $note = $this->note_model->get_by_key($id);
        if (!empty($note) && $note['user_create'] == $this->data['user']['username']) {
            $this->note_model->update(array('status' => 1), $id);
            echo 1;
            exit;
        }
        echo 0;
    }

    function empty_note() {
        $id = intval(@$_POST['id']);
        $note = $this->note_model->get_by_key($id);
        if (!empty($note) && $note['user_create'] == $this->data['user']['username']) {
            $this->note_model->delete($id);
            echo 1;
            exit;
        }
        echo 0;
    }

}
