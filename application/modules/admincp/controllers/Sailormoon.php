<?php

class Sailormoon extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('country_model','services_model','mind_service_model','checklist_model','mind_handle_model','mind_question_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload'));
        $this->data['pre'] = 'service_fees';
        $this->data['group_services'] = field_enums('services', 'group');
        $this->data['services'] = $this->services_model->get_all(null,'alias');
    }
    public function build_services(){
        $id = $_POST['id'];
        $data = @$_POST['data'];   
        if(!empty($data)){
            if(!empty($data['select']))
                $data['select'] = serialize($data['select']);
            if ($id == 0){
                $id = $this->services_model->insert($data);
                $this->data['add_service'] = 1;
            } else {
                $this->services_model->update($data, $id);
                $this->data['edit_service'] = 1;
            }
            $this->data['msg'] = 'Dịch vụ lưu thành không lúc '.date('H:s:i');
        }
        if($id > 0){
            $this->data['service'] = $this->services_model->get_by_key($id);
            if(!empty($this->data['service']['select']))
                $this->data['service']['select'] = unserialize($this->data['service']['select']);
        }
        $this->load->view('sailormoon/services', $this->data);
        return false;
    }
    public function training() {
        $this->data['check_error'] = -1;
        $alias = @$_GET['service'];
        $this->data['service'] = $this->services_model->get_row_by(array('alias' => $alias));
        if(empty($this->data['service'])){
            redirect(base_url('admincp/sailormoon'));
            return false;
        }
        if(!empty($this->data['service']['select']))
                $this->data['service']['select'] = unserialize($this->data['service']['select']);
        $id = 0;
        if (isset($_POST['submit'])) {
            $array_case = [];
            foreach ($_POST['case_key'] as $k => $v) {
                $array_case[$v] = $_POST['case_value'][$k];
            }
            $mind_case = serialize($array_case);
            $service = $this->mind_service_model->get_row_by(array('service' => $_GET['service'], 'mind_case' => $mind_case));
            if(empty($service)){
                if(!empty($_POST['mind_handled_key'])){
                    $array_handle = [];
                    foreach ($_POST['mind_handled_key'] as $k => $v) {
                        $array_handle[$v] = $_POST['mind_handled_value'][$k];
                    }
                    $mind_handle= serialize($array_handle);
                }else{
                    $mind_handle = '';
                }
                $add = array(
                    'service' => $_GET['service'],
                    'mind_case' => $mind_case,
                    'mind_question' => $this->input->post('mind_question'),
                    'mind_handle' => $mind_handle,
                    'fee' => $this->input->post('fee'),
                    'fee_consul' => $this->input->post('fee_consul'),
                    'note' => $this->input->post('note'),
                    'checklist' => $this->input->post('checklist'),
                );
                $this->data['check_error'] = 0;
                $id = $this->mind_service_model->insert($add);
            }else{
                $this->data['check_error'] = 1;
                $this->data['msg'] = 'Trường hợp này đã tồn tại rùi';
            }
        }
        //pre($this->data['service']);
        $this->data['load_file'] = $this->load_block_file('mind_service_model', $id);
        $this->template->write_view('content_block', 'sailormoon/training', $this->data);
        $this->template->render();
    }
    public function training_edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['mind_service'] = $this->mind_service_model->get_by_key($id);
        if(empty($this->data['mind_service'])){
            redirect(base_url('admincp/sailormoon'));
            return false;
        }
        $this->data['service'] = $this->services_model->get_row_by(array('alias' => $this->data['mind_service']['service']));
        if(!empty($this->data['service']['select']))
                $this->data['service']['select'] = unserialize($this->data['service']['select']);
        if (isset($_POST['submit'])) {
            $array_case = [];
            foreach ($_POST['case_key'] as $k => $v) {
                $array_case[$v] = $_POST['case_value'][$k];
            }
            $mind_case = serialize($array_case);
            $service = $this->mind_service_model->get_row_by(array('service' => $_GET['service'], 'mind_case' => $mind_case));
            if(empty($service)){
                if(!empty($_POST['mind_handled_key'])){
                    $array_handle = [];
                    foreach ($_POST['mind_handled_key'] as $k => $v) {
                        $array_handle[$v] = $_POST['mind_handled_value'][$k];
                    }
                    $mind_handle= serialize($array_handle);
                }else{
                    $mind_handle = '';
                }
                $add = array(
                    'service' => $_GET['service'],
                    'mind_case' => $mind_case,
                    'mind_question' => $this->input->post('mind_question'),
                    'mind_handle' => $mind_handle,
                    'fee' => $this->input->post('fee'),
                    'fee_consul' => $this->input->post('fee_consul'),
                    'note' => $this->input->post('note'),
                    'checklist' => $this->input->post('checklist'),
                );
                $this->data['check_error'] = 0;
                $id = $this->mind_service_model->insert($add);
            }else{
                $this->data['check_error'] = 1;
                $this->data['msg'] = 'Trường hợp này đã tồn tại rùi';
            }
        }
        //pre($this->data['service']);
        $this->data['load_file'] = $this->load_block_file('mind_service_model', $id);
        $this->template->write_view('content_block', 'sailormoon/training_edit', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['mind_service_pos'] = intval(@$_COOKIE['mind_service_pos']);
        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];
        $this->data['results'] = $this->mind_service_model->get_all();
        $this->template->write_view('content_block', 'sailormoon/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;

        if (isset($_POST['submit'])) {
            $service = $this->mind_service_model->get_row_by(array('services' => $_POST['services'],'type_visa' => $_POST['type_visa'],'exp_service' => $_POST['exp_service']));
            if(empty($service) || $service['id'] == $id){
                $add_service = @implode(',', $_POST['add_service']);
                if(!empty($add_service))
                    $add_service = ','.$add_service.',';
                $checklist_id = @implode(',', $_POST['checklist_id']);
                if(!empty($checklist_id))
                    $checklist_id = ','.$checklist_id.',';
                $update = array(
                    'services' => $this->input->post('services'),
                    'exp_service' => $this->input->post('exp_service'),
                    'type_visa' => $this->input->post('type_visa'),
                    'note' => $this->input->post('note'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'add_service' => $add_service,
                    'author_update' => $this->data['user']['username'],
                    'fee' => $this->input->post('fee'),
                    'fee_net' => $this->input->post('fee_net'),
                    'checklist_id' => $checklist_id
                );
                $this->data['check_error'] = 0;
                $this->mind_service_model->update($update,$id);
            }else{
                $this->data['check_error'] = 1;
                $this->data['msg'] = 'Trường hợp này đã tồn tại rùi, vui lòng chọn trường hợp khác';
            }
            
        }

        $this->data['service'] = $this->mind_service_model->get_by_key($id);
        //$this->data['load_file'] = $this->load_block_file('mind_service_model', $id);
        $this->template->write_view('content_block', 'service_fees/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->mind_service_model->delete($id);
    }
    public function del_services() {
        $id = intval(@$_POST['id']);
        $this->services_model->delete($id);
    }
    function load_mind_service() {
        $pos = intval(@$_REQUEST['pos']);
        $limit = 20;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $this->data['mind_services'] = $this->mind_service_model->get_for_page($limit, $pos, null, $order);
        $total = $this->mind_service_model->get_total_rows(null);
        setcookie("mind_service_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('sailormoon/load_mind_service', $this->data);
        return false;
    }
    function load_question() {
        $this->load->model('mind_question_model');
        $pos = intval(@$_POST['pos']);
        $cond = "";
//        if (!empty($_POST['service'])) {
//            $service = str_replace('"', ",", $_POST['service']); 
//            $cond = 'group_service like "%' . $service . '%"';
//        }
        $limit = 10;
        $order = 'id desc';
        $this->data['results'] = $this->mind_question_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->mind_question_model->get_total_rows($cond);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('sailormoon/list_question', $this->data);
        return false;
    }
    function load_handle() {
        $this->load->model('mind_handle_model');
        $pos = intval(@$_POST['pos']);
        $cond = null;
        if (!empty($_POST['service'])) {
            $service = str_replace('"', ",", $_POST['service']); 
            $cond = 'group_service like "%' . $service . '%"';
        }
        $limit = 5;
        $order = 'id desc';
        $this->data['results'] = $this->mind_handle_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->mind_handle_model->get_total_rows($cond);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('sailormoon/list_handle', $this->data);
        return false;
    }
    function add_edit_handle() {
        $data = @$_POST['data'];
        if ($data['is_special'] != '') {
            if($data['is_special'] == 'VOA'){
                $data_temp = array(
                    'services' => $data['is_special'],
                    'type_of_service' => $data['type_of_service'],
                    'purpose_of_visit' => $data['purpose_of_visit'],
                );
            }else{
                $data_temp = array(
                    'services' => $data['is_special'],
                    'type_of_service' => $data['type_of_service'],
                );
            }
            $data['is_special'] = serialize($data_temp);
        }
        unset($data['type_of_service']);
        unset($data['purpose_of_visit']);
        unset($data['type_of_visit']);
        //pre($data,false);       
        if (empty($data['id']) || $data['id'] == ''){
            unset($data['id']);
            $data['publish_date'] = date('Y-m-d H:i:s');
            $data['author'] = $this->data['user']['username'];
            $id = $this->mind_handle_model->insert($data);
        } else {
            $data['last_update'] = date('Y-m-d H:i:s');
            $data['author_update'] = $this->data['user']['username'];
            $this->mind_handle_model->update($data, $data['id']);
            $id = $data['id'];
        }
        $handle = $this->mind_handle_model->get_by_key($id);
        echo json_encode($handle);
    }
    function add_edit_question() {
        $data = @$_POST['data'];
        //pre($data,false);   
        if(empty($data)){
            echo json_encode(array('error' => 1));
            exit;
        }
        if (empty($data['id']) || $data['id'] == ''){
            unset($data['id']);
            $id = $this->mind_question_model->insert($data);
        } else {
            $this->mind_question_model->update($data, $data['id']);
            $id = $data['id'];
        }
        $question = $this->mind_question_model->get_by_key($id);
        echo json_encode($question);
    }
    function load_checklist(){
        $service = str_replace('"', ",", $_POST['service']); 
        $cond = 'group_service like "%' . $service . '%"';
        $results = $this->checklist_model->get_by($cond);
        $html = '';
        foreach ($results as $v) {
            $html .= '<option value="'.$v['id'].'">'.$v['title'].'</option>';
        }
        echo $html;
    }
    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond = "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function document_print($id) {

        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);

        $this->data['page'] = $this->mind_service_model->get_by_key($id);
        $this->load->view('service_fees/print', $this->data);
    }

    function sendmail($id) {
        $this->data['check_error'] = -1;
        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);
        $this->data['page'] = $this->mind_service_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('mind_service_model', $id);
        $this->template->write_view('content_block', 'service_fees/sendmail', $this->data);
        $this->template->render();
    }

    function test() {
        $this->load->library('pdf');

        if ($_POST) {
            $params = array();
            foreach ($_POST as $k => $v) {
                $k = str_replace(array('__', '_'), array('.', ' '), $k);
                $params[$k] = $v;
            }
            //echo '<pre>';
            //print_r($params);exit;
            if (fillFormPdf(APPPATH . '../assets/pdf/956a.pdf', $params, APPPATH . '../assets/pdf/deploys/956aoutput.pdf') == 1) {
                echo '<a href="./deploys/956aoutput.pdf?t=' . time() . '" target="_blank">Check</a>';
            } else {
                echo 'Fail';
            }
        }

        $this->load->view('service_fees/test');
    }

}
