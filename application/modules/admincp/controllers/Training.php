<?php

class Training extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('training_cat_model', 'network_model', 'training_chapter_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload', 'my_category', 'resize_image'));
        $this->data['status_update'] = field_enums('training_chapter', 'status');
        $this->data['pre'] = 'training';
    }

    public function index() {
        $this->data['pos'] = intval(@$_COOKIE['pos_page']);
        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];
        $this->data['training_cat'] = $this->training_cat_model->get_by('parent = 0', 'id DESC');

        $this->template->write_view('content_block', 'training/index', $this->data);
        $this->template->render();
    }

    public function lesson($id) {
        $this->data['training_cat'] = $this->training_cat_model->get_by_key($id);
        $this->data['topic'] = $this->training_chapter_model->get_by('cat_id = ' . $id);

        $this->template->write_view('content_block', 'training/lesson', $this->data);
        $this->template->render();
    }

    function chapter($id) {

        $this->data['chapter'] = $this->training_chapter_model->get_by_key($id);

        $this->data['training_cat'] = $this->training_cat_model->get_by_key($this->data['chapter']['cat_id']);
        $this->data['chapter_list'] = $this->training_chapter_model->get_by('cat_id = ' . $this->data['chapter']['cat_id']);

        $this->template->write_view('content_block', 'training/chapter', $this->data);
        $this->template->render();
    }

    function lesson_process($id) {
        $this->data['check_error'] = -1;
        $pra = $this->uri->uri_to_assoc(2);

        if (isset($_POST['submit'])) {

            unset($_POST['submit']);
            $this->form_validation->set_rules('name', 'Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $data = $_POST;
                $data['date_create'] = date('Y-m-d');
                $data['user_create'] = $this->data['user']['username'];

                $path = PATH_UPLOAD . 'training/';
                $img_hide = $this->input->post('photo');

                if ($img_hide != '') {
                    $img_name = $data['alias'] . '-' . rand(1, 9999) . '.jpg';
                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $data['image'] = $img_name;
                }

                if ($data['post_type'] == 'add') {
                    $this->data['check_error'] = 0;
                    unset($data['post_type']);
                    unset($data['photo']);
                    $this->training_cat_model->insert($data);
                } else {
                    unset($data['photo']);
                    unset($data['post_type']);
                    $this->training_cat_model->update($data, $id);
                }
            }
        }

        if ($pra[$id] == 'add') {

            $this->data['training_cat'] = $this->training_cat_model->get_by_key($id);
        } else {
            $this->data['lesson'] = $this->training_cat_model->get_by_key($id);

            $this->data['training_cat'] = $this->training_cat_model->get_by('id = ' . $this->data['lesson']['id']);
        }
        $this->data['list_cat'] = $this->training_cat_model->get_by('parent = 0', 'id DESC');
       

        $this->template->write_view('content_block', 'training/lesson_process', $this->data);
        $this->template->render();
    }

    function chapter_process($id) {

        $pra = $this->uri->uri_to_assoc(2);

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {

            unset($_POST['submit']);
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('content', 'Content', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $data = $_POST;
                $data['date_create'] = date('Y-m-d');
                $data['user_create'] = $this->data['user']['username'];

                if ($data['post_type'] == 'add') {
                    $this->data['check_error'] = 0;
                    unset($data['post_type']);
                    $this->training_chapter_model->insert($data);
                } else {
                    unset($data['post_type']);
                    $this->training_chapter_model->update($data, $id);
                }
            }
        }


        if ($pra[$id] == 'add') {
            $this->data['training_cat'] = $this->training_cat_model->get_by_key($id);
            $this->data['lesson'] = $this->training_chapter_model->get_by('cat_id = ' . $id);
        } else {
            $this->data['chapter'] = $this->training_chapter_model->get_by_key($id);
            $this->data['training_cat'] = $this->training_cat_model->get_by_key($this->data['chapter']['cat_id']);
            $this->data['lesson'] = $this->training_chapter_model->get_by('cat_id = ' . $this->data['chapter']['cat_id']);
        }


        $this->template->write_view('content_block', 'training/chapter_process', $this->data);
        $this->template->render();
    }

    public function add() {

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $folder = $this->folder_model->get_by_key($this->input->post('folder_id'));
                $add = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'content' => $this->input->post('content'),
                    'youtube' => $this->input->post('youtube'),
                    'publish_date' => date('Y-m-d H:i:s'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags'),
                    'folder_id' => $this->input->post('folder_id'),
                    'folder_name' => $folder['name'],
                    'network_id' => $this->data['user']['network_id'],
                );
                $this->data['check_error'] = 0;
                $this->document_model->insert($add);
            }
        }
        $this->template->write_view('content_block', 'training/add', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;

        if (isset($_POST['submit']) && $_POST['submit'] != '-1') {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $folder = $this->folder_model->get_by_key($this->input->post('folder_id'));
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'content' => $this->input->post('content'),
                    'youtube' => $this->input->post('youtube'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags'),
                    'folder_id' => $this->input->post('folder_id'),
                    'folder_name' => $folder['name'],
                );

                //$this->data['check_error'] = 0;
                $this->document_model->update($update, $id);
                $this->data['check_error'] = 0;
            }
        }

        $this->data['page'] = $this->document_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('document_model', $id);
        $this->template->write_view('content_block', 'training/edit', $this->data);
        $this->template->render();
    }

    public function lesson_del() {
        $id = intval(@$_POST['id']);
        $cond = array('cat_id' => $id);
        $this->training_chapter_model->delete_where($cond);
        $this->training_cat_model->delete($id);
    }

    function load() {
        $id = intval(@$_REQUEST['id']);
        $cond = null;
        if ($id) {
            $cond = 'parent = ' . $id;
        } else {
            $cond = 'parent > 0';
        }

        $order = 'id desc';
        $data['id'] = $id;
        $data['results'] = $this->training_cat_model->get_by($cond, $order);

        $this->load->view('training/list', $data);
        return false;
    }

    function test() {
        $this->load->library('pdf');

        if ($_POST) {
            $params = array();
            foreach ($_POST as $k => $v) {
                $k = str_replace(array('__', '_'), array('.', ' '), $k);
                $params[$k] = $v;
            }
            //echo '<pre>';
            //print_r($params);exit;
            if (fillFormPdf(APPPATH . '../assets/pdf/956a.pdf', $params, APPPATH . '../assets/pdf/deploys/956aoutput.pdf') == 1) {
                echo '<a href="./deploys/956aoutput.pdf?t=' . time() . '" target="_blank">Check</a>';
            } else {
                echo 'Fail';
            }
        }

        $this->load->view('training/test');
    }

}
