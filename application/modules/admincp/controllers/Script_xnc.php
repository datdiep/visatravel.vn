<?php

class Script_xnc extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
 
        $this->load->model(array('xnc_model'));
        include APPPATH . 'libraries/simple_html_dom.php';
    }
    function del(){
        $arr_profile = $this->xnc_model->get_by('ho_ten = " "');
        foreach ($arr_profile as $v) {
            $this->xnc_model->delete($v['id']);
        }
    }
    function index(){
        $url = 'http://kbtt.catphcm.bocongan.gov.vn/hochieu/Print.aspx?Id='; 
        $ele = '.BoxField';             
        $cookie="cookie.txt";
        $agent = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';               
        $profile = $this->xnc_model->get_row_by(null,'id DESC');
        if(!empty($profile))
            $url_id = $profile['url_id'];
        else
            $url_id = 140;
        while ($url_id < 2000000){
            $url_id++;
            $link = $url.$url_id;
            echo '-------------> Script <---------<br>';
            echo $link.'<br>';
            $html_arr = $this->curl($link,false,false,$cookie);
            $e = str_get_html($html_arr);
            $ho_ten = @$e->find('#Print1_txtHoten', 0)->plaintext;
            if($e->find('.BoxField', 0) && empty($ho_ten))
                exit;
            if(!empty($ho_ten) && $ho_ten != " "){
                $nam = $e->find('#Print1_chkNam', 0)->checked;
                $gioi_tinh = !empty($nam) ? 'Nam':'Nữ';
                $ngay_sinh = $e->find('#Print1_txtNgaysinh', 0)->plaintext;
                $noi_sinh = $e->find('#Print1_ddlNoiSinhId', 0)->plaintext;
                $cmnd = $e->find('#Print1_txtCmnd', 0)->plaintext;
                $ngay_cap = $e->find('#Print1_txtNgaycap', 0)->plaintext;
                $noi_cap = $e->find('#Print1_ddlNoiCap', 0)->plaintext;
                $dan_toc = $e->find('#Print1_ddlDantocid', 0)->plaintext;
                $ton_giao = $e->find('#Print1_ddlTongiaoid', 0)->plaintext;
                $so_dien_thoai = $e->find('#Print1_txtSodienthoai', 0)->plaintext;
                $dia_chi_thuong_tru = $e->find('#Print1_txtDiachithuongtru', 0)->plaintext;
                $dia_chi_tam_tru = $e->find('#Print1_txtDiachitamtru', 0)->plaintext;
                $nghe_nghiep = $e->find('#Print1_ddlNghenghiepid', 0)->plaintext;
                $ten_va_diachi_co_quan = $e->find('#Print1_txtTenvadiachicoquan', 0)->plaintext;
                $ho_ten_cha = $e->find('#Print1_txtHotencha', 0)->plaintext;
                $ngay_sinh_cha = $e->find('#Print1_txtNgaysinhcha', 0)->plaintext;
                $ho_ten_me = $e->find('#Print1_txtHotenme', 0)->plaintext;
                $ngay_sinh_me = $e->find('#Print1_txtNgaysinhme', 0)->plaintext;
                $ho_ten_vochong = $e->find('#Print1_txtHotenvohoacchong', 0)->plaintext;
                $ngay_sinh_vochong = $e->find('#Print1_txtNgaysinhvohoacchong', 0)->plaintext;
                $so_hieu_chieu = $e->find('#Print1_txtSohcgannhat', 0)->plaintext;
                $ngay_cap_ho_chieu = $e->find('#Print1_txtNgaycaphcgannhat', 0)->plaintext;
                $noi_dung_de_nghi = $e->find('#Print1_txtNoidungdenghi', 0)->plaintext;
                $result = array(
                    'ho_ten' => @$ho_ten,
                    'gioi_tinh' => @$gioi_tinh,
                    'ngay_sinh' => trim(preg_replace('/\s\s+|3. Sinh ngày/', ' ', trim($ngay_sinh))),
                    'noi_sinh' => trim(preg_replace('/\s\s+/', ' ', trim($noi_sinh))),
                    'cmnd' => trim(preg_replace('/\s\s+|4. Giấy CMND\/thẻ CCCD số \(2\)/', ' ', trim($cmnd))),
                    'ngay_cap' => trim(preg_replace('/\s\s+|Ngày cấp/', '', trim($ngay_cap))),
                    'noi_cap' => trim(preg_replace('/\s\s+|\s\s+|Nơi cấp \(tỉnh, TP\)/', ' ', trim($noi_cap))),
                    'dan_toc' => trim(preg_replace('/\s\s+|5. Dân tộc/', ' ', trim($dan_toc))),
                    'ton_giao' => trim(preg_replace('/\s\s+|6. Tôn giáo/', ' ', trim($ton_giao))),
                    'so_dien_thoai' => trim(preg_replace('/\s\s+|7. Số điện thoại/', ' ', trim($so_dien_thoai))),
                    'dia_chi_thuong_tru' => @$dia_chi_thuong_tru,
                    'dia_chi_tam_tru' => @$dia_chi_tam_tru,
                    'nghe_nghiep' => trim(preg_replace('/\s\s+|10. Nghề nghiệp/', ' ', trim($nghe_nghiep))),
                    'ten_va_diachi_co_quan' => @$ten_va_diachi_co_quan,
                    'ho_ten_cha' => trim(preg_replace('/\s\s+|12. Cha: họ và tên/', ' ', trim($ho_ten_cha))),
                    'ngay_sinh_cha' => trim(preg_replace('/\s\s+|sinh ngày/', ' ', trim($ngay_sinh_cha))),
                    'ho_ten_me' => trim(preg_replace('/\s\s+|&nbsp;&nbsp;&nbsp; Mẹ: họ và tên/', ' ', trim($ho_ten_me))),
                    'ngay_sinh_me' => trim(preg_replace('/\s\s+|sinh ngày/', ' ', trim($ngay_sinh_me))),
                    'ho_ten_vochong' => trim(preg_replace('/\s\s+|&nbsp;&nbsp;&nbsp; Vợ \/chồng\: họ và tên/', ' ', trim($ho_ten_vochong))),
                    'ngay_sinh_vochong' => trim(preg_replace('/\s\s+|sinh ngày/', ' ', trim($ngay_sinh_vochong))),
                    'so_hieu_chieu' => trim(preg_replace('/\s\s+|13. Hộ chiếu PT được cấp lần gần nhất \(nếu có\) số/', ' ', trim($so_hieu_chieu))),
                    'ngay_cap_ho_chieu' => trim(preg_replace('/\s\s+|cấp ngày/', ' ', trim($ngay_cap_ho_chieu))),
                    'noi_dung_de_nghi' => trim(preg_replace('/\s\s+|14. Nội dung đề nghị\(3\)/', ' ', trim($noi_dung_de_nghi))),
                    'url_id' => $url_id
                );
                //pre($result,false);
                $this->xnc_model->insert($result);
            }
            
        }
    }
    function curl($url ,$binary=false,$post=false,$cookie =false ){
    if(!file_exists($cookie)){
        $fp = fopen($cookie, "w");
        fclose($fp);
    }    
    $ch = curl_init();

            curl_setopt ($ch, CURLOPT_URL, $url );
            curl_setopt ($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_REFERER, $url);
            curl_setopt($ch, CURLOPT_ENCODING, 'gzip,deflate');
            curl_setopt($ch, CURLOPT_AUTOREFERER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);



            if($cookie){

                $agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)";
                curl_setopt($ch, CURLOPT_USERAGENT, $agent);
                curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
                curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);

            }


            if($binary)
                curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

            if($post){
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            }

         return  curl_exec ($ch);
    }
}
