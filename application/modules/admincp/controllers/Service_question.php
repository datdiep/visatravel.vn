<?php

/**
 * @property Service_question $Service_question
 */
class Service_question extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('category_service_model', 'service_detail_model', 'service_question_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'service_question';
        $this->data['service_detail'] = $this->service_detail_model->get_all();
        $this->data['category_service'] = $this->category_service_model->get_by('id_parent <> 0', "", "id");
        foreach ($this->data['service_detail'] as $k => $item) {
            $this->data['category_service'][$item['service_id']]['detail'][] = $item;
        }
    }

    public function add() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            if ($this->form_validation->run() != FALSE) {
                $service_ids = $this->input->post('checkbox');
                $ids = ',';
                if (!empty($service_ids)) {
                    foreach ($service_ids as $value) {
                        $ids .= $value . ',';
                    }
                } else
                    $ids .= "0,";
                $alias = $this->alias->create_alias($this->input->post('title'));
                //   setcookie('news_cat_id', $this->data['cat_id'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                // $tags_id = ',' . implode(',', $this->input->post('tags')) . ',';
                $data = array(
                    'title' => $this->input->post('title'),
                    'alias' => $alias,
                    'content' => $this->input->post('content'),
                    'date_create' => date('Y-m-d H:i:s'),
                    'cat_ids' => $ids,
                    'status' => 'active',
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags')
                );
                $id = $this->service_question_model->insert($data);
                $this->data['check_error'] = 0;
            } else {
                $this->data['check_error'] = 1;
            }
        }
        $this->template->write_view('content_block', 'service_question/add', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['questions'] = $this->service_question_model->get_by_key($id);
        $this->data['list_service_checked'] = $this->get_list_service($this->data['questions']['cat_ids'], 2);
        $this->data['show_button'] = $this->getdays($this->data['questions']['date_create'], date("Y-m-d")) < 3 || $this->data['user']['root'] ? 1 : 0;
        if (isset($_POST['submit']) && ($this->getdays($this->data['questions']['date_create'], date("Y-m-d")) < 3 || $this->data['user']['root'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $service_ids = $this->input->post('checkbox');
                $ids = ',';
                if (!empty($service_ids)) {
                    foreach ($service_ids as $value) {
                        $ids .= $value . ',';
                    }
                } else
                    $ids .= "0,";
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->input->post('alias'),
                    'content' => $this->input->post('content'),
                    'date_create' => date('Y-m-d H:i:s'),
                    'cat_ids' => $ids,
                    'status' => 'active',
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags')
                );
                $this->data['check_error'] = 0;
                $this->service_question_model->update($update, $id);
            }
            $this->data['questions'] = $this->service_question_model->get_by_key($id);
            $this->data['list_service_checked'] = $this->get_list_service($this->data['questions']['cat_ids'], 2);
        }

        $this->template->write_view('content_block', 'service_question/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->data['question'] = $this->service_question_model->get_by_key($id);
        if ($this->data['user']['root']) {
            $this->service_question_model->delete($id);
            return "";
        }
        if ($this->getdays($this->data['question']['date_create'], date("Y-m-d")) > 3)
            echo "Liên Hệ Admin Để xóa";
        else {
            $this->service_question_model->delete($id);
        }
    }

    public function index() {
        $this->data['pos'] = 0;
        $this->template->write_view('content_block', 'service_question/index', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $cond = 'id > 0';
        $cat_id = @intval($_POST['cat_id']);
        if ($cat_id) {
            $cond .= " and cat_ids like '%" . $cat_id . "%'";
        }
        $keyword = @$_POST['keyword'];
        if ($keyword) {
            $cond .= " and ( id = '$keyword' or title like '%$keyword%')";
        }
        $data['service_detail'] = $this->service_detail_model->get_all();
        $data_country = $this->category_service_model->get_by('id_parent != 0');

        $limit = 15;
        $order = 'id DESC';
        $data['results'] = $this->service_question_model->get_for_page($limit, $pos, $cond, $order);
        //get title in list ids
        foreach ($data['results'] as $key => $value) {
            $data['results'][$key]['title_service'] = $this->get_list_service($value['cat_ids'], 1);
            $data['results'][$key]['delete'] = $this->getdays($data['results'][$key]['date_create'], date("Y-m-d")) < 3 ? 1 : 0;
            $data['results'][$key]['edit'] = $this->getdays($data['results'][$key]['date_create'], date("Y-m-d")) < 3 ? 1 : 0;
        }
        if ($this->data['user']['root'])
            $data['results'][$key]['view'] = 0;
        $total = $this->service_question_model->get_total_rows($cond);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('service_question/list', $data);
        return false;
    }

    private function get_list_service($array_id, $flag) {
        $this->load->model(array("category_service_model"));
        /// $flag = 1 for page , 2 for get id in 
        if ($flag == 1) {
            $ids = '(' . substr($array_id, 1, -1) . ')';
            $title = "";
            $list_service = $this->service_detail_model->get_by("id in $ids");
            foreach ($list_service as $value) {
                $title .= $value['title'] . ",";
            }
            return $title;
        } else if ($flag == 2) {
            $ids = '(' . substr($array_id, 1, -1) . ')';
            $list_service = $this->service_detail_model->get_by("id in $ids", null, 'id');
            return $list_service;
        }
        // id not in
//        else {
//            $ids = '(' . substr($array_id, 1, -1) . ')';
//            $list_service = $this->service_detail_model->get_by("id not in $ids");
//            return $list_service;
//        }
    }

//    function search() {
//        if (isset($_POST['id'])) {
//            $id = ($this->security->xss_clean($_POST['id']));
//            $this->load->model(array('category_service_model'));
//            $data = $this->category_service_model->get_by("id_parent = $id");
//            foreach ($data as $value) {
//                print_r("<input type='checkbox' name='checkbox[]' value='" . $value['id'] . "'>" . $value['title'] . "</input>");
//            }
//        }
//    }

    function update_hot() {
        if (isset($_POST['data'])) {
            $this->service_detail_model->update_batch($_POST['data']);
        }
    }

}
