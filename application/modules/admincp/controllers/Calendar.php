<?php

class Calendar extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('category_news_model');
        $this->load->library(array('form_validation', 'my_pagination', 'upload', 'alias'));

        $this->data['pre'] = 'calendar';
    }

    public function index() {

        $this->template->write_view('content_block', 'block/calendar', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->category_news_model->delete($id);
    }

}
