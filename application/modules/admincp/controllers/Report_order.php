<?php

class Report_order extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation'));
        $this->data['pre'] = 'report';
    }

    function index() {
        $this->data['date'] = '01/' . date('m/Y') . ' - ' . date('d/m/Y');
        $this->data['pre2'] = 'report_order';
        $this->template->write_view('content_block', 'report/order/index', $this->data);
        $this->template->render();
    }

    function get_list_traffic() {

        if (isset($_POST['date_install'])) {
            $date_install = (@$_POST['date_install']);
            $date_install = explode(' - ', $date_install);
            $from = $this->revert_time($date_install[0]);
            $to = $this->revert_time($date_install[1]) . ' 23:59:59';
            $cond = 'date >= "' . $from . '" and date <= "' . $to . '"';
            echo $cond;
            $this->load->model('report_product_model');
            include APPPATH . 'config/maps_order.php';
            $data['status_order'] = $this->data['status_order'];
            $data['orders'] = $this->report_product_model->get_order_report_temp($cond);
            $this->load->view('report/order/list', $data);
        }
    }

    function get_list_product() {

        if (isset($_POST['date_install']) && isset($_POST['keyword'])) {
            $keyword = $_POST['keyword'];
            $this->load->model('product_model');
            $product = $this->product_model->get_row_by('product_code = "' . $keyword . '"');            
            if (!empty($product)) {
                $date_install = (@$_POST['date_install']);
                $date_install = explode(' - ', $date_install);
                $from = $this->revert_time($date_install[0]);
                $to = $this->revert_time($date_install[1]) . ' 23:59:59';
                $cond = 'date >= "' . $from . '" and date <= "' . $to . '" and product_id = ' . $product['id'];
                $this->load->model('report_product_model');
                $data['orders'] = $this->report_product_model->get_report_product($cond);              
                $this->load->view('report/product/list', $data);
            } else {
                echo 0;
            }
        }
    }

    function product() {
        $this->data['date'] = '01/' . date('m/Y') . ' - ' . date('d/m/Y');
        $this->data['pre2'] = 'report_order_product';
        $this->template->write_view('content_block', 'report/product/index', $this->data);
        $this->template->render();
    }

}
