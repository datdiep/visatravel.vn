<?php
class Notify extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
    }
    function index($id){
        $id = intval($id);
        $notify = $this->updatenotify($this->data['username'], $id);
        if(!empty($notify))
            redirect(base_url($notify['url']));
    }    
}