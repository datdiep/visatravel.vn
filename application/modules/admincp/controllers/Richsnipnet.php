<?php

class Richsnipnet extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('richsnipnet_model');
        $this->load->library(array('form_validation', 'my_pagination', 'upload', 'alias'));

        $this->data['pre'] = 'richsnipnet';
    }

    public function add() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('url', 'Url', 'required');
            $this->form_validation->set_rules('date', 'Date', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('url_request', 'Url Request', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $add = array(
                    'name' => $this->input->post('name'),
                    'url' => $this->input->post('url'),
                    'date' => $this->input->post('date'),
                    'location' => $this->input->post('location'),
                    'url_request' => $this->input->post('url_request')
                );

                $this->data['check_error'] = 0;
                $this->richsnipnet_model->insert($add);
            }
        }
        $this->template->write_view('content_block', 'richsnipnet/add', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['list_richsnipnet'] = $this->richsnipnet_model->get_all();
        $this->template->write_view('content_block', 'richsnipnet/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['list_richsnipnet'] = $this->richsnipnet_model->get_by_key($id);
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('url', 'Url', 'required');
            $this->form_validation->set_rules('date', 'Date', 'required');
            $this->form_validation->set_rules('location', 'Location', 'required');
            $this->form_validation->set_rules('url_request', 'Url Request', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $update = array(
                    'name' => $this->input->post('name'),
                    'url' => $this->input->post('url'),
                    'date' => $this->input->post('date'),
                    'location' => $this->input->post('location'),
                    'url_request' => $this->input->post('url_request')
                );

                $this->data['check_error'] = 0;
                $this->richsnipnet_model->update($update, $id);
            }
        }
        $this->data['list_richsnipnet'] = $this->richsnipnet_model->get_by_key($id);
        $this->template->write_view('content_block', 'richsnipnet/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->richsnipnet_model->delete($id);
    }

}
