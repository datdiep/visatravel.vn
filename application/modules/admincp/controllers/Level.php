<?php

/**
 * @property Page_Model $task_model
 */
class Task extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'resize_image', 'alias'));
        $this->load->model(array('task_model','admin_model'));
        $this->data['type'] = $this->session->userdata('type_page');
        $this->data['pre'] = 'task';
    }

    function index() {
        $this->data['pos_page'] = intval(@$_COOKIE['pos_page']);
        $this->data['type_page'] = @$_COOKIE['type_page'];
        $this->data['users'] = $this->admin_model->get_by('level >='.$this->data['user']['level'],'level ASC');
        $this->template->write_view('content_block', 'task/index', $this->data);
        $this->template->render();
    }

    function add() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('propose', 'Đề xuất công việc', 'required');

            if ($this->form_validation->run() == true) {
                $insert = array(
                    'timeadd' => date('Y-m-d H:i:s'),
                    'type' => $_POST['type'],
                    'username' => $this->data['user']['username'],
                    'userid' => $this->data['user']['id'],
                    'propose' => $this->input->post('propose'),
                    'lv' => $this->data['user']['lv'],
                );
                $id = $this->task_model->insert($insert);
                $this->data['check_error'] = 0;
            } else {
                $this->data['check_error'] = 1;
            }
        }
        $this->template->write_view('content_block', 'task/add', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $parrams = $this->security->xss_clean($_REQUEST);
        $data['user'] = $this->data['user']['username'];
        $lv = $this->data['user']['level'];
        $cond = '';
        if ($parrams['type']) {
            $cond = 'propose != "" ';
            if ($parrams['type'] != 'All') {
                $cond .= 'and ';
                $cond .= 'type = "' . $parrams['type'] . '"';
            }
            if (@$parrams['member']) {
                if (@$parrams['member'] != 'all') {
                    $cond.=' and username ="' . $parrams['member'] . '"';
                }
            }
        } else {
            $cond = 'type = "Open"';
        }
        $cond .= ' and level >= ' . $lv;
        $pos = intval($parrams['pos']);
        if ($pos < 0) {
            $pos = 0;
        }
        echo $cond; exit;
        $data['results'] = $this->task_model->get_for_page($this->limit, $pos, $cond, 'id DESC');
        $total = $this->task_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("type_page", $parrams['type'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $this->limit);
        $this->load->view('task/list', $data);
        return false;
    }

    function del() {
        $id = intval($_POST['id']);
        $task = $this->task_model->get_by_key($id);
        if (!empty($task) && $this->data['user']['role'] == 'admin') {
            $this->task_model->delete($id);
        }
    }

    function edit($id = 0) {
        $this->data['check_error'] = -1;
        $this->data['task'] = $this->task_model->get_by_key($id);

        if ($this->data['user']['lv'] > $this->data['task']['lv']) {
            redirect(base_url('admincp/task'));
        }
        if (isset($_POST['submit'])) {

            $insert = array(
                'type' => $_POST['type'],
                'appraise' => $this->input->post('appraise'),
                'action' => $this->input->post('action')
            );
            //pre($insert);
            $this->task_model->update($insert, $id);
            $this->data['check_error'] = 0;
            $this->data['task'] = $this->task_model->get_by_key($id);
        }

        if ($this->data['task']) {
            $workload_links = $this->workload_link_model->get_by(array('admin' => $this->data['task']['userid']));
            if (!empty($workload_links)) {
                $this->data['workload_link'] = $workload_links[0]['link'];
            } else {
                $this->data['workload_link'] = '';
            }

            $this->template->write_view('content_block', 'task/edit', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

}
