<?php

class Agency extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('document_model', 'network_model', 'folder_model', 'ticket_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload', 'my_category'));
        $this->data['type'] = $this->folder_model->get_by(array('type' => 'document', 'network_id' => $this->data['my_network']['id']), 'priority DESC');

        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['staff'] = $this->admin_model->get_by($cond);

        $this->data['pre'] = 'document';
    }

    public function index() {
        $this->data['pos'] = intval(@$_COOKIE['pos_page']);

        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];

        $this->data['results'] = $this->document_model->get_all();
        $this->template->write_view('content_block', 'document/index', $this->data);
        $this->template->render();
    }

    public function ticket_del() {
        $id = intval(@$_POST['id']);
        $this->ticket_model->delete($id);
    }

    function ticket_list() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';

        $cond .= ' and network_id = ' . $this->data['user']['network_id'] . ' ';
        if (!empty($this->data['user']['role_network'])) {
            $cond = 'id > 0';
        }

        $limit = 10;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $data['results'] = $this->ticket_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->ticket_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);

        $data['links'] = $this->get_page($pos, $total, $limit);


        $this->load->view('agency/ticket_list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond .= "title like '%$keyword%' or id = " . intval($keyword);
            $cond .= " and network_id = " . $this->data['user']['network_id'];
            $news = $this->ticket_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function ticket() {
        $this->data['pre'] = 'ticket';
        $this->data['check_error'] = -1;
        $this->form_validation->set_rules('room', 'Phòng ban cần liên hệ', 'required');
        $this->form_validation->set_rules('subject', 'Tiêu đề', 'trim|required');
        $this->form_validation->set_rules('content', 'Nội dung', 'trim|required');
        $this->load->library('form_validation');

        if (isset($_POST['submit'])) {
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                unset($_POST['submit']);
                $this->data['datapost'] = $this->input->post();
                $this->data['datapost']['user_create'] = $this->data['user']['username'];
                $this->data['datapost']['date_create'] = date('Y-m-d H:i:s');
                $this->data['datapost']['network_id'] = $this->data['user']['network_id'];
                $last_id = $this->ticket_model->insert($this->data['datapost']);
                $path = PATH_UPLOAD . 'ticket/' . $last_id;
                @mkdir($path, 0777);
                if ($_FILES['images']['name'][0] != '') {
                    $this->upload_mutiple($_FILES, $path);
                }
                $this->data['check_error'] = 0;
            }
        }

        $this->data['pos'] = intval(@$_COOKIE['pos_page']);

        $this->template->write_view('content_block', 'agency/ticket', $this->data);
        $this->template->render();
    }

}
