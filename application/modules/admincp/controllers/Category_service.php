<?php

class Category_service extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('category_service_model'));
        $this->load->library(array('form_validation', 'my_pagination', 'upload', 'alias', 'resize_image'));
        $this->load->helper('database');
        //$this->data['status'] = field_enums('category_service_tips', 'tips');
        $this->data['pre'] = 'category_service';
    }

    public function add() {
        $this->data['category_service'] = $this->category_service_model->get_by('id_parent = 0');
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $image = '';
                @mkdir(PATH_UPLOAD . '/category_service', 0777);
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'category_service/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->input->post('alias') . '-' . $_FILES['image']['name']
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        // $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'zithumb_' . $img['file_name'], 36, 36);
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $add = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->input->post('alias'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'id_parent' => $this->input->post('id_parent'),
                    'description' => $this->input->post('description'),
                    'icon' => $image
                );

                $this->data['check_error'] = 0;
                $this->category_service_model->insert($add);
            }
        }
        $this->template->write_view('content_block', 'category_service/add', $this->data);
        $this->template->render();
    }

    public function index() {
        $cate_child = $this->category_service_model->get_by('id_parent != 0');
        $this->data['category_service'] = $this->category_service_model->get_by('id_parent = 0', null, 'id');
        foreach ($cate_child as $child) {
            $this->data['category_service'][$child["id_parent"]]['childs'][] = $child;
        }
        // pre($this->data['category_service']);
        $this->template->write_view('content_block', 'category_service/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['category_detail'] = $this->category_service_model->get_by_key($id);
        $this->data['category_service'] = $this->category_service_model->get_by('id_parent = 0');
        //pre($this->data['category_service_tips']);
        if (isset($_POST['submit']) && !empty($this->data['category_service'])) {
            $this->form_validation->set_rules('title', 'title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->input->post('alias'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'id_parent' => $this->input->post('id_parent'),
                    'description' => $this->input->post('description'),
                );

                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'category_service/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => true,
                        'file_name' => $this->input->post('alias') . '-' . $_FILES['image']['name']
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $update['icon'] = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $this->data['check_error'] = 0;
                $this->category_service_model->update($update, $id);
                $this->data['category_detail'] = $this->category_service_model->get_by_key($id);
            }
        }

        $this->template->write_view('content_block', 'category_service/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->category_service_model->delete($id);
    }

}
