<?php

class Category_News extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('category_news_model');
        $this->load->library(array('form_validation', 'my_pagination', 'upload', 'alias'));
        $this->load->helper('database');
        $this->data['list_access'] = field_enums('category_news', 'access');
        $this->data['list_type'] = field_enums('category_news', 'type');
        $this->data['pre'] = 'category_news';
        $this->data['pre1'] = 'news';
    }

    public function add_edit($id = '') {

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $data = @$_POST;
                // ad cate new cho moi tin tuc
                $data['type'] = 'news';
                unset($data['submit']);
                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'category_news/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->alias->create_alias($_FILES['image']['name'])
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $data['image'] = $image;
                $data['network_id'] = $this->data['user']['network_id'];

                $this->data['check_error'] = 0;
                if ($id) {
                    $this->category_news_model->update($data, $id);
                } else {
                    $this->category_news_model->insert($data);
                }
            }
        }

        if ($id) {
            $this->data['category_news'] = $this->category_news_model->get_by_key($id);
        }
        $this->template->write_view('content_block', 'category_news/add_edit', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['category_news'] = $this->category_news_model->get_all('type');
        $this->template->write_view('content_block', 'category_news/index', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->category_news_model->delete($id);
    }

}
