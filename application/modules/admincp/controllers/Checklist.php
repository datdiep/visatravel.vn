<?php

class Checklist extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('checklist_model', 'network_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload'));
        $this->data['type'] = field_enums('checklist', 'type');
        $this->data['pre'] = 'checklist';
    }

    public function add() {

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('group_service[]', 'group_service', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $group_service = @implode(',', $_POST['group_service']);
                if(!empty($group_service))
                    $group_service = ','.$group_service.',';
                $add = array(
                    'title' => $this->input->post('title'),
                    'type' => $this->input->post('type'),
                    'sub_checklist' => $this->input->post('sub_checklist'),
                    'note' => $this->input->post('note'),
                    'publish_date' => date('Y-m-d H:i:s'),
                    'author' => $this->data['user']['username'],
                    'group_service' => $group_service
                );
                $this->data['check_error'] = 0;
                $id = $this->checklist_model->insert($add);
                if($_POST['submit'] == 2)
                    redirect (base_url('admincp/checklist/edit/'.$id));
            }
        }
        $this->template->write_view('content_block', 'checklist/add', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['pos'] = intval(@$_COOKIE['pos_page']);

        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];

        $this->data['results'] = $this->checklist_model->get_all();
        $this->template->write_view('content_block', 'checklist/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;

        if (isset($_POST['submit']) && $_POST['submit'] != '-1') {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('group_service[]', 'group_service', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $group_service = @implode(',', $_POST['group_service']);
                if(!empty($group_service))
                    $group_service = ','.$group_service.',';
                $update = array(
                    'title' => $this->input->post('title'),
                    'type' => $this->input->post('type'),
                    'sub_checklist' => $this->input->post('sub_checklist'),
                    'note' => $this->input->post('note'),
                    'author_update' => $this->data['user']['username'],
                    'group_service' => $group_service,
                    'last_update' => date('Y-m-d H:i:s'),
                    
                );

                //$this->data['check_error'] = 0;
                $this->checklist_model->update($update, $id);
                $this->data['check_error'] = 0;
                if($_POST['submit'] == 3)
                    redirect (base_url('admincp/checklist/add'));
                if($_POST['submit'] == 2)
                    redirect (base_url('admincp/checklist'));
            }
        }

        $this->data['checklist'] = $this->checklist_model->get_by_key($id);
        $this->data['load_file'] = $this->load_block_file('checklist_model', $id);
        $this->template->write_view('content_block', 'checklist/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->checklist_model->delete($id);
    }

    function load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];

        $cond .= ' and network_id = ' . $this->data['user']['network_id'] . ' ';
        if (!empty($this->data['user']['role_network'])) {
            $cond = 'id > 0';
        }

        if ($type) {
            $cond .= ' and type = "' . $type . '"';
        }

        if ($keyword) {
            $cond.=" and ( id = '$keyword' or title like '%$keyword%')";
            //$cond .= " and title like '%$keyword%'";
        }


        $limit = 20;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $data['results'] = $this->checklist_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->checklist_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("type_cookies", $type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('checklist/list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond = "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function document_print($id) {

        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);

        $this->data['page'] = $this->checklist_model->get_by_key($id);
        $this->load->view('checklist/print', $this->data);
    }

    function sendmail($id) {
        $this->data['check_error'] = -1;
        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);
        $this->data['page'] = $this->checklist_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('checklist_model', $id);
        $this->template->write_view('content_block', 'checklist/sendmail', $this->data);
        $this->template->render();
    }

    function test() {
        $this->load->library('pdf');

        if ($_POST) {
            $params = array();
            foreach ($_POST as $k => $v) {
                $k = str_replace(array('__', '_'), array('.', ' '), $k);
                $params[$k] = $v;
            }
            //echo '<pre>';
            //print_r($params);exit;
            if (fillFormPdf(APPPATH . '../assets/pdf/956a.pdf', $params, APPPATH . '../assets/pdf/deploys/956aoutput.pdf') == 1) {
                echo '<a href="./deploys/956aoutput.pdf?t=' . time() . '" target="_blank">Check</a>';
            } else {
                echo 'Fail';
            }
        }

        $this->load->view('checklist/test');
    }

}
