<?php

class Hotline extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('curl');
    }

    function index() {
        $this->data['pre'] = 'hotline';
        $this->data['check_error'] = -1;
        if (isset($_POST['phone_click']) && ($this->data['user']['manager_editor'] || $this->data['user']['root'] )) {
            $phone = array();
            foreach ($_POST['phone_click'] as $k => $value) {
                // Key is Click , Value is show_phone
                $phone[$value] = $_POST['phone_show'][$k];
            }
            $parram = array('phone' => json_encode($phone), 'website' => substr(base_url(), 0, -1));
            //  pre(array('phone' => json_encode($phone), 'website' => base_url()));
            $this->curl->get_url(SITE_ADMIN . 'api/update_hotline', $parram);
            $this->data['check_error'] = 0;
            file_put_contents(APPPATH . 'cache/phone', serialize($phone));
        }
        $this->data['result'] = @file_get_contents(APPPATH . 'cache/phone');
        if ($this->data['result']) {
            $this->data['result'] = unserialize($this->data['result']);
            //pre($this->data['result']);
        }
        $this->template->write_view('content_block', 'hot_line', $this->data);
        $this->template->render();
    }

}
