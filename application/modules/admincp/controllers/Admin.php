<?php

/**
 * @property Admin_Model $admin_model
 */
Class Admin extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model(array('admin_model', 'role_controller_model', 'level_department_model', 'department_model'));
        $this->load->library(array('form_validation', 'resize_image', 'upload'));
        $this->load->helper('database');
        $this->data['pre'] = 'admin';
        $this->data['pre1'] = 'dashboard';
        $this->data['level_department'] = $this->level_department_model->get_all(null, 'id');
        $cond = null;
        if (empty($this->data['user']['role_network']))
            $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['department'] = $this->department_model->get_by($cond, null, 'id');
        $this->data['status'] = field_enums('admin', 'status');
        //pre($this->data['level_department']);
    }

    public function index($id = 0) {
	$id = intval($id);
        $this->data['results'] = $this->admin_model->get_all();
        $this->template->write_view('content_block', 'admin/index', $this->data);
        $this->template->render();
    }

    public function add() {
        $this->data['pre'] = 'admin_add';
        $this->data['check_error'] = -1;
        // pre($this->data['network']);

        if (isset($_POST['submit'])) {

            $this->form_validation->set_rules('username', 'Username', 'required|min_length[3]|max_length[18]|alpha_dash');
            $this->form_validation->set_rules('password', 'Password', 'required|matches[repassword]');
            $this->form_validation->set_rules('repassword', 'Password Confirmation', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $username = $this->input->post('username');
                $user = $this->admin_model->get_row_by(array('username' => $username));
                if (empty($user)) {
                    $leader = $this->input->post('leader');
                    if (empty($leader))
                        $leader = $this->data['user']['id'];
                    $role_controller = @implode(',', $this->input->post('role_controller'));
                    $role_order_progress = @implode(',', $this->input->post('role_order_progress'));
                    $insert = array(
                        'username' => $this->input->post('username'),
                        'phone' => $this->input->post('phone'),
                        'password' => md5($this->input->post('password')),
                        'fullname' => $this->input->post('fullname'),
                        'email' => $this->input->post('email'),
                        'role_controller' => $role_controller,
                        'role_order_progress' => $role_order_progress,
                        'level_department' => $this->input->post('level_department'), // Quyền của phòng ban
                        'department_id' => $this->input->post('department_id'),
                        'date_create' => date('Y-m-d'),
                        'work_phone' => $this->input->post('work_phone'),
                        'job' => $this->input->post('job'),
                        'dob' => $this->input->post('dob'),
                        'date_joincompany' => $this->input->post('date_joincompany'),
                        'tax_code' => $this->input->post('tax_code'),
                        'bank_account' => $this->input->post('bank_account'),
                        'home' => $this->input->post('home'),
                        'leader' => $leader
                    );


                    $image = '';
                    $path = PATH_UPLOAD . 'avatar/';
                    $img_hide = $this->input->post('photo');
                    if ($img_hide != '') {
                        $img_name = $insert['username'] . '.jpg';
                        $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                        $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                        $insert['avatar'] = $img_name;
                    }

                    $this->admin_model->insert($insert);
                    $this->data['check_error'] = 0;
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = 'Tên đăng nhập đã tồn tại';
                }
            }
        }
        $this->template->write_view('content_block', 'admin/add', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['admin'] = $this->admin_model->get_by_key($id);
        if (empty($this->data['admin'])) {
            redirect(base_url() . ADMIN_URL . 'admin');
        }
        if (isset($_POST['submit'])) {

            $this->form_validation->set_rules('password', 'Password', 'matches[repassword]');
            $this->form_validation->set_rules('repassword', 'Password Confirmation');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $leader = $this->input->post('leader');
                if (empty($leader))
                    $leader = $this->data['user']['id'];
                $role_controller = @implode(',', $this->input->post('role_controller'));
                $role_order_progress = @implode(',', $this->input->post('role_order_progress'));
                $update = array(
                    'status' => $this->input->post('status'),
                    'phone' => $this->input->post('phone'),
                    'fullname' => $this->input->post('fullname'),
                    'email' => $this->input->post('email'),
                    'role_controller' => $role_controller,
                    'role_order_progress' => $role_order_progress,
                    'level' => $this->input->post('level'), // Quyền của network
                    'level_department' => $this->input->post('level_department'), // Quyền của phòng ban
                    'department_id' => $this->input->post('department_id'),
                    'work_phone' => $this->input->post('work_phone'),
                    'job' => $this->input->post('job'),
                    'dob' => $this->input->post('dob'),
                    'date_joincompany' => $this->input->post('date_joincompany'),
                    'tax_code' => $this->input->post('tax_code'),
                    'bank_account' => $this->input->post('bank_account'),
                    'home' => $this->input->post('home'),
                    'leader' => $leader
                );
                if (!empty($this->data['user']['role_network'])) {
                    $update['network_id'] = $this->input->post('network_id');
                    $update['network_type'] = $this->input->post('network_type');
                } else {
                    $update['network_id'] = $this->data['user']['network_id'];
                    $update['network_type'] = $this->data['user']['network_type'];
                }
                if ($_POST['password']) {
                    $update['password'] = md5($_POST['password']);
                }

                $path = PATH_UPLOAD . 'avatar/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    if ($this->data['admin']['avatar']) {
                        $img_name = $this->data['admin']['avatar'];
                    } else {
                        $img_name = $update['username'] . '.jpg';
                    }

                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $update['avatar'] = $img_name;
                }

                //pre($update);
                $this->admin_model->update($update, $id);
                $this->data['check_error'] = 0;
                $this->data['admin'] = $this->admin_model->get_by_key($id);
            }
        }
        $this->data['leader'] = $this->admin_model->get_by_key($this->data['admin']['leader']);
        $this->data['leader']['level_department_name'] = $this->data['level_department'][$this->data['leader']['level_department']]['name'];
        $this->template->write_view('content_block', 'admin/edit', $this->data);
        $this->template->render();
    }

    function load_access() {
        $data['role_cog'] = $this->role_controller_model->get_by("type = 'cog'");
        $data['role_codepen'] = $this->role_controller_model->get_by("type = 'codepen'");
        $this->load->view('admin/load_access', $data);
        return false;
    }

    public function del() {
        $id = intval($_POST['id']);
        $this->admin_model->delete($id);
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('admin_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $network_id = $_GET['n'];
            $cond = "(network_id = '$network_id' and username like '%$keyword%')";
            if (!empty($this->data['user']['role_network']))
                $cond .= ' or username = "' . $this->data['user']['username'] . '"';
            $news = $this->admin_model->get_for_page(10, 0, $cond, 'id ASC');
            //pre($news);
            foreach ($news as $k => $v) {
                if (!empty($this->data['department'][$v['department_id']]))
                    $news[$k]['department_name'] = $this->data['department'][$v['department_id']]['name'];
                else
                    $news[$k]['department_name'] = 'Không thuộc';
                $news[$k]['level_department_name'] = $this->data['level_department'][$v['level_department']]['name'];
            }
            echo json_encode($news); //format the array into json data            
        }
    }

}
