<?php

class Homeinfo extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
	$this->load->model('controller_model');
	$this->data['map_type'] = array('check-square' => 'Quyền mặc định','codepen' => 'Quyền điều khiển','cog' => 'Quyền hệ thống');
    }

    function index() {
        $this->data['pre'] = 'seohome';
        $this->data['check_error'] = -1;
        if (isset($_POST['seo_title'])) {
            $this->data['check_error'] = 0;
            file_put_contents(APPPATH . 'cache/seo_home', serialize($_POST));
        }
        $this->data['result'] = @file_get_contents(APPPATH . 'cache/seo_home');
        if ($this->data['result']) {
            $this->data['result'] = unserialize($this->data['result']);
        }
        $this->template->write_view('content_block', 'seohome', $this->data);
        $this->template->render();
    }
    function page_role() {
        $role_type = @$_POST['role_type'];
	$cond = null;
        if ($role_type) {
            $cond = 'type = "' . $role_type.'"';
        }
	//echo $cond;
        $data['results'] = $this->controller_model->get_by($cond,"id DESC");
	$data['map_type'] = $this->data['map_type'] ;
	//pre($data['results']); 
	
        setcookie("role_type", $role_type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $this->load->view('list_role', $data);
        return false;
    }
    function role(){
	$id = intval($_POST['id']);
        $this->data['role'] = $this->controller_model->get_by_key($id);
        if (empty($this->data['role'])) {
            $this->data['role']['id'] = 0;
        }
        $this->load->view('role', $this->data);
    }
    function save_role(){
	$data = $_POST['data'];
        if (empty($data["id"])){
            $data['id'] = $this->controller_model->insert($data);
	}else{
	    $role = $this->controller_model->get_by_key($data['id']);
	    if(empty($role)){
		echo -1; exit;
	    }
	    $this->controller_model->update($data,$data['id']);
	}
	echo $data['id']; exit;
    }
    function del_role(){
	$id = intval($_POST['id']);       
        $this->controller_model->delete($id);
    }

}
