<?php

/**
 * @property News_model $news_model
 */
class News extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('news_model', 'category_news_model', 'admin_model', 'admin_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'news';
        $this->data['list_cate'] = $this->category_news_model->get_all();
        $this->data['list_editor'] = $this->admin_model->get_by("role_controller like '%news%'");
    }

    public function add() {

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            if ($this->form_validation->run() != FALSE) {
                $alias = $this->alias->create_alias($this->input->post('title'));
                $this->data['cat_id'] = $this->input->post('cat_id');
//   setcookie('news_cat_id', $this->data['cat_id'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
//                $tags_id = ','.implode(',', $this->input->post('tags_id')).',';

                $image = '';
                $data = array(
                    'title' => $this->input->post('title'),
                    'alias' => $alias,
                    'description' => $this->input->post('description'),
                    'content' => $this->input->post('content'),
                    // 'country' => $this->input->post('country'),
                    'date_create' => date('Y-m-d H:i:s'),
                    'cate_id' => $this->input->post('cate_id'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'news_ids' => $this->input->post('news_ids'),
                    'status' => !$this->data['user']['publish'] && !$this->data['user']['manager_editor'] && $_POST['submit'] != 3 ? 2 : $_POST['submit'], // -1 : tai xuat ban , 1 : xuat ban ,2 : doi duyet, 3: nhap
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'network_id' => $this->data['user']['network_id'],
                    'tags' => $this->input->post('tags')
                );
                $cat_news = $this->category_news_model->get_by_key($this->input->post('cate_id'));
                if ($cat_news['id']) {
                    $data['type'] = $cat_news['type'];
                }

                if ($this->input->post('schedule')) {
                    $data['is_hide'] = 1; // hidden
                    $data['schedule'] = date('Y-m-d', strtotime(str_replace("/", "-", $this->input->post('schedule')))); // Chuyển từ mm/dd/yyyy sang yyyy/mm/dd
                }
                @mkdir(PATH_UPLOAD . '/news', 0777);
                $path = PATH_UPLOAD . 'news/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    $img_name = $alias . '-' . rand(1, 9999) . '.jpg';
                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $data['image'] = $img_name;
                }

                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'news/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => 'fb_' . $img_name
                    ));

                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $id = $this->news_model->insert($data);
                $this->data['check_error'] = 0;
            } else {
                $this->data['check_error'] = 1;
            }
        }
        $this->template->write_view('content_block', 'news/add', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['news'] = $this->news_model->get_by_key($id);
        $user_write = $this->admin_model->get_row_by("username = '" . $this->data['news']['author_username'] . "'");
        // showbutton : -1 => hide button, 1:tai xua ban/ tai xuat ban va doi leader tham dinh/ cho duyet 
        $this->data['show_button'] = 0;
        if ($this->data['news']['status'] == -1) {
            $tmp_name = $id . '-' . $this->data['news']['alias'];
            $tmp_news = @file_get_contents(APPPATH . 'cache/news_update/' . $tmp_name);
            if ($this->data['user']['username'] == $user_write['username']) {
                $this->data['news'] = unserialize($tmp_news);
                // show text : tai xuat ban va doi leader tham dinh
                $this->data['show_button'] = 1;
            } elseif ($this->data['user']['id'] == $user_write['leader'] || $this->data['user']['manager_editor'] == 1) {
                // show text : tai xuat banbai viet
                $this->data['show_button'] = 1;
                $this->data['tmp_news'] = unserialize($tmp_news);
            }
        }
        // show button tai xuat ban khi sau 3 ngay 
        if ($this->data['user']['username'] == $user_write['username'] && $this->getdays($this->data['news']['date_create'], date("Y-m-d")) > 3)
            $this->data['show_button'] = 1;
        // show button duyet cho bai viet doi duyet
        if (($this->data['user']['manager_editor'] || $this->data['user']['root']) && $this->data['news']['status'] == 2)
            $this->data['show_button'] = 1;

        //hide button : bai viet ko phai cua leader va sau 3 ngay va khong phai bai viet tai xuat ban hide button,khong phai la doi duyet
        if ($this->data['user']['username'] != $user_write['username'] && $this->getdays($this->data['news']['date_create'], date("Y-m-d")) > 3 && $this->data['news']['status'] != -1 && $this->data['news']['status'] != 2)
            $this->data['show_button'] = -1;

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $this->data['news'] = $this->news_model->get_by_key($id);
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->input->post('alias'),
                    'description' => $this->input->post('description'),
                    'content' => $this->input->post('content'),
                    //   'country' => $this->input->post('country'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'cate_id' => $this->input->post('cate_id'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'news_ids' => $this->input->post('news_ids'),
                    'status' => $_POST['submit'],
                    'tags' => $this->input->post('tags'),
                    'network_id' => $this->data['user']['network_id'],
                    'type' => 'news',
                );

                if ($this->input->post('schedule')) {
                    $update['is_hide'] = 1; // hidden
                    $update['schedule'] = date('Y-m-d', strtotime(str_replace("/", "-", $this->input->post('schedule')))); // Chuyển từ mm/dd/yyyy sang yyyy/mm/dd
                }
                $path = PATH_UPLOAD . 'news/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    if ($this->data['news']['image']) {
                        $img_name = $this->data['news']['image'];
                    } else {
                        $img_name = $alias . '.jpg';
                    }
                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $update['image'] = $img_name;
                }

                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'news/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => true,
                        'file_name' => 'fb_' . $img_name
                    ));

                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }
                // status > 1 =>  status = 2: doi duyet, 3 ban nhap
                //check bai viet truoc 3 ngay full controll
                if ($this->getdays($this->data['news']['date_create'], date("Y-m-d")) < 3 || $this->data['news']['status'] > 1) {
                    $this->news_model->update($update, $id);
                    $this->data['msg'] = 'Cập nhập thành công';
                    $this->data['check_error'] = 0;
                    //bai viet sau 3 ngayf neu la dang xuat ban thi duoc luu vo ban tam.
                } elseif ($this->data['news']['status'] <= 1 && $this->data['user']['username'] == $user_write['username']) {
                    $tmp_name = $id . '-' . $this->data['news']['alias'];
                    @mkdir(APPPATH . '/cache/news_update', 0777);
                    @unlink(APPPATH . 'cache/news_update/' . $tmp_name);
                    file_put_contents(APPPATH . 'cache/news_update/' . $tmp_name, serialize($update));
                    $this->data['msg'] = 'Tin Của bạn đã được lưu và đợi leader duyệt lại';
                    $this->data['check_error'] = 0;
                    $this->news_model->update(array('status' => -1), $id);
                }
                // bai viet la tai xuat ban , va hon 3 ngay , va phai la leader moi xuat ban, hoac neu la editor_manager duyet dc moi bai viet
                elseif (($_POST['submit'] == -1 && $this->getdays($this->data['news']['date_create'], date("Y-m-d")) > 3 && $this->data['user']['id'] == $user_write['leader']) || $this->data['user']['manager_editor'] == 1) {
                    $this->data['msg'] = 'Tái xuất bản thành công';
                    $this->data['check_error'] = 0;
                    $update['status'] = 1;
                    $this->news_model->update($update, $id);
                    $tmp_name = $id . '-' . $this->data['news']['alias'];
                    $this->data['news']['status'] = 1;
                    @unlink(APPPATH . 'cache/news_update/' . $tmp_name);
                    // end quyen tai xuat ban => hide button 
                    $this->data['show_button'] = -1;
                }
            }
            /// get data show form edit
            $tmp_name = $id . '-' . $this->data['news']['alias'];
            $tmp_news = @file_get_contents(APPPATH . 'cache/news_update/' . $tmp_name);
            if ($tmp_news) {
                $this->data['news'] = unserialize($tmp_news);
            } else {
                $this->data['news'] = $this->news_model->get_by_key($id);
            }
        }
        if ($this->data['news']['news_ids'] != ',' && $this->data['news']['news_ids'] != '') {
            $temp = '(' . trim($this->data['news']['news_ids'], ',') . ')';
            $cond = 'id   in' . $temp;
            $this->data['link_news'] = $this->news_model->get_by($cond, null, 'id');
        }

        ///pre($this->data['show_button']);
        if (($this->data['user']['id'] == $user_write['leader'] || $this->data['user']['manager_editor'] == 1) && $this->data['news']['status'] == -1)
            $this->template->write_view('content_block', 'news/leader_edit', $this->data);
        else
            $this->template->write_view('content_block', 'news/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->data['news'] = $this->news_model->get_by_key($id);
        if ($this->data['user']['root'])
            $this->news_model->delete($id);
        // check bai viet da xuat ban va sau 3 ngay
        if ($this->data['news']['status'] <= 1 && $this->getdays($this->data['news']['date_create'], date("Y-m-d")) > 3 && $this->data['user']['root'] != 1)
            echo "Liên Hệ Admin Để xóa";
        else {
            $this->news_model->delete($id);
        }
    }

    public function index() {
        $this->data['pos'] = 0;
        $this->template->write_view('content_block', 'news/index', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        // get list user of manager_user
        $writer = $this->admin_model->get_by("leader = '" . $this->data['user']['id'] . "'");
        if (!empty($writer)) {
            $str_username = "('" . $this->data['user']['username'] . "',";
            foreach ($writer as $value) {
                $str_username .= "'" . $value['username'] . "',";
            }
            $str_username = substr($str_username, 0, -1) . ")";
            $cond = "author_username in " . $str_username;
        } else {
            $cond = "author_username ='" . $this->data['user']['username'] . "'";
        }
        if ($this->data['user']['manager_editor'])
            $cond = " id > 0";
        $cat_id = @intval($_POST['cat_id']);
        $status = @intval($_POST['status']);
        if ($cat_id) {
            $cond .= ' and cate_id = ' . $cat_id;
        }
        $author_username = @$_POST['author_username'];
        if ($author_username) {
            $cond .= " and author_username = '" . $author_username . "'";
        }
        $keyword = @$_POST['keyword'];
        if ($keyword) {
            $cond .= " and ( id = '$keyword' or title like '%$keyword%')";
        }

        if ($status != "") {
            $cond .= ' and status = ' . $status;
        }
        $limit = 15;
        $order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $data['results'] = $this->news_model->get_for_page($limit, $pos, $cond, $order);
        // set show/hide action
        foreach ($data['results'] as $key => $value) {
            $data['results'][$key]['range_date'] = $this->getdays($value['date_create'], date("Y-m-d"));
            if ($value['status'] == -1) {
                $tmp_name = $value['id'] . '-' . $value['alias'];
                $tmp_news = @file_get_contents(APPPATH . 'cache/news_update/' . $tmp_name);
                // just get data - show  of tmp
                $data['results'][$key]['title'] = unserialize($tmp_news)['title'];
                $data['results'][$key]['icon'] = "<i class='fa fa-copy'></i> Tái xuất bản";
                $data['results'][$key]['id'] = $value['id'];
                $data['results'][$key]['author_username'] = $value['author_username'];
                // show edit tai xuat ban cho manager_editor va nguoi giam sat
                $data['results'][$key]['edit'] = 1;
            }

            //show icon -1 tai xuat ban, 1 xuat ban, 2 doi duyet, 3 ban nhap
            else if ($value['status'] == 2)
                $data['results'][$key]['icon'] = "<i class='fa fa-hourglass-2'></i> Đợi duyệt";
            else if ($value['status'] == 3)
                $data['results'][$key]['icon'] = "<i class='fa fa-sticky-note'></i> Bản nháp";
            else
                $value['status'] < 1 || $data['results'][$key]['range_date'] > 3 ? $data['results'][$key]['icon'] = "<i class='fa fa-check'> Đã xuất bản</i>" : $data['results'][$key]['icon'] = "<i class='fa fa-check'> Đã xuất bản</i>";
            //show function del//1 : show xoa, nguoc lai ( del bai viet cua chinh tac gia,leader, manager_editor va truoc 3 ngay)
            $data['results'][$key]['del'] = $data['results'][$key]['range_date'] < 3 ? 1 : 0;
            //show function edit//1 : show edit, nguoc lai ( edit bai viet cua chinh tac gia, va chi co user quan ly tai xuat ban)
            if ($data['results'][$key]['range_date'] < 3 || $this->data['user']['username'] == $value['author_username'])
                $data['results'][$key]['edit'] = 1;
            if (($this->data['user']['manager_editor'] || $this->data['user']['root']) && $data['results'][$key]['status'] == 2) {
                $data['results'][$key]['edit'] = 1;
            }
            if ($this->data['user']['root']) {
                $data['results'][$key]['del'] = 1;
                $data['results'][$key]['edit'] = 1;
            }
            //khi khong dc sua va xoa thi chi dc xem
            !$data['results'][$key]['del'] && !$data['results'][$key]['edit'] ? $data['results'][$key]['view_web'] = 1 : $data['results'][$key]['view_web'] = 0;
        }

        $total = $this->news_model->get_total_rows($cond);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('news/list', $data);
    }

    function search() {
        if (isset($_GET['q'])) {

            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond .= "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function approved() {
        $id = intval(@$_POST['id']);
        $detail = $this->news_model->get_by_key($id);
        $user_write = $this->admin_model->get_row_by("username = '" . $detail['author_username'] . "'");
        if ($user_write['leader'] == $this->data['user']['id'] && $_POST['status'] == 'approved') {
            if ($this->news_model->update(array('status' => 1), $id))
                echo "approved_ok";
            else {
                echo "";
            }
        } else {
            echo "";
        }
    }

    function update_hot() {
        if (isset($_POST['data'])) {
            $this->news_model->update_batch($_POST['data']);
        }
    }

}
