<?php

class Network extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('network_model', 'department_model', 'role_controller_model'));
        $this->load->library(array('alias', 'form_validation'));
        $this->load->helper('database');
        $this->data['type'] = field_enums('network', 'type');
        $this->data['role_controller'] = $this->role_controller_model->get_all();
        $this->data['role_order_progress'] = array('check', 'censor', 'trash', 'empty');
        $this->data['pre'] = 'network';
    }

    public function add() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $role_controller = implode(',', $this->input->post('role_controller'));
                $role_order_progress = implode(',', $this->input->post('role_order_progress'));
                $add = array(
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'content' => $this->input->post('content'),
                    'date_create' => date('Y-m-d H:i:s'),
                    'website' => $this->input->post('website'),
                    'address' => $this->input->post('address'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'role_controller' => $role_controller,
                    'role_order_progress' => $role_order_progress
                );
                //pre($add);

                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'network/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->alias->create_alias($_FILES['image']['name'])
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        // $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'zithumb_' . $img['file_name'], 36, 36);
                        $image = $img['file_name'];
                        $add['image'] = $image;
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }
                $this->data['check_error'] = 0;
                $id = $this->network_model->insert($add);
//                $department = array(
//                    'name' => 'Điều hành',
//                    'type' => 'root',
//                    'network_id' => $id,
//                );
//                $this->department_model->insert($department);
            }
        }
        $this->template->write_view('content_block', 'network/add', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['pos'] = intval(@$_COOKIE['pos_page']);
        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];
        $this->data['results'] = $this->network_model->get_all();
        $this->template->write_view('content_block', 'network/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['partner'] = $this->network_model->get_by_key($id);
        // pre($this->data['partner']);
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $role_controller = implode(',', $this->input->post('role_controller'));
                $role_order_progress = implode(',', $this->input->post('role_order_progress'));
                $update = array(
                    'name' => $this->input->post('name'),
                    'type' => $this->input->post('type'),
                    'content' => $this->input->post('content'),
                    'website' => $this->input->post('website'),
                    'address' => $this->input->post('address'),
                    'phone' => $this->input->post('phone'),
                    'email' => $this->input->post('email'),
                    'role_controller' => $role_controller,
                    'role_order_progress' => $role_order_progress
                );


                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'network/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->alias->create_alias($_FILES['image']['name'])
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $update['logo'] = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $this->data['check_error'] = 0;
                $this->network_model->update($update, $id);

                $department_list = $this->department_model->get_by('network_id =' . $id);

//                if (empty($department_list)) {
//
//                    $department = array(
//                        'name' => 'Điều hành',
//                        'type' => 'root',
//                        'network_id' => $id,
//                    );
//                    $this->department_model->insert($department);
//                }
            }
        }
        $this->data['partner'] = $this->network_model->get_by_key($id);
        $this->template->write_view('content_block', 'network/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->network_model->delete($id);
    }

    function load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];
        if ($type) {
            $cond .= ' and type = "' . $type . '"';
        }

        if ($keyword) {
            $cond.=" and ( id = '$keyword' or name like '%$keyword%')";
            //$cond .= " and title like '%$keyword%'";
        }
        //echo $cond; exit;
        $limit = 20;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $data['results'] = $this->network_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->network_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("type_cookies", $type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('network/list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond = "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

}
