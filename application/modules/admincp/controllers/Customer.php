<?php

/**
 * @property Video_model $customer_model
 */
class Customer extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('customer_model', 'services_model', 'order_sku_model', 'network_model', 'admin_model', 'transaction_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->load->helper('database');
        $this->data['pre'] = 'customer';
        $this->data['msg_error'] = $this->session->flashdata('msg_error');
        $this->data['staff'] = $this->admin_model->get_by('network_id = ' . $this->data['user']['network_id'] . ' and role_controller LIKE "%order%"');
        $this->data['services'] = $this->services_model->get_all(null, 'alias');
        $this->data['status'] = field_enums('customer', 'status');
    }

    function index($network_id = 0) {
        $this->data['staff'] = $this->admin_model->get_by('network_id = ' . $network_id . ' and role_controller LIKE "%order%"');
        if ($network_id <= 0 && empty($this->data['user']['role_network']))
            redirect(base_url('admincp/customer/' . $this->data['user']['network_id']));
        elseif (empty($this->data['user']['role_network']))
            $this->data['networks'] = '';
        else
            $this->data['networks'] = $this->network_model->get_all();
        $network = $this->network_model->get_by_key($network_id);
        if (empty($network))
            redirect(base_url('admincp/customer/' . $this->data['user']['network_id']));
        $this->data['pos'] = intval(@$_COOKIE['order_pos']);
        $this->data['status'] = @$_COOKIE['order_status'];
        $this->data['network_id'] = $network_id;
        $this->template->write_view('content_block', 'customer/index', $this->data);
        $this->template->render();
    }

    function progress_update() {
        $msg = $_POST['msg'];
        $id = intval(@$_POST['id']);
        $data = $this->customer_model->get_by_key($id);
        if (!empty($data)) {
            $data['progress'] = @unserialize($data['progress']);
            $row = array(
                'date_create' => date('Y-m-d H:i:s'),
                'username' => $this->data['user']['username'],
                'content' => $msg,
            );
            $data['progress'][] = $row;
            $row['date_create'] = date('d/m/Y');
            $str = serialize($data['progress']);
            $this->customer_model->update(array('progress' => $str), $id);
            echo json_encode($row, true);
        }
    }

    function save_order() {
        $data = @$_POST['data'];
        if (empty($data['id'])) {
            $data['date_create'] = date('Y-m-d H:i:s');
            $data['network_id'] = $this->data['user']['network_id'];
            $data['username'] = $this->data['user']['username'];
            $id = $this->customer_model->insert($data);
            echo $id;
            echo 'Khách hàng mới có số điện thoại <b style="font-size:20px"> ' . $data['phone'] . '</b> đã được tạo thành công';
            exit;
        } else {
            $customer = $this->customer_model->get_by_key($data['id']);
            if (empty($customer)) {
                echo 0;
                exit;
            } else {
                $data['last_update'] = date('Y-m-d H:i:s');
                $data['consultant'] = $this->data['user']['fullname'];

                // Không được phép edit số điện thoại nếu không phải là người add
                if ($customer['phone']) {
                    if ($data['phone'] != $customer['phone'] && $customer['username'] != $this->data['user']['username']) {
                        echo 'Bạn không được phép thay đổi số điện thoại vì bạn không phải người tạo ra';
                        exit;
                    }
                }

                // Nếu quên update trạng thái thì sẽ tự động chuyển qua đã gọi
                if ($customer['status'] == 'new' && $data['content']) {
                    $data['status'] = 'called';
                }

                $this->customer_model->update($data, $data['id']);
                echo 'Khách hàng có số điện thoại <b style="font-size:20px"> ' . $data['phone'] . '</b> đã được cập nhật thành công';
                exit;
            }
        }
    }

    function profile() {
        $id = intval(@$_POST['id']);
        $data['services'] = $this->data['services'];
        $data['customer_status'] = $this->data['customer_status'];
        $data['customer_source'] = $this->data['customer_source'];
        $data['status'] = $this->data['status'];
        $data['customer'] = $this->customer_model->get_by_key($id);

        // Nếu có ID thì save người tư vấn vào lại
        if ($data['customer']['id'] && $data['customer']['consultant'] == '') {
            $data_save['consultant'] = $this->data['user']['fullname'];
            $data_save['consultant_user'] = $this->data['user']['username'];
            $data_save['status'] = 'calling';
            $this->customer_model->update($data_save, $id);
        }

        $this->load->view('customer/profile', $data);
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $status = @$_POST['status'];
        $keyword = @$_POST['keyword'];
        $network_id = @$_POST['network_id'];
        $employees = @$_POST['employees'];
        if (empty($this->data['user']['role_network'])) {
            $cond = 'network_id  = ' . $network_id;
//            if (empty($this->data['user']['role_department'])) {
//                $cond .= ' and (consultant =' . $this->data['user']['username'];
//                $cond .= ' or username =' . $this->data['user']['username'] . ')';
//            }
        } else {
            $cond = 'network_id = ' . $network_id;
        }
        if ($keyword)
            $cond .= " and (phone = '$keyword' or full_name like '%$keyword%')";
        if ($employees)
            $cond .= " and consultant_user = '$employees'";

        //else {
        if ($status)
            $cond .= ' and status = "' . $status . '"';
        else
            $cond .= ' and status != "done" and status != "trash"';
        //}

        $limit = 10;
        $order = 'id DESC';
        $this->data['results'] = $this->customer_model->get_for_page($limit, $pos, $cond, $order);
        echo $cond;
        //pre($data['results']);
        $total = $this->customer_model->get_total_rows($cond);
        @setcookie("order_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        @setcookie("order_status", $status, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('customer/list', $this->data);
        return false;
    }

    function trash() {
        $id = intval(@$_POST['id']);
        $this->customer_model->update(array('status' => 'trash'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa');
        echo 1;
    }

    function undo() {
        $id = intval(@$_POST['id']);
        $this->customer_model->update(array('status' => 'new'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa được phục hồi');
        echo 1;
    }

    function trash_empty() {
        $id = intval(@$_POST['id']);
//        if (!$this->check_role_order($id, 'empty'))
//            return false;
        $this->customer_model->delete($id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa hoàn toàn');
        echo 1;
    }

    private function check_role_order($id, $role_order_progress) {
        $order = $this->customer_model->get_by_key($id);
        if (empty($order))
            return false;
        if (!empty($this->data['user']['role_network']) || $role_order_progress == 'view')
            return true;
        if (strpos(',' . $order['network_id'] . ',', ',' . $this->data['user']['network_id'] . ',') === false)
            return false;
        if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',' . $role_order_progress . ',') === false)
            return false;
        return true;
    }

}
