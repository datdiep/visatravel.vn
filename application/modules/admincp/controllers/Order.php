<?php

/**
 * @property Video_model $order_services_model
 */
class Order extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('order_services_model','services_model', 'order_sku_model','network_model','admin_model','transaction_model','profile_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'order';
        $this->data['msg_error'] = $this->session->flashdata('msg_error');
        $this->data['staff'] = $this->admin_model->get_by('network_id = '.$this->data['user']['network_id'].' and role_controller LIKE "%order%"');
        $this->data['services'] = $this->services_model->get_all(null,'alias');
	//pre($this->data['user']);
    }

    function index($network_id = 0) {
        if($network_id <= 0 && empty($this->data['user']['role_network']))
            redirect(base_url('admincp/order/'.$this->data['user']['network_id']));
        else
            $this->data['networks'] = $this->network_model->get_all();
        $network = $this->network_model->get_by_key($network_id);
        if(empty($network))
            redirect(base_url('admincp/order/'.$this->data['user']['network_id']));
        $this->data['pos'] = intval(@$_COOKIE['order_pos']);
        $this->data['status'] = @$_COOKIE['order_status'];
        $this->data['network_id'] = $network_id;
        $this->template->write_view('content_block', 'order/index', $this->data);
        $this->template->render();
    }
    function process($id = 0) {
        $id = intval($id);
        if($id <= 0){
            $this->data['order']['id'] = 0;
	    $this->data['pre'] = 'order_add';
        }else{
            $this->data['order'] = $this->order_services_model->get_by_key($id);
            if(empty($this->data['order']))
                redirect(base_url('admincp/order/'.$this->data['user']['network_id']));
	    $this->data['load_chat'] = $this->load_block_chat('order_services_model', $id);
            $this->data['transaction'] = $this->transaction_model->get_by('order_id = '.$id);
	    $this->data['profiles'] = $this->profile_model->get_by('order_ids LIKE "%,'.$id.',%"','id DESC');
        }
        $this->data['msg_new_order'] = $this->session->flashdata('msg_new_order');
        $this->template->write_view('content_block', 'order/process', $this->data);
        $this->template->render();
    }
    function progress_update(){
	$msg = $_POST['msg'];
        $id = intval(@$_POST['id']);
	$data = $this->order_services_model->get_by_key($id);
	if (!empty($data)) {
            $data['progress'] = @unserialize($data['progress']);	    
	    $row = array(
		'date_create' => date('Y-m-d H:i:s'),
		'username' => $this->data['user']['username'],
		'content' => $msg,
	    );
	    $data['progress'][] = $row;
	    $row['date_create'] = date('d/m/Y');
	    $str = serialize($data['progress']);
	    $this->order_services_model->update(array('progress' => $str), $id);
	    echo json_encode($row, true);
           
        }
    }
    function save_order(){
        $data = @$_POST['data'];
        $data_transaction = @$_POST['data_transaction'];
        //pre($data,false);
	//pre($data_transaction);
        if (empty($data['id'])) {
            $data['date_create'] = date('Y-m-d H:i:s');
            $data['network_id'] = ','.$this->data['user']['network_id'].',';
            $id = $this->order_services_model->insert($data);
            if(!empty($data_transaction)){
                foreach ($data_transaction as $k => $v) {
                    $data_transaction[$k]['date_create'] = date('Y-m-d H:i:s');
                    $data_transaction[$k]['user_create'] =  $this->data['user']['username'];
                    $data_transaction[$k]['order_id'] =  $id;
		    $data_transaction[$k]['network_id'] = $this->data['user']['network_id'];
                }
                $this->transaction_model->insert_batch($data_transaction);
            }
            $this->session->set_flashdata('msg_new_order', 'Đơn hàng mới có mã số <b style="font-size:20px">#'.$id.'</b> đã được tạo thành công');
            echo $id; exit;
        } else {
            $order = $this->order_services_model->get_by_key($data['id']);
	    if (empty($order)){
		echo 0; exit;
	    }else{
		$this->order_services_model->update($data, $data['id']);
		echo $order['id']; exit;
	    }
            
        }
    }
    function save_transaction(){
        $data = @$_POST['transaction'];
	$order = @$_POST['order'];
	//pre($transaction);
        if (empty($data['id'])) {
	    $data['date_create'] = date('Y-m-d H:i:s');
	    $data['user_create'] =  $this->data['user']['username'];
	    $data['network_id'] = $this->data['user']['network_id'];
	    $id = $this->transaction_model->insert($data);
	    $this->order_services_model->update($order, $order['id']);
            echo $id; exit;
        } else {
            $transaction = $this->transaction_model->get_by_key($data['id']);
	    if (empty($transaction)){
		echo 0; exit;
	    }else{
		if($transaction['un_lock'] == 0){
		    $this->transaction_model->update($data, $data['id']);
		    $this->order_services_model->update($order, $order['id']);
		    echo $data['id']; exit;
		}
	    }
            
        }
    }
    function del_transaction(){
	$id = intval(@$_POST['id']);
	$transaction = $this->transaction_model->get_by_key($id);
	if(!empty($transaction) && !empty($this->data['user']['role_network'])){
	    if(($transaction['user_create'] == $this->data['user']['username'] || !empty($this->data['user']['role_network'])) && $transaction['un_lock'] == 0){
		$this->transaction_model->delete($id);
		$order = $this->order_services_model->get_by_key($transaction['order_id']);
		if(!empty($order)){
		    $arrears = $order['arrears'] + $m;
		    $total = $order['total'];
		    if($arrears < 0){
			$total = $order['total'] - $arrears;
			$arrears = 0;
		    }
		    $this->order_services_model->update(array('arrears' => $arrears,'total' => $total), $order['id']);
		}
		echo 1; exit;
	    }else{
		echo -1; exit;
	    }
	}else{
	    echo 0, exit;
	}
	
        
    }
    function lock_transaction(){
        $id = intval(@$_POST['id']);
	$transaction = $this->transaction_model->get_by_key($id);
	if(!empty($transaction) && !empty($this->data['user']['role_network'])){
	    if($transaction['un_lock'] == 0){
		$this->transaction_model->update(array('un_lock' => 1), $id);
		echo 1; exit;
	    }else{
		$this->transaction_model->update(array('un_lock' => 0), $id);
		echo 0; exit;
	    }
        } else {
	    echo -1; exit;
            
        }
    }
    function profile() {
	$id = intval($_POST['id']);
	$this->data['profile'] =  $this->profile_model->get_by_key($id);
	if (empty($this->data['profile'] )){
	    $this->data['profile']['id'] = 0; 
	}
        $this->load->view('order/profile', $this->data);
    }
    function save_profile(){
        $data = @$_POST['data'];
	$data['ngay_sinh'] = date('Y-m-d', strtotime(str_replace('/', '-',$data['ngay_sinh'])));
	$data['ngay_cap_cmnd'] = date('Y-m-d', strtotime(str_replace('/', '-',$data['ngay_cap_cmnd'])));
	$data['ngay_cap_ho_chieu'] = date('Y-m-d', strtotime(str_replace('/', '-',$data['ngay_cap_ho_chieu'])));
	$data['ngay_het_han_ho_chieu'] = date('Y-m-d', strtotime(str_replace('/', '-',$data['ngay_het_han_ho_chieu'])));
	$data['visa_hien_co'] = serialize($data['visa_hien_co']);
	$data['thong_tin_gia_dinh'] = serialize($data['thong_tin_gia_dinh']);
	$data['danh_sach_dich_vu'] = serialize($data['danh_sach_dich_vu']);
        if (empty($data['id'])) {
            $data['ngay_tao'] = date('Y-m-d H:i:s');
            $data['nguoi_tao'] = $this->data['user']['username'];
	    $data['order_ids'] = ','.$data['order_ids'].',';
            $id = $this->profile_model->insert($data);
	    $data['id'] = $id;
        } else {
            $profile = $this->profile_model->get_by_key($data['id']);
	    if (empty($profile)){
		echo 0; exit;
	    }else{
		if(strpos($profile['order_ids'], ','.$data['order_ids'].',') === false){
		    $data['order_ids'] = $profile['order_ids'].$data['order_ids'].',';
		}else{
		    $data['order_ids'] = $profile['order_ids'];
		}
		$this->profile_model->update($data, $data['id']);
	    }
            
        }
	$this->data['profile'] = $data;
	$this->load->view('order/show_profile', $this->data);
    }
    function del_profile(){
	$id = intval($_POST['id']);
	$order_id = $_POST['order_id'];
	$profile = $this->profile_model->get_by_key($id);
	if(!empty($profile)){	
	    $profile['order_ids'] = str_replace($order_id.',', "", $profile['order_ids']);
	    if(strlen($profile['order_ids']) == 1){
		$profile['order_ids'] = "";
	    }
	    $this->profile_model->update(array('order_ids' => $profile['order_ids']), $id);
	    echo $id; exit;
	}else{
	    echo -1; exit;
	}
    }
    function page() {
        $pos = intval(@$_POST['pos']);
        $status = @$_POST['status'];
        $keyword = @$_POST['keyword'];
        $network_id = $_POST['network_id'];
        if (empty($this->data['user']['role_network'])) {
            $cond = 'network_id like "%,' . $this->data['user']['network_id'] . ',%"';
            if (empty($this->data['user']['role_department'])) {
                $cond .= ' and (who_consultant = "'.$this->data['user']['username'].'"';
                $cond .= ' or who_handle like "%,' . $this->data['user']['username'] . ',%")';
            }
        }else{
            $cond = 'network_id like "%,' . $network_id . ',%"';
        }
        if ($keyword)
            $cond .= " and (phone = '$keyword' or email like '%$keyword%')";
        else{
            if ($status)
                $cond .= ' and status = "' . $status . '"';
	    else
		$cond .= ' and status != "done" and status != "trash"';
        }
        $limit = 20;
        $order = 'status ASC, id DESC';
        $this->data['results'] = $this->order_services_model->get_for_page($limit, $pos, $cond, $order);
        //pre($data['results']);
        $total = $this->order_services_model->get_total_rows($cond);
        setcookie("order_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_status", $status, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $this->data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('order/list', $this->data);
        return false;
    }


    function trash() {
        $id = intval(@$_POST['id']);
        $this->order_services_model->update(array('status' => 'trash'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa');
        echo 1; exit;
    }
    
    function undo() {
        $id = intval(@$_POST['id']);
        $this->order_services_model->update(array('status' => 'process'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa được phục hồi');
        echo 1; exit;
    }

    function trash_empty() {
        $id = intval(@$_POST['id']);
	$order = $this->order_services_model->get_by_key($id);
	if(!empty($order) && !empty($this->data['user']['role_network'])){
	    $this->order_services_model->delete($id);
	    $transaction = $this->transaction_model->delete_where('order_id ='. $id);
	    $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa hoàn toàn');
	    echo 1; exit;
	}
	echo 0; exit;
    }

    private function check_role_order($id, $role_order_progress) {
        $order = $this->order_services_model->get_by_key($id);
        if (empty($order))
            return false;
        if (!empty($this->data['user']['role_network']) || $role_order_progress == 'view')
            return true;
        if (strpos(',' . $order['network_id'] . ',', ',' . $this->data['user']['network_id'] . ',') === false)
            return false;
        if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',' . $role_order_progress . ',') === false)
            return false;
        return true;
    }

    function update_passport() {
        $data = @$_POST['data'];
        $result = array();
        if (!$this->check_role_order($data['order_id'], 'check'))
            return false;
        if ($data['id'] == '' || empty($data['id'])) {
            unset($data['id']);
            $id = $this->order_sku_model->insert($data);
        } else {
            $order_sku = $this->order_sku_model->get_by_key($data['id']);
            if (empty($order_sku))
                return false;
            if (!empty($data['nationality'])) {
                $this->load->model('country_model');
                $country_old = $this->country_model->get_row_by(array('name' => $order_sku['nationality']));
                $country_new = $this->country_model->get_row_by(array('name' => $data['nationality']));
                $nationality_fees = $country_new['special_fee'] - $country_old['special_fee'];
                if ($nationality_fees != 0) {
                    $order = $this->order_services_model->get_by_key($data['order_id']);
                    $result['nationality_fees'] = $order['nationality_fees'] + $nationality_fees;
                    $result['total_fees'] = $order['total_fees'] + $nationality_fees;
                    $this->order_services_model->update($result, $data['order_id']);
                    $data['special_fee'] = $country_new['special_fee'];
                }
            }
            $this->order_sku_model->update($data, $data['id']);
        }
        echo json_encode($result);
    }

}
