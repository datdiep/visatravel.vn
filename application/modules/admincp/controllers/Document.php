<?php

class Document extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('document_model', 'network_model', 'folder_model', 'ticket_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation', 'upload', 'my_category'));
        $this->data['type'] = $this->folder_model->get_by(array('type' => 'document', 'network_id' => $this->data['my_network']['id']), 'priority DESC');

        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['staff'] = $this->admin_model->get_by($cond);

        $this->data['pre'] = 'document';
    }

    public function del_folder() {
        $id = intval(@$_POST['id']);
        $this->folder_model->delete($id);
    }

    public function manage_folder($id = 0) {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $data = array(
                    'name' => $this->input->post('name'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'parent' => $this->input->post('parent'),
                    'type' => 'document',
                    'priority' => $this->input->post('priority'),
                    'network_id' => $this->data['user']['network_id'],
                );

                if ($id == 0) {
                    $this->data['check_error'] = 0;
                    $this->folder_model->insert($data);
                } else {
                    $this->folder_model->update($data, $id);
                }
            }
        }


        if ($id != 0) {
            $this->data['folder'] = $this->folder_model->get_by_key($id);

            $this->data['list_parent'] = $this->folder_model->get_by('id != ' . $id . '');

            if (empty($this->data['folder']) || $this->data['folder']['network_id'] != $this->data['user']['network_id'])
                redirect(base_url('/admincp/document/'));
        }



        $this->template->write_view('content_block', 'document/manage_folder', $this->data);
        $this->template->render();
    }

    public function add() {

        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $folder = $this->folder_model->get_by_key($this->input->post('folder_id'));
                $add = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'content' => $this->input->post('content'),
                    'youtube' => $this->input->post('youtube'),
                    'publish_date' => date('Y-m-d H:i:s'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags'),
                    'folder_id' => $this->input->post('folder_id'),
                    'folder_name' => $folder['name'],
                    'network_id' => $this->data['user']['network_id'],
                );
                $this->data['check_error'] = 0;
                $id = $this->document_model->insert($add);
                if ($_POST['submit'] == 2) {
                    redirect(base_url('admincp/document/edit/' . $id));
                }
            }
        }
        $this->template->write_view('content_block', 'document/add', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['pos'] = intval(@$_COOKIE['pos_page']);

        $this->data['type_cookies'] = @$_COOKIE['type_cookies'];

        $this->data['results'] = $this->document_model->get_all();
        $this->template->write_view('content_block', 'document/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;

        if (isset($_POST['submit']) && $_POST['submit'] != '-1') {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $folder = $this->folder_model->get_by_key($this->input->post('folder_id'));
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'content' => $this->input->post('content'),
                    'youtube' => $this->input->post('youtube'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags'),
                    'folder_id' => $this->input->post('folder_id'),
                    'folder_name' => $folder['name'],
                );

                //$this->data['check_error'] = 0;
                $this->document_model->update($update, $id);
                $this->data['check_error'] = 0;
            }
        }

        $this->data['page'] = $this->document_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('document_model', $id);
        $this->template->write_view('content_block', 'document/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->document_model->delete($id);
    }

    function load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];

        $cond .= ' and network_id = ' . $this->data['user']['network_id'] . ' ';
        if (!empty($this->data['user']['role_network'])) {
            $cond = 'id > 0';
        }

        if ($type) {
            $cond .= ' and folder_id = "' . $type . '"';
        }

        if ($keyword) {
            $cond .= " and ( id = '$keyword' or title like '%$keyword%')";
        }

        $limit = 20;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $data['results'] = $this->document_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->document_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("type_cookies", $type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('document/list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond .= "title like '%$keyword%' or id = " . intval($keyword);
            $cond .= " and network_id = " . $this->data['user']['network_id'];
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function document_print($id) {

        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);

        $this->data['page'] = $this->document_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('document_model', $id);
        $this->document_model->update(array('view' => $this->data['page']['view'] + 1), $id);

        $this->load->view('document/print', $this->data);
    }

    function sendmail($id) {
        $this->data['check_error'] = -1;
        $cond = 'id = ' . $this->data['user']['network_id'];
        $this->data['network'] = $this->network_model->get_row_by($cond);
        $this->data['page'] = $this->document_model->get_by_key($id);

        $this->data['load_file'] = $this->load_block_file('document_model', $id);
        $this->template->write_view('content_block', 'document/sendmail', $this->data);
        $this->template->render();
    }

    function test() {
        $this->load->library('pdf');
        $data = getDataPdf(APPPATH . '../assets/pdf/FormNhat.pdf');
        pre($data, false);
        //if ($_POST) {
        $params = array();
        $params = array(
            'topmostSubform[0].Page1[0].T49[0]' => 'fdsfsdafsafa',
            'topmostSubform[0].Page1[0].#area[5].#area[6].#area[7].RB1[0]' => 'F',
            'topmostSubform[0].Page1[0].#area[8].RB2[0]' => 4,
            'topmostSubform[0].Page1[0].#area[1].#area[2].RB3[0]' => 4,
            'topmostSubform[0].Page2[0].RB1[1]' => 1,
            'topmostSubform[0].Page1[0].T50[0]' => 'Diệp Minh Đạt'
        );
//            foreach ($_POST as $k => $v) {
//                $k = str_replace(array('__', '_'), array('.', ' '), $k);
//                $params[$k] = $v;
//            }
//            echo '<pre>';
//            print_r($params);exit;
        if (fillFormPdf(APPPATH . '../assets/pdf/FormNhat.pdf', $params, APPPATH . '../assets/pdf/deploys/FormNhatoutput.pdf') == 1) {
            echo '<a href="./deploys/956aoutput.pdf?t=' . time() . '" target="_blank">Check</a>';
        } else {
            echo 'Fail';
        }
        // }

        $this->load->view('document/test');
    }

}
