<?php

/**
 * @property Video_model $order_services_model
 */
class Order extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('order_services_model', 'order_sku_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'order';
        $this->load->helper('database');
        $this->data['progress'] = field_enums('order_services', 'progress');
        $this->data['services'] = field_enums('customer', 'services');
        $this->data['payment'] = field_enums('customer', 'paid_method');

        $this->load->library(array('my_category'));
        $this->data['service_fees'] = $this->my_category->build_visa_fees();
        $this->data['map_order'] = array(
            'create' => '<span class="badge bg-light-blue">Mới</span>',
            'process' => '<span class="badge bg-yellow">25%</span>',
            'transfer' => '<span class="badge bg-red">50%</span>',
            'receive' => '<span class="badge bg-green">75%</span>',
            'complete' => '<span class="badge bg-gray">100%</span>',
            'wait' => '<span class="badge bg-gray">85%</span>',
            'waiting' => '<span class="badge bg-gray">95%</span>',
            '1 month single' => '1 S',
            '3 month single' => '1 S',
            '1 month multiple' => '1 M',
            '3 month multiple' => '3 M',
            '6 month multiple' => '6 M',
            '1 year multiple' => '1 Y',
            'Normal' => '+2 days',
            'Urgent' => '+8 hours',
            'Emergency' => '+30 minutes'
        );
        $this->data['status'] = array(
            'handle' => 'Xử lý đơn',
            'final' => 'Hoàn tất',
            'deny' => 'Từ chối'
        );
        if ($this->data['user']['network_type'] != 'issue visa') {
            $this->data['status']['cancel'] = 'Đơn bị hủy';
        }

        if (!empty($this->data['user']['role_network']))
            $this->data['status']['trash'] = 'thùng rác';


        $this->data['msg_error'] = $this->session->flashdata('msg_error');
    }

    function process() {
        $this->data['pre'] = 'order_process';
        $this->data['web_title'] = 'Hồ sơ khách hàng đang xử lý';
        $this->data['order_pos'] = intval(@$_COOKIE['order_pos']);
        $this->data['order_status'] = (@$_COOKIE['order_status']);
        $this->data['order_progress'] = (@$_COOKIE['order_progress']);
        $this->data['has_get_money'] = (@$_COOKIE['has_get_money']);

        $this->template->write_view('content_block', 'order/process_index', $this->data);
        $this->template->render();
    }

    function process_list() {

        $this->data['pre'] = 'order_process';
        $pos = intval(@$_POST['pos']);
        $progress = $_POST['progress'];
        $status = $_POST['status'];
        $has_get_money = intval($_POST['has_get_money']);

        $cond = 'status = "' . $status . '" and services != "" ';

        $data['map_services'] = $this->data['map_services'];
        $data['user'] = $this->data['user'];
        if (empty($this->data['user']['role_network'])) {
            $cond .= ' and network_id like "%,' . $this->data['user']['network_id'] . ',%"';
        }
        if ($progress == 5)
            $cond = 'status != "all"';

        if ($progress) {
            $cond .= ' and progress = ' . $progress;
        }
        if ($has_get_money != -1) {
            $cond .= ' and has_get_money = ' . $has_get_money;
        }
        $keyword = @$_POST['keyword'];
        if ($keyword) {
            $cond.=" and ( id = '$keyword' or email like '%$keyword%')";
        }
// echo $cond.'<br>'; exit;
        $limit = 20;
        $order = 'has_get_money DESC, processing_time_lv DESC, progress ASC';
        $data['map_order'] = $this->data['map_order'];
        $data['results'] = $this->order_services_model->get_for_page($limit, $pos, $cond, $order);
//pre($data['results']);
        $total = $this->order_services_model->get_total_rows($cond);
        setcookie("order_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_progress", $progress, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_status", $status, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("has_get_money", $has_get_money, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        echo $cond;
        $this->load->view('order/process_list', $data);
        return false;
    }

    function process_view($id) {
        $this->data['pre'] = 'order_process';

        $id = intval($id);

        if (isset($_POST['submit'])) {

            $this->form_validation->set_rules('full_name', 'full_name', 'required');
            $this->form_validation->set_rules('phone', 'phone', 'required');
            $this->form_validation->set_rules('services', 'services', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $add = @$_POST;


                $add['date_create'] = date('Y-m-d H:i:s');
                $add['last_update'] = date('Y-m-d H:i:s');
                $add['appointment'] = date('Y-m-d', strtotime($_POST['appointment']));
                $add['network_id'] = ',' . $this->data['user']['network_id'] . ',';
                unset($add['submit']);

                $this->data['check_error'] = 0;

                $new_customer = $this->order_services_model->update($add, $id);

                if ($this->data['root_network']['username']) {
                    $url = 'admincp/customer/edit/' . $new_customer;
                    $msg_send = $this->data['user']['username'] . ' vừa cập nhật hồ sơ khách hàng ' . $this->input->post('name');
                    $this->addnotify($msg_send, $url, $this->data['root_network']['username']);
                }
            }
        }

        $this->data['order'] = $order = $this->order_services_model->get_by_key($id);


        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['staff'] = $this->admin_model->get_by($cond);

        $this->data['log'] = $this->order_services_model->getlog($id);
        $this->load->model('country_model');
        $this->data['country'] = $this->country_model->get_all();
        $this->data['order_sku'] = $this->order_sku_model->get_by('order_id = ' . $id);

        $this->data['load_file'] = $this->load_block_file('order_services_model', $id);
        $this->data['load_chat'] = $this->load_block_chat('order_services_model', $id);

        $this->data['web_title'] = 'Hồ sơ khách hàng ' . $this->data['order']['full_name'];
        $this->template->write_view('content_block', 'order/process_view', $this->data);
        $this->template->render();
    }

    function check($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
        if (empty($order) || !$this->check_role_order($id, 'check') || $order['status'] != 'handle')
            redirect(base_url('admincp/order'));
        if (empty($order['who_check'])) {
            $this->order_services_model->update(array('who_check' => $this->data['user']['username'], 'who_handle' => $this->data['user']['username'], 'progress' => 'process'), $id);
            $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
        } else {
            if ($order['who_check'] != $this->data['username'] || $order['who_handle'] != $this->data['username'])
                redirect(base_url('admincp/order/view/' . $id));
        }
        if (isset($_POST['submit'])) {
            if ($order['progress'] != 'transfer' || $_POST['submit'] == 1) {
                $this->form_validation->set_rules('contact_fullname', 'contact fullname', 'required');
                $this->form_validation->set_rules('contact_email', 'contact email', 'required');
                $this->form_validation->set_rules('group_size', 'Number Of Persons ', 'required');
                $this->form_validation->set_rules('visit_purpose', 'Purpose Of Visit', 'required');
                $this->form_validation->set_rules('visa_type', 'Type Of Visa', 'required');
                $this->form_validation->set_rules('arrival_port', 'Arrival Airport', 'required');
                $this->form_validation->set_rules('arrival_date', 'Arrival Date', 'required');
                if ($this->form_validation->run() != FALSE) {

                    if ($_POST['group_size'] < 3 || $_POST['group_size'] == 4) {
                        $pax = $_POST['group_size'] . '_pax';
                    } else if ($_POST['group_size'] == 3 || $_POST['group_size'] == 5) {
                        $pax = ($_POST['group_size'] - 1) . '_pax';
                    } else if (5 < $_POST['group_size'] && $_POST['group_size'] < 10) {
                        $pax = '6_pax';
                    } else {
                        $pax = '10_pax';
                    }
                    $services_fees = $this->data['service_fees']['visa_fees'][$_POST['visit_purpose']][$_POST['visa_type']][$pax] * $_POST['group_size'];
                    $processing_time_fees = $this->data['service_fees']['Processing time'][$_POST['processing_time']]['fee'] * $_POST['group_size'];
                    $total_fees = $services_fees + $processing_time_fees + $order['nationality_fees'];
                    $update = array(
                        'full_name' => $_POST['contact_fullname'],
                        'email' => $_POST['contact_email'],
                        'phone' => $_POST['contact_phone'],
                        'address' => $_POST['contact_address'],
                        'leave_message' => $_POST['leave_message'],
                        'pax' => $_POST['group_size'],
                        'purpose_of_visit' => $_POST['visit_purpose'],
                        'type_of_visa' => $_POST['visa_type'],
                        'arrival_airport' => $_POST['arrival_port'],
                        'arrival_date' => date('Y-m-d H:i:s', strtotime($_POST['arrival_date'])),
                        'processing_time' => $_POST['processing_time'],
                        'total_fees' => $total_fees,
                        'services_fees' => $services_fees,
                        'processing_time_fees' => $processing_time_fees,
                        'has_get_money' => $_POST['has_get_money'],
                    );
                    if (isset($_POST['money_paid']))
                        $update['money_paid'] = $_POST['money_paid'];
                    $this->order_services_model->update($update, $id);
                    $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
                    $msg_error = 'Bạn vừa lưu đơn hàng #' . $id . ' thành công.';
                } else {
                    $msg_error = validation_errors();
                }
            }

// chuyển
            if ($_POST['submit'] == 2) {
                if (empty($order['who_censor'])) {
                    if ($order['progress'] == 'transfer') {
                        $network_id = explode(',', $order['network_id']);
                        $update = array(
                            'network_id' => ',' . $network_id[1] . ',',
                            'progress' => 2,
                        );
                        $msg_error = 'Bạn đã thu hồi lại đơn hàng #' . $id . ' thành công.';
                    } else {
                        $this->load->model('network_model');
                        $network = $this->network_model->get_by('type = "issue visa"');
                        $i = array_rand($network);
                        $network_id = $order['network_id'] . $network[$i]['id'] . ',';
                        $update = array(
                            'network_id' => $network_id,
                            'progress' => 3,
                        );
                        $msg_error = 'Bạn chuyển đơn hàng #' . $id . ' thành công, trong thời gian chưa ai nhận xử lý tiếp, thì bạn vẫn có thể chỉnh sữa hoặc thu hồi đơn hàng.';
                    }
                    $this->order_services_model->update($update, $id);
                    $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
//pre($order);
                } else {
                    $this->order_services_model->update(array('who_handle' => $order['who_censor']), $id);
                    $msg_error = 'Bạn vừa chuyển lại đơn hàng cho ' . $order['who_censor'] . ' để xử lý tiếp.';
                }
            }
            if ($_POST['submit'] == 3) {
                $this->order_services_model->update(array('status' => 'final', 'progress' => 'complete'), $id);
                $msg_error = 'Bạn hoàn thành đơn hàng #' . $id . ', cám ơn bạn rất nhiều.';
                $this->session->set_flashdata('msg_error', $msg_error);
                redirect(base_url('admincp/order/view/' . $id));
            }
            $this->session->set_flashdata('msg_error', $msg_error);
        }

//pre($this->data['log']);
        $this->data['msg_error'] = $this->session->flashdata('msg_error');
        $this->load->model('country_model');
        $this->data['country'] = $this->country_model->get_all();
        $this->data['order_sku'] = $this->order_sku_model->get_by('order_id = ' . $id);
        $this->data['load_file'] = $this->load_block_file('order_services_model', $id);
        $this->data['load_chat'] = $this->load_block_chat('order_services_model', $id);
        $this->template->write_view('content_block', 'order/check', $this->data);
        $this->template->render();
    }

    function censor($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
        if (empty($order) || !$this->check_role_order($id, 'censor') || $order['status'] != 'handle')
            redirect(base_url('admincp/order'));
        if (empty($order['who_censor'])) {
            $this->order_services_model->update(array('who_censor' => $this->data['user']['username'], 'who_handle' => $this->data['user']['username'], 'progress' => 'receive'), $id);
            $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
        } else {
            if ($order['who_censor'] != $this->data['username'] || $order['who_handle'] != $this->data['username'])
                redirect(base_url('admincp/order/view/' . $id));
        }
        if (isset($_POST['submit'])) {
            if ($_POST['submit'] == 1) {
                $this->order_services_model->update(array('status' => 'deny'), $id);
                $msg_error = 'Bạn vừa từ chối đơn hàng #' . $id . ' .Vui lòng chọn đơn khác tiếp tục xử lý.';
                $this->session->set_flashdata('msg_error', $msg_error);
                redirect(base_url('admincp/order'));
            }
            if ($_POST['submit'] == 2) {
                $this->order_services_model->update(array('who_handle' => $order['who_check']), $id);
                $msg_error = 'Bạn vừa chuyển đơn hàng #' . $id . ' cho ' . $order['who_check'] . ' bổ sung đơn hàng.';
                $msg_send = $order['who_check'] . ' hãy bổ sung thêm thông tin cho đơn hàng #' . $id;
                $url = 'admincp/order/check/' . $id;
                $this->addnotify($msg_send, $url, $order['who_check'], $order['who_censor']);
                $this->session->set_flashdata('msg_error', $msg_error);
                redirect(base_url('admincp/order/view/' . $id));
            }
            if ($_POST['submit'] == 3) {
                $this->order_services_model->update(array('status' => 'final', 'progress' => 'complete'), $id);
                $msg_error = 'Bạn hoàn thành đơn hàng #' . $id . ', cám ơn bạn rất nhiều.';
                $this->session->set_flashdata('msg_error', $msg_error);
            }
        }
        $this->data['msg_error'] = $this->session->flashdata('msg_error');
        $this->load->model('country_model');
        $this->data['country'] = $this->country_model->get_all();
        $this->data['order_sku'] = $this->order_sku_model->get_by('order_id = ' . $id);
        $this->data['load_file'] = $this->load_block_file('order_services_model', $id);
        $this->data['load_chat'] = $this->load_block_chat('order_services_model', $id);
        $this->template->write_view('content_block', 'order/censor', $this->data);
        $this->template->render();
    }

    function view($id) {
        $id = intval($id);
        $this->data['order'] = $order = $this->order_services_model->get_by_key($id);
        if (empty($order) || !$this->check_role_order($id, 'view') || ($order['status'] == 'trash' && empty($this->data['user']['role_network'])))
            redirect(base_url('admincp/order'));
        if ($order['status'] == 'cancel' && !$this->check_role_order($id, 'trash'))
            redirect(base_url('admincp/order'));
        $this->data['log'] = $this->order_services_model->getlog($id);
        $this->load->model('country_model');
        $this->data['country'] = $this->country_model->get_all();
        $this->data['order_sku'] = $this->order_sku_model->get_by('order_id = ' . $id);

        $this->data['load_chat'] = $this->load_block_chat('order_services_model', $id);
        $this->template->write_view('content_block', 'order/view', $this->data);
        $this->template->render();
    }

    function index() {

        $this->data['order_pos'] = intval(@$_COOKIE['order_pos']);
        $this->data['order_status'] = (@$_COOKIE['order_status']);
        $this->data['order_progress'] = (@$_COOKIE['order_progress']);
        $this->data['has_get_money'] = (@$_COOKIE['has_get_money']);

        $this->template->write_view('content_block', 'order/index', $this->data);
        $this->template->render();
    }

    function add() {
        $this->data['pre'] = 'order_add';
        $this->data['web_title'] = 'Thêm hồ sơ đang xử lý';
        $this->data['check_error'] = -1;

        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['staff'] = $this->admin_model->get_by($cond);

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('full_name', 'full_name', 'required');
            $this->form_validation->set_rules('phone', 'phone', 'required');
            $this->form_validation->set_rules('services', 'services', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $add = @$_POST;

                $add['date_create'] = date('Y-m-d H:i:s');
                $add['last_update'] = date('Y-m-d H:i:s');
                $add['appointment'] = date('Y-m-d', strtotime($_POST['appointment']));
                $add['network_id'] = ',' . $this->data['user']['network_id'] . ',';
                unset($add['submit']);

                $this->data['check_error'] = 0;
                $new_customer = $this->order_services_model->insert($add);

                if ($this->data['root_network']['username']) {
                    $url = 'admincp/customer/edit/' . $new_customer;
                    $msg_send = $this->data['user']['username'] . ' vừa thêm hóa đơn dịch vụ cho khách hàng ' . $this->input->post('name');
                    $this->addnotify($msg_send, $url, $this->data['root_network']['username']);
                }
            }
        }

       
        $this->template->write_view('content_block', 'order/add', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $progress = $_POST['progress'];
        $status = $_POST['status'];
        $has_get_money = intval($_POST['has_get_money']);
        $cond = 'status = "' . $status . '"';
        $data['user'] = $this->data['user'];
        if ($progress) {
            $cond = 'progress = ' . $progress;
        }
        if ($has_get_money != -1) {
            $cond .= ' and has_get_money = ' . $has_get_money;
        }
        $keyword = @$_POST['keyword'];
        if ($keyword) {
            $cond = "( id = '$keyword' or email like '%$keyword%')";
        }
        if (empty($this->data['user']['role_network'])) {
            $cond .= ' and network_id like "%,' . $this->data['user']['network_id'] . ',%"';
        }
        // echo $cond.'<br>'; exit;
        $limit = 20;
        $order = 'has_get_money DESC, processing_time_lv DESC, progress ASC';
        $data['map_order'] = $this->data['map_order'];
        $data['results'] = $this->order_services_model->get_for_page($limit, $pos, $cond, $order);
//pre($data['results']);
        $total = $this->order_services_model->get_total_rows($cond);
        setcookie("order_pos", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_progress", $progress, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("order_status", $status, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("has_get_money", $has_get_money, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        //echo $cond;
        $this->load->view('order/list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('order_services_model', 'category_order_services_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond = "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->order_services_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function update_hot() {
        if (isset($_POST['data'])) {
            $this->order_services_model->update_batch($_POST['data']);
        }
    }

//0=>'Đang xử lý', 1=>'Từ chối một phần', 2=>'Từ chối tất cả', 3=>'Hoàn tất', 4=>'Hủy', 5=>'Thùng rác'
    function trash() {
        $id = intval(@$_POST['id']);
        $this->check_role_order($id, 'trash');
        if (!$this->check_role_order($id, 'trash'))
            return false;
        $this->order_services_model->update(array('status' => 'trash'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa');
        echo 1;
    }

    function trash_empty() {
        $id = intval(@$_POST['id']);
        if (!$this->check_role_order($id, 'empty'))
            return false;
        $this->order_services_model->delete($id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị bạn xóa hoàn toàn');
        echo 1;
    }

    function cancel() {
        $id = intval(@$_POST['id']);
        if (!$this->check_role_order($id, 'check'))
            return false;
        $this->order_services_model->update(array('status' => 'cancel'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa bị hủy');
        echo 1;
    }

    function uncancel() {
        $id = intval(@$_POST['id']);
        if (!$this->check_role_order($id, 'check'))
            return false;
        $this->order_services_model->update(array('status' => 'handle'), $id);
        $this->session->set_flashdata('msg_error', 'Đơn hàng #' . $id . ' vừa được bạn phục hồi');
        echo 1;
    }

    private function check_role_order($id, $role_order_progress) {
        $order = $this->order_services_model->get_by_key($id);
        if (empty($order))
            return false;
        if (!empty($this->data['user']['role_network']) || $role_order_progress == 'view')
            return true;
        if (strpos(',' . $order['network_id'] . ',', ',' . $this->data['user']['network_id'] . ',') === false)
            return false;
        if (strpos(',' . $this->data['user']['role_order_progress'] . ',', ',' . $role_order_progress . ',') === false)
            return false;
        return true;
    }

    function update_passport() {
        $data = @$_POST['data'];
        $result = array();
        if (!$this->check_role_order($data['order_id'], 'check'))
            return false;
        if ($data['id'] == '' || empty($data['id'])) {
            unset($data['id']);
            $id = $this->order_sku_model->insert($data);
        } else {
            $order_sku = $this->order_sku_model->get_by_key($data['id']);
            if (empty($order_sku))
                return false;
            if (!empty($data['nationality'])) {
                $this->load->model('country_model');
                $country_old = $this->country_model->get_row_by(array('name' => $order_sku['nationality']));
                $country_new = $this->country_model->get_row_by(array('name' => $data['nationality']));
                $nationality_fees = $country_new['special_fee'] - $country_old['special_fee'];
                if ($nationality_fees != 0) {
                    $order = $this->order_services_model->get_by_key($data['order_id']);
                    $result['nationality_fees'] = $order['nationality_fees'] + $nationality_fees;
                    $result['total_fees'] = $order['total_fees'] + $nationality_fees;
                    $this->order_services_model->update($result, $data['order_id']);
                    $data['special_fee'] = $country_new['special_fee'];
                }
            }
            $this->order_sku_model->update($data, $data['id']);
        }
        echo json_encode($result);
    }

    public function block_cost() {
        $order = 'id DESC';
        $cond = 'network_id = ' . $this->data['user']['network_id'];
        $this->data['xx'] = $this->order_services_model->get_total_rows();
        $this->data['x'] = $this->order_services_model->get_for_page(10, 0, $cond, $order);

        return $this->load->view('block/cost', $this->data, true);
    }

}
