<?php

/**
 * @property Admin_Model $admin_model
 */
class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
    }

    function login() {
        $this->data['error'] = -1;

        if (isset($_POST['submit'])) {
            $login = array(
                'username' => $this->input->post('username'),
                'password' => md5($this->input->post('password'))
            );
            $user = $this->admin_model->get_row_by($login);
            if (empty($user)) {
                $this->data['error'] = 1;
            } else {
                //$this->session->set_userdata('username', $user['username']);
                if ($user['status'] == 'career_break') { // Nhân viên đã nghỉ việc ko login được
                    $this->data['error'] = 2;
                } else {
                    //$this->session->set_userdata('username', $user['username']);
                    setcookie("username", $user['username'], (time() + (86400 * 30)), '/', $_SERVER['SERVER_NAME']);
                   
                    redirect(base_url() . ADMIN_URL);
                }
            }
        }
        $this->load->view('login', $this->data);
    }

    function logout() {
        $this->load->helper('cookie');
        setcookie("username", "", -1, '/', $_SERVER['SERVER_NAME']);
        redirect(ADMIN_URL . 'auth/login');
    }

}
