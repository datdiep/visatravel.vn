<?php

/**
 * @property Service_detail_model $service_detail_model
 */
class Service_detail extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('service_detail_model', 'service_price_model', 'category_service_model', 'service_question_model', 'admin_model'));
        $this->load->library(array('form_validation', 'upload', 'alias', 'resize_image', 'my_pagination'));
        $this->data['pre'] = 'service_detail';
        //  $this->data['list_price'] = $this->service_price_model->get_all();
        $this->data['list_country'] = $this->category_service_model->get_by('id_parent <> 0');
        $this->data['service_question'] = $this->service_question_model->get_all();
    }

    public function add() {
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('description', 'Description', 'required');
            if ($this->form_validation->run() != FALSE) {
                $alias = $this->alias->create_alias($this->input->post('title'));
                $service_question = $this->input->post('checkbox');
                $image = '';
                $data = array(
                    'title' => $this->input->post('title'),
                    'alias' => $alias,
                    'description' => $this->input->post('description'),
                    'content' => $this->input->post('content'),
                    'date_create' => date('Y-m-d H:i:s'),
                    'service_id' => $this->input->post('service_id'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'status' =>  $this->data['user']['publish'] != 1 && $_POST['submit'] != 3 ? 2 : $_POST['submit'], // -1 : tai xuat ban , 1 : xuat ban ,2 : doi duyet, 3: nhap
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags')
                );
                if ($this->input->post('schedule')) {
                    $data['is_hide'] = 1; // hidden
                    $data['schedule'] = date('Y-m-d', strtotime(str_replace("/", "-", $this->input->post('schedule')))); // Chuyển từ mm/dd/yyyy sang yyyy/mm/dd
                }
                @mkdir(PATH_UPLOAD . '/service_detail', 0777);
                $path = PATH_UPLOAD . 'service_detail/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    $img_name = $alias . '-' . rand(1, 9999) . '.jpg';
                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $data['image'] = $img_name;
                }

                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'service_detail/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => 'fb_' . $img_name
                    ));

                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $id = $this->service_detail_model->insert($data);
                $service_question = $this->input->post('checkbox');
                $all_question = $this->service_question_model->get_by('id <> 0', null, 'id');
                if (!empty($service_question)) {
                    foreach ($service_question as $id_service) {
                        $this->service_question_model->update(array('cat_ids' => $all_question[$id_service]['cat_ids'] . $id . ','), $id_service);
                    }
                }

                $this->data['check_error'] = 0;
            } else {
                $this->data['check_error'] = 1;
            }
        }
        $this->template->write_view('content_block', 'service_detail/add', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = @intval($id);
        $this->data['check_error'] = -1;
        $this->data['detail'] = $this->service_detail_model->get_by_key($id);
        $user_write = $this->admin_model->get_row_by("username = '" . $this->data['detail']['author_username'] . "'");
        $this->data['list_question'] = $this->service_question_model->get_by("cat_ids like " . "'%,$id,%'", "", "id", 'id');
        // showbutton : -1 => hide button, 1:tai xua ban/ tai xuat ban va doi leader tham dinh
        $this->data['show_button'] = 0;
        if ($this->data['detail']['status'] == -1) {
            $tmp_name = $id . '-' . $this->data['detail']['alias'];
            $tmp_detail = @file_get_contents(APPPATH . 'cache/service_update/' . $tmp_name);
            if ($this->data['user']['username'] == $user_write['username']) {
                $this->data['detail'] = unserialize($tmp_detail);
                // show text : tai xuat ban va doi leader tham dinh
                $this->data['show_button'] = 1;
            } elseif ($this->data['user']['id'] == $user_write['leader'] || $this->data['user']['manager_editor'] == 1) {
                // show text : tai xuat ban bai viet
                $this->data['show_button'] = 1;
                $this->data['tmp_detail'] = unserialize($tmp_detail);
            }
        }
        // show button tai xuat ban khi sau 3 ngay 
        if ($this->data['user']['username'] == $user_write['username'] && $this->getdays($this->data['detail']['date_create'], date("Y-m-d")) > 3)
            $this->data['show_button'] = 1;
        //hide : button bai viet ko phai cua leader va sau 3 ngay va khong phai bai viet tai xuat ban hide button
        if ($this->data['user']['username'] != $user_write['username'] && $this->getdays($this->data['detail']['date_create'], date("Y-m-d")) > 3 && $this->data['detail']['status'] != -1)
            $this->data['show_button'] = -1;

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $this->data['detail'] = $this->service_detail_model->get_by_key($id);
                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->input->post('alias'),
                    'description' => $this->input->post('description'),
                    'content' => $this->input->post('content'),
                    // 'date_create' => date('Y-m-d H:i:s'),
                    'service_id' => $this->input->post('service_id'),
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'status' => $_POST['submit'], // -1 : tmp_xuat ban , 1 : xuat ban ,2 : doi duyet, 3: nhap
                    'author_id' => $this->data['user']['id'],
                    // 'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags')
                );

                if ($this->input->post('schedule')) {
                    $update['is_hide'] = 1; // hidden
                    $update['schedule'] = date('Y-m-d', strtotime(str_replace("/", "-", $this->input->post('schedule')))); // Chuyển từ mm/dd/yyyy sang yyyy/mm/dd
                }
                @mkdir(PATH_UPLOAD . '/service_detail', 0777);
                $path = PATH_UPLOAD . 'service_detail/';
                $img_hide = $this->input->post('photo');
                if ($img_hide != '') {
                    if ($this->data['detail']['image']) {
                        @unlink($path . $this->data['detail']['image']);
                        $img_name = $this->data['detail']['image'];
                    } else {
                        $img_name = $alias . '-' . rand(1, 9999) . '.jpg';
                    }

                    $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                    $this->resize_image->base64_png($repla, $path . '/' . $img_name);
                    $update['image'] = $img_name;
                }
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'service_detail/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => true,
                        'file_name' => 'fb_' . $img_name
                    ));

                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                        $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                        $image = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }
                // status > 1 =>  status = 2: doi duyet, 3 ban nhap
                //check bai viet truoc 3 ngay full controll
                if ($this->getdays($this->data['detail']['date_create'], date("Y-m-d")) < 3 || $this->data['detail']['status'] > 1) {
                    $this->service_detail_model->update($update, $id);
                    // pre($this->service_detail_model->get_by_key($id));
                    $this->data['msg'] = 'Cập nhập thành công';
                    $this->data['check_error'] = 0;
                    //bai viet sau 3 ngayf neu la dang xuat ban thi duoc luu vo ban tam.
                } elseif ($this->data['detail']['status'] <= 1 && $this->data['user']['username'] == $user_write['username']) {
                    $tmp_name = $id . '-' . $this->data['detail']['alias'];
                    @mkdir(APPPATH . '/cache/service_update', 0777);
                    @unlink(APPPATH . 'cache/service_update/' . $tmp_name);
                    file_put_contents(APPPATH . 'cache/service_update/' . $tmp_name, serialize($update));
                    $this->data['msg'] = 'Tin Của bạn đã được lưu và đợi leader duyệt lại';
                    $this->data['check_error'] = 0;
                    $this->service_detail_model->update(array('status' => -1), $id);
                }
                // bai viet la tai xuat ban , va hon 3 ngay , va phai la leader moi xuat ban, hoac neu la editor_manager duyet dc moi bai viet
                elseif (($_POST['submit'] == -1 && $this->getdays($this->data['detail']['date_create'], date("Y-m-d")) > 3 && $this->data['user']['id'] == $user_write['leader']) || $this->data['user']['manager_editor'] == 1) {
                    $this->data['msg'] = 'Tái xuất bản thành công';
                    $this->data['check_error'] = 0;
                    $update['status'] = 1;
                    $this->service_detail_model->update($update, $id);
                    $tmp_name = $id . '-' . $this->data['detail']['alias'];
                    $this->data['detail']['status'] = 1;
                    @unlink(APPPATH . 'cache/service_update/' . $tmp_name);
                    // end quyen tai xuat ban => hide button 
                    $this->data['show_button'] = -1;
                }

                // update check box
                if (!empty($this->input->post('checkbox'))) {
                    $id_question_new = "," . implode(",", $this->input->post('checkbox')) . ",";
                    $update_question = $this->update_check($id, $id_question_new);
                    if (!empty($update_question))
                        $this->service_question_model->update_batch($update_question);
                }

                /// get again data show form edit
                $tmp_name = $id . '-' . $this->data['detail']['alias'];
                $tmp_detail = @file_get_contents(APPPATH . 'cache/service_update/' . $tmp_name);
                if ($tmp_detail) {
                    $this->data['detail'] = unserialize($tmp_detail);
                } else {
                    $this->data['detail'] = $this->service_detail_model->get_by_key($id);
                }
                $this->data['list_question'] = $this->service_question_model->get_by("cat_ids like " . "'%,$id,%'", "", "id", 'id');
            }
        }
        if (($this->data['user']['id'] == $user_write['leader'] || $this->data['user']['manager_editor'] == 1) && $this->data['detail']['status'] == -1)
            $this->template->write_view('content_block', 'service_detail/leader_edit', $this->data);
        else
            $this->template->write_view('content_block', 'service_detail/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $detail_service = $this->service_detail_model->get_by_key($id);
        if ($this->data['user']['root'])
            $this->service_detail_model->delete($id);
        elseif ($detail_service['status'] <= 1 && $this->getdays($detail_service['date_create'], date("Y-m-d")) > 3)
            echo "Liên Hệ Leader để xóa";
        else {
            $this->service_detail_model->delete($id);
        }
    }

    public function index() {
        $this->data['pos'] = 0;
        $this->template->write_view('content_block', 'service_detail/index', $this->data);
        $this->template->render();
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        // get list user of manager_user
        $writer = $this->admin_model->get_by("leader = '" . $this->data['user']['id'] . "'");
        if (!empty($writer)) {
            $str_username = "('" . $this->data['user']['username'] . "',";
            foreach ($writer as $value) {
                $str_username .= "'" . $value['username'] . "',";
            }
            $str_username = substr($str_username, 0, -1) . ")";
            $cond = "author_username in " . $str_username;
        } else {
            $cond = "author_username ='" . $this->data['user']['username'] . "'";
        }
        if ($this->data['user']['manager_editor'])
            $cond = " id > 0";
        $service_id = @intval($_POST['service_id']);
        if ($service_id) {
            $cond .= ' and service_id = ' . $service_id;
        }
        $status = @intval($_POST['status']);
        if ($cat_id) {
            $cond .= ' and status = ' . $status;
        }
        $keyword = @$_POST['keyword'];
        if ($keyword) {
            $cond .= " and ( id = '$keyword' or title like '%$keyword%')";
        }
        if ($status != "") {
            $cond .= ' and status = ' . $status;
        }
        $data['list_country'] = $this->category_service_model->get_by('id_parent <> 0', null, "id");
        $limit = 15;
        $order = 'id DESC';
        $data['results'] = $this->service_detail_model->get_for_page($limit, $pos, $cond, $order);
        // set show/hide action
        foreach ($data['results'] as $key => $value) {
            $data['results'][$key]['range_date'] = $this->getdays($value['date_create'], date("Y-m-d"));
            if ($value['status'] == -1) {
                $tmp_name = $value['id'] . '-' . $value['alias'];
                $tmp_detail = @file_get_contents(APPPATH . 'cache/service_update/' . $tmp_name);
                // just get data - show  of tmp
                $data['results'][$key]['title'] = unserialize($tmp_detail)['title'];
                $data['results'][$key]['icon'] = "<i class='fa fa-copy'></i> Tái xuất bản";
                $data['results'][$key]['id'] = $value['id'];
                $data['results'][$key]['author_username'] = $value['author_username'];
                // show edit tai xuat ban cho manager_editor va nguoi giam sat
                $data['results'][$key]['edit'] = 1;
            }

            //show icon -1 tai xuat ban, 1 xuat ban, 2 doi duyet, 3 ban nhap
            else if ($value['status'] == 2)
                $data['results'][$key]['icon'] = "<i class='fa fa-hourglass-2'></i> Đợi duyệt";
            else if ($value['status'] == 3)
                $data['results'][$key]['icon'] = "<i class='fa fa-sticky-note'></i> Bản nháp";
            else
                $value['status'] < 1 || $data['results'][$key]['range_date'] > 3 ? $data['results'][$key]['icon'] = "<i class='fa fa-check'> Đã xuất bản</i>" : $data['results'][$key]['icon'] = "<i class='fa fa-check'> Đã xuất bản</i>";
            //show function del//1 : show xoa, nguoc lai ( del bai viet cua chinh tac gia,leader, manager_editor va truoc 3 ngay)
            $data['results'][$key]['del'] = $data['results'][$key]['range_date'] < 3 ? 1 : 0;
            //show function edit//1 : show edit, nguoc lai ( edit bai viet cua chinh tac gia, va chi co user quan ly tai xuat ban)
            if ($data['results'][$key]['range_date'] < 3 || $this->data['user']['username'] == $value['author_username'])
                $data['results'][$key]['edit'] = 1;
            if ($this->data['user']['root']) {
                $data['results'][$key]['del'] = 1;
                $data['results'][$key]['edit'] = 1;
            }
            //khi khong dc sua va xoa thi chi dc xem
            !$data['results'][$key]['del'] && !$data['results'][$key]['edit'] ? $data['results'][$key]['view_web'] = 1 : $data['results'][$key]['view_web'] = 0;
            // add category for link view
            $data['results'][$key]['cate_alias'] = $data['list_country'][$value['service_id']]['alias'];
        }
        $total = $this->service_detail_model->get_total_rows($cond);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('service_detail/list', $data);
    }

    function search() {
        if (isset($_GET['q'])) {
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond .= "title like '%$keyword%' or id = " . intval($keyword);
            $detail = $this->service_detail_model->get_for_page(10, 0, $cond);
            echo json_encode($detail); //format the array into json data            
        }
    }

    function approved() {
        $id = intval(@$_POST['id']);
        $detail = $this->service_detail_model->get_by_key($id);
        $user_write = $this->admin_model->get_row_by("username = '" . $detail['author_username'] . "'");
        if ($user_write['leader'] == $this->data['user']['id'] && $_POST['status'] == 'approved') {
            if ($this->service_detail_model->update(array('status' => 1), $id))
                echo "approved_ok";
            else {
                echo "";
            }
        } else {
            echo "";
        }
    }

    private function update_check($id, $id_question_new) {
        $question = $this->service_question_model->get_all();
        foreach ($question as $k => $item) {
            if (strpos($item['cat_ids'], ",$id,") !== FALSE && strpos($id_question_new, "," . $item['id'] . ",") === FALSE) {
                $data[$k]['cat_ids'] = str_replace("$id,", "", $item['cat_ids']);
                $data[$k]['id'] = $item['id'];
            }
            if (strpos($item['cat_ids'], ",$id,") === FALSE && strpos($id_question_new, "," . $item['id'] . ",") !== FALSE) {
                $data[$k]['cat_ids'] = $item['cat_ids'] . $id . ",";
                $data[$k]['id'] = $item['id'];
            }
        }
        //pre($data);
        return $data;
    }

}
