<?php

/**
 * @property Page_Model $task_model
 */
class Task extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('form_validation', 'resize_image', 'alias'));
        $this->load->model(array('task_model', 'admin_model', 'network_model', 'department_model', 'planing_model'));
        $this->data['type'] = $this->session->userdata('type_page');
        $this->data['pre'] = 'task';
    }

    function price() {
        $this->data['pre'] = 'task_price';

        if ($this->data['user']['network_id'] == 7) //chudu
            $this->template->write_view('content_block', 'task/price_chudu', $this->data);
        else {
            $this->template->write_view('content_block', 'task/price_achau', $this->data);
        }


        $this->template->render();
    }

    function department($network_id = null, $department_id = 0) {
        $member_username = @$_GET['u'];
        if (!empty($member_username)) {
            $this->data['member'] = $this->admin_model->get_row_by(array('username' => $member_username));
            if (!empty($this->data['member'])) {
                $network_id = $this->data['member']['network_id'];
                $department_id = $this->data['member']['department_id'];
            }
        }
        if (empty($network_id) || ($network_id != $this->data['user']['network_id'] && empty($this->data['user']['role_network'])))
            redirect(base_url('admincp/task/department/' . $this->data['user']['network_id']));
        if ($department_id != $this->data['user']['department_id'] && empty($this->data['user']['role_department']) && empty($this->data['user']['role_network']))
            redirect(base_url('admincp/task/department/' . $this->data['user']['network_id'] . '/' . $this->data['user']['department_id']));
        $this->data['network'] = $this->network_model->get_by_key($network_id);
        if (empty($this->data['network']))
            redirect(base_url('admincp/task/department/' . $this->data['user']['network_id']));
        if (!empty($this->data['user']['role_network'])) {
            $this->data['departments'] = $this->department_model->get_by('network_id =' . $network_id);
            $this->data['networks'] = $this->network_model->get_all();
        }
        if (!empty($this->data['user']['role_department']))
            $this->data['departments'] = $this->department_model->get_by('network_id =' . $network_id);
        if ($department_id == 0) {
            $this->data['list_member'] = $this->admin_model->get_join('ad.network_id = ' . $network_id, 'id');
        } else {
            $this->data['list_member'] = $this->admin_model->get_join('ad.department_id = ' . $department_id, 'id');
            $this->data['department'] = $this->department_model->get_by_key($department_id);
            if (empty($this->data['department']))
                redirect(base_url('admincp/task/department/' . $this->data['user']['network_id'] . '/' . $this->data['user']['department_id']));
            $this->data['load_chat'] = $this->load_block_chat('department_model', $department_id);
            $this->data['planings'] = $this->planing_model->get_for_page(10, 0, 'department_id =' . $department_id, 'status ASC');
        }

        if (!empty($this->data['list_member'])) {
            $list_member_id = '';
            if (!empty($this->data['member'])) {
                $list_member_id .= $this->data['member']['id'] . ',';
                $str_key = 'M,Y';
            } else {
                foreach ($this->data['list_member'] as $v) {
                    //$this->data['list_department'][$v['department_id']][] = $v;
                    $list_member_id .= $v['id'] . ',';
                }
                $str_key = 'd M,Y';
            }
            $temp = '(' . trim($list_member_id, ',') . ')';
            $cond = 'user_handle_id   in' . $temp;
            $this->data['date'] = $this->session->userdata('date_install');
            if (empty($this->data['date']))
                $this->data['date'] = date('F d,Y', strtotime('-7 days')) . ' - ' . date('F d,Y');
            $date_install = explode(' - ', $this->data['date']);
            $from = date('Y-m-d', strtotime($date_install[0]));
            $to = date('Y-m-d', strtotime($date_install[1])) . ' 23.59.59';
            $cond .= ' and date_create >= "' . $from . '" and date_create <= "' . $to . '"';
            //echo $cond; exit;
            $task = $this->task_model->get_by($cond, 'date_create DESC, mark ASC, status DESC');
            foreach ($task as $v) {
                $key = date($str_key, strtotime($v['date_create']));
                $this->data['task'][$key][] = $v;
            }
        }

        $this->data['load_header'] = $this->load->view('task/header', $this->data, true);
        $this->template->write_view('content_block', 'task/index', $this->data);
        $this->template->render();
    }

    function load_date() {
        $date_install = explode(' - ', $_POST['d']);
        if (date('F d, Y', strtotime($date_install[1])) == $date_install[1] && date('F d, Y', strtotime($date_install[0])) == $date_install[0]) {
            $this->session->set_userdata('date_install', $_POST['d']);
            echo 1;
            exit;
        }
        echo 0;
        exit;
    }

    function process($id = 0) {
        $this->data['pre'] = 'task_add';
        $this->data['check_error'] = -1;
        if ($id != 0) {
            $this->data['task'] = $this->task_model->get_by_key($id);
            if (empty($this->data['task']))
                redirect(base_url('admincp/task'));
            $cond = 'ad.id = ' . $this->data['task']['user_handle_id'];
            $this->data['user_handle'] = $this->admin_model->get_join($cond);
            $this->data['map_status'] = array(
                'open' => '<span class="bg-yellow margin-r-5" style="padding:0 6px">Đang chờ</span>',
                'doing' => '<span class="bg-green margin-r-5" style="padding:0 6px">Đang làm</span>',
                'finish' => '<span class="bg-gray margin-r-5" style="padding:0 6px">Hoàn thành</span>'
            );



            $this->data['load_chat'] = $this->load_block_chat('task_model', $id);
        }
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('content', 'Đề xuất công việc', 'required');
            if ($this->form_validation->run() == true) {
                $submit = $_POST['submit'];
                unset($_POST['submit']);
                if ($id == 0) {
                    $_POST['date_create'] = date('Y-m-d H:i:s');
                    $_POST['user_create'] = $this->data['user']['username'];
                    if ($_POST['type'] == 'daily') {
                        $_POST['user_handle'] = $this->data['user']['username'];
                        $_POST['user_handle_id'] = $this->data['user']['id'];
                        $_POST['user_leader'] = $this->data['user']['leader'];
                    } else {
                        $_POST['user_leader'] = $this->data['user']['id'];
                        $_POST['deadline'] = date('Y-m-d H:i:s', strtotime($_POST['deadline']));
                    }
                    $id = $this->task_model->insert($_POST);
                } else {
                    $_POST['status'] = 'doing';
                    if ($submit == 3)
                        $_POST['status'] = 'finish';
                    $this->task_model->update($_POST, $id);
                }
                $this->data['check_error'] = 0;
                if ($submit == 2)
                    redirect(base_url('admincp/task'));
                redirect(base_url('admincp/task/process/' . $id));
            } else {
                $this->data['check_error'] = 1;
            }
        }
        $this->data['department'] = $this->department_model->get_by_key($this->data['user']['department_id']);
        $this->data['leader_user'] = $this->admin_model->get_join('ad.id != ' . $this->data['user']['id'] . ' and ad.leader = ' . $this->data['user']['id']);
        $this->template->write_view('content_block', 'task/add_edit', $this->data);
        $this->template->render();
    }

    function handle_department() {
        if (empty($this->data['user']['role_department']) && empty($this->data['user']['edit_department']))
            exit;
        $id = @$_POST['id'];
        $data = @$_POST['data'];
        $network_id = @$_POST['network_id'];
        $submit = @$_POST['submit'];
        if (!empty($data)) {
            $img_hide = $data['img'];
            unset($data['img']);
            $data['network_id'] = $network_id;
            if (empty($this->data['user']['role_network']))
                $data['network_id'] = $this->data['user']['network_id'];
            if (!empty($id)) {
                $department = $this->department_model->get_by_key($id);
                if (!empty($department) && ($department['network_id'] == $this->data['user']['network_id'] || !empty($this->data['user']['role_network']))) {
                    $this->department_model->update($data, $id);
                    $data['msg'] = 'Cập nhật thành công';
                }
            } else {
                $id = $this->department_model->insert($data);
                $data['msg'] = 'Thêm mới thành công';
            }
            $data['change_department'] = 1;
        }
        if (!empty($id)) {
            $data['department'] = $this->department_model->get_by_key($id);
            if (!empty($img_hide)) {
                if (empty($data['department']['folder'])) {
                    $folder = date('Y') . '/' . date('m') . '/department' . $id;
                    $path = PATH_UPLOAD . 'data/' . $folder;
                    @mkdir(PATH_UPLOAD . 'data/' . date('Y'), 0777);
                    @mkdir(PATH_UPLOAD . 'data/' . date('Y') . '/' . date('m'), 0777);
                    @mkdir($path, 0777);
                    $this->department_model->update(array('folder' => $folder), $id);
                    $data['department']['folder'] = $folder;
                } else {
                    $path = PATH_UPLOAD . 'data/' . $data['department']['folder'];
                }
                $repla = str_replace("data:image/jpeg;base64,", "", $img_hide);
                $this->resize_image->base64_png($repla, $path . '/banner.jpg');
                $this->resize_image->crop_resize($path . '/banner.jpg', $path . '/banner.jpg', 640, 300);
            }
        }
        $data['user'] = $this->data['user'];
        $this->load->view('task/department', $data);
        return false;
    }

    function handle_planing() {
        $id = @$_POST['id'];
        $data = @$_POST['data'];
        $submit = @$_POST['submit'];
        $network_id = @$_POST['network_id'];
        $department_id = @$_POST['department_id'];
        if (!empty($data)) {
            if ($submit == 1)
                $data['approve'] = 1;
            if ($this->data['user']['id'] == $this->data['user']['leader'])
                $data['approve'] = 2;
            if (!empty($id)) {
                $planing = $this->planing_model->get_by_key($id);
                if (!empty($planing) && ($planing['user_create'] == $this->data['user']['username'])) {
                    $this->planing_model->update($data, $id);
                    $data['msg'] = 'Cập nhật thành công';
                }
            } else {
                $leader = $this->admin_model->get_by_key($this->data['user']['leader']);
                if (!empty($leader))
                    $data['user_leader'] = $leader['username'];
                $data['user_create'] = $this->data['user']['username'];
                $data['network_id'] = $network_id;
                $data['department_id'] = $department_id;
                if (empty($this->data['user']['role_network']))
                    $data['network_id'] = $this->data['user']['network_id'];
                if (empty($this->data['user']['role_department']))
                    $data['department_id'] = $this->data['user']['department_id'];
                $data['date_create'] = date('Y-m-d H:i:s');
                $data['date_start'] = date('Y-m-d H:i:s', strtotime($data['date_start']));
                $data['date_end'] = date('Y-m-d H:i:s', strtotime($data['date_end']));
                $id = $this->planing_model->insert($data);
                $data['msg'] = 'Thêm mới thành công';
            }
            $data['change_planing'] = 1;
        }
        $data['user'] = $this->data['user'];
        if (!empty($id)) {
            $data['planing'] = $this->planing_model->get_by_key($id);
            if ($submit == 10 && $this->data['user']['id'] == $this->data['user']['leader'])
                $data['planing']['approve'] = 1;
            if ($data['planing']['approve'] == 2 || $data['planing']['user_create'] != $this->data['username']) {
                $data['user_create'] = $this->admin_model->get_row_by(array('username' => $data['planing']['user_create']));
                $this->load->view('task/planing_view', $data);
                return false;
            }
        }
        $this->load->view('task/planing', $data);
        return false;
    }

    function update_planing() {
        $id = @$_POST['id'];
        $appraise = @$_POST['appraise'];
        $submit = @$_POST['submit'];
        $data = array();
        if (empty($id))
            exit;
        $planing = $this->planing_model->get_by_key($id);
        if (!empty($planing)) {
            if ($submit == 1 || $submit == 2) {
                if ($planing['user_create'] == $this->data['user']['username']) {
                    $data['status'] = 'finish';
                    if ($submit == 1)
                        $data['status'] = 'close';
                }
            }else {
                if ($planing['user_leader'] == $this->data['user']['username']) {
                    if ($submit == 3)
                        $data['approve'] = 2;
                    else
                        $data['appraise'] = $appraise;
                }
            }
            $this->planing_model->update($data, $id);
            $data['planing'] = $this->planing_model->get_by_key($id);
            $data['user'] = $this->data['user'];
            $data['user_create'] = $this->admin_model->get_row_by(array('username' => $data['planing']['user_create']));
            $this->load->view('task/planing_view', $data);
            return false;
        }
        exit;
    }

    function mark() {
        $mark = $_POST['mark'];
        $id = $_POST['id'];
        $task = $this->task_model->get_by_key($id);
        // && empty($task['mark'])
        if (!empty($task) && empty($task['mark']) && $task['user_leader'] == $this->data['user']['id']) {
            $this->task_model->update(array('mark' => $mark), $id);
            $user = $this->admin_model->get_by_key($task['user_handle_id']);
            $mark_array = unserialize($user['mark']);
            $key = date('mY');
            if (!empty($mark_array[$key]['quality'])) {
                $mark_array[$key]['quality'] += $mark;
            } else {
                $mark_array[$key]['quality'] = $mark;
            }
            $mark_array = serialize($mark_array);
            $this->admin_model->update(array('mark' => $mark_array), $task['user_handle_id']);
            echo 1;
        } else {
            echo 0;
        }
        exit;
    }

    function load_access() {
        $this->load->model(array('admin_model'));
        $leader_id = $this->data['user']['id'];
        $cond = "leader = '$leader_id'";
        $user = $this->admin_model->get_join($cond);
        echo json_encode($user); //format the array into json data
        exit;
    }

    function page() {
        $pos = intval(@$_POST['pos']);
        $parrams = $this->security->xss_clean($_REQUEST);
        $data['user'] = $this->data['user']['username'];
        $lv = $this->data['user']['level'];
        $cond = '';
        if ($parrams['type']) {
            $cond = 'propose != "" ';
            if ($parrams['type'] != 'All') {
                $cond .= 'and ';
                $cond .= 'type = "' . $parrams['type'] . '"';
            }
            if (@$parrams['member']) {
                if (@$parrams['member'] != 'all') {
                    $cond .= ' and username ="' . $parrams['member'] . '"';
                }
            }
        } else {
            $cond = 'type = "Open"';
        }
        $cond .= ' and level >= ' . $lv;
        $pos = intval($parrams['pos']);
        if ($pos < 0) {
            $pos = 0;
        }
        echo $cond;
        exit;
        $data['results'] = $this->task_model->get_for_page($this->limit, $pos, $cond, 'id DESC');
        $total = $this->task_model->get_total_rows($cond);
        setcookie("pos_page", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("type_page", $parrams['type'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $this->limit);
        $this->load->view('task/list', $data);
        return false;
    }

    function del() {
        $id = intval($_POST['id']);
        $task = $this->task_model->get_by_key($id);
        if (!empty($task) && $this->data['user']['role'] == 'admin') {
            $this->task_model->delete($id);
        }
    }

    function del_department() {
        if (empty($this->data['user']['role_department'])) {
            echo 'Bạn không có quyền xóa phòng ban này.';
            exit;
        }
        $id = intval($_POST['id']);
        $department = $this->department_model->get_by_key($id);
        if (!empty($department)) {
            if ($department['network_id'] != $this->data['user']['network_id'] && empty($this->data['user']['role_network'])) {
                echo 'Bạn không có quyền xóa phòng ban này.';
                exit;
            }
            $list_member = $this->admin_model->get_by(array('department_id' => $department['id']));
            if (!empty($list_member)) {
                echo 'Bạn chỉ có thể xóa được phòng ban không có thành viên.';
                exit;
            }
            $this->department_model->delete($id);
            echo $id;
            exit;
        }
        echo 'Không tìm thấy phòng ban này.';
        exit;
    }

}
