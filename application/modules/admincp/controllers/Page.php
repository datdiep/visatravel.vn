<?php

class Page extends BACKEND_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('page_model'));
        $this->load->helper('database');
        $this->load->library(array('alias', 'form_validation'));
        $this->data['type'] = field_enums('page', 'type');
        $this->data['pre'] = 'page';

        $this->data['pre1'] = 'news';
    }

    public function add() {
        $this->data['pre'] = 'page_add';
        $this->data['check_error'] = -1;
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'page/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->alias->create_alias($_FILES['image']['name'])
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();
                        // $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'zithumb_' . $img['file_name'], 36, 36);
                        $image = $img['file_name'];
                        $add['image'] = $image;
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $add = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'type' => $this->input->post('type'),
                    'headline1' => $this->input->post('headline1'),
                    'headline2' => $this->input->post('headline2'),
                    'content' => $this->input->post('content'),
                    'publish_date' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'tags' => $this->input->post('tags')
                );
                $this->data['check_error'] = 0;
                $this->page_model->insert($add);
            }
        }
        $this->template->write_view('content_block', 'page/add', $this->data);
        $this->template->render();
    }

    public function index() {

        $this->data['pos'] = 1;
        $this->data['results'] = $this->page_model->get_all();
        $this->template->write_view('content_block', 'page/index', $this->data);
        $this->template->render();
    }

    public function edit($id) {
        $id = intval($id);
        $this->data['check_error'] = -1;
        $this->data['page'] = $this->page_model->get_by_key($id);
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('alias', 'Alias', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'type' => $this->input->post('type'),
                    'content' => $this->input->post('content'),
                    'headline1' => $this->input->post('headline1'),
                    'headline2' => $this->input->post('headline2'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'seo_title' => $this->input->post('seo_title'),
                    'meta_description' => $this->input->post('meta_description'),
                    'meta_keyword' => $this->input->post('meta_keyword'),
                    'tags' => $this->input->post('tags')
                );

                $image = '';
                if ($_FILES['image']['name']) {
                    $this->load->library(array('alias', 'upload', 'resize_image'));
                    $this->upload->initialize(array(
                        'upload_path' => PATH_UPLOAD . 'page/',
                        'allowed_types' => 'gif|jpg|png',
                        'overwrite' => false,
                        'file_name' => $this->alias->create_alias($_FILES['image']['name'])
                    ));
                    if ($this->upload->do_upload('image')) {
                        $img = $this->upload->data();

                        $update['image'] = $img['file_name'];
                    } else {
                        $this->data['check_error'] = 1;
                        $this->data['msg'] = $this->upload->display_errors();
                    }
                }

                $this->data['check_error'] = 0;
                $this->page_model->update($update, $id);
                $this->data['check_error'] = 0;
            }
        }
        $this->data['page'] = $this->page_model->get_by_key($id);
        $this->template->write_view('content_block', 'page/edit', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->page_model->delete($id);
    }

    function load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];
        if ($type) {
            $cond .= ' and type = "' . $type . '"';
        }

        if ($keyword) {
            $cond .= " and ( id = '$keyword' or title like '%$keyword%')";
            //$cond .= " and title like '%$keyword%'";
        }

        $limit = 20;
        //$order = $_POST['order'] ? $_POST['order'] : 'id DESC';
        $order = 'id desc';
        $data['results'] = $this->page_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->page_model->get_total_rows($cond);

        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('page/list', $data);
        return false;
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond = "title like '%$keyword%' or id = " . intval($keyword);
            $news = $this->news_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

}
