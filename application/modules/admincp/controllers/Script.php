<?php

class Script extends BACKEND_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('country_model', 'visa_fees_model', 'news_model'));
        $this->load->library(array('resize_image', 'transload'));
        $this->load->library(array('transload', 'alias', 'resize_image', 'curl'));
    }

    function visa_fees() {
        $this->load->library(array('my_category'));
        $visa_fees = $this->my_category->build_visa_fees();
        pre();
    }

    function get_country() {
        $config_domain = array(
            'name' => 'vietnamvisaon',
            'curl' => true,
            'will_script' => 1,
            'domain' => 'https://vietnamvisaon.com/en/visa-fees/',
            'prefix' => 'https://vietnamvisaon.com/en/visa-fees',
            'row' => '.select-nationality option',
        );
        $this->load->library(array('transload', 'alias', 'resize_image', 'curl'));
        include APPPATH . 'libraries/simple_html_dom.php';
        $link = $config_domain['domain'];
        echo $link . '<br/>';
        if ($config_domain['curl']) {
            $header = 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>';
            $agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
            $html = str_get_html($this->curl->get($link, $agent, $config_domain['domain']));
        } else {
            $html = file_get_html($link);
        }
        $row = $html->find($config_domain['row']);
        foreach ($row as $key => $element) {
            $id = $element->value;
            $name = $element->plaintext;
            echo $name . '<br>';
            $result[$key]['name'] = $name;
            $result[$key]['alias'] = $this->alias->create_alias($name);
            $href = $config_domain['domain'] . $result[$key]['alias'] . '/' . $id;
            $result[$key]['url_source'] = $href;
            $result[$key]['source_type'] = $config_domain['name'];
            $result[$key]['will_script'] = $config_domain['will_script'];
        }
        //pre($result);
        $count_result = count($result);
        if ($count_result) {
            $this->country_model->insert_batch($result);
        }
    }

    function get_total_visa_fees() {
        $config_get_content = array(
            array('name' => 'vietnamvisaon', 'function_name' => 'get_content_visa_fees'),
        );
        if ($this->{$config_get_content[0]['function_name']}()) {
            echo '<h1>Đã lấy xong</h1>';
            exit;
        }
        echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script/get_total_visa_fees" />';
    }

    function get_content_news() {
        $config_get_content = array(
            array('name' => 'vietnamvisaon', 'function_name' => 'get_total'),
        );
        if ($this->{$config_get_content[0]['function_name']}()) {
            echo '<h1>Đã lấy xong</h1>';
            exit;
        }
        echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script/get_content_news" />';
    }

    private function get_content_visa_fees() {
        $product = $this->country_model->get_row_by('will_script = 1');
        if (empty($product)) {
            return true;
        } else {
            echo '---------- Script #' . $product['id'] . ' ' . $product['name'] . ' --------------<br/>';
            include APPPATH . 'libraries/simple_html_dom.php';
            $this->load->library(array('curl', 'alias'));
            $domain = 'https://vietnamvisaon.com/';
            $agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
            $html = str_get_html($this->curl->get($product['url_source'], $agent, $domain));
            $link = $product['url_source'];
            echo $link . '<br>';
            $table_price = $html->find('.table-bordered tbody');
            $box_waring = $html->find('.box-waring');
            array_pop($table_price);
            array_pop($table_price);
            $x = 0;
            foreach ($table_price as $key => $element) {
                $tr = $element->find('tr');
                array_splice($tr, 0, 3);
                foreach ($tr as $k => $ele) {
                    $td = $ele->find('td');
                    if ($key == 0) {
                        $result[$x]['visa_type'] = 'for tourism visa';
                    }
                    if ($key == 1) {
                        $result[$x]['visa_type'] = 'for business visa';
                    }
                    if ($key == 2) {
                        $result[$x]['visa_type'] = 'family/friend visit visa';
                    }

                    $result[$x]['country_id'] = $product['id'];
                    $result[$x]['visa_exp'] = @$td[0]->plaintext;
                    if ($k == 5) {
                        $result[$x]['visa_exp'] = '1 year multiple';
                    }
                    $result[$x]['1_pax'] = @$td[1]->plaintext;
                    $result[$x]['2_pax'] = @$td[2]->plaintext;
                    $result[$x]['4_pax'] = @$td[3]->plaintext;
                    $result[$x]['6_pax'] = @$td[4]->plaintext;
                    $result[$x]['10_pax'] = @$td[5]->plaintext;
                    $result[$x]['urgent'] = @$td[6]->plaintext;
                    $result[$x]['emergency'] = @$td[7]->plaintext;
                    $result[$x]['overtime'] = @$td[8]->plaintext;
                    $result[$x]['holiday'] = @$td[9]->plaintext;
                    $n = 0;
                    if ($k == 0 || $k == 1)
                        $n = 1;
                    if ($k == 2 || $k == 3)
                        $n = 2;
                    if ($k == 4)
                        $n = 3.8;
                    if ($k == 5)
                        $n = 5.4;
                    $result[$x]['stamping_fee'] = $n * 25;
                    $x++;
                }
            }
            $count_result = count($result);
            $count_box = count($box_waring);
            $update = array(
                'will_script' => 0
            );
            if ($count_box > 3) {
                $update['note'] = $box_waring[0]->innertext;
                $update['note'] = str_replace('vietnamvisaon.com', 'getvietnamvisa.net', $update['note']);
            }
            pre($result, FALSE);
            if ($count_result) {
                $this->visa_fees_model->insert_batch($result);
            }
            $this->country_model->update($update, $product['id']);
        }
        return false;
    }

    function get_total($domain = 0, $cat = 0, $page = -1) {
        $this->total_config = array(
            array(
                'name' => 'quatangthuongyeu',
                'curl' => true,
                'will_script' => 1,
                'domain' => 'https://vietnamvisaon.com/en/visa-fees/',
                'prefix' => 'https://vietnamvisaon.com/en/visa-fees',
                'row' => '.prohot .new',
                'last_url' => '',
                'element' => array(
                    'href' => array('tag' => 'a', 'attr' => 'href', 'index' => 0),
                    'image' => array('has_get' => TRUE, 'tag' => 'img', 'attr' => 'src', 'index' => 0),
                    'title' => array('tag' => 'img', 'attr' => 'title', 'index' => 0),
                ),
                'category' => array(
                    array('url' => 'cach-tang-qua.html?page=', 'from' => 1, 'to' => 5, 'cat_id' => 74),
                )
            ),
	    array(
                'name' => 'quatangthuongyeu',
                'curl' => true,
                'will_script' => 1,
                'domain' => 'https://vietnamvisaon.com/en/visa-fees/',
                'prefix' => 'https://vietnamvisaon.com/en/visa-fees',
                'row' => '.prohot .new',
                'last_url' => '',
                'element' => array(
                    'href' => array('tag' => 'a', 'attr' => 'href', 'index' => 0),
                    'image' => array('has_get' => TRUE, 'tag' => 'img', 'attr' => 'src', 'index' => 0),
                    'title' => array('tag' => 'img', 'attr' => 'title', 'index' => 0),
                ),
                'category' => array(
                    array('url' => 'cach-tang-qua.html?page=', 'from' => 1, 'to' => 5, 'cat_id' => 74),
                )
            ),
        );
        $domain = intval($domain);
        $cat = intval($cat);
        if (isset($this->total_config[$domain]['category'][$cat])) {
            //$this->load->model(array('product_model'));
            $this->load->library(array('transload', 'alias', 'resize_image', 'curl'));
            include APPPATH . 'libraries/simple_html_dom.php';
            $config_domain = $this->total_config[$domain];
            $category = $config_domain['category'][$cat];
            if ($page == -1) {
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_news/get_total/' . $domain . "-" . $config_domain['name'] . '/' . $cat . '/' . $category['to'] . '" />';
                return false;
            }

            $link = $config_domain['domain'] . $category['url'] . $page . $config_domain['last_url'];

            echo $link . '<br/>';
            //$http_headers = get_headers($link);
            //pre($http_headers);
            //if (strpos($http_headers[0], '200 OK') >= 0) {
            if ($config_domain['curl']) {
                $header = 'Mozilla/5.0 (Linux; <Android Version>; <Build Tag etc.>) AppleWebKit/<WebKit Rev> (KHTML, like Gecko) Chrome/<Chrome Rev> Mobile Safari/<WebKit Rev>';
                $agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
                if (!empty($config_domain['param'])) {
                    pre($config_domain['param'], FALSE);
                    $reponse = $this->curl->post($link, $config_domain['param'], $header, 'http://muahangsi.vn/san_pham/');
                    $html = str_get_html($reponse);
                } else {
                    $html = str_get_html($this->curl->get($link, $agent, $config_domain['domain']));
                }
            } else {
                $html = file_get_html($link);
            }
            // echo $html; exit;
            $row = $html->find($config_domain['row']);
            //pre($row);
            $result = array();
            $fHref = @file_get_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_href.txt');
            $fTitle = @file_get_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_title.txt');
            $more_fhref = $more_ftitle = '';
            // @mkdir(PATH_PAGE . date('d-m-Y'), 0777);                
            foreach ($row as $key => $element) {
                $href = strtolower($element->find($config_domain['element']['href']['tag'], $config_domain['element']['href']['index'])->{$config_domain['element']['href']['attr']});
                $title = ($element->find($config_domain['element']['title']['tag'], $config_domain['element']['title']['index'])->{$config_domain['element']['title']['attr']});
                if ($href && $title) {
                    if (strpos($fHref, $href . ',') === false && stripos($fTitle, $title . ',') === false) {
                        $fHref .= $href . ',';
                        $fTitle .= $title . ',';
                        $more_fhref .= $href . ',';
                        $more_ftitle .= $title . ',';
                        if (strpos($href, 'http://') === false && strpos($href, 'https://') === false) {
                            $href = $this->total_config[$domain]['prefix'] . $href;
                        }
                        $result[$key]['title'] = $title;
                        $result[$key]['alias'] = $this->alias->create_alias($title);
                        $result[$key]['id_cat'] = $category['cat_id'];
                        $result[$key]['url_source'] = $href;
                        $result[$key]['date_create'] = date('Y-m-d H:i:s');
                        $result[$key]['source_type'] = $config_domain['name'];
                        $result[$key]['will_script'] = $config_domain['will_script'];

                        if ($config_domain['element']['image']['has_get']) {
                            if (strlen($result[$key]['alias']) > 50) {
                                $img_name = substr($result[$key]['alias'], 0, 50);
                            } else {
                                $img_name = $result[$key]['alias'];
                            }
                            $path = PATH_UPLOAD . 'news';
                            @mkdir($path, 0777);
                            $img = @$element->find($config_domain['element']['image']['tag'], $config_domain['element']['image']['index'])->$config_domain['element']['image']['attr'];
                            $img = str_replace(' ', '%20', $img);
                            //$img = str_replace('https://', 'http://', $img);
                            //echo $img;
                            if (!empty($img)) {
                                $contents_img = $this->GetImageFromUrl($img);
                                $savefile = fopen($path . '/' . $img_name . '.jpg', 'w');
                                fwrite($savefile, $contents_img);
                                fclose($savefile);
                                $this->resize_image->crop_resize($path . '/' . $img_name . '.jpg', $path . '/' . $img_name . '.jpg', 292, 190);
                                $result[$key]['image'] = $img_name . '.jpg';
                            }
                        }
                    }
                }
            }
            //}

            $count_result = count($result);
            //pre($result);
            if ($more_fhref) {
                file_put_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_href.txt', $more_fhref, FILE_APPEND);
                file_put_contents(PATH_LOG_VIDEO . $config_domain['name'] . '_title.txt', $more_ftitle, FILE_APPEND);
            }
//            foreach ($result as $v) {
//                $this->page_model->insert($v);
//            }
            if ($count_result) {
                $this->news_model->insert_batch($result);
            }
        }

        echo '<pre>';
        print_r($result);
        if ($page == $this->total_config[$domain]['category'][$cat]['from']) {
            if (isset($this->total_config[$domain]['category'][++$cat])) {
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_news/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . $this->total_config[$domain]['category'][$cat]['to'] . '" />';
            } elseif (isset($this->total_config[++$domain])) {
                $cat = 0;
                echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_news/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . $this->total_config[$domain]['category'][$cat]['to'] . '" />';
            } else {
                //pre($result);
                echo 'Lấy thanh công, chuyển qua lấy content';
                $link_redirect = '/' . ADMIN_URL . 'script_news/get_total_content';
                echo '<meta http-equiv="refresh" content="0; url=' . $link_redirect . '" />';
            }
        } else {
            echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script_news/get_total/' . $domain . "-" . $this->total_config[$domain]['name'] . '/' . $cat . '/' . ( --$page) . '" />';
        }
    }

    function get_total_content($key = -1) {
        $key = intval($key);
        $config_get_content = array(
            array('name' => 'visatravel', 'function_name' => 'get_content_news'),
        );
        if ($key != -1) {
            if (isset($config_get_content[$key])) {
                if ($this->{$config_get_content[$key]['function_name']}($config_get_content[$key]['name'])) {
                    $key++;
                }
            } else {
                echo '<h1>Đã lấy xong</h1>';
                exit;
            }
        } else {
            $key++;
        }
        echo '<meta http-equiv="refresh" content="0; url=/' . ADMIN_URL . 'script/get_total_content/' . $key . '" />';
    }

    private function get_content_quatangthuongyeu($type) {
        $product = $this->news_model->get_row_by('will_script = 1 and source_type = "' . $type . '"');
        if (empty($product)) {
            return true;
        } else {
            echo '---------- Script #' . $product['id'] . ' ' . $product['title'] . ' --------------<br/>';
            include APPPATH . 'libraries/simple_html_dom.php';
            $this->load->library(array('curl', 'alias'));
            $html = @file_get_html($product['url_source']);
            $domain = 'https://quatangthuongyeu.com/';
            $agent = 'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/37.0.2049.0 Safari/537.36';
            $html = str_get_html($this->curl->get($product['url_source'], $agent, $domain));
            $link = $product['url_source'];
            echo $link;
            //$data = $content->find('#content');
            //pre($data,FALSE);
            $title = $html->find('title', 0)->plaintext;
            $des = @$html->find('meta', 6)->content;
            $keyword = @$html->find('meta', 7)->content;
            $content = $html->find('.contentnew', 0)->innertext;
            $alias = $product['alias'];
            if (!empty($content)) {
                $content = $this->getImg($content, $alias);
            }
            $update = array(
                'will_script' => 0,
                'seo_title' => $title,
                'meta_description' => $des,
                'content' => $content,
                'meta_keyword' => $keyword,
            );
            //pre($update);
            $this->news_model->update($update, $product['id']);
        }
        return false;
    }

    private function getImg($content, $alias) {
        $html = str_get_html($content);
        $i = 0;
        $images = array();
        $path = APPPATH . '../assets/images/';
        @mkdir($path, 0777, true);
        foreach (@$html->find('a') as $e) {
            $href = $e->href;
            $href = str_replace('https://quatangthuongyeu.com', '/chi-tiet', $href);
            $e->href = $href;
        }
        foreach (@$html->find('img') as $e) {
            $img = $e->src;
            $img = str_replace(' ', '%20', $img);
            $img_name = $alias . '-' . $i++ . '.jpg';
            if (!empty($img)) {
                $contents_img = $this->GetImageFromUrl($img);
                $savefile = fopen($path . '/' . $img_name, 'w');
                fwrite($savefile, $contents_img);
                fclose($savefile);
                $e->src = '/assets/images/' . $img_name;
            }
        }

        $html->find('a', -1)->href = '';
        $ret = $html->save();
        return $ret;
    }

    private function getContent($html, $att_content, $arr_att_clean = array()) {
        include APPPATH . 'libraries/simple_html_dom.php';
//        $content_html = '';
        foreach ($html->find($att_content) as $e) {
            $content_html = $e->innertext;
        }
        $html = str_get_html($content_html);
        foreach ($arr_att_clean as $att_clean) {
            // google+
            foreach ($html->find($att_clean) as $e) {
                $e->outertext = '';
            }
        }

        $ret = $html->save();
        $this->removeLink($ret);
        return $ret;
    }

    private function removeLink($content) {
        $html = str_get_html($content);
        // link content
        foreach ($html->find('a') as $e) {
            $e->outertext = $e->innertext;
        }
        $ret = $html->save();
        return $ret;
    }

    private function remove_p($content) {
        $html = str_get_html($content);
        // link content
        foreach ($html->find('p') as $e) {
            $e->outertext = $e->innertext;
        }
        $ret = $html->save();
        return $ret;
    }

    function GetImageFromUrl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_REFERER, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    // function xoa phan tu html cuoi cung
    private function removeLastElement($content, $element) {
        $html = str_get_html($content);
        // link content
        $html->find($element, -1)->outertext = '';
        $ret = $html->save();
        return $ret;
    }

    // function xoa phan tu html truyen vao dau tien
    private function removeFirstElement($content, $element) {
        $html = str_get_html($content);
        $html->find($element, 0)->outertext = '';
        $ret = $html->save();
        return $ret;
    }

}
