<?php

class Tool extends BACKEND_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model(array('taiwan_model', 'labour_contract_model'));
        $this->load->library(array('resize_image', 'transload', 'curl', 'alias'));
        $this->load->helper('directory');

        if (!empty($this->data['user']['role_network'])) {
            $this->data['networks'] = $this->network_model->get_all();
        }
    }

    public function detect_passport() {
        $pathImage = APPPATH . '../' . 'assets/';
        $mrzEngine = new mrz(array(
            'pathImage' => $pathImage,
            'pathTesseract' => '/usr/local/bin/tesseract',
            'debug' => true
        ));
        $filenameUpload = $pathImage . '/imageUpload/' . md5(time()) . '.png';

        if ($_FILES['webcam']['error'] === 0) {
            if (move_uploaded_file($_FILES['webcam']['tmp_name'], $filenameUpload)) {
                $data = $mrzEngine->run($filenameUpload);
                if (empty($data) === false) {
                    echo json_encode($mrzEngine->run($filenameUpload));
                } else {
                    // @unlink($filenameUpload);
                    echo json_encode($data);
                }
            };
        }
    }

    public function listvisa() {
        $this->data['pre'] = 'tool_listvisa';
        $this->load->library(array('ocr', 'transload', 'curl', 'alias'));

        include APPPATH . 'libraries/ocr/index.php';

        //$pathImage = APPPATH . '../' . 'assets/';
        $pathImage = PATH_UPLOAD . '/ocr/';
        $mrzEngine = new mrz(array(
            'pathImage' => $pathImage,
            'pathTesseract' => '/usr/local/bin/tesseract',
            'debug' => true
        ));

        if ($handle = opendir($pathImage . 'imageTest')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
                    echo '<img src="./imageTest/' . $entry . '" width="500"><br>';
                    echo '<pre>';
                    $pathMrzImage = $pathImage . 'imageTest/' . $entry;
                    print_r($mrzEngine->run($pathMrzImage));
                    echo '</pre>';
                    echo '<br>====================================<br>';
                }
            }
            closedir($handle);
        }

        $this->template->write_view('content_block', 'tool/list_visa', $this->data);
        $this->template->render();
    }

    public function arrival($id) {
        if (is_numeric($id)) {
            if (isset($_POST['submit'])) {
                $data_post = $this->input->post();
                unset($data_post['submit']);
                $this->taiwan_model->update($data_post, $id);
            }

            $objInfor = $this->taiwan_model->get_by_key($id);
            $birthDate = date("Y/m/d", strtotime($objInfor['birthDate']));
            $this->data['ngaysinh'] = explode('/', $birthDate);
        }

        $this->data['objInfor'] = $objInfor;


        $this->load->view('tool/arrival_online', $this->data);
        return false;

//        $this->template->write_view('content_block', 'tool/arrival_online', $this->data);
//        $this->template->render();
    }

    public function apply($id) {
        $this->data['pre'] = 'tool_taiwan';
        $this->data['check_error'] = -1;
        $rand = md5(microtime());
        $this->curl->setpathcookie(PATH_LOG . 'cookies/cookie_' . $rand . '.txt');

        if (is_numeric($id)) {
            $objInfor = $this->taiwan_model->get_by_key($id);
            $this->data['info']['birthDate'] = date("d/m/Y", strtotime($objInfor['birthDate']));
            $this->data['info']['passportExpiryDate'] = date("d/m/Y", strtotime($objInfor['passportExpiryDate']));
        }

        if (isset($_POST['submit'])) {

//        $objInfor = array(
//                'language' => '4',
//                'resutlMsg' => '',
//                'nation' => '34',
//                'surname' => 'LE',
//                'given' => 'HONG LINH',
//                'englishName' => 'LE HONG LINH',
//                'passno' => 'B5623888',
//                'passportExpiryDate' => '20210709',
//                'birthDate' => '19870206',
//                'sex' => '1',
//                'rq.flightNo' => 'VJ582',
//                'expectEntryDate_year' => '2017',
//                'expectEntryDate_month' => '10',
//                'expectEntryDate_day' => '16',
//                'rq.occupation' => '24',
//                'rq.reason' => '23',
//                'rq.liveCountry' => '',
//                'rq.liveAddress' => '',
//                'rq.hotelFlag' => '1',
//                'rq.zipCode' => '1',
//                'rq.address' => 'Taipei hotel',
//                'phone_number' => '0909555666',
//                'email' => '',
//                'capacity' => '1',
//                'capacity_1_type' => ' ',
//                'capacity_2_type' => '015',
//                'latest_arrival_date' => '0',
//                'latest_depart_date' => '0',
//                'credentials' => '05076433',
//                'cardExpiryDate' => '',
//                'verifyCode' => '1326',
//            );
            // Xu ly hộ chiếu
//            require_once(APPPATH . 'libraries/ocr/index.php');
//            $path = APPPATH . 'libraries/ocr/';
//            $pathImage = PATH_UPLOAD . 'ocr/';
//            $mrzEngine = new mrz(array(
//                'path' => $path,
//                'pathImage' => $pathImage,
//                'pathTesseract' => '/usr/bin/tesseract'
//            ));
//
//
//            if ($handle = opendir($pathImage . 'imageTest')) {
//                while (false !== ($entry = readdir($handle))) {
//                    if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
//                        echo '<img src="/assets/upload/ocr/imageTest/' . $entry . '" width="500"><br>';
//                        echo '<pre>';
//                        $pathMrzImage = $pathImage . 'imageTest/' . $entry;
//                        print_r($mrzEngine->run($pathMrzImage));
//                        echo '</pre>';
//                        echo '<br>====================================<br>';
//                    }
//                }
//                closedir($handle);
//            }
            // end


            $objInfor = $_POST;

            $this->data['info'] = array(
                'expectEntryDate' => $objInfor['expectEntryDate'],
                'passportExpiryDate' => $objInfor['passportExpiryDate'],
                'birthDate' => $objInfor['birthDate']
            );

            $this->form_validation->set_rules('surname', 'Họ', 'required');
            $this->form_validation->set_rules('given', 'Tên', 'required');
            $this->form_validation->set_rules('phone_number', 'Số điện thoại', 'required');
            $this->form_validation->set_rules('passno', 'Số hộ chiếu', 'required');
            $this->form_validation->set_rules('rq_flightNo', 'Số hiệu chuyến bay', 'required');
            $this->form_validation->set_rules('birthDate', 'Ngày tháng năm sinh', 'required');
            $this->form_validation->set_rules('passportExpiryDate', 'Ngày hết hạn hộ chiếu', 'required');
            $this->form_validation->set_rules('expectEntryDate', 'Ngày đến Đài Loan', 'required');
            $this->form_validation->set_rules('rq_zipCode', 'Thành phố sẽ đến', 'required');
            $this->form_validation->set_rules('rq_address', 'Địa chỉ lưu trú ở Đài Loan', 'required');
            $this->form_validation->set_rules('capacity_2_type', 'Quốc gia tiên tiến có visa', 'required');
            $this->form_validation->set_rules('credentials', 'Số Visa tiên tiến', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $expectEntryDate = explode('/', $objInfor['expectEntryDate']);
                $passportExpiryDate = explode('/', $objInfor['passportExpiryDate']);
                $birthDate = explode('/', $objInfor['birthDate']);
                $objInfor['passportExpiryDate'] = $passportExpiryDate[2] . $passportExpiryDate[1] . $passportExpiryDate[0];
                $objInfor['birthDate'] = $birthDate[2] . $birthDate[1] . $birthDate[0];

                $html_special = '';

//                if ($objInfor['special_block'] == '1') {
//                    $html_special = ' ';
//                } elseif ($objInfor['special_block'] == '2') {
//                    $html_special = '&nbsp;';
//                }

                $objInfor = array(
                    'language' => '4',
                    'resutlMsg' => '',
                    'nation' => '34',
                    'surname' => strtoupper($objInfor['surname']) . $html_special,
                    'given' => strtoupper($objInfor['given']) . $html_special,
                    'englishName' => strtoupper($objInfor['surname']) . $html_special . strtoupper($objInfor['given']) . $html_special,
                    'passno' => strtoupper($objInfor['passno']),
                    'passportExpiryDate' => $objInfor['passportExpiryDate'],
                    'birthDate' => $objInfor['birthDate'],
                    'sex' => $objInfor['sex'],
                    'expectEntryDate_year' => $expectEntryDate[2],
                    'expectEntryDate_month' => $expectEntryDate[1],
                    'expectEntryDate_day' => $expectEntryDate[0],
                    'rq.flightNo' => $objInfor['rq_flightNo'],
                    'rq.occupation' => $objInfor['rq_occupation'],
                    'rq.reason' => $objInfor['rq_reason'],
                    'rq.liveCountry' => '',
                    'rq.liveAddress' => '',
                    'rq.hotelFlag' => $objInfor['rq_hotelFlag'],
                    'rq.zipCode' => $objInfor['rq_zipCode'],
                    'rq.address' => $objInfor['rq_address'],
                    'phone_number' => $objInfor['phone_number'],
                    'email' => '',
                    'capacity' => $objInfor['capacity'],
                    'capacity_1_type' => ' ',
                    'capacity_2_type' => $objInfor['capacity_2_type'],
                    'latest_arrival_date' => '0',
                    'latest_depart_date' => '0',
                    'credentials' => $objInfor['credentials'],
                    'cardExpiryDate' => '',
                    'verifyCode' => '1326',
                );

                //pre($objInfor, FALSE);
                //echo json_encode($objInfor);
                $html = $this->curl->post('https://niaspeedy.immigration.gov.tw/nia_southeast/booking!doApproval', $objInfor);

                $objPrint['mac'] = $this->transload->split_content('name="mac" value="', '"', $html);
                $objPrint['receiveNo'] = $this->transload->split_content('name="receiveNo" value="', '"', $html);

                if ($objPrint['mac']) {
                    $html = $this->curl->post('https://niaspeedy.immigration.gov.tw/nia_southeast/southeastPrint', $objPrint);
                    if ($html) {

                        $filename = strtoupper($this->alias->create_alias($objInfor['englishName'] . '-' . $objInfor['passno'] . '-' . date('d-m-Y')));
                        $this->curl->writefile(PATH_LOG . '/pdf/' . $filename . '.pdf', $html);

                        //insert database
                        unset($objInfor['rq.flightNo']);
                        unset($objInfor['rq.occupation']);
                        unset($objInfor['rq.reason']);
                        unset($objInfor['rq.liveCountry']);
                        unset($objInfor['rq.liveAddress']);
                        unset($objInfor['rq.hotelFlag']);
                        unset($objInfor['rq.zipCode']);
                        unset($objInfor['rq.address']);
                        unset($objInfor['special_block']);

                        $objInfor['rq_flightNo'] = $_POST['rq_flightNo'];
                        $objInfor['rq_occupation'] = $_POST['rq_occupation'];
                        $objInfor['rq_reason'] = $_POST['rq_reason'];
                        $objInfor['rq_liveCountry'] = '';
                        $objInfor['rq_liveAddress'] = '';
                        $objInfor['rq_hotelFlag'] = $_POST['rq_hotelFlag'];
                        $objInfor['rq_zipCode'] = $_POST['rq_zipCode'];
                        $objInfor['rq_address'] = $_POST['rq_address'];
                        $objInfor['username'] = $this->data['user']['username'];
                        $objInfor['date_post'] = date('Y-m-d H:i:s');
                        $objInfor['network_id'] = $this->data['user']['network_id'];
                        $objInfor['author_id'] = $this->data['user']['id'];
                        $objInfor['file'] = $filename . '.pdf';

                        // pre($objInfor);


                        $last_id = $this->taiwan_model->insert($objInfor);

                        // end

                        $this->data['result'] = 'ok';
                        $this->data['customer'] = $objPrint;
                        $this->data['customer']['url'] = '/assets/logs/pdf/' . $filename . '.pdf';
                    } else {
                        // Khong load duoc file pdf
                        $this->data['result'] = 'false';
                        @file_put_contents($html, PATH_LOG . 'error/error_' . $this->alias->create_alias($objInfor['englishName'] . '-' . $objInfor['passno']));
                    }
                }
            }
        }

        $this->data['objInfor'] = $objInfor;
        if ($this->data['objInfor']['id']) {
            $this->data['last_id'] = $this->data['objInfor']['id'];
        } else {
            $this->data['last_id'] = $last_id;
        }
        $this->template->write_view('content_block', 'tool/apply_taiwan', $this->data);
        $this->template->render();
    }

    public function copy($id) {
        $this->data['pre'] = 'tool_taiwan';
        $id = intval($id);
        $this->data['check_error'] = -1;

        $rand = md5(microtime());
        $this->curl->setpathcookie(PATH_LOG . 'cookies/cookie_' . $rand . '.txt');
        $objInfor = $_POST;

        if (isset($_POST['submit']) && $_POST['submit'] != '-1') {

            $this->form_validation->set_rules('surname', 'Họ', 'required');
            $this->form_validation->set_rules('given', 'Tên', 'required');
            $this->form_validation->set_rules('phone_number', 'Số điện thoại', 'required');
            $this->form_validation->set_rules('passno', 'Số hộ chiếu', 'required');
            $this->form_validation->set_rules('rq_flightNo', 'Số hiệu chuyến bay', 'required');
            $this->form_validation->set_rules('birthDate', 'Ngày tháng năm sinh', 'required');
            $this->form_validation->set_rules('passportExpiryDate', 'Ngày hết hạn hộ chiếu', 'required');
            $this->form_validation->set_rules('expectEntryDate', 'Ngày đến Đài Loan', 'required');
            $this->form_validation->set_rules('rq_zipCode', 'Thành phố sẽ đến', 'required');
            $this->form_validation->set_rules('rq_address', 'Địa chỉ lưu trú ở Đài Loan', 'required');
            $this->form_validation->set_rules('capacity_2_type', 'Quốc gia tiên tiến có visa', 'required');
            $this->form_validation->set_rules('credentials', 'Số Visa tiên tiến', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {

                $update = array(
                    'title' => $this->input->post('title'),
                    'alias' => $this->alias->create_alias($this->input->post('alias')),
                    'content' => $this->input->post('content'),
                    'youtube' => $this->input->post('youtube'),
                    'last_update' => date('Y-m-d H:i:s'),
                    'author_id' => $this->data['user']['id'],
                    'author_username' => $this->data['user']['username'],
                    'tags' => $this->input->post('tags'),
                );

                //$this->data['check_error'] = 0;
                $this->taiwan_model->update($update, $id);
                $this->data['check_error'] = 0;
            }
        }



        $this->template->write_view('content_block', 'tool/copy', $this->data);
        $this->template->render();
    }

    public function company() {
        $this->data['pre'] = 'tool_company';
        $this->data['check_error'] = -1;
        include APPPATH . 'libraries/simple_html_dom.php';

        if ($_POST) {

            $this->form_validation->set_rules('mst', 'Mã số thuế', 'required');
            $this->form_validation->set_rules('captcha', 'Captcha', 'required');

            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $path_cookies = PATH_LOG . 'cookies/' . $_POST['session_company'] . '.txt';
                $this->curl->setpathcookie($path_cookies);
                //Step 2: chạy hàm này sau khi chạy step 1 để lấy thông tin, session phải giống ở bước 1

                $this->data['thue'] = $this->getInformation($this->curl, $_POST['session_company'], $_POST['mst'], $this->input->post('captcha'));

                $this->data['mst'] = $this->input->post('mst');
                // Get Dangkykinhdoanh.gov.vn
                $this->data['dkkd'] = $this->getInformationDKKD($this->curl, $_POST['session_company'], $_POST['mst'], PATH_LOG);
            }
        }

        $this->data['session_company'] = md5(microtime());

        $this->curl->setpathcookie(PATH_LOG . 'cookies/' . $this->data['session_company'] . '.txt');
        //Step 1: chạy hàm này trước để lấy captcha về, session là số ngãu nhiên của giao dịch đó, lưu ý: session của 2 bước phải giống nhau
        $this->generateCaptcha($this->curl, $this->data['session_company']);


        $this->template->write_view('content_block', 'tool/company', $this->data);
        $this->template->render();
    }

    public function index() {
        $this->data['pre'] = 'list_taiwan';
        $this->data['pos'] = intval(@$_COOKIE['pos_page_taiwan']);
        $this->data['taiwan_type'] = @$_COOKIE['taiwan_type'];

        $cond = '';
        $this->data['results'] = $this->taiwan_model->get_for_page($limit, $pos, $cond, $order);
        $total = $this->taiwan_model->get_total_rows($cond);

        $this->data['links'] = $this->get_page($pos, $total, $limit);

        $this->template->write_view('content_block', 'tool/index', $this->data);
        $this->template->render();
    }

    public function del() {
        $id = intval(@$_POST['id']);
        $this->taiwan_model->delete($id);
    }

    function load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];

        $cond .= ' and network_id = ' . $this->data['user']['network_id'] . ' ';
        if (!empty($this->data['user']['role_network'])) {
            $cond = 'id > 0';
        }

        if ($type) {
            $cond .= ' and network_id = "' . $type . '" ';
        }

        if ($keyword) {
            $cond .= " and ( englishName like '%$keyword%' or passno like '%$keyword%')";
        }


        $limit = 20;

        $order = 'id desc';
        //echo $cond;
        $data['results'] = $this->taiwan_model->get_for_page($limit, $pos, $cond, $order);

        $total = $this->taiwan_model->get_total_rows($cond);
        setcookie("pos_page_taiwan", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("taiwan_type", $type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('tool/list', $data);
        return false;
    }

    function labour_load() {
        $pos = intval(@$_REQUEST['pos']);
        $cond = 'id > 0';
        $type = @$_REQUEST['type'];
        $keyword = @$_REQUEST['keyword'];

        $cond .= ' and network_id = ' . $this->data['user']['network_id'] . ' ';
        if (!empty($this->data['user']['role_network'])) {
            $cond = 'id > 0';
        }

        if ($type) {
            $cond .= ' and network_id = "' . $type . '" ';
        }

        if ($keyword) {
            $cond .= " and ( hoten like '%$keyword%' or cmnd like '%$keyword%')";
        }


        $limit = 20;

        $order = 'id desc';
        //echo $cond;
        $data['results'] = $this->labour_contract_model->get_for_page($limit, $pos, $cond, $order);

        $total = $this->labour_contract_model->get_total_rows($cond);
        setcookie("pos_page_taiwan", $pos, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        setcookie("taiwan_type", $type, (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
        $data['links'] = $this->get_page($pos, $total, $limit);
        $this->load->view('tool/labour_load', $data);
        return false;
    }

    public function labour_contract($id) {
        $this->data['pre'] = 'tool_labour_contract';
        $this->load->library(array('word', 'transload', 'curl', 'alias'));
        $this->load->helper(array('date', 'database', 'download'));

        $this->data['list_loaihd'] = field_enums('labour_contract', 'loai_hd');

        $current_month = mdate('%m', time());
        $this->data['salary_date'] = array(
            'current_month' => $current_month,
            'current_day' => mdate('%d', time()),
            '3' => $current_month,
            '2' => $current_month - 1,
            '1' => $current_month - 2
        );


        $this->data['list_company'] = directory_map('./assets/upload/labour_contract/template', 1);

        if (is_numeric($id)) {
            $this->data['info_labour'] = $objInfor = $this->labour_contract_model->get_by_key($id);
            $this->data['info']['birthDate'] = date("d/m/Y", strtotime($objInfor['birthDate']));
            $this->data['info']['passportExpiryDate'] = date("d/m/Y", strtotime($objInfor['passportExpiryDate']));
        }

        if (isset($_POST['submit'])) {
            $this->word->pathTemp = PATH_UPLOAD . 'labour_contract/temporaries/';
            $this->word->pathOutput = PATH_UPLOAD . 'labour_contract/output';

            $template = PATH_UPLOAD . 'labour_contract/template/' . $_POST['company_name'];

            $datapost = $this->input->post();

            $this->data['info_labour'] = $datapost;

            $filename = $this->alias->create_alias($this->input->post('hoten')) . '-' . $this->alias->create_alias($this->input->post('company_name')) . '-' . time();

            $this->data['info_labour']['luong'] = number_format($this->data['info_labour']['luong']);

            for ($i = 1; $i < 4; $i++) {
                $this->data['info_labour']['kinhdoanh_' . $i] = number_format($this->data['info_labour']['kinhdoanh_' . $i]);
                $this->data['info_labour']['phucap_' . $i] = number_format($this->data['info_labour']['phucap_' . $i]);
                $this->data['info_labour']['giamtru_' . $i] = number_format($this->data['info_labour']['giamtru_' . $i]);
                $this->data['info_labour']['tong_' . $i] = number_format($this->data['info_labour']['tong_' . $i]);
            }


//            pre($this->data['info_labour']);

            $result = $this->word->replaceInFolder($template, $this->data['info_labour'], $filename);

            if ($result) {

                $datapost['user_create'] = $this->data['user']['username'];
                $datapost['date_create'] = date('Y-m-d H:i:s');
                $datapost['network_id'] = $this->data['user']['network_id'];
                $datapost['filename'] = $filename;

                unset($datapost['submit']);

                if (is_numeric($id)) {
                    $this->labour_contract_model->update($datapost, $id);
                } else {
                    $this->labour_contract_model->insert($datapost);
                }

                // download file ve 
                force_download($this->word->pathOutput . '/' . basename($result), NULL);
            } else {
                echo 'thất bại';
            }
        }

        if ($id == 'list') {
            $this->data['pre'] = 'tool_list_labour';
            $this->data['pos'] = intval(@$_COOKIE['pos_page_taiwan']);
            $this->data['taiwan_type'] = @$_COOKIE['taiwan_type'];
            $this->data['results'] = $this->taiwan_model->get_all();
            $this->template->write_view('content_block', 'tool/labour_list', $this->data);
        } else {
            $this->template->write_view('content_block', 'tool/labour_contract', $this->data);
        }

        $this->template->render();
    }

    function search() {
        if (isset($_GET['q'])) {
            $this->load->model(array('news_model', 'category_news_model'));
            $keyword = ($this->security->xss_clean($_GET['q']));
            $cond .= "title like '%$keyword%' or id = " . intval($keyword);
            $cond .= " and network_id = " . $this->data['user']['network_id'];
            $news = $this->taiwan_model->get_for_page(10, 0, $cond);
            echo json_encode($news); //format the array into json data            
        }
    }

    function generateCaptcha($curl, $session) {

        //echo PATH_LOG . 'cookies/company' . $session . '.txt';
        $html = $this->curl->get('http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp');
        $urlCaptcha = $this->transload->split_content('src="/tcnnt/captcha.png?', '"', $html);
        $this->curl->writefile(PATH_LOG . 'captcha/' . $session . '.png', $this->curl->get('http://tracuunnt.gdt.gov.vn/tcnnt/captcha.png?' . $urlCaptcha));
        return PATH_LOG . 'captcha/' . $session . '.png';
    }

    function getInformation($curl, $session, $mst, $captcha) {
        $this->data['check_error'] = -1;
        $obj = array(
            'action' => 'action',
            'id' => '',
            'page' => '1',
            'mst' => $mst,
            'fullname' => '',
            'address' => '',
            'cmt' => '',
            'captcha' => $captcha
        );
        $this->curl->setreferer('http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp');
        $htmlStep1 = $this->curl->post('http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp', http_build_query($obj));

        $temp = $this->transload->split_content('<table class="ta_border">', '</table>', $htmlStep1);
        $temp = $this->transload->split_content('<tr>', '</tr>', $temp);
        $temp = $this->transload->split_content('<td>', '</td>', $temp[1]);
        $info['congty'] = $this->transload->clearTag($temp[1]);
        $info['co_quan_thue'] = $this->transload->clearTag($temp[2]);
        $info['the_can_cuoc'] = $this->transload->clearTag($temp[3]);
        $info['ngay_thay_doi_thong_tin'] = $this->transload->clearTag($temp[4]);
        $info['ghi_chu'] = $this->transload->clearTag($temp[5]);



        if (!$info['congty']) {
            $this->data['check_error'] = 1;
            return false;
        }
        $obj = array(
            'action' => 'action',
            'id' => $mst,
            'page' => '1',
            'mst' => $mst,
            'fullname' => '',
            'address' => '',
            'cmt' => '',
            'captcha' => ''
        );


        $this->curl->setreferer('http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp');
        $htmlStep2 = $this->curl->post('http://tracuunnt.gdt.gov.vn/tcnnt/mstdn.jsp', http_build_query($obj));


        $info['ma_so_doanh_nghiep'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Mã số doanh nghiệp</th>', '</td>', $htmlStep2));
        $info['ngay_cap'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày cấp</td>', '</td>', $htmlStep2));
        $info['ngay_dong_mst'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày đóng MST</th>', '</td>', $htmlStep2));
        $info['ten_chinh_thuc'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Tên chính thức</th>', '</td>', $htmlStep2));
        $info['ten_giao_dich'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Tên giao dịch</td>', '</td>', $htmlStep2));
        $info['noi_dang_ky_quan_ly_thue'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Nơi đăng ký quản lý thuế</th>', '</td>', $htmlStep2));
        $info['dia_chi_tru_so'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Địa chỉ trụ sở</th>', '</td>', $htmlStep2));
        $info['noi_dang_ky_nop_thue'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Nơi đăng ký nộp thuế</th>', '</td>', $htmlStep2));
        $info['dia_chi_nhan_thong_bao_thue'] = $this->transload->clearTag($this->transload->split_content('th width="16%">Địa chỉ nhận thông báo thuế</th>', '</td>', $htmlStep2));
        $info['qdtl_ngay_cap'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">QĐTL-Ngày cấp</th>', '</td>', $htmlStep2));
        $info['co_quan_ra_quyet_dinh'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Cơ quan ra quyết định</th>', '</td>', $htmlStep2));
        $info['gpkd_ngay_cap'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">GPKD-Ngày cấp</th>', '</td>', $htmlStep2));
        $info['co_quan_cap'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Cơ quan cấp</th>', '</td>', $htmlStep2));
        $info['ngay_nhan_to_khai'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày nhận tờ khai</th>', '</td>', $htmlStep2));
        $info['bat_dau_nam_tai_chinh'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày/tháng bắt đầu <br>năm tài chính</th>', '</td>', $htmlStep2));
        $info['ket_thuc_nam_tai_chinh'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày/tháng kết thúc <br>năm tài chính</th>', '</td>', $htmlStep2));
        $info['ma_so_hien_thoi'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Mã số hiện thời</th>', '</td>', $htmlStep2));
        $info['ngay_bat_dau_hd'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Ngày bắt đầu HĐ</th>', '</td>', $htmlStep2));
        $info['chuong_khoang'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Chương – Khoản</th>', '</td>', $htmlStep2));
        $info['hinh_thuc_h_toan'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Hình thức h.toán</th>', '</td>', $htmlStep2));
        $info['pp_tinh_thue_gtgt'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">PP tính thuế GTGT</th>', '</td>', $htmlStep2));
        $info['chu_so_huu'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Chủ sở hữu/Người đại diện pháp luật</th>', '</td>', $htmlStep2));
        $info['dia_chi_chu_so_huu'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Địa chỉ chủ sở hữu/người đại diện pháp luật</th>', '</td>', $htmlStep2));
        $info['ten_giam_doc'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Tên giám đốc</th>', '</td>', $htmlStep2));
        $info['dia_chi_giam_doc'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Địa Chỉ</th>', '</td>', $htmlStep2)[0]);
        $info['ke_toan_truong'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Kế toán trưởng</th>', '</td>', $htmlStep2));
        $info['dia_chi_ket_toan_truong'] = $this->transload->clearTag($this->transload->split_content('<th width="16%">Địa Chỉ</th>', '</td>', $htmlStep2)[1]);

        return $info;
    }

    function getTrade($curl, $enterpriseID, $p = 0) {
        $post = array(
            'PageIndex' => $p,
            'EnterpriseID' => $enterpriseID
        );
        $result = array();
        $html = $this->curl->postJson('https://dichvuthongtin.dkkd.gov.vn/inf/Forms/Searches/EnterpriseInfo.aspx/LoadMore', json_encode($post));
        if ($html) {
            $json = json_decode($html, true);
            $data = $json['d'];
            if ($data) {
                $data = $this->transload->split_content('<tr>', '</tr>', $data);
                foreach ($data as $key => $value) {
                    $temp = $this->transload->split_content('<td>', '</td>', $value);
                    $result[trim(strip_tags($temp[0]))] = trim($temp[1]);
                }
                return $result;
            } else {
                return false;
            }
        }
    }

    function getInformationDKKD($curl, $session, $mst, $path) {
        $this->curl->setpathcookie($path . 'cookies/dkkd-' . $session . '.txt');
        $this->curl->setfollowlocation(true);
        $html = $this->curl->get('https://dichvuthongtin.dkkd.gov.vn/inf/default.aspx');
        $html = str_get_html($html);
        $form = $html->find('form', 0);

        if ($form) {
            $post = array();
            foreach ($form->find('input') as $input) {
                $post[$input->name] = $input->value;
                $post['ctl00$FldSearch'] = $mst;
            }

            $html = $this->curl->post('https://dichvuthongtin.dkkd.gov.vn/inf/' . $form->action, $post);
            $html = str_get_html($html);
            $form = $html->find('form', 0);
            if ($form) {
                $post = array();
                foreach ($form->find('input') as $input) {
                    if ($input->type == 'text' || $input->type == 'hidden') {
                        $post[$input->name] = $input->value;
                    }
                }
                $post['ctl00$SM'] = 'ctl00$C$UC_ENT_LIST1$CtlList$ctl02$Cmd1';
                $post['__EVENTTARGET'] = 'ctl00$C$UC_ENT_LIST1$CtlList$ctl02$Cmd1';
                $post['__ASYNCPOST'] = true;
                $html = $this->curl->post('https://dichvuthongtin.dkkd.gov.vn/inf/Forms/Searches/' . $form->action, $post);
                $hdf = $this->transload->split_content('id="ctl00_C_hdf" value="', '"', $html);
                $objInfo = array(
                    'ten_doanh_nghiep' => trim($this->transload->split_content('<span id="ctl00_C_NAMEFld" class="viewInput widthfull viewSearch">', '</span>', $html)),
                    'ten_doanh_nghiep_tieng_anh' => trim($this->transload->split_content('<span id="ctl00_C_NAME_FFld" class="viewInput widthfull viewSearch">', '</span>', $html)),
                    'ten_doanh_nghiep_viet_tat' => trim($this->transload->split_content('<span id="ctl00_C_SHORT_NAMEFld" class="viewInput widthfull viewSearch">', '</span>', $html)),
                    'tinh_trang' => trim($this->transload->split_content('<span id="ctl00_C_STATUSNAMEFld" class="viewInput viewSearch">', '</span>', $html)),
                    'ma_so_doanh_nghiep' => trim($this->transload->split_content('<span id="ctl00_C_ENTERPRISE_GDT_CODEFld" class="viewInput viewSearch">', '</span>', $html)),
                    'loai_hinh_phap_ly' => trim($this->transload->split_content('<span id="ctl00_C_ENTERPRISE_TYPEFld" class="viewInput viewSearch">', '</span>', $html)),
                    'ngay_thanh_lap' => trim($this->transload->split_content('<span id="ctl00_C_FOUNDING_DATE" class="viewInput widthfull viewSearch">', '</span>', $html)),
                    'dai_dien_phap_luat' => trim(strip_tags($this->transload->split_content('<span class="viewInput viewSearch">', '</span>', $html))),
                    'dia_chi_tru_so' => trim($this->transload->split_content('<span id="ctl00_C_HO_ADDRESS" class="viewInput widthfull viewSearch">', '</span>', $html))
                );
                if (strpos($html, 'ctl00_C_LinkDownload') !== false) {
                    $objHtml = str_get_html($html);
                    $form = $objHtml->find('form', 0);

                    if ($form) {
                        $post = array();
                        foreach ($form->find('input') as $input) {
                            if ($input->type == 'text' || $input->type == 'hidden') {
                                $post[$input->name] = $input->value;
                            }
                        }
                        $post['__EVENTTARGET'] = 'ctl00$C$LinkDownload';
                    }
                    $pdf = $this->curl->post('https://dichvuthongtin.dkkd.gov.vn/inf/Forms/Searches/' . $form->action, $post);

                    // echo $pdf;
                    //$rand = md5(microtime()) . '.pdf';
                    if (!preg_match('/Mẫu dấu/i', $pdf)) {
                        $filename_maudau = $mst . '.pdf';
                        $this->curl->writefile($path . 'pdf/' . $filename_maudau, $pdf);
                        $objInfo['mau_dau'] = $filename_maudau;
                    }
                }
                $objInfo['nganh_nghe_kinh_doanh'] = array();
                for ($i = 0; $i < 10; $i++) {
                    $trade = $this->getTrade($curl, $hdf, $i);
                    if ($trade) {
                        foreach ($trade as $k => $v) {
                            $objInfo['nganh_nghe_kinh_doanh'][$k] = $this->transload->split_content('<div>', '</div>', $v);
                        }
                    } else {
                        break;
                    }
                }
                $this->curl->setfollowlocation(false);
                return $objInfo;
            }
        } else {
            $this->curl->setfollowlocation(false);
            return false;
        }
    }

}
