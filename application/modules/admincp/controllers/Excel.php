<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Class Excel extends BACKEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library(array('PHPExcel', 'alias'));
    }

    function index() {
        if (isset($_POST['submit'])) {
            $error = 0;
            $filename = $_FILES["file"]['tmp_name'];
            //pre($filename);exit;
            $inputFileType = PHPExcel_IOFactory::identify($filename);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load("$filename");
            $total_sheets = $objPHPExcel->getSheetCount();
            $allSheetName = $objPHPExcel->getSheetNames();
            $arraydata = array();
            foreach ($allSheetName as $key => $table) {
                if ($this->db->table_exists($table)) {
                    $objWorksheet = $objPHPExcel->setActiveSheetIndex($key);
                    $highestRow = $objWorksheet->getHighestRow();
                    $highestColumn = $objWorksheet->getHighestColumn();
                    $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                    for ($row = 1; $row <= $highestRow; ++$row) {
                        if ($error == 1) {
                            break;
                        }
                        for ($col = 0; $col < $highestColumnIndex; ++$col) {
                            if ($row == 1) {
                                $field_name = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                                if ($this->db->field_exists($field_name, $table)) {
                                    $field[] = $field_name;
                                } else {
                                    $error = 1;
                                    $this->session->set_flashdata('message', 'Không tồn cột dữ liệu ' . $field_name . ' , vùi lòng xem lại cấu trúc thiết lập file excel');
                                    break;
                                }
                            } else {
                                if ($field[$col] == 'alias') {
                                    $value = $this->alias->create_alias($arraydata[$row - 2]['title']);
                                    $arraydata[$row - 2][$field[$col]] = $value;
                                } else {
                                    $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                                    $arraydata[$row - 2][$field[$col]] = $value;
                                }
                            }
                        }
                    }
                    if ($error == 0) {
                        $id = $this->db->insert_ignore_batch($table, $arraydata);
                        $this->session->set_flashdata('message', 'Bạn đã thêm thành công ' . $id . ' dòng dữ liệu');
                    }
                } else {
                    $this->session->set_flashdata('message', 'Không tồn tại bảng, vùi lòng xem lại cấu trúc thiết lập file excel');
                }
            }
        }
        $this->data['message'] = $this->session->flashdata('message');
        $this->template->write_view('content_block', 'excel/index', $this->data);
        $this->template->render();
    }

    function mobile() {
        $path = PATH_UPLOAD . '/phone/visa24hcantho.xls';
        $inputFileType = PHPExcel_IOFactory::identify($filename);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load("$filename");
        $total_sheets = $objPHPExcel->getSheetCount();
        $allSheetName = $objPHPExcel->getSheetNames();
        $arraydata = array();
        foreach ($allSheetName as $key => $table) {
            if ($this->db->table_exists($table)) {
                $objWorksheet = $objPHPExcel->setActiveSheetIndex($key);
                $highestRow = $objWorksheet->getHighestRow();
                $highestColumn = $objWorksheet->getHighestColumn();
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);
                for ($row = 1; $row <= $highestRow; ++$row) {
                    if ($error == 1) {
                        break;
                    }
                    for ($col = 0; $col < $highestColumnIndex; ++$col) {
                        if ($row == 1) {
                            $field_name = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                            if ($this->db->field_exists($field_name, $table)) {
                                $field[] = $field_name;
                            } else {
                                $error = 1;
                                $this->session->set_flashdata('message', 'Không tồn cột dữ liệu ' . $field_name . ' , vùi lòng xem lại cấu trúc thiết lập file excel');
                                break;
                            }
                        } else {
                            if ($field[$col] == 'alias') {
                                $value = $this->alias->create_alias($arraydata[$row - 2]['title']);
                                $arraydata[$row - 2][$field[$col]] = $value;
                            } else {
                                $value = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                                $arraydata[$row - 2][$field[$col]] = $value;
                            }
                        }
                    }
                }
                if ($error == 0) {
                    pre($arraydata);
//$id = $this->db->insert_ignore_batch($table, $arraydata);
                    $this->session->set_flashdata('message', 'Bạn đã thêm thành công ' . $id . ' dòng dữ liệu');
                }
            } else {
                $this->session->set_flashdata('message', 'Không tồn tại bảng, vùi lòng xem lại cấu trúc thiết lập file excel');
            }
        }

        $this->data['message'] = $this->session->flashdata('message');
        $this->template->write_view('content_block', 'excel/index', $this->data);
        $this->template->render();
    }

}
