<?php 
require_once 'vendor/autoload.php';
use mikehaertl\pdftk\Pdf;

function fillFormPdf($source,$params,$outputPath){

    $pdf = new Pdf($source,[
        //'command' => '/usr/bin/pdftk',
		'command' => 'C:\Program Files (x86)\PDFtk Server\bin\pdftk.exe',
        'useExec' => true,
    ]);

    $pdf->fillForm($params)
        -> needAppearances()
        ->saveAs($outputPath);

    if($pdf->getError()){
		echo $pdf->getError();
		return false;
	}
    return true;
}
function getDataPdf($path){

    // Get form data fields
    $pdf = new Pdf($path);
    $data = $pdf->getDataFields();

    if($pdf->getError()){
		echo $pdf->getError();
		return false;
	}
    return $data;
}