<?php

class Curl {

    var $core;
    var $header = FALSE;
    var $useragent;
    var $referer = FALSE;
    var $followlocation;
    var $ssl = FALSE;
    var $pathcookie;
    protected $CI;

    public function __construct() {
        $this->useragent = $_SERVER['HTTP_USER_AGENT'];
    }

    public function postJson($url, $vars) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, $this->header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17');
        curl_setopt($ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->followlocation);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->pathcookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->pathcookie);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8')
        );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data) {
            if ($this->callback) {
                $callback = $this->callback;
                $this->callback = FALSE;
                return call_user_func($callback, $data);
            } else {
                return $data;
            }
        } else {
            return @curl_error($ch);
        }
    }

    private function request($method, $url, $vars) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, $this->header);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/601.5.17 (KHTML, like Gecko) Version/9.1 Safari/601.5.17');
        curl_setopt($ch, CURLOPT_REFERER, $this->referer);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->followlocation);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->ssl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $this->pathcookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $this->pathcookie);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                //'Accept: Accept=text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                //'Accept-Encoding: gzip, deflate',
                //'Accept-Language: en-US,en;q=0.5',
                //'Cache-Control: no-cache',
                //'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                //'UPGRADE-INSECURE-REQUESTS: 1'
        ));
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $vars);
        }
        $data = curl_exec($ch);
        curl_close($ch);
        if ($data) {
            if ($this->callback) {
                $callback = $this->callback;
                $this->callback = FALSE;
                return call_user_func($callback, $data);
            } else {
                return $data;
            }
        } else {
            return @curl_error($ch);
        }
    }

    public function setheader($boolean = TRUE) {
        if ($boolean) {
            $this->header = TRUE;
        } else {
            $this->header = FALSE;
        }
        return TRUE;
    }

    public function setuseragent($agent = FALSE) {
        if ($agent) {
            $this->useragent = $agent;
        } else {
            $this->useragent = '';
        }
        return TRUE;
    }

    public function setreferer($referer = FALSE) {
        if ($referer) {
            $this->referer = $referer;
        } else {
            $this->referer = '';
        }
        return TRUE;
    }

    public function setfollowlocation($boolean = FALSE) {
        if ($boolean) {
            $this->followlocation = TRUE;
        } else {
            $this->followlocation = FALSE;
        }
        return TRUE;
    }

    public function setssl($boolean = TRUE) {
        if ($boolean) {
            $this->ssl = FALSE;
        } else {
            $this->ssl = TRUE;
        }
        return TRUE;
    }

    public function setpathcookie($path) {
        if (empty($path)) {
            $this->pathcookie = FALSE;
            return FALSE;
        }
        $this->pathcookie = $path;
        return TRUE;
    }

    public function get($url) {
        if (empty($url)) {
            return FALSE;
        }
        $html = $this->request('GET', $url, 'NULL');
        return $html;
    }

    public function post($url, $vars) {
        if (empty($url) || empty($vars)) {
            return FALSE;
        }
        $html = $this->request('POST', $url, $vars);
        return $html;
    }

    public function tranload($pathFileName, $linktranload) {
        $handle = @fopen($linktranload, "rb");
        $contents = @stream_get_contents($handle);
        @fclose($handle);
        $f2 = @fopen($pathFileName, "w");
        @fwrite($f2, $contents);
        @fclose($f2);
        if (file_exists($pathFileName) && filesize($pathFileName)) {
            return filesize($pathFileName);
        } else {
            @unlink($pathFileName);
            return FALSE;
        }
    }

    public function writefile($filename, $content) {
        $fh = fopen($filename, "wb");
        fwrite($fh, $content);
        fclose($fh);
    }

    public function get_url($url, $parram) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parram);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function get_dev_url($url, $parram) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch, CURLOPT_POSTFIELDS, $parram);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        $data = curl_exec($ch);
        pre($data);
        curl_close($ch);
        return $data;
    }

}

?>