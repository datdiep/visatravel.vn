<?php

class Vtc {

    private $secret_key = SECRET_KEY_VTC;
    private $websiteid = 2378;
    private $receiver_acc = "0943000300";
    private $curency = 1;
    private $returnurl = 'http://sanquatang.com/order/vtc_payment';

    function doRequest($order) {        
        $destinationUrl = "https://pay.vtc.vn/cong-thanh-toan/checkout.html";
        //new version
        $plaintext = $this->websiteid . "-" . $this->curency . "-" . $order['id'] . "-" . $order['total'] . "-" . $this->receiver_acc . '--' . $this->secret_key . "-" . $this->returnurl;
        $sign = strtoupper(hash('sha256', $plaintext));
        $data = "?website_id=" . $this->websiteid . "&payment_method=" . $this->curency . "&order_code=" . $order['id'] . "&amount=" . $order['total'] . "&receiver_acc=" . $this->receiver_acc . "&urlreturn=" . $this->returnurl;
        $data = $data . "&customer_first_name=" . htmlentities($order['fullname_giver']) . "&customer_last_name=" . "&customer_mobile=" . $order["phone_giver"] . "&bill_to_address_line1=" . htmlentities($order['address_giver']) . "&bill_to_address_line2=" . "&city_name=" . htmlentities($order['city']) . "&address_country=vn" . "&customer_email=" . "&order_des=sanquatang" . "&sign=" . $sign;
        $destinationUrl = $destinationUrl . $data;
        return $destinationUrl;
    }

}