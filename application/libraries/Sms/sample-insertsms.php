<?php
class Neo{
	
}
/**
 * Test with InsertSMS for 'http://g3g4.vn:8008/smsws/services/SendMT?wsdl'
 * @package InsertSMS
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
ini_set('memory_limit', '512M');
ini_set('display_errors', true);
error_reporting(-1);
/**
 * Load autoload
 */
require_once dirname(__FILE__) . '/InsertSMSAutoload.php';
/**
 * Wsdl instanciation infos. By default, nothing has to be set.
 * If you wish to override the SoapClient's options, please refer to the sample below.
 * 
 * This is an associative array as:
 * - the key must be a InsertSMSWsdlClass constant beginning with WSDL_
 * - the value must be the corresponding key value
 * Each option matches the {@link http://www.php.net/manual/en/soapclient.soapclient.php} options
 * 
 * Here is below an example of how you can set the array:
 * $wsdl = array();
 * $wsdl[InsertSMSWsdlClass::WSDL_URL] = 'http://g3g4.vn:8008/smsws/services/SendMT?wsdl';
 * $wsdl[InsertSMSWsdlClass::WSDL_CACHE_WSDL] = WSDL_CACHE_NONE;
 * $wsdl[InsertSMSWsdlClass::WSDL_TRACE] = true;
 * $wsdl[InsertSMSWsdlClass::WSDL_LOGIN] = 'myLogin';
 * $wsdl[InsertSMSWsdlClass::WSDL_PASSWD] = '**********';
 * etc....
 * Then instantiate the Service class as: 
 * - $wsdlObject = new InsertSMSWsdlClass($wsdl);
 */
/**
 * Examples
 */
/* * **********************************
 * Example for InsertSMSServiceInsert
 */
$insertSMSServiceInsert = new InsertSMSServiceInsert();
// sample call for InsertSMSServiceInsert::insertSMS()
if ($insertSMSServiceInsert->insertSMS(new InsertSMSStructInsertSMS("sonachau", "123456", "0967686101", "Thu nghiem sms CongVanNhapCanh.net. Hoan cui bap", 2, "NEOJSC", 0, "123"))) {
    echo '<pre>';
    print_r($insertSMSServiceInsert->getResult());
} else {
    echo '<pre>';
    print_r($insertSMSServiceInsert->getLastError());
}
    