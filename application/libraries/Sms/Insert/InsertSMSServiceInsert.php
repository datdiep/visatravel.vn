<?php
/**
 * File for class InsertSMSServiceInsert
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSServiceInsert originally named Insert
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSServiceInsert extends InsertSMSWsdlClass
{
    /**
     * Method to call the operation originally named insertSMS
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructInsertSMS $_insertSMSStructInsertSMS
     * @return InsertSMSStructInsertSMSResponse
     */
    public function insertSMS(InsertSMSStructInsertSMS $_insertSMSStructInsertSMS)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->insertSMS($_insertSMSStructInsertSMS));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see InsertSMSWsdlClass::getResult()
     * @return InsertSMSStructInsertSMSResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
