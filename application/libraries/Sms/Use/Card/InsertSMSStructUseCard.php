<?php
/**
 * File for class InsertSMSStructUseCard
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSStructUseCard originally named useCard
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://g3g4.vn:8008/smsws/services/SendMT?wsdl}
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSStructUseCard extends InsertSMSWsdlClass
{
    /**
     * The username
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $username;
    /**
     * The password
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $password;
    /**
     * The issure
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $issure;
    /**
     * The cardCode
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $cardCode;
    /**
     * The cardSerial
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $cardSerial;
    /**
     * Constructor method for useCard
     * @see parent::__construct()
     * @param string $_username
     * @param string $_password
     * @param string $_issure
     * @param string $_cardCode
     * @param string $_cardSerial
     * @return InsertSMSStructUseCard
     */
    public function __construct($_username = NULL,$_password = NULL,$_issure = NULL,$_cardCode = NULL,$_cardSerial = NULL)
    {
        parent::__construct(array('username'=>$_username,'password'=>$_password,'issure'=>$_issure,'cardCode'=>$_cardCode,'cardSerial'=>$_cardSerial),false);
    }
    /**
     * Get username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set username value
     * @param string $_username the username
     * @return string
     */
    public function setUsername($_username)
    {
        return ($this->username = $_username);
    }
    /**
     * Get password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set password value
     * @param string $_password the password
     * @return string
     */
    public function setPassword($_password)
    {
        return ($this->password = $_password);
    }
    /**
     * Get issure value
     * @return string|null
     */
    public function getIssure()
    {
        return $this->issure;
    }
    /**
     * Set issure value
     * @param string $_issure the issure
     * @return string
     */
    public function setIssure($_issure)
    {
        return ($this->issure = $_issure);
    }
    /**
     * Get cardCode value
     * @return string|null
     */
    public function getCardCode()
    {
        return $this->cardCode;
    }
    /**
     * Set cardCode value
     * @param string $_cardCode the cardCode
     * @return string
     */
    public function setCardCode($_cardCode)
    {
        return ($this->cardCode = $_cardCode);
    }
    /**
     * Get cardSerial value
     * @return string|null
     */
    public function getCardSerial()
    {
        return $this->cardSerial;
    }
    /**
     * Set cardSerial value
     * @param string $_cardSerial the cardSerial
     * @return string
     */
    public function setCardSerial($_cardSerial)
    {
        return ($this->cardSerial = $_cardSerial);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see InsertSMSWsdlClass::__set_state()
     * @uses InsertSMSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return InsertSMSStructUseCard
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
