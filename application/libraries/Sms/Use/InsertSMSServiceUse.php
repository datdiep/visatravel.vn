<?php
/**
 * File for class InsertSMSServiceUse
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSServiceUse originally named Use
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSServiceUse extends InsertSMSWsdlClass
{
    /**
     * Method to call the operation originally named useCard
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructUseCard $_insertSMSStructUseCard
     * @return InsertSMSStructUseCardResponse
     */
    public function useCard(InsertSMSStructUseCard $_insertSMSStructUseCard)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->useCard($_insertSMSStructUseCard));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see InsertSMSWsdlClass::getResult()
     * @return InsertSMSStructUseCardResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
