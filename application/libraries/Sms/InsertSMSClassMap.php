<?php
/**
 * File for the class which returns the class map definition
 * @package InsertSMS
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * Class which returns the class map definition by the static method InsertSMSClassMap::classMap()
 * @package InsertSMS
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSClassMap
{
    /**
     * This method returns the array containing the mapping between WSDL structs and generated classes
     * This array is sent to the SoapClient when calling the WS
     * @return array
     */
    final public static function classMap()
    {
        return array (
  'insertSMS' => 'InsertSMSStructInsertSMS',
  'insertSMSResponse' => 'InsertSMSStructInsertSMSResponse',
  'sendFromBrandname' => 'InsertSMSStructSendFromBrandname',
  'sendFromBrandnameResponse' => 'InsertSMSStructSendFromBrandnameResponse',
  'sendFromServiceNumber' => 'InsertSMSStructSendFromServiceNumber',
  'sendFromServiceNumberResponse' => 'InsertSMSStructSendFromServiceNumberResponse',
  'sendMessage' => 'InsertSMSStructSendMessage',
  'sendMessageResponse' => 'InsertSMSStructSendMessageResponse',
  'sendSMS' => 'InsertSMSStructSendSMS',
  'sendSMSResponse' => 'InsertSMSStructSendSMSResponse',
  'useCard' => 'InsertSMSStructUseCard',
  'useCardResponse' => 'InsertSMSStructUseCardResponse',
);
    }
}
