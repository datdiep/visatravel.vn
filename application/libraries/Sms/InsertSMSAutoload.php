<?php

/**
 * File to load generated classes once at once time
 * @package InsertSMS
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * Includes for all generated classes files
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
require_once dirname(__FILE__) . '/InsertSMSWsdlClass.php';
require_once dirname(__FILE__) . '/Send/Brandname/InsertSMSStructSendFromBrandname.php';
require_once dirname(__FILE__) . '/Send/Response/InsertSMSStructSendFromServiceNumberResponse.php';
require_once dirname(__FILE__) . '/Send/Response/InsertSMSStructSendFromBrandnameResponse.php';
require_once dirname(__FILE__) . '/Insert/SMS/InsertSMSStructInsertSMS.php';
require_once dirname(__FILE__) . '/Insert/Response/InsertSMSStructInsertSMSResponse.php';
require_once dirname(__FILE__) . '/Send/Number/InsertSMSStructSendFromServiceNumber.php';
require_once dirname(__FILE__) . '/Send/Response/InsertSMSStructSendMessageResponse.php';
require_once dirname(__FILE__) . '/Use/Response/InsertSMSStructUseCardResponse.php';
require_once dirname(__FILE__) . '/Send/SMS/InsertSMSStructSendSMS.php';
require_once dirname(__FILE__) . '/Send/Response/InsertSMSStructSendSMSResponse.php';
require_once dirname(__FILE__) . '/Send/Message/InsertSMSStructSendMessage.php';
require_once dirname(__FILE__) . '/Use/Card/InsertSMSStructUseCard.php';
require_once dirname(__FILE__) . '/Insert/InsertSMSServiceInsert.php';
require_once dirname(__FILE__) . '/Send/InsertSMSServiceSend.php';
require_once dirname(__FILE__) . '/Use/InsertSMSServiceUse.php';
require_once dirname(__FILE__) . '/InsertSMSClassMap.php';
