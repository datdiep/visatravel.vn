<?php
/**
 * File for class InsertSMSServiceSend
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSServiceSend originally named Send
 * @package InsertSMS
 * @subpackage Services
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSServiceSend extends InsertSMSWsdlClass
{
    /**
     * Method to call the operation originally named sendMessage
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructSendMessage $_insertSMSStructSendMessage
     * @return InsertSMSStructSendMessageResponse
     */
    public function sendMessage(InsertSMSStructSendMessage $_insertSMSStructSendMessage)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->sendMessage($_insertSMSStructSendMessage));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named sendFromServiceNumber
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructSendFromServiceNumber $_insertSMSStructSendFromServiceNumber
     * @return InsertSMSStructSendFromServiceNumberResponse
     */
    public function sendFromServiceNumber(InsertSMSStructSendFromServiceNumber $_insertSMSStructSendFromServiceNumber)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->sendFromServiceNumber($_insertSMSStructSendFromServiceNumber));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named sendFromBrandname
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructSendFromBrandname $_insertSMSStructSendFromBrandname
     * @return InsertSMSStructSendFromBrandnameResponse
     */
    public function sendFromBrandname(InsertSMSStructSendFromBrandname $_insertSMSStructSendFromBrandname)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->sendFromBrandname($_insertSMSStructSendFromBrandname));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Method to call the operation originally named sendSMS
     * @uses InsertSMSWsdlClass::getSoapClient()
     * @uses InsertSMSWsdlClass::setResult()
     * @uses InsertSMSWsdlClass::saveLastError()
     * @param InsertSMSStructSendSMS $_insertSMSStructSendSMS
     * @return InsertSMSStructSendSMSResponse
     */
    public function sendSMS(InsertSMSStructSendSMS $_insertSMSStructSendSMS)
    {
        try
        {
            return $this->setResult(self::getSoapClient()->sendSMS($_insertSMSStructSendSMS));
        }
        catch(SoapFault $soapFault)
        {
            return !$this->saveLastError(__METHOD__,$soapFault);
        }
    }
    /**
     * Returns the result
     * @see InsertSMSWsdlClass::getResult()
     * @return InsertSMSStructSendFromBrandnameResponse|InsertSMSStructSendFromServiceNumberResponse|InsertSMSStructSendMessageResponse|InsertSMSStructSendSMSResponse
     */
    public function getResult()
    {
        return parent::getResult();
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
