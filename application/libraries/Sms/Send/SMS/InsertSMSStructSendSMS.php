<?php
/**
 * File for class InsertSMSStructSendSMS
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSStructSendSMS originally named sendSMS
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://g3g4.vn:8008/smsws/services/SendMT?wsdl}
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSStructSendSMS extends InsertSMSWsdlClass
{
    /**
     * The username
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $username;
    /**
     * The password
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $password;
    /**
     * The receiver
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $receiver;
    /**
     * The content
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $content;
    /**
     * The loaisp
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * @var int
     */
    public $loaisp;
    /**
     * The brandname
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $brandname;
    /**
     * The target
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $target;
    /**
     * Constructor method for sendSMS
     * @see parent::__construct()
     * @param string $_username
     * @param string $_password
     * @param string $_receiver
     * @param string $_content
     * @param int $_loaisp
     * @param string $_brandname
     * @param string $_target
     * @return InsertSMSStructSendSMS
     */
    public function __construct($_username = NULL,$_password = NULL,$_receiver = NULL,$_content = NULL,$_loaisp = NULL,$_brandname = NULL,$_target = NULL)
    {
        parent::__construct(array('username'=>$_username,'password'=>$_password,'receiver'=>$_receiver,'content'=>$_content,'loaisp'=>$_loaisp,'brandname'=>$_brandname,'target'=>$_target),false);
    }
    /**
     * Get username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set username value
     * @param string $_username the username
     * @return string
     */
    public function setUsername($_username)
    {
        return ($this->username = $_username);
    }
    /**
     * Get password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set password value
     * @param string $_password the password
     * @return string
     */
    public function setPassword($_password)
    {
        return ($this->password = $_password);
    }
    /**
     * Get receiver value
     * @return string|null
     */
    public function getReceiver()
    {
        return $this->receiver;
    }
    /**
     * Set receiver value
     * @param string $_receiver the receiver
     * @return string
     */
    public function setReceiver($_receiver)
    {
        return ($this->receiver = $_receiver);
    }
    /**
     * Get content value
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * Set content value
     * @param string $_content the content
     * @return string
     */
    public function setContent($_content)
    {
        return ($this->content = $_content);
    }
    /**
     * Get loaisp value
     * @return int|null
     */
    public function getLoaisp()
    {
        return $this->loaisp;
    }
    /**
     * Set loaisp value
     * @param int $_loaisp the loaisp
     * @return int
     */
    public function setLoaisp($_loaisp)
    {
        return ($this->loaisp = $_loaisp);
    }
    /**
     * Get brandname value
     * @return string|null
     */
    public function getBrandname()
    {
        return $this->brandname;
    }
    /**
     * Set brandname value
     * @param string $_brandname the brandname
     * @return string
     */
    public function setBrandname($_brandname)
    {
        return ($this->brandname = $_brandname);
    }
    /**
     * Get target value
     * @return string|null
     */
    public function getTarget()
    {
        return $this->target;
    }
    /**
     * Set target value
     * @param string $_target the target
     * @return string
     */
    public function setTarget($_target)
    {
        return ($this->target = $_target);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see InsertSMSWsdlClass::__set_state()
     * @uses InsertSMSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return InsertSMSStructSendSMS
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
