<?php
/**
 * File for class InsertSMSStructSendSMSResponse
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
/**
 * This class stands for InsertSMSStructSendSMSResponse originally named sendSMSResponse
 * Meta informations extracted from the WSDL
 * - from schema : {@link http://g3g4.vn:8008/smsws/services/SendMT?wsdl}
 * @package InsertSMS
 * @subpackage Structs
 * @author WsdlToPhp Team <contact@wsdltophp.com>
 * @version 20150429-01
 * @date 2017-04-17
 */
class InsertSMSStructSendSMSResponse extends InsertSMSWsdlClass
{
    /**
     * The return
     * Meta informations extracted from the WSDL
     * - minOccurs : 0
     * - nillable : true
     * @var string
     */
    public $return;
    /**
     * Constructor method for sendSMSResponse
     * @see parent::__construct()
     * @param string $_return
     * @return InsertSMSStructSendSMSResponse
     */
    public function __construct($_return = NULL)
    {
        parent::__construct(array('return'=>$_return),false);
    }
    /**
     * Get return value
     * @return string|null
     */
    public function getReturn()
    {
        return $this->return;
    }
    /**
     * Set return value
     * @param string $_return the return
     * @return string
     */
    public function setReturn($_return)
    {
        return ($this->return = $_return);
    }
    /**
     * Method called when an object has been exported with var_export() functions
     * It allows to return an object instantiated with the values
     * @see InsertSMSWsdlClass::__set_state()
     * @uses InsertSMSWsdlClass::__set_state()
     * @param array $_array the exported values
     * @return InsertSMSStructSendSMSResponse
     */
    public static function __set_state(array $_array,$_className = __CLASS__)
    {
        return parent::__set_state($_array,$_className);
    }
    /**
     * Method returning the class name
     * @return string __CLASS__
     */
    public function __toString()
    {
        return __CLASS__;
    }
}
