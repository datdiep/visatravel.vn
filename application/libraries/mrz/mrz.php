<?php


class mrz {
	private $config;
	function __construct($config) {
       	require_once(realpath(dirname(__FILE__)) . '/index.php');
		require_once (realpath(dirname(__FILE__)) . '/class.image.php');
		$this->config = $config;

   }

	function run($pathMrzImage){
		$path = $this->config['pathImage'];
		$pathSourceImage =  $pathMrzImage;
		$rand = md5(microtime()).'.png';
		$pathImageDestination = $path . 'temp/' . $rand;
		$this->processImage($pathSourceImage,$pathImageDestination,false);
		$mrz = $this->detectMrz($pathImageDestination); //$mrz =false;
		if($mrz == false){
			$this->processImage($pathSourceImage,$pathImageDestination,true);
			$mrz = $this->detectMrz($pathImageDestination);
		}
		@unlink($pathImageDestination);
		if($mrz == false){
			return false;
		}else{
			return $mrz;
		}
	}

	function processImage($pathImageSource,$pathImageDestination,$grayScale = false){
		// exec("/usr/local/bin/mrz '{$pathImageSource}' -r '$pathImageDestination'",$data);
		// print_r($data);
		// $im = imagecreatefromfile($pathImageDestination);
		// return ;
		// exit;
		$imageEngine = new image();
		$im = $imageEngine->fix_orientation($pathImageSource);

		if(is_resource($im)){
			$im2 = imagecrop($im, ['x' => 0, 'y' => imagesy($im)/4*3, 'width' => imagesx($im), 'height' => imagesy($im)-(imagesy($im)/4*3)]);

			if ($im2 !== FALSE) {
				if($grayScale === true){
					imagefilter($im2, IMG_FILTER_GRAYSCALE);
					imagefilter($im2, IMG_FILTER_CONTRAST,-100);
				}
			    imagepng($im2, $pathImageDestination);
			    $imageEngine->resize((imagesx($im2)*(1000*100/imagesx($im2)))/100, (imagesy($im2)*(1000*100/imagesx($im2)))/100, $pathImageDestination, $pathImageDestination, false);
			    imagedestroy($im2);
			}
			imagedestroy($im);
		}else{
			return false;
		}

	}


	function clearMrz($mrzString){
		//$mrzString = str_replace('<K<','<<<',$mrzString);
		return $mrzString;
	}
	function validMrzLine($mrz){
		$date = substr($mrz[1],13,6);
		if(is_numeric($date) === false){
			$dateFix = str_replace('Z','2',$date);
			$mrz[1] = substr_replace($mrz[1],$dateFix,13,6);
		}

		$pId = substr($mrz[1],1,5);
		if(is_numeric($pId) === false){
			$pIdFix = str_replace('Z','2',$pId);
			$mrz[1] = substr_replace($mrz[1],$pIdFix,1,5);
		}
		return $mrz;
	}

	function detectMrz($pathMrzImage){
		exec($this->config['pathTesseract'] . " '" . $pathMrzImage . "' stdout -l ocrb",$response);
//print_r($response);exit;
		$filter = array();
		$mrz = '';
		$tempLine = '';
		foreach ($response as $key => $value) {
			if(strpos($value,'<') !== FALSE){
				$tempLine = str_replace(' ','',$value);
				$tempLine = str_replace('.','',$tempLine);
				$tempLine = str_replace('/','',$tempLine);
				$tempLine = str_replace('-','',$tempLine);
				if(strpos($tempLine,'P<') !== false){
					$tempLine = substr($tempLine,strpos($tempLine,'P<'),strlen($tempLine));
				}

				//echo substr($tempLine,-2);
				$filter[] = substr($tempLine,0,44);
				//$filter[] = $tempLine;

			}
		}
		if ($this->config['debug'] === true){
			print_r($filter);
		}
		
		if(count($filter) == 2){
			if(strlen($filter[0]) == 44 && strlen($filter[1]) == 44){
				if(substr($filter[0],0,2) == 'P<'){
					$filter = $this->validMrzLine($filter);
					$mrz = implode('',$filter);
				}else{

				}
			}
		}elseif(count($filter) == 4){
			if(substr($filter[2],0,2) == 'P<'){
				$mrzTemp = array(
					$filter[2].$filter[0],
					$filter[3].$filter[1],
				);
				$mrzTemp = $this->validMrzLine($mrzTemp);
				$mrz = implode('',$mrzTemp);
				$mrz = $this->clearMrz($mrz);
				if($this->config['debug'] === true){
					echo $mrz;
				}
			}else{
				$filterTemp = array();
				foreach ($filter as $keyTemp => $valueTemp) {
					if(strlen($valueTemp) == 44){
						$filterTemp[] = $valueTemp;
					}
				}
				$filter = $filterTemp;
				if(strlen($filter[0]) == 44 && strlen($filter[1]) == 44){
					if(substr($filter[0],0,2) == 'P<'){
						$filter = validMrzLine($filter);
						$mrz = implode('',$filter);
					}else{

					}
				}
			}
		}elseif(count($filter) == 3){
			if(substr($filter[1],0,2) == 'P<'){
				if(str_replace('<','',$filter[0]) == ''){
					$mrzTemp = array(
						$filter[1].$filter[0],
						$filter[2],
					);
					$mrzTemp = validMrzLine($mrzTemp);
					$mrz = implode('',$mrzTemp);
					if($this->config['debug'] === true){
						echo $mrz;
					}
				}
			}else{

			}
		}else{
			$filterTemp = array();
			foreach ($filter as $keyTemp => $valueTemp) {
				if(strlen($valueTemp) == 44){
					$filterTemp[] = $valueTemp;
				}
			}
			$filter = $filterTemp;
			if(strlen($filter[0]) == 44 && strlen($filter[1]) == 44){
				if(substr($filter[0],0,2) == 'P<'){
					$filter = $this->validMrzLine($filter);
					$mrz = implode('',$filter);
				}else{

				}
			}
		}

		if($mrz){
			$parser = new Deft\MrzParser\MrzParser();
			$travelDocument = $parser->parseString($mrz);
			if ($this->config['debug'] === true){
				print_r($travelDocument);
			}
			if($travelDocument->getDateOfExpiry() == false || $travelDocument->getDateOfBirth() == false ){
				return false;
			}
			// TODO: kiem tra xac nhan ngay thang
			$result = array(
				'type' => $travelDocument->getType(),
				'getDocumentNumber' => $travelDocument->getDocumentNumber(),
				'getIssuingCountry' => $travelDocument->getIssuingCountry(),
				'getDateOfExpiry' => $travelDocument->getDateOfExpiry(),
				'getPrimaryIdentifier' => $travelDocument->getPrimaryIdentifier(),
				'getSecondaryIdentifier' => $travelDocument->getSecondaryIdentifier(),
				'getSex' => $travelDocument->getSex(),
				'getDateOfBirth' => $travelDocument->getDateOfBirth(),
				'getNationality' => $travelDocument->getNationality(),
				'getPersonalNumber' => $travelDocument->getPersonalNumber()
			);


			return $result;
		}
		return false;
	}
}

?>