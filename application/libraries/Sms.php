<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sms {

    function __construct() {
        $this->CI = $this->CI = & get_instance();
    }

    function send($phone_receiver, $content) {

        include('Sms/InsertSMSAutoload.php');

        $insertSMSServiceInsert = new InsertSMSServiceInsert();


        // sample call for InsertSMSServiceInsert::insertSMS()
        if ($insertSMSServiceInsert->insertSMS(new InsertSMSStructInsertSMS("sonachau", "123456", $phone_receiver, $content, 2, "VisaTop", 1, "123"))) {
            $data = $insertSMSServiceInsert->getResult();
        } else {
            $data = $insertSMSServiceInsert->getLastError();
        }

        return $data->return;
    }

}
