<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class paypal {

    private $returnurl, $cancelurl;

    public function __construct($config = array()) {
        // $this->CI = & get_instance();
        //sandbox/*
        $this->baseurl = 'https://api-3t.sandbox.paypal.com/nvp'; //sandbox
//        $this->username = urlencode('usddat-facilitator_api1.gmail.com');
//        $this->password = urlencode('1401437632');
//        $this->signature = urlencode('AZcUgoXOPR94Ozc.sRkS0dQ35cQTApNMJQl6TyY1DmKfV4XAhBU5HL81');
//        $this->redirecturl = "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="; //sandbox*/
        //live        
        $this->baseurl = 'https://api-3t.paypal.com/nvp'; //live  
        $this->username = urlencode(PAYPAL_USERNAME);
        $this->password = urlencode(PAYPAL_PASSOWRD);
        $this->signature = urlencode(PAYPAL_SIGNATURE);
        $this->redirecturl = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token="; //live

        $this->returnurl = base_url() . 'index/process_paypal/?act=success'; // where the user is sent upon successful completion
        $this->cancelurl = base_url() . 'index/process_paypal/?act=cancel'; // where the user is sent upon canceling the transaction
        //$this->redirecturl = "https://www.paypal.com/incontext?token="; 	
    }

    public function doPayment($money, $order_id) {
        $this->returnurl .= "&order_id=" . $order_id . "&money=" . $money;
        $this->cancelurl .= "&order_id=" . $order_id . "&money=" . $money;
        $result = $this->sendRequest($money, "Invoice%20Payment%20for%20#" . $order_id);
        //echo '<pre>';print_r($result);exit;
        if ($result['ACK']) {
            $token = (!empty($result['TOKEN'])) ? $result['TOKEN'] : '';
            $test = "order_id => $order_id,";
            foreach ($result as $k => $value) {
                $test.= $k . '=>' . $value . ',';
            }
//            $dir = PATH_ASSETS . 'logs/payment/paypal/' . date('m-Y') . '/';
//            @mkdir($dir);
            file_put_contents('doconfirm.txt', date('d-m-Y H:i:s ') . $test . "\n", FILE_APPEND);
            $data['result'] = $result;
            $data['status'] = 0;
            $data['link'] = $this->redirecturl . $token;
            return $data;
        } else {
            $data['status'] = 1;
        }
    }

    public function doConfirm($money, $token, $payerid) {
        $result = $this->sendRespond($money, "Thanh%20toan%20hoa%20don", $token, $payerid);
//        $result = $this->GetExpressCheckoutDetails($money, "Nap%20tien%20bom", $token, $payerid);
        $test = '';
        foreach ($result as $k => $value) {
            $test.= $k . '=>' . $value . ',';
        }
        file_put_contents('doconfirm.txt', date('d-m-Y H:i:s ') . $test . "\n", FILE_APPEND);
        return $result;
    }

    private function sendRequest($money, $nameitem) {
        $post[] = "USER=$this->username";
        $post[] = "PWD=$this->password";
        $post[] = "SIGNATURE=$this->signature";
        $post[] = "VERSION=65.1"; // very important!
        $post[] = "PAYMENTREQUEST_0_CURRENCYCODE=USD";
        $post[] = "PAYMENTREQUEST_0_AMT=$money";
        $post[] = "PAYMENTREQUEST_0_ITEMAMT=$money";
        $post[] = "PAYMENTREQUEST_0_PAYMENTACTION=Sale"; // do not alter
        $post[] = "L_PAYMENTREQUEST_0_NAME0=$nameitem"; // use %20 for spaces
//        $post[] = "L_PAYMENTREQUEST_0_ITEMCATEGORY0=Digital"; // do not alter
        $post[] = "L_PAYMENTREQUEST_0_QTY0=1";
        $post[] = "L_PAYMENTREQUEST_0_AMT0=$money";
        $post['returnurl'] = "RETURNURL=" . urlencode($this->returnurl); // do not alter
        $post['cancelurl'] = "CANCELURL=" . urlencode($this->cancelurl); // do not alter
        $post['method'] = "METHOD=SetExpressCheckout"; // do not alter
        $post_str = implode('&', $post);
        $output_str = $this->CurlMePost($this->baseurl, $post_str);
        parse_str($output_str, $output_array);
        return $output_array;
    }

    private function GetExpressCheckoutDetails($money, $nameitem, $token, $payerid) {
        $post[] = "USER=$this->username";
        $post[] = "PWD=$this->password";
        $post[] = "SIGNATURE=$this->signature";
        $post[] = "VERSION=65.1"; // very important!
        $post[] = "PAYMENTREQUEST_0_CURRENCYCODE=USD";
        $post[] = "PAYMENTREQUEST_0_AMT=$money";
        $post[] = "PAYMENTREQUEST_0_ITEMAMT=$money";
        $post[] = "PAYMENTREQUEST_0_PAYMENTACTION=Sale"; // do not alter
        $post[] = "L_PAYMENTREQUEST_0_NAME0=$nameitem"; // use %20 for spaces
        $post[] = "L_PAYMENTREQUEST_0_ITEMCATEGORY0=Digital"; // do not alter
        $post[] = "L_PAYMENTREQUEST_0_QTY0=1";
        $post[] = "L_PAYMENTREQUEST_0_AMT0=$money";
        $post['returnurl'] = "RETURNURL=" . urlencode($this->returnurl); // do not alter
        $post['cancelurl'] = "CANCELURL=" . urlencode($this->cancelurl); // do not alter
        $post['token'] = "TOKEN=$token";
        $post['method'] = "METHOD=DoExpressCheckout"; // do not alter
        $post_str = implode('&', $post);
        $output_str = $this->CurlMePost($this->baseurl, $post_str);
        parse_str($output_str, $output_array);
        return $output_array;
    }

    private function sendRespond($money, $nameitem, $token, $payerid) {
        $post[] = "USER=$this->username";
        $post[] = "PWD=$this->password";
        $post[] = "SIGNATURE=$this->signature";
        $post[] = "VERSION=65.1";
        $post[] = "PAYMENTREQUEST_0_CURRENCYCODE=USD";
        $post[] = "PAYMENTREQUEST_0_AMT=$money";
        $post[] = "PAYMENTREQUEST_0_ITEMAMT=$money";
        $post[] = "PAYMENTREQUEST_0_PAYMENTACTION=Sale";
        $post[] = "L_PAYMENTREQUEST_0_NAME0=$nameitem"; // use %20 for spaces
//        $post[] = "L_PAYMENTREQUEST_0_ITEMCATEGORY0=Digital";
        $post[] = "L_PAYMENTREQUEST_0_QTY0=1";
        $post[] = "L_PAYMENTREQUEST_0_AMT0=$money";
        $post['method'] = "METHOD=DoExpressCheckoutPayment";
        $post['token'] = "TOKEN=$token";
        $post['payerid'] = "PayerID=$payerid";
        $post_str = implode('&', $post);
        $output_str = $this->CurlMePost($this->baseurl, $post_str);
        parse_str($output_str, $output_array);
        return $output_array;
    }

    private function CurlMePost($url, $post) {
// $post is a URL encoded string of variable-value pairs separated by &
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3); // 3 seconds to connect
        curl_setopt($ch, CURLOPT_TIMEOUT, 10); // 10 seconds to complete
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function CurlMePaypal($url, $post) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CAINFO, getcwd() . "/api-3t.paypal.com.crt");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}

?>
