<?php

class My_Category {

    function __construct() {
        $this->CI = $this->CI = & get_instance();
    }

    function build_visa_fees() {
        $this->CI->load->model(array('visa_fees_model', 'processing_time_model'));
        $vise_fees = $this->CI->visa_fees_model->get_all('id ASC');
        $processing_time = $this->CI->processing_time_model->get_all('id ASC');
        foreach ($vise_fees as $item) {
            $service_fees['visa_fees'][$item['purpose_of_visit']][$item['type_of_visa']] = $item;
        }
        foreach ($processing_time as $item) {
            $service_fees['Processing time'][$item['alias']] = $item;
        }
        //pre($service_fees);
        return $service_fees;
    }

    function build_visa_fees_country() {
        $this->CI->load->model(array('country_model', 'visa_fees_country_model'));
        $country = $this->CI->country_model->get_by(null, null, 'id');
        $vise_fees = $this->CI->visa_fees_country_model->get_all('', '', '');
        foreach ($vise_fees as $item) {
            $country[$item['country_id']]['visa_fees'][$item['visa_type']][$item['visa_exp']] = $item;
        }
        //pre($country);
        return $country;
    }

    function build_list_cat($cond = null) {
        $this->CI->load->model('category_model');
        $category = $this->CI->category_model->get_by('is_hide = 0', 'priority DESC');
        $list_category = $this->parseTree($category);
        return $list_category;
    }

    function show_menu($cond = null) {
        $this->CI->load->model('category_model');
        $category = $this->CI->category_model->get_by($cond, 'priority DESC');
        $list_category = $this->parseTree($category);
        return $list_category;
    }

    function build_menu() {
        $this->CI->load->model('group_category_model');
        $group_menu = $this->CI->group_category_model->get_all('priority DESC', 'id');
        $list_category = $this->build_list_cat();
        foreach ($list_category as $item) {
            if ($item['group_id'] != 0) {
                $group_menu[$item['group_id']]['category'][] = $item;
            }
        }
        return $group_menu;
    }

    function build_list_cat_news() {
        $this->CI->load->model('category_news_model');
        $parent = $this->CI->category_news_model->get_by('parent RLIKE "^([0-9]+,)$"', 'order ASC');
        $child = $this->CI->category_news_model->get_by('parent RLIKE "^([0-9]+,)([0-9]+,)+$"', 'order ASC');
        foreach ($parent as &$item) {
            foreach ($child as $k => $c) {
                $pattern = '/^' . $item['parent'] . '[0-9]+,/';
                if (preg_match($pattern, $c['parent']) != 0) {
                    $item['child'][] = $c;
                    unset($child[$k]);
                }
            }
        }
        return $parent;
    }

    function build_list_location() {
        $this->CI->load->model('location_model');
        $parent = $this->CI->location_model->get_by('parent RLIKE "^([0-9]+,)$"', 'order ASC');
        $child = $this->CI->location_model->get_by('parent RLIKE "^([0-9]+,)([0-9]+,)+$"', 'order ASC');
        foreach ($parent as &$item) {
            foreach ($child as $k => $c) {
                $pattern = '/^' . $item['parent'] . '[0-9]+,/';
                if (preg_match($pattern, $c['parent']) != 0) {
                    $item['child'][] = $c;
                    unset($child[$k]);
                }
            }
        }
        return $parent;
    }

    function showMenu($menus, $id_parent = 0) {
        $menu_tmp = array();
        foreach ($menus as $key => $item) {
            if ((int) $item['parent_id'] == (int) $id_parent) {
                $menu_tmp[$item['id']] = $item;
                unset($menus[$key]);
            }
        }
        if ($menu_tmp) {
            foreach ($menus as $item) {
                if (!empty($menu_tmp[$item['parent_id']])) {
                    $menu_tmp[$item['parent_id']]['childs'][] = $item;
                    @$menu_tmp[$item['parent_id']]['child_ids'].=$item['id'] . ',';
                }
            }
        }
        return $menu_tmp;
    }

    function parseTree($tree, $root = 0) {
        $return = array();
        foreach ($tree as $child => $parent) {
            if ($parent['parent'] == $root) {
                unset($tree[$child]);
                $return[$parent['id']] = $parent;
                $return[$parent['id']]['childs'] = $this->parseTree($tree, $parent['id']);
            }
        }
        return empty($return) ? null : $return;
    }

}
