<?php

class My_Api {

    function call_api($url = 'order', $method = "get", $data = '', $token = '') {
        $server_url = 'http://getvietnamvisa.lc/api/' . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $server_url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($method == 'put') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        } elseif ($method == 'delete') {
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        } elseif ($method == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'checksum: ""', 'x-access-token: ' . $token));
        if ($method != 'get') {
            $data_json = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            //echo $data_json;
        }
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        //$data = @json_decode($data, true);
        return array(
            'code' => $httpCode,
            'data' => $data
        );
    }

    function get_api_client($url, $type = 'results') {


        $server_url = 'http://52.76.14.70:8080/' . $url;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $server_url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $data = @json_decode($data, true);
        if ($httpCode == 200) {
            if ($type == 'results') {
                return $data['results'];
            } else {
                return $data;
            }
        }
        return false;
    }

    function post() {
        $service_url = 'http://getvietnamvisa.lc/api/order';
        $curl = curl_init($service_url);
        $curl_post_data = array(
            'message' => 'test message',
            'useridentifier' => 'agent@example.com',
            'department' => 'departmentId001',
            'subject' => 'My first conversation',
            'recipient' => 'recipient@example.com',
            'apikey' => 'key001'
        );
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        if ($curl_response === false) {
            $info = curl_getinfo($curl);
            curl_close($curl);
            die('error occured during curl exec. Additioanl info: ' . var_export($info));
        }
        curl_close($curl);
        $decoded = json_decode($curl_response);
        if (isset($decoded->response->status) && $decoded->response->status == 'ERROR') {
            die('error occured: ' . $decoded->response->errormessage);
        }
        echo 'OK!';
        var_export($decoded->response);
    }

    function post_to_url($url, $data) {
        $post = curl_init();       
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'checksum: ""', 'x-access-token: ' . $token));
        curl_setopt($post, CURLOPT_POSTFIELDS, $data);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($post);

        curl_close($post);
        return $result;
    }
    
    
}
