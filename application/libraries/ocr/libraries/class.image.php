<?php

class image {

    private $imageType = array(
        '1' => 'gif',
        '2' => 'jpg',
        '3' => 'png',
        '6' => 'bmp',
        '14' => 'iff',
        '15' => 'wbmp',
        '16' => 'xbm'
    );

    function imagetype($filename) {
        if (( list($width, $height, $type, $attr) = getimagesize($filename) ) !== false) {
            return $type;
        }
        return false;
    }

    function read($filename) {
        if (empty($filename)) {
            return false;
        }
        $type = $this->getType($filename);
        if (empty($type))
            return false;
        if ($type == 'gif') {
            return imagecreatefromgif($filename);
        } elseif ($type == 'jpg') {
            return imagecreatefromjpeg($filename);
        } elseif ($type == 'png') {
            return imagecreatefrompng($filename);
        } else {
            return false;
        }
    }

    public function getType($filename) {
        if (empty($filename)) {
            return false;
        }
        $intType = $this->imagetype($filename);
        if ($intType) {
            return $this->imageType[$intType];
        } else {
            return false;
        }
    }

    public function getSize($filename) {
        if (empty($filename)) {
            return false;
        }
        $resultGetSize = @getimagesize($filename);
        if ($resultGetSize) {
            return $resultGetSize;
        } else {
            return false;
        }
    }

    public function getSizeWithResource($resource) {
        if (!is_resource($resource)) {
            return false;
        }
        $resultGetSize = array(
            imagesx($resource),
            imagesy($resource)
        );
        if ($resultGetSize) {
            return $resultGetSize;
        } else {
            return false;
        }
    }

    public function resize($new_x, $new_y, $pathInput, $pathOutput, $fillColor = false) {
        if (empty($pathInput) || empty($pathOutput)) {
            return false;
        }
        if ($new_x == false)
            $new_x = 0;
        if ($new_y == false)
            $new_y = 0;
        if (!is_numeric($new_x) || !is_numeric($new_y)) {
            return false;
        }
        if (empty($fillColor) || !is_array($fillColor)) {
            $fillColor = array(255, 255, 255);
        }
        $src = $this->read($pathInput);
        if (!$src) {
            return false;
        }
        $resize = imagecreatetruecolor($new_x, $new_y);
        $color = imagecolorallocate($resize, $fillColor[0], $fillColor[1], $fillColor[2]);
        imagefill($resize, 0, 0, $color);
        list($src_x, $src_y) = $this->getSize($pathInput);
        if ($new_x < $new_y) {
            $percent = 100 * $new_x / $src_x;
        } else {
            if ($src_x < $src_y) {
                $percent = (double) (100 * $new_y) / $src_y;
            } else {
                $percent = (double) (100 * $new_x) / $src_x;
            }
        }
        $temp_x = $src_x * $percent / 100;
        $temp_y = $src_y * $percent / 100;

        $temp = imagecreatetruecolor($temp_x, $temp_y);

        imagecopyresampled($temp, $src, 0, 0, 0, 0, $temp_x, $temp_y, $src_x, $src_y);

        if ($temp_x <= $new_x || $temp_y <= $new_y) {
            $top = round(( $new_y - $temp_y ) / 2, 3);
            $left = round(( $new_x - $temp_x ) / 2, 3);
            imagecopy($resize, $temp, $left, $top, 0, 0, $temp_x, $temp_y);
        } else {
            $center_x = round($temp_x / 2);
            $center_y = round($temp_y / 2);

            $left = $center_x - ( round($new_x / 2) );
            $right = $center_x + ( round($new_x / 2) );
            $top = $center_y - ( round($new_y / 2) );
            $bottom = $center_y + ( round($new_y / 2) );
            imagecopy($resize, $temp, 0, 0, $left, $top, $right, $bottom);
        }
        $resultMake = imagejpeg($resize, $pathOutput, 100);
        imagedestroy($resize);
        imagedestroy($src);
        imagedestroy($temp);
        if ($resultMake) {
            return true;
        } else {
            return false;
        }
    }

    public function resizeWithResource($new_x, $new_y, $resourceInput,$fillColor = false) {
        if (!is_resource($resourceInput)) {
            return false;
        }
        if ($new_x == false)
            $new_x = 0;
        if ($new_y == false)
            $new_y = 0;
        if (!is_numeric($new_x) || !is_numeric($new_y)) {
            return false;
        }
        if (empty($fillColor) || !is_array($fillColor)) {
            $fillColor = array(255, 255, 255);
        }
        $src = $resourceInput;

        if (!$src) {
            return false;
        }
        $resize = imagecreatetruecolor($new_x, $new_y);
        $color = imagecolorallocate($resize, $fillColor[0], $fillColor[1], $fillColor[2]);
        imagefill($resize, 0, 0, $color);
        list($src_x, $src_y) = $this->getSizeWithResource($src);
        if ($new_x < $new_y) {
            $percent = 100 * $new_x / $src_x;
        } else {
            if ($src_x < $src_y) {
                $percent = (double) (100 * $new_y) / $src_y;
            } else {
                $percent = (double) (100 * $new_x) / $src_x;
            }
        }
        $temp_x = $src_x * $percent / 100;
        $temp_y = $src_y * $percent / 100;

        $temp = imagecreatetruecolor($temp_x, $temp_y);

        imagecopyresampled($temp, $src, 0, 0, 0, 0, $temp_x, $temp_y, $src_x, $src_y);

        if ($temp_x <= $new_x || $temp_y <= $new_y) {
            $top = round(( $new_y - $temp_y ) / 2, 3);
            $left = round(( $new_x - $temp_x ) / 2, 3);
            imagecopy($resize, $temp, $left, $top, 0, 0, $temp_x, $temp_y);
        } else {
            $center_x = round($temp_x / 2);
            $center_y = round($temp_y / 2);

            $left = $center_x - ( round($new_x / 2) );
            $right = $center_x + ( round($new_x / 2) );
            $top = $center_y - ( round($new_y / 2) );
            $bottom = $center_y + ( round($new_y / 2) );
            imagecopy($resize, $temp, 0, 0, $left, $top, $right, $bottom);
        }
        imagedestroy($src);
        imagedestroy($temp);
        return $resize;
    }

    public function addWatermark($x, $y, $pathImageInput, $pathWatemarkInput, $pathOutput, $opacity = false, $align = false) {
        if (empty($pathImageInput) || empty($pathWatemarkInput) || empty($pathOutput)) {
            return false;
        }
        if ($x == false)
            $x = 0;
        if ($y == false)
            $y = 0;
        if (!is_numeric($x) || !is_numeric($y)) {
            return false;
        }
        $src_image = $this->read($pathImageInput);
        if (empty($src_image))
            return false;
        $src_watermark = $this->read($pathWatemarkInput);
        if (empty($src_watermark))
            return false;
        $size_watermark = $this->getSize($pathWatemarkInput);

        if ($align) {
            $size_image = $this->getSize($pathImageInput);
        }
        if ($size_image[0] < $size_watermark[0] || $size_image[1] < $size_watermark[1]) {
            return copy($pathImageInput, $pathOutput);
        }
        if ($align == 'lb') {   // lb = Left Bottom
            $y = $size_image[1] - $size_watermark[1] - $y;
        } elseif ($align == 'rb') { // rb = Right Bottom
            $x = $size_image[0] - $size_watermark[0] - $x;
            $y = $size_image[1] - $size_watermark[1] - $y;
        } elseif ($align == 'rt') { // lt = Left Bottom
            $x = $size_image[0] - $size_watermark[0] - $x;
        } elseif ($align == 'lc') { // lc = Left Center
            $y = round(($size_image[1] - $size_watermark[1]) / 2, 3) + $y;
        } elseif ($align == 'rc') { // rc = Right Center
            $x = $size_image[0] - $size_watermark[0] - $x;
            $y = round(($size_image[1] - $size_watermark[1]) / 2, 3) + $y;
        } elseif ($align == 'tc') { // tc = Top Center
            $x = round(($size_image[0] - $size_watermark[0]) / 2, 3) + $x;
        } elseif ($align == 'bc') { // bc = Bottom Center
            $x = round(($size_image[0] - $size_watermark[0]) / 2, 3) + $x;
            $y = $size_image[1] - $size_watermark[1] - $y;
        } elseif ($align == 'c') { // c = Center
            $x = round(($size_image[0] - $size_watermark[0]) / 2, 3) + $x;
            $y = round(($size_image[1] - $size_watermark[1]) / 2, 3) + $y;
        }
        if ($opacity && $opacity != '100' && is_numeric($opacity)) {
            imagecopymerge($src_image, $src_watermark, $x, $y, 0, 0, $size_watermark[0], $size_watermark[1], $opacity);
        } else {
            imagecopy($src_image, $src_watermark, $x, $y, 0, 0, $size_watermark[0], $size_watermark[1]);
        }
        $resultMake = imagejpeg($src_image, $pathOutput, 100);
        imagedestroy($src_image);
        imagedestroy($src_watermark);
        if ($resultMake) {
            return true;
        } else {
            return false;
        }
    }

    public function fix_orientation($fileandpath) {
      // Does the file exist to start with?
      if(!file_exists($fileandpath))
        return false;
      // Get all the exif data from the file

      $exif = @read_exif_data($fileandpath, 'IFD0');
      // If we dont get any exif data at all, then we may as well stop now
      if(!$exif || !is_array($exif))
        return false;
      
      // I hate case juggling, so we're using loweercase throughout just in case
      $exif = array_change_key_case($exif, CASE_LOWER);
      
      // If theres no orientation key, then we can give up, the camera hasn't told us the 
      // orientation of itself when taking the image, and i'm not writing a script to guess at it!
      if(!array_key_exists('orientation', $exif)) 
        return false;
      
      // Gets the GD image resource for loaded image
      $img_res = $this->read($fileandpath);
      
      // If it failed to load a resource, give up
      if(is_null($img_res))
        return false;
      
      // The meat of the script really, the orientation is supplied as an integer, 
      // so we just rotate/flip it back to the correct orientation based on what we 
      // are told it currently is 
      switch($exif['orientation']) {
        
        // Standard/Normal Orientation (no need to do anything, we'll return true as in theory, it was successful)
        case 1: return $img_res; break;
        
        // Correct orientation, but flipped on the horizontal axis (might do it at some point in the future)
        case 2: 
          $final_img = imageflip($img_res, IMG_FLIP_HORIZONTAL);
        break;
        
        // Upside-Down
        case 3: 
          $final_img = imageflip($img_res, IMG_FLIP_VERTICAL);
        break;
        
        // Upside-Down & Flipped along horizontal axis
        case 4:  
          $final_img = imageflip($img_res, IMG_FLIP_BOTH);
        break;
        
        // Turned 90 deg to the left and flipped
        case 5:  
          $final_img = imagerotate($img_res, -90, 0);
          $final_img = imageflip($img_res, IMG_FLIP_HORIZONTAL);
        break;
        
        // Turned 90 deg to the left
        case 6: 
          $final_img = imagerotate($img_res, -90, 0);
        break;
        
        // Turned 90 deg to the right and flipped
        case 7: 
          $final_img = imagerotate($img_res, 90, 0);
          $final_img = imageflip($img_res,IMG_FLIP_HORIZONTAL);
        break;
        
        // Turned 90 deg to the right
        case 8: 
          $final_img = imagerotate($img_res, 90, 0); 
        break;
        
      }
      
      // If theres no final image resource to output for whatever reason, give up
      if(!isset($final_img))
        return false;
      
      //-- rename original (very ugly, could probably be rewritten, but i can't be arsed at this stage)
      //rename($fileandpath, $path.'/'.$newname);
        return $final_img;
      //return $this->resizeWithResource((imagesx($final_img)*(1500*100/imagesx($final_img)))/100, (imagesy($final_img)*(1500*100/imagesx($final_img)))/100, $final_img, false);
      // Save it and the return the result (true or false)
      //$done = save_image_resource($final_img,$fileandpath);
      
      //return $done;
    }

}

?>