<?php

class Transload {

    function __construct() {
        $this->CI = &get_instance();
    }

    function transload_img($linktranload, $pathFileName) {
        if (strpos($linktranload, "?")) {
            $temp = explode("?", $linktranload);
            $linktranload = $temp[0];
        }

        $data = explode('.', $linktranload);
        $file_type = strtolower($data[count($data) - 1]);
        if ($file_type == 'jpg' || $file_type == 'png' || $file_type = 'jpeg') {
            $handle = @fopen($linktranload, "rb");
            $contents = @stream_get_contents($handle);
            @fclose($handle);
            $f2 = @fopen($pathFileName, "w");
            @fwrite($f2, $contents);
            @fclose($f2);
            if (file_exists($pathFileName) && filesize($pathFileName) && getimagesize($pathFileName)) {
                return filesize($pathFileName);
            } else {
                @unlink($pathFileName);
                return false;
            }
        }
        return false;
    }

    function split_content($tag_start, $tag_end, $str) {
        $temp = '';
        $temp1 = '';
        $result = '';
        $temp = explode($tag_start, $str);
        if (count($temp) > 2) {
            for ($i = 1; $i < count($temp); $i++) {
                $temp1 = explode($tag_end, $temp[$i]);
                $result[] = $temp1[0];
            }
        } else {
            $temp1 = @explode($tag_end, $temp[1]);
            $result = $temp1[0];
        }


        return $result;
    }

    function isValidURL($url) {
        return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
    }

    function get_image($content, $folder, $dir, $no_image, $alias) {
        $dir = $dir . $folder;
        $list_image = $this->split_content('<img', '>', $content);
        $get_no_image = 0;
        for ($i = 0; $i < count($list_image); $i++) {
            $temp = '';
            $temp = $this->split_content('src="', '"', $list_image[$i]);
            if (!$temp[0]) {
                $temp = $this->split_content("src='", "'", $list_image[$i]);
            }
            $arr_list_image[] = $temp[0];
        }
        $img_url = '/assets/upload/page/' . $folder;
        for ($i = 0; $i < count($arr_list_image); $i++) {
            if ($this->isValidURL($arr_list_image[$i])) {
                $temp = explode("?", $arr_list_image[$i]);
                $filename_url_grab[$i] = basename($arr_list_image[$i]);
                if (strpos($filename_url_grab[$i], "?")) {
                    $temp = explode("?", $filename_url_grab[$i]);
                    $filename_url_grab[$i] = $temp[0];
                }
                $filename_url_ext[$i] = substr($filename_url_grab[$i], -3);
                if (strtolower($filename_url_ext[$i]) == 'jpg' || strtolower($filename_url_ext[$i]) == 'png' || strtolower($filename_url_ext[$i]) == 'peg') {
                    $filename[$i] = random_string("alnum") . '.jpg';
                    $ok = $this->transload_img($arr_list_image[$i], $dir . $filename[$i]);
                    if ($ok) {
                        if ($get_no_image == 0 && $no_image) {
                            $abc = getimagesize($dir . $filename[$i]);
                            if ($abc[0] >= 230) {
                                $this->crop_resize($dir . $filename[$i], $dir . $alias . '.jpg', 230, 150);
                                $get_no_image = 1;
                            }
                        }
                        $this->thumb_width($dir . $filename[$i], $dir . $filename[$i], 650);

                        $content = str_replace($list_image[$i], ' src="' . $img_url . $filename[$i] . '"', $content);
                        //$content = str_replace($arr_list_image[$i], $img_url . $filename[$i], $content);
                    }
                }
            }
        }
        $content = preg_replace('/<a href="(.+)">/', '', $content);
        return array('content' => $content, 'get_no_image' => $get_no_image);
    }

    function get_image_for_page($content) {
        $list_image = $this->split_content('<img', '>', $content);

        for ($i = 0; $i < count($list_image); $i++) {
            $temp = '';
            $temp = $this->split_content('src="', '"', $list_image[$i]);
            if (!$temp[0]) {
                $temp = $this->split_content("src='", "'", $list_image[$i]);
            }
            $arr_list_image[] = $temp[0];
        }
        $img_url = '/assets/upload/page/';
        $dir = PATH_PAGE;
        for ($i = 0; $i < count($arr_list_image); $i++) {
            $img_url_blogger = '';
            if ($this->isValidURL($arr_list_image[$i])) {
                $temp = explode("?", $arr_list_image[$i]);
                $filename_url_grab[$i] = basename($arr_list_image[$i]);
                if (strpos($filename_url_grab[$i], "?")) {
                    $temp = explode("?", $filename_url_grab[$i]);
                    $filename_url_grab[$i] = $temp[0];
                }
                $filename_url_ext[$i] = substr($filename_url_grab[$i], -3);
                if (strtolower($filename_url_ext[$i]) == 'jpg' || strtolower($filename_url_ext[$i]) == 'png' || strtolower($filename_url_ext[$i]) == 'peg') {
                    $filename[$i] = random_string("alnum", 5) . '.jpg';
                    $ok = $this->transload_img($arr_list_image[$i], $dir . $filename[$i]);
                    if ($ok) {
                        $this->crop($dir . $filename[$i]);
                        $this->thumb_width($dir . $filename[$i], $dir . $filename[$i], 650);
                        $content = str_replace($list_image[$i], ' src="' . $img_url . $filename[$i] . '"', $content);
                        //$content = str_replace($arr_list_image[$i], $img_url . $filename[$i], $content);
                    }
                }
            }
        }

        //  $content = preg_replace('#(.+?)width="(.+?)"(.+?)height="(.+?)"(.*)#is', '\\1\\5', $content);
        // $content = preg_replace('/<a href="(.+)">/', '', $content);      
        return $content;
    }

    function get_image_for_product($content, $folder, $product_id) {
        $content = str_replace('"/upload/images/', '"http://sucashop.com/upload/images/', $content);
        $list_image = $this->split_content('<img', '>', $content);
        for ($i = 0; $i < count($list_image); $i++) {
            $temp = '';
            $temp = $this->split_content('src="', '"', $list_image[$i]);
            if (!$temp[0]) {
                $temp = $this->split_content("src='", "'", $list_image[$i]);
            }
            $arr_list_image[] = str_replace(' ', '%20', $temp[0]);
        }
        $img_url = '/assets/upload/product/' . $folder . '/goc/';
        $dir = PATH_UPLOAD . 'product/' . $folder . '/goc/';
        @mkdir($dir, 0777);
        $images = '';
        for ($i = 0; $i < count($arr_list_image); $i++) {
            if ($this->isValidURL($arr_list_image[$i])) {
                $temp = explode("?", $arr_list_image[$i]);
                $filename_url_grab[$i] = basename($arr_list_image[$i]);
                if (strpos($filename_url_grab[$i], "?")) {
                    $temp = explode("?", $filename_url_grab[$i]);
                    $filename_url_grab[$i] = $temp[0];
                }
                $filename_url_ext[$i] = substr($filename_url_grab[$i], -3);
                if (strtolower($filename_url_ext[$i]) == 'jpg' || strtolower($filename_url_ext[$i]) == 'png' || strtolower($filename_url_ext[$i]) == 'peg') {
                    $filename[$i] = random_string("alnum", 5) . '.jpg';
                    $ok = $this->transload_img($arr_list_image[$i], $dir . $filename[$i]);
                    if ($ok) {
                        $images[] = array(
                            'name' => $filename[$i],
                            'product_id' => $product_id
                        );
                        $this->crop_resize($dir . $filename[$i], $dir . $filename[$i], 600, 800);
                        $content = str_replace($list_image[$i], ' src="' . $img_url . $filename[$i] . '"', $content);
                        //$content = str_replace($arr_list_image[$i], $img_url . $filename[$i], $content);
                    }
                }
            }
        }
        $content = preg_replace('#(.+?)width="(.+?)"(.+?)height="(.+?)"(.*)#is', '\\1\\5', $content);
        return array(
            'content' => $content,
            'images' => $images
        );
    }

    function crop_resize($img, $new_image, $thumb_w, $thumb_h, $position = 'center') {
        $cf = getimagesize($img);
        $config['source_image'] = $img;
        $config['new_image'] = $new_image;
        $config['image_library'] = 'gd2';
        $config['maintain_ratio'] = false;
        $percent_w = $cf[0] / $thumb_w;
        $percent_h = $cf[1] / $thumb_h;
        $percent = $percent_w > $percent_h ? $percent_h : $percent_w;
        $config['width'] = $thumb_w * $percent;
        $config['height'] = $thumb_h * $percent;
        $config['x_axis'] = ($cf[0] - $config['width']) / 2;
        $config['y_axis'] = ($cf[1] - $config['height']) / 2;
        $this->CI->load->library('image_lib');
        $this->CI->image_lib->clear();
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->crop();
        if ($config['width'] > $thumb_w) {
            $config['width'] = $thumb_w;
            $config['height'] = $thumb_h;
            $config['source_image'] = $new_image;
            $config['new_image'] = $new_image;
            $this->CI->image_lib->initialize($config);
            $this->CI->image_lib->resize();
        }
    }

    function thumb_width($img, $new_image, $width, $wmark = TRUE) {
        $cf = getimagesize($img);
        $config['source_image'] = $img;
        $config['image_library'] = 'gd2';
        $config['maintain_ratio'] = false;
        if ($cf[0] < $width) {
            $config['width'] = $cf[0];
            $config['height'] = $cf[1];
        } else {
            $percent = $cf[0] / $width;
            $config['width'] = $width;
            $config['height'] = $cf[1] / $percent;
        }
        $config['new_image'] = $new_image;
        $this->CI->load->library('image_lib');
        $this->CI->image_lib->clear();
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->resize();
        if ($wmark == TRUE)
            $this->_watermark($new_image, $width . '.png');
    }

    function crop($img) {
        $cf = getimagesize($img);
        $config['source_image'] = $img;
        $config['image_library'] = 'gd2';
        $config['maintain_ratio'] = false;
        $config['width'] = $cf[0];
        $config['height'] = $cf[1] - 20;
        $config['x_axis'] = 0;
        $config['y_axis'] = 0;
        $this->CI->load->library('image_lib');
        $this->CI->image_lib->clear();
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->crop();
    }

    function _watermark($new_image, $watermark) {
        $this->CI->load->library('image_lib');
        $config['create_thumb'] = FALSE;
        $config['source_image'] = $new_image;
        $config['wm_overlay_path'] = APPPATH . '../assets/upload/' . $watermark;
        $config['wm_type'] = 'overlay';
        $config['dynamic_output'] = TRUE;
        $config['wm_vrt_alignment'] = 'B';
        $config['wm_hor_alignment'] = 'R';
        $config['dynamic_output'] = false;
        $this->CI->image_lib->clear();
        $this->CI->image_lib->initialize($config);
        $this->CI->image_lib->watermark();
        $this->CI->image_lib->display_errors();
    }

    function ago($tm, $rcs = 0) {
        if (date('Y-m-d', $tm) == '1970-01-01')
            return false;
        $cur_tm = time();
        $dif = $cur_tm - $tm;
        $pds = array('giây', 'phút', 'giờ', 'ngày', 'tuần', 'tháng', 'năm', 'decade');
        $lngh = array(1, 60, 3600, 86400, 604800, 2630880, 31570560, 315705600);
        for ($v = sizeof($lngh) - 1; ($v >= 0) && (($no = $dif / $lngh[$v]) <= 1); $v--)
            ; if ($v < 0)
            $v = 0;
        $_tm = $cur_tm - ($dif % $lngh[$v]);
        $no = floor($no);
        if ($no <> 1)
            $pds[$v] .= '';
        $x = sprintf("%d %s ", $no, $pds[$v]);
        if (($rcs == 1) && ($v >= 1) && (($cur_tm - $_tm) > 0))
            $x .= time_ago($_tm);
        return $x;
    }

    function clearTag($input) {
        return trim(strip_tags($input));
    }

    function VndText($amount) {
        $pre_fix = '';
        if ($amount == 0) {
            return $textnumber = "Không đồng";
        } elseif ($amount < 0) {
            $pre_fix = 'Âm ';
            $amount *= -1;
        }

        $Text = array("không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín");
        $TextLuythua = array("", "nghìn", "triệu", "tỷ", "ngàn tỷ", "triệu tỷ", "tỷ tỷ");
        $textnumber = "";
        $length = strlen($amount);

        for ($i = 0; $i < $length; $i++)
            $unread[$i] = 0;

        for ($i = 0; $i < $length; $i++) {
            $so = substr($amount, $length - $i - 1, 1);

            if (($so == 0) && ($i % 3 == 0) && ($unread[$i] == 0)) {
                for ($j = $i + 1; $j < $length; $j ++) {
                    $so1 = substr($amount, $length - $j - 1, 1);
                    if ($so1 != 0)
                        break;
                }

                if (intval(($j - $i ) / 3) > 0) {
                    for ($k = $i; $k < intval(($j - $i) / 3) * 3 + $i; $k++)
                        $unread[$k] = 1;
                }
            }
        }

        for ($i = 0; $i < $length; $i++) {
            $so = substr($amount, $length - $i - 1, 1);
            if ($unread[$i] == 1)
                continue;

            if (($i % 3 == 0) && ($i > 0))
                $textnumber = $TextLuythua[$i / 3] . " " . $textnumber;

            if ($i % 3 == 2)
                $textnumber = 'trăm ' . $textnumber;

            if ($i % 3 == 1)
                $textnumber = 'mươi ' . $textnumber;


            $textnumber = $Text[$so] . " " . $textnumber;
        }

        //Phai de cac ham replace theo dung thu tu nhu the nay
        $textnumber = str_replace("không mươi", "lẻ", $textnumber);
        $textnumber = str_replace("lẻ không", "", $textnumber);
        $textnumber = str_replace("mươi không", "mươi", $textnumber);
        $textnumber = str_replace("một mươi", "mười", $textnumber);
        $textnumber = str_replace("mươi năm", "mươi lăm", $textnumber);
        $textnumber = str_replace("mươi một", "mươi mốt", $textnumber);
        $textnumber = str_replace("mười năm", "mười lăm", $textnumber);

        return $pre_fix . ucfirst($textnumber . " đồng chẵn");
    }

}
