<?php

class Word {

    var $pathTemp = '';
    var $pathOutput = '';

    private function listFileWordInFolder($source) {
        $listFile = scandir($source);
        $files = preg_grep('~\.(docx)$~', $listFile);
        return array_values($files);
    }

    private function replace($filename, $arrSearchReplace) {
        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            return false;
        }
        $xml = $zip->getFromName('word/document.xml');
        foreach ($arrSearchReplace as $key => $value) {
            $xml = str_replace('{' . strtoupper($key) . '}', str_replace(array('&'), array('&amp;'), $value), $xml);
        }
        $result = false;
        if ($zip->addFromString('word/document.xml', $xml)) {
            $result = true;
        }
        $zip->close();
        return $result;
    }

    private function saveToZip($filename, $arrFile) {
        $zip = new ZipArchive();
        if ($zip->open($filename, ZipArchive::CREATE) !== TRUE) {
            return false;
        }
        foreach ($arrFile as $key => $value) {
            if (!$zip->addFile($value, $key)) {
                echo 'Can\'t create zip file !';
                exit;
            }
        }
        $zip->close();
        return true;
    }
    
    function replaceInFolder($folderTemplate, $arrSearchReplace, $zipname) {
        $listFile = $this->listFileWordInFolder($folderTemplate);
        $listFileTemp = array();
        $listFileSuccess = array();

        foreach ($listFile as $key => $value) {
            $filename = $folderTemplate . '/' . $value;
            $filenameTemp = $this->pathTemp . '/' . md5(microtime()) . '.docx';
            
            if (copy($filename, $filenameTemp)) {
                
                $listFileTemp[$value] = $filenameTemp;
            } else {
                echo 'Can\'t create temporary file !';
                exit;
            }
        }

        foreach ($listFileTemp as $key => $value) {
            if ($this->replace($value, $arrSearchReplace)) {
                $listFileSuccess[$key] = $value;
            }
        }

        $filenameOutput = $this->pathOutput . '/' . $zipname . '.zip';
        if ($this->saveToZip($filenameOutput, $listFileSuccess)) {
            foreach ($listFileTemp as $key => $value) {
                @unlink($value);
            }
            return $filenameOutput;
        } else {
            return false;
        }
    }

}
