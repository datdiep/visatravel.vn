<?php

function convert_id($id) {
    $id = '00000000' . $id;
    return substr($id, -5, 5);
}

function check_order($order) {
    if ($order['status'] != 'success' && $order['status'] != 'fail') {
        return true;
    }
    return false;
}

function check_is_export($order) {
    if ($order['status'] != 'shipping' && $order['status'] != 'success' && $order['status'] != 'fail') {
        return false;
    }
    return true;
}

function revert_date($date = "") {
    $date = explode('/', $date);
    return $date[1] . '/' . $date[0];
}

function cal_order($order) {
    $total = $order['total'] * (100 - $order['discount_date_money']) / 100;
    $discount = 0;
    if ($order['voucher_price']) {
        $discount = $order['voucher_price'];
        if ($order['voucher_type'] == '%')
            $discount = $total * $discount / 100;
    }
    return $total - $discount + $order['shipping_price'] + $order['service_price'] - $order['payed_money'];
}

function pre($list, $exit = true) {
    echo "<pre>";
    print_r($list);
    if ($exit) {
        die;
    }
}

function pre_html($html, $exit = true) {
    echo '<pre>';
    echo htmlspecialchars($html);
    echo '</pre>';
    if ($exit) {
        die;
    }
}

function time_stamp($time_ago) {
    $cur_time = time();
    $time_elapsed = $time_ago - $cur_time;
    $seconds = $time_elapsed;
    $minutes = round($time_elapsed / 60);
    $hours = round($time_elapsed / 3600);
    $days = round($time_elapsed / 86400);
    $weeks = round($time_elapsed / 604800);
    $months = round($time_elapsed / 2600640);
    $years = round($time_elapsed / 31207680);
// Seconds
    if ($time_elapsed < 0) {
        echo "Hết giờ";
        return false;
    }
    if ($seconds <= 60) {
        echo "còn $seconds giây ";
    }
//Minutes
    else if ($minutes <= 60) {
        if ($minutes == 60) {
            echo "còn 1 giờ ";
        } else {
            echo "còn $minutes phút";
        }
    }
//Hours
    else if ($hours <= 24) {
        if ($hours == 24) {
            echo "1 ngày ";
        } else {
            echo "còn  $hours giờ ";
        }
    }
//Days
    else if ($days <= 7) {
        if ($days == 7) {
            echo "còn 1 tuần ";
        } else {
            echo "còn $days ngày ";
        }
    }
//Weeks
    else if ($weeks <= 4.3) {
        if ($weeks == 4.3) {
            echo "còn 1 tháng ";
        } else {
            echo "còn $weeks tuần";
        }
    }
//Months
    else if ($months <= 12) {
        if ($months == 12) {
            echo "còn 1 năm ";
        } else {
            echo " còn $months tháng";
        }
    }
//Years
    else {
        echo "còn $years năm ";
    }
}

function format_date($date) {
    if (date('Y-m-d', strtotime($date)) == '1970-01-01')
        return false;
    return date('F d,Y', strtotime($date));
}

function detect_url($s) {
    return preg_replace('/https?:\/\/[\w\-\%.!~#?&=;+\*\'"(),\/]+/', '<a href="$0" target="blank" rel="nofollow" title="Click để mở site này">$0</a>', $s);
}

function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach ($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}

function sanitizeTitle($string) {
    if (!$string)
        return false;
    $utf8 = array(
        'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'd' => 'đ|Đ',
        'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'i' => 'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
        'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
        'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
        'y' => 'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
    );
    foreach ($utf8 as $ascii => $uni)
        $string = preg_replace("/($uni)/i", $ascii, $string);
    $string = utf8Url($string);
    return $string;
}

function utf8Url($string) {
    $string = strtolower($string);
    $string = str_replace("ß", "ss", $string);
    $string = str_replace("%", "", $string);
    $string = preg_replace("/[^_a-zA-Z0-9 -.]/", "", $string);
    $string = str_replace(array('%20', ' '), '-', $string);
    $string = str_replace("----", "-", $string);
    $string = str_replace("---", "-", $string);
    $string = str_replace("--", "-", $string);
    return $string;
}
