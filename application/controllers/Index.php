<?php

/**
 * @property Product_Model $product_model
 */
class Index extends FONTEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('gallery_model', 'page_model', 'country_model', 'service_question_model', 'news_model', 'category_news_model'));
        $this->load->helper('cookie');

        $this->load->library('curl');
        $this->load->helper('text');
        $this->data['visa_type_show'] = array('for tourism' => 'Du lịch', 'for business' => 'Công tác', 'family/friend visit' => 'Thăm thân / Việc riêng');
        $this->data['visa_stay_show'] = array('1 month single' => '1 tháng nhập cảnh 1 lần',
            '3 month single' => '3 tháng nhập cảnh 1 lần',
            '1 month multiple' => '1 tháng nhập cảnh nhiều lần',
            '3 month multiple' => '3 tháng nhập cảnh nhiều lần',
            '6 month multiple' => '6 tháng nhập cảnh nhiều lần',
            '1 year multiple' => '1 năm nhập cảnh nhiều lần (Chỉ dành cho Mỹ)'
        );

        $this->data['all_question'] = 0;
        $result = @file_get_contents(APPPATH . 'cache/phone');
        if ($result) {
            $data = unserialize($result);
            $this->data['all_question'] = count($data);
        }
    }

    function update_alias_country() {
        $this->load->model(array('country_model', 'country_tips_model'));
        $tips = $this->country_tips_model->get_all(NULL, 'id');
        foreach ($tips as $k => $item) {
            $country[$item['id']] = $this->country_model->get_by(array('id' => $item['country_id']));
            $this->country_tips_model->update_by(array('country_alias' => $country[$k][0]['alias']), array('id' => $item['id']));
        }
    }

    function index() {
        $this->data['pre'] = 'home';
        $this->data['home'] = 'home';

        // lay  noi dung xem nhieu nhat va  noi dung moi nhat
        $this->data['cate_news_1'] = $this->category_news_model->get_row_by("alias = 'visa-xuat-canh'");
        $this->data['news_1_hot'] = $this->news_model->get_by_limit('cate_id = ' . $this->data['cate_news_1']['id'], ' view DESC', 3);
        $this->data['news_1'] = $this->news_model->get_by_limit('cate_id = ' . $this->data['cate_news_1']['id'], 'id DESC', 5);

        $this->data['cate_news_2'] = $this->category_news_model->get_row_by("alias = 'cam-nang-du-lich'");
        $this->data['news_2_hot'] = $this->news_model->get_by_limit('cate_id = ' . $this->data['cate_news_2']['id'], ' view DESC', 2);
        $this->data['news_2'] = $this->news_model->get_by_limit('cate_id = ' . $this->data['cate_news_2']['id'], 'id DESC', 12);

        //list view xem hot nhat va moi nhat
        $cond = 'cate_id != ' . $this->data['cate_news_1']['id'] . ' and cate_id != ' . $this->data['cate_news_2']['id'];
        $this->data['news_views_new'] = $this->news_model->get_by_limit($cond, 'id DESC', 6);
        $this->data['news_views_hot'] = $this->news_model->get_by_limit($cond, ' view DESC', 2);


        $this->template->write_view('content_block', 'index/home', $this->data);
        $this->template->render();
    }

    function services($alias, $page = 1, $url = '', $cond = null, $uri_segment = 3) {
        $this->load->model(array('country_tips_model'));
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }
        $this->limit = 10;
        $page = intval($page);
        $page < 1 ? 1 : $page;

        $pos = ($page - 1) * $this->limit;

        $alias = strtolower($alias);
//'Embassy','Requirements','How to get Vietnam visa','Visa extension','Working permit','Temporary Resident Card','Document'
        if ($alias == 'gia-han-visa') {
            $query = 'Visa extension';
            $this->data['service_title'] = 'Gia hạn visa Việt Nam cho người nước ngoài';

            $this->data['web']['seo_title'] = 'Gia hạn visa Việt Nam cho người nước ngoài | Trang ' . $page;
            $this->data['web']['meta_description'] = '【Dịch vụ gia hạn visa】 đảm bảo thành công 100% - Xử lý mọi quốc tịch khó';
            $this->data['web']['meta_keyword'] = 'gia han visa, dich vu gia han visa, vietnam visa extension, gia han visa vietnam';
        } elseif ($alias == 'giay-phep-lao-dong') {
            $query = 'Working permit';
            $this->data['service_title'] = 'Giấy phép lao động cho người nước ngoài';

            $this->data['web']['seo_title'] = 'Giấy phép lao động cho người nước ngoài đang sống tại Việt Nam | Trang ' . $page;
            $this->data['web']['meta_description'] = '【Work Permit - Giấy phép lao động】cho người nước ngoài đảm bảo thành công 100% - Xử lý mọi quốc tịch khó';
            $this->data['web']['meta_keyword'] = 'giay phep lao dong, xin giay phep lao dong, dich vu xin giay phep lao dong, thu tuc cap giay phep lao dong cho nguoi nuoc ngoai';
        } elseif ($alias == 'the-tam-tru') {
            $query = 'Temporary Resident Card';
            $this->data['service_title'] = 'Làm thẻ tạm trú cho người nước ngoài ở Việt Nam';

            $this->data['web']['seo_title'] = 'Làm thẻ tạm trú cho người nước ngoài đang sống tại Việt Nam | Trang ' . $page;
            $this->data['web']['meta_description'] = '【Thẻ tạm trú】cho người nước ngoài đảm bảo thành công 100% - Xử lý mọi quốc tịch khó';
            $this->data['web']['meta_keyword'] = 'the tam tru, xin the tam tru, xin the tam tru cho nguoi nuoc ngoai, dịch vụ xin thẻ tạm trú cho người nước ngoài, làm thẻ tạm trú';
        } else {
            $query = 'How to get Vietnam visa';
            $this->data['service_title'] = 'Công văn nhập cảnh';

            $this->data['web']['seo_title'] = 'Dịch vụ xin công văn nhập cảnh vào Việt Nam | Trang ' . $page;
            $this->data['web']['meta_description'] = '【Công văn nhập cảnh】cho người nước ngoài đảm bảo thành công 100% - Xử lý mọi quốc tịch khó';
            $this->data['web']['meta_keyword'] = 'xin cong van nhap canh,cong van nhap canh, visa viet nam online, gia han visa';
        }

        $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));


        $link = '/services/' . $alias;
        $cond = array('tips' => $query);

        $this->data['load_tips'] = $this->country_tips_model->get_for_page($this->limit, $pos, $cond);
        $total_row = $this->country_tips_model->get_total_rows($cond);
        $this->data['links'] = $this->create_pagination($this->limit, $link, $total_row, $uri_segment);

        $this->template->write_view('content_block', 'services', $this->data);
        $this->template->render();
    }

    function country($alias) {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }

        $alias = strtolower($alias);

        if (!empty($this->data['load_country'])) {
            $this->load->model('country_tips_model');
            $this->data['load_tips'] = $this->country_tips_model->get_by(array('country_id' => $this->data['load_country']['id']));


            $this->data['visa_fees_country'] = $this->cache->get('visa_fees_country');

            if (empty($this->data['visa_fees_country'])) {
                $this->load->library(array('my_category'));
                $this->data['visa_fees_country'] = $this->my_category->build_visa_fees_country();
                $this->cache->save('visa_fees_country', $this->data['visa_fees_country'], $this->cache_time);
            }


            $this->data['web']['seo_title'] = $this->data['load_country']['seo_title'] ? $this->data['load_country']['seo_title'] : 'Thủ tục xin visa ' . $this->data['load_country']['name'];
            $this->data['web']['meta_description'] = $this->data['load_country']['meta_description'];
            $this->data['web']['meta_keyword'] = $this->data['load_country']['meta_keyword'];

            if ($this->data['load_country']['icon']) {
                $this->data['seo']['img'] = '/assets/upload/country/' . $this->data['load_country']['icon'];
            }

            $this->country_model->update(array('view' => $this->data['load_country']['view'] + 1), $this->data['load_country']['id']);

            $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));

            // Post comment Fake Facebook
            if (isset($_POST['submit']) || isset($_POST['delete'])) {
                $this->post_comments('country', $alias, $this->input->post());
            }
            $data['list_comments'] = $this->show_comments($alias, 'country');
            $this->data['comments'] = $this->load->view('block/comments', $data, true);
            // end Facebook

            $this->template->write_view('content_block', 'country', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    function country_tips($country, $alias) {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }

        $alias = strtolower($alias);
        $this->load->model('country_tips_model');
        $this->data['load_tips'] = $this->country_tips_model->get_row_by(array('alias' => $alias));

        $this->data['tags'] = explode(',', $this->data['load_tips']['tags']);

        $this->data['load_country'] = $this->country_model->get_row_by(array('id' => $this->data['load_tips']['country_id']));
        //pre($this->data['load_tips']);
        if (!empty($this->data['load_tips'])) {

            $this->data['related_tips'] = $this->country_tips_model->get_by(array('country_id' => $this->data['load_tips']['country_id']), null, 'id');

            //unset($this->data['related_tips'][$this->data['load_tips']['id']]);

            $this->data['visa_fees_country'] = $this->cache->get('visa_fees_country');

            if (empty($this->data['visa_fees_country'])) {
                $this->load->library(array('my_category'));
                $this->data['visa_fees_country'] = $this->my_category->build_visa_fees_country();
                $this->cache->save('visa_fees_country', $this->data['visa_fees_country'], $this->cache_time);
            }

            $this->data['web']['seo_title'] = $this->data['load_tips']['seo_title'] ? $this->data['load_tips']['seo_title'] : $this->data['load_tips']['title'];
            $this->data['web']['meta_description'] = $this->data['load_tips']['meta_description'];
            $this->data['web']['meta_keyword'] = $this->data['load_tips']['meta_keyword'];

            if ($this->data['load_tips']['image']) {
                $this->data['seo']['img'] = '/assets/upload/country/' . $this->data['load_tips']['image'];
            }

            // Nếu là gia hạn visa Thì link đến gia hạn
            if ($this->data['load_tips']['tips'] == 'Visa extension') {
                $this->data['url']['apply'] = '/gia-han-visa';
                $this->data['seo']['bt_apply'] = $this->data['seo']['bt_renew'];
            } else {
                $this->data['url']['apply'] = '/apply-vietnamvisa/begin';
            }

            $this->country_tips_model->update(array('view' => $this->data['load_tips']['view'] + 1), $this->data['load_tips']['id']);
            $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));


            // Post comment Fake Facebook
            if (isset($_POST['submit']) || isset($_POST['delete'])) {
                $this->post_comments('country_tips', $alias, $this->input->post());
            }
            $data['list_comments'] = $this->show_comments($alias, 'country_tips');
            $this->data['comments'] = $this->load->view('block/comments', $data, true);
            // end Facebook


            $this->data['alias'] = $alias;

            $this->template->write_view('content_block', 'country_tips', $this->data);
            $this->template->render();
        } else {

            redirect(base_url());
        }
    }

    function check_visa_status() {
        $this->data['web']['seo_title'] = 'Kiểm tra tiến trình visa Việt Nam';
        $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));
        $this->template->write_view('content_block', 'check-visa-status', $this->data);
        $this->template->render();
    }

    function visa_fees($alias) {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }

        $alias = strtolower($alias);

        if (!empty($this->data['load_country'])) {
            $this->data['visa_fees_country'] = $this->cache->get('visa_fees_country');

            $this->data['country'] = $this->data['visa_fees_country'][$this->data['load_country']['id']];
            $this->data['web']['seo_title'] = 'Bảng giá visa Việt Nam dành cho người  ' . $this->data['country']['name'] . ' - Vietnam Visa Cost for ' . $this->data['country']['name'] . '';
            $this->data['web']['meta_description'] = 'Bảng giá visa Việt Nam dành cho người' . $this->data['country']['name'] . ', giá rẻ, minh bạch chi phí ';
            $this->data['web']['meta_keyword'] = 'vietnam visa fee, vietnam visa cost, visa fee for ' . $this->data['country']['name'] . ', vietnam visa cost for ' . $this->data['country']['name'];

            $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));
            $this->template->write_view('content_block', 'visa_fees', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    function page($alias = '') {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }

        $this->load->model(array('category_news_model', 'news_model', 'page_model'));
        $alias = strtolower($alias);
        $this->data['page'] = $this->page_model->get_row_by(array('alias' => $alias));

        $this->data['web']['seo_title'] = $this->data['page']['seo_title'] ? $this->data['page']['seo_title'] : $this->data['page']['title'];
        $this->data['web']['meta_description'] = $this->data['page']['meta_description'] ? $this->data['page']['meta_description'] : $this->data['page']['title'];
        $this->data['web']['meta_keyword'] = $this->data['page']['meta_keyword'] ? $this->data['page']['meta_keyword'] : $this->data['page']['title'];

        $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));

        // Post comment Fake Facebook
        if (isset($_POST['submit']) || isset($_POST['delete'])) {
            $this->post_comments('page', $alias, $this->input->post());
        }
        $data['list_comments'] = $this->show_comments($alias, 'page');
        $this->data['comments'] = $this->load->view('block/comments', $data, true);
        // end Facebook

        if (!empty($this->data['page'])) {
            $this->page_model->update(array('view' => $this->data['page']['view'] + 1), $this->data['page']['id']);
            $this->template->write_view('content_block', 'page', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    private function get_page_news($page = 1, $url = '', $cond = null, $uri_segment = 3) {
        $page = intval($page);
        $page < 1 ? 1 : $page;
        $pos = ($page - 1) * $this->limit;
        $this->data['list_news'] = $this->news_model->get_for_page($this->limit, $pos, $cond);
        $total_row = $this->news_model->get_total_rows($cond);
        $this->data['links'] = $this->create_pagination($this->limit, $url, $total_row, $uri_segment);
    }

    private function create_pagination($limit, $url, $count, $uri_segment = 2) {
        $this->load->library('my_pagination');
        $config['per_page'] = $limit;
        $config['base_url'] = $url;
        $config['uri_segment'] = $uri_segment;
        $config['use_page_numbers'] = true;
        $config['full_tag_open'] = '<p>';
        $config['full_tag_close'] = '</p>';
        $config['total_rows'] = $count;

        $this->my_pagination->initialize($config);
        return $this->my_pagination->create_links();
    }

    function category_news($alias = '', $page = 1) {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }
        $alias = strtolower($alias);
        $this->load->helper('text');
        $this->load->model(array('category_news_model', 'news_model', 'page_model'));
        $this->data['category_news'] = $this->category_news_model->get_row_by(array('alias' => $alias));
        if (!empty($this->data['category_news'])) {
            $link = '/category/' . $alias;
            $cond = 'id_cat =' . $this->data['category_news']['id'];
            $uri_segment = 3;

            $this->data['web']['seo_title'] = $this->data['category_news']['seo_title'] ? $this->data['category_news']['seo_title'] : $this->data['category_news']['title'];
            $this->data['web']['seo_title'] .= ' | Page ' . $page;

            $this->data['category_news']['meta_description'] ? $this->data['web']['meta_description'] = $this->data['category_news']['meta_description'] : '';
            $this->data['category_news']['meta_keyword'] ? $this->data['web']['meta_keyword'] = $this->data['category_news']['meta_keyword'] : '';

            $this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));
            $this->get_page_news($page, $link, $cond, $uri_segment);
        } else {
            redirect(base_url());
        }



        $this->template->write_view('content_block', 'news-list', $this->data);
        $this->template->render();
    }

    function post_comments($controller = '', $alias = '', $data = '') {
        $this->load->helper('directory');
        $this->load->model(array('comments_model'));
        $controller = $this->security->xss_clean($controller);
        $alias = $this->security->xss_clean($alias);

        $path = APPPATH . '../assets/user/avatar/';
        $folder_avatar = directory_map($path);

        $data['avatar'] = $folder_avatar[rand(0, count($folder_avatar) - 1)];

        if (isset($data)) {
            if (@$data['delete']) {
                $id = intval($data['delete']);
                $this->comments_model->delete($id);
                return false;
            }
            if ($data['name'] && $data['sub_title'] && $data['avatar'] && $data['date_create'] && $data['content'] && $this->data['user']['username']) {
                $data = array(
                    'name' => $data['name'],
                    'alias' => $alias,
                    'avatar' => $data['avatar'],
                    'sub_title' => $data['sub_title'],
                    'content' => $data['content'],
                    'link_facebook' => $data['link_facebook'],
                    'link_avatar' => $data['link_avatar'],
                    'date_create' => date('Y-m-d H:i:s', strtotime($data['date_create'])),
                    'page' => $controller,
                    'author_username' => $this->data['user']['username'],
                    'status' => 'show'
                );
                $this->comments_model->insert($data);
                $this->data['post_sucess'] = 1;
            }
        }
        return true;
    }

    function show_comments($alias, $page) {
        $alias = $this->security->xss_clean($alias);
        $page = $this->security->xss_clean($page);
        $this->load->model(array('comments_model'));
        $data = $this->comments_model->get_by(array('alias' => $alias, 'page' => $page, 'status' => 'show'));
        return $data;
    }

    function sitemap() {
        $time = intval(@file_get_contents(APPPATH . 'cache/sitemaps_cache'));
        if ($time + 86400 <= time()) {
            file_put_contents(APPPATH . 'cache/sitemaps_cache', time());
            $this->load->model(array('category_news_model', 'news_model', 'page_model', 'country_model', 'country_tips_model'));

            $country = $this->cache->get('list_country');
            if (empty($country)) {
                $this->load->library(array('my_category'));
                $country = $this->country_model->get_all(NULL, 'id');
                $this->cache->save('list_country', $country, $this->cache_time);
            }
            $xml = '<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><url><loc>' . base_url() . '</loc><priority>1.0</priority></url>';


            //
            $xml .= "<url><loc>" . base_url() . "info/vietnamvisa-on-arrival</loc><priority>1.0</priority></url>";
            $xml .= "<url><loc>" . base_url() . "apply-vietnamvisa/begin</loc><priority>1.0</priority></url>";
            $xml .= "<url><loc>" . base_url() . "rss</loc><priority>1.0</priority></url>";
            // Page
            $page = $this->page_model->get_site_maps();
            if (!empty($page)) {
                foreach ($page as $item) {
                    $xml .= "<url><loc>" . base_url() . "page/{$item['alias']}</loc><priority>1.0</priority></url>";
                }
            }

            // Danh muc tin tuc
            $cat_news = $this->category_news_model->get_site_maps();
            if (!empty($cat_news)) {
                foreach ($cat_news as $item) {
                    $xml .= "<url><loc>" . base_url() . "category/{$item['alias']}</loc><priority>1.0</priority></url>";
                }
            }


            // Bai viet moi nhat
            $article = $this->news_model->get_site_maps();
            if (!empty($article)) {
                foreach ($article as $item) {
                    $xml .= "<url><loc>" . base_url() . "article/{$item['alias']}</loc><priority>1.0</priority></url>";
                }
            }
            // Quoc gia & Fee

            if (!empty($country)) {
                foreach ($country as $item) {
                    $xml .= "<url><loc>" . base_url() . "country-{$item['alias']}</loc><priority>1.0</priority></url>";
                    $xml .= "<url><loc>" . base_url() . "visa-fees/{$item['alias']}</loc><priority>1.0</priority></url>";
                }
            }


            // Tips
            $tips = $this->country_tips_model->get_site_maps();
            if (!empty($tips)) {
                foreach ($tips as $item) {
                    $xml .= "<url><loc>" . base_url() . "country-{$country[$item['country_id']]['alias']}/{$item['alias']}</loc><priority>1.0</priority></url>";
                }
            }


            $xml .= '</urlset>';
            file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/sitemaps.xml', $xml);
        }
        header("content-type: text/xml");
        echo @file_get_contents('sitemaps.xml');
    }

    function rss($prefix = '', $alias = '') {

        $this->load->model(array('country_model', 'category_news_model', 'news_model', 'country_tips_model'));
        $this->limit = 20;
        $date = date(DATE_RFC2822);
        $base_url = base_url();
        $prefix = $this->security->xss_clean($prefix);
        $alias = $this->security->xss_clean($alias);
        $img = '';
        if ($prefix == 'article') {
            if ($alias == 'latest') {
                $cond = array('status' => 1);
            } else {
                $category_news = $this->category_news_model->get_row_by(array('alias' => $alias));
                $cond = array('id_cat' => $category_news['id'], 'status' => 1);
            }

            $news = $this->news_model->get_for_page(50, 0, $cond, 'id DESC');

            $html = '<?xml version="1.0" encoding="UTF-8"?>
                    <rss version="2.0" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
                      <channel>
                        <title> Article Site map RSS</title>
                        <description> RSS</description>
                        <pubDate>' . $date . '</pubDate>
                        <generator>' . $base_url . 'rss</generator>
                        <link>' . current_url() . '</link>';

            foreach ($news as $item) {
                $url = $base_url . $prefix . '/' . $item['alias'];

                if ($item['image']) {
                    $img = '<img width=214 height=272 src="' . $base_url . 'assets/upload/news/' . $item['image'] . '" ></a>';
                }

                $html .= '<item>
                            <title>' . $item['title'] . '</title>
                            <description><![CDATA[<a href="' . $url . '">
                            ' . $img . '
                            </br>' . ($item['description'] ? $item['description'] : $item['title']) . '.]]></description>
                            <pubDate>' . $date . '</pubDate>
                            <link>' . $url . '</link>
                            <guid>' . $url . '</guid>
                            <slash:comments>0</slash:comments>
                          </item>';
            }
            header("content-type: text/xml");
            echo $html . '</channel></rss>';
        } elseif ($prefix == 'country') {
            $country = $this->cache->get('list_country');
            if (empty($country)) {
                $this->load->library(array('my_category'));
                $country = $this->country_model->get_all(NULL, 'id');
                $this->cache->save('list_country', $country, $this->cache_time);
            }

            if ($alias == 'latest') {
                $load_tips = $this->country_tips_model->get_all();
            } else {
                $alias = explode('-', $alias);
                $load_tips = $this->country_tips_model->get_by(array('country_id' => $alias[1]), 'id DESC');
            }

            $html = '<?xml version="1.0" encoding="UTF-8"?>
                    <rss version="2.0" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
                      <channel>
                        <title> Vietnam visa for  Site map RSS</title>
                        <description> RSS</description>
                        <pubDate>' . $date . '</pubDate>
                        <generator>' . $base_url . 'rss</generator>
                        <link>' . current_url() . '</link>';

            foreach ($load_tips as $item) {

                $url = $base_url . 'country-' . $country[$item['country_id']]['alias'] . '/' . $item['alias'];

                if ($item['image']) {
                    $img = '<img width=214 height=272 src="' . $base_url . 'assets/upload/country/tips/' . $item['image'] . '" ></a>';
                }

                $html .= '<item>
                            <title>' . $item['title'] . '</title>
                            <description><![CDATA[<a href="' . $url . '">
                            ' . $img . '
                            </br>' . ($item['meta_description'] ? $item['meta_description'] : $item['title']) . '.]]></description>
                            <pubDate>' . $date . '</pubDate>
                            <link>' . $url . '</link>
                            <guid>' . $url . '</guid>
                            <slash:comments>0</slash:comments>
                          </item>';
            }
            header("content-type: text/xml");
            echo $html . '</channel></rss>';
        } else {
            $this->data['page_title'] = 'RSS SITE MAP';
            $this->data['list_country'] = $this->country_model->get_all();
            $this->data['list_news'] = $this->category_news_model->get_all();
            $this->data['list_page'] = $this->page_model->get_all();
            $this->template->write_view('content_block', 'rss', $this->data);
            $this->template->render();
        }
    }

    function ampify($html = '') {
        # Replace img, audio, and video elements with amp custom elements
        $html = preg_replace('/(<[^>]+) data-cke-saved-src=".*?"/i', '$1', $html);
        $html = str_replace('/>', '>', $html);
        $html = str_replace('/ >', '>', $html);

        $tags_old = array('<video', '/video>', '<audio', '/audio>');
        $tags_amp = array('<amp-video', '/amp-video>', '<amp-audio', '/amp-audio>');
        $html = str_ireplace($tags_old, $tags_amp, $html);


        # Add closing tags to amp-img custom element
        // $html = preg_replace('/<amp-img(.*?)>/', '<amp-img$1 width="230" height="150"></amp-img>', $html);
        # Whitelist of HTML tags allowed by AMP
        $html = strip_tags($html, '<img><h1><h2><h3><h4><h5><h6><a><p><ul><ol><li><blockquote><q><cite><ins><del><strong><em><code><pre><svg><table><thead><tbody><tfoot><th><tr><td><dl><dt><dd><article><section><header><footer><aside><figure><time><abbr><div><span><hr><small><br><amp-img><amp-audio><amp-video><amp-ad><amp-anim><amp-carousel><amp-fit-rext><amp-image-lightbox><amp-instagram><amp-lightbox><amp-twitter><amp-youtube>');
        return $html;
    }

    function split_content($tag_start, $tag_end, $str) {
        $temp = '';
        $temp1 = '';
        $result = '';
        $temp = explode($tag_start, $str);
        if (count($temp) > 2) {
            for ($i = 1; $i < count($temp); $i++) {
                $temp1 = explode($tag_end, $temp[$i]);
                $result[] = $temp1[0];
            }
        } else {
            $temp1 = explode($tag_end, $temp[1]);
            $result = $temp1[0];
        }
        return $result;
    }

    function generateClassByStyleInline($html) {
        $class = array();

        preg_match_all('#<*(style(.+?)"[^"]*")[^>]*#is', $html, $matches);
        for ($i = 0, $c = count($matches[0]); $i < $c; $i++) {
            $css = $this->split_content('"', '"', $matches[0][$i]);
            if ($css[0]) {
                $keyClass = 'CAMP-' . $i;
                if (!@$class[$keyClass]) {
                    $class[$keyClass] = '.' . $keyClass . ' { ' . $css[0] . ' }' . PHP_EOL;
                }
                $html = str_replace($matches[0][$i], "class=\"{$keyClass}\"", $html);
            }
        }

        preg_match_all('#<img(.+?)>#is', $html, $matcheImages);
        for ($i = 0, $c = count($matcheImages[0]); $i < $c; $i++) {
            $append = '';
            if (!strpos($matcheImages[0][$i], 'width')) {
                $append .= ' width="350"';
            }
            if (!strpos($matcheImages[0][$i], 'height')) {
                $append .= ' height="200"';
            }
            $tmp = str_replace(array('<img', '>'), array('<amp-img', $append . '></amp-img>'), $matcheImages[0][$i]);
            $html = str_replace($matcheImages[0][$i], $tmp, $html);
        }

        $data = array(
            'html' => $html,
            'style' => implode('', $class)
        );

        return $data;
    }

    ////////// tri add code

    function news($id) {
        $id = intval($id);
        $this->load->model(array('category_news_model', 'news_model', 'page_model'));
        $this->data['detail'] = $this->news_model->get_by_key($id);

        $alias = $this->data['detail']['alias'];
        if (!empty($this->data['detail'])) {
            $this->load->helper('text');
            $this->data['cate_news'] = $this->category_news_model->get_by_key($this->data['detail']['cate_id']);
            $this->data['web']['seo_title'] = $this->data['detail']['seo_title'] ? $this->data['detail']['seo_title'] : $this->data['detail']['title'];
            $this->data['detail']['meta_description'] ? $this->data['web']['meta_description'] = $this->data['detail']['meta_description'] : '';
            $this->data['detail']['meta_keyword'] ? $this->data['web']['meta_keyword'] = $this->data['detail']['meta_keyword'] : '';
            $this->data['pre'] = $this->data['cate_news']['alias'];
            if ($this->data['detail']['image']) {
                $this->data['seo']['img'] = '/assets/upload/news/fb_' . $this->data['detail']['image'];
            } else {
                $this->data['seo']['img'] = '/assets/upload/news/' . $this->data['detail']['image'];
            }

            $cond = 'cate_id =' . $this->data['detail']['cate_id'] . ' and `status` <= 1';

            //$this->data['widget'] = $this->load_block(array('support', 'payment', 'ads'));

            $this->news_model->update(array('view' => $this->data['detail']['view'] + 1), $this->data['detail']['id']);


            $this->data['tags'] = explode(',', $this->data['detail']['tags']);
            $this->data['list_news'] = $this->news_model->get_for_page(8, 0, $cond, 'rand()');


            if ($this->data['detail']['news_ids'] != ',' && !empty($this->data['detail']['news_ids'])) {
                $temp = '(' . trim($this->data['detail']['news_ids'], ',') . ')';
                $cond2 = 'id   in' . $temp;
                $this->data['news_connect'] = $this->news_model->get_by($cond2, null);
            }

            if ($this->uri->segment(3) == 'amp') {
                $this->data['detail']['data'] = $this->generateClassByStyleInline($this->ampify($this->data['detail']['content']));
                $this->load->view('amp/amp-news', $this->data);
            } else {
                $this->data['amp_header'] = '<link rel="amphtml" href="' . current_url() . '/amp">';

                // Post comment Fake Facebook
                if (isset($_POST['submit']) || isset($_POST['delete'])) {
                    $this->post_comments('news', $alias, $this->input->post());
                }
                $this->data['tags'] = explode(",", $this->data['detail']['tags']);
                $this->data['list_comments'] = $this->show_comments($alias, 'news');
                $this->data['comments'] = $this->load->view('block/comments', $this->data, true);
                // end Facebook
                $this->template->write_view('content_block', 'index/news_detail', $this->data);
                $this->template->render();
            }
        } else {
            redirect(base_url());
        }
    }

    function detail_cate($alias = "", $alias_detail = "") {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }

        if (empty($alias))
            redirect(base_url());
        else {
            $this->data['pre'] = 'service';
            $this->data['cat_service'] = $this->category_service_model->get_row_by('alias like ' . "'$alias'");
            //$this->data['cat_service'] = $this->service_detail_model->get_row_by('id like ' . $this->data['detail']['service_id']);
            $this->data['list_service'] = $this->service_detail_model->get_by('service_id like ' . $this->data['cat_service']['id'] . " and schedule <= '" . date('Y-m-d') . "'");
            if (empty($alias_detail)) {
                $alias_detail = $this->service_detail_model->get_row_by('service_id like ' . $this->data['cat_service']['id'])['alias'];
                redirect(base_url() . 'dich-vu/' . $alias . '/' . $alias_detail);
            } else
                $this->data['detail'] = $this->service_detail_model->get_row_by('alias like ' . "'$alias_detail'" . " and service_id = '" . $this->data['cat_service']['id'] . "'");
            $this->service_detail_model->update(array('view' => $this->data['detail']['view'] + 1), $this->data['detail']['id']);
            $this->data['question'] = $this->service_question_model->get_by('cat_ids like ' . "'%," . $this->data['detail']['id'] . ",%'");
            $this->data['web']['seo_title'] = 'Dịch vụ nhập cảnh, lưu trú, visa';
            $this->data['web']['meta_description'] = 'Thủ tục xin visa Việt Nam theo danh sách từng quốc gia ';
            $this->data['web']['meta_keyword'] = 'dich vu xuat nhap canh, dich vu gia han visa, the tam tru cho nguoi nuoc ngoai o vietnam ';
            // Post comment Fake Facebook
            $alias_detail = $this->data['detail']['alias'];
            if (isset($_POST['submit']) || isset($_POST['delete'])) {
                $this->post_comments('service_detail', $alias_detail, $this->input->post());
            }

            $this->data['list_comments'] = $this->show_comments($alias_detail, 'service');
            $this->data['comments'] = $this->load->view('block/comments', $this->data, true);

            // end Facebook
            $this->template->write_view('content_block', 'service_detail', $this->data);
            $this->template->render();
        }
    }

    function news_cat($alias = "", $page = 1) {
        if ($alias == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }
        $this->limit = 7;
        $this->load->model(array('category_news_model', 'news_model', 'page_model', 'gallery_model'));
        $cond = array("alias" => $alias);
        $this->data['category_news'] = $this->category_news_model->get_row_by(array('alias' => $alias));
        if (!empty($this->data['category_news'])) {
            $this->data['pre'] = $this->data['category_news']['alias'];
            $link = '/' . $alias;
            $cond = 'cate_id =' . $this->data['category_news']['id'] . " and schedule <= '" . date('Y-m-d') . "'";
            $uri_segment = 2;
            $this->data['web']['seo_title'] = $this->data['category_news']['seo_title'] ? $this->data['category_news']['seo_title'] : $this->data['category_news']['title'];
            $this->data['web']['seo_title'] .= ' | Page ' . $page;
            $this->data['category_news']['meta_description'] ? $this->data['web']['meta_description'] = $this->data['category_news']['meta_description'] : '';
            $this->data['category_news']['meta_keyword'] ? $this->data['web']['meta_keyword'] = $this->data['category_news']['meta_keyword'] : '';
            $page = intval($page);
            $page < 1 ? 1 : $page;
            $pos = ($page - 1) * $this->limit;
            $this->data['list_news'] = $this->news_model->get_for_page($this->limit, $pos, $cond);
            $total_row = $this->news_model->get_total_rows($cond);
            $this->data['links'] = $this->create_pagination($this->limit, $link, $total_row, $uri_segment);
            $this->template->write_view('content_block', 'index/news_cat', $this->data);
            $this->template->render();
        } else
            redirect(base_url() . '404_override');
    }

    function contact() {
        $this->load->library(array('alias', 'form_validation'));
        $this->data['web']['seo_title'] = 'Liên hệ chúng tôi';
        $this->data['pre'] = 'contact';
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('email', 'email', 'required');
            $this->form_validation->set_rules('phone', 'phone', 'required');
            $this->form_validation->set_rules('message', 'message', 'required');
            if ($this->form_validation->run() == FALSE) {
                $this->data['check_error'] = 1;
            } else {
                $this->load->model(array('call_back_model'));
                $add_comment = array(
                    'name' => @$_POST['fullname'],
                    'phone' => @$_POST['phone'],
                    "email" => @$_POST['email'],
                    "content" => @$_POST['message'],
                    "status" => 0,
                    "date_create" => date('Y-m-d H:i:s'),
                );
                $id = $this->call_back_model->insert($add_comment);
                !empty($id) ? $this->data['msg'] = "<i class='fa fa-check'></i>Cảm ơn bạn gửi thông tin, Chúng tôi sẽ gọi lại ngay khi nhận" : redirect();
            }
        }
        if (($_POST['request'])) {
            $this->load->model(array('call_back_model'));
            $add_comment = array(
                'phone' => @$_POST['phone'],
                "content" => "",
                "status" => 0,
                "date_create" => date('Y-m-d H:i:s'),
            );
            $id = $this->call_back_model->insert($add_comment);
            echo!empty($id) ? 'Visatravel sẽ gọi lại quý khách hàng ngay sau khi nhận được ' : redirect();
            exit;
        }
        $this->template->write_view('content_block', 'index/contact', $this->data);
        $this->template->render();
    }

    function tag($tag, $page = 1) {
        if ($tag == 'assets') {
            header("HTTP/1.0 404 Not Found");
            return false;
        }
        if ($tag != "") {
            $tag = urldecode($this->security->xss_clean($tag));
            $this->data['tag'] = $tag;
            $this->limit = 6;
            $this->load->model(array('news_model', 'page_model', 'gallery_model'));
            $cond = "tags like'%" . $tag . "%'";
            $link = '/tag/' . urlencode($tag);
            $uri_segment = 3;
            $page = intval($page);
            $page < 1 ? 1 : $page;
            $pos = ($page - 1) * $this->limit;
            $this->data['list_news'] = $this->news_model->get_for_page($this->limit, $pos, $cond);
            $total_row = $this->news_model->get_total_rows($cond);
            $this->data['links'] = $this->create_pagination($this->limit, $link, $total_row, $uri_segment);
        }
        $this->template->write_view('content_block', 'index/tag', $this->data);
        $this->template->render();
    }

    function faq() {
        $this->data['question'] = $this->service_question_model->get_all();
        $this->data['web']['seo_title'] = 'Dịch vụ nhập cảnh, lưu trú, visa';
        $this->data['web']['meta_description'] = 'Thủ tục xin visa Việt Nam theo danh sách từng quốc gia ';
        $this->data['web']['meta_keyword'] = 'dich vu xuat nhap canh, dich vu gia han visa, the tam tru cho nguoi nuoc ngoai o vietnam ';
        $this->template->write_view('content_block', 'index/faq', $this->data);
        $this->template->render();
    }

    function get_id($alias) {
        $array_alias = explode("-", $alias);
        return $id = $array_alias[count($array_alias) - 1];
    }

    function search() {
        $this->load->helper('my_helper');
        $this->load->model(array('news_model', 'keyword_model'));
        if (isset($_GET['q'])) {
            $this->data['keyword'] = $keyword = urldecode($this->security->xss_clean($_GET['q']));
            $this->data['web']['seo_title'] = 'Kết quả tìm kiếm từ khóa : ' . $this->data['keyword'];
            $this->data['web']['meta_description'] = $this->data['keyword'];
            $this->data['web']['meta_keyword'] = $this->data['keyword'];
            $this->keyword_model->update_or_add($keyword);
            $this->data['list_keyword'] = $this->keyword_model->get_keyword();
            $this->data['list_news'] = $this->news_model->get_by("alias like '%" . $this->sanitizeTitle($keyword) . "%' or " . "title like '%" . $this->sanitizeTitle($keyword) . "%'");
            $this->data['list_news'] = $this->news_model->get_by("alias like '%" . $this->sanitizeTitle($keyword) . "%'");
            $this->template->write_view('content_block', 'index/search', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    function sanitizeTitle($string) {
        if (!$string)
            return false;
        $utf8 = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach ($utf8 as $ascii => $uni)
            $string = preg_replace("/($uni)/i", $ascii, $string);
        $string = utf8Url($string);
        return $string;
    }

    function direct_link($alias_cate = "", $alias_detail = "") {
        if ($alias_cate && $alias_detail) {
            $url_old = "http://visatravel.vn/" . $alias_cate . "/" . $alias_detail . '.html';
            $item_detail = $this->news_model->get_row_by("url_source = '" . $url_old . "'");
            if ($item_detail)
                redirect(base_url() . $item_detail['alias'] . '-' . $item_detail['id'] . ".html");
            else
                redirect(base_url());
        } else
            redirect(base_url());
    }

    function error_404() {

        $this->load->helper('text');
        $this->template->write_view('content_block', 'index/error_404', $this->data);
        $this->template->render();
    }

}
