<?php

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('call_back_model', 'category_service_model', 'service_detail_model', 'service_question_model', 'category_news_model', 'news_model', 'page_model', 'gallery_model', 'admin_model'));
//        if (empty($table))
//            pre("ERROR table");
    }

    /* ===== START Editor for DATABASE ====== */

    /* FOR NOTE all data wrong return string : GET_ERROR/DELETE_ERROR/ADD_ERROR/EDIT_ERROR */

    function get_for_page() {
        $table = @$_POST['table'];
        if (!empty($table)) {
            $pos = intval($_POST['pos']);
            $cond = $_POST['cond'];
            $limit = @$_POST['limit'];
            $order = @$_POST['order'];
            $data['results'] = $this->$table->get_for_page($limit, $pos, $cond, $order);
            $data['total'] = $this->$table->get_total_rows($cond);
            print_r(json_encode($data));
        } else
            echo "GET_ERROR";
    }

    function update_batch() {
        $table = @$_POST['table'];
        if (!empty($table) && !empty($_POST['set'])) {
            $set = json_decode($_POST['set'], true);
            $index = !empty($_POST['index']) ? $_POST['index'] : "id";
            $this->$table->update_batch($set, $index);
            echo "OK";
        } else
            echo "GET_ERROR";
    }

    function query() {
        $table = @$_POST['table'];
        if (!empty($table) && !empty($_POST['query'])) {
            print_r(json_encode($this->$table->query($_POST['query'])->result_array()));
        } else
            echo "QUERY_ERROR";
    }

    function delete() {
        $table = @$_POST['table'];
        if (!empty($table)) {
            $this->$table->delete($_POST['id']);
            echo "OK";
        } else
            echo "DELETE_ERROR";
    }

    function get_by() {

        $table = @$_POST['table'];
        if (!empty($table)) {
            $cond = !empty(@$_POST['cond']) ? $_POST['cond'] : "";
            $order = !empty(@$_POST['order']) ? $_POST['order'] : null;
            $rex_index = !empty(@$_POST['rex_index']) ? $_POST['rex_index'] : "";
            $field = !empty(@$_POST['field']) ? $_POST['field'] : null;
            $data = $this->$table->get_by($cond, $order, $rex_index, $field);
            print_r(json_encode($data));
        } else
            echo "GET_ERROR";
    }

    function get_by_key() {
        $table = @$_POST['table'];
        if (!empty($table)) {
            $id = !empty(@$_POST['id']) ? $_POST['id'] : "";
            $data = $this->$table->get_by_key($id);
            print_r(json_encode($data));
        } else
            echo "GET_ERROR";
    }

    function get_all() {
        $table = @$_POST['table'];
        if (!empty($table)) {
            $id = !empty(@$_POST['id']) ? $_POST['id'] : "";
            $data = $this->$table->get_all();
            print_r(json_encode($data));
        } else
            echo "GET_ERROR";
    }

    function update() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data']);
        if (!empty($table) && isset($_POST['id'])) {
            $this->$table->update($data_update, $_POST['id']);
            echo "OK";
        } else
            echo "UPDATE_ERROR";
    }

    function insert() {
        $table = @$_POST['table'];
        $data = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data)) {
            echo $this->$table->insert($data);
        } else
            echo "ADD_ERROR";
    }

    /* ====== put_file_content tmp for news and service when edit  ===== */

    function put_tmp() {
        $tmp_name = $_POST['tmp_name'];
        if (!empty($tmp_name) && $_POST['data'] && !empty($_POST['folder'])) {
            @mkdir(APPPATH . '/cache/service_update', 0777);
            @unlink(APPPATH . 'cache/service_update/' . $tmp_name);
            file_put_contents(APPPATH . 'cache/' . $_POST['folder'] . '/' . $tmp_name, serialize(json_decode($_POST['data'], true)));
            echo '';
        } else
            echo "EDIT_ERROR";
    }

    function get_tmp() {
        $tmp_name = $_POST['tmp_name'];
        if (!empty($tmp_name) && !empty($_POST['folder']))
            print_r(json_encode(@file_get_contents(APPPATH . 'cache/' . $_POST['folder'] . '/' . $tmp_name)));
        else
            echo 'ERROR_GET';
    }

    function delete_tmp() {
        $tmp_name = $_POST['tmp_name'];
        if (!empty($tmp_name) && !empty($_POST['folder']))
            @unlink(APPPATH . 'cache/' . $_POST['folder'] . '/' . $tmp_name);
        else
            echo 'ERROR_GET';
    }

    /* ===== END Editor for DATABASE ===== */

    /* editor  for comment */


    /* editor  for comment */


    /* editor for category_service === */

    function insert_category_service() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data_add'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'category_service', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'category_service/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                return $data_add += ['icon' => $image];
            }
            echo $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_category_service() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data_add'], true);
        if (!empty($table) && !empty($data_update) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'category_service', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'category_service/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update += ['icon' => $image];
            }
            $this->$table->update($data_update, $id);
        } else
            echo "ADD_ERROR";
    }

    /* End category_service */


    /* editor for category_service === */

    function insert_service_detail() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data_add'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'service_detail', 0777);
            $path = PATH_UPLOAD . 'service_detail/';
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'service_detail/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));

                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                    $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_add['image'] = $image;
            }
            echo $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_service_detail() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data_update'], true);
        $id = $_POST['id'];
        if (!empty($table) && !empty($data_update)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . '/category_service', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'service_detail/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));

                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . $img['file_name'], 650, 400);
                    $this->resize_image->crop_resize($img['full_path'], $img['file_path'] . 'thumb_' . $img_name, 50, 36);
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update['image'] = $image;
            }
            $this->$table->update($data_update, $id);
        } else
            echo "ADD_ERROR";
    }

    /* End category_service */

    /* editor for category_service === */

    function insert_category_news() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'category_news', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'category_news/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }

                $data_add['image'] = $image;
            }
            return $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_category_news() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data'], true);

        if (!empty($table) && !empty($data_update) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . '/category_news', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'category_news/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update['image'] = $image;
            }
            $this->$table->update($data_update, $id);
        } else
            echo "UPDATE_ERROR";
    }

    /* End category_service */

    /* editor for category_service === */

    function insert_news() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'news', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'news/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }

                $data_add['image'] = $image;
            }
            echo $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_news() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data_update) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'news', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'news/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update['image'] = $image;
            }
            $this->$table->update($data_update, $id);
        } else
            echo "UPDATE_ERROR";
    }

    /* End category_service */

    /* editor for PAGE === */

    function insert_page() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'page', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'page/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }

                $data_add['image'] = $image;
            }
            echo $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_page() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data'], true);

        if (!empty($table) && !empty($data_update) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . '/page', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'page/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update['image'] = $image;
            }
            $this->$table->update($data_update, $id);
        } else
            echo "UPDATE_ERROR";
    }

    /* End PAGE */


    /* editor for PAGE === */

    function insert_gallery() {
        $table = @$_POST['table'];
        $data_add = json_decode($_POST['data'], true);
        if (!empty($table) && !empty($data_add)) {
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . 'gallery', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'gallery/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }

                $data_add['image'] = $image;
            }
            echo $this->$table->insert($data_add);
        } else
            echo "ADD_ERROR";
    }

    function update_gallery() {
        $table = @$_POST['table'];
        $data_update = json_decode($_POST['data'], true);

        if (!empty($table) && !empty($data_update) && !empty($_POST['id'])) {
            $id = $_POST['id'];
            $image = $_FILES['file'];
            @mkdir(PATH_UPLOAD . '/gallery', 0777);
            if (!empty($image['name'])) {
                $this->load->library(array('alias', 'upload', 'resize_image'));
                $this->upload->initialize(array(
                    'upload_path' => PATH_UPLOAD . 'gallery/',
                    'allowed_types' => 'gif|jpg|png',
                    'overwrite' => false,
                    'file_name' => $data_add['alias'] . '-' . $image['name']
                ));
                if ($this->upload->do_upload('file')) {
                    $img = $this->upload->data();
                    $image = $img['file_name'];
                } else {
                    $this->data['check_error'] = 1;
                    $this->data['msg'] = $this->upload->display_errors();
                }
                $data_update['image'] = $image;
            }
            $this->$table->update($data_update, $id);
        } else
            echo "UPDATE_ERROR";
    }

    /* End PAGE */

    /* editor for homeinfo === */

    function homeinfo() {
        if (!empty($_POST['seo_title'])) {
            file_put_contents(APPPATH . 'cache/seo_home', serialize(json_decode($_POST['data'], true)));
        } else {
            print_r(json_encode(@file_get_contents(APPPATH . 'cache/seo_home')));
        }
    }

    /* End homeinfo */

    function hotline() {
        if (!empty($_POST['phone'])) {
            file_put_contents(APPPATH . 'cache/phone', serialize(json_decode($_POST['phone'], true)));
        } else {
            print_r(json_encode(@file_get_contents(APPPATH . 'cache/phone')));
        }
    }

    function check_network() {
        $site_ = @$_POST['website'];
        $token_ = @$_POST['token'];
        if ($site_ == DOMAIN_SITE && $token_ == TOKEN_SITE)
            echo "OK";
        else
            echo "ERROR";
    }

    function get_token() {
        $site_admin = @$_POST['site_admin'];
        if ($site_admin == SITE_ADMIN)
            echo TOKEN_SITE;
        else
            echo "";
    }

    function get_phone() {
        $position = $_POST['position'];
        $result = @file_get_contents(APPPATH . 'cache/phone');
        if ($result) {
            $data = unserialize($result);
            if ($position < 0 || $position > count($data)) {
                echo "";
            } else {
                $i = 1;
                foreach ($data as $key => $value) {
                    if ($i == $position) {
                        echo "<a href='tel:" . $key . "'>" . $value . "</a>";
                        exit;
                    } else
                        $i++;
                }
            }
        }
    }

    function change_line() {
        if (isset($_POST['phone'])) {
            $update = json_decode($_POST['phone'], true);
            file_put_contents(APPPATH . 'cache/phone', serialize($update));
        }
    }

}
