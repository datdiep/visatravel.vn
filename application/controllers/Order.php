<?php

/**
 * @property order_services_model $order_services_model
 */
class Order extends FONTEND_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model(array('order_services_model','gallery_model'));
        $this->data['sliders'] = $this->gallery_model->get_by('type = "slider"');
        $this->data['html_order'] = $this->gallery_model->get_by('type = "html"', null, 'title');
        $this->data['order_key'] = '%--?enReQ#$6JwVw';
    }

    function select_location() {
        $id = intval(@$_POST['id']);
        if ($id > 0) {
            $this->load->model('location_model');
            $location = $this->location_model->get_by('parent = ' . $id, 'priority DESC,name ASC');
            if (!empty($location)) {
                echo json_encode($location);
            } else {
                echo '';
            }
        }
        exit;
    }

    function check_voucher() {
        if (isset($_POST['voucher'])) {
            $this->load->model('voucher_model');
            $voucher = $this->voucher_model->get_row_by('code = "' . $_POST['voucher'] . '" and status = "unused"');
            $return['error'] = 1;
            if (!empty($voucher)) {
                $return['error'] = 0;
                $return['price'] = $voucher['price'];
                $return['type'] = $voucher['type'];
            }
            echo json_encode($return);
        }
    }

    function del_product() {
        $sku_id = intval(@$_POST['sku_id']);
        if ($sku_id > 0) {
            $maps_count_product = $this->session->userdata('maps_count_product');
            if ($maps_count_product) {
                $maps_count_product = unserialize($maps_count_product);
            }
            unset($maps_count_product[$sku_id]);
            $this->session->set_userdata('maps_count_product', serialize($maps_count_product));
            $this->get_total_money($maps_count_product);
        }
        exit;
    }

    function add_product() {
        //unset($_SESSION['maps_count_product']);
        $parrams = $this->security->xss_clean($_POST);
        $product_id = intval(@$parrams['product_id']);
        $quantity = intval(@$parrams['quantity']);
        $color = @$parrams['color'];
        $size = @$parrams['size'];
        if ($product_id > 0) {
            ($quantity < 1 || $quantity > 11) ? $quantity = 1 : '';
            $this->load->model('sku_model');
            $cond = 'product_id = ' . $product_id;
            $color ? $cond .= ' and color = "' . $color . '"' : '';
            $size ? $cond .= ' and size = "' . $size . '"' : '';
            $sku = $this->sku_model->get_row_by($cond);
            //pre($skus);
            if (empty($sku))
                $sku = $this->sku_model->get_row_by('product_id = ' . $product_id);
            if (!empty($sku)) {
                $maps_count_product = $this->session->userdata('maps_count_product');
                $maps_count_product ? $maps_count_product = unserialize($maps_count_product) : '';
                @$maps_count_product[$sku['id']]+=$quantity;
                $this->session->set_userdata('maps_count_product', serialize($maps_count_product));
                $this->get_total_money($maps_count_product);
                echo 1;
                exit;
            }
        }
        echo 'Sản phẩm không tồn tại';
    }

    function change_quantity() {
        $sku_id = intval($_POST['sku_id']);
        $quantity = intval($_POST['quantity']);
        if ($sku_id > 0 && $quantity > 0) {
            $maps_count_product = $this->session->userdata('maps_count_product');
            if ($maps_count_product) {
                $maps_count_product = unserialize($maps_count_product);
            }
            if (isset($maps_count_product[$sku_id])) {
                $maps_count_product[$sku_id] = $quantity;
                $this->session->set_userdata('maps_count_product', serialize($maps_count_product));
                $this->get_total_money($maps_count_product);
                echo 1;
            } else {
                echo 'Sản phẩm không tồn tai';
            }
            exit;
        }
    }

    private function get_total_money($maps_count_product) {
        $this->load->model('sku_model');
        $total = 0;
        $total_count = 0;
        if (!empty($maps_count_product)) {
            $sku_ids = '';
            foreach ($maps_count_product as $k => $v) {
                $total_count+=$v;
                $sku_ids.=$k . ',';
            }
            $skus = $this->sku_model->get_sku_price(trim($sku_ids, ','));

            foreach ($skus as $item) {
                $total+=$item['price'] * @$maps_count_product[$item['id']];
            }
        }
        $this->session->set_userdata('total_money', $total);
        $this->session->set_userdata('total_count', $total_count);
    }

    private function get_user_info() {
        if (!$this->data['user'] && isset($_COOKIE['customer_info_name'])) {
            $this->data['user'] = array(
                'name' => @$_COOKIE['customer_info_name'],
                'phone' => @$_COOKIE['customer_info_phone'],
                'email' => @$_COOKIE['customer_info_email'],
                'address' => @$_COOKIE['customer_info_address'],
                'city' => @$_COOKIE['customer_info_city'],
                'district' => @$_COOKIE['customer_info_district'],
                'name_giver' => @$_COOKIE['customer_info_name_giver'],
                'phone_giver' => @$_COOKIE['customer_info_phone_giver'],
                'address_giver' => @$_COOKIE['customer_info_address_giver'],
                'email_giver' => @$_COOKIE['customer_info_email_giver'],
            );
        }
    }

    private function check_discount_date() {
        $this->load->model('discount_model');
        $date = date('m/d');
        return $this->discount_model->get_row_by("start_date <= '$date' and end_date >= '$date'");
    }

    function cart() {
        $this->data['discount_date'] = $this->check_discount_date();
        $this->load->model(array('product_model', 'location_model', 'sku_model', 'discount_model'));
        $this->data['cities'] = $this->location_model->get_by('parent = ""', 'priority DESC,name ASC');
        $this->data['seo']['seo_title'] = 'Giỏ hàng';
        $this->data['maps_count_product'] = $this->session->userdata('maps_count_product');
        if ($this->data['maps_count_product']) {
            $this->data['maps_count_product'] = $maps_count_product = unserialize($this->data['maps_count_product']);
            if (!empty($maps_count_product)) {
                $sku_ids = '';
                foreach ($maps_count_product as $k => $v) {
                    $sku_ids .= $k . ',';
                }
                $this->data['skus'] = $this->product_model->get_for_cart(trim($sku_ids, ','));
            } else {
                $this->data['maps_count_product'] = '';
            }
        }
        $this->get_user_info();
        $this->data['districts'] = array();
        $this->data['ward'] = array();
        if ($this->data['user']['city']) {
            $this->data['districts'] = $this->location_model->get_by('parent = "' . $this->data['user']['city'] . '"', 'priority DESC,name ASC');
            if ($this->data['user']['district']) {
                $this->data['ward'] = $this->location_model->get_by('parent = "' . $this->data['user']['district'] . '"', 'priority DESC,name ASC');
            }
        }
        $this->template->write_view('content_block', 'cart', $this->data);
        $this->template->render();
    }

    function get_shipping_price($city_id = 0, $district_id = 0, $ajax = 1, $count = 0) {
//        $count = intval($count);
//        if ($count == 0) {
//            $maps_count_product = $this->session->userdata('maps_count_product');
//            $maps_count_product = unserialize($maps_count_product);
//            foreach ($maps_count_product as $item) {
//                $count+=$item;
//            }
//        }
//        $shipping_price = 0;
//        if ($count > 0 && $count < 3) {
//            $this->load->model('location_model');
//            $location = $this->location_model->get_by_key($district_id);
//            $shipping_price = @$location['price'];
//        }
//        if ($ajax) {
//            echo $shipping_price;
//        } else {
//            return $shipping_price;
//        }
            $shipping_price = 0;
            $this->load->model('location_model');
            $location = $this->location_model->get_by_key($city_id);
            $shipping_price = @$location['price'];
            if ($ajax) {
                echo $shipping_price;
            } else {
                return $shipping_price;
            }
    }

    private function rand_user() {
        $this->load->model('admin_model');
        $user = $this->admin_model->get_by('role = "staff"');
        $index = @intval(file_get_contents(APPPATH . 'cache/index_rand_user'));
        $index = count($user) > $index ? $index : 0;
        file_put_contents(APPPATH . 'cache/index_rand_user', $index + 1);
        return $user[$index]['username'];
    }

    function finish_order() {
        if (isset($_POST['submit'])) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name_giver', 'Tên khách hàng', 'required');
            $this->form_validation->set_rules('phone_giver', 'Số điện thoại', 'required');
            $this->form_validation->set_rules('address_giver', 'Địa chỉ', 'required');
            $this->form_validation->set_rules('email_giver', 'Email', 'required');
            $this->form_validation->set_rules('name', 'Tên khách hàng', 'required');
            $this->form_validation->set_rules('phone', 'Số điện thoại', 'required');
            $this->form_validation->set_rules('city', 'Tỉnh thành', 'required');
            $this->form_validation->set_rules('district', 'Quận huyện', 'required');
            $this->form_validation->set_rules('address', 'Địa chỉ', 'required');
            if ($this->form_validation->run() == true) {
                $this->load->model(array('product_model', 'location_model', 'sku_model', 'order_product_model'));
                $city = $this->location_model->get_by_key(intval($_POST['city']));
                $district = $this->location_model->get_by_key(intval($_POST['district']));
                //$ward = $this->location_model->get_by_key(intval($_POST['ward']));
                if ($city && $district && @$city['id'] == @$district['parent']) {
                    $this->data['maps_count_product'] = $this->session->userdata('maps_count_product');
                    if ($this->data['maps_count_product']) {
                        $source = @$_COOKIE['source'] ? $_COOKIE['source'] : 'other';
                        $campaign = @$_COOKIE['campaign'] ? $_COOKIE['campaign'] : 'other';
                        $this->data['maps_count_product'] = $maps_count_product = unserialize($this->data['maps_count_product']);

                        if (!empty($maps_count_product)) {
                            $sku_ids = '';
                            foreach ($maps_count_product as $k => $v) {
                                $sku_ids .= $k . ',';
                            }

                            $skus = $this->product_model->get_for_insert(trim($sku_ids, ','));
                            setcookie("customer_info_name_giver", $_POST['name_giver'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_address_giver", $_POST['address_giver'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_phone_giver", $_POST['phone_giver'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_name", $_POST['name'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_address", $_POST['address'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_phone", $_POST['phone'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_city", $city['id'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_district", $district['id'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_email", $_POST['email'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            setcookie("customer_info_email_giver", $_POST['email_giver'], (time() + (86400 * 10)), '/', $_SERVER['SERVER_NAME']);
                            $shipping_type = $city['shipper_id'];
                            $total = 0;
                            $report_product = $product_ids = '';
                            //$h = date('H') + date('i') / 60;
//                            echo (date('i')/60).'<br>';
//                            echo $h; exit;
                            $old_order = '';
                            //  if ($h <= 12 || $h > 17.5)
                            //$old_order = $this->order_services_model->get_row_by('( status = "order" or status = "unknown" or status = "waitting_pay" or status = "waitting_product")  and date like "' . date('Y-m-d') . '%" and phone = "' . $_POST['phone'] . '"');
                            $this->load->model('voucher_model');
                            $voucher = $this->voucher_model->get_row_by('status ="unused" and code = "' . $_POST['voucher'] . '"');
                            if (!empty($voucher))
                                $this->voucher_model->update(array('date_use' => date('Y-m-d H:i:s'), 'status' => 'used'), $voucher['id']);
                            $discount_date = $this->check_discount_date();
                            if (!empty($old_order)) {
                                $total = $old_order['total'];
                                $order_id = $old_order['id'];
                                $old_sku = $this->order_product_model->get_by('order_id = ' . $old_order['id'], null, 'sku_id');
                                $update_order_product = $insert_order_product = '';
                                foreach ($skus as &$item) {
                                    $item['attr'] = ($item['size'] ? $item['size'] . ' / ' : '') . $item['color'];
                                    unset($item['color']);
                                    unset($item['size']);
                                    $item['order_id'] = $old_order['id'];
                                    $item['count'] = $maps_count_product[$item['sku_id']];
                                    $total+=$item['price'] * $item['count'];
                                    if (isset($report_product[$item['product_id']])) {
                                        $report_product[$item['product_id']]['count']+=$item['count'];
                                    } else {
                                        $report_product[$item['product_id']] = array(
                                            'count' => $item['count'],
                                            'name' => $item['name'],
                                            'source' => $source
                                        );
                                    }
                                    $product_ids.=$item['product_id'] . ',';
                                    if (isset($old_sku[$item['sku_id']])) {
                                        $update_order_product[] = array(
                                            'id' => $old_sku[$item['sku_id']]['id'],
                                            'count' => $old_sku[$item['sku_id']]['count'] + $item['count']
                                        );
                                    } else {
                                        $insert_order_product[] = $item;
                                    }
                                }
                                $update_old_order = '';
                                if (!empty($voucher)) {
                                    $update_old_order = array(
                                        'voucher_id' => $voucher['id'],
                                        'voucher_price' => $voucher['price'],
                                        'voucher_type' => $voucher['type'] . ''
                                    );
                                }
                                if (!empty($discount_date)) {
                                    $update_old_order['discount_date_money'] = $discount_date['money'];
                                    $update_old_order['discount_date_title'] = $discount_date['title'];
                                }
                                if ($update_old_order) {
                                    $this->order_services_model->update($update_old_order, $old_order['id']);
                                }
                                if ($insert_order_product) {
                                    $this->order_product_model->insert_batch($insert_order_product);
                                }
                                if ($update_order_product) {
                                    @$this->order_product_model->update_batch($update_order_product);
                                }
                            } else {
                                $date_ship = explode('-', $_POST['date_ship']);
                                $date_ship = $date_ship[2] . '-' . $date_ship[1] . '-' . $date_ship[0];
                               
                                $order = array(
                                    'name' => $_POST['name'],
                                    'address' => $_POST['address'],
                                    'phone' => $_POST['phone'],
                                    'email' => @$_POST['email'],
                                    'note' => $_POST['note'],
                                    'date' => date('Y-m-d H:i:s'),
                                    'name_giver' => $_POST['name_giver'],
                                    'address_giver' => $_POST['address_giver'],
                                    'phone_giver' => $_POST['phone_giver'],
                                    'email_giver' => $_POST['email_giver'],
                                    'hours_ship' => $_POST['hours_ship'],
                                    'date_ship' => $date_ship,
                                    'status' => 'order',
                                    'city' => $city['name'],
                                    //'username_owner' => $this->rand_user(),
                                    'shipping_price' => $this->get_shipping_price($city['id'], $district['id'], 0),
                                    'service_price' => 25000,
                                    'district' => $district['name'],
                                    //'ward' => $ward['name'],
                                    'shipping_type' => $shipping_type,
                                    'source' => $source,
                                    'campaign' => $campaign,
                                    'device_type' => $this->data['is_mobile'] ? 'mobile' : 'desktop',
                                    'voucher_id' => intval(@$voucher['id']),
                                    'voucher_price' => intval(@$voucher['price']),
                                    'voucher_type' => @$voucher['type'] . '',
                                    'discount_date_money' => intval(@$discount_date['money']),
                                    'discount_date_title' => @$discount_date['title'] . '',
                                );
                                $order_id = $this->order_services_model->insert($order);
                                foreach ($skus as &$item) {
                                    $item['attr'] = ($item['size'] ? $item['size'] . ' / ' : '') . $item['color'];
                                    unset($item['color']);
                                    unset($item['size']);
                                    $item['order_id'] = $order_id;
                                    $item['count'] = $maps_count_product [$item['sku_id']];
                                    $total+=$item['price'] * $item['count'];
                                    if (isset($report_product[$item['product_id']])) {
                                        $report_product[$item['product_id']]['count']+=$item['count'];
                                    } else {
                                        $report_product[$item['product_id']] = array(
                                            'count' => $item['count'],
                                            'name' => $item['name'],
                                            'source' => $source
                                        );
                                    }
                                    $product_ids.=$item['product_id'] . ',';
                                    unset($item['is_limit']);
                                }

                                $this->order_product_model->insert_batch($skus);
                            }
                            $this->order_services_model->update(array('total' => $total), $order_id);
                            $this->data['order_id'] = $order_id;
                            $this->report_product($report_product, $product_ids);
                            $this->session->set_userdata('maps_count_product', '');
                            $this->session->set_userdata('total_money', 0);
                            $this->session->set_userdata('total_count', 0);
                            $this->session->set_flashdata('order_id', $order_id);
                            $this->data['seo']['seo_title'] = 'Đặt hàng thành công';
                            $this->data['total'] = $total;
                            // pre($this->data);
//                            $this->template->write_view('content_block', 'order_success', $this->data);
                            $this->template->write_view('content_block', 'payment_method', $this->data);
                            $this->template->render();
                            return false;
                        }
                    }
                }
            }
        }
        $order_id = $this->session->flashdata('order_id');
        if (!empty($order_id)) {
            $this->session->set_flashdata('order_id', $order_id);
            $this->data['order_id'] = $order_id;
            $this->template->write_view('content_block', 'payment_method', $this->data);
            $this->template->render();
        } else {
            redirect(base_url());
        }
    }

    private function report_product($report_product, $product_ids) {
        $this->load->model(array('report_product_model'));
        $date = date('Y-m-d');
        $report = $this->report_product_model->get_by('date = "' . $date . '" and product_id in (' . trim($product_ids, ',') . ')', null, 'product_id');
        $insert = $update = '';
        foreach ($report_product as $k => $v) {
            $temp = @$report[$k];
            if ($temp) {
                $update[] = array(
                    'id' => $temp['id'],
                    'buyed' => $temp['buyed'] + $v['count']
                );
            } else {
                $insert[] = array(
                    'buyed' => $v['count'],
                    'product_name' => $v['name'],
                    'product_id' => $k,
                    'date' => $date,
                    'source' => $v['source']
                );
            }
        }
        if ($insert) {
            @$this->report_product_model->insert_batch($insert);
        }
        if ($update) {
            @$this->report_product_model->update_batch($update);
        }
    }

    function payment($order_id = 0) {
        $order_id = intval($order_id);
        if (!empty($_POST['payment_method'])) {
            $this->order_services_model->update(array('paid_method' => $_POST['payment_method']), $order_id);
        } else {
//            $order = $this->order_services_model->get_by_key($order_id);
//            if(!empty($order)){
//                
//            }
            $this->order_services_model->update(array('paid_method' => 'direct'), $order_id);
        }
        $order = $this->order_services_model->get_by_key($order_id);
        //pre($order);
        if (!empty($order)) {
            if ($order['payed_money'] == 0 && md5($this->data['order_key'] . $order['id']) == @$_GET['token']) {
                if ($order['paid_method'] == 'nganluong' || $order['paid_method'] == 'paypal' || $order['paid_method'] == 'vtc') {
                    if ($order['paid_method'] == 'nganluong') {
                        $this->load->library('nganluong');
                        $href = $this->nganluong->buildCheckoutUrlExpand(base_url() . 'order/payment_success', USER_NGANLUONG, 'Thanh toán hoá hơn đặt hoa', 'HD-' . $order['id'], $order['total'] + $order['ship_price'], 'vnd', $order['count']);
                        echo '<META http-equiv="refresh" content="0;URL=' . $href . '"> ';
                        redirect($href);
                    } elseif ($order['paid_method'] == 'paypal') {
                        $this->load->library('paypal');
                        $total = $order['total'] + $order['shipping_price'] + $order['service_price'] ;
                        $data = $this->paypal->doPayment(round($total / DOLLAR_RATE, 1), $order_id);
                        redirect($data['link']);
                        return false;
                    } elseif ($order['paid_method'] == 'vtc') {
                        $this->load->library('vtc');
                        $order['total'] = $order['total'] + $order['shipping_price'] + $order['service_price'];
                        $data = $this->vtc->doRequest($order);
                        redirect($data);
                        return false;
                    }
                } else {
                    $this->data['page_title'] = 'Thanh toán đơn đặt hàng';
                    $order['total'] = $order['total'] + $order['shipping_price'] + $order['service_price'];
                    $this->data['order'] = $order;
                    //pre($this->data);
                    $this->send_mail_order($order_id);
                    $this->data['content_order'] = $this->data['html_order'][$order['paid_method']]['content'];
                    $this->template->write_view('content_block', 'order_success', $this->data);
                    $this->template->render();
                    return false;
                }
            }
        }
    }

    function payment_success() {
        $check = isset($_GET['transaction_info']) && isset($_GET['order_code']) &&
                isset($_GET['price']) && isset($_GET['payment_id']) &&
                isset($_GET['payment_type']) && isset($_GET['error_text']) && isset($_GET['secure_code']);
        if ($check) {
            $this->load->library('nganluong');
            $this->load->model('transaction_model');
            if (!$this->nganluong->verifyPaymentUrl($_GET['transaction_info'], $_GET['order_code'], $_GET['price'], $_GET['payment_id'], $_GET['payment_type'], $_GET['error_text'], $_GET['secure_code'])) {
                if ($_GET['error_text'] == '') {
                    $order_id = intval(str_replace('hd-', '', strtolower($_GET['order_code'])));
                    $this->data['order'] = $this->order_services_model->get_by_key($order_id);
                    if (@$this->data['order']['has_get_money']) {
                        redirect(base_url());
                    }
                    $this->order_services_model->update(array('has_get_money' => 1), $order_id);
                    $this->transaction_model->insert(array(
                        'transaction_info' => $_GET['transaction_info'],
                        'order_code' => $_GET['order_code'],
                        'price' => $_GET['price'],
                        'payment_id' => $_GET['payment_id'],
                        'payment_type' => $_GET['payment_type'],
                        'error_text' => $_GET['error_text'],
                        'secure_code' => $_GET['secure_code'],
                        'date' => date('Y-m-d H:i:s')
                    ));
                }

                $txt = "time \t" . ' : ' . date('d-m-Y H:i:s');
                foreach ($_GET as $k => $v) {
                    $txt .= "\t" . $k . ' : ' . $v;
                }
                $dir = PATH_ASSETS . 'logs/payment/' . date('m-Y') . '/';
                @mkdir($dir);
                file_put_contents($dir . 'log.txt', $txt . "\n", FILE_APPEND);
                $this->data['page_title'] = 'Thanh toán thành công';
                $this->data['content_order'] = $this->data['html_order']['default']['content'];
                $this->template->write_view('content_block', 'order_success', $this->data);
                $this->template->render();
                return false;
            }
        }
        redirect(base_url());
    }

    function vtc_payment() {
        $status = @$_GET["status"];
        $websiteid = @$_GET["website_id"];
        $order_id = @$_GET["order_code"];
        $amount = intval(@$_GET["amount"]);
        $sign = @$_GET["sign"];
        $data = $status . "-" . $websiteid . "-" . $order_id . "-" . $amount;
        $plaintext = $status . "-" . $websiteid . "-" . $order_id . "-" . $amount . "-" . SECRET_KEY_VTC;
        $mysign = strtoupper(hash('sha256', $plaintext));
        if ($mysign != $sign) {
            redirect(base_url());
        } else {
            if ($status == 1 || $status == 2) {
                $this->load->model('transaction_model');
                $this->data['order'] = $this->order_services_model->get_by_key($order_id);
                if (@$this->data['order']['has_get_money'])
                    redirect(base_url());
                $this->order_services_model->update(array('has_get_money' => 1), $order_id);
                $this->transaction_model->insert(array(
                    'transaction_info' => 'vtc pay',
                    'order_code' => $order_id,
                    'price' => $amount,
                    'payment_id' => '',
                    'date' => date('Y-m-d H:i:s')
                ));
                $this->data['page_title'] = 'Thanh toán thành công';
                $this->send_mail_order($order_id);
                $this->data['content_order'] = $this->data['html_order']['default']['content'];
                $this->template->write_view('content_block', 'order_success', $this->data);
                $this->template->render();
                return false;
            } else {
                redirect(base_url());
            }
        }
    }

    function process_paypal() {
        if ($_GET['act'] == 'cancel') {
            redirect(base_url());
        } elseif ($_GET['act'] == 'success') {
            if (isset($_GET['money']) && isset($_GET['token']) && isset($_GET['PayerID'])) {
                $order_id = intval(str_replace('hd-', '', strtolower($_GET['order_id'])));
                $this->data['order'] = $this->order_services_model->get_by_key($order_id);
                if (@$this->data['order']['payed_money']) {
                    redirect(base_url());
                }
                $this->load->library('paypal');
                $result = $this->paypal->doConfirm($_GET['money'], $_GET['token'], $_GET['PayerID'], $order_id);
                if (strtolower(@$result['PAYMENTINFO_0_ACK']) == 'success' || strtolower(@$result['PAYMENTINFO_0_ACK']) == 'successwithwarning') {
//                    $money_payed = intval(floatval($_GET['money']) * CHANGE_DOLA);
                    $this->order_services_model->update(array('payed_money' => 1), $order_id);
                    $this->load->model('transaction_model');
                    $this->transaction_model->insert(array(
                        'transaction_info' => 'paypal',
                        'order_code' => $_GET['order_id'],
                        'price' => $_GET['money'],
                        'payment_id' => $result['PAYMENTINFO_0_TRANSACTIONID'],
                        'date' => date('Y-m-d H:i:s')
                    ));
                    $txt = "time \t" . ' : ' . date('d-m-Y H:i:s');
                    foreach ($_GET as $k => $v) {
                        $txt .= "\t" . $k . ' : ' . $v;
                    }
                    $this->data['error'] = 0;
                    $dir = PATH_ASSETS . 'logs/payment/paypal/' . date('m-Y') . '/';
                    @mkdir($dir);
                    file_put_contents($dir . 'log.txt', $txt . "\n", FILE_APPEND);
                } else {
                    $this->data['order']['id'] = $_GET['order_id'];
                    $this->data['msg'] = '#' . $result['L_ERRORCODE0'] . ': ' . $result['L_SHORTMESSAGE0'];
                    $this->data['error'] = 1;
                    redirect(base_url());
                }
                $this->send_mail_order($order_id);
                $this->data['content_order'] = $this->data['html_order']['default']['content'];
                $this->template->write_view('content_block', 'order_success', $this->data);
                $this->template->render();
            } else {
                redirect(base_url());
            }
        }
    }

    function sendmail($order_id) {
        $order_id = intval($order_id);
        $data['order'] = $order = $this->order_services_model->get_by_key($order_id);
        if (!empty($order)) {
            if (@$order['has_send_mail'] == 0) {
                $this->order_services_model->update(array('has_send_mail' => 1), $order['id']);
                $config = array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'ssl://smtp.googlemail.com',
                    'smtp_port' => '465',
                    'smtp_user' => USER_SEND_MAIL,
                    'smtp_pass' => PASS_SEND_MAIL,
                    'mailtype' => 'html',
                    'charset' => 'utf-8'
                );
                $this->load->library('email', $config);
                $this->email->set_newline("\r\n");
                $this->email->from(USER_SEND_MAIL, 'Điện hoa 24h');
                $list = array($order['email_giver'], MORE_EMAIL);
                $this->email->to($list);
                $this->email->subject('Đăt hàng thành công từ điện hoa 24h');
                $url = base_url() . 'thanh-toan/' . $order['id'] . '?token=' . md5($this->data['order_key'] . $order['id']);
                $content = 'Bạn đã đặt hàng thành công, mã hoá đơn dặt hàng <b> &nbsp; HD-' . $order['id'] . '</b><br/>Bấm vào <a href="' . $url . '"><b>đây để thanh toán</b></a>';
                $this->load->model('order_product_model');
                $data['products'] = $this->order_product_model->get_by(array('order_id' => $order_id));
                $content .= $this->load->view('order_success', $data, true);
                $this->email->message($content);
                $this->email->send();
            }
        }
    }

    function sendmailtest() {
        $order_id = 13;
        $data['order'] = $order = $this->order_services_model->get_by_key($order_id);

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => '465',
            'smtp_user' => USER_SEND_MAIL,
            'smtp_pass' => PASS_SEND_MAIL,
            'mailtype' => 'html',
            'charset' => 'utf-8'
        );
        //echo '<pre>';print_r($config);exit;
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from(USER_SEND_MAIL, 'Điện hoa 24h');
        //$list = array($order['email_giver'], 'dathoatuoi2008@gmail.com');
        $this->email->to(array('datdiep81@gmail.com'));
        $this->email->subject('Đăt hàng thành công từ điện hoa 24h');
        $url = base_url() . 'thanh-toan/' . $order['id'] . '?token=' . md5($this->data['order_key'] . $order['id']);
        $content = 'Bạn đã đặt hàng thành công, mã hoá đơn dặt hàng <b> &nbsp; HD-' . $order['id'] . '</b><br/>Bấm vào <a href="' . $url . '"><b>đây để thanh toán</b></a>';
        //$this->load->model('order_product_model');
        //$data['products'] = $this->order_product_model->get_by(array('order_id' => $order_id));
        //$content .= $this->load->view('order/sendmail', $data, true);
        $this->email->message($content);
        $this->email->send();
        echo $this->email->print_debugger();
    }

    function sendmail2($order_id) {
        $data['order'] = $order = $this->order_services_model->get_by_key($order_id);
        if (@$order['has_send_mail'] == 1) {
            $this->order_services_model->update(array('has_send_mail' => 1), $order['id']);
            $list = $order['email_giver'] . ',' . MORE_EMAIL;
            $url = base_url() . 'thanh-toan/' . $order['id'] . '?token=' . md5($this->data['order_key'] . $order['id']);
            $content = 'Bạn đã đặt hàng thành công, mã hoá đơn dặt hàng <b> &nbsp; HD-' . $order['id'] . '</b><br/>Bấm vào <a href="' . $url . '"><b>đây để thanh toán</b></a>';
            $this->load->model('order_product_model');
            $data['products'] = $this->order_product_model->get_by(array('order_id' => $order_id));
            $content .= $this->load->view('order/sendmail', $data, true);
            $parrams = array(
                'from' => 0,
                'to' => $list,
                    // 'content' => urlencode($content)
            );
            $url = 'http://achau.net/sendmail';
            $this->load->library('curl');
            echo $this->curl->post($url, "postvar1=value1&postvar2=value2&postvar3=value3");
        }
    }

    function send_mail_order($order_id) {
        return false;
        $order_id = intval($order_id);
        $data['order'] = $order = $this->order_services_model->get_by_key($order_id);
        if(!empty($order)){
            $content = '';
            $this->load->model('order_product_model');
            //$data['url_pay'] = base_url() . 'thanh-toan/' . $order['id'] . '?token=' . md5($this->data['order_key'] . $order['id']);
            $data['content_order'] = $this->data['html_order'][$order['paid_method']]['content'];
            $data['products'] = $this->order_product_model->get_by(array('order_id' => $order_id));
            $content .= $this->load->view('send_mail', $data, true);
            // Cấu hình
            $this->load->library('email');
            $config['protocol'] = 'sendmail';
            $config['charset'] = 'utf-8';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);

            //cau hinh email va ten nguoi gui
            $this->email->from('no-reply@sanquatang.com', 'SÀN QUÀ TẶNG');
            //cau hinh nguoi nhan
            $this->email->to($order['email_giver']);

            $this->email->subject('Đăt hàng thành công từ Sanquatang.com');
            $this->email->message($content);

            //dinh kem file
            //$this->email->attach('/path/to/photo1.jpg');
            //thuc hien gui
            if (!$this->email->send()) {
                // Generate error
                //echo $this->email->print_debugger();
                return 1;
            } else {
                return 0;
            }
        }
    }
}
