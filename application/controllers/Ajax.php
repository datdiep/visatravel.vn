<?php

class Ajax extends FONTEND_Controller {
    //$_SERVER['HTTP_X_REQUESTED_WITH']
    public function __construct() {
        parent::__construct(true);
        $this->load->model('order_services_model');
    }

    function get_product_by_attr() {
        $parrams = $this->security->xss_clean($_POST);
        $cat_id = @intval($parrams['cat_id']);
        $key = @$parrams['key'];
        $attr = @$parrams['attr'];
        if ($cat_id && $key && $attr) {
            include APPPATH . 'config/attribute.php';
            if (isset($_attrs[$key])) {
                $this->load->model('product_model');
                $data['products'] = $this->product_model->get_for_fontend(0, LIMIT_PRODUCT, "cat_id = $cat_id and $key like '%,$attr,%'");
                $data['attr'] = $attr;
                $this->load->view($this->folder . 'list_product', $data);
            }
        }
    }

    function load_more() {
        $parrams = $this->security->xss_clean($_POST);
        $cat_id = @intval($parrams['cat_id']);
        $page = @intval($parrams['page']);
        $attr = @$parrams['attr'];
        $sort = @$parrams['sort'];
        if ($cat_id && $page > 0 && $sort) {
            include APPPATH . 'config/attribute.php';
            $cond = 'cat_id = ' . $cat_id;
            $cond_attr = '';
            if ($attr) {
                foreach ($attr as $k => $v) {
                    $cond_attr ? $cond_attr.=' and ( ' : '';
                    if (!isset($_attrs[$k]))
                        exit;
                    foreach ($v as $v1) {
                        $cond_attr ? $cond_attr.=' or ' : $cond_attr = '(';
                        $cond_attr.=$k . ' like ",%' . $v1 . '%,"';
                    }
                    $cond_attr ? $cond_attr.=' ) ' : '';
                }
            }
            $maps_sort = array('desc' => 'price DESC', 'asc' => 'price ASC');
            $sort = isset($maps_sort[$sort]) ? $maps_sort[$sort] : 'id DESC';
            if ($cond_attr)
                $cond .= ' and (' . str_replace('and (  or', 'and (', $cond_attr) . ')';
            $this->load->model('product_model');
            $data['products'] = $this->product_model->get_for_fontend(($page - 1) * LIMIT_PRODUCT, LIMIT_PRODUCT + 1, $cond, $sort);
            $return['load_more'] = count($data['products']) > LIMIT_PRODUCT ? 1 : 0;
            unset($data['products'][LIMIT_PRODUCT]);
            $return['html'] = $this->load->view('all/list_product_more', $data, true);
            echo json_encode($return);
        }
    }

}
