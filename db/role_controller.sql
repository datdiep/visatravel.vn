-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2018 at 04:28 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `visatravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `role_controller`
--

CREATE TABLE `role_controller` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(100) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(100) NOT NULL COMMENT 'codpen -> Quyen dieu khien, cog -> quyen he thong, check -> quyen mac dinh'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_controller`
--

INSERT INTO `role_controller` (`id`, `name`, `controller`, `description`, `type`) VALUES
(1, 'Quản trị tài khoản', 'admin', '', 'cog'),
(2, 'Quản lý trang', 'page', '', 'cog'),
(3, 'Đơn hàng', 'order', '', 'cog'),
(4, 'Tin tức', 'news', '', 'cog'),
(5, 'Thể loại tin tức', 'category_news', '', 'cog'),
(6, 'Thông tin trang chủ', 'homeinfo', '', 'cog'),
(7, 'Quốc gia', 'country', '', 'cog'),
(8, 'Ads', 'gallery', '', 'cog'),
(9, 'richsnipnet', 'richsnipnet', '', 'cog'),
(10, 'Quản lý đối tác', 'network', '', 'cog'),
(11, 'Quản lý khách hàng', 'customer', '', 'cog'),
(13, 'Đào Tạo ', 'training', '', 'cog'),
(14, 'Tiện ích xử lý hồ sơ', 'tool', '', 'cog'),
(15, 'sailormoon', 'sailormoon', '', 'cog'),
(16, 'Quản lý công ty', 'network_owner', '', 'codepen'),
(17, 'Quản lý phòng ban', 'department_owner', '', 'codepen'),
(18, 'Quản lý thu chi', 'transaction', '', 'codepen'),
(23, 'Hồ sơ cá nhân', 'profile', '', 'check-square'),
(24, 'Tin nhắn tự động', 'notify', '', 'check-square'),
(25, 'Gọi ajax', 'ajax', '', 'check-square'),
(26, 'Trang tổng quan', 'dashboard', '', 'check-square'),
(27, 'Quản lý công việc', 'task', '', 'check-square'),
(30, 'Quản lý lịch', 'calendar', '', 'check-square'),
(31, 'Quản lý hotline', 'hotline', '', 'cog'),
(32, 'Quyền tổng biên tập', 'editor', '', 'codepen');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `role_controller`
--
ALTER TABLE `role_controller`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `role_controller`
--
ALTER TABLE `role_controller`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
