-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2018 at 06:37 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chudutravel`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_service`
--

CREATE TABLE `category_service` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `seo_title` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `id_parent` int(10) NOT NULL,
  `view` int(11) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category_service`
--

INSERT INTO `category_service` (`id`, `title`, `alias`, `seo_title`, `meta_keyword`, `icon`, `id_parent`, `view`, `description`) VALUES
(10, 'Visa đi Châu Á', 'visa-di-chau-a', 'visa-di-chau-a', 'visa-di-chau-a', '', 0, 0, 'visa-di-chau-a'),
(11, 'Visa đi Châu Âu', 'visa-di-chau-au', 'visa-di-chau-au', 'visa-di-chau-au', '', 0, 0, 'visa-di-chau-au'),
(12, 'Visa đi Hàn Quốc', 'visa-di-han-quoc', 'visa-di-han-quoc', 'visa-di-han-quoc', 'visa-di-han-quoc-pageHd-sver.jpg', 10, 21, 'visa-di-han-quoc'),
(13, 'Visa đi Trung Quốc', 'visa-di-trung-quoc', 'visa-di-trung-quoc', 'visa-di-trung-quoc', 'visa-di-trung-quoc-Du-lich-trung-quoc.jpg', 10, 1, 'visa-di-trung-quoc'),
(14, 'Visa đi Nhật Bản', 'visa-di-nhat-ban', 'visa-di-nhat-ban', 'visa-di-nhat-ban', 'visa-di-nhat-ban-Du-lich-nhat-ban-3.jpg', 10, 0, 'visa-di-nhat-ban'),
(15, 'Visa đi Pháp', 'visa-di-phap', 'visa-di-phap', 'visa-di-phap', 'visa-di-phap-banner-29_france.jpg', 11, 6, 'visa-di-phap'),
(16, 'visa đài chau au', 'visa-dai-chau-au', 'visa-dai-chau-au', 'visa-dai-chau-au', '-Khoi-nghiep-kinh-doanh-online-co-phai-la-dung-dan-1.jpg', 11, 1, 'visa-dai-chau-au'),
(17, 'TEST IMAGE UP FOR CLIEN chau au', 'test-image-up-for-clien-chau-au', 'test-image-up-for-clien chau au', 'test-image-up-for-clien chau au', 'test-image-up-for-clien-thum.jpg', 11, 0, 'test-image-up-for-clien chau au');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_service`
--
ALTER TABLE `category_service`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_service`
--
ALTER TABLE `category_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
