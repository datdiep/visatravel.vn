-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 12, 2018 at 06:12 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `evisatop`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `page` enum('country','country_tips','page','news') COLLATE utf8_unicode_ci NOT NULL,
  `date_create` datetime NOT NULL,
  `status` enum('show','hide') COLLATE utf8_unicode_ci NOT NULL,
  `author_username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent` int(10) NOT NULL,
  `link_facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link_avatar` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `like_fb` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `avatar`, `sub_title`, `alias`, `email`, `content`, `page`, `date_create`, `status`, `author_username`, `parent`, `link_facebook`, `link_avatar`, `like_fb`) VALUES
(1, 'Minh Hy', '12670634_1153852731311956_7138070247931560871_n.jpg', 'Giám đốc công ty Việt India', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-an-do', '', 'Dịch vụ tốt, tôi đã từng làm ở đây vài lần, giá phải chăng. Nói chung là hài lòng', 'country_tips', '2017-05-01 00:00:00', 'show', 'admin', 0, '', '', 0),
(2, 'Trần Linh', '13151517_256268848059229_1445980752753825696_n.jpg', 'Bệnh viện phụ sản quốc tế', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-an-do', '', 'Tôi có gia hạn visa cho người nhà tôi quốc tịch Ấn Độ, khá hài lòng với dịch vụ này. Trả hồ sơ nhanh gọn lẹ. Lần sau gia hạn tôi sẽ liên lạc tiếp. Thanks', 'country_tips', '2017-05-02 00:00:00', 'show', 'admin', 0, '', '', 0),
(3, 'Hải Yến', '13567522_1170049123016789_4799166160621887739_n.jpg', 'Phòng sản phẩm công ty Rakos', 'dich-vu-xin-the-tam-tru-cho-nguoi-an-do-o-viet-nam', '', 'Bên này làm nhanh, yêu cầu không khó, nói chung hài lòng.', 'country_tips', '2017-05-01 00:00:00', 'show', 'phuong', 0, '', '', 0),
(4, 'Hải Minh', '424511_322681177848822_1091658877_n.jpg', 'Giám đốc công ty bánh kẹo Hải Hà', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-an-do', '', 'Cám ơn dịch vụ đã hộ trợ người nhà tôi gia hạn visa nhanh gọn lẹ', 'country_tips', '2017-05-01 00:00:00', 'show', 'admin', 0, '', '', 0),
(5, 'Minh Thư', '15621612_1032713110174007_5836098628264661845_n.jpg', 'Giám đốc bệnh viên Mekong', 'dich-vu-xin-gia-han-visa-viet-nam-cho-nguoi-nuoc-ngoai-gia-re-uy-tin-100-dau', '', 'Cám ơn dịch vụ đã hỗ trợ gia hạn visa cho đoàn bác sĩ sang việt nam của bên mình. Dịch vụ nhanh gọn, giá tốt', 'news', '2017-05-01 00:00:00', 'show', 'admin', 0, '', '', 0),
(6, 'Hải Linh', '13062027_10207579049526887_6099357071507495883_n.jpg', 'Đại học khoa học tự nhiên', 'dich-vu-xin-gia-han-visa-viet-nam-cho-nguoi-nuoc-ngoai-gia-re-uy-tin-100-dau', '', 'Hôm trước có dùng dịch vụ bên đây gia hạn visa cho người bạn nước ngoài, mình rất thích cách làm việc của bên bạn vì tư vấn nhiệt tình và làm lẹ nữa... lần sau có ai cần mình sẽ giới thiệu', 'news', '2017-05-02 00:00:00', 'show', 'admin', 0, '', '', 0),
(7, 'Minh Thư', '15622211_706659422843211_4651808049404172078_n.jpg', 'Trưởng phòng công ty bia Sài Gòn', 'dich-vu-xin-gia-han-visa-viet-nam-cho-nguoi-nuoc-ngoai-gia-re-uy-tin-100-dau', '', 'Dịch vụ nói chung ok, thấy có vẻ đông khách nên gọi điện thoại thấy hơi lâu nghe máy', 'news', '2017-05-02 00:00:00', 'show', 'admin', 0, '', '', 0),
(8, 'Minh Khôi', '482078_10151041680060822_1666274497_n.jpg', 'Nhân viên kinh doanh vé máy bay', 'dich-vu-xin-gia-han-visa-viet-nam-cho-nguoi-nuoc-ngoai-gia-re-uy-tin-100-dau', '', 'mình bên dịch vụ bán vé máy bay, có gì liên kết nha, có khách gia hạn visa cho người nước ngoài mình giới thiệu qua', 'news', '2017-05-02 00:00:00', 'show', 'admin', 0, '', '', 0),
(9, 'David Nguyễn', '17522596_10203176475970527_3432232362553961202_n.jpg', 'Công ty Truyền Thông Minh Khôi', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-singapore', '', 'Dịch vụ gia hạn visa bên bạn làm rất chuyên nghiệp, tôi hài lòng và sẽ giới thiệu cho những người tôi quen. ', 'country_tips', '2017-05-01 00:00:00', 'show', 'admin', 0, '', '', 0),
(10, 'Linh Hoai Vo', '15219458_10211310456219695_2338476963409149330_n.jpg', 'Nu Cuoi Moi', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-an-do', '', 'Nhanh gọn lẹ và chăm sóc nhiệt tình. Rất cám ơn công văn nhập cảnh', 'country_tips', '2017-05-06 00:00:00', 'show', 'datdiep', 0, '', '', 0),
(11, 'Linh Hoai Vo', '18157308_10210481159319625_5832293649814974968_n.jpg', 'Nu Cuoi Moi', 'dich-vu-gia-han-visa-viet-nam-cho-nguoi-an-do', '', 'Nhanh gọn lẹ và chăm sóc nhiệt tình. Rất cám ơn công văn nhập cảnh', 'country_tips', '2017-05-06 00:00:00', 'show', 'datdiep', 0, '', '', 0),
(13, 'Ngọc Trinh', '17903759_1654329441261151_919909293070853725_n.jpg', 'Cô Bé Ăn Hàng', 'india', '', 'Vote 5 sao cho dịch vụ tốt nè, mốt Trinh nhờ giúp nữa nhé. Thanks', 'country', '2017-03-30 13:19:11', 'show', 'admin', 0, 'https://www.facebook.com/ngoctrinhfashion89?fref=ts', 'https://scontent.fsgn2-1.fna.fbcdn.net/v/t1.0-1/p160x160/16681550_973720029425478_6410301477103602090_n.jpg?oh=6b4df75a74ab78bc7cbc08dd2cbcd418&oe=59BFFC75', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
